// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.04 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.04;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,dith:2,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32775,y:32712,varname:node_1,prsc:2|emission-45-RGB,alpha-44-A;n:type:ShaderForge.SFN_Tex2d,id:44,x:33301,y:32662,ptovrint:False,ptlb:Mask,ptin:_Mask,varname:node_2599,prsc:2,tex:72ff3dc09bbbf7942998d9535450ab39,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:45,x:33084,y:32874,ptovrint:False,ptlb:Gauge,ptin:_Gauge,varname:node_9204,prsc:2,tex:ebf6a9a48aed53b4d8a3a5a5a6c52f1a,ntxv:0,isnm:False|UVIN-3687-OUT;n:type:ShaderForge.SFN_TexCoord,id:76,x:33527,y:32816,varname:node_76,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:3441,x:33425,y:33071,ptovrint:False,ptlb:Value,ptin:_Value,varname:node_804,prsc:2,min:-0.5,cur:-0.01282051,max:0.5;n:type:ShaderForge.SFN_Add,id:3687,x:33255,y:32891,varname:node_3687,prsc:2|A-76-UVOUT,B-3441-OUT;proporder:45-44-3441;pass:END;sub:END;*/

Shader "Shader Forge/BoostGauge" {
    Properties {
        _Gauge ("Gauge", 2D) = "white" {}
        _Mask ("Mask", 2D) = "black" {}
        _Value ("Value", Range(-0.5, 0.5)) = -0.01282051
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform sampler2D _Gauge; uniform float4 _Gauge_ST;
            uniform float _Value;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float2 node_3687 = (i.uv0+_Value);
                float4 _Gauge_var = tex2D(_Gauge,TRANSFORM_TEX(node_3687, _Gauge));
                float3 emissive = _Gauge_var.rgb;
                float3 finalColor = emissive;
                float4 _Mask_var = tex2D(_Mask,TRANSFORM_TEX(i.uv0, _Mask));
                return fixed4(finalColor,_Mask_var.a);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
