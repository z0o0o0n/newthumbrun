﻿namespace Junhee
{
	using UnityEngine;
	using System.Collections;

	public class JunheeLib : MonoBehaviour
	{	
		public delegate void MonoEvent();
		public static event MonoEvent UpdateEvent;

		void Update()
		{
			if (JunheeLib.UpdateEvent != null) JunheeLib.UpdateEvent();
		}
	}
}
