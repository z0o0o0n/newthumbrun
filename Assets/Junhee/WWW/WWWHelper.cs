﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class WWWHelper : MonoBehaviour 
{
    public delegate void HttpRequestDelegate(string id, string wwwText);
    public event HttpRequestDelegate OnHttpRequest;
    public event HttpRequestDelegate Error;
    public event HttpRequestDelegate TimeOut;

    private int _requestId;

    static WWWHelper current = null;
    static GameObject container = null;

    private Coroutine _currentCoroutine;

    public static WWWHelper instance
    {
        get 
        {
            if (current == null) 
            {
                container = new GameObject();
                container.name = "WWWHelper";
                current = container.AddComponent(typeof(WWWHelper)) as WWWHelper;
            }
            return current;
        }
    }

    public void Get(string id, string url) 
    {
        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(id, www));
    }

    public void Post(string id, string url, IDictionary<string, string> stringArgs) 
    {
        WWWForm form = new WWWForm();

        Dictionary<string, string> headers = form.headers;
        headers["Content-Type"] = "application/x-www-form-urlencoded";

        foreach (KeyValuePair<string, string> stringArg in stringArgs) 
        {
            //Debug.Log("********************************* " + stringArg.Key + " : " + stringArg.Value);
            form.AddField(stringArg.Key, stringArg.Value);
        }

        //Debug.Log("-----> url: " + url);
        WWW www = new WWW(url, form.data, headers);

        DOTween.Kill("WWWHelperCoroutine");
        DOVirtual.DelayedCall(15f, ()=>OnTimeOut(id)).SetId("WWWHelperCoroutine");

        if(_currentCoroutine != null) StopCoroutine(_currentCoroutine);
        _currentCoroutine = StartCoroutine(WaitForRequest(id, www));
    }

    private IEnumerator WaitForRequest(string id, WWW www)
    {
        do
        {
            yield return null;
        }
        while (!www.isDone);

        if(www.error != null)
        {
            // Debug.LogError("www Error: " + www.error);
            Debug.Log(www.text);
            DOTween.Kill("WWWHelperCoroutine");
            if(Error != null) Error(id, www.text);
            yield break;
        }

        Debug.Log(www.text);
        DOTween.Kill("WWWHelperCoroutine");
        if (OnHttpRequest != null) OnHttpRequest(id, www.text);

        www.Dispose();
    }

    private void OnTimeOut(string id)
    {
        StopCoroutine(_currentCoroutine);
        _currentCoroutine = null;
        if (TimeOut != null) TimeOut(id, "Error.Timeout");
        Debug.Log("WWWHelper Time Out");
    }
}