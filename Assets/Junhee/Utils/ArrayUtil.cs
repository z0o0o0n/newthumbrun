﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArrayUtil
{
    public static void PrintArrayList(ArrayList arrayList)
    {
        string data = "";
        for (int i = 0; i < arrayList.Count; i++)
        {
            if (i < (arrayList.Count - 1)) data += arrayList[i].ToString() + " | ";
            else data += arrayList[i].ToString();
        }
        Debug.Log(data);
    }
}
