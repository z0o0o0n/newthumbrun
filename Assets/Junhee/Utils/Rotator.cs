﻿namespace Junhee.Utils
{
	using UnityEngine;
	using System.Collections;

	public class Rotator : MonoBehaviour
	{
		public float speed = 10;

		void Start ()
		{
		
		}

		void Update ()
		{
	//		Quaternion target = Quaternion.Euler(1, 0, 1);
			transform.Rotate (Vector2.up * (Time.deltaTime * speed));
		}
	}
}