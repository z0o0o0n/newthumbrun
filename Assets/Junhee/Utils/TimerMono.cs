﻿namespace Junhee.Utils
{
    using UnityEngine;
    using System.Collections;

    public class TimerMono : MonoBehaviour
    {
        public delegate void TimerEvent();
        public event TimerEvent Tick;
        public event TimerEvent TimerComplete;

        public string name;
        public float delay = 1.0f;
        public int repeatCount = 0;
        public bool isStart = false;
        public bool isComplete = false;
        public int currentCount
        {
            get { return _currentRepeatCount; }
        }

        private int _currentRepeatCount = 0;
        private float _currentDelay = 0.0f;

        public void SetTime(float delay = 2.0f, int repeatCount = 0)
        {
            this.delay = delay;
            this.repeatCount = repeatCount;
            _currentDelay = delay;
        }

        void Update()
        {
            if (isStart)
            {
                if (!isComplete && _currentDelay >= delay)
                {
                    _currentDelay = 0;
                    if (Tick != null) Tick();
                    _currentRepeatCount++;
                }
                _currentDelay += Time.deltaTime;

                if (repeatCount == -1)
                {

                }
                else if ((repeatCount - 1) < _currentRepeatCount)
                {
                    StopTimer();
                    isComplete = true;
                    if (TimerComplete != null) TimerComplete();
                }
            }
        }

        public void StartTimer()
        {
            if (!isStart)
            {
                isStart = true;
                isComplete = false;
            }
        }

        public void StopTimer()
        {
            if (isStart)
            {
                isStart = false;
                _currentDelay = delay;
                _currentRepeatCount = 0;
            }
        }
    }
}
