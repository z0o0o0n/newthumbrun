﻿using UnityEngine;
using System.Collections;
using System;

public class StringUtil
{
	public static string[] Split(string targetString, string separator)
    {
        string[] separatorList = { separator };
        return targetString.Split(separatorList, StringSplitOptions.None);
    }
}
