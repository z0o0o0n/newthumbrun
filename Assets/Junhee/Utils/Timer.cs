﻿namespace Junhee.Utils
{
	using UnityEngine;
	using System.Collections;

	public class Timer
	{
		public delegate void TimerEvent();
		public event TimerEvent Tick;
		public event TimerEvent TimerComplete;

		public float delay = 1.0f;
		private float _currentDelay = 0.0f;
		public int repeatCount = 0;
		private int _currentRepeatCount = 0;
		public int currentCount
		{
			get{ return _currentRepeatCount; }
		}

		public bool isStart = false;
        public bool isComplete = false;

		public Timer(float delay = 2.0f, int repeatCount = 0)
		{
			this.delay = delay;
			this.repeatCount = repeatCount;
            _currentDelay = delay;
		}

		public void Update()
		{
            if (isStart)
			{
                if (!isComplete && _currentDelay >= delay)
				{
					_currentDelay = 0;
					if(Tick != null) Tick();
					_currentRepeatCount++;
				}
				_currentDelay += Time.deltaTime;

                if(repeatCount == -1)
                {

                }
                else if ((repeatCount - 1) < _currentRepeatCount)
                {
                    Stop();
                    isComplete = true;
                    if (TimerComplete != null) TimerComplete();
                }
			}
		}

		public void Start()
		{
            if (!isStart) 
			{
                isStart = true;
                isComplete = false;
			}
		}

		public void Stop()
		{
            if (isStart)
            {
                isStart = false;
                _currentDelay = delay;
                _currentRepeatCount = 0;
            }
		}
	}
}
