﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AxisController : MonoBehaviour
{
    public delegate void AxisEvent();
    public event AxisEvent OnStartDecrement;
    public event AxisEvent OnStartIncrement;
    public event AxisEvent TurnPositive;
    public event AxisEvent TurnNegative;

    [Tooltip("-1 ~ +1 까지의 이동 시간")]
    public float changeTime = 1; // -1 ~ +1 까지의 이동 시간 기준
    [Tooltip("목표 축으로 증감 시 Ease 값")]
    public AnimationCurve curve; // 목표 축으로 증감 시 Ease 값

    private float _axis = 0.0f;

    public float GetAxis()
    {
        return _axis;
    }

    public void Decrease()
    {
        float targetAxis = -1;
        float time = CalculateTime(targetAxis);
        DOTween.Kill("axisTween");
        DOTween.To(() => _axis, x => _axis = x, targetAxis, time).SetId("axisTween");
    }

    public void Increase()
    {
        float targetAxis = 1;
        float time = CalculateTime(targetAxis);
        DOTween.Kill("axisTween");
        DOTween.To(() => _axis, x => _axis = x, targetAxis, time).SetId("axisTween");
    }

    public void Zero(float time)
    {
        float targetAxis = 0;
        DOTween.Kill("axisTween");
        DOTween.To(() => _axis, x => _axis = x, targetAxis, time).SetId("axisTween");
    }

    // 증감 폭에 따른 이동시간을 계산 함
    private float CalculateTime(float targetAxis)
    {
        // -1 ~ +1 까지의 증감 시간을 1초로 설정. (-1 ~ +1의 간격은 2만큼의 간격이므로 1로 맞추기 위해 2로 나눠 줌)
        float time = (Mathf.Abs(targetAxis - _axis) / 2) * changeTime;
        return time;
    }

	void Start ()
    {
	
	}
	
	void Update ()
    {
	
	}
}
