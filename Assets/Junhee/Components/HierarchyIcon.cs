﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections.Generic;

public class HierarchyIcon : MonoBehaviour
{
	private Texture2D texture;
    //private List<int> _markedObjects;
	
	void Start ()
	{
		#if UNITY_EDITOR
        texture = Resources.Load("uv") as Texture2D;
        EditorApplication.hierarchyWindowItemOnGUI += HierarchyItemEA;
		#endif
	}

	void HierarchyItemEA (int instanceID, Rect selectionRect)
    {
        if (45028 == instanceID)
        {
            Rect r = new Rect(selectionRect);
            r.x = r.width - 4;
            r.y = r.y - 1;
            r.width = 20;
            r.height = 20;
            GUI.Label(r, texture);
        }
    }

    void Destroy()
    {
        #if UNITY_EDITOR
            EditorApplication.hierarchyWindowItemOnGUI -= HierarchyItemEA;
        #endif
    }
}
