﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class NetworkChecker : MonoBehaviour
{
    public delegate void Callback(bool isConnection);

    private const bool _allowCarrierDataNetwork = false;
    private const string _pingAddress = "8.8.8.8"; // Google Public DNS server

    private bool _debug = true;
    private float _waitingTime = 1.0f;
    private float _currentTime = 0.0f;
    private Ping _ping;
    //private float pingStartTime;
    private bool _isConnection = false;
    private bool _isTimeout = false;
    private Callback _callbackFunction;

    public void Start()
    {
    }
    
    /**
     * watingTime은 Ping 후 응답 대기 시간으로 최소 값은 0.1로 함
     */
    public void Check(float watingTime, Callback callbackFunction)
    {
        _waitingTime = watingTime;
        if (_waitingTime < 0.1)
        {
            Debug.LogWarning("NetworkChecker.Check() : watingTime은 0 또는 음수일 수 없습니다. 최소값인 0.1로 강제 변경 되었습니다.");
            _waitingTime = 0.1f;
        }

        _callbackFunction = callbackFunction;
        CheckThroughSystem();
        if (_isConnection) CheckPing();
        else _callbackFunction(false);
    }

    private void CheckThroughSystem()
    {
        if (_debug) Debug.Log("Start System Check: " + Application.internetReachability);
        switch (Application.internetReachability)
        {
            case NetworkReachability.ReachableViaLocalAreaNetwork:
                _isConnection = true;
                break;
            case NetworkReachability.ReachableViaCarrierDataNetwork:
                _isConnection = true;
                break;
            default:
                _isConnection = false;
                break;
        }
        if (_debug) Debug.Log("NetworkChecker - CheckThroughSystem() / isConnection: " + _isConnection);
    }

    private void CheckPing()
    {
        if (_debug) Debug.Log("NetworkChecker - CheckPing()");

        /**
         * Time.timeScale 값이 0일 경우에도 Timeout Check를 위해 DOTween으로 Timeout을 Check함 
         */
        _isTimeout = false;
        _currentTime = 0;
        DOTween.Kill("NetworkChecker_Timeout_Tween");
        DOTween.To(() => _currentTime, x => _currentTime = x, _waitingTime, _waitingTime).SetId("NetworkChecker_Timeout_Tween").OnComplete(OnTimeoutTweenCompleted).SetUpdate(true);

        _ping = new Ping(_pingAddress);
        //pingStartTime = Time.time;
    }

    private void OnTimeoutTweenCompleted()
    {
        _isTimeout = true;
    }

    void Update()
    {
        if (_ping != null)
        {
            bool stopCheck;
            if (_debug) Debug.Log("NetworkChecker.Update() / isDone: " + _ping.isDone);
            if (_debug) Debug.Log("NetworkChecker.Update() / isTimeout: " + _isTimeout);
            if (_ping.isDone)
            {
                _isConnection = true;
                stopCheck = true;
            }
            else if (!_isTimeout) // 아직 제한시간이 남은 경우
            {
                _isConnection = false;
                stopCheck = false;
            }
            else
            {
                _isConnection = false;
                stopCheck = true;
            }

            if (stopCheck)
            {
                _ping.DestroyPing();
                _ping = null;
                if (_debug) Debug.Log("End Ping Check");

                _callbackFunction(_isConnection);
            }
        }
    }

    //IEnumerator CheckAvailable()
    //{
    //    while(true)
    //    {
    //        yield return new WaitForSeconds(3.0f);
    //        Check();
    //        Debug.Log(">>> Connection: " + isConnection);
    //    }
    //}
}