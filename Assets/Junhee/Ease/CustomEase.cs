﻿using UnityEngine;
using System.Collections;

public class CustomEase : MonoBehaviour
{
    public AnimationCurve OutPongElastic;
    public AnimationCurve OutBangElastic;

    private static CustomEase _instance = null;
    public static CustomEase instance
    {
        get
        {
            //if (_instance == null) _instance = (CustomEase)FindObjectOfType(typeof(CustomEase));
            //if (_instance == null) _instance = (CustomEase)FindObjectOfType(typeof(CustomEase));
            return _instance;
        }
    }

    void Awake()
    {
        if(_instance == null)
        {
            _instance = gameObject.GetComponent<CustomEase>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}