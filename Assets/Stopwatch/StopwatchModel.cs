﻿using UnityEngine;
using System.Collections;
using System;

public class StopwatchModel : MonoBehaviour
{
    public delegate void StopwatchEventHandler();
    public event StopwatchEventHandler Tick;

    private int _min = 0;
    private int _sec = 0;
    private float _msec = 0.00f;
    private float _startTime;

    private bool _isStarted = false;

	public int GetMin()
    {
        return _min;
    }

    public int GetSec()
    {
        return _sec;
    }

    public float GetMsec()
    {
        return _msec;
    }

    public void Play()
    {
        if (!_isStarted)
        {
            _isStarted = true;
            _startTime = Time.time;
        }
    }

    public void Pause()
    {
        if (_isStarted)
        {
            _isStarted = false;
        }
    }

    public void Stop()
    {
        _isStarted = false;
        _min = 0;
        _sec = 0;
        _msec = 0.0f;
        if (Tick != null) Tick();
    }

    public void Update()
    {
        if (_isStarted)
        {
            float[] timeSplit = SplitTime(Time.time - _startTime);
            _min = Convert.ToInt32(timeSplit[0]);
            _sec = Convert.ToInt32(timeSplit[1]);
            _msec = timeSplit[2];
            if (Tick != null) Tick();
        }
    }




    // 외부 유틸로 분리할 것
    // Time.time 값을 분, 초, 밀리초로 나눠줌.
    private float[] SplitTime(float time)
    {
        string timeString = time.ToString();
        string[] timeSplit = timeString.Split('.');
        float[] returnTime = new float[3];

        int min = Convert.ToInt32(timeSplit[0]) / 60;
        int sec = Convert.ToInt32(timeSplit[0]) - (min * 60);
        float msec = time - Convert.ToInt32(timeSplit[0]);

        returnTime[0] = min;
        returnTime[1] = sec;
        returnTime[2] = msec;

        return returnTime;
    }
}
