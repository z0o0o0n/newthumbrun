﻿using UnityEngine;
using System;
using System.Collections;
using Junhee.Utils;

public class StopwatchView : MonoBehaviour
{
    public StopwatchModel model;
    public StopwatchController controller;
    public UILabel bestTimeUILabel;
    private UILabel uilabel;

	// Use this for initialization
	void Start () {
        model.Tick += Tick;
		uilabel = GetComponent<UILabel>();
		Debug.Log (Screen.width/2 + " : " +  bestTimeUILabel);
        //uilabel.transform.localPosition = new Vector3 (uilabel.transform.localPosition.x, Screen.height / 2 - 34, uilabel.transform.localPosition.z);
        //bestTimeUILabel.transform.localPosition = new Vector3 ((Screen.width/2)-200, Screen.height / 2 - 30, uilabel.transform.localPosition.z);
	}
	
	// Update is called once per frame
	void Update () {
       // transform.position = new Vector3(Camera.main.transform.position.x - 0.8f, Camera.main.transform.position.y + 2, transform.position.z);
	}

    void Tick()
    {
        if (model.GetMsec() != 0) {
			uilabel.text = Format.ConvertDigit(model.GetSec().ToString(), 2, "0") + " . " + Format.ConvertDigit(model.GetMsec().ToString().Substring(2), 3, "0");
        }
        //Debug.Log(model.GetMsec().ToString());    
    }

    public void CheckTime()
    {
        //bestTimeUILabel.GetComponent<TextMesh>().text = Format.ConvertDigit(model.GetMin().ToString(), 2, "0") + " : " + Format.ConvertDigit(model.GetSec().ToString(), 2, "0") + " : " + model.GetMsec().ToString().Substring(2, 2);
        if (bestTimeUILabel.text == "00.000")
        {
            Debug.Log("best ------------------ " + Format.ConvertDigit(model.GetSec().ToString(), 2, "0") + " . " + Format.ConvertDigit(model.GetMsec().ToString().Substring(2), 3, "0"));
            bestTimeUILabel.text = Format.ConvertDigit(model.GetSec().ToString(), 2, "0") + " . " + Format.ConvertDigit(model.GetMsec().ToString().Substring(2), 3, "0");
        }
        else
        {
            string[] bestTimeTextList = bestTimeUILabel.text.Split('.');
            if (Convert.ToInt32(bestTimeTextList[0]) > Convert.ToInt32(Format.ConvertDigit(model.GetSec().ToString(), 2, "0")))
            {
                bestTimeUILabel.text = Format.ConvertDigit(model.GetSec().ToString(), 2, "0") + " . " + Format.ConvertDigit(model.GetMsec().ToString().Substring(2), 3, "0");
            }
            else if (Convert.ToInt32(bestTimeTextList[0]) == Convert.ToInt32(Format.ConvertDigit(model.GetSec().ToString(), 2, "0")))
            {
                if (Convert.ToInt32(bestTimeTextList[1]) > Convert.ToInt32(Format.ConvertDigit(model.GetMsec().ToString().Substring(2), 3, "0")))
                {
                    bestTimeUILabel.text = Format.ConvertDigit(model.GetSec().ToString(), 2, "0") + " . " + Format.ConvertDigit(model.GetMsec().ToString().Substring(2), 3, "0");
                }
            }
        }
    }
}
