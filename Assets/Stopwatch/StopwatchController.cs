﻿using System.Collections;
using UnityEngine;

public class StopwatchController : MonoBehaviour
{
    public StopwatchModel _model;
    public IStopwatchViewContainer _view;

    public void Play ()
    {
        _model.Play();
    }

	public void Pause ()
    {
        _model.Pause();
    }

    public void Stop ()
    {
        _model.Stop();
    }
}
