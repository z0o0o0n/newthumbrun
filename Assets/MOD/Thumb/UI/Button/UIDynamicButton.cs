﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class UIDynamicButton : MonoBehaviour 
{
    /**
     * 떰 시리즈에서 공통으로 사용하는 버튼
     * - 알림을 요청하면 우측 상단에 빨간색 알림이 뜸.
     * - 버튼 하단에 상태창을 열어 다양한 용도로 활용할 수 있음. (주로 현재상태나 남은시간으로 활용)
     * - 버튼은 content를 변경할 수 있음.
     * - 비활성화 할 수 있음.
     */
    public delegate void ButtonEvent(GameObject target);
    public event ButtonEvent Click;

    public GameObject button;

    [SerializeField]
    private GameObject _newIcon;
    [SerializeField]
    private UIWidget _content; // 버튼의 기능을 표시하는 이미지, 텍스트의 집합.
    [SerializeField]
    private GameObject _buttonBg;
    [SerializeField]
    private GameObject _infoPanel;
    [SerializeField]
    private GameObject _shadow;
    [SerializeField]
    private float _contentStartY;
    [SerializeField]
    private float _infoPanelOpenY;
    [SerializeField]
    private float _infoPanelCloseY;
    private float _mvmtDistance = 8f;
    private float _shadowMvmtDistance = 10f;
    private bool _isEnable = true;

    public bool isEnable
    {
        get{ return _isEnable; }
    }





    void Awake ()
    {
        _newIcon.gameObject.SetActive(false);
    }

	void Start () 
    {
	    
	}
	
	void Update () 
    {
        //if(Input.GetKeyUp(KeyCode.Alpha1))
        //{
        //    Enable();
        //}
        //else if(Input.GetKeyUp(KeyCode.Alpha2))
        //{
        //    Disable();
        //}
        //else if(Input.GetKeyUp(KeyCode.Alpha3))
        //{
        //    ShowInfoPanel();
        //}
        //else if(Input.GetKeyUp(KeyCode.Alpha4))
        //{
        //    HideInfoPanel();
        //}
        //else if(Input.GetKeyUp(KeyCode.Alpha5))
        //{
        //    ShowNewIcon();
        //}
    }

    public void OnPress()
    {
        if(_isEnable)
        {
            _content.transform.DOLocalMoveY(_contentStartY - _mvmtDistance, 0f);
            _buttonBg.transform.DOLocalMoveY(_contentStartY - _mvmtDistance, 0f);
            DOTween.To(()=>_content.alpha, x=>_content.alpha=x, 1f, 0f);
            _shadow.gameObject.SetActive(false);
        }
    }

    public void OnRelease()
    {
        if(_isEnable)
        {
            _content.transform.DOLocalMoveY(_contentStartY, 0f);
            _buttonBg.transform.DOLocalMoveY(_contentStartY, 0f);
            DOTween.To(()=>_content.alpha, x=>_content.alpha=x, 1f, 0f);
            _shadow.gameObject.SetActive(true);

            _newIcon.gameObject.SetActive(false);

            if(Click != null) Click(gameObject);
        }
    }

    public void OnDragOut()
    {
        if(_isEnable)
        {
            _content.transform.DOLocalMoveY(_contentStartY, 0f);
            _buttonBg.transform.DOLocalMoveY(_contentStartY, 0f);
            DOTween.To(()=>_content.alpha, x=>_content.alpha=x, 1f, 0f);
            _shadow.gameObject.SetActive(true);
        }
    }



    #region Public Functions
    // 버튼을 사용할 수 없는 상태
    public void Enable()
    {
        if(!_isEnable)
        {
            _isEnable = true;
            _content.transform.DOLocalMoveY(_contentStartY, 0f);
            _buttonBg.transform.DOLocalMoveY(_contentStartY, 0f);
            DOTween.To(()=>_content.alpha, x=>_content.alpha=x, 1f, 0f);
            _shadow.gameObject.SetActive(true);
        }
    }

    public void Disable()
    {
        if(_isEnable)
        {
            _isEnable = false;
            _content.transform.DOLocalMoveY(_contentStartY - _mvmtDistance, 0f);
            _buttonBg.transform.DOLocalMoveY(_contentStartY - _mvmtDistance, 0f);
            DOTween.To(()=>_content.alpha, x=>_content.alpha=x, 0.3f, 0f);
            _shadow.gameObject.SetActive(false);
        }
    }

    public void ShowInfoPanel()
    {
        _infoPanel.transform.DOLocalMoveY(_infoPanelOpenY, 0f);
    }

    public void HideInfoPanel()
    {
        _infoPanel.transform.DOLocalMoveY(_infoPanelCloseY, 0f);
    }

    public void ShowNewIcon()
    {
        _newIcon.gameObject.SetActive(true);
    }

    public void HideNewIcon()
    {
        _newIcon.gameObject.SetActive(false);
    }
    #endregion
}
