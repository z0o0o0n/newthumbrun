﻿using UnityEngine;
using System.Collections;

public class UIBasicButton : MonoBehaviour 
{
    public delegate void UIButtonEvent();
    public event UIButtonEvent On_Press;
    public event UIButtonEvent On_Release;
    public event UIButtonEvent On_Click;

    private bool _isDebug = false;
    private string _logIdentifier = ">>>>>>>>>>";

    public UIButton _uiButton;
    public AudioSource _audioSource;
    public AudioClip _pressSound;
    public AudioClip _clickSound;

    void Awake ()
    {
        _uiButton = GetComponent<UIButton>();
    }

	void Start () 
	{
	    
	}
	
    //void Update () 
    //{
    //    if(Input.GetKeyUp(KeyCode.A))
    //    {
    //        SetButtonActive(true);
    //    }
    //    else if(Input.GetKeyUp(KeyCode.S))
    //    {
    //        SetButtonActive(false);
    //    }
    //}

    void OnClick()
    {
        if (_uiButton.enabled)
        {
            if (_isDebug) Debug.Log(_logIdentifier + "버튼 Click");
            if (_audioSource && _clickSound)
            {
                _audioSource.clip = _clickSound;
                _audioSource.Play();
            }

            if (On_Click != null) On_Click();
        }
        else
        {
            if (_isDebug) Debug.Log(_logIdentifier + "현재 버튼이 비활성화 상태입니다.");
        }
    }

    void OnPress(bool isDown)
    {
        if (_uiButton.enabled)
        {
            if (isDown)
            {
                if(_isDebug) Debug.Log(_logIdentifier + "버튼 Press");
                if (_audioSource && _pressSound)
                {
                    _audioSource.clip = _pressSound;
                    _audioSource.Play();
                }
                if (On_Press != null) On_Press();
            }
            else
            {
                if(_isDebug) Debug.Log(_logIdentifier + "버튼 Release");
                if (On_Release != null) On_Release();
            }
        }
        else
        {
            if(_isDebug) Debug.Log(_logIdentifier + "현재 버튼이 비활성화 상태입니다.");
        }
    }

    public void SetButtonActive(bool isActivation)
    {
        if (isActivation)
        {
            if (_isDebug) Debug.Log(_logIdentifier + "버튼 활성화");
        }
        else
        {
            if (_isDebug) Debug.Log(_logIdentifier + "버튼 비활성화");
        }
        _uiButton.enabled = isActivation;
    }

	public void IsEnabled(bool isEnabled)
	{
		_uiButton.isEnabled = isEnabled;
	}
}
