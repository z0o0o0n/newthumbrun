﻿namespace Com.Mod.ThumbRun.UI.Popup
{
    using Setting.Application;
    using UnityEngine;

    public class BasicPopup : MonoBehaviour 
	{
        private bool _isCloseable = true;

        public bool isCloseable
        {
            get{ return _isCloseable; }
            set{ _isCloseable = value; }
        }



        private void Awake()
        {
            //Close();
        }

        void Start()
        {
            gameObject.transform.localPosition = Vector3.zero;
        }

        void Update()
        {

        }





        public virtual void Open()
        {
            _isCloseable = isCloseable;
			LobbyBackButtonManager.SetCurrentPopup(this);
            gameObject.SetActive(true);
        }

        public virtual void Close()
        {
            if(_isCloseable)
            {
                LobbyBackButtonManager.SetCurrentPopup(null);
                gameObject.SetActive(false);
            }
        }

        public void SetCloseable(bool isCloseable)
        {
            _isCloseable = isCloseable;
        }
	}
}