﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class HttpRequest : MonoBehaviour
{
    public delegate void HttpRequestDelegate(string id, string wwwText);
    public event HttpRequestDelegate Complete;
    public event HttpRequestDelegate Error;
    public event HttpRequestDelegate TimeOut;

    private Coroutine _currentCoroutine;
    private bool _isDebug = false;




    public void Get(string id, string url, float limitTime = 15f)
    {
        WWW www = new WWW(url);

        DOTween.Kill("WaitForRequest." + GetInstanceID());
        DOVirtual.DelayedCall(limitTime, ()=>OnTimeOut(id, www)).SetId("WaitForRequest." + GetInstanceID());

        if (_currentCoroutine != null) StopCoroutine(_currentCoroutine);
        _currentCoroutine = StartCoroutine(WaitForRequest(id, www));
    }

    public void Post(string id, string url, IDictionary<string, string> stringArgs, float limitTime = 15f)
    {
        WWWForm form = new WWWForm();
        foreach (KeyValuePair<string, string> stringArg in stringArgs)
        {
            form.AddField(stringArg.Key, stringArg.Value);
        }

        WWW www = new WWW(url, form.data);

        DOTween.Kill("WaitForRequest." + GetInstanceID());
        DOVirtual.DelayedCall(limitTime, ()=>OnTimeOut(id, www)).SetId("WaitForRequest." + GetInstanceID());

        if (_currentCoroutine != null) StopCoroutine(_currentCoroutine);
        _currentCoroutine = StartCoroutine(WaitForRequest(id, www));
    }

    private IEnumerator WaitForRequest(string id, WWW www)
    {
        do { yield return null; }
        while (!www.isDone);

        if (www.error != null)
        {
            Debug.Log("Http Request / " + www.text + " / " + www.error);
            DOTween.Kill("WaitForRequest." + GetInstanceID());
            if (Error != null) Error(id, www.text);
            www.Dispose();
            yield break;
        }

        Debug.Log("Http Request / " + www.text);
        DOTween.Kill("WaitForRequest." + GetInstanceID());
        if (Complete != null) Complete(id, www.text);
        www.Dispose();
    }

    private void OnTimeOut(string id, WWW www)
    {
        Debug.Log("Http Request / Time Out");
        StopCoroutine(_currentCoroutine);
        _currentCoroutine = null;
        if (TimeOut != null) TimeOut(id, "Timeout");
        www.Dispose();
    }
}