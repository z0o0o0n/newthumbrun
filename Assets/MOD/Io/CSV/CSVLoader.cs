﻿using UnityEngine;

namespace Com.Mod.Io
{
	public class CSVLoader : MonoBehaviour 
	{
		private TextAsset _csvFile;

		void Start () 
		{
		
		}

		void Update () 
		{
		
		}

		

		#region Public Functions
		public void Load(string path)
		{
			_csvFile = (TextAsset)Resources.Load(path) as TextAsset;
		}

		public void LoadURL(string url)
		{

		}

		public string GetText()
		{
			return _csvFile.text;
		}
		#endregion
	}
}