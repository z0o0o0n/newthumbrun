﻿namespace Com.Mod.Io
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class CSVParser : MonoBehaviour
    {
        private static 

        void Start()
        {

        }

        void Update()
        {

        }




        public static CSVData Parse(int columnCount, string csvText)
        {
            Dictionary<string, int> columnKeyIndexes = new Dictionary<string, int>();
            List<Dictionary<string, string>> rawCsvData = new List<Dictionary<string, string>>();

            string aa = "설정&#44;메뉴";
            Debug.Log("................. " + aa.Replace("&#44;", ","));

            string[] rows = csvText.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None); // 라인 분리
            for (int i = 0; i < rows.Length; i++)
            {
                //Debug.Log("Line[" + i + "]: " + lineArray[i]);
            }

            // Column Key별 배열 생성
            string[] types = rows[0].Split(',');
            for (int i = 0; i < types.Length; i++)
            {
                //Debug.Log("type [" + i + "]: " + types[i]);
                rawCsvData.Add(new Dictionary<string, string>());
                if (i != 0)
                {
                    columnKeyIndexes.Add(types[i], i - 1);
                }
            }

            // 각 // Column Key별 배열에 row Field 값 추가
            for (int i = 0; i < rows.Length; i++)
            {
                string[] rowFields = rows[i].Split(',');
                for (int j = 0; j < rowFields.Length; j++)
                {
                    if (j != 0)
                    {
                        Dictionary<string, string> column = rawCsvData[j - 1];
                        string key = rowFields[0];
                        string rowFieldValue = rowFields[j].Replace("&#44;", ",");
                        //Debug.Log(rowFields[j] + " / " + rowFieldValue + " / " + rowFields[j].Replace("&#44;", ","));
                        column.Add(key, rowFieldValue);
                    }
                }
            }

            CSVData csvData = new CSVData(columnKeyIndexes, rawCsvData);
            return csvData;
        }
    }
}