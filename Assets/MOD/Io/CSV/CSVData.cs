﻿namespace Com.Mod.Io
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class CSVData
    {
        private Dictionary<string, int> _columnKeyIndexes;
        private List<Dictionary<string, string>> _rawCsvData;

        public CSVData(Dictionary<string, int> columnKeyIndexes, List<Dictionary<string, string>> rawCsvData)
        {
            _columnKeyIndexes = columnKeyIndexes;
            _rawCsvData = rawCsvData;
        }





        public string GetField(string columnKey, string rowKey)
        {
            int columnKeyIndex = _columnKeyIndexes[columnKey];
            string result = _rawCsvData[columnKeyIndex][rowKey];
            return result;
        }
    }
}