﻿using UnityEngine;
using Com.Mod.Game.LanguageManager;
using UnityEngine.UI;

public class NGUILocalizeText : MonoBehaviour 
{
    public delegate void LocalizeTextEventHandler();
    public LocalizeTextEventHandler Changed;

	private LanguageManager _languageManager;
	private UILabel _text;
	[SerializeField]
	private string _id;
    [SerializeField]
    private int _maxWidth = 0;
    private UILabel.Overflow _originOverflow;

    public UILabel label
    {
        get { return _text; }
    }

    void Awake ()
	{
		_languageManager = GameObject.FindGameObjectWithTag("LanguageManager").GetComponent<LanguageManager>();
		_languageManager.LanguageChanged += OnLanguageChanged;

        _text = GetComponent<UILabel>();
        _originOverflow = _text.overflowMethod;
    }

	void Start () 
	{
		UpdateText();
	}
	
	void Update () 
	{
	
	}

	void OnDestroy()
	{
		_languageManager.LanguageChanged -= OnLanguageChanged;
	}



	public string GetText()
	{
		return _languageManager.GetValue(_id);
	}



	#region Private Functions
	private void OnLanguageChanged()
	{
		UpdateText();
    }

	private void UpdateText()
	{
		if (_languageManager.GetLanguageType() == Language.type.THAI) _text.trueTypeFont = _languageManager.GetFont(1);
		else _text.trueTypeFont = _languageManager.GetFont(0);

        _text.overflowMethod = UILabel.Overflow.ResizeFreely;
        if (_maxWidth == 0) _text.overflowMethod = _originOverflow;

        _text.text = _languageManager.GetValue(_id);
        
        if(_maxWidth != 0)
        {   
            if (_maxWidth <= _text.width)
            {
                _text.overflowMethod = UILabel.Overflow.ClampContent;
                _text.width = (int)_maxWidth;
            }
        }
        
        if (Changed != null) Changed();
    }
	#endregion
}
