﻿using UnityEngine;
using Com.Mod.Io;
using Com.Mod.ThumbRun.Setting.Application;
using System.Collections.Generic;

namespace Com.Mod.Game.LanguageManager
{
	class LanguageManager : MonoBehaviour 
	{
        public event LanguageManagerEvent.LanguageManagerEventHandler Initialized;
        public event LanguageManagerEvent.LanguageManagerEventHandler LanguageChanged;

        [SerializeField]
		private CSVLoader _csvLoader;
		[SerializeField]
		private LanguageData _languageData;
		[SerializeField]
		private List<Font> _fontList;
		private SettingService _settingService;
        private bool _isInitialized = false;

        private static LanguageManager _instance;

        public bool isInitialized
        {
            get { return _isInitialized; }
        }

        public static LanguageManager instance
        {
            get { return _instance; }
        }

		void Awake ()
		{
			if(_instance == null)
			{
				_instance = this;
			
				_csvLoader.Load("Language");
				_languageData.ParseCSV(_csvLoader.GetText());
				//_languageData.ChangeLanguage(Language.type.KOREAN);
				//Debug.Log(_languageData.GetValue("Settings"));

				_settingService = GameObject.FindGameObjectWithTag("Setting").GetComponent<SettingService>();
                _settingService.Prepared += OnSettingServicePrepared;
                if (_settingService.isPrepared) OnSettingServicePrepared();

                GameObject.DontDestroyOnLoad(gameObject);
			}
			else
			{
				Destroy(gameObject);
			}
		}

		void Start () 
		{
		
		}
		
		void Update () 
		{
		
		}

        private void OnDestroy()
        {
            if(_settingService) _settingService.Prepared -= OnSettingServicePrepared;
        }




        private void OnSettingServicePrepared()
        {
            Debug.Log("+++++++ language: " + _settingService.GetLanguageType());
			
			if (_settingService.GetLanguageType() == Language.type.NONE)
			{
				ChangeLanguage(GetSystemLanguage());
			}
            else
			{
				ChangeLanguage(_settingService.GetLanguageType());
			}
            _isInitialized = true;
            if (Initialized != null) Initialized();
			
        }

		private Language.type GetSystemLanguage()
		{
			Debug.Log("++++++ System Language: " + Application.systemLanguage);
			SystemLanguage lang = Application.systemLanguage;
			Language.type result = Language.type.ENGLISH;
            if (lang == SystemLanguage.Korean) result = Language.type.KOREAN;
            else if(lang == SystemLanguage.English) result = Language.type.ENGLISH;
            else if (lang == SystemLanguage.Russian) result = Language.type.RUSSIAN;
            else if (lang == SystemLanguage.German) result = Language.type.GERMAN;
            else if (lang == SystemLanguage.Spanish) result = Language.type.SPANISH;
            else if (lang == SystemLanguage.French) result = Language.type.FRENCH;
            else if (lang == SystemLanguage.Italian) result = Language.type.ITALIAN;
            else if (lang == SystemLanguage.Portuguese) result = Language.type.PORTUGUESE;
            else if (lang == SystemLanguage.Turkish) result = Language.type.TURKISH;
			else if(lang == SystemLanguage.ChineseSimplified) result = Language.type.CHINESE_S;
			else if(lang == SystemLanguage.ChineseTraditional) result = Language.type.CHINESE_T;
            else if (lang == SystemLanguage.Thai) result = Language.type.THAI;
            else if (lang == SystemLanguage.Japanese) result = Language.type.JAPANESE;
            return result;
		}





        public void ChangeLanguage(Language.type language)
		{
            Debug.Log(">>>>>>>>>> Change Language: " + language.ToString());
			Debug.Log(">>>>>>>>>> Setting Service: " + _settingService);

			_settingService = GameObject.FindGameObjectWithTag("Setting").GetComponent<SettingService>();
            _settingService.SetLanguageType(language);
            _languageData.ChangeLanguage(language);
			if(LanguageChanged != null) LanguageChanged();
		}

		public string GetValue(string id)
		{
			return _languageData.GetValue(id);
		}

		public Language.type GetLanguageType()
		{
			return _languageData.GetLanguageType();
		}

		public Font GetFont(int index)
		{
            //Debug.Log("No font requested. font index: " + index);
            
			return _fontList[index];
		}
	}
}