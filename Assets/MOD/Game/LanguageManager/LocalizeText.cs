﻿using UnityEngine;
using Com.Mod.Game.LanguageManager;
using UnityEngine.UI;

public class LocalizeText : MonoBehaviour 
{
	[SerializeField]
	private LanguageManager _languageManager;
	[SerializeField]
	private Text _text;
	[SerializeField]
	private string _id;

	void Awake ()
	{
		if(_languageManager == null) _languageManager = GameObject.FindGameObjectWithTag("LanguageManager").GetComponent<LanguageManager>();
		_languageManager.LanguageChanged += OnLanguageChanged;
	}

	void Start () 
	{
		UpdateText();
	}
	
	void Update () 
	{
	
	}

	void OnDestroy()
	{
		_languageManager.LanguageChanged -= OnLanguageChanged;
	}



	#region Private Functions
	private void OnLanguageChanged()
	{
		UpdateText();
	}

	private void UpdateText()
	{
		_text.text = _languageManager.GetValue(_id);
	}
	#endregion
}
