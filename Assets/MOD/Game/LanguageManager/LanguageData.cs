﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using System;

namespace Com.Mod.Game.LanguageManager
{
	public class LanguageData : MonoBehaviour 
	{
		[SerializeField]
		private List<Language.type> languageTags; // CSV에서의 언어 순서 (좌측부터)
		private Language.type _selectedLanguageType = Language.type.ENGLISH;
		private int _selectedLanguageIndex = 0;
		private List<Dictionary<string, string>> languageDics;

        void Start () 
		{
        }
		
		void Update () 
		{
		
		}



		#region Public Functions
		public void ParseCSV(string csvText)
		{
			languageDics = new List<Dictionary<string, string>>(); // 언어 리스트 초기화

            string[] lineArray = csvText.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None); // 라인 분리
            for (int i = 0; i < lineArray.Length; i++)
			{
				//Debug.Log("Line[" + i + "]: " + lineArray[i]);
			}

			string[] languageIdSequence = lineArray[0].Split(','); // 언어 아이디
			for(int i = 0; i < languageIdSequence.Length; i++)
			{
				//Debug.Log("Language ID [" + i + "]: " + languageIdSequence[i]);
				languageDics.Add(new Dictionary<string, string>()); // 언어별 딕셔너리 초기화
			}

			for(int i = 0; i < lineArray.Length; i++)
			{
				string[] languageSequence = lineArray[i].Split(',');
				for(int j = 0; j < languageSequence.Length; j++)
				{
					if(j != 0)
					{
						//Debug.Log("j:" + j + " / languageDics[" + i + "]: " + languageSequence[0] + " / " + languageSequence[j]);
						languageDics[j-1].Add(languageSequence[0],languageSequence[j].Replace("&#44;", ","));
					}
				}
			}

			//ChangeLanguage(Language.type.ENGLISH);
		}

		public string GetValue(string id)
		{
			//Debug.Log("_selectedLanguageIndex: " + _selectedLanguageIndex);
			return languageDics[_selectedLanguageIndex][id];
		}

		public void ChangeLanguage(Language.type language)
		{
			_selectedLanguageType = language;

			Debug.Log("Language Data - Change Language / languageTags.Count: " + languageTags.Count);
			for(int i = 0; i < languageTags.Count; i++)
			{
				Debug.Log(_selectedLanguageType + " / " + languageTags[i]);
				if(_selectedLanguageType == languageTags[i])
				{
					_selectedLanguageIndex = i;
					//Debug.Log(_selectedLanguageIndex);
				}
			}
		}

		public Language.type GetLanguageType()
		{
			return _selectedLanguageType;
		}
		#endregion
	}
}