﻿namespace Com.Mod.Game.ScrollView
{
    using UnityEngine;
    using System.Collections;
    using DG.Tweening;

    public class ScrollView : MonoBehaviour
    {
        private const float MOVEMENT_TIME = 0.6f;
        private const float END_RETURN_TIME = 0.8f;
        private const float RETURN_TIME = 0.3f;
        private const float CHARACTER_MOVEMENT_TIME = 1.5f;
		public PageFlickController _controller;
		private ScrollGrid _grid;
		private bool _isTouchDown = false;
        private Vector2 _dragVector2D = Vector2.zero;
        private Vector3 _dragVector3D = Vector3.zero;
		private float _prevPosX = 0f;

        void Awake()
        {
			VerifyGrid();
            
			_controller.On_Init += OnInit;
			_controller.On_Prev += OnPrev;
			_controller.On_Next += OnNext;
			_controller.On_StartDrag += OnStartDrag;
			_controller.On_Return += OnReturn;

			UpdateDragPoint2D(_grid.transform.localPosition);
        }

        void Start()
        {
        }

        void Update()
        {
			// Press
			if (Input.GetMouseButtonDown (0)) {
				Debug.Log ("Mouse Button Down");
				if (HasItem (Input.mousePosition)) {
					Debug.Log ("Press");
					_controller.Press ();
					_isTouchDown = true;
				}

			}

			// Move
			if (_isTouchDown) {
				

//			    Vector3 localPos = _grid.transform.localPosition;
//				localPos.x += ((Input.mousePosition.x) - localPos.x) * 0.9f;
//			    _grid.transform.localPosition = localPos;

				float delta = _prevPosX - Input.mousePosition.x;
				_controller.Drag (new Vector2(delta, 0));
				Debug.Log ("delta: " + delta);
				_prevPosX = Input.mousePosition.x;

				Vector3 localPos = _grid.transform.localPosition;
				localPos.x += (delta - localPos.x) * 0.9f;
			    _grid.transform.localPosition = localPos;
			}

			// Release
			if (Input.GetMouseButtonUp (0)) {
				Debug.Log ("Mouse Button Up");
				_controller.Release ();
				_isTouchDown = false;

				if (HasItem (Input.mousePosition)) {
					Debug.Log ("Release");
				}
			}
        }
			
		private bool HasItem(Vector3 mousePosition)
		{
			Ray ray = Camera.main.ScreenPointToRay (mousePosition);
			RaycastHit hit;
			bool hasHit = Physics.Raycast (ray, out hit, 100f);
			Debug.Log ("Raycast: " + hasHit);
			if (hasHit) {
				Debug.DrawLine (ray.origin, hit.point);
				ScrollItem scrollViewItem = hit.collider.gameObject.GetComponent<ScrollItem> ();
				if (scrollViewItem != null) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}

        private void VerifyGrid()
        {
            int gridCount = 0;
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform child = transform.GetChild(i);
                ScrollGrid grid = child.GetComponent<ScrollGrid>();
				if (i == 0 && grid != null)
                {
					_grid = grid;
                    gridCount++;
                }
            }

            if (gridCount == 0) Debug.LogWarning("Scroll View에 Grid가 존재하지 않습니다.");
            else if (gridCount > 1) Debug.LogError("Scroll View에는 1개의 Grid만 포함할 수 있습니다. 현재 " + gridCount + "개의 Grid가 존재합니다.");
        }

        private void OnInit()
        {
			MoveMap(_controller.GetCurrentMapIndex(), 0);
        }

        private void OnPrev(bool isEnd)
        {
            if (!isEnd)
            {
				MoveMap(_controller.GetCurrentMapIndex(), MOVEMENT_TIME);
            }
            else
            {
				_controller.isMoving = true;
                DOTween.Kill("MapMovement_End_Return");
				_grid.transform.DOLocalMoveX(_controller.GetCurrentMapIndex() * -7f, END_RETURN_TIME).SetId("MapMovement_End_Return").SetEase(Ease.OutSine).OnComplete(OnMoved);
            }
        }

        private void OnNext(bool isEnd)
        {
            if (!isEnd)
            {
				MoveMap(_controller.GetCurrentMapIndex(), MOVEMENT_TIME);
            }
            else
            {
				_controller.isMoving = true;
                DOTween.Kill("MapMovement_End_Return");
				_grid.transform.DOLocalMoveX(_controller.GetCurrentMapIndex() * -7f, END_RETURN_TIME).SetId("MapMovement_End_Return").SetEase(Ease.OutSine).OnComplete(OnMoved);
            }
        }

        private void OnStartDrag()
        {
            Debug.Log("On Start Drag Map");
            DOTween.Kill("MapMovement");
        }

        private void OnReturn()
        {
            ReturnMap();
        }

        private void OnPress()
        {
			_controller.Press();
            UpdateDragPoint2D(_grid.transform.localPosition);
        }

        private void OnDrag(Vector2 delta)
        {
			if (_controller.isDragEnd) return;

			_controller.Drag(delta);

			if (!_controller.isMoving)
            {
                _dragVector2D += delta / 2;
                UpdateDragPoint3D(_dragVector2D);
            }
        }

        private void OnRelease()
        {
			_controller.Release();
        }

        private void MoveMap(int index, float time)
        {
			_controller.isMoving = true;
            _grid.transform.DOLocalMoveX(index * -400f, time).SetEase(Ease.OutExpo).OnComplete(OnMoved);
        }

        private void ReturnMap()
        {
			_controller.isMoving = true;
			_grid.transform.DOLocalMoveX(_controller.GetCurrentMapIndex() * -400f, RETURN_TIME).SetEase(Ease.OutExpo).OnComplete(OnReturned);
        }

        private void OnMoved()
        {
            UpdateDragPoint2D(_grid.transform.localPosition);
            UpdateDragPoint3D(_dragVector2D);
			_controller.isMoving = false;

			_controller.OnMapMoved();
        }

        private void OnReturned()
        {
            UpdateDragPoint2D(_grid.transform.localPosition);
            UpdateDragPoint3D(_dragVector2D);
			_controller.isMoving = false;

			_controller.OnMapReturned();
        }

        private void UpdateDragPoint2D(Vector3 pos)
        {
			_dragVector2D = Camera.main.WorldToScreenPoint(pos);
        }

        private void UpdateDragPoint3D(Vector2 pos)
        {
			_dragVector3D = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, 0, 6));
        }
    }
}