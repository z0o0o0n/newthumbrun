﻿namespace Com.Mod.Game.ScrollView
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;

	public class ScrollGrid : MonoBehaviour
	{
	    private List<GameObject> items = new List<GameObject>();

	    void Start()
	    {
	        FindItemInChilds();



	        //Transform transform = gameObject.GetComponentInChildren<Transform>();
	        //Debug.Log("name: " + transform.gameObject.name);

	        //for (int i = 0; i < transform.childCount; i++)
	        //{
	        //    Transform child = transform.GetChild(i);
	        //    Debug.Log("child " + i + " name: " + child.name);
	        //}
	    }

	    void Update()
	    {

	    }

	    private void FindItemInChilds()
	    {
	        for(int i = 0; i < transform.childCount; i++)
	        {
	            Transform child = transform.GetChild(i);
	            items.Add(child.gameObject);
	            //Debug.Log("child " + i + " name: " + child.name);
	        }
	    }
	}
}