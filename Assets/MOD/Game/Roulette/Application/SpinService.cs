﻿namespace Com.Mod.Game.Roulette.Application
{
	using UnityEngine;
	using Com.Mod.Game.Roulette.Domain;

	public class SpinService : MonoBehaviour
	{
		[SerializeField]
		private Roulette _roulette;

        void Start()
        {

        }

        void Update()
        {

        }

        public SpinResult Spin()
		{
			return _roulette.Spin();
		}
	}
}