﻿using UnityEngine;
using Com.Mod.Game.Roulette.Application;
using Com.Mod.Game.Roulette.Domain;
using UnityEngine.UI;
using DG.Tweening;
using Com.Mod.ThumbRun.RouletteGame.Application;
using Com.Mod.ThumbRun.RouletteGame.View;
using Com.Mod.ThumbRun.IAPManager.Domain;
using Com.Mod.ThumbRun.Common.View;

namespace Com.Mod.Game.Roulette.View
{
    public class RouletteMachine : MonoBehaviour 
	{
        public delegate void RouletteMachineEvent();
        public delegate void RouletteMachineSpinHandler(int prizeId);
        public event RouletteMachineSpinHandler SpinStart;
        public event RouletteMachineSpinHandler SpinEnded;
        public event RouletteMachineEvent GameClose;

        [SerializeField]
		private GameObject _board;
        [SerializeField]
        private UIDynamicButton _spinButton;
        [SerializeField]
		private SpinService _spinService;
        //[SerializeField]
        private IAPManager _iapManager;
        [SerializeField]
        private bool _isIap = false;
        [SerializeField]
        private LoadingScreen _loadingScreen;
        [SerializeField]
        private UILabel _iapPriceLabel;

        private void Awake()
        {
            _iapManager = IAPManager.instance;
        }

        void Start () 
		{
            if (_isIap)
            {
                _iapPriceLabel.text = IAPManager.instance.GetProductById("item.roulette_a").metadata.localizedPriceString;
            }
        }

		void Update () 
		{
		
		}





        private void OnPurchaseSuccess(string productId)
        {
            _loadingScreen.gameObject.SetActive(false);
            _iapManager.PurchaseSuccess -= OnPurchaseSuccess;
            _iapManager.PurchaseFail -= OnPurchaseFail;
            StartSpin();
        }

        private void OnPurchaseFail(string productId, string errorId)
        {
            Debug.Log("Roulette Machine - OnPurchaseFail /  errorId: " + errorId);
            _loadingScreen.gameObject.SetActive(false);
            _iapManager.PurchaseSuccess -= OnPurchaseSuccess;
            _iapManager.PurchaseFail -= OnPurchaseFail;
            if (GameClose != null) GameClose();
        }





        public void Spin()
		{
            if(_isIap)
            {
                _loadingScreen.gameObject.SetActive(true);

                _iapManager.PurchaseSuccess += OnPurchaseSuccess;
                _iapManager.PurchaseFail += OnPurchaseFail;
                _iapManager.Buy("item.roulette_a");
            }
            else
            {
                StartSpin();
            }
        }

        private void StartSpin()
        {
            _spinButton.gameObject.SetActive(false);

            SpinResult spinResult = _spinService.Spin();
            float bitAngle = 360f / spinResult.resultCount;
            float angle = bitAngle * spinResult.resultId;
            float targetAngle = (-360f * 3) + angle;
            //Debug.Log("+++++ resultId: " + spinResult.resultId + " / angle: " + angle);
            _board.transform.DOLocalRotate(new Vector3(0f, 0f, targetAngle), 2f, RotateMode.FastBeyond360).OnComplete(() => OnSpinEnded(spinResult.resultId));

            if (SpinStart != null) SpinStart(-1);
        }

		private void OnSpinEnded(int prizeId)
		{
            if (SpinEnded != null) SpinEnded(prizeId);
		}

        public void ResetRoulette()
        {
            _spinButton.gameObject.SetActive(true);
        }
	}
}