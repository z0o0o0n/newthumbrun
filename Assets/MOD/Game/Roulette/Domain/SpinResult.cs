﻿namespace Com.Mod.Game.Roulette.Domain
{
	public class SpinResult 
	{
		private int _resultId;
		private int _resultCount;

		public SpinResult(int resultId, int resultCount)
		{
			_resultId = resultId;
			_resultCount = resultCount;
		}

		public int resultId
		{
			get { return _resultId; }
		}

		public int resultCount
		{
			get {return _resultCount; }
		}

		public bool Equals(SpinResult spinResult)
		{
			if(_resultId != spinResult.resultId) return false;
			if(_resultCount != spinResult.resultCount) return false;
			return true;
		}
	}	
}