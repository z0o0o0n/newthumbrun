﻿namespace Com.Mod.Game.Roulette.Domain
{
	[System.SerializableAttribute]
	public class RouletteItem 
	{
		public int itemId;
		public int probabilityCount;
	}
}