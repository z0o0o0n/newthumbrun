﻿using System.Collections.Generic;
using UnityEngine;

namespace Com.Mod.Game.Roulette.Domain
{
    public class Roulette : MonoBehaviour
	{
		[SerializeField]
		private List<RouletteItem> _rouletteItems;
		private int _probabilityAmount;

        void Awake()
        {
            CalculateProbabilityAmount();
        }

        void Start()
        {
            
        }

        void Update()
        {

        }

		private void CalculateProbabilityAmount()
		{
			_probabilityAmount = 0;
			for(int i = 0; i < _rouletteItems.Count; i++)
			{
				_probabilityAmount += _rouletteItems[i].probabilityCount;
			}
		}

		public SpinResult Spin()
		{
			CheckRouletteItemCount();
			int randomValue = Random.Range (0, _probabilityAmount);
			int resultId = CalculateResultId(randomValue);

			SpinResult spinResult = new SpinResult(resultId, _rouletteItems.Count);
			return spinResult;
		}

		private void CheckRouletteItemCount()
		{
            Debug.Log("Roulette Items Count: " + _rouletteItems.Count);
			if(_rouletteItems.Count < 2) Debug.LogError("Roulette의 resultCount 값은 2보다 작을 수 없습니다.");
			else if(_rouletteItems.Count > 36) Debug.LogError("Roulette의 resultCount 값은 36보다 클 수 없습니다.");
		}

		private int CalculateResultId(int randomValue)
		{
			int resultId = 0;
			int startCount = 0;
			for(int i = 0; i < _rouletteItems.Count; i++)
			{
				int endCount = startCount + _rouletteItems[i].probabilityCount - 1;
				//Debug.Log("startCount: " + startCount + " / random: " + randomValue + " / endCount: " + endCount);
				if(startCount <= randomValue && randomValue < endCount) resultId = i;
				startCount += _rouletteItems[i].probabilityCount;
			}
			return resultId;
		}
	}
}