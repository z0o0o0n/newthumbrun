﻿namespace Com.Mod.Util
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;

	public class SpeedChecker : MonoBehaviour
	{
	    public bool useAverageSpeed = false;
	    public int averageBufferNum = 5;

		private float _speed = 0;
	    private float _speedX = 0;
	    private float _speedY = 0;

	    void Start()
	    {

	    }

		public float GetSpeed()
		{
			return _speed;
		}

	    public float GetSpeedX()
	    {
	        return _speedX;
	    }

	    public float GetSpeedY()
	    {
	        return _speedY;
	    }

	    public void UpdatePos(Vector3 pos)
	    {
	        if (useAverageSpeed) CheckAverageSpeed(pos);
	        else CheckSpeed(pos);
	    }

		private ArrayList _speedHistoryArray = new ArrayList();
	    private ArrayList _speedXHistoryArray = new ArrayList();
	    private ArrayList _speedYHistoryArray = new ArrayList();
		private Vector3 _oldPosition;
		private void CheckAverageSpeed(Vector3 position)
		{
			if(_oldPosition == Vector3.zero) _oldPosition = position;
			
	        float speed = (position - _oldPosition).magnitude;
	        float speedX = (position - _oldPosition).x;
	        float speedY = (position - _oldPosition).y;
			
			_speedHistoryArray.Add(speed);
	        _speedXHistoryArray.Add(speedX);
	        _speedYHistoryArray.Add(speedY);

	        if (_speedHistoryArray.Count > averageBufferNum) _speedHistoryArray.RemoveAt(0);
	        if (_speedXHistoryArray.Count > averageBufferNum) _speedXHistoryArray.RemoveAt(0);
	        if (_speedYHistoryArray.Count > averageBufferNum) _speedYHistoryArray.RemoveAt(0);
			
			float averageSpeed = 0.0f;
	        float averageSpeedX = 0.0f;
	        float averageSpeedY = 0.0f;
	        for (int i = 0; i < _speedHistoryArray.Count; i++)
			{
				averageSpeed += (float)_speedHistoryArray[i];
	            averageSpeedX += (float)_speedXHistoryArray[i];
	            averageSpeedY += (float)_speedYHistoryArray[i];
			}

	        //_speed = averageSpeed / _speedHistoryArray.Count;
	        //_speedX = averageSpeedX / _speedHistoryArray.Count;
	        //_speedY = averageSpeedY / _speedHistoryArray.Count;
	        _speed = averageSpeed;
	        _speedX = averageSpeedX;
	        _speedY = averageSpeedY;

	        _oldPosition = position;
		}
		
		private void CheckSpeed(Vector3 position)
		{
			if(_oldPosition == Vector3.zero) _oldPosition = position;
	        _speed = (position - _oldPosition).magnitude;
	        _speedX = (position - _oldPosition).x;
	        _speedY = (position - _oldPosition).y;
			_oldPosition = position;
		}

	    public void Reset()
	    {
	        _speed = 0;
	        _speedX = 0;
	        _speedY = 0;
	        _oldPosition = Vector3.zero;
	        _speedHistoryArray.Clear();
	        _speedXHistoryArray.Clear();
	        _speedYHistoryArray.Clear();
	    }
	}
}