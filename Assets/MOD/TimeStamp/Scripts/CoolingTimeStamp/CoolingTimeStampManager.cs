﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Com.Mod.ThumbRun.Analytics.Domain;
using Com.Mod.ThumbRun.Analytics.Application;

namespace Com.Mod.TimeStamp
{
    public class CoolingTimeStampManager : MonoBehaviour
    {
        /**
         * Time Cheat Prevention Plugin 필요.
         * https://www.assetstore.unity3d.com/en/#!/content/18984
         * 
         * - 동적 추가 기능 없음
         * - 주의 > 게임 도메인에서 앱 삭제 후 재 설치시 ResetAllTimestamp()를 사용해 타임스탬프 기록도 모두 삭제되도록 처리해야 함.
         */

        public delegate void CoolingTimeStampEvent(CoolingTimeStamp coolingTimeStamp);
        public event CoolingTimeStampEvent CoolingStarted;
        public event CoolingTimeStampEvent CoolingEnded;

        private static CoolingTimeStampManager _instance;

        [SerializeField]
        private List<CoolingTimeStamp> _coolingTimeStampList;
        private string _logPrefix = "------------->> ";
        private bool _isDebug = true;

        public static CoolingTimeStampManager instance
        {
            get { return _instance; }
        }

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void Start()
        {
            Init();
        }

        private void Init()
        {
            for (int i = 0; i < _coolingTimeStampList.Count; i++)
            {
                string key = _coolingTimeStampList[i].key;
                DateTime defaultTime = UnbiasedTime.Instance.Now(); // 메모리에 타임스템프 값이 없는경우 사용 될 기본 값
                DateTime coolingEndTime = ReadTimestamp(key, defaultTime);

                _coolingTimeStampList[i].SetCoolingEndTime(coolingEndTime);

                if (_isDebug) Debug.Log(_logPrefix + "CoolingTimeStampManager - Init / key: " + _coolingTimeStampList[i].key + " / time: " + _coolingTimeStampList[i].GetCoolingEndTime());
            }
        }

        void Update()
        {
            for (int i = 0; i < _coolingTimeStampList.Count; i++)
            {
                CoolingTimeStamp coolingTimeStamp = _coolingTimeStampList[i];
                TimeSpan remainingTime = GetRemainingTime(coolingTimeStamp.key);
                coolingTimeStamp.SetRemainingSec((int)remainingTime.TotalSeconds);

                if (remainingTime.TotalSeconds <= 0)
                {
                    // Time End
                    _coolingTimeStampList[i].isCooling = false;
                    if (CoolingEnded != null) CoolingEnded(coolingTimeStamp);
                }
                else
                {
                    _coolingTimeStampList[i].isCooling = true;
                }
            }
        }




        private DateTime ReadTimestamp(string key, DateTime defaultValue)
        {
            long time = Convert.ToInt64(PlayerPrefs.GetString(key, "0"));

            if (time == 0) // 메모리에 해당 key의 값이 없는 경우
            {
                if (_isDebug) Debug.Log(_logPrefix + "CoolingTimeStampManager - ReadTimestamp / 메모리에 key에 해당하는 값이 없음. / key: " + key);
                return defaultValue;
            }

            if (_isDebug) Debug.Log(_logPrefix + "CoolingTimeStampManager - ReadTimestamp / key: " + key + " / data: " + DateTime.FromBinary(time));
            return DateTime.FromBinary(time);
        }

        private void WriteTimestamp(string key, DateTime time)
        {
            if (_isDebug) Debug.Log(_logPrefix + "CoolingTimeStampManager - WriteTimestamp / key: " + key + " / time: " + time);

            PlayerPrefs.SetString(key, time.ToBinary().ToString());
            PlayerPrefs.Save();
        }

        // Cooling Time Stamp를 초기화하고 메모리에 저장 된 coolingEndTime을 현재시간으로 초기화함.
        private void Reset(string key)
        {
            CoolingTimeStamp coolingTimeStamp = GetCoolingTimeStamp(key);
            if (coolingTimeStamp == null) return;

            coolingTimeStamp.isCooling = false;
            //coolingTimeStamp.SetRemainingTime();
            WriteTimestamp(coolingTimeStamp.key, UnbiasedTime.Instance.Now());
        }





        private void OnApplicationPause(bool paused)
        {
            if (paused)
            {
                for (int i = 0; i < _coolingTimeStampList.Count; i++)
                {
                    //if (_isDebug) Debug.Log(_logPrefix + "TimeStampManager - OnApplicationPause: ture / key: " + _coolingTimeStampList[i].key + " / i: " + i + " / time: " + _coolingTimeStampList[i].GetCurrentTime());

                    string key = _coolingTimeStampList[i].key;
                    WriteTimestamp(_coolingTimeStampList[i].key, _coolingTimeStampList[i].GetCoolingEndTime());
                }
            }
            else
            {
                for (int i = 0; i < _coolingTimeStampList.Count; i++)
                {
                    DateTime currentTime = ReadTimestamp(_coolingTimeStampList[i].key, UnbiasedTime.Instance.Now().AddSeconds(_coolingTimeStampList[i].coolingTime));
                    _coolingTimeStampList[i].SetCoolingEndTime(currentTime);
                }
            }
        }

        private void OnApplicationQuit()
        {
            for (int i = 0; i < _coolingTimeStampList.Count; i++)
            {
                WriteTimestamp(_coolingTimeStampList[i].key, _coolingTimeStampList[i].GetCoolingEndTime());
            }
        }










        
        public void Use(string key)
        {
            CoolingTimeStamp coolingTimeStamp = GetCoolingTimeStamp(key);
            if (coolingTimeStamp == null) return;
            if (coolingTimeStamp.isCooling) return;

            coolingTimeStamp.isCooling = true;
            DateTime coolingEndTime = UnbiasedTime.Instance.Now().AddSeconds(coolingTimeStamp.coolingTime);
            coolingTimeStamp.SetCoolingEndTime(coolingEndTime);
            WriteTimestamp(key, coolingEndTime);

            if (CoolingStarted != null) CoolingStarted(coolingTimeStamp);
        }

        public TimeSpan GetRemainingTime(string key)
        {
            TimeSpan remainingTime = TimeSpan.FromSeconds(0); // 리턴 초기값이 Null일 수 없어 0.
            for (int i = 0; i < _coolingTimeStampList.Count; i++)
            {
                if (key == _coolingTimeStampList[i].key)
                {
                    remainingTime = _coolingTimeStampList[i].GetCoolingEndTime() - UnbiasedTime.Instance.Now();
                }
            }
            return remainingTime;
        }

        public CoolingTimeStamp GetCoolingTimeStamp(string key)
        {
            for (int i = 0; i < _coolingTimeStampList.Count; i++)
            {
                if (_coolingTimeStampList[i].key == key) return _coolingTimeStampList[i];
            }
            return null;
        }

        public void ResetAllCoolingTimestamp()
        {
            for (int i = 0; i < _coolingTimeStampList.Count; i++)
            {
                string key = _coolingTimeStampList[i].key;
                Reset(key);
            }
        }
    }
}