﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CoolingTimeStamp
{
    public string key;
    public int coolingTime; // 사용 시 쿨타임 시간 설정 값
    public bool isCooling = false;

    private DateTime _coolingEndTime;
    private int _remainingSec;

    public CoolingTimeStamp()
    {
    }




    public void SetCoolingEndTime(DateTime time)
    {
        _coolingEndTime = time;
    }

    public DateTime GetCoolingEndTime()
    {
        return _coolingEndTime;
    }

    public void SetRemainingSec(int sec)
    {
        _remainingSec = sec;
    }

    public int GetRemainingSec()
    {
        return _remainingSec;
    }
}
