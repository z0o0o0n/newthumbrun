﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Com.Mod.TimeStamp
{
    public class TimeStampTestUI : MonoBehaviour
    {
        public delegate void TimeStampTestUIEvent(string key);
        public event TimeStampTestUIEvent Reset;

        public Text keyText;
        public Text remainingTimeText;

        void Start()
        {

        }

        void Update()
        {

        }



        #region Public Functions
        public void OnResetButtonPressed()
        {
            if (Reset != null) Reset(keyText.text);
        }
        #endregion
    }
}
