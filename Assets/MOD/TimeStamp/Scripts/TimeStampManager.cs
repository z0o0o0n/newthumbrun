﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Com.Mod.ThumbRun.Analytics.Domain;
using Com.Mod.ThumbRun.Analytics.Application;

namespace Com.Mod.TimeStamp
{
    public class TimeStampManager : MonoBehaviour
    {
        /**
         * Time Cheat Prevention Plugin 필요.
         * https://www.assetstore.unity3d.com/en/#!/content/18984
         */

        public event TimeStampEvent.TimeStampEventHandler Initialized;
        public event TimeStampEvent.TimeStampEndEventHandler Timeout;

        private static TimeStampManager _instance;

        [SerializeField]
        private List<TimeStamp> timeStampList;
        private string _logPrefix = "------------->> ";
        private bool _isDebug = false;
        private AnalyticsService _analyticsService;

        public static TimeStampManager instance
        {
            get { return _instance; }
        }

        private void Awake()
        {
            _analyticsService = GameObject.FindGameObjectWithTag("Analytics").GetComponent<AnalyticsService>();

            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void Start()
        {
            // 동적추가를 위해 Manager는 수동으로 Init을 해야 함.
            //Add("test", 400);
            Init();
        }

        void Update()
        {
            for (int i = 0; i < timeStampList.Count; i++)
            {
                string key = timeStampList[i].key;
                TimeSpan remainingTime = GetRemainingTime(timeStampList[i].key);
                if (remainingTime.TotalSeconds > 0)
                {
                    //if (key == "Special_2_Display" || key == "Special_2_Cool") if (_isDebug) Debug.Log(_logPrefix + "TimeStampManager - Update / remainingTime: " + remainingTime);
                    //unbiasedFormatted = string.Format("{0}:{1:D2}:{2:D2}", remainingTime.Hours, remainingTime.Minutes, remainingTime.Seconds);
                }
                else // Time End
                {
                    if(!timeStampList[i].isEnd)
                    {
                        //if (key == "Special_2_Display" || key == "Special_2_Cool") if (_isDebug) Debug.Log(_logPrefix + "TimeStampManager - Update / isEnd: true / key: " + timeStampList[i].key + " / time: " + timeStampList[i].time);
                        timeStampList[i].isEnd = true;
                        if (Timeout != null) Timeout(timeStampList[i].key);
                    }
                }
            }
        }




        private DateTime ReadTimestamp(string key, DateTime defaultValue)
        {
            long tmp = Convert.ToInt64(PlayerPrefs.GetString(key, "0"));
            if (tmp == 0)
            {
                if (key == "Special_2_Display" || key == "Special_2_Cool") if (_isDebug) Debug.Log(_logPrefix + "TimeStampManager - ReadTimestamp / isDefault: true / key: " + key);
                return defaultValue;
            }
            if (key == "Special_2_Display" || key == "Special_2_Cool") if (_isDebug) Debug.Log(_logPrefix + "TimeStampManager - ReadTimestamp / isDefault: false / key: " + key + " / data: " + DateTime.FromBinary(tmp));
            return DateTime.FromBinary(tmp);
        }

        private void WriteTimestamp(string key, DateTime time)
        {
            if (key == "Special_2_Display" || key == "Special_2_Cool") if (_isDebug) Debug.Log(_logPrefix + "TimeStampManager - WriteTimestamp / key: " + key + " / time: " + time);
            PlayerPrefs.SetString(key, time.ToBinary().ToString());
            PlayerPrefs.Save();
        }

        private void ResetAllTimestamp()
        {
            for (int i = 0; i < timeStampList.Count; i++)
            {
                string key = timeStampList[i].key;
                Reset(key, timeStampList[i].defaultEnd);
            }
        }





        private void OnApplicationPause(bool paused)
        {
            Debug.Log("??????????? paused: " + paused);
            if (paused)
            {
                for (int i = 0; i < timeStampList.Count; i++)
                {
                    string key = timeStampList[i].key;
                    if (key == "Special_2_Display" || key == "Special_2_Cool") if (_isDebug) Debug.Log(_logPrefix + "TimeStampManager - OnApplicationPause: ture / key: " + timeStampList[i].key + " / i: " + i + " / time: " + timeStampList[i].time);
                    WriteTimestamp(timeStampList[i].key, timeStampList[i].time);
                }
            }
            else
            {
                for (int i = 0; i < timeStampList.Count; i++)
                {
                    timeStampList[i].time = ReadTimestamp(timeStampList[i].key, UnbiasedTime.Instance.Now().AddSeconds(timeStampList[i].sec));
                }
            }
        }

        private void OnApplicationQuit()
        {
            for (int i = 0; i < timeStampList.Count; i++)
            {
                WriteTimestamp(timeStampList[i].key, timeStampList[i].time);
            }
        }






        private void Init()
        {
            //Debug.Log("%%%%%%%%%%%%%%%%%%%%%%%% execute count: " + _analyticsService.GetExecuteCount());
            if (_analyticsService.GetExecuteCount() <= 1)
            {
                //    Debug.Log("%%%%%%%%%%%%%%%%%%%%%%%% Reset All Timestamp");
                ResetAllTimestamp();
            }

            for (int i = 0; i < timeStampList.Count; i++)
            {
                string key = timeStampList[i].key;
                timeStampList[i].time = ReadTimestamp(timeStampList[i].key, UnbiasedTime.Instance.Now().AddSeconds(timeStampList[i].sec));
                if (key == "Special_2_Display" || key == "Special_2_Cool") if (_isDebug) Debug.Log(_logPrefix + "TimeStampManager - Init / key: " + timeStampList[i].key + " / time: " + timeStampList[i].time);
            }

            if(Initialized != null) Initialized();
        }

        //public void Add(string key, int sec)
        //{
        //    timeStampList.Add(new TimeStamp(key, sec));
        


        public void Reset(string key, bool isEnd)
        {
            for (int i = 0; i < timeStampList.Count; i++)
            {
                if (key == timeStampList[i].key)
                {
                    if(isEnd)
                    {
                        timeStampList[i].isEnd = true;
                        timeStampList[i].time = UnbiasedTime.Instance.Now();
                        WriteTimestamp(timeStampList[i].key, timeStampList[i].time);
                    }
                    else
                    {
                        timeStampList[i].isEnd = false;
                        timeStampList[i].time = UnbiasedTime.Instance.Now().AddSeconds(timeStampList[i].sec);
                        WriteTimestamp(timeStampList[i].key, timeStampList[i].time);
                    }
                }
            }
        }

        public TimeSpan GetRemainingTime(string key)
        {
            TimeSpan remainingTime = TimeSpan.FromSeconds(0); // 리턴 초기값이 Null일 수 없어 0.
            for(int i = 0; i < timeStampList.Count; i++)
            {
                if (key == timeStampList[i].key)
                {
                    remainingTime = timeStampList[i].time - UnbiasedTime.Instance.Now();
                }
            }
            return remainingTime;
        }

        public List<TimeStamp> GetTimeStampList()
        {
            return timeStampList;
        }

        public TimeStamp GetTimeStamp(string key)
        {
            for(int i = 0; i < timeStampList.Count; i++)
            {
                if (timeStampList[i].key == key) return timeStampList[i];
            }
            return null;
        }
    }





    [System.Serializable]
    public class TimeStamp
    {
        public string key;
        public int sec;
        public DateTime time;
        public bool defaultEnd;
        private bool _isEnd = false;

        public TimeStamp(string key, int sec)
        {
            this.key = key;
            this.sec = sec;
        }

        public bool isEnd
        {
            get{
                _isEnd = (PlayerPrefs.GetInt(key + "_isEnd", 1) == 1) ? true : false;
                return _isEnd;
            }
            set {
                _isEnd = value;
                PlayerPrefs.SetInt(key + "_isEnd", (_isEnd == true) ? 1 : 0);
                PlayerPrefs.Save();
            }
        }
    }
}

