﻿using UnityEngine;
using System.Collections;

namespace Com.Mod.TimeStamp
{
    public class TimeStampEvent
    {
        public delegate void TimeStampEventHandler();
        public delegate void TimeStampEndEventHandler(string key);
    }
}