﻿using UnityEngine;
using System.Collections;
using Com.Mod.TimeStamp;
using System.Collections.Generic;
using System;

namespace Com.Mod.TimeStamp
{
    public class TimeStampTestPanel : MonoBehaviour
    {
        [SerializeField]
        private TimeStampManager _timeStampManager;
        [SerializeField]
        private GameObject _timeStampUIPrefab;
        private bool _isInitialized = false;
        private List<TimeStampTestUI> _timeStampTestUIList;

        void Awake()
        {
            _timeStampTestUIList = new List<TimeStampTestUI>();
            _timeStampManager.Initialized += OnManagerInitialized;
            _timeStampManager.Timeout += OnTimeout;
        }
        
        void Start()
        {

        }

        void Update()
        {
            if(_isInitialized)
            {
                for(int i = 0; i < _timeStampTestUIList.Count; i++)
                {
                    TimeSpan remainingTime = _timeStampManager.GetRemainingTime(_timeStampTestUIList[i].keyText.text);
                    string unbiasedFormatted = "END";
                    if (remainingTime.TotalSeconds > 0)
                    {
                        unbiasedFormatted = string.Format("{0}:{1:D2}:{2:D2}", remainingTime.Hours, remainingTime.Minutes, remainingTime.Seconds);
                    }
                    _timeStampTestUIList[i].remainingTimeText.text = unbiasedFormatted;
                }
            }
        }

        void OnDestroy()
        {
            _timeStampManager.Initialized -= OnManagerInitialized;
            _timeStampManager.Timeout -= OnTimeout;
            for (int i = 0; i < _timeStampManager.GetTimeStampList().Count; i++)
            {
                _timeStampTestUIList[i].Reset -= OnReset;
            }
        }



        #region Private Functions
        private void OnManagerInitialized()
        {
            CreateTimeStampUI();
            _isInitialized = true;
        }

        private void CreateTimeStampUI()
        {
            List<TimeStamp> timeStampList = _timeStampManager.GetTimeStampList();

            for(int i = 0; i < timeStampList.Count; i++)
            {
                GameObject instance = GameObject.Instantiate(_timeStampUIPrefab);
                instance.transform.parent = transform;
                instance.transform.localPosition = new Vector3(0f, -i * 80f, 0f);

                TimeStampTestUI ui = instance.GetComponent<TimeStampTestUI>();
                ui.keyText.text = timeStampList[i].key;
                ui.Reset += OnReset;

                _timeStampTestUIList.Add(ui);
            }
        }

        private void OnReset(string key)
        {
            _timeStampManager.Reset(key, true);
        }

        private void OnTimeout(string key)
        {
            Debug.Log("Timeout: " + key);
        }
        #endregion
    }
}