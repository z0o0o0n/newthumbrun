﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ScrollViewDockingOption : MonoBehaviour 
{
	private UIScrollView _uiScrollView;

	void Awake ()
	{
		_uiScrollView = gameObject.GetComponent<UIScrollView> ();
		_uiScrollView.onDragStarted += OnDragStarted;
		_uiScrollView.onDragFinished += OnDragFinished;
		_uiScrollView.onStoppedMoving += OnStoppedMoving;
	}

	void Start () 
	{
	
	}

	void Update () 
	{
	
	}

	void OnDestroy()
	{
		_uiScrollView.onDragStarted -= OnDragStarted;
		_uiScrollView.onDragFinished -= OnDragFinished;
		_uiScrollView.onStoppedMoving -= OnStoppedMoving;
	}



	private void OnDragStarted()
	{
		DOTween.Kill ("ReturnTween");
	}

	private void OnDragFinished()
	{
		
	}

	private void OnStoppedMoving()
	{
		if (transform.localPosition.x > 0 || transform.localPosition.x < -1600) return;

		DOTween.Kill ("ReturnTween");
		int index = (int) Mathf.Round (transform.localPosition.x / 400);
		Debug.Log ("index: " + index);
		transform.DOLocalMoveX (0f, 0.5f).SetId("ReturnTween");
	}
}
