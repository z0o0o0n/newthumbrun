﻿namespace Com.Mod.NGUI.Effect
{
    using DG.Tweening;
    using UnityEngine;

    public class Filcker : MonoBehaviour 
	{
		[SerializeField]
		private UIWidget _widget; 
		[SerializeField]
		private float _speed = 0.2f;
		[SerializeField]
		private float _startAlpha = 1f;
		[SerializeField]
		private float _endAlpha = 0f;

		void Start () 
		{
			_widget.alpha = _startAlpha;
			Play();
		}
		
		void Update () 
		{
			
		}




		public void Play()
		{
			DOTween.To(()=>_widget.alpha, x=>_widget.alpha=x, _endAlpha, _speed).SetLoops(-1, LoopType.Yoyo);
		}

		public void Stop()
		{
			DOTween.To(()=>_widget.alpha, x=>_widget.alpha=x, _startAlpha, 0.1f);
		}
	}
}