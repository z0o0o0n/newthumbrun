﻿namespace Com.Mod.NGUI.LogConsole
{
    using UnityEngine;
    using System.Collections;

    public class LogConsole : MonoBehaviour
    {
        public delegate void LogConsoleEventHandler(string log);
        public event LogConsoleEventHandler Updated;
        
        private static LogConsole _instance;
        private string _logData;

        public static LogConsole instance
        {
            get { return _instance; }
        }

        void Awake()
        {
            if(_instance == null)
            {
                _instance = gameObject.GetComponent<LogConsole>();
                DontDestroyOnLoad(gameObject);
                return;
            }

            Destroy(gameObject);
        }

        void Start()
        {

        }

        void Update()
        {

        }



        public void Log(string value)
        {
            _logData += value + "\n";
            if (Updated != null) Updated(_logData);
        }
    }
}