﻿namespace Com.Mod.NGUI.LogConsole
{
    using UnityEngine;
    using System.Collections;

    public class LogConsoleDisplay : MonoBehaviour
    {
        [SerializeField]
        private UILabel _label;

        void Awake()
        {
            LogConsole.instance.Updated += OnLogUpdated;
        }

        void Start ()
        {
	
	    }
	
	    void Update ()
        {
	
	    }

        void OnDestroy()
        {
            LogConsole.instance.Updated -= OnLogUpdated;
        }



        private void OnLogUpdated(string value)
        {
            _label.text = value;
        }
    }
}