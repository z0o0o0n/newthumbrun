﻿namespace popupManager
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;

    /**
    * 개발담당 : 김준희
    * 개발일자 : 2015-07-21
    * 수정일자 : -
    * 수정내용 : -
    **/

    public class PopupManager : MonoBehaviour 
    {
        public delegate void PopupManagerEvent();
        public delegate void PopupManagerOpenEvent(BasePopup popup);
        public event PopupManagerOpenEvent Opened;
        public event PopupManagerEvent On_End;

        private List<BasePopup> _cueue = new List<BasePopup>();
        private BasePopup _currentPopup;

	    void Start () 
        {
	
	    }

        void OnDestroy()
        {
            for (int i = 0; i < _cueue.Count; i++)
            {
                if(_cueue[i]) Destroy(_cueue[i].gameObject); // 대기열의 GameObject 삭제
            }
            _cueue.Clear(); // 대기열 초기화

            if (_currentPopup)
            {
                _currentPopup.On_CloseEnd -= OnCloseEnd; // currentPopup 초기화
                _currentPopup = null;
            }
        }


        // Push Popup To Cueue
        // 팝업을 대기열에 추가함
        public void PushPopupToCueue(BasePopup popup)
        {
            _cueue.Add(popup);
        }


        // Open
        public void Open()
        {
            Debug.Log("popupmanager open");
            _currentPopup = _cueue[0];
            _currentPopup.On_CloseEnd += OnCloseEnd;
            _currentPopup.Open(0.3f);

            if (Opened != null) Opened(_currentPopup);
        }

        private void OnCloseEnd()
        {
            _currentPopup.On_CloseEnd -= OnCloseEnd;
            _cueue.RemoveAt(0); // 첫번째 팝업은 제거함
            
            //Destroy(_currentPopup.gameObject); // currentPopup 삭제
            
            if (_cueue.Count > 0) Open(); // 대기열에 Popup이 있다면 Open 함
            else if (On_End != null) On_End(); // 대기열에 Popup이 없다면 종료
        }
    }
}
