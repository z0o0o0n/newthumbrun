﻿using System;
public interface IPopup
{
    void Open(float time = 0);
    void Close(float time = 0);
}

[Serializable]
public class IPopupContainer : IUnifiedContainer<IPopup> { }