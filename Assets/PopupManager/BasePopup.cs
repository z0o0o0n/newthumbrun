﻿namespace popupManager
{
    using UnityEngine;
    using System.Collections;
    using Junhee.Utils;
    using DG.Tweening;

    public class BasePopup : MonoBehaviour, IPopup
    {
        public string name;
        public delegate void PopupEvent();
        public event PopupEvent On_OpenEnd;
        public event PopupEvent On_CloseEnd;



        // Open
        public virtual void Open(float time)
        {
        }



        // End Open
        public void EndOpen()
        {
            if (On_OpenEnd != null) On_OpenEnd();
        }



        // Close
        public virtual void Close(float time)
        {
        }



        // End Close
        public void EndClose()
        {
            if (On_CloseEnd != null) On_CloseEnd();
        }
    }
}