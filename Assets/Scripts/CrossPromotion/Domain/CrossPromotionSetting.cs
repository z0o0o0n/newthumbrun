﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossPromotionSetting : MonoBehaviour
{
    private static CrossPromotionSetting _instance;
    public static CrossPromotionSetting instance
    {
        get { return _instance; }
    }

    private bool _use = true;
    private int _rate = 100;

    public bool use
    {
        get { return _use; }
    }

    public int rate
    {
        get { return _rate; }
    }





    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }




    public void SetValue(bool use, int rate)
    {
        _use = use;
        _rate = rate;
    }
}
