﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossPromotionBannerData : MonoBehaviour 
{
	public string code; //프로모션 고유 코드
	public string packageName; // App 패키지 이름
	public Texture bannerTexture;
}
