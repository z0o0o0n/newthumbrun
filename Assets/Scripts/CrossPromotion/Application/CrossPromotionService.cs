﻿namespace Com.Mod.ThumbRun.CrossPromotion.Application
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Domain;
    using Com.Mod.Game.LanguageManager;
    using LitJson;
    using Com.Mod.ThumbRun.Tutorial.Application;
    using Infrastructure;
    using SignUp.Application;
    using Analytics.Application;

    public class CrossPromotionService : MonoBehaviour 
	{
		public delegate void CrossPromotionServiceEventHandler(Texture2D texture, int width, int height, string url);
		public event CrossPromotionServiceEventHandler Updated;

		[SerializeField]
		private CrossPromotion _crossPromotion;
        [SerializeField]
        private CrossPromotionServerManager _crossPromotionServerManager;
		[SerializeField]
		private TutorialService _tutorialService;
        [SerializeField]
        private SignUpService _signUpService;
        [SerializeField]
        private AnalyticsService _analyticsService;
        private LanguageManager _languageManager;
		private WWWHelper _imageLoader;
		private JsonData _json;
        private List<bool> _adRates;

		void Awake ()
		{
			_languageManager = GameObject.FindGameObjectWithTag("LanguageManager").GetComponent<LanguageManager>();
            _analyticsService = GameObject.FindGameObjectWithTag("Analytics").GetComponent<AnalyticsService>();

            _crossPromotionServerManager.PromotionDataLoadCompleted += OnCrossPromotionUpdated;
			_tutorialService.Prepared += OnPrepared;
            _signUpService.Prepared += OnPrepared;

            CreateAdRate();
        }

		void Start () 
		{
		}
		
		void Update () 
		{
		}

		void OnDestory()
		{
            _crossPromotionServerManager.PromotionDataLoadCompleted -= OnCrossPromotionUpdated;
			_tutorialService.Prepared -= OnPrepared;
            _signUpService.Prepared -= OnPrepared;
        }




		private void OnPrepared()
		{
            //Debug.Log("CrossPromotionService - OnPrepared / IsBasicTutorialEnd: " + _tutorialService.IsBasicTutorialEnd() + " / isCrossBannerEnded: " + GameManager.isCrossBannerEnded);
            if (!_signUpService.isPrepared) return;
            if (!_tutorialService.isPrepared) return;

            // 기초 튜토리얼이 진행중인 경우 패스
            if (!_tutorialService.IsBasicTutorialEnd()) return;
            if (!_tutorialService.IsConditionTutorialEnd()) return;
            if (!_signUpService.IsCompleted()) return;

            // 3회 미만 실행시에는 패스
            if (_analyticsService.GetExecuteCount() <= 3) return;

            // 서버에서 전달받은 노출 확률 확인
            if (!IsAllowShow()) return;

            // 게임 실행 후 한번 노출된 경우 패스
            if (GameManager.isCrossBannerEnded) return;

            RequestCrossPromotion();
		}

		private string GetLanguageISO(Language.type languageType)
		{
			string languageISO;
			if(languageType == Language.type.CHINESE_S) languageISO = "ZH";
			if(languageType == Language.type.CHINESE_T) languageISO = "ZH";
			if(languageType == Language.type.ENGLISH) languageISO = "EN";
			if(languageType == Language.type.FRENCH) languageISO = "FR";
			if(languageType == Language.type.GERMAN) languageISO = "DE";
			if(languageType == Language.type.ITALIAN) languageISO = "IT";
			if(languageType == Language.type.JAPANESE) languageISO = "JA";
			if(languageType == Language.type.KOREAN) languageISO = "KR";
			if(languageType == Language.type.PORTUGUESE) languageISO = "PT";
			if(languageType == Language.type.RUSSIAN) languageISO = "RU";
			if(languageType == Language.type.SPANISH) languageISO = "ES";
			if(languageType == Language.type.TURKISH) languageISO = "TR";
			else languageISO = "EN";
			return languageISO;
		}

		private void OnCrossPromotionUpdated(string wwwText)
		{
			_json = JsonMapper.ToObject(wwwText);
			// Debug.Log(".........>>>>>>>> : " + _json["url"]);

			StartCoroutine(LoadImage());
		}

		private IEnumerator LoadImage()
		{
			string imageUrl = _json["image"].ToString();
			WWW www = new WWW(imageUrl);
			yield return www;

			// Renderer renderer = GetComponent<Renderer>();
			// renderer.material.mainTexture = www.texture;
			GameManager.isCrossBannerEnded = true;
			int width = int.Parse(_json["image_width"].ToString());
			int height = int.Parse(_json["image_height"].ToString());
			if(Updated != null) Updated(www.texture, width, height, _json["url"].ToString());
		}




        private void CreateAdRate()
        {
            _adRates = new List<bool>();
            for (int i = 0; i < 100; i++)
            {
                if (i < CrossPromotionSetting.instance.rate) _adRates.Add(true);
                else _adRates.Add(false);
            }
        }

        private void RequestCrossPromotion()
		{
            _crossPromotionServerManager.RequestCrossPromotion(GetLanguageISO(_languageManager.GetLanguageType()));
		}

        private bool IsAllowShow()
        {
            bool result = false;

            // 서버에서 '사용안함'으로 설정 된 경우
            if (!CrossPromotionSetting.instance.use) return false;

            // 확률에 따른 결정
            int randomIndex = Random.Range(0, _adRates.Count);
            result = _adRates[randomIndex];
            return result;
        }
    }
}