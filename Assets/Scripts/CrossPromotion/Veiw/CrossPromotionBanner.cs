﻿namespace Com.Mod.ThumbRun.CrossPromotion.View
{
    using System.Collections;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.CrossPromotion.Application;
    using UnityEngine;

    public class CrossPromotionBanner : MonoBehaviour 
	{
		[SerializeField]
		private CrossPromotionService _crossPromotionService;
		[SerializeField]
		private UITexture _image;
		[SerializeField]
		private UIBasicButton _closeButton;
		private string _bannerLinkUrl;

		void Awake ()
		{
			transform.localPosition = Vector2.zero;
			Close();
			_crossPromotionService.Updated += OnCrossPromotionUpdated;
		}

		void Start () 
		{
			
		}
		
		void Update () 
		{
			
		}

		void OnDestroy()
		{
			_crossPromotionService.Updated -= OnCrossPromotionUpdated;
		}




		private void OnCrossPromotionUpdated(Texture2D texture, int width, int height, string url)
		{
			Open();
			_image.mainTexture = texture;
			_image.width = width;
			_image.height = height;
			_closeButton.transform.localPosition = new Vector2(_image.width/2 + 30f, _image.height/2 - (45/2));
			_bannerLinkUrl = url;
		}

		public void OnBannerClick()
		{
			Application.OpenURL(_bannerLinkUrl);
		}

		public void OnCloseButtonClick()
		{
			Close();
		}

		


		public void Close()
		{
			gameObject.SetActive(false);
		}

		public void Open()
		{
			gameObject.SetActive(true);
		}
	}
}