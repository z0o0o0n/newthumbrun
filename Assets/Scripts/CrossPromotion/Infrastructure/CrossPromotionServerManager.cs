﻿namespace Com.Mod.ThumbRun.CrossPromotion.Infrastructure
{
    using App;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class CrossPromotionServerManager : MonoBehaviour
    {
        public delegate void CouponServerEventHandler(string wwwText);
        public event CouponServerEventHandler PromotionDataLoadCompleted;
        public event CouponServerEventHandler Error;

        [SerializeField]
        private string _host;
        [SerializeField]
        private string _crossPromotionRequestURI;
        [SerializeField]
        private string _clientID;
        private HttpRequest _httpRequest;

        private void Awake()
        {
            _httpRequest = gameObject.AddComponent(typeof(HttpRequest)) as HttpRequest;
            _httpRequest.Complete += OnCompleted;
            _httpRequest.TimeOut += OnError;
            _httpRequest.Error += OnError;
        }

        void Start()
        {
        }

        void Update()
        {
        }

        private void OnDestroy()
        {
            _httpRequest.Complete -= OnCompleted;
            _httpRequest.TimeOut -= OnError;
            _httpRequest.Error -= OnError;
        }




        public void RequestCrossPromotion(string language)
        {
            string platform = Platform.GetPlatformType().ToString();
            string url = _host + _crossPromotionRequestURI + "?client_id=" + _clientID + "&platform=" + platform + "&orientation=HORIZONTAL" + "&language=" + language;
            _httpRequest.Get("requestCrossPromotion", url);
        }




        private void OnCompleted(string id, string wwwText)
        {
            if (id == "requestCrossPromotion")
            {
                Debug.Log("Cross Promotion Completed: " + wwwText);
                if (PromotionDataLoadCompleted != null) PromotionDataLoadCompleted(wwwText);
            }
        }

        private void OnError(string id, string wwwText)
        {
            if (id == "requestCrossPromotion")
            {
                Debug.Log("Cross Promotion Error: " + wwwText);
                if (Error != null) Error(wwwText);
            }
        }
    }
}