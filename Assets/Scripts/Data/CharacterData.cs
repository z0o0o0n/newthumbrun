﻿using UnityEngine;
using System.Collections;

public class CharacterData 
{
    // 런타임시 변경되지 않는 항목
    public int index;
    public int id;
    public string name;
    public Texture profileImage;
    public CharacterType.type type;
    public CharacterKind.kind kind;
    public string meshType;
    public int collectionID;
    public Color color;
    public Color bgColor;
    public AbilityStepData basicAbilityStepData;
    private CharacterStatusData rewardedStatusData;
    private AbilityStepData _basicAbilityStepData;
    private AbilityData _abilityData;
    public float gachaProbability;

    // 런타임시 변경되는 항목
    // 변경되는 항목은 변경 즉시 로컬에 저장함

    public CharacterData()
    {
    }

    public int exp
    {
        get { return GameData.instance.GetCharacterDataList().list[index].exp; }
        set { GameData.instance.GetCharacterDataList().list[index].exp = value; }
    }

    public float rewardedRun
    {
        get {
            float result = basicAbilityStepData.run + ((basicAbilityStepData.run / 100) * RewardDataManager.instance.GetRewardData().run);
            if(result > AbilityRange.maxRun) result = AbilityRange.maxRun;
            else if(result < AbilityRange.minRun) result = AbilityRange.minRun;
            return result;
        }
        //set { GameData.instance.GetCharacterDataList().list[id].rewardRun = value; }
    }
    public float rewardedBoostRun
    {
        get {
            float result = basicAbilityStepData.boostAbility + ((basicAbilityStepData.boostAbility / 100) * RewardDataManager.instance.GetRewardData().boostRun);
            if (result > AbilityRange.maxRun) result = AbilityRange.maxRun;
            else if (result < AbilityRange.minRun) result = AbilityRange.minRun;
            return result;
        }
        //set { GameData.instance.GetCharacterDataList().list[id].rewardBoostRun = value; }
    }

    public float rewardedJump
    {
        get {
            float result = basicAbilityStepData.jump + ((basicAbilityStepData.jump / 100) * RewardDataManager.instance.GetRewardData().jump);
            if (result > AbilityRange.maxJump) result = AbilityRange.maxJump;
            else if (result < AbilityRange.minJump) result = AbilityRange.minJump;
            return result;
        }
        //set { GameData.instance.GetCharacterDataList().list[id].rewardJump = value; }
    }

    public float rewardedBoostJump
    {
        get {
            float result = basicAbilityStepData.boostAbility + ((basicAbilityStepData.boostAbility / 100) * RewardDataManager.instance.GetRewardData().boostJump);
            if (result > AbilityRange.maxJump) result = AbilityRange.maxJump;
            else if (result < AbilityRange.minJump) result = AbilityRange.minJump;
            return result;
        }
        //set { GameData.instance.GetCharacterDataList().list[id].rewardBoostJump = value; }
    }

    //public float rewardedBoostCharging
    //{
    //    get {
    //        float result = basicAbilityStepData.boostCharging + ((basicAbilityStepData.boostCharging / 100) * RewardDataManager.instance.GetRewardData().boostCharging);
    //        if (result > AbilityRange.minBoostCharging) result = AbilityRange.minBoostCharging;
    //        else if (result < AbilityRange.minBoostCharging) result = AbilityRange.minBoostCharging;
    //        return result;
    //    }
    //    //set { GameData.instance.GetCharacterDataList().list[id].rewardBoostCharging = value; }
    //}

    //public float rewardedBoostConsumption
    //{
    //    get {
    //        float result = basicAbilityStepData.boostConsumption + ((basicAbilityStepData.boostConsumption / 100) * RewardDataManager.instance.GetRewardData().boostConsumption);
    //        if (result > AbilityRange.maxBoostConsumption) result = AbilityRange.maxBoostConsumption;
    //        else if (result < AbilityRange.minBoostConsumption) result = AbilityRange.minBoostConsumption;
    //        return result;
    //    }
    //    //set { GameData.instance.GetCharacterDataList().list[id].rewardBoostConsumption = value; }
    //}

    public bool isCollected
    {
        get { return GameData.instance.GetCharacterDataList().list[index].isCollected; }
        set { GameData.instance.GetCharacterDataList().list[index].isCollected = value; }
    }

    public CharacterStatusData GetRewardedStatusData()
    {
        CharacterStatusData result = new CharacterStatusData();
        result.run = rewardedRun;
        result.boostRun = rewardedBoostRun;
        result.jump = rewardedJump;
        result.boostJump = rewardedBoostJump;
        //result.boostCharging = rewardedBoostCharging;
        //result.boostConsumption = rewardedBoostConsumption;
        return result;
    }

    public AbilityData GetAbilityData()
    {
        _abilityData = new AbilityData();
        // AbilityStepData bonusAbilityStepData = BonusAbilityModel.instance.GetAbilityStepData();
        // AbilityStepData totalAbilityStepData = SumAbilityStepData(basicAbilityStepData, bonusAbilityStepData);

        _abilityData.run = AbilityRange.minRun + (AbilityRange.runStepValue * basicAbilityStepData.run);
        _abilityData.jump = AbilityRange.minJump + (AbilityRange.jumpStepValue * basicAbilityStepData.jump);
        _abilityData.jumpSpeed = AbilityRange.minJumpSpeed + (AbilityRange.jumpSpeedStepValue * basicAbilityStepData.jump);
        _abilityData.boostRun = AbilityRange.minBoostRun + (AbilityRange.boostRunStepValue * basicAbilityStepData.boostAbility);
        _abilityData.boostJump = AbilityRange.minBoostJump + (AbilityRange.boostJumpStepValue * basicAbilityStepData.boostAbility);
        _abilityData.boostJumpSpeed = AbilityRange.minBoostJumpSpeed + (AbilityRange.boostJumpSpeedStepValue * basicAbilityStepData.boostAbility);
        //_abilityData.boostAbility = AbilityRange.minBoostAbility + (AbilityRange.boostAbilityStepValue * totalAbilityStepData.boostAbility);
        //_abilityData.boostCharging = AbilityRange.minBoostCharging + (AbilityRange.boostChargingStepValue * totalAbilityStepData.boostCharging);
        //_abilityData.boostConsumption = AbilityRange.minBoostConsumption + (AbilityRange.boostConsumptionStepValue * totalAbilityStepData.boostConsumption);
        
        //Debug.Log("========================================================");
        //Debug.Log("basic ability step: " + basicAbilityStepData.ToString());
        //Debug.Log("bonus ability step: " + bonusAbilityStepData.ToString());
        //Debug.Log("total ability step: " + totalAbilityStepData.ToString());
        //Debug.Log("ability data: " + _abilityData.ToString());

        return _abilityData;
    }

    private AbilityStepData SumAbilityStepData(AbilityStepData a, AbilityStepData b)
    {
        AbilityStepData abilityStepData = new AbilityStepData();
        abilityStepData.run = a.run + b.run;
        abilityStepData.jump = a.jump + b.jump;
        abilityStepData.boostAbility = a.boostAbility + b.boostAbility;
        //abilityStepData.boostCharging = a.boostCharging + b.boostCharging;
        //abilityStepData.boostConsumption = a.boostConsumption + b.boostConsumption;

        return abilityStepData;
    }
    

    public string ToString()
    {
        string result = "< Character Data >\n";
        result += "index: " + index + "\n";
        result += "id: " + id + " \n";
        result += "name: " + name + " \n";
        result += "type: " + type.ToString() + " \n";
        result += "kind: " + kind.ToString() + " \n";
        result += "meshType: " + meshType.ToString() + " \n";
        result += "collectionID: " + collectionID + " \n";
        result += "color: " + color + " \n";
        result += "bgColor: " + bgColor + " \n";
        //result += statusData.ToString();
        result += "gachaProbabilty: " + gachaProbability + " \n";
        result += "rewardRun: " + rewardedRun + " \n";
        result += "rewardBoostRun: " + rewardedBoostRun + " \n";
        result += "rewardJump: " + rewardedJump + " \n";
        result += "rewardBoostJump: " + rewardedBoostJump + " \n";
        //result += "rewardBoostCharging: " + rewardedBoostCharging + " \n";
        //result += "rewardBoostConsumption: " + rewardedBoostConsumption + " \n";
        result += "isCollected: " + isCollected + " \n";
        return result;
    }
}
