﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterStatusData 
{
    public float run;
    public float boostRun;
    public float jump;
    public float boostJump;
    //public float boostCharging;
    //public float boostConsumption;
    public List<StringFloatDic> abilityList = new List<StringFloatDic>();

    public CharacterStatusData(float run = 0.0f, float boostRun = 0.0f, float jump = 0.0f, float boostJump = 0.0f)
    {
        this.run = run;
        this.boostRun = boostRun;
        this.jump = jump;
        this.boostJump = boostJump;
        //this.boostCharging = boostCharging;
        //this.boostConsumption = boostConsumption;

        abilityList.Add(new StringFloatDic(CharacterStatusType.type.Run.ToString(), run));
        abilityList.Add(new StringFloatDic(CharacterStatusType.type.BoostRun.ToString(), boostRun));
        abilityList.Add(new StringFloatDic(CharacterStatusType.type.Jump.ToString(), jump));
        abilityList.Add(new StringFloatDic(CharacterStatusType.type.BoostJump.ToString(), boostJump));
        //abilityList.Add(new StringFloatDic(CharacterStatusType.type.BoostCharging.ToString(), boostCharging));
        //abilityList.Add(new StringFloatDic(CharacterStatusType.type.BoostConsumption.ToString(), boostConsumption));
    }

    public void SetRun(float value)
    {
        this.run = value;
        abilityList[0] = new StringFloatDic(CharacterStatusType.type.Run.ToString(), value);
    }

    public void SetBoostRun(float value)
    {
        this.boostRun = value;
        abilityList[1] = new StringFloatDic(CharacterStatusType.type.BoostRun.ToString(), boostRun);
    }

    public void SetJump(float value)
    {
        this.jump = value;
        abilityList[2] = new StringFloatDic(CharacterStatusType.type.Jump.ToString(), jump);
    }

    public void SetBoostJump(float value)
    {
        this.boostJump = value;
        abilityList[3] = new StringFloatDic(CharacterStatusType.type.BoostJump.ToString(), boostJump);
    }

    //public void SetBoostCharging(float value)
    //{
    //    this.boostCharging = value;
    //    abilityList[4] = new StringFloatDic(CharacterStatusType.type.BoostCharging.ToString(), boostCharging);
    //}

    //public void SetBoostConsumption(float value)
    //{
    //    this.boostConsumption = value;
    //    abilityList[5] = new StringFloatDic(CharacterStatusType.type.BoostConsumption.ToString(), boostConsumption);
    //}

    public string ToString()
    {
        string result = "";
        result += "run: " + run + " \n";
        result += "boostRun: " + boostRun + " \n";
        result += "jump: " + jump + " \n";
        result += "boostJump: " + boostJump + " \n";
        //result += "boostCharging: " + boostCharging + " \n";
        //result += "boostConsumption: " + boostConsumption + " \n";
        return result;
    }
}
