﻿using Com.Mod.ThumbRun.Race.Domain;

public class RaceResult 
{
	public ParticipantData participantData;
    public Reward reward;
}
