﻿using UnityEngine;
using System.Collections;

public class VersusDifficultyType : MonoBehaviour 
{
    public enum type { Easy, Normal, Hard, VeryHard, Intense};

    public static VersusDifficultyType.type GetTypeByIndex(int index)
    {
        VersusDifficultyType.type result = VersusDifficultyType.type.Easy;
        
        if(index == 0)
        {
            result = VersusDifficultyType.type.Easy;
        }
        else if (index == 1)
        {
            result = VersusDifficultyType.type.Normal;
        }
        else if (index == 2)
        {
            result = VersusDifficultyType.type.Hard;
        }
        else if (index == 3)
        {
            result = VersusDifficultyType.type.VeryHard;
        }
        else if (index == 4)
        {
            result = VersusDifficultyType.type.Intense;
        }

        return result;
    }
}
