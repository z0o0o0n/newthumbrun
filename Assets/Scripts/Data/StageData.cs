﻿using UnityEngine;
using System.Collections;
using GameDataEditor;
using System.Collections.Generic;

public class StageData 
{
    public string id;
    public string name;
    public float previewZoom;
    public float previewPosY;
    public Color bgColor;
    public int replayDataVer;

    public string ToString()
    {
        string result = "< Stage Data >\n";
        result += "id: " + id + " \n";
        result += "name: " + name + " \n";
        result += "previewZoom: " + previewZoom + " \n";
        result += "previewPosY: " + previewPosY;
        return result;
    }
}
