﻿using UnityEngine;
using System.Collections.Generic;

public class CharacterDataManager : BaseSingleton<CharacterDataManager> 
{
    private bool _isDebug = false;
    private List<CharacterData> _characterDataList;

    public void Init(List<CharacterData> characterDataList)
    {
        _characterDataList = characterDataList;
    }

    public void PushNewCharacter(int characterID)
    {
        LogCollectionCondition();

        if (IsNewCharacter(characterID))
        {
            AddToCollectedCharacterIDList(characterID);
            AddToNewCharacterIDList(characterID);

            //int collectionID = CharacterDataManager.instance.GetCharacterDataByID(characterID).collectionID;
            //CollectionDataManager.instance.UpdateCompletedCollection(collectionID, characterID);
        }
    }

    private void LogCollectionCondition()
    {
        if (_isDebug)
        {
            LogList("(추가전) 수집된 캐릭터 ID 리스트", GameData.instance.GetUserCollectionData().collectedCharacterIDList);
            LogList("(추가전) 신규 캐릭터 ID 리스트", GameData.instance.GetUserCollectionData().newCharacterIDList);
        }
    }

    private bool IsNewCharacter(int characterID)
    {
        Debug.Log("추가된 ID " + characterID + " 캐릭터가 신규수집된 캐릭터인지 확인합니다.");
        bool result = true;
        List<int> collectedCharacterIDList = GameData.instance.GetUserCollectionData().collectedCharacterIDList;
        if (collectedCharacterIDList.Contains(characterID))
        {
            Debug.Log("-> false. ID " + characterID + " 캐릭터는 신규수집된 캐릭터가 아닙니다.");
            result = false;
        }
        else
        {
            Debug.Log("-> true. ID " + characterID + " 캐릭터는 신규수집된 캐릭터입니다.");
            result = true;
        }
        return result;
    }

    private void AddToCollectedCharacterIDList(int characterID)
    {
        if (_isDebug) Debug.Log("수집된 캐릭터 ID 리스트에 추가합니다.");

        // LowData인 '사용자수집현황데이터'의 '수집한캐릭터아이디리스트'에 저장
        List<int> collectedCharacterIDList = GameData.instance.GetUserCollectionData().collectedCharacterIDList;
        collectedCharacterIDList.Add(characterID);
        GameData.instance.GetUserCollectionData().Set_collectedCharacterIDList();

        // '컬렉션데이터'의 '수집한캐릭터리스트' 저장
        CollectionData collectionData = CollectionDataManager.instance.GetCollectionDataByCharacterID(characterID);
        collectionData.PushCollectedCharacterID(characterID);

        // 해당 '캐릭터데이터'의 '수집여부'를 '참'으로 설정
        CharacterDataManager.instance.GetCharacterDataByID(characterID).isCollected = true;

        if (_isDebug) Debug.Log("수집된 캐릭터 ID: " + characterID);
        if (_isDebug) LogList("수집된 캐릭터 ID 리스트", collectedCharacterIDList);
    }

    private void AddToNewCharacterIDList(int characterID)
    {
        if (_isDebug) Debug.Log("신규 수집된 캐릭터 ID 리스트에 추가합니다.");

        // LowData인 '사용자수집현황데이터'의 '신규캐릭터아이디리스트'에 저장
        List<int> newCharacterIDList = GameData.instance.GetUserCollectionData().newCharacterIDList;
        newCharacterIDList.Add(characterID);
        GameData.instance.GetUserCollectionData().Set_newCharacterIDList();

        if (_isDebug) Debug.Log("신규 캐릭터 ID: " + characterID);
        if (_isDebug) LogList("신규 캐릭터 ID 리스트", newCharacterIDList);
    }

    private void LogList(string name, List<int> list)
    {
        string tempLog = name + ": [";
        for (int i = 0; i < list.Count; i++)
        {
            if (i == list.Count - 1) tempLog = tempLog + list[i].ToString() + "]";
            else tempLog = tempLog + list[i].ToString() + ", ";
        }
        if (list.Count == 0) tempLog = tempLog + "]";
        Debug.Log(tempLog);
    }

    public void ClearNewCharacterIDList()
    {
        List<int> newCharacterIDList = GameData.instance.GetUserCollectionData().newCharacterIDList;
        newCharacterIDList.Clear();
        GameData.instance.GetUserCollectionData().Set_newCharacterIDList();
        if (_isDebug) Debug.Log("신규캐릭터아이디리스트 의 내용이 비워짐. Count: " + newCharacterIDList.Count);
    }

    public bool HasNew()
    {
        if (HasNewCharacter() || HasNewCompletedCollection()) return true;
        else return false;
    }

    private bool HasNewCharacter()
    {
        List<int> newCharacterIDList = GameData.instance.GetUserCollectionData().newCharacterIDList;
        if (newCharacterIDList.Count > 0) return true;
        else return false;
    }

    private bool HasNewCompletedCollection()
    {
        return false;
    }

    public List<CharacterData> GetCharacterDataByType(CharacterType.type type)
    {
        List<CharacterData> result = new List<CharacterData>();

        for(int i = 0; i < _characterDataList.Count; i++)
        {
            CharacterData data = _characterDataList[i];
            if (type == data.type) result.Add(data);
        }
        return result;
    }

    public CharacterData GetCharacterDataByID(int id)
    {
        CharacterData result = null;
        for (int i = 0; i < _characterDataList.Count; i++)
        {
            CharacterData data = _characterDataList[i];
            if(_isDebug) Debug.Log("id: " + id + " / data.id: " + data.id + " / " + (id == data.id));
            if (id == data.id)
            {
                result = _characterDataList[i];
                break;
            }
        }
        //Debug.Log("result: " + result.ToString() + " / result.id: " + result.id);
        if (result == null) Debug.LogError("CharacterDataManager - GetCharacterDataByID() / id:" + id + "와 매칭되는 CharacterData가 Null입니다.");
        return result;
    }

    public CharacterData GetCharacterDataByIndex(int index)
    {
        CharacterData result = null;
        
        return result;
    }
}
