﻿using UnityEngine;
using System.Collections;

public class WinnerDataManager : MonoBehaviour 
{
    public Texture profileTexture;
    public Texture nationTexture;
    public Texture defaultProfileTexture;
    public Texture defaultNationTexture;

    private static GameObject _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = gameObject;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void Reset()
    {
        profileTexture = defaultProfileTexture;
        nationTexture = defaultNationTexture;
    }
}
