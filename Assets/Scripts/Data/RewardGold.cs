﻿using UnityEngine;
using System.Collections;

public class RewardGold : MonoBehaviour 
{
    public int good;
    public int fair;
    public int poor;

    public RewardGold(int good, int fair, int poor)
    {
        this.good = good;
        this.fair = fair;
        this.poor = poor;
    }

    public string ToString()
    {
        string result = "< Reward Gold >\n";
        result += "good: " + good + " \n";
        result += "fair: " + fair + " \n";
        result += "poor: " + poor + " \n";
        return result;
    }
}
