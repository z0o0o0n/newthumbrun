﻿using UnityEngine;
using System.Collections;

public class PersonalRecord 
{
    public float record = 0.0f;
    public ReplayData replayData;
    public int characterID = 0;

    public PersonalRecord(float record, ReplayData replayData, int characterID)
    {
        this.record = record;
        this.replayData = replayData;
        this.characterID = characterID;
    }

    public string ToString()
    {
        string result = "< Personal Record >\n";
        result += "record: " + record + " \n";
        result += replayData.ToString();
        result += "characterID: " + characterID + " \n";
        return result;
    }
}
