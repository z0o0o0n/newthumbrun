﻿using UnityEngine;
using System.Collections;
using GameDataEditor;
using Com.Mod.ThumbRun.Event;

namespace Com.Mod.ThumbRun.Data
{
    public class BoosterData : MonoBehaviour
    {
        public event DataEvent.DataEventHandler Prepared;

        private GDEBoosterData rawData;

        void Start()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Booster_Booster, out rawData))
            {
                Debug.Log("BoosterData Prepared");
                if (Prepared != null) Prepared();
            }
            else
            {
                Debug.LogError("Read Error BoosterData");
            }
        }

        void Update()
        {

        }



        #region Public Functions
        public GDEBoosterData GetRawData()
        {
            return rawData;
        }
        #endregion
    }
}