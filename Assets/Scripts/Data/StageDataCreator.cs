﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using Junhee.Utils;
using System;

public class StageDataCreator : BaseSingleton<StageDataCreator> 
{
    public List<StageData> Create(GDEStageDataListData data)
    {
        List<StageData> list = new List<StageData>();
        for (int i = 0; i < data.list.Count; i++)
        {
            GDEStageDataData lowData = data.list[i];
            StageData stageData = new StageData();
            stageData.id = lowData.id;
            stageData.name = lowData.name;
            stageData.previewZoom = lowData.previewZoom;
            stageData.previewPosY = lowData.previewPosY;
            stageData.bgColor = lowData.bgColor;
            stageData.replayDataVer = lowData.replayVer;
            list.Add(stageData);

            //Debug.Log(stageData.ToString());
        }

        return list;
    }
}
