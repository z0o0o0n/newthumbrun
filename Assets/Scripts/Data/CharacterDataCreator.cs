﻿using UnityEngine;
using System.Collections;
using GameDataEditor;
using System.Collections.Generic;
using Junhee.Utils;
using System;

public class CharacterDataCreator : BaseSingleton<CharacterDataCreator>
{
    public List<CharacterData> Create(GDECharacterDataListData data)
    {
        List<CharacterData> list = new List<CharacterData>();

        for(int i = 0; i < data.list.Count; i++)
        {
            GDECharacterDataData lowData = data.list[i];

            CharacterData characterData = new CharacterData();
            characterData.index = i;
            characterData.id = lowData.id;
            characterData.name = lowData.name;
            characterData.profileImage = Resources.Load("CharacterProfileImage/CharacterProfileThumb_" + lowData.id) as Texture;
            characterData.type = CharacterType.CompareTypeAndString(lowData.type);
            characterData.kind = CharacterKind.CompareKindAndString(lowData.kind);
            characterData.collectionID = lowData.collectionID;
            characterData.color = ColorUtil.ConvertHexToColor(Convert.ToInt32(lowData.color, 16));
            characterData.bgColor = ColorUtil.ConvertHexToColor(Convert.ToInt32(lowData.bgColor, 16));
            characterData.basicAbilityStepData = new AbilityStepData(lowData.abilityStepRun, lowData.abilityStepJump, lowData.abilityStepBoostAbility);
            characterData.gachaProbability = lowData.gachaProbability;
            characterData.isCollected = lowData.isCollected;
            characterData.meshType = lowData.meshType;
            list.Add(characterData);
            //Debug.Log(characterData.ToString());
        }
        return list;
    }
}
