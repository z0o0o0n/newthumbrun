﻿using UnityEngine;
using System.Collections;

public class RewardStandard : MonoBehaviour 
{
    public float good;
    public float fair;
    public float poor;

    public RewardStandard(float good, float fair, float poor)
    {
        this.good = good;
        this.fair = fair;
        this.poor = poor;
    }

    public string ToString()
    {
        string result = "< Reward Standard >\n";
        result += "good: " + good + " \n";
        result += "fair: " + fair + " \n";
        result += "poor: " + poor + " \n";
        return result;
    }
}
