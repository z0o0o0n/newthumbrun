﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

namespace Com.Mod.ThumbRun.Data
{
	public class LuckyChanceData : MonoBehaviour 
	{
		public event LuckyChanceEvent.LuckyChanceEventHandler Prepared;

		// private bool _isChanceActivated = false;
		// private int _goalRaceCount = 5;
		// private int _standardGold = 1000;
		// private float _diameter = 1f;
		// private int _remainingRaceCount;
		private List<float> _diameterList = new List<float>(){1f, 1.5f, 2f, 2.5f, 3f, 5f};
		// private List<bool> _raceOutcomeList = new List<bool>();
		private GDELuckyChanceData _rawData;
		private bool _isPrepared = false;
		
		void Start () 
		{
			
		}

		void Update () 
		{
			
		}



		#region Properties
		public bool isChanceActivated
		{
			get{
				return _rawData.isChanceActivated;
			}
			set{
				_rawData.isChanceActivated = value;
			}
		}

		public int goalRaceCount
		{
			get{
				return _rawData.goalRaceCount;
			}
		}

		public int standardGold
		{
			get{
				return _rawData.standardGold;
			}
		}

		public float diameter
		{
			get{
				return _rawData.diameter;
			}
			set{
				_rawData.diameter = value;
			}
		}

		public int remainingRaceCount
		{
			get{
				return _rawData.remainingRaceCount;
			}
			set{
				_rawData.remainingRaceCount = value;
			}
		}

		public List<float> diameterList
		{
			get{
				return _diameterList;
			}
		}

		public List<bool> raceOutcomeList
		{
			get{
				return _rawData.raceOutcomeList;
			}
		}

		public int bonusGold
		{
			get{
				return (int) (_rawData.standardGold * _rawData.diameter);
			}
		}

		public bool isPrepared
		{
			get{
				return _isPrepared;
			}
		}
		#endregion



		#region Public Functions
		public void Prepare()
		{
			if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.LuckyChance_LuckyChance, out _rawData))
            {
                Debug.Log("LuckyChance Prepared");

                CheckRaceOutcomeListCount();

				_isPrepared = true;
                if (Prepared != null) Prepared ();
            }
            else
            {
                Debug.LogError("Read Error LuckyChance");
            }
		}

		public void AddRaceOutcome(bool outcome)
		{
			_rawData.raceOutcomeList.Add(outcome);
			_rawData.Set_raceOutcomeList();
		}

		public void Reset()
		{
			// _rawData.isChanceActivated = false;
			// _rawData.diameter = 1f;
			// _rawData.remainingRaceCount = _rawData.goalRaceCount;
			// _rawData.raceOutcomeList.Clear();
			_rawData.ResetAll();
		}
		#endregion



        #region Private Functions
        // raceOutcomeList의 수량이 goalRaceCount보다 큰 경우 끝에서 goalRaceCount만큼을 제외한 나머지를 삭제함
        private void CheckRaceOutcomeListCount()
        {
            int count = _rawData.raceOutcomeList.Count;
            int goalCount = _rawData.goalRaceCount;
            if (count > goalCount)
            {
                _rawData.raceOutcomeList.RemoveRange(0, count - goalCount);
                _rawData.Set_raceOutcomeList();
            }
        }
        #endregion
    }
}