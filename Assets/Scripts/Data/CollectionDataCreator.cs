﻿using UnityEngine;
using System.Collections;
using GameDataEditor;
using System.Collections.Generic;
using System;
using Junhee.Utils;

public class CollectionDataCreator : BaseSingleton<CollectionDataCreator> 
{
    public List<CollectionData> Create(GDECollectionDataListData data)
    {
        List<CollectionData> list = new List<CollectionData>();
        
        for (int i = 0; i < data.list.Count; i++)
        {
            GDECollectionDataData lowData = data.list[i];
            CollectionData collectionData = new CollectionData();
            collectionData.id = lowData.id;
            collectionData.name = lowData.name;
            collectionData.characterIDList = lowData.characterIDList;
            collectionData.color = ColorUtil.ConvertHexToColor(Convert.ToInt32(lowData.color, 16));
            collectionData.bgColor = ColorUtil.ConvertHexToColor(Convert.ToInt32(lowData.bgColor, 16));
            collectionData.rewardData = new CharacterStatusData(lowData.rewardRun, lowData.rewardBoostRun, lowData.rewardJump, lowData.rewardBoostJump);
            list.Add(collectionData);

            //Debug.Log(collectionData.ToString());
        }

        return list;
    }
}
