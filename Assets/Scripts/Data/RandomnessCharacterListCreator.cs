﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomnessCharacterListCreator : BaseSingleton<RandomnessCharacterListCreator> 
{
    private bool _isInit = false;
    private List<int> _randomnessListE = new List<int>();
    private List<int> _randomnessListA = new List<int>();
    private List<int> _randomnessListD = new List<int>();
    private List<int> _randomnessListS = new List<int>();

    public void Init()
    {
        if (!_isInit)
        {
            _isInit = true;
            Create();
        }
    }

    public void Create()
    {
        _randomnessListA = CreateRandomnessList(CharacterType.type.A);
        _randomnessListD = CreateRandomnessList(CharacterType.type.D);
        _randomnessListE = CreateRandomnessList(CharacterType.type.E);
        _randomnessListS = CreateRandomnessList(CharacterType.type.S);
        LogRandomnessList();
    }

    private void LogRandomnessList()
    {
        string log = "";
        log += "< 캐릭터 무작위 리스트 생성 > \n";
        log += "A타입 리스트: " + _randomnessListA.Count + " \n";
        log += "D타입 리스트: " + _randomnessListD.Count + " \n";
        log += "E타입 리스트: " + _randomnessListE.Count + " \n";
        log += "S타입 리스트: " + _randomnessListS.Count + " \n";
        //Debug.Log(log);
    }

    private List<int> CreateRandomnessList(CharacterType.type type)
    {
        CharacterDataManager characterDataManger = CharacterDataManager.instance;
        List<CharacterData> characterDataList = characterDataManger.GetCharacterDataByType(type);
        List<int> result = new List<int>();
        for (int i = 0; i < characterDataList.Count; i++)
        {
            for (int j = 0; j < characterDataList[i].gachaProbability; j++)
            {
                result.Add(characterDataList[i].id);
            }
        }
        return result;
    }

    // Get Lottery List
    //public List<int> GetListByType(CharacterType.type characterType)
    //{
    //    if (characterType == CharacterType.type.A) return _randomnessListA;
    //    else if (characterType == CharacterType.type.D) return _randomnessListD;
    //    else if (characterType == CharacterType.type.E) return _randomnessListE;
    //    else if (characterType == CharacterType.type.S) return _randomnessListS;
    //    return null;
    //}

    public bool IsInit()
    {
        return _isInit;
    }
}
