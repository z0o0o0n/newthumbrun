﻿using UnityEngine;
using System.Collections;

public class FreeGoldProvisionData : MonoBehaviour 
{
    [SerializeField]
    private int _rewardADGold;
    [SerializeField]
    private int _minSpecialRewardADGoldRete;
    [SerializeField]
    private int _maxSpecialRewardADGoldRete;
    [SerializeField]
    private int _freeGold;

	void Start () 
    {
	
	}
	
	void Update () 
    {

    }



    #region Properties
    public int rewardADGold
    {
        get { return _rewardADGold; }
    }

    public int freeGold
    {
        get { return _freeGold; }
    }
    #endregion



    #region Public Functions

    #endregion
}
