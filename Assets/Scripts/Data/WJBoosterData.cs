﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Event;
using GameDataEditor;

namespace Com.Mod.ThumbRun.Data
{
    public class WJBoosterData : MonoBehaviour
    {
        public event DataEvent.DataEventHandler Prepared;

        private GDEWallJumpBoosterData rawData;

        void Start()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.WallJumpBooster_WallJumpBooster, out rawData))
            {
                Debug.Log("WallJumpBooster Prepared");
                if (Prepared != null) Prepared();
            }
            else
            {
                Debug.LogError("Read Error WallJumpBoosterData");
            }
        }

        void Update()
        {

        }



        #region Public Functions
        public GDEWallJumpBoosterData GetRawData()
        {
            return rawData;
        }
        #endregion
    }
}