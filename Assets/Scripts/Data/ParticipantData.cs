﻿using UnityEngine;
using System.Collections;

public class ParticipantData 
{
    public bool isUser;
    public string nickname;
    //public int characterIndex;
    public int characterId;
    public string countryCode;
    public int rank;
    private float _record;
    public int level;
    public short[] posXList;
    public short[] posYList;
    public short[] stateHashList;

    public ParticipantData(float record)
    {
        _record = record;
    }

    public float record
    {
        get { return _record; }
    }

    public void ChangeRecord(float record)
    {
        _record = record;
        //Debug.Log("/////////////// record: " + _record);
    }
}
