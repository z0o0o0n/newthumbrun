﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class SponsorData
{
    public GDESponsorDataData rawData;
    public int rewardGold;
    public int rewardGoldForFree;
    public bool isSponsoring
    {
        get
        {
            return rawData.isSponsoring;
        }
        set
        {
            rawData.isSponsoring = value;
        }
    }

    public string ToString()
    {
        string result = "< Sponsor Data >\n";
        result += "rewardGold: " + rewardGold + " \n";
        result += "rewardGoldForFree: " + rewardGoldForFree + " \n";
        result += "isSponsoring: " + isSponsoring + " \n";
        return result;
    }
}
