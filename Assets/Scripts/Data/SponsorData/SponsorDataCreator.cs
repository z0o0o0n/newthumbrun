﻿using UnityEngine;
using System.Collections;
using GameDataEditor;
using System.Collections.Generic;

public class SponsorDataCreator : BaseSingleton<SponsorDataCreator> 
{
    public SponsorData Create(GDESponsorDataData rawData)
    {
        SponsorData sponsorData = new SponsorData();
        sponsorData.rawData = rawData;
        sponsorData.isSponsoring = rawData.isSponsoring;
        sponsorData.rewardGold = rawData.rewardGold;
        sponsorData.rewardGoldForFree = rawData.rewardGoldForFree;

        return sponsorData;
    }
}
