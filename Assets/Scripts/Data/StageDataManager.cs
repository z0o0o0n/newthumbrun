﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StageDataManager : BaseSingleton<StageDataManager> 
{
    private bool isDebug = true;
    private List<StageData> _stageDataList;

    public void Init(List<StageData> stageDataList)
    {
        _stageDataList = stageDataList;
    }

    //public StageData GetStageDataByIndex(int index)
    //{
    //    int count = GetStageDataList().Count;

    //    StageData result = null;
    //    for (int i = 0; i < count; i++)
    //    {
    //        //Debug.Log("----- index: " + index + " == stageData.index: " + _stageDataList[i].index + " / bool: " + (index == _stageDataList[i].index));
    //        if(index == _stageDataList[i].index)
    //        {
    //            //Debug.Log("----- stageData: " + _stageDataList[i].id);
    //            result = _stageDataList[i];
    //        }
    //    }
    //    return result;
    //}

    public StageData GetStageDataByID(string stageID)
    {
        int count = GetStageDataList().Count;

        StageData result = null;
        for (int i = 0; i < count; i++)
        {
            if (stageID == _stageDataList[i].id)
            {
                result = _stageDataList[i];
            }
        }
        return result;
    }

    public List<StageData> GetStageDataList()
    {
        return _stageDataList;
    }

    public int GetStageCount()
    {
        return _stageDataList.Count;
    }

    public List<string> GetStageIDList()
    {
        int count = GetStageDataList().Count;

        List<string> result = new List<string>();
        for (int i = 0; i < count; i++)
        {
            result.Add(GetStageDataList()[i].id);
        }
        return result;
    }

    public void LogStageIDList()
    {
        int count = GetStageDataList().Count;
        string log = "----- Stage ID List: ";

        for(int i = 0; i < count; i++)
        {
            if (i >= count) log += GetStageDataList()[i].id;
            else log += GetStageDataList()[i].id + ", ";
        }
        Debug.Log(log);
    }
}
