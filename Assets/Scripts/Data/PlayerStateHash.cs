﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerStateHash : MonoBehaviour 
{
    /*
     * Replay Data의 상태Hash를 1자리형 byte형으로 저장하기위해 ID를 도입
     */

    private static List<string> stateNameList = new List<string> { "Main.Idle", "Main.LeftRun", "Main.RightRun", "Main.LeftJump", "Main.RightJump" };

	void Start () {
	
	}
	
	void Update () {
	
	}

    /*
     * stateID는 1부터 최대 127값까지 사용가능.
     * 해당 state가 없는경우 0을 리턴.
     */
    public static int GetHashToID(int stateHash)
    {
        for(int i = 0; i < stateNameList.Count; i++)
        {
            int hash = Animator.StringToHash(stateNameList[i]);
            if(hash == stateHash)
            {
                return i+1;
            }
        }
        return 0;
    }

    public static int GetIDToHash(int stateID)
    {
        int hash = Animator.StringToHash(stateNameList[stateID-1]);
        return hash;
    }

    public static string GetIDtoString(int stateID)
    {
        return stateNameList[stateID-1];
    }
}
