﻿using UnityEngine;
using System.Collections;

namespace Com.Mod.ThumbRun.Event
{
    public class BBBoosterEvent
    {
        public delegate void BBBoosterEventHandler();
        public delegate void BoostEventHandler(Vector3 pos, int direction);
    }
}