﻿using UnityEngine;
using System.Collections;

public class UIButtonEventDispatcher : MonoBehaviour
{
    public delegate void UIButtonEvent(bool isDown);
    public event UIButtonEvent On_ButtonPress;

    void OnPress(bool isDown)
    {
        if(On_ButtonPress != null) On_ButtonPress(isDown);
    }
}
