﻿using UnityEngine;
using System.Collections;

public class RaceEvent : MonoBehaviour 
{
    public delegate void RaceEventHandler();
    public delegate void RaceRoundEventHandler(int currentLap);
}
