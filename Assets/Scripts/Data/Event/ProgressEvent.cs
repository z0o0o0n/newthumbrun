﻿using UnityEngine;
using System.Collections;

namespace Com.Mod.ThumbRun.Event
{
    public class ProgressEvent
    {
        public delegate void ProgressEventHandler(float progress);
    }
}