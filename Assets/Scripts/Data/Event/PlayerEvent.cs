﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Item.View;

namespace Com.Mod.ThumbRun.Event
{
    public class PlayerEvent
    {
        public delegate void PlayerEventHandler();
        public delegate void StateEventHandler(int id, DamageType.type damageType);
        public delegate void PlayerJumpEventHandler(int direction);
        public delegate void PassPointEventHandler(int id);
        public delegate void RankEventHandler(int rank);
    }
}