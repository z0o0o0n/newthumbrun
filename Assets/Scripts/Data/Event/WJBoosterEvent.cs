﻿using UnityEngine;
using System.Collections;

namespace Com.Mod.ThumbRun.Event
{
    public class WJBoosterEvent
    {
        public delegate void WJBoosterEventHandler();
        public delegate void BoostEventHandler(Vector3 pos, int direction);
        public delegate void CumulativeBoostEventHandler(int qty, float rate);
    }
}