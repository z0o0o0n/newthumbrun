﻿using UnityEngine;
using System.Collections;

public class ItemManagerEvent : MonoBehaviour 
{
    public delegate void ItemManagerEventHandler(int itemID);
}
