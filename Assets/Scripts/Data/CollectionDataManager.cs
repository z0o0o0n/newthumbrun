﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectionDataManager : BaseSingleton<CollectionDataManager> 
{
    public bool isDebug = true;
    private List<CollectionData> _collectionDataList;

    public void Init(List<CollectionData> collectionDataList)
    {
        _collectionDataList = collectionDataList;
    }

    //public void UpdateCompletedCollection(int collectionID, int characterID)
    //{
    //    CollectionData collectionData = GetCollectionDataByID(collectionID);
    //    collectionData.collectedCharacterIDList.Add(characterID);
    //}

    public CollectionData GetCollectionDataByCharacterID(int CharacterID)
    {
        int collectionID = CharacterDataManager.instance.GetCharacterDataByID(CharacterID).collectionID;
        CollectionData collectionData = GetCollectionDataByCollectionID(collectionID);
        return collectionData;
    }

    public CollectionData GetCollectionDataByCollectionID(int collectionID)
    {
        CollectionData result = null;
        for (int i = 0; i < _collectionDataList.Count; i++)
        {
            //Debug.Log("collectionID: " + collectionID + " / " + _collectionDataList[i].id);
            if(collectionID == _collectionDataList[i].id)
            {
                result = _collectionDataList[i];
            }
        }
        return result;
    }

    public void ClearNewCollectionIDList()
    {
        List<int> idList = GameData.instance.GetUserCollectionData().newCompletedCollectionIDList;
        idList.Clear();
        GameData.instance.GetUserCollectionData().Set_newCompletedCollectionIDList();
        if (isDebug) Debug.Log("신규수집된컬렉션아이디리스트 의 내용이 비워짐. Count: " + idList.Count);
    }
}
