﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

public class CollectionData 
{
    // 런타임시 변경되지 않는 항목
    public int id;
    public string name;
    public List<int> characterIDList;
    public Color color;
    public Color bgColor;
    public CharacterStatusData rewardData;

    // 런타임시 변경되는 항목
    // 변경되는 항목은 변경 즉시 로컬에 저장함
    //public float rewardRun
    //{
    //    get { return GameData.instance.GetCollectionDataList().list[id].rewardRun; }
    //    set { GameData.instance.GetCollectionDataList().list[id].rewardRun = value; }
    //}
    //public float rewardBoostRun
    //{
    //    get { return GameData.instance.GetCollectionDataList().list[id].rewardBoostRun; }
    //    set { GameData.instance.GetCollectionDataList().list[id].rewardBoostRun = value; }
    //}

    //public float rewardJump
    //{
    //    get { return GameData.instance.GetCollectionDataList().list[id].rewardJump; }
    //    set { GameData.instance.GetCollectionDataList().list[id].rewardJump = value; }
    //}

    //public float rewardBoostJump
    //{
    //    get { return GameData.instance.GetCollectionDataList().list[id].rewardBoostJump; }
    //    set { GameData.instance.GetCollectionDataList().list[id].rewardBoostJump = value; }
    //}

    //public float rewardBoostCharging
    //{
    //    get { return GameData.instance.GetCollectionDataList().list[id].rewardBoostCharging; }
    //    set { GameData.instance.GetCollectionDataList().list[id].rewardBoostCharging = value; }
    //}

    //public float rewardBoostConsumption
    //{
    //    get { return GameData.instance.GetCollectionDataList().list[id].rewardBoostConsumption; }
    //    set { GameData.instance.GetCollectionDataList().list[id].rewardBoostConsumption = value; }
    //}

    public bool isCompleted
    {
        get { return LowCollectionData().isCompleted; }
        set { LowCollectionData().isCompleted = value; }
    }


    // 생성자
    public CollectionData()
    {
        
    }

    //public void AddCollectedCharacterID(int characterID)
    //{
    //    if (LowCollectionData().characterIDList.Count == GetCollectedCharacterID().Count)
    //    {

    //    }
    //}

    private GDECollectionDataData LowCollectionData()
    {
        List<GDECollectionDataData> list = GameData.instance.GetCollectionDataList().list;
        GDECollectionDataData result = null;
        for (int i = 0; i < list.Count; i++)
        {
            if(list[i].id == id)
            {
                result = list[i];
            }
        }
        return result;
    }

    public List<int> GetCollectedCharacterID()
    {
        return LowCollectionData().collectedCharacterIDList;
    }

    public void PushCollectedCharacterID(int value)
    {
        LowCollectionData().collectedCharacterIDList.Add(value);
        LowCollectionData().Set_collectedCharacterIDList();

        if (IsCompletedCollection())
        {
            #if UNITY_IOS
                //GameManager.achievementHandler.ReportAchievement("Diorama_" + id, 100);
            #endif
            isCompleted = true;
            PushToCompletedCollectionIDList();
            AddRewardToGlobalRewardData(rewardData);
        }
        else isCompleted = false;
    }

    private bool IsCompletedCollection()
    {
        int totalCount = LowCollectionData().characterIDList.Count;
        int collectedCount = LowCollectionData().collectedCharacterIDList.Count;
        if (totalCount == collectedCount) return true;
        else return false;
    }

    private void PushToCompletedCollectionIDList()
    {
        GameData.instance.GetUserCollectionData().newCompletedCollectionIDList.Add(id);
        GameData.instance.GetUserCollectionData().Set_newCompletedCollectionIDList();
    }

    private void AddRewardToGlobalRewardData(CharacterStatusData rewardData)
    {
        RewardDataManager.instance.AddRewardToGlobalRewardData(rewardData);
        //GameData.instance.GetGlobalRewardData().run += rewardData.run;
        //GameData.instance.GetGlobalRewardData().boostRun += rewardData.boostRun;
        //GameData.instance.GetGlobalRewardData().jump += rewardData.jump;
        //GameData.instance.GetGlobalRewardData().boostJump += rewardData.boostJump;
        //GameData.instance.GetGlobalRewardData().boostCharging += rewardData.boostCharging;
        //GameData.instance.GetGlobalRewardData().boostConsumption += rewardData.boostConsumption;
    }

    public void ClearCollectedCharacterIDList()
    {
        LowCollectionData().collectedCharacterIDList.Clear();
        LowCollectionData().Set_collectedCharacterIDList();
    }

    public string ToString()
    {
        string result = "< Collection Data >\n";
        result += "id: " + id + " \n";
        result += "name: " + name + " \n";
        result += "character ID List: ";
        for (int i = 0; i < characterIDList.Count; i++)
        {
            if (i >= (characterIDList.Count-1)) result += characterIDList[i] + "\n";
            else result += characterIDList[i] + ", ";
        }
        result += "collected character ID List: ";
        for (int j = 0; j < GetCollectedCharacterID().Count; j++ )
        {
            if (j >= (GetCollectedCharacterID().Count - 1)) result += GetCollectedCharacterID()[j] + "\n";
            else result += GetCollectedCharacterID()[j] + ", ";
        }
        result += "bgColor: " + bgColor + "\n";
        result += "rewardRun: " + rewardData.run + " \n";
        result += "rewardBoostRun: " + rewardData.boostRun + " \n";
        result += "rewardJump: " + rewardData.jump + " \n";
        result += "rewardBoostJump: " + rewardData.boostJump + " \n";
        //result += "rewardBoostCharging: " + rewardData.boostCharging + " \n";
        //result += "rewardBoostConsumption: " + rewardData.boostConsumption + " \n";
        result += "isCompleted: " + isCompleted + " \n";
        return result;
    }
}
