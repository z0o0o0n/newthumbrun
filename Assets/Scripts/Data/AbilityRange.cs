﻿using UnityEngine;
using System.Collections;

public class AbilityRange
{
    public static float minRun = 3.5f;
    public static float maxRun = 3.5f;
    public static float minBoostRun = 5f;
    public static float maxBoostRun = 5f;

    public static float minJump = 5.4f;
    public static float maxJump = 5.4f; // 8,4
    public static float minBoostJump = 5.8f;
    public static float maxBoostJump = 5.8f; // 6.2

    public static float minJumpSpeed = 3.5f;
    public static float maxJumpSpeed = 3.5f;
    public static float minBoostJumpSpeed = 0f;
    public static float maxBoostJumpSpeed = 0f;
    
    //public static float minBoostAbility = 12.0f;
    //public static float maxBoostAbility = 14.0f;
    //public static float minBoostCharging = 1.8f;
    //public static float maxBoostCharging = 2.3f;
    //public static float minBoostConsumption = 0.5f; // 5 - 값 = 효율(3.5)
    //public static float maxBoostConsumption = 1.5f; // 5 - 값 = 효율(2.5)
    //public static float consumptionReverseValue = 5;

    public static int stepNum = 15;

    public static float runStepValue
    {
        get { return (AbilityRange.maxRun - AbilityRange.minRun) / AbilityRange.stepNum; }
    }

    public static float jumpStepValue
    {
        get { return (AbilityRange.maxJump - AbilityRange.minJump) / AbilityRange.stepNum; }
    }

    public static float jumpSpeedStepValue
    {
        get { return (AbilityRange.maxJumpSpeed - AbilityRange.minJumpSpeed) / AbilityRange.stepNum; }
    }

    public static float boostRunStepValue
    {
        get { return (AbilityRange.maxBoostRun - AbilityRange.minBoostRun) / AbilityRange.stepNum; }
    }

    public static float boostJumpStepValue
    {
        get { return (AbilityRange.maxBoostJump - AbilityRange.minBoostJump) / AbilityRange.stepNum; }
    }

    public static float boostJumpSpeedStepValue
    {
        get { return (AbilityRange.maxBoostJumpSpeed - AbilityRange.minBoostJumpSpeed) / AbilityRange.stepNum; }
    }

    //public static float boostAbilityStepValue
    //{
    //    get { return (AbilityRange.maxBoostAbility - AbilityRange.minBoostAbility) / AbilityRange.stepNum; }
    //}

    //public static float boostChargingStepValue
    //{
    //    get { return (AbilityRange.maxBoostCharging - AbilityRange.minBoostCharging) / AbilityRange.stepNum; }
    //}

    //public static float boostConsumptionStepValue
    //{
    //    get { return (AbilityRange.maxBoostConsumption - AbilityRange.minBoostConsumption) / AbilityRange.stepNum; }
    //}
}
