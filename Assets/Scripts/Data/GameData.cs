﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using Com.Mod.ThumbRun.Tutorial.Application;
using Com.Mod.ThumbRun.CharacterManagement.Application;

public class GameData : BaseSingleton<GameData>
{
    private bool _isDebug = true;
    private static bool _isInit = false;
    
    private GDEConfigData _configData;
    //private GDEPersonalRecordData _personalRecordData;
    private GDEUserCollectionDataData _userCollectionData;
    private GDECollectionDataListData _lowCollectionDataList;
    private GDECharacterDataListData _lowCharacterDataList;
    private GDEStageDataListData _lowStageDataList;
    private GDEGlobalRewardDataData _globalRewardData;
    private GDEStageDataListData _stageDataList;
	private GDESettingData _settingData;
    // private GDEPurchaseData _purchaseData;
    
    
    // private GDESponsorDataData _sponsorRawData;
    // private GDEBonusAbilityDataData _bonusAbilityData;
    // private SponsorData _sponsorData;
    [SerializeField]
    private TutorialService _tutorialService;
    [SerializeField]
    private CharacterManagementService _characterManagementService;

    public void Init()
    {
        if (!_isInit)
        {
            _isInit = true;
            if (_isDebug) Debug.Log("GameData - Init() / Init Game Data");
            GDEDataManager.Init("gde_data");

            if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Config_Config, out _configData))
            {
                Debug.LogError("Error reading Config data!");
            }

            if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.UserCollectionData_UserCollectionData, out _userCollectionData))
            {
                Debug.LogError("Error reading Collection data!");
            }

            if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.CollectionDataList_CollectionDataList, out _lowCollectionDataList))
            {
                Debug.LogError("Error reading Collection data!");
            }

            if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.CharacterDataList_CharacterDataList, out _lowCharacterDataList))
            {
                Debug.LogError("Error reading CharacterDataList data!");
            }

            if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.StageDataList_StageDataList, out _lowStageDataList))
            {
                Debug.LogError("Error reading StageDataList data!");
            }

            if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.CharacterStatusData_GlobalRewardData, out _globalRewardData))
            {
                Debug.LogError("Error reading GlobalRewardData data!");
            }

            if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.StageDataList_StageDataList, out _stageDataList))
            {
                Debug.LogError("Error reading StageDataList data!");
            }

            // if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.SponsorData_SponsorData, out _sponsorRawData))
            // {
            //     Debug.LogError("Error reading SponsorData data!");
            // }


            // if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.BonusAbilityData_BonusAbilityData, out _bonusAbilityData))
            // {
            //     Debug.LogError("Error reading BonusAbilityData data!");
            // }

			if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Setting_Setting, out _settingData))
			{
				Debug.LogError("Error reading SettingData data!");
			}

            // if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Purchase_Purchase, out _purchaseData))
            // {
            //     Debug.LogError("Error reading PurchaseData data!");
            // }

            // CreateSponsorData();

            InitCollectionDataManager(CreateCollectionDataList());
            InitCharacterDataManager(CreateCharacterDataList());
            InitStageDataManager(CreateStageDataList());
        }
    }

    // private void CreateSponsorData()
    // {
    //     _sponsorData = SponsorDataCreator.instance.Create(_sponsorRawData);
    // }

    private List<CollectionData> CreateCollectionDataList()
    {
        return CollectionDataCreator.instance.Create(_lowCollectionDataList);
    }

    private List<CharacterData> CreateCharacterDataList()
    {
        return CharacterDataCreator.instance.Create(_lowCharacterDataList);
    }

    private List<StageData> CreateStageDataList()
    {
        return StageDataCreator.instance.Create(_lowStageDataList);
    }

    private void InitCollectionDataManager(List<CollectionData> collectionDataList)
    {
        CollectionDataManager.instance.Init(collectionDataList);
    }

    private void InitCharacterDataManager(List<CharacterData> characterDataList)
    {
        CharacterDataManager.instance.Init(characterDataList);
    }

    private void InitStageDataManager(List<StageData> stageDataList)
    {
        StageDataManager.instance.Init(stageDataList);
    }

    // public SponsorData GetSponsorData()
    // {
    //     return _sponsorData;
    // }

    public GDEConfigData GetConfigData()
    {
        return _configData;
    }

    public GDEUserCollectionDataData GetUserCollectionData()
    {
        return _userCollectionData;
    }

    public GDECollectionDataListData GetCollectionDataList()
    {
        return _lowCollectionDataList;
    }

    public GDECharacterDataListData GetCharacterDataList()
    {
        return _lowCharacterDataList;
    }

    public GDEGlobalRewardDataData GetGlobalRewardData()
    {
        return _globalRewardData;
    }

    public GDEStageDataListData GetStageDataList()
    {
        return _stageDataList;
    }

    // public GDEBonusAbilityDataData GetBonusAbilityData()
    // {
    //     return _bonusAbilityData;
    // }

	public GDESettingData GetSettingData()
	{
		return _settingData;
	}

    // public GDEPurchaseData GetPurchaseData()
    // {
    //     return _purchaseData;
    // }

    public bool IsInit()
    {
        return _isInit;
    }

    public void ClearAllData()
    {

        // _characterManagementService.ResetData();
    }
}
