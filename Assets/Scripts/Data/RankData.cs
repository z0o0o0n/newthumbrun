﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RankData : MonoBehaviour 
{
    public int rank = -1;
    public List<float> roundRecords = new List<float>();
    public float totalRecord = 0.0f;
}
