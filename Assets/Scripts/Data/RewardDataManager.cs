﻿using UnityEngine;
using System.Collections;

public class RewardDataManager : BaseSingleton<RewardDataManager> 
{
    // GlobalReward를 전달받아 LowGlobalRewardData에 저장한다.

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void AddRewardToGlobalRewardData(CharacterStatusData rewardData)
    {
        GameDataEditor.GDEGlobalRewardDataData lowData = GameData.instance.GetGlobalRewardData();
        lowData.run += rewardData.run;
        lowData.boostRun += rewardData.boostRun;
        lowData.jump += rewardData.jump;
        lowData.boostJump += rewardData.boostJump;
        //lowData.boostCharging += rewardData.boostCharging;
        //lowData.boostConsumption += rewardData.boostConsumption;
    }

    public CharacterStatusData GetRewardData()
    {
        GameDataEditor.GDEGlobalRewardDataData lowData = GameData.instance.GetGlobalRewardData();
        CharacterStatusData result = new CharacterStatusData(lowData.run, lowData.boostRun, lowData.jump, lowData.boostJump);
        return result;
    }

    //public void AddCollectionReward(int collectionID, CharacterStatusData reward)
    //{
    //    CollectionData collectionData = CollectionDataManager.instance.GetCollectionDataByCollectionID(collectionID);
    //    collectionData.rewardRun = reward.run;
    //    collectionData.rewardBoostRun = reward.boostRun;
    //    collectionData.rewardJump = reward.jump;
    //    collectionData.rewardBoostJump = reward.boostJump;
    //    collectionData.rewardBoostCharging = reward.boostCharging;
    //    collectionData.rewardBoostConsumption = reward.boostConsumption;
    //}
}
