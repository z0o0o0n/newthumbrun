﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Event;
using GameDataEditor;

namespace Com.Mod.ThumbRun.Data
{
    public class BBBoosterData : MonoBehaviour
    {
        public event DataEvent.DataEventHandler Prepared;

        private GDEBBBoosterData rawData;

        void Start()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.BBBooster_BBBooster, out rawData))
            {
                Debug.Log("BeatBoardBoostData Prepared");
                if (Prepared != null) Prepared();
            }
            else
            {
                Debug.LogError("Read Error BeatBoardBoostData");
            }
        }

        void Update()
        {

        }



        #region Public Functions
        public GDEBBBoosterData GetRawData()
        {
            return rawData;
        }
        #endregion
    }
}