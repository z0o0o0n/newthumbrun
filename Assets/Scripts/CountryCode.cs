﻿using UnityEngine;
using System.Collections;

public class CountryCode
{
    private static string[] code = { "051", "048", "072", "120", "140", "384", "818", "624", "324", "226", "356", "376", "400", "404", "414", "450", "466", "504", "480", "508", "562", "566", "512", "634", "682", "686", "710", "788", "800", "784", "036", "156", "344", "360", "392", "410", "458", "554", "608", "702", "158", "764", "704", "056", "100", "203", "208", "276", "233", "724", "250", "300", "191", "372", "380", "428", "438", "440", "442", "807", "348", "470", "498", "499", "528", "578", "040", "616", "620", "642", "643", "703", "705", "756", "246", "752", "792", "826", "660", "028", "032", "052", "084", "060", "068", "076", "092", "136", "152", "170", "188", "212", "214", "218", "222", "308", "320", "328", "340", "388", "484", "500", "558", "591", "600", "604", "659", "662", "670", "740", "044", "780", "796", "858", "862", "124", "630", "840"};

    public static string GetRandomCode()
    {
        return CountryCode.code[Random.Range(0, CountryCode.code.Length - 1)];
    }
}
