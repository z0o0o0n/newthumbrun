﻿using Com.Mod.ThumbRun.Util;
using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPAcknowledgmentManager : MonoBehaviour 
{
    public delegate void IAPAcknowledgmentEvent(Product product);
    public event IAPAcknowledgmentEvent Success;
    public event IAPAcknowledgmentEvent Fail;

    [SerializeField]
	private string _host;
	[SerializeField]
	private string _acknowledgmentAndroidRequestURI;
	[SerializeField]
	private string _acknowledgmentIOSRequestURI;
	[SerializeField]
	private string _clientID;
	[SerializeField]
	private string _secretKey;
    [SerializeField]
    private Product _product;
    private WWWHelper _wwwHelper;

    void Start () 
	{
		
	}
	
	void Update () 
	{

	}




    public void AcknowledgeForAndroid(string purchase, string signature, Product product)
    {
        _wwwHelper = WWWHelper.instance;
        _wwwHelper.OnHttpRequest += OnAcknowledged;

        _product = product;

        Dictionary<string, string> stringArgs = new Dictionary<string, string>();
        stringArgs.Add("client_id", _clientID);
        stringArgs.Add("purchase", purchase);
        stringArgs.Add("signature", signature);

        Debug.Log(">>>>>>>>>>>>>>>>>>> client_id: " + _clientID);
        Debug.Log(">>>>>>>>>>>>>>>>>>> purchase: " + purchase);
        Debug.Log(">>>>>>>>>>>>>>>>>>> signature: " + signature);

        string uri = _host + _acknowledgmentAndroidRequestURI;
        _wwwHelper.Post("AcknowledgedIAP", uri, stringArgs);
    }

	public void AcknowledgeForIOS(string receipt, string mode, Product product)
    {
        _wwwHelper = WWWHelper.instance;
        _wwwHelper.OnHttpRequest += OnAcknowledged;

        _product = product;

        Dictionary<string, string> stringArgs = new Dictionary<string, string>();
        stringArgs.Add("client_id", _clientID);
        stringArgs.Add("receipt", receipt);
        stringArgs.Add("mode", mode);
        
        string uri = _host + _acknowledgmentIOSRequestURI;
        _wwwHelper.Post("AcknowledgedIAP", uri, stringArgs);
    }




	private void OnAcknowledged(string id, string wwwText)
	{
        if(id == "AcknowledgedIAP")
        {
            _wwwHelper.OnHttpRequest -= OnAcknowledged;
            JsonData data = JsonMapper.ToObject(wwwText);
            Debug.Log("IAP Acknowledgment Manager - OnAcknowledged / result: " + data["result"].ToString());
            if(data["result"].ToString() == "success")
            {
                string token = TokenManager.CreateToken(_clientID + data["result"].ToString() + data["product_id"].ToString() + data["timestamp"].ToString(), _secretKey);
                if(data["hash"].ToString() == token)
                {
                    if (Success != null) Success(_product);
                }
                else
                {
                    if (Fail != null) Fail(_product);
                }
            }
            else
            {
                if (Fail != null) Fail(_product);
            }
        }
	}
}
