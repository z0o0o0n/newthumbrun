﻿namespace Com.Mod.ThumbRun.IAPManager.Domain
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using LitJson;
    using UnityEngine;
    using UnityEngine.Purchasing;

    public class IAPManager : MonoBehaviour, IStoreListener
	{
        public delegate void IAPManagerEvent(string productId);
        public delegate void IAPManagerFailEvent(string productId, string errorId);
        public event IAPManagerEvent PurchaseSuccess;
        public event IAPManagerFailEvent PurchaseFail;

        private static IAPManager _instance;

		[SerializeField]
		private IAPAcknowledgmentManager _iapAcknowledgmentManager;
		private IStoreController _controller;
    	private IExtensionProvider _extensions;
		private string _lastTransationID;
		private string _receipt;
        private string _currentProductId = "";
        private bool _isDebug = true;
        private List<Product> _products;

        public static IAPManager instance
		{
			get{ return _instance; }
		}





        private void Awake()
        {
            _iapAcknowledgmentManager.Success += OnAcknowledgmentSuccess;
            _iapAcknowledgmentManager.Fail += OnAcknowledgmentFail;
        }

        void Start () 
		{
			if(_instance == null)
			{
				_instance = gameObject.GetComponent<IAPManager>();
				DontDestroyOnLoad(gameObject);

				SetupBuilder();
			}
			else
			{
				Destroy(gameObject);
			}
		}
		
		void Update () 
		{
			
		}

        private void OnDestroy()
        {
            _iapAcknowledgmentManager.Success -= OnAcknowledgmentSuccess;
            _iapAcknowledgmentManager.Fail -= OnAcknowledgmentFail;
        }





        private void SetupBuilder()
        {
            if(_isDebug) Debug.Log("-----> IAP Manager / Setup Builder");
            var module = StandardPurchasingModule.Instance();
            module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;

            var builder = ConfigurationBuilder.Instance(module);
            builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1k41JXyQvvZsqRfvmjv6SdrNBkAukpqk69DNCB/uz61lzo2nANg/P/Q4YhvD8DLbRaHW8EfSZP8899D1enVF209JJhvZAGD0KzYaQbI0TBDDPd0zpozvL6C2A/nzUxt7g2p+9WT83JA5M5yg0FW7rYZSdqKtrqrVCP5IgwP5/VmOnT9XDX6cbtYq9cbcJces7+DAques2vr0F0xdcrm2Fyz+L+tY4BwWMdh+E7W7/htx0tuPCWfD876uhok0aip3kB551+rzoJKegTeji1+tHGQS/Z4eKMboeXUnXQeS5vBJLHu/CcVUijD17BVWTvH9WpNoqRBhr64sN6nKBdBxHQIDAQAB");
            builder.AddProduct("item.gold_a", ProductType.Consumable, new IDs
            {
				//{"item.gold_a", GooglePlay.Name},
                {"item.gold_a", GooglePlay.Name},
                {"item.gold_a", AppleAppStore.Name}
            });
            builder.AddProduct("item.gold_b", ProductType.Consumable, new IDs
            {
                //{"item.gold_b", GooglePlay.Name}
                {"item.gold_b", GooglePlay.Name},
                {"item.gold_b", AppleAppStore.Name}
            });
            builder.AddProduct("item.gold_c", ProductType.Consumable, new IDs
            {
                //{"item.gold_c", GooglePlay.Name}
                {"item.gold_c", GooglePlay.Name},
                {"item.gold_c", AppleAppStore.Name}
            });
            builder.AddProduct("item.gold_d", ProductType.Consumable, new IDs
            {
                //{"item.gold_d", GooglePlay.Name}
                {"item.gold_d", GooglePlay.Name},
                {"item.gold_d", AppleAppStore.Name}
            });
            builder.AddProduct("item.gold_e", ProductType.Consumable, new IDs
            {
                //{"item.gold_e", GooglePlay.Name}
                {"item.gold_e", GooglePlay.Name},
                {"item.gold_e", AppleAppStore.Name}
            });
            builder.AddProduct("item.gold_g", ProductType.Consumable, new IDs
            {
                //{"item.gold_e", GooglePlay.Name}
                {"item.gold_g", GooglePlay.Name},
                {"item.gold_g", AppleAppStore.Name}
            });
            builder.AddProduct("item.gold_h", ProductType.Consumable, new IDs
            {
                //{"item.gold_e", GooglePlay.Name}
                {"item.gold_h", GooglePlay.Name},
                {"item.gold_h", AppleAppStore.Name}
            });

            builder.AddProduct("item.goldevent_a", ProductType.Consumable, new IDs
            {
                //{"item.goldevent_a", GooglePlay.Name}
                {"item.goldevent_a", GooglePlay.Name},
                {"item.goldevent_a", AppleAppStore.Name}
            });
            builder.AddProduct("item.goldevent_b", ProductType.Consumable, new IDs
            {
                //{"item.goldevent_b", GooglePlay.Name}
                {"item.goldevent_b", GooglePlay.Name},
                {"item.goldevent_b", AppleAppStore.Name}
            });

            builder.AddProduct("item.roulette_a", ProductType.Consumable, new IDs
            {
                //{"item.roulette_a", GooglePlay.Name}
                {"item.roulette_a", GooglePlay.Name},
                {"item.roulette_a", AppleAppStore.Name}
            });

            builder.AddProduct("unlock_slot_tier1", ProductType.Consumable, new IDs
            {
                //{"item.roulette_a", GooglePlay.Name}
                {"unlock_slot_tier1", GooglePlay.Name},
                {"unlock_slot_tier1", AppleAppStore.Name}
            });
            
            UnityPurchasing.Initialize(this, builder);
        }





        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            _controller = controller;
            _extensions = extensions;
            
            _products = new List<Product>();
            Debug.Log("-----> IAP Manager / init");
            foreach (var item in _controller.products.all)
            {
                if (item.availableToPurchase)
                {
                    _products.Add(item);

                    Debug.Log(string.Join(" - ",
                        new[]
                        {
                        item.metadata.localizedTitle,
                        item.metadata.localizedDescription,
                        item.metadata.isoCurrencyCode,
                        item.metadata.localizedPrice.ToString(),
                        item.metadata.localizedPriceString,
                        item.transactionID,
                        item.receipt
                        }));
                }
            }
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
			Debug.Log("Billing failed to initialize!");
			switch (error)
			{
				case InitializationFailureReason.AppNotKnown:
					Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
					break;
				case InitializationFailureReason.PurchasingUnavailable:
					// Ask the user if billing is disabled in device settings.
					Debug.Log("Billing disabled!");
					break;
				case InitializationFailureReason.NoProductsAvailable:
					// Developer configuration error; check product metadata.
					Debug.Log("No products available for purchase!");
					break;
			}
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            if (_isDebug) Debug.Log("----- Purchase Processing Result");
			_lastTransationID = e.purchasedProduct.transactionID;
			_receipt = e.purchasedProduct.receipt;

			JsonData receipt = JsonMapper.ToObject(_receipt);
			
            #if UNITY_ANDROID
                JsonData payload = JsonMapper.ToObject(receipt["Payload"].ToString());
            Debug.Log("-----> IAPManager / ---- Payload.json: " + payload["json"]);
            Debug.Log("-----> IAPManager / ---- Payload.signature: " + payload["signature"]);
			    _iapAcknowledgmentManager.AcknowledgeForAndroid(payload["json"].ToString(), payload["signature"].ToString(), e.purchasedProduct);
            #elif UNITY_IPHONE
                Debug.Log("-----> IAPManager / ---- Payload: " + receipt["Payload"]);
                _iapAcknowledgmentManager.AcknowledgeForIOS(receipt["Payload"].ToString(), "DEVELOPMENT", e.purchasedProduct);
            #endif

            //return PurchaseProcessingResult.Complete;
            return PurchaseProcessingResult.Pending;
        }

        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
            Debug.Log("-----> IAPManager / OnPurchaseFailed");
            if (p == PurchaseFailureReason.PurchasingUnavailable)
            {
            }
            else if(p == PurchaseFailureReason.DuplicateTransaction)
            {

            }
            else if (p == PurchaseFailureReason.ExistingPurchasePending)
            {

            }
            else if (p == PurchaseFailureReason.PaymentDeclined)
            {

            }
            else if (p == PurchaseFailureReason.ProductUnavailable)
            {

            }
            else if (p == PurchaseFailureReason.SignatureInvalid)
            {

            }
            else if (p == PurchaseFailureReason.Unknown)
            {

            }
            else if (p == PurchaseFailureReason.UserCancelled)
            {

            }

            Debug.Log("Purchase failed: " + i.definition.id);
            _currentProductId = "";
            if (PurchaseFail != null) PurchaseFail(_currentProductId, p.ToString()); // IAP may be disabled in device settings.
        }

        private void OnAcknowledgmentSuccess(Product product)
        {
            Debug.Log("IAP Manager - OnAcknowledgmentSuccess / SUCCESS / product: " + product.transactionID);
            //
            _controller.ConfirmPendingPurchase(product);
            if (PurchaseSuccess != null) PurchaseSuccess(_currentProductId);
            _currentProductId = "";
        }

        private void OnAcknowledgmentFail(Product product)
        {
            Debug.Log("IAP Manager - OnAcknowledgmentFail / FAIL / product: " + product.transactionID);
            if (PurchaseFail != null) PurchaseFail(_currentProductId, "AcknowledgmentFail");
            _currentProductId = "";
            //_controller.ConfirmPendingPurchase(product);
        }





        public void Buy(string productId) 
		{
            Debug.Log("================ buy: _controller: " + _controller);
            Debug.Log("-----> IAPManager - productId: " + productId);
            _currentProductId = productId;
            _controller.InitiatePurchase(productId);
		}

        // 디바이스에서 활성화된 스토어 또는 국가를 기준으로 가져온 상품정보
        public List<Product> GetProducts()
        {
            return _products;
        }

        public Product GetProductById(string productId)
        {
			if (_controller == null) return null;
			Product product = _controller.products.WithStoreSpecificID(productId);
            return product;
        }
	}
}