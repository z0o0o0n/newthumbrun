﻿namespace Com.Mod.ThumbRun.Item.View
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;

    public class DamageIce : MonoBehaviour
    {
        [SerializeField]
        private List<Vector3> rotationList;

        void Start()
        {
            int randomIndex = Random.Range(0, rotationList.Count);
            transform.localRotation = Quaternion.Euler(rotationList[randomIndex]);
        }

        void Update()
        {

        }
    }
}