﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TimeSlideDamageImage : MonoBehaviour
{
    [SerializeField]
    private UISprite _image;
    [SerializeField]
    private BgmManager _bgmManager;

    void Start ()
    {
	
	}
	
	void Update ()
    {
	
	}



    public void Show(float duration)
    {
        DOTween.Kill("ShowTimeSlideDamageImage." + GetInstanceID());
        DOTween.To(() => _image.alpha, x => _image.alpha = x, 0.75f, 1f).SetId("ShowTimeSlideDamageImage." + GetInstanceID());
        DOTween.Kill("HideTimeSlideDamageImageDelay." + GetInstanceID());
        DOVirtual.DelayedCall(duration, Hide).SetId("HideTimeSlideDamageImageDelay." + GetInstanceID());
        _bgmManager.SlowMode();    
    }

    private void Hide()
    {
        DOTween.To(() => _image.alpha, x => _image.alpha = x, 0f, 1f).SetId("ShowTimeSlideDamageImage." + GetInstanceID());
        AudioSource bgmAudioSource = _bgmManager.GetComponent<AudioSource>();
        DOTween.To(() => bgmAudioSource.pitch, x => bgmAudioSource.pitch = x, 1f, 0.5f).SetId("BgmPitch." + GetInstanceID());
        _bgmManager.NormalMode();
    }
}
