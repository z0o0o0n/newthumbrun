﻿namespace Com.Mod.ThumbRun.Item.View
{
    using UnityEngine;
    using System.Collections;
    using Junhee.Utils;
    using System.Collections.Generic;

    public class ItemDamageEffect : MonoBehaviour
    {
        [SerializeField]
        private ParticleSystem _trapDamageEffect;
        [SerializeField]
        private GameObject _iceBombEffect;
        [SerializeField]
        private ParticleSystem _missileDamageEffect;
        [SerializeField]
        private AudioSource _audioSource;
        [SerializeField]
        private AudioClip _missileExplosionSound;
        [SerializeField]
        private List<AudioClip> _bombFreezeSounds;
        private GameObject _playerGO;
        private Character _character;

        void Start()
        {
        }

        void Update()
        {

        }

        void OnDestroy()
        {
            if (_playerGO.tag == "Player")
            {
                PlayerManager player = _playerGO.GetComponent<PlayerManager>();
                player.Initialized -= OnPlayerInitialized;
                player.PlayerSwooned -= OnPlayerSwooned;
                player.PlayerRecovered -= OnPlayerRecovered;
            }
            else if (_playerGO.tag == "Replayer")
            {
                Replayer player = _playerGO.GetComponent<Replayer>();
                player.Initialized -= OnPlayerInitialized;
                player.PlayerSwooned -= OnPlayerSwooned;
                player.PlayerRecovered -= OnPlayerRecovered;
            }
        }



        public void Init(GameObject playerGO)
        {
            _playerGO = playerGO;

            if (_playerGO.tag == "Player")
            {
                PlayerManager player = _playerGO.GetComponent<PlayerManager>();
                player.Initialized += OnPlayerInitialized;
                player.PlayerSwooned += OnPlayerSwooned;
                player.PlayerRecovered += OnPlayerRecovered;
            }
            else if (_playerGO.tag == "Replayer")
            {
                Replayer player = _playerGO.GetComponent<Replayer>();
                player.Initialized += OnPlayerInitialized;
                player.PlayerSwooned += OnPlayerSwooned;
                player.PlayerRecovered += OnPlayerRecovered;
            }
        }



        private void OnPlayerInitialized()
        {
            IRaceParticipant player = null;
            if (_playerGO.tag == "Player")
            {
                player = _playerGO.GetComponent<PlayerManager>();
            }
            else if (_playerGO.tag == "Replayer")
            {
                player = _playerGO.GetComponent<Replayer>();
            }

            _character = player.GetCharacter();
            //Debug.Log("+++++++ _character: " + _character);
        }

        private void OnPlayerSwooned(int playerId, DamageType.type damageType)
        {
            Character character = _character.GetComponent<Character>();
            Animator anim = _character.GetComponent<Animator>();
            if (damageType == DamageType.type.TRAP)
            {
                _trapDamageEffect.gameObject.SetActive(true);
                character.ChangeMaterialProp(ColorUtil.ConvertHexToColor(0x6D437C), 0.6f);
                anim.Play(Animator.StringToHash("Main.TrapDamage"));
            }
            else if (damageType == DamageType.type.BOMB)
            {
                _iceBombEffect.gameObject.SetActive(true);
                character.ChangeMaterialProp(ColorUtil.ConvertHexToColor(0x0070A8), 0.8f);
                anim.speed = 0f;

                int randomIndex = Random.Range(0, _bombFreezeSounds.Count);
                _audioSource.clip = _bombFreezeSounds[randomIndex];
                _audioSource.Play();
            }
            else if(damageType == DamageType.type.MISSILE)
            {
                character.ChangeMaterialProp(Color.black, 0.9f);
                _missileDamageEffect.gameObject.SetActive(true);
                _missileDamageEffect.Play();
                anim.speed = 0f;

                _audioSource.clip = _missileExplosionSound;
                _audioSource.Play();
            }
            else if (damageType == DamageType.type.TIME_SLICE)
            {
                character.ChangeMaterialProp(Color.black, 0.4f);
                anim.speed = 0f;
            }
            else
            {
                anim.speed = 0f;
            }
        }

        private void OnPlayerRecovered(int playerId, DamageType.type damageType)
        {
            HideEffect(damageType);
        }

        private void HideEffect(DamageType.type damageType)
        {
            if (damageType == DamageType.type.TRAP)
            {
                _trapDamageEffect.gameObject.SetActive(false);
            }
            else if (damageType == DamageType.type.BOMB)
            {
                _iceBombEffect.gameObject.SetActive(false);
            }

            Character character = _character.GetComponent<Character>();
            character.ChangeMaterialProp(Color.black, 0f);
        }
    }
}