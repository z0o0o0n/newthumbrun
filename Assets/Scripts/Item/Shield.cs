﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using System;

public class Shield : MonoBehaviour, IItem
{
	[SerializeField]
	private List<GameObject> _shieldEffects;
	[SerializeField]
	private AudioSource _audioSource;
	private int _id = 2;
	private float _sheldDuration = 4f;

    public int id
    {
        get
        {
            return _id;
        }
    }



	void Awake ()
	{
		
	}

    void Start () 
	{
        for(int i = 0; i < _shieldEffects.Count; i++)
        {
            _shieldEffects[i].gameObject.SetActive(false);
        }
	}
	
	void Update () 
	{
	
	}



    public void Use(IRaceParticipant user, List<IRaceParticipant> others)
    {
        for (int i = 0; i < _shieldEffects.Count; i++)
        {
            _shieldEffects[i].gameObject.SetActive(true);
        }

        //if (shieldDuration > 0) _sheldDuration = shieldDuration;

        DOVirtual.DelayedCall(_sheldDuration, OnSheldEnded);
        user.Protect(_sheldDuration);
		_audioSource.Play();
    }



	private void OnSheldEnded()
	{
        for (int i = 0; i < _shieldEffects.Count; i++)
        {
            _shieldEffects[i].gameObject.SetActive(false);
        }
		Destroy(this);
	}
}
