﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using System;

public class ExpBox : MonoBehaviour, IItem
{
	[SerializeField]
	private GameObject _shieldEffect;
	[SerializeField]
	private AudioSource _audioSource;
	private int _id = 9;
	private float _sheldDuration = 0.5f;

    public int id
    {
        get
        {
            return _id;
        }
    }



	void Awake ()
	{
		
	}

    void Start () 
	{
		_shieldEffect.gameObject.SetActive(false);
	}
	
	void Update () 
	{
	
	}



	public void Use(IRaceParticipant user, List<IRaceParticipant> others)
    {
		_shieldEffect.gameObject.SetActive(true);
        DOVirtual.DelayedCall(_sheldDuration, OnSheldEnded);
		//_audioSource.Play();
    }



	private void OnSheldEnded()
	{
		_shieldEffect.gameObject.SetActive(false);
		Destroy(this);
	}
}
