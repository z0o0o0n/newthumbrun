﻿using System.Collections.Generic;

public interface IItem
{
	int id { get; }
	void Use(IRaceParticipant user, List<IRaceParticipant> others);
}