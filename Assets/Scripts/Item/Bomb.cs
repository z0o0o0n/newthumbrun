﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using System;
using Com.Mod.ThumbRun.Item.View;

public class Bomb : MonoBehaviour, IItem
{
    [SerializeField]
    private ParticleSystem _explosionPrecursorEffectA;
    [SerializeField]
    private ParticleSystem _explosionPrecursorEffectB;
    [SerializeField]
    private GameObject _explosionFire;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _precursorSound;
    [SerializeField]
    private AudioClip _explosionSound;
    [SerializeField]
    private List<ParticleSystem> _effects;
    private int _id = 5;
    private float _diameter = 1.8f;
    private float _explosionPrecursorDuration = 1f;
    private float _swoonDuration = 1f;
    private bool _isUsed = false;
    private List<IRaceParticipant> _victims;
    private IRaceParticipant _user;
    private List<IRaceParticipant> _others;

    private float _additionalAttackArea = 0f;
    private float _additionalPower = 0f;

    public int id
    {
        get
        {
            return _id;
        }
    }



    void Awake ()
    {
        _explosionPrecursorEffectA.gameObject.SetActive(false);
        _explosionPrecursorEffectB.gameObject.SetActive(false);
        _explosionFire.SetActive(false);
    }

	void Start () 
    {
	    
	}
	
	void Update () 
    {

    }



    public void Use(IRaceParticipant user, List<IRaceParticipant> others)
    {
        if (!_isUsed)
        {
            _isUsed = true;
            _user = user;
            _others = others;
            StartExplosionPrecursor();
        }
    }



    // others 중 피해를 받을 대상자를 찾음
    private void IdentifyVictim(IRaceParticipant user, List<IRaceParticipant> others)
    {
        _victims = new List<IRaceParticipant>();
        GameObject userGO = ((MonoBehaviour)user).gameObject;
        for(int i = 0; i < others.Count; i++)
        {
            IRaceParticipant raceParticipant = others[i];
            GameObject raceParticipantGO = ((MonoBehaviour)raceParticipant).gameObject;
            
            float distance = Vector2.Distance(userGO.transform.localPosition, raceParticipantGO.transform.localPosition);
            if (distance < (_diameter + _additionalAttackArea))
            {
                if (!raceParticipant.IsSwoon())
                {
                    _victims.Add(raceParticipant);
                }
            }
        }
    }

    private void StartExplosionPrecursor()
    {
        _audioSource.clip = _precursorSound;
        _audioSource.Play();

        _explosionPrecursorEffectA.gameObject.SetActive(true);
        _explosionPrecursorEffectB.gameObject.SetActive(true);
        DOVirtual.DelayedCall(_explosionPrecursorDuration, OnExplosion);
    }

    private void OnExplosion()
    {
        IdentifyVictim(_user, _others);

        _audioSource.clip = _explosionSound;
        _audioSource.Play();

        _explosionPrecursorEffectA.gameObject.SetActive(false);
        _explosionPrecursorEffectB.gameObject.SetActive(false);
        float explosionFireScale = (_diameter + _additionalAttackArea) * 0.79f; // 1 = 0.79 scale
        _explosionFire.transform.localScale = new Vector3(explosionFireScale, explosionFireScale, explosionFireScale);
        _explosionFire.SetActive(true);

        for(int i = 0; i < _victims.Count; i++)
        {
            IRaceParticipant raceParticipant = _victims[i];
            raceParticipant.Swoon(DamageType.type.BOMB, _swoonDuration + _additionalPower, _user);
        }

        DOVirtual.DelayedCall(0.2f, OnExplosionEnd);        
    }

    private void OnExplosionEnd()
    {
        Destroy(gameObject);
    }




    public void SetAdditionalAttackArea(float attackArea)
    {
        Debug.Log("---------- 스킬 / 얼음폭탄 추가 피해범위: " + attackArea + " / 기존 피해범위: " + _diameter);
        _additionalAttackArea = attackArea;
    }

    public void SetAdditionalPower(float power)
    {
        Debug.Log("---------- 스킬 / 얼음폭탄 추가 파워(기절시간): " + power + " / 기존 파워(기절시간): " + _swoonDuration);
        _additionalPower = power;
    }
}
