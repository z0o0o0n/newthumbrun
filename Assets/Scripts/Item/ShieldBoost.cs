﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;

public class ShieldBoost : MonoBehaviour, IItem 
{
    [SerializeField]
    private List<GameObject> _effects;
    [SerializeField]
    public TrailRenderer _boostTrail;
    [SerializeField]
    private AudioSource _audioSource;

	private int _id = 7;
	private float _duration = 3f;

    public int id
    {
        get
        {
			return _id;
        }
    }



	void Awake ()
	{
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(false);
        }
        if(_boostTrail) _boostTrail.time = 0;
    }

    void Start () 
	{
		
	}
	
	void Update () 
	{
	
	}



	#region Public Functions
	public void Use(IRaceParticipant user, List<IRaceParticipant> others)
    {
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(true);
        }

        if(_boostTrail) _boostTrail.time = 0.2f;
		user.Boost(_duration, 30f);
        user.Protect(_duration);

        _audioSource.Play();

        DOVirtual.DelayedCall(_duration, OnBoostEnded);
    }

	private void OnBoostEnded()
	{
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(true);
        }

        if(_boostTrail) _boostTrail.time = 0f;

        Destroy(gameObject);
	}
	#endregion
}
