﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;
using Com.Mod.ThumbRun.Item.View;

public class TimeStopWatch : MonoBehaviour, IItem 
{
    [SerializeField]
    private List<GameObject> _effects;
    public TrailRenderer _boostTrail;
    private RaceParticipationManager _raceParticipationManager;
    private TimeSlideDamageImage _timeSlideDamageImage;

	private int _id = 8;
	private float _duration = 1.5f;

    public int id
    {
        get
        {
			return _id;
        }
    }



	void Awake ()
	{
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(false);
        }
        if(_boostTrail) _boostTrail.time = 0;

        _raceParticipationManager = GameObject.FindGameObjectWithTag("RaceParticipationManager").GetComponent<RaceParticipationManager>();

    }

    void Start () 
	{
        _timeSlideDamageImage = GameObject.FindGameObjectWithTag("TimeSlideDamageImage").GetComponent<TimeSlideDamageImage>();
    }
	
	void Update () 
	{
	
	}



	#region Public Functions
	public void Use(IRaceParticipant user, List<IRaceParticipant> others)
    {
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(true);
        }

        if(_boostTrail) _boostTrail.time = 0.08f;
		DOVirtual.DelayedCall(_duration, OnBoostEnded);
		user.Boost(_duration, 30f);

        for(int i = 0; i < others.Count; i++)
        {
            others[i].Swoon(DamageType.type.TIME_SLICE, _duration, user);
        }

        _timeSlideDamageImage.Show(_duration);
    }

	private void OnBoostEnded()
	{
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(true);
        }

		if(_boostTrail) _boostTrail.time = 0f;
		Destroy(gameObject);
	}
	#endregion
}
