﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;

public class BoostS : MonoBehaviour, IItem 
{
    [SerializeField]
    private List<GameObject> _effects;
    public TrailRenderer _boostTrail;

	private int _id = 1;
	private float _boostDuration = 3f;
    private float _boostIncRate = 30f;
    private float _additionalTime = 0f;

    public int id
    {
        get
        {
			return _id;
        }
    }



	void Awake ()
	{
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(false);
        }
        if(_boostTrail) _boostTrail.time = 0;
	}

    void Start () 
	{
		
	}
	
	void Update () 
	{
	
	}



	#region Public Functions
	public void Use(IRaceParticipant user, List<IRaceParticipant> others)
    {
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(true);
        }
        if(_boostTrail) _boostTrail.time = 0.2f;
		DOVirtual.DelayedCall(_boostDuration + _additionalTime, OnBoostEnded);
		user.Boost(_boostDuration + _additionalTime, _boostIncRate);
    }

	private void OnBoostEnded()
	{
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(false);
        }
        if(_boostTrail) _boostTrail.time = 0f;
		Destroy(gameObject);
	}

    public void SetBoostAdditionalTime(float additionalTime)
    {
        _additionalTime = additionalTime;
    }
	#endregion
}
