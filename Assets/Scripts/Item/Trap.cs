﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using Com.Mod.ThumbRun.Item.View;

public class Trap : MonoBehaviour, IItem 
{
	[SerializeField]
	private BoxCollider _collider;
    [SerializeField]
    private List<ParticleSystem> _effects;
    [SerializeField]
    private Color _activationColor;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _placementSound;
    [SerializeField]
    private AudioClip _activationSound;
    [SerializeField]
    private AudioClip _popSound;
    private int _id = 6;
    private float _precursorDuration = 1.4f;
    private float _swoonDuration = 0.6f;
	private bool _isUsed = false;
	private bool _isActivated = false;
	private bool _isExplosion = false;
    private IRaceParticipant _user;
    private float _defaultSize = 1f;

    private float _additionalSize = 0f;
    private float _additionalPower = 0f;
    private float _additionalPreTime = 0f;

    public int id
    {
        get
        {
            return _id;
        }
    }

    

    void Start () 
	{
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(false);
        }
    }
	
	void Update () 
	{
		
	}

	void OnTriggerEnter(Collider other)
	{
		// other.gameObject.tag == "Replayer" ||
		if(_isActivated)
		{
			IRaceParticipant participant; 
			if(other.gameObject.tag == "ItemHitArea")
			{
				GameObject parent = other.gameObject.transform.parent.gameObject;
				if(parent.tag == "Player") participant = parent.GetComponent<PlayerManager>();
				else participant = parent.GetComponent<Replayer>();
				// participant = other.gameObject.GetComponent<Core>().participantGO;
				if(!_isExplosion)
				{
					Explosion(participant);
				}
			}
		}
	}



	#region Private Functions
	private void Explosion(IRaceParticipant participant)
	{
		DroppedTrapManager.RemoveTrap(gameObject.GetComponent<Trap>());
		_isExplosion = true;
		participant.Swoon(DamageType.type.TRAP, _swoonDuration + _additionalPower, _user);

        _audioSource.clip = _popSound;
        _audioSource.Play();

        DOVirtual.DelayedCall(_swoonDuration + _additionalPower, OnExplosionEnd);
    }

    private void OnExplosionEnd()
    {
        Destroy(gameObject);
    }

    private void OnActivate()
	{
        Debug.Log(">>>>>>>>>>>> trap / on activate");
		_isActivated = true;
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(true);
        }

        _effects[0].transform.DOScale(1.0f, 0.3f).SetEase(CustomEase.instance.OutBangElastic);
        //_effects[0].startColor = _activationColor;

        _audioSource.clip = _activationSound;
        _audioSource.Play();
    }
    #endregion



    #region Public Functions
    public void Use(IRaceParticipant user, List<IRaceParticipant> others)
    {
        if (!_isUsed)
        {
            _isUsed = true;
            _user = user;
            transform.parent = null;
			_collider.enabled = true;
			DroppedTrapManager.PushTrap(gameObject.GetComponent<Trap>());

            Debug.Log(">>>>>>>>>>>>>>> trap / _precursorDuration: " + _precursorDuration + " / _additionalPreTime: " + _additionalPreTime);
            float preTime = _precursorDuration - _additionalPreTime;
            if (preTime < 0.1f) preTime = 0.1f;

            DOTween.Kill("TrapActivate." + GetInstanceID());
            DOVirtual.DelayedCall(preTime, OnActivate).SetId("TrapActivate." + GetInstanceID());

            //float size = (_defaultSize + _additionalSize) ;
            

            for (int i = 0; i < _effects.Count; i++)
            {
                _effects[i].gameObject.SetActive(true);
                //_effects[i].startSize = 0.45f;
                _effects[i].transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            }

            _audioSource.clip = _placementSound;
            _audioSource.Play();

            BoxCollider collider = GetComponent<BoxCollider>();
            //collider.size = new Vector3(size * 0.1f, size * 0.1f, size * 0.1f);
            collider.gameObject.SetActive(true);
        }
    }

    public void SetAdditionalSize(float additionalSize)
    {
        _additionalSize = additionalSize;
    }

    public void SetAdditionalPreTime(float additionalPreTime)
    {
        _additionalPreTime = additionalPreTime;
    }

    public void SetAdditionalPower(float additionalPower)
    {
        _additionalPower = additionalPower;
    }
    #endregion
}
