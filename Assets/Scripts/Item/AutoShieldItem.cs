﻿namespace Com.Mod.ThumbRun.Race.View
{
    using UnityEngine;
    using System.Collections.Generic;
    using DG.Tweening;
    using System;
    using Com.Mod.ThumbRun.Race.Application;

    public class AutoShieldItem : MonoBehaviour, IItem
    {
        [SerializeField]
        private List<GameObject> _shieldEffects;
        [SerializeField]
        private AudioSource _audioSource;
        [SerializeField]
        private AutoShieldService _autoShieldService;
        private int _id = 11;
        private float _sheldDuration = 0.5f;

        public int id
        {
            get
            {
                return _id;
            }
        }





        void Awake()
        {
            _autoShieldService.ItemUsed += OnItemUsed;
        }

        void Start()
        {
            for (int i = 0; i < _shieldEffects.Count; i++)
            {
                _shieldEffects[i].gameObject.SetActive(false);
            }
        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _autoShieldService.ItemUsed -= OnItemUsed;
        }





        private void OnItemUsed(IRaceParticipant player)
        {
            Use(player, null);
        }





        public void Use(IRaceParticipant user, List<IRaceParticipant> others)
        {
            for (int i = 0; i < _shieldEffects.Count; i++)
            {
                _shieldEffects[i].gameObject.SetActive(true);
            }
            DOVirtual.DelayedCall(_sheldDuration, OnSheldEnded);
            user.Protect(_sheldDuration);
            _audioSource.Play();
        }

        private void OnSheldEnded()
        {
            for (int i = 0; i < _shieldEffects.Count; i++)
            {
                _shieldEffects[i].gameObject.SetActive(false);
            }
            Destroy(this);
        }
    }
}