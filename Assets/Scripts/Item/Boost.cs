﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;

public class Boost : MonoBehaviour, IItem 
{
	[SerializeField]
    private List<GameObject> _effects;
    //public TrailRenderer _boostTrail;

	private int _id = 0;
	private float _boostDuration = 1.5f;
    private float _boostIncRate = 20f;
    private float _additionalTime = 0f;

    public int id
    {
        get
        {
			return _id;
        }
    }



	void Awake ()
	{
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(false);
        }
        //_boostTrail.time = 0;
	}

    void Start () 
	{
		
	}
	
	void Update () 
	{
	
	}



	public void Use(IRaceParticipant user, List<IRaceParticipant> others)
    {
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(true);
        }
        //_boostTrail.time = 0.2f;
		DOVirtual.DelayedCall(_boostDuration + _additionalTime, OnBoostEnded);
		user.Boost(_boostDuration + _additionalTime, _boostIncRate);
    }

	private void OnBoostEnded()
	{
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(false);
        }
        //_boostTrail.time = 0f;
		Destroy(gameObject);
	}

    public void SetBoostAdditionalTime(float additionalTime)
    {
        _additionalTime = additionalTime;
    }
}
