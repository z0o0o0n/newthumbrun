﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using Com.Mod.Util;
using Com.Mod.ThumbRun.Item.View;

public class Missile : MonoBehaviour, IItem
{
    [SerializeField]
    private BoxCollider _collider;
    [SerializeField]
    private List<GameObject> _effects;
    [SerializeField]
    private GameObject _missile;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _shotSound;
    private int _id = 4;
    private float _precursorDuration = 0f;
    private float _swoonDuration = 0.8f;
    private bool _isUsed = false;
    private bool _isActivated = false;
    private bool _isExplosion = false;
    private Vector2 _direction;
    private IRaceParticipant _user;
    private List<IRaceParticipant> _others;

    private float _additionalSpeed = 0f;
    private float _additionalPower = 0f; // 추가 기절 시간


    public int id
    {
        get
        {
            return _id;
        }
    }



    void Awake()
    {
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(false);
        }

        _missile.gameObject.SetActive(false);
    } 

    void Start()
    {

    }

    void FixedUpdate()
    {
        if(_isActivated)
        {
            Vector3 pos = transform.localPosition;
            pos.x += _direction.x * (1.5f + _additionalSpeed);
            pos.y += _direction.y * (1.5f + _additionalSpeed);
            transform.localPosition = pos;

            _missile.transform.Rotate(new Vector3(0f, 10f, 0f));
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (_isActivated)
        {
            IRaceParticipant participant;

            if (other.gameObject.tag == "ItemHitArea")
            {
                GameObject userGO = ((MonoBehaviour)_user).gameObject;
                GameObject parentGO = other.gameObject.transform.parent.gameObject;
                //Debug.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!! user: " + userGO.GetInstanceID() + " / other: " + parentGO.GetInstanceID() + " / " + (userGO == parentGO));
                if (userGO == parentGO) return;

                if (parentGO.tag == "Player") participant = parentGO.GetComponent<PlayerManager>();
                else participant = parentGO.GetComponent<Replayer>();
                // participant = other.gameObject.GetComponent<Core>().participantGO;
                if (!_isExplosion)
                {
                    Explosion(participant);
                }
            }
        }
    }



    private void Explosion(IRaceParticipant participant)
    {
        Debug.Log("---------- 스킬능력 / 미사일 폭발 / 추가 기절시간: " + _additionalPower + " / 기본 기절시간: " + _swoonDuration);

        _isExplosion = true;
        participant.Swoon(DamageType.type.MISSILE, _swoonDuration + _additionalPower, _user);

        _audioSource.Stop();

        Destroy(gameObject);
    }



    public void Use(IRaceParticipant user, List<IRaceParticipant> others)
    {
        if (!_isUsed)
        {
            //Debug.Log("---------- !!!!!!!!!! USE");
            _isUsed = true;
            transform.parent = null;
            _collider.enabled = true;
            _isActivated = true;
            _user = user;
            _others = others;

            for (int i = 0; i < _effects.Count; i++)
            {
                _effects[i].gameObject.SetActive(true);
            }
            _missile.gameObject.SetActive(true);

            //GameObject userGO = ((MonoBehaviour)user).gameObject;
            //SpeedChecker speedChecker = userGO.GetComponent<SpeedChecker>();
            _direction = new Vector2(user.GetDirection()/10f, 0);
            _missile.transform.localRotation = Quaternion.Euler(0f, 0f, -90f * user.GetDirection());

            _audioSource.clip = _shotSound;
            _audioSource.Play();

            BoxCollider collider = GetComponent<BoxCollider>();
            collider.gameObject.SetActive(true);
        }
    }

    public void SetAdditionalSpeed(float additionalSpeed)
    {
        Debug.Log("---------- 스킬능력 / 미사일 발사속도 설정 / 추가 발사속도: " + additionalSpeed + " / 기본 발사속도: 1.5f");
        _additionalSpeed = additionalSpeed;
    }

    public void SetAdditionalPower(float additionalPower)
    {
        _additionalPower = additionalPower;
    }
}
