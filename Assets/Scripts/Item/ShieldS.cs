﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using System;

public class ShieldS : MonoBehaviour, IItem
{
    [SerializeField]
    private List<GameObject> _effects;
    [SerializeField]
	private AudioSource _audioSource;
	private int _id = 3;
	private float _sheldDuration = 3.5f;
    private float _additionalShieldDuration = 0f;

    public int id
    {
        get
        {
            return _id;
        }
    }



	void Awake ()
	{
		
	}

    void Start () 
	{
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(false);
        }
    }
	
	void Update () 
	{
	
	}



	#region Public Functions
	public void Use(IRaceParticipant user, List<IRaceParticipant> others)
    {
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(true);
        }

        DOVirtual.DelayedCall(_sheldDuration + _additionalShieldDuration, OnSheldEnded);
		user.Protect(_sheldDuration + _additionalShieldDuration);
		_audioSource.Play();
    }
	#endregion



	#region Private Functions
	private void OnSheldEnded()
	{
        for (int i = 0; i < _effects.Count; i++)
        {
            _effects[i].gameObject.SetActive(false);
        }

        Destroy(this);
	}
	#endregion




    public void SetAdditionalShieldDuration(float duration)
    {
        _additionalShieldDuration = duration;
    }
}
