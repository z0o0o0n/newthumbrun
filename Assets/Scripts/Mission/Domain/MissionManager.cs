﻿using Com.Mod.ThumbRun.Analytics.Application;
using Com.Mod.ThumbRun.CharacterManagement.Application;
using Com.Mod.ThumbRun.CharacterManagement.Domain;
using Com.Mod.ThumbRun.LuckyChance.Domain;
using Com.Mod.ThumbRun.Util;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionManager : MonoBehaviour
{
    private static MissionManager _instance;

    private AnalyticsService _analyticsServcie;
    private CharacterManagementService _characterManagementServcie;

    public static MissionManager instance
    {
        get { return _instance; }
    }





    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;

            _analyticsServcie = GameObject.FindGameObjectWithTag("Analytics").GetComponent<AnalyticsService>();
            _analyticsServcie.Prepared += OnPrepared;

            _characterManagementServcie = GameObject.FindGameObjectWithTag("CharacterManagement").GetComponent<CharacterManagementService>();
            _characterManagementServcie.Prepared += OnPrepared;

            if (_analyticsServcie.isPrepared && _characterManagementServcie.isPrepared) OnPrepared();

            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}




    private void OnPrepared()
    {
        if (!_analyticsServcie.isPrepared) return;
        if (!_characterManagementServcie.isPrepared) return;

        UpdateMissionState();
    }




    public void UpdateMissionState()
    {
        // 캐릭터 0 Panda Koo A
        //Debug.Log("########################## GetParticipantCount: " + _analyticsServcie.GetParticipantCount());
        //if (_analyticsServcie.GetParticipantCount() > 50)
        //{
        //    if (!_characterManagementServcie.GetItemById(0).isSkillUnlockA) _characterManagementServcie.UnlockSkillA(0);
        //}

        //if(_analyticsServcie.GetParticipantCount() > 332)
        //{
        //    if(!_characterManagementServcie.GetItemById(0).isSkillUnlockB) _characterManagementServcie.UnlockSkillB(0);
        //}

        for(int i = 0; i < _characterManagementServcie.GetItems().Count; i++)
        {
            GetCurrentState(_characterManagementServcie.GetItems()[i].characterId, 0);
            GetCurrentState(_characterManagementServcie.GetItems()[i].characterId, 1);
        }
    }




    public string GetCurrentState(int characterId, int slotId)
    {
        //GDECharacterManagementItemData gmItem = _characterManagementServcie.GetItemById(characterId);

        if (characterId == 0)
        {
            if (slotId == 0)
            {
                int goal = 15;
                int current = _analyticsServcie.GetspecificParticipantCount(1);
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                int goal = 10;
                int current = _analyticsServcie.GetTotalBadgeCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 1)
        {
            if (slotId == 0)
            {
                int goal = 25;
                int current = _analyticsServcie.GetParticipantCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                int goal = 5;
                int current = _analyticsServcie.GetSuccessionRankingCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 2)
        {
            if (slotId == 0)
            {
                return "";
            }
            else if (slotId == 1)
            {
                int goal = 5;
                int current = _analyticsServcie.GetKilledPlayerCountWithBomb();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 4)
        {
            if (slotId == 0)
            {
                int goal = 2500;
                int current = _analyticsServcie.GetTotalPrizeGold();
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + GoldFormat.ConvertString(current) + "/" + GoldFormat.ConvertString(goal) + ")";
            }
            else if (slotId == 1)
            {
                int goal = 3;
                int current = _analyticsServcie.GetSuccessionFirstRankingCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 6)
        {
            if (slotId == 0)
            {
                int goal = 10;
                int current = _analyticsServcie.GetWatchedAdCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                int goal = 10;
                int current = _analyticsServcie.GetKilledPlayerCountWithMissile();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 7)
        {
            if (slotId == 0)
            {
                int goal = 10;
                int current = _analyticsServcie.GetSpecificBadgeCount(LuckyChanceBadge.badge.BRONZE);
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                float goal = 30;
                float current = _analyticsServcie.GetBestRecord("1");
                if (current <= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + ScoreFormat.ConvertString(current) + "s/" + ScoreFormat.ConvertString(goal) + "s)";
            }
            else return "";
        } //------------------------------------
        if (characterId == 8)
        {
            if (slotId == 0)
            {
                int goal = 30;
                int current = _analyticsServcie.GetspecificParticipantCount(2); //변경
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                int goal = 25;
                int current = _analyticsServcie.GetTotalBadgeCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 3)
        {
            if (slotId == 0)
            {
                int goal = 100;
                int current = _analyticsServcie.GetParticipantCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                int goal = 7;
                int current = _analyticsServcie.GetSuccessionRankingCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 18)
        {
            if (slotId == 0)
            {
                if(_characterManagementServcie.GetItemById(23).isUnlocked) _characterManagementServcie.UnlockSkillA(characterId); //변경
                return "";
            }
            else if (slotId == 1)
            {
                int goal = 25;
                int current = _analyticsServcie.GetKilledPlayerCountWithBomb();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 29)
        {
            if (slotId == 0)
            {
                int goal = 250000;
                int current = _analyticsServcie.GetTotalPrizeGold();
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + GoldFormat.ConvertString(current) + "/" + GoldFormat.ConvertString(goal) + ")";
            }
            else if (slotId == 1)
            {
                int goal = 5;
                int current = _analyticsServcie.GetSuccessionFirstRankingCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 30)
        {
            if (slotId == 0)
            {
                int goal = 30;
                int current = _analyticsServcie.GetWatchedAdCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                int goal = 30;
                int current = _analyticsServcie.GetKilledPlayerCountWithMissile();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 44)
        {
            if (slotId == 0)
            {
                int goal = 30;
                int current = _analyticsServcie.GetSpecificBadgeCount(LuckyChanceBadge.badge.SILVER); // 변경
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                float goal = 40;
                float current = _analyticsServcie.GetBestRecord("2"); // 변경
                if (current <= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + ScoreFormat.ConvertString(current) + "s/" + ScoreFormat.ConvertString(goal) + "s)";
            }
            else return "";
        } //------------------------------------
        if (characterId == 23)
        {
            if (slotId == 0)
            {
                int goal = 30;
                int current = _analyticsServcie.GetspecificParticipantCount(3); //변경
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                int goal = 50;
                int current = _analyticsServcie.GetTotalBadgeCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 24)
        {
            if (slotId == 0)
            {
                int goal = 200;
                int current = _analyticsServcie.GetParticipantCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                int goal = 10;
                int current = _analyticsServcie.GetSuccessionRankingCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 25)
        {
            if (slotId == 0)
            {
                if (_characterManagementServcie.GetItemById(49).isUnlocked) _characterManagementServcie.UnlockSkillA(characterId); //변경
                return "";
            }
            else if (slotId == 1)
            {
                int goal = 50;
                int current = _analyticsServcie.GetKilledPlayerCountWithBomb();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 31)
        {
            if (slotId == 0)
            {
                int goal = 5000000;
                int current = _analyticsServcie.GetTotalPrizeGold();
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + GoldFormat.ConvertString(current) + "/" + GoldFormat.ConvertString(goal) + ")";
            }
            else if (slotId == 1)
            {
                int goal = 7;
                int current = _analyticsServcie.GetSuccessionFirstRankingCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 54)
        {
            if (slotId == 0)
            {
                int goal = 50;
                int current = _analyticsServcie.GetWatchedAdCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                int goal = 50;
                int current = _analyticsServcie.GetKilledPlayerCountWithMissile();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 32)
        {
            if (slotId == 0)
            {
                int goal = 30;
                int current = _analyticsServcie.GetSpecificBadgeCount(LuckyChanceBadge.badge.GOLD); // 변경
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                float goal = 45;
                float current = _analyticsServcie.GetBestRecord("3"); // 변경
                if (current <= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + ScoreFormat.ConvertString(current) + "s/" + ScoreFormat.ConvertString(goal) + "s)";
            }
            else return "";
        } // -----------------------------------
        else if (characterId == 49)
        {
            if (slotId == 0)
            {
                int goal = 30;
                int current = _analyticsServcie.GetspecificParticipantCount(4); //변경
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                int goal = 100;
                int current = _analyticsServcie.GetTotalBadgeCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
        else if (characterId == 53)
        {
            if (slotId == 0)
            {
                int goal = 300;
                int current = _analyticsServcie.GetParticipantCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else if (slotId == 1)
            {
                int goal = 15;
                int current = _analyticsServcie.GetSuccessionRankingCount();
                if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
                return "\n(" + current + "/" + goal.ToString() + ")";
            }
            else return "";
        }
		else if (characterId == 15)
		{
			if (slotId == 0)
			{
				if (_characterManagementServcie.GetItemById(53).isUnlocked) _characterManagementServcie.UnlockSkillA(characterId); //변경
				return "";
			}
			else if (slotId == 1)
			{
				int goal = 100;
				int current = _analyticsServcie.GetKilledPlayerCountWithBomb();
				if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
				return "\n(" + current + "/" + goal.ToString() + ")";
			}
			else return "";
		}
		else if (characterId == 51)
		{
			if (slotId == 0)
			{
				int goal = 10000000;
				int current = _analyticsServcie.GetTotalPrizeGold();
				if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
				return "\n(" + GoldFormat.ConvertString(current) + "/" + GoldFormat.ConvertString(goal) + ")";
			}
			else if (slotId == 1)
			{
				int goal = 10;
				int current = _analyticsServcie.GetSuccessionFirstRankingCount();
				if (current >= goal) _characterManagementServcie.UnlockSkillB(characterId);
				return "\n(" + current + "/" + goal.ToString() + ")";
			}
			else return "";
		}
		else if (characterId == 19)
		{
			if (slotId == 0)
			{
				int goal = 50;
				int current = _analyticsServcie.GetSpecificBadgeCount(LuckyChanceBadge.badge.GOLD); // 변경
				if (current >= goal) _characterManagementServcie.UnlockSkillA(characterId);
				return "\n(" + current + "/" + goal.ToString() + ")";
			}
			else if (slotId == 1)
			{
				float goal = 42;
				float current = _analyticsServcie.GetBestRecord("3"); // 변경
				if (current <= goal) _characterManagementServcie.UnlockSkillB(characterId);
				return "\n(" + ScoreFormat.ConvertString(current) + "s/" + ScoreFormat.ConvertString(goal) + "s)";
			}
			else return "";
		}
		
		return "";
    }
}
