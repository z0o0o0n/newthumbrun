﻿using UnityEngine;
using System.Collections;

public static class CharacterKind
{
    public enum kind { Hero, Item, None };

    public static CharacterKind.kind CompareKindAndString(string stringKind)
    {
        if (CharacterKind.kind.Hero.ToString().ToUpper() == stringKind.ToUpper()) return CharacterKind.kind.Hero;
        else if (CharacterKind.kind.Item.ToString().ToUpper() == stringKind.ToUpper()) return CharacterKind.kind.Item;
        else return CharacterKind.kind.None;
    }
}
