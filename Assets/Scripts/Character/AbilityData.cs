﻿using UnityEngine;
using System.Collections;

public class AbilityData 
{
    public float run;
    public float jump;
    public float jumpSpeed;
    public float boostRun;
    public float boostJump;
    public float boostJumpSpeed;
    //public float boostAbility;
    //public float boostCharging;
    //public float boostConsumption;

    public string ToString()
    {
        string result = "< Ability Data > \n";
        result += "run: " + run + "\n";
        result += "jump: " + jump + "\n";
        result += "jumpSpeed: " + jumpSpeed + "\n";
        result += "boostRun: " + boostRun + "\n";
        result += "boostJump: " + boostJump + "\n";
        result += "boostJump: " + boostJumpSpeed + "\n";
        //result += "boostAbility: " + boostAbility;
        //result += "boostCharging: " + boostCharging + "\n";
        //result += "boostConsumption: " + boostConsumption + "\n";
        return result;
    }
}
