﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CharacterPrefabData
{
	public int id;
	public GameObject prefab;
}
