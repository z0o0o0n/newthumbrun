﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;

public class CharacterDimmer : MonoBehaviour 
{
    public Renderer[] _renderer;

    void Awake()
    {
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void Dim(Color color)
    {
        //Color color = ColorUtil.ConvertHexToColor(0x222222);
        //_renderer.material.SetColor("_Color", color) ;
        for (int i = 0; i < _renderer.Length; i++ )
        {
            _renderer[i].material.SetColor("_Color", color);
            _renderer[i].material.SetFloat("_Alpha", 1.0f);
        }
    }

    public void Distinct()
    {
        for (int i = 0; i < _renderer.Length; i++)
        {
            _renderer[i].material.SetFloat("_Alpha", 0.0f);
        }
        //Color color = ColorUtil.ConvertHexToColor(0x808080);
        //_renderer.material.SetColor("_Color", color);
    }
}
