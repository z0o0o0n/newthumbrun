﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterPrefabProvider : BaseSingleton<CharacterPrefabProvider>
{
	private bool _isInit = false;
	private List<CharacterPrefabData> _characterPrefabDataList;

	public bool IsInit()
	{
		return _isInit;
	}

	public void Init(List<CharacterPrefabData> characterPrefabDataList)
	{
		if(!_isInit)
		{
            //Debug.Log("----- characterPrefabDataList: " + characterPrefabDataList.Count);
			_characterPrefabDataList = characterPrefabDataList;
			_isInit = true;
		}
	}

	public GameObject Get(int characterID)
	{
        GameObject result = null;
		for(int i = 0; i < _characterPrefabDataList.Count; i++)
		{
			if(_characterPrefabDataList[i].id == characterID)
			{
				return _characterPrefabDataList[i].prefab;
			}
		}
        if (result == null) Debug.LogError("CharacterPrefabProvider - Get() / Character Prefab Data List에 id:" + characterID + "와 매칭되는 Data가 없습니다.");
        return result;
	}

    public int GetIndexById(int characterID)
    {
        int result = -1;
        for (int i = 0; i < _characterPrefabDataList.Count; i++)
        {
            if (_characterPrefabDataList[i].id == characterID)
            {
                result = i;
                return result;
            }
        }

        return result;
    }

    public int GetIdByIndex(int index)
    {
        int result = -1;
        if (index < _characterPrefabDataList.Count) result = _characterPrefabDataList[index].id;
        return result;
    }
}
