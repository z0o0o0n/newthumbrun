﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour 
{
    public int id;
    public RuntimeAnimatorController normalAnimatorController;
    public RuntimeAnimatorController replayAnimatorController;
    public Material tintMaterial;
    public Material transparentMaterial;
    public Texture tex;

    private Animator _animator;
    private SkinnedMeshRenderer _renderer;
	
    void Awake ()
    {
        _animator = GetComponent<Animator>();
    }

	void Start () 
    {
        //ChangeMaterial(tintMaterial);
    }

    void Update ()
    {
        //if(Input.GetKeyUp(KeyCode.A))
        //{
        //    ChnageNormalCharacter();
        //}
        //else if (Input.GetKeyUp(KeyCode.S))
        //{
        //    ChangeReplayCharacter();
        //}
        //else if (Input.GetKeyUp(KeyCode.D))
        //{
        //    ChangeCollectionCharacter();
        //}
    }

    public void ChnageNormalCharacter()
    {
        ChangeMaterial(tintMaterial);
        ChangeAnimatorController(normalAnimatorController);
    }

    public void ChangeRivalCharacter()
    {
        ChangeMaterial(tintMaterial);
        ChangeAnimatorController(replayAnimatorController);
    }

    public void ChangeReplayCharacter()
    {
        //transparentMaterial.SetFloat("_node_5628", 1);
        ChangeMaterial(transparentMaterial);
        ChangeAnimatorController(replayAnimatorController);
    }

    public void ChangeCollectionCharacter()
    {
        ChangeMaterial(tintMaterial);
        ChangeAnimatorController(normalAnimatorController);
    }

    private void ChangeMaterial(Material material)
    {
        gameObject.SetActive(false);
        _renderer = GetComponent<SkinnedMeshRenderer>();
        Material[] materials = _renderer.materials;
        materials[0] = material;
        _renderer.materials = materials;
        gameObject.SetActive(true);
    }

    private void ChangeAnimatorController(RuntimeAnimatorController animatorController)
    {
        _animator.runtimeAnimatorController = animatorController;
    }

    public void ChangeMaterialProp(Color color, float alpha)
    {
        tintMaterial.SetColor("_Color", color);
        tintMaterial.SetFloat("_Alpha", alpha);
    }
}
