﻿using System.Collections;

public static class CharacterType
{
	public enum type { E, A, D, S, E_S, A_S, D_S, S_S, None };

    public static CharacterType.type CompareTypeAndString(string stringType)
    {
        if (CharacterType.type.A.ToString().ToUpper() == stringType.ToUpper()) return CharacterType.type.A;
        else if (CharacterType.type.A_S.ToString().ToUpper() == stringType.ToUpper()) return CharacterType.type.A_S;
        else if (CharacterType.type.D.ToString().ToUpper() == stringType.ToUpper()) return CharacterType.type.D;
        else if (CharacterType.type.D_S.ToString().ToUpper() == stringType.ToUpper()) return CharacterType.type.D_S;
        else if (CharacterType.type.E.ToString().ToUpper() == stringType.ToUpper()) return CharacterType.type.E;
        else if (CharacterType.type.E_S.ToString().ToUpper() == stringType.ToUpper()) return CharacterType.type.E_S;
        else if (CharacterType.type.S.ToString().ToUpper() == stringType.ToUpper()) return CharacterType.type.S;
        else if (CharacterType.type.S_S.ToString().ToUpper() == stringType.ToUpper()) return CharacterType.type.S_S;
        else return CharacterType.type.None;
    }
}
