﻿using System.Collections.Generic;

namespace Com.Mod.ThumbRun.SignUp.Application
{
	using UnityEngine;
	using System.Collections;
	using Domain;
	using User.Domain;

	public class SignUpService : MonoBehaviour 
	{
		public delegate void SignUpServiceEventHandler();
		public event SignUpServiceEventHandler Prepared;

		[SerializeField]
		private SignUp _signUp;
		[SerializeField]
		private User _user;
        private bool _isPrepared = false;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }





		void Awake ()
		{
			_signUp.Prepared += OnSignPrepared;
		}

		void Start () 
		{
			
		}

		void Update () 
		{
		
		}

		void OnDestroy ()
		{
			_signUp.Prepared -= OnSignPrepared;
		}



		private void OnSignPrepared()
		{
            _isPrepared = true;
            if (Prepared != null) Prepared ();
		}



		public List<string> GetCountryCodes()
		{
			return _signUp.GetCountryCodes ();
		}

		public void SignUp(string nickname, string countryCode)
		{
			_user.SetNickname (nickname);
			_user.SetCountryCode (countryCode);
		}

        public bool IsCompleted()
        {
            return _signUp.IsCompleted();
        }

        public void Complete()
        {
            _signUp.Complete();
        }

        public int GetChangeCost()
        {
            return _signUp.GetChangeCost();
        }
    }
}