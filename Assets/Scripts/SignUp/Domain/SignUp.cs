﻿using System.Collections.Generic;

namespace Com.Mod.ThumbRun.SignUp.Domain
{
	using GameDataEditor;
	using UnityEngine;
	using System.Collections;

	public class SignUp : MonoBehaviour 
	{
		public delegate void SignUpEventHandler();
		public event SignUpEventHandler Prepared;
		
		private bool _isPrepared = false;
		private GDESignUpData _rawData;
		
		void Start () 
		{
			if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.SignUp_SignUp, out _rawData))
			{
				Debug.Log("User Data Prepared");

				_isPrepared = true;
				if (Prepared != null) Prepared();
                //_rawData.ResetAll(); // 지울것
			}
			else
			{
				Debug.LogError("Read Error User Data");
			}
		}

		void Update () 
		{
		
		}



		public List<string> GetCountryCodes()
		{
			return _rawData.countryCodes;
		}

        public bool IsCompleted()
        {
            return _rawData.isCompleted;
        }

        public void Complete()
        {
            _rawData.isCompleted = true;
        }

        public int GetChangeCost()
        {
            return _rawData.ChangeCost;
        }
	}
}