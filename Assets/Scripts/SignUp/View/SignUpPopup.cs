﻿using Com.Mod.ThumbRun.SignUp.Application;

namespace Com.Mod.ThumbRun.SignUp.View
{
    using UnityEngine;
    using System.Collections;
    using User.Application;
    using System.Collections.Generic;
    using DG.Tweening;
    using Com.Mod.ThumbRun.Controller;
    using Com.Mod.ThumbRun.UI.Popup;
    using Util;

    public class SignUpPopup : BasicPopup 
	{
		[SerializeField]
		private SignUpService _signUpService;
        [SerializeField]
        private SignUpResultBox _signUpResultBox;
        [SerializeField]
		private GameObject _flagPrefab;
		[SerializeField]
		private GameObject _flagContainer;
        [SerializeField]
        private UIInput _input;
        [SerializeField]
        private UIDynamicButton _okButton;
        [SerializeField]
        private UILabel _okButtonLabel;
        [SerializeField]
        private UIGold _okButtonUiGold;
        [SerializeField]
        private UISprite _selectionBg;
        [SerializeField]
        private UserService _userService;
        [SerializeField]
        private UIBasicButton _closeButton;
        [SerializeField]
        private SettingMenuPopup _settingPopup;
        [SerializeField]
        private UIScrollView _uiScrollView;
        [SerializeField]
        private UISprite _marginImage;
        private bool _isCloseable = false;

		private bool _isDebug = true;
        private string _selectedCountryCode = "000";
        private Dictionary<string, GameObject> flagButtons = new Dictionary<string, GameObject>();

        void Awake ()
		{
			_signUpService.Prepared += OnSignUpServicePrepared;
            _okButton.Click += OnOkButtonClick;
            transform.localPosition = Vector2.zero;
		}

		void Start () 
		{
		
		}

		void Update () 
		{
		}

		void OnDestroy()
		{
			_signUpService.Prepared -= OnSignUpServicePrepared;
            _okButton.Click -= OnOkButtonClick;
        }



		private void OnSignUpServicePrepared()
		{
            List<string> countryCodes = _signUpService.GetCountryCodes();
            int countryCodeCount = countryCodes.Count;
			if (_isDebug) Debug.Log("country code count: " + countryCodeCount);

            for (int i = 0; i < countryCodeCount; i++) {
				GameObject instance = GameObject.Instantiate (_flagPrefab);

                if (i == 0)
                {
                    _marginImage.transform.parent = instance.transform;
                    _marginImage.transform.localScale = Vector3.one;
                    _marginImage.transform.localPosition = new Vector3(-50f, 0f, 0f);
                    //_uiScrollView.transform.localPosition = new Vector3(20f, -20, 0f);
                }

                FlagButton uiBasicButton = instance.GetComponent<FlagButton>();
                uiBasicButton.Init(countryCodes[i]);
                uiBasicButton.On_PressFlagButton += OnFlagButtonPress;
                uiBasicButton.GetComponent<UIDragScrollView>().scrollView = _uiScrollView;

                instance.transform.localPosition = new Vector3(120f * i + 10, 0f, 0f);
                instance.transform.parent = _flagContainer.transform;
				instance.transform.localScale = Vector2.one;

				UISprite uiSprite = instance.GetComponent<UISprite>();
                uiSprite.spriteName = "Flag_" + countryCodes[i];
				
				if(countryCodes[i] == "688")
				{
					Debug.Log("has 688 country code");
				}

                flagButtons.Add(countryCodes[i], instance);
			}

            if (_signUpService.IsCompleted())
            {
                ClosePopup();
                return;
            }
            else
            {
                OpenPopup(false);
            }
        }

        private void OnFlagButtonPress(string code)
        {
            List<string> countryCodes = _signUpService.GetCountryCodes();
            int countryCodeCount = countryCodes.Count;
            for (int i = 0; i < countryCodeCount; i++)
            {
                if (countryCodes[i] == code)
                {
                    _selectionBg.gameObject.SetActive(true);
                    _selectionBg.transform.parent = flagButtons[code].transform;
                    _selectionBg.transform.localScale = Vector2.one;
                    _selectionBg.transform.localPosition = Vector2.zero;
                    _selectionBg.depth = 3;

                    _selectedCountryCode = code;
                }
            }
        }

        private void OnOkButtonClick(GameObject target)
        {
            if (_input.value.Length < 3 || _input.value.Length > 15)
            {
                _signUpResultBox.Show(SignUpResultBox.type.NICKNAME_LENGTH_ERROR);
                return;
            }
            
            if(_selectedCountryCode == "000")
            {
                _signUpResultBox.Show(SignUpResultBox.type.COUNTRY_SELECTION_ERROR);
                return;
            }

            if (_isCloseable)
            {
                if(!_userService.Spend(_signUpService.GetChangeCost()))
                {
                    return;
                }
            }

            _userService.SetNickname(_input.value);
            _userService.SetCountryCode(_selectedCountryCode);
            _signUpService.Complete();
            ClosePopup();
        }



        public void OpenPopup(bool isCloseable)
        {
            _isCloseable = isCloseable;
            if (_isCloseable)
            {
                _closeButton.gameObject.SetActive(true);
                _okButtonLabel.transform.localPosition = new Vector3(0f, 8f, 0f);
                _okButtonLabel.gameObject.SetActive(false);
                _okButtonUiGold.gameObject.SetActive(true);
                _okButtonUiGold.SetGold(_signUpService.GetChangeCost());
                _okButtonUiGold.transform.localPosition = new Vector3(0f, 0f, 0f);
            }
            else
            {
                _closeButton.gameObject.SetActive(false);
                _okButtonLabel.transform.localPosition = new Vector3(0f, 0f, 0f);
                _okButtonLabel.gameObject.SetActive(true);
                _okButtonUiGold.gameObject.SetActive(false);
            }
            base.SetCloseable(isCloseable);
            base.Open();
        }

        public void ClosePopup()
		{
            if(_isCloseable) _settingPopup.OpenPopup();
            base.SetCloseable(true);
            base.Close();
		}
	}
}