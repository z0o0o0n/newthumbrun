﻿namespace Com.Mod.ThumbRun.SignUp.View
{
    using System.Collections;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.Coupon.Domain;
    using UnityEngine;
    using DG.Tweening;
    using Domain;

    public class SignUpResultBox : MonoBehaviour
    {
        public enum type { COUNTRY_SELECTION_ERROR, NICKNAME_LENGTH_ERROR };

        [SerializeField]
        private UILabel _countrySelectionErrorLabel;
        [SerializeField]
        private UILabel _nicknameLengthErrorLabel;
        [SerializeField]
        private UISprite _bg;
        [SerializeField]
        private Color _successBgColor;
        [SerializeField]
        private Color _failBgColor;


        void Start()
        {
            Hide();
        }

        void Update()
        {
            // if(Input.GetKeyUp(KeyCode.Q))
            // {
            // 	Show(CouponResultBox.result.SUCCESS);
            // }
            // if(Input.GetKeyUp(KeyCode.W))
            // {
            // 	Show(CouponResultBox.result.FAIL);
            // }
            // if(Input.GetKeyUp(KeyCode.E))
            // {
            // 	Hide();
            // }
        }




        public void Show(type resultType)
        {
            HideAll();

            _bg.color = _failBgColor;
            _bg.gameObject.SetActive(true);

            if (resultType == type.COUNTRY_SELECTION_ERROR)
            {
                _countrySelectionErrorLabel.gameObject.SetActive(true);
                _bg.color = _failBgColor;
            }
            else if (resultType == type.NICKNAME_LENGTH_ERROR)
            {
                _nicknameLengthErrorLabel.gameObject.SetActive(true);
                _bg.color = _failBgColor;
            }

            DOTween.Kill("SignUpResultBoxAutoHide." + GetInstanceID());
            DOVirtual.DelayedCall(5f, Hide).SetId("SignUpResultBoxAutoHide." + GetInstanceID());
        }

        public void Hide()
        {
            DOTween.Kill("SignUpResultBoxAutoHide." + GetInstanceID());
            HideAll();
            _bg.gameObject.SetActive(false);
        }




        private void HideAll()
        {
            _countrySelectionErrorLabel.gameObject.SetActive(false);
            _nicknameLengthErrorLabel.gameObject.SetActive(false);
        }
    }
}