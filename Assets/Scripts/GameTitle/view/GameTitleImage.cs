﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTitleImage : MonoBehaviour
{
    [SerializeField]
    private UIWidget _widget;

	void Start ()
    {
        float scale = UIScreen.instance.GetHeight() / 640f;
        _widget.height = UIScreen.instance.GetHeight();
        _widget.width = (int)(_widget.width * scale);
    }
	
	void Update ()
    {
		
	}
}
