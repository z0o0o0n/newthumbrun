﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MainLoader : MonoBehaviour
{
    public float minimumLoadingTime = 2f; // Content Load가 완료되어도 최소 이시간 동안은 Loading 페이지를 노출 함.

    private bool _isLoading = false;
    private AsyncOperation _asyncOperation;
    private string _sceneName;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (_asyncOperation != null)
        {
            //Debug.Log("------------ _asyncOperation.isDone : " + _asyncOperation.isDone);
        }
    }

    public void Load(string sceneName)
    {
        _sceneName = sceneName;
        transform.DOMoveX(0, minimumLoadingTime).OnComplete(OnCompleteLoadingMotion);
    }

    private void OnCompleteLoadingMotion()
    {
        if (!_isLoading) LoadLevel();

        //Debug.Log("------------ _asyncOperation.isDone : " + _asyncOperation.isDone);


        //if (!_asyncOperation.isDone)
        //{
        //    Debug.Log("아직 로드 안됨");
        //}
    }

    private void LoadLevel()
    {
        _isLoading = true;
        _asyncOperation = Application.LoadLevelAsync(SceneName.MAIN);
    }

    void OnDestroy()
    {
    }
}
