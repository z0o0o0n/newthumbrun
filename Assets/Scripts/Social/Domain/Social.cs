﻿namespace Com.Mod.ThumbRun.SocialManager.Domain
{
    using System.Collections;
    using System.Collections.Generic;
    using GooglePlayGames;
    using UnityEngine;

    public class SocialManager : MonoBehaviour 
	{
		private static SocialManager _instance;
		private bool _isAuthenticated = false;

		public static SocialManager instance
		{
			get{ return _instance; }
		}

		void Start () 
		{
			if(_instance != null)
			{
				_instance = GetComponent<SocialManager>();
				DontDestroyOnLoad(gameObject);

				#if UNITY_ANDROID
					PlayGamesPlatform.Activate();
					Social.localUser.Authenticate(OnAuthenticated);
				#elif UNITY_IPHONE
				#endif
			}
			else
			{
				Destroy(gameObject);
			}
		}
		
		void Update () 
		{
			
		}




		private void OnAuthenticated(bool success)
		{
			_isAuthenticated = true;
		}

		private void OnAchievementReportCompleted(bool success)
		{
		}

		private void OnGoldBadgeScorePostCompleted(bool success)
		{
		}




		public void ReportAchievement(string id)
		{
			Social.ReportProgress(id, 100.0f, OnAchievementReportCompleted);
		}

		public void PostGoldBadgeToLeaderBoard(int goldBadgeAmount)
		{
			Social.ReportScore(goldBadgeAmount, GPGSIds.leaderboard_rank_of_earned_golds, OnGoldBadgeScorePostCompleted);
		}
	}
}