﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAutoResizer : MonoBehaviour
{
	void Start ()
    {
        InitSize();
    }

    private void Update()
    {
        UIWidget uiSprite = GetComponent<UIWidget>();
        uiSprite.width = UIScreen.instance.GetWidth();
        //uiSprite.height = UIScreen.instance.GetHeight();
    }

    private void InitSize()
    {
        UIWidget uiSprite = GetComponent<UIWidget>();
        uiSprite.width = UIScreen.instance.GetWidth();
        //uiSprite.height = UIScreen.instance.GetHeight();
    }
}
