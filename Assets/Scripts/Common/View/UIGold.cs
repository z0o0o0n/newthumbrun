﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGold : MonoBehaviour
{
    [SerializeField]
    private UISprite _gold;
    [SerializeField]
    private UILabel _label;

	void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}




    private void Replace()
    {
        int goldRightMargin = 3;
        int contentWidth = _gold.width + _label.width + goldRightMargin;

        float goldStartPoint = -contentWidth / 2;
        Vector2 goldPos = _gold.transform.localPosition;
        goldPos.x = goldStartPoint;
        _gold.transform.localPosition = goldPos;

        float labelStartPoint = _gold.transform.localPosition.x + _gold.width + goldRightMargin;
        Vector2 labelPos = _label.transform.localPosition;
        labelPos.x = labelStartPoint;
        _label.transform.localPosition = labelPos;
    }




    public void SetGold(int goldAmount)
    {
        _label.text = String.Format("{0:##,##}", goldAmount);

        Replace();
    }
    
    public void SetValue(string text)
    {
        _label.text = text;

        Replace();
    }
}
