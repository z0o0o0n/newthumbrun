﻿namespace Com.Mod.ThumbRun.Common.View
{
    using UnityEngine;
    using System.Collections;
    using DG.Tweening;

    public class BackLight : MonoBehaviour
    {
        private bool _isEnabled = false;

        void Start()
        {

        }

        void Update()
        {
            if(_isEnabled)
            {
                transform.Rotate(0f, 0f, 0.5f);
            }
        }

        void OnEnable()
        {
            _isEnabled = true;
        }

        void OnDisable()
        {
            _isEnabled = false;
        }
    }
}