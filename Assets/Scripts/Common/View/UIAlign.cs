﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAlign : MonoBehaviour
{
    public delegate void UIAlignEvent();
    public event UIAlignEvent Replaced;

    public enum direction { Horizontal, Vertical };
    public enum align { Left, Center, Right };
    public enum elementType { Sprite, LocalizeText, Label };

    [SerializeField]
    private UIAlign.direction _direction = direction.Horizontal;
    [SerializeField]
    private UIAlign.align _align = align.Center;
    [SerializeField]
    private List<Element> _uiElements;

    private void Awake()
    {
        for (int i = 0; i < _uiElements.Count; i++)
        {
            if (_uiElements[i].type == elementType.LocalizeText)
            {
                NGUILocalizeText localizeText = (NGUILocalizeText)_uiElements[i].ui.GetComponent<NGUILocalizeText>();
                localizeText.Changed += OnLocalizeTextChanged;
            }
            else if (_uiElements[i].type == elementType.Label)
            {
                UILabel label = (UILabel)_uiElements[i].ui;
                label.onChange += OnLocalizeTextChanged;
            }
        }
    }

    void Start ()
    {
        Replace();
    }
	
	void Update ()
    {
		
	}

    private void OnDestroy()
    {
        for (int i = 0; i < _uiElements.Count; i++)
        {
            if (_uiElements[i].type == elementType.LocalizeText)
            {
                NGUILocalizeText localizeText = (NGUILocalizeText)_uiElements[i].ui.GetComponent<NGUILocalizeText>();
                localizeText.Changed -= OnLocalizeTextChanged;
            }
        }
    }




    private void OnLocalizeTextChanged()
    {
        Replace();
    }




    public int GetContentWidth()
    {
        float width = 0;
        for (int i = 0; i < _uiElements.Count; i++)
        {
            width += _uiElements[i].ui.width + _uiElements[i].leftMargin;
        }
        return (int)width;
    }




    public void Replace()
    {
        if (_direction == direction.Horizontal)
        {
            int contentWidth = GetContentWidth();

            if(_align == align.Center)
            {
                float startPosX = -contentWidth / 2;
                for (int i = 0; i < _uiElements.Count; i++)
                {
                    if(i == 0)
                    {
                        Vector2 pos = _uiElements[i].ui.transform.localPosition;
                        pos.x = startPosX + _uiElements[i].leftMargin;
                        _uiElements[i].ui.transform.localPosition = pos;
                    }
                    else
                    {
                        float posX = _uiElements[i - 1].ui.transform.localPosition.x + _uiElements[i - 1].ui.width;
                        Vector2 pos = _uiElements[i].ui.transform.localPosition;
                        pos.x = posX + _uiElements[i].leftMargin;
                        _uiElements[i].ui.transform.localPosition = pos;
                    }
                }
            }
        }
        else if(_direction == direction.Vertical)
        {

        }

        if (Replaced != null) Replaced();
    }
}

[System.Serializable]
class Element
{
    public UIAlign.elementType type;
    public UIWidget ui;
    public float leftMargin;
}