﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VersionText : MonoBehaviour 
{
	[SerializeField]
	private UILabel _label;

	void Start () 
	{
		_label.text = "ver." + Application.version.ToString();
	}
	
	void Update () 
	{
		
	}
}
