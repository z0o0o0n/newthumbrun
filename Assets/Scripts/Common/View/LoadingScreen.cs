﻿namespace Com.Mod.ThumbRun.Common.View
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class LoadingScreen : MonoBehaviour 
	{
		[SerializeField]
		private OfferWallService _offerWallService;

		void Awake ()
		{
			_offerWallService.Prepared += OnOfferWallServicePrepared;
			_offerWallService.OfferWallLoadStart += OnOfferWallLoadStart;
			_offerWallService.OfferWallLoadStart += OnOfferWallLoadCompleted;
			_offerWallService.Closed += OnClosed;
            _offerWallService.Error += OnError;


            transform.localPosition = Vector2.zero;
			gameObject.SetActive(false);
		}

		void Start () 
		{
			
		}
		
		void Update () 
		{
			
		}

		void OnDestroy()
		{
			_offerWallService.Prepared -= OnOfferWallServicePrepared;
			_offerWallService.OfferWallLoadStart -= OnOfferWallLoadStart;
			_offerWallService.OfferWallLoadStart -= OnOfferWallLoadCompleted;
			_offerWallService.Closed -= OnClosed;
            _offerWallService.Error -= OnError;
        }




		private void OnOfferWallServicePrepared()
		{
			
		}

		private void OnOfferWallLoadStart()
		{
			gameObject.SetActive(true);
		}

		private void OnOfferWallLoadCompleted()
		{
			
		}

		private void OnClosed()
		{
			gameObject.SetActive(false);
		}

        private void OnError()
        {
            gameObject.SetActive(false);
        }

    }
}