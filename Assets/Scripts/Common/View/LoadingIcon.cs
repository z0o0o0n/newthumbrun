﻿namespace Com.Mod.ThumbRun.Common.View
{
	using UnityEngine;
	using System.Collections;

	public class LoadingIcon : MonoBehaviour 
	{
        [SerializeField]
        private bool _autoPlay = false;
        private bool _isPlay = false;
		
		void Start () 
		{
            if (_autoPlay) Play();
		}

		void Update () 
		{
			if(_isPlay) transform.Rotate (new Vector3 (0f, 0f, -5f));
		}



		public void Play()
		{
			_isPlay = true;
		}

		public void Stop()
		{
			_isPlay = false;
		}
	}
}