﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAlignBg : MonoBehaviour
{
    [SerializeField]
    private UIAlign _uiAlign;
    [SerializeField]
    private UISprite _uiSprite;
    [SerializeField]
    private int _leftMargin;
    [SerializeField]
    private int _rightMargin;

    private void Awake()
    {
        _uiAlign.Replaced += OnReplaced;
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    private void OnDestroy()
    {
        _uiAlign.Replaced -= OnReplaced;
    }





    private void OnReplaced()
    {
        _uiSprite.width = _uiAlign.GetContentWidth() + _leftMargin + _rightMargin;
    }
}
