﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using Com.Mod.ThumbRun.Setting.Application;

public class BgmManager : MonoBehaviour 
{
    [SerializeField]
    private List<AudioClip> _bgmList;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private float _volumeChangeDuraction = 0.5f;
    private bool _isOn = false;
    //private SettingService _settingService;

    void Awake()
    {
    }

    void Update () 
	{
	
	}

    private void OnDestroy()
    {
    }





    public void SetOnOff(bool isOn)
    {
        _isOn = isOn;
    }

    public void Play(int index)
    {
        if(index > _bgmList.Count - 1) index = _bgmList.Count - 1;
        _audioSource.clip = _bgmList[index];

        if (_isOn)
        {
            _audioSource.Play();
            _audioSource.volume = 0;
            DOTween.Kill("BgmVolumeTween");
            DOTween.To(() => _audioSource.volume, x => _audioSource.volume = x, 0.8f, _volumeChangeDuraction).SetId("BgmVolumeTween").SetUpdate(true);
        }
    }

    public void Resume()
    {
        _audioSource.Play();
    }

    public void Pause()
    {
        _audioSource.Pause();
    }

    public void Stop()
    {
        DOTween.Kill("BgmVolumeTween");
        DOTween.To(() => _audioSource.volume, x => _audioSource.volume = x, 0f, _volumeChangeDuraction).SetId("BgmVolumeTween").SetUpdate(true).OnComplete(OnStoped);
    }

    public void SlowMode()
    {
        DOTween.Kill("BgmPitch." + GetInstanceID());
        AudioSource bgmAudioSource = gameObject.GetComponent<AudioSource>();
        DOTween.To(() => bgmAudioSource.pitch, x => bgmAudioSource.pitch = x, 0.3f, 1f).SetId("BgmPitch." + GetInstanceID());
    }

    public void NormalMode()
    {
        DOTween.Kill("BgmPitch." + GetInstanceID());
        AudioSource bgmAudioSource = gameObject.GetComponent<AudioSource>();
        DOTween.To(() => bgmAudioSource.pitch, x => bgmAudioSource.pitch = x, 1f, 0.5f).SetId("BgmPitch." + GetInstanceID());
    }

    private void OnStoped()
    {
        _audioSource.Stop();
    }
}
