﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Controller;

public class ItemManager : MonoBehaviour 
{
    public event ItemManagerEvent.ItemManagerEventHandler ItemGot;
    public event ItemManagerEvent.ItemManagerEventHandler ItemDestroyed;

    private IItem _currentItem;
    private int _currentItemID = -1; // -1: 아이템 없음
    [SerializeField]
    private PlayerManager _playerManager;
    [SerializeField]
    private RaceParticipationManager _raceParticipationManager;
    private float _missileAdditionalSpeed = 0f;
    private float _missileAdditionalPower = 0f;
    private float _bombAdditionalAttackArea = 0f;
    private float _bombAdditionalPower = 0f;
    private float _shieldAdditionalActivationTime = 0f;
    private float _trapAdditionalSize = 0f;
    private float _trapAdditionalPreTime = 0f;
    private float _trapAdditionalPower = 0f;
    private float _boostAdditionalTime = 0f;
    private float _boostSAdditionalTime = 0f;

    void Start () 
    {
	}
	
	void Update () 
    {
    }



    public bool SetItem(IItem item)
    {
        if(HasItem()) return false;

        if(!HasItem())
        {
            _currentItem = item;
            _currentItemID = item.id;
            GameObject itemGO = ((MonoBehaviour)item).gameObject; 
            itemGO.transform.parent = _playerManager.transform;
            itemGO.transform.localPosition = Vector3.zero;
            itemGO.transform.localScale = Vector3.one;

            if (item.id == 0)
            {
                Boost boost = (Boost)item;
                boost.SetBoostAdditionalTime(_boostAdditionalTime);
            }
            else if(item.id == 1)
            {
                BoostS boostS = (BoostS)item;
                boostS.SetBoostAdditionalTime(_boostSAdditionalTime);
            }
            else if (item.id == 3)
            {
                ShieldS shield = (ShieldS)item;
                shield.SetAdditionalShieldDuration(_shieldAdditionalActivationTime);
            }
            else if (item.id == 4)
            {
                Missile missile = (Missile)item;
                missile.SetAdditionalSpeed(_missileAdditionalSpeed);
                missile.SetAdditionalPower(_missileAdditionalPower);
            }
            else if(item.id == 5)
            {
                Bomb bomb = (Bomb)item;
                bomb.SetAdditionalAttackArea(_bombAdditionalAttackArea);
                bomb.SetAdditionalPower(_bombAdditionalPower);
            }
            else if(item.id == 6)
            {
                Trap trap = (Trap)item;
                trap.SetAdditionalPreTime(_trapAdditionalPreTime);
                //trap.SetAdditionalSize(_trapAdditionalSize);
                trap.SetAdditionalPower(_trapAdditionalPower);
            }

            if (ItemGot != null) ItemGot(item.id);
        }
        return true;
    }

    public void UseItem()
    {
        Debug.Log("----------!!!!!!!!!! user Item: swoon: " + _playerManager.IsSwoon());
        //if (_playerManager.IsSwoon()) return;
        _currentItem.Use(_playerManager, _raceParticipationManager.GetReplayerList());
        _currentItemID = -1;
    }

    public void DestroyItem()
    {
        if(ItemDestroyed != null) ItemDestroyed(_currentItemID);
        
        Destroy((MonoBehaviour)_currentItem);
        _currentItem = null;
        _currentItemID = -1;
    }



    public bool HasItem()
    {
        bool result;
        if (_currentItemID == -1) result = false;
        else result = true;
        return result;
    }



    // Missile
    public void SetMissileAdditionalSpeed(float additionalSpeed)
    {
        _missileAdditionalSpeed = additionalSpeed;
    }

    public void SetMissileAdditionalPower(float additionalPower)
    {
        _missileAdditionalPower = additionalPower;
    }

    // Bomb
    public void SetBombAdditionalAttackArea(float additionalAttackArea)
    {
        _bombAdditionalAttackArea = additionalAttackArea;
    }

    public void SetBombAdditionalPower(float additionalPower)
    {
        _bombAdditionalPower = additionalPower;
    }

    // Shield
    public void SetShieldAdditionalActivationTime(float additionalActivationTime)
    {
        _shieldAdditionalActivationTime = additionalActivationTime;
    }

    // Trap
    public void SetTrapAdditionalSize(float additionalSize)
    {
        _trapAdditionalSize = additionalSize;
    }

    public void SetTrapAdditionalPreTime(float additionalPreTime) // 준비시간 짧아짐
    {
        _trapAdditionalPreTime = additionalPreTime;
    }

    public void SetTrapAdditionalPower(float additionalPower)
    {
        _trapAdditionalPower = additionalPower;
    }

    // Boost
    public void SetBoostAdditionalTime(float additionalTime)
    {
        _boostAdditionalTime = additionalTime;
    }

    public void SetBoostSAdditionalTime(float additionalTime)
    {
        _boostSAdditionalTime = additionalTime;
    }
}
