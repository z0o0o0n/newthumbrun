﻿using UnityEngine;
using System.Collections;

public class CollectionBackButtonManager : MonoBehaviour 
{
    public CollectionScene collectionScene;

    void Awake ()
    {
        collectionScene.ui.On_PressLevelUpPointButton += OnPressLevelUpPointButton;
        collectionScene.ui.On_ClosedBonusAbilityPopup += OnClosedBonusAbilityPopup;
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            //if(GameManager.state == GameState.state.COLLECTION)
            //{
            //    collectionScene.GoHome();
            //    GameManager.state = GameState.state.NONE;
            //}
            //else if (GameManager.state == GameState.state.UPGRADE_POPUP)
            //{
            //    GameManager.state = GameState.state.NONE;
            //}
        }
	}

    void OnDestroy ()
    {
        collectionScene.ui.On_PressLevelUpPointButton -= OnPressLevelUpPointButton;
        collectionScene.ui.On_ClosedBonusAbilityPopup -= OnClosedBonusAbilityPopup;
    }

    private void OnPressLevelUpPointButton()
    {
        //GameManager.state = GameState.state.UPGRADE_POPUP;
    }

    private void OnClosedBonusAbilityPopup()
    {
        //GameManager.state = GameState.state.COLLECTION;
    }

    private void OnBonusAbilityPopupClosed()
    {
        //GameManager.state = GameState.state.COLLECTION;
    }
}
