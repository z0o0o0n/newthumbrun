﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.UI.Popup;

public class LobbyBackButtonManager : MonoBehaviour 
{
    [SerializeField]
    private BasicPopup _exitPopup;
    private static BasicPopup _currentPopup;

    void Awake ()
    {
    }

	void Start () 
	{
	
	}

    void OnDestroy()
    {
    }
	
	void Update () 
	{
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if(_currentPopup == null)
            {
                _exitPopup.Open();
                LobbyBackButtonManager.SetCurrentPopup(_exitPopup);
            }
            else
            {
                if(_currentPopup.isCloseable)
                {
                    _currentPopup.Close();
                    LobbyBackButtonManager.SetCurrentPopup(null);
                }
            }
        }
	}

    public static void SetCurrentPopup(BasicPopup popup)
    {
        _currentPopup = popup;
    }
}
