﻿using UnityEngine;
using System.Collections;

public class ADManager : MonoBehaviour
{
    //public delegate void ADManagerResultEvent(ShowResult result);
    //public event ADManagerResultEvent On_CompletedAD;

    [Header("Default AD")]
    public int frequency = 5; // 광고 노출 빈도: (0 노출안함), (1 매번 노출), (2~.. 2번 실행 시 1번 호출)

    [Header("Reward AD")]
    //private int callCount = 0;
    //private ShowOptions defaultAdOption;
    //private ShowOptions rewardAdOption;
    private bool _hasRewardAdDebug = false;

    private bool _isDebug = true;
    private string _logPrefix = "ADManager - ";

    void Awake()
    {
        //DontDestroyOnLoad(gameObject);

        //if (Advertisement.isSupported)
        //{
        //    string unityAdId;
            
        //    // 유니티 AD OS 및 버전별 분기
        //    #if UNITY_IOS
        //        //if (GameManager.isFreeVersion) unityAdId = "112892";
        //        unityAdId = "78171";

        //    #elif UNITY_ANDROID
        //        unityAdId = "104737";
        //    #endif

        //    Advertisement.Initialize(unityAdId);
        //}
        //else {
        //    Debug.Log(_logPrefix + "Awake() / Platform not supported");
        //}

        //defaultAdOption = new ShowOptions();
        //defaultAdOption.resultCallback = ADCallbackHandler;

        //rewardAdOption = new ShowOptions();
        //rewardAdOption.resultCallback = ADCallbackHandler;
    }

    void Update()
    {
        // 메모리 해킹으로 값이 조작될 경우 강제로 다시 변경 함.
        if(frequency != 5) frequency = 5; // 광고 반복 주기
        // if(GameData.instance.GetPurchaseData().isNoAdsPurchased) GameData.instance.GetPurchaseData().isNoAdsPurchased = false; // No-ADs 결제 여부
    }

    #region < Default AD >
    public bool ShowDefaultAD()
    {
        //     // No Ads 상품 구입여부 확인
        //     if (_isDebug) Debug.Log(_logPrefix + "ShowDefaultAD() / isNoAdsPurchased: " + GameData.instance.GetPurchaseData().isNoAdsPurchased);
        //     if (GameData.instance.GetPurchaseData().isNoAdsPurchased) return false;

        //     // 광고 노출 주기(frequency) 확인
        //     if (_isDebug) Debug.Log(_logPrefix + "ShowDefaultAD() / frequency: " + frequency + "\n racecount: " + GameData.instance.GetConfigData().totalRaceCount);
        //     if (frequency == 0) return false;

        //     if (GameData.instance.GetConfigData().totalRaceCount % (frequency * 2) == 0)
        //     {
        //         if (!Advertisement.IsReady("forcedAds"))
        //         {
        //             if (_isDebug) Debug.Log(_logPrefix + "ShowDefaultAD() / forcedAds isReady: false");
        //             return false;
        //         }
        //         Advertisement.Show("forcedAds", defaultAdOption);
        //         return true;
        //     }
        //     else if (GameData.instance.GetConfigData().totalRaceCount % frequency == 0)
        //     {
        //         // 광고 준비 여부 확인
        //         if (!Advertisement.IsReady("defaultZone"))
        //         {
        //             if (_isDebug) Debug.Log(_logPrefix + "ShowDefaultAD() / defaultZone isReady: false");
        //             return false;
        //         }
        //Advertisement.Show("defaultZone", defaultAdOption);
        //         return true;
        //     }
        //     else
        //     {
        //         return false;
        //     }
        return false;
    }
    #endregion

    public bool ShowRewardAD()
    {
        //#if UNITY_EDITOR
        ////StartCoroutine(WaitForAd());
        //#endif
        //if (_isDebug) Debug.Log("ADManager - ShowRewardAD()");
        //Advertisement.Show("rewardedVideoZone", rewardAdOption);
        return true;
    }

    public bool HasRewardAD(bool isDebug = false)
    {
   //     bool result = false;
   //     if (isDebug)
   //     {
   //         //Debug.Log("광고 테스트 모드다");
   //         result = _hasRewardAdDebug;
   //     }
   //     else
   //     {
   //         //Debug.Log("광고 실제 모드다");
			//result = Advertisement.IsReady("rewardedVideoZone");
   //     }
   //     //if (_isDebug) Debug.Log("ADManager - HasRewardAD() / result: " + result);
        return false;
    }

    // 검증 안됨
    //IEnumerator WaitForAd()
    //{
    //    float currentTimesScale = Time.timeScale;
    //    Time.timeScale = 0f;
    //    yield return null;

    //    while (Advertisement.isShowing)
    //        yield return null;

    //    Time.timeScale = currentTimesScale;
    //}

    //private void ADCallbackHandler(ShowResult result)
    //{
    //    if (_isDebug) Debug.Log("ADManager - ADCallbackHandler() / result: " + result);
    //    //switch(result)
    //    //{
    //    //    case ShowResult.Failed:
    //    //        break;
    //    //    case ShowResult.Skipped:
    //    //        break;
    //    //    case ShowResult.Finished:
    //    //        break;
    //    //}

    //    if (On_CompletedAD != null) On_CompletedAD(result);
    //}

    public void SetRewardAdDebug(bool hasRewardAdDebug)
    {
        _hasRewardAdDebug = hasRewardAdDebug;
    }
}
