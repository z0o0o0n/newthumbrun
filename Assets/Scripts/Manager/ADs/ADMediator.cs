﻿using UnityEngine;
using System.Collections;

public class ADMediator : MonoBehaviour 
{
    public delegate void ADMediatorEvent(bool isSucceeded);
    public event ADMediatorEvent VideoLoaded;
    public event ADMediatorEvent VideoFinished;

    public bool isReady = false;

    private bool _isDebug = true;
    private string _logPrefix = "ADMediator - ";

    void Awake ()
    {
        DontDestroyOnLoad(gameObject);
        //AdMediation.Instance.OnVideoLoadComplete += OnVideoLoadComplete;
        //AdMediation.Instance.OnVideoFinished += OnVideoFinished;
        //AdMediation.Instance.OnVideoLeftApplication += OnVideoLeftApplication;
        //AdMediation.Instance.Init();
    }

	void Start () 
	{

	}

    void OnDestroy()
    {
        //if (AdMediation.Instance)
        //{
        //    AdMediation.Instance.OnVideoLoadComplete -= OnVideoLoadComplete;
        //    AdMediation.Instance.OnVideoFinished -= OnVideoFinished;
        //    AdMediation.Instance.OnVideoLeftApplication -= OnVideoLeftApplication;
        //}
    }

    // 광고 로드
    private void LoadVideo()
    {
        if (_isDebug) Debug.Log(_logPrefix + "LoadVideo() / 비디오광고 로드");
        //AdMediation.Instance.LoadVideo();
    }

    //private void OnVideoLoadComplete(M_VideoLoadResult res)
    //{
    //    if (res.IsSucceeded)
    //    {
    //        if (_isDebug) Debug.Log(_logPrefix + "LoadVideo() / 비디오광고 준비됨");
    //        isReady = true;
    //    }
    //    else
    //    {
    //        if (_isDebug) Debug.Log(_logPrefix + "LoadVideo() / 비디오광고 준비 실패, 다시 로드");
    //        AdMediation.Instance.LoadVideo();
    //    }

    //    if (VideoLoaded != null) VideoLoaded(res.IsSucceeded);
    //}

    // 광고 재생
    public void ShowVideo()
    {
        if (isReady)
        {
            if (_isDebug) Debug.Log(_logPrefix + "LoadVideo() / 비디오광고 재생");
            isReady = false;
            //AdMediation.Instance.ShowVideo();
        }
        else
        {
            if (_isDebug) Debug.Log(_logPrefix + "LoadVideo() / 비디오광고 준비안됨");
        }
    }

    //private void OnVideoFinished(M_VideoFinishResult res)
    //{
    //    if (_isDebug) Debug.Log(_logPrefix + "OnVideoFinished() / 비디오광고 재생 완료 / isSucceeded: " + res.IsSucceeded + " / isFailed: " + res.IsFailed);
    //    if (VideoFinished != null) VideoFinished(res.IsSucceeded);

    //    AdMediation.Instance.LoadVideo();
    //}

    //private void OnVideoLeftApplication(M_VideoLeftApplicationResult res)
    //{
    //    if (_isDebug) Debug.Log(_logPrefix + "OnVideoLeftApplication() / 비디오광고 재생 중 게임을 떠남 / isSucceeded: " + res.IsSucceeded + " / isFailed: " + res.IsFailed);
    //}
}
