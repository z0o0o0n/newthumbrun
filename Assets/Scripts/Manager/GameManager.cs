﻿using UnityEngine;
using System.Collections;
using Com.Mod.NGUI.LogConsole;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    /**
     * 모든 Scene에 범용적으로 사용되는 Class들의 참조를 제공함
     * 가급적 각 Scene의 Main Class에서만 GameManager를 참조하는 것이 좋음
     */
    public ADManager adManager;
    public ADMediator adMediator;
    public CharacterDataManager characterDataManager;
    public SceneLoader sceneLoader;

	public static SystemLanguage phoneLanguage;
	//public static bool isFreeVersion = true; // 여기서 속성값을 변경하면 안됨. Awake에서 변경해야 함.
    public static bool useDebugPannel = false;
    //public bool isRandomMap = false;
    public bool isRecordMode = false;
    public static bool isCrossBannerEnded = false;
    //public static GameState.state state = GameState.state.LOADING;
    public static bool isSimpleControlMode = true;
    public static bool isFirstTimeLobbyTemp = true;
    public static string cliendID = "tvUSfDlTmW8awJ1eDBaX7wdIcnFGBVHDzrB1RlMFqfnugrqyjL";
    public static string secretKey = "4WtuH1Obo3159qGKYvOA4f3e0TiykP13";
    //public static int replayDataVer = 7;

    void Awake()
    {
        Application.targetFrameRate = 60;
        DontDestroyOnLoad(gameObject);

        //#if UNITY_IOS
        //    isFreeVersion = true; // iOS는 유/무료 버전이 모두 존재하기 때문에 빌드 시 이 값을 변경해야 함.
        //#elif UNITY_ANDROID
        //    isFreeVersion = true;
        //#endif

		if (Application.systemLanguage == SystemLanguage.Korean)
		{
			GameManager.phoneLanguage = Application.systemLanguage;
		}
		else if (Application.systemLanguage == SystemLanguage.Japanese)
		{
			GameManager.phoneLanguage = Application.systemLanguage;
		}
		else
		{
			GameManager.phoneLanguage = SystemLanguage.English;
		}

        //GameManager.phoneLanguage = SystemLanguage.Korean;
    }
	
	void Start ()
    {
	    
	}

	void Update ()
    {
		// Free
		//if (!isFreeVersion) isFreeVersion = true;

		// Paid
		//if(isFreeVersion) isFreeVersion = false;
	}
    //public int entryIndex
    //{
    //    get
    //    {
    //        int index = currentStageIndex + 1;
    //        if (isRandomMap) index = 0;
    //        return index;
    //    }
    //}
}
