﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using VoxelBusters.Utility;
//using VoxelBusters.NativePlugins;

    public class PurchaseManager : MonoBehaviour
    {
        public delegate void PurchaseManagerEvent(string productId);
        public delegate void RestoringSuccessEvent(List<string> productIds);
        public delegate void RestoringErrorEvent(string error);
        public event PurchaseManagerEvent SuccessPurchase;
        public event PurchaseManagerEvent ErrorPurchase;
        public event RestoringSuccessEvent SuccessRestoring;
        public event RestoringErrorEvent ErrorRestoring;

        private bool _isDebug = true;
        private string _logPrefix = "PurchaseManager - ";
        private int m_productIter;
        //private BillingProduct[] m_products;
        private bool m_productRequestFinished;

        void Start()
        {
            //m_products = NPSettings.Billing.Products;
            //m_productRequestFinished = false;

            //RequestBillingProducts(m_products);
        }

        //void OnEnable()
        //{
        //    Billing.DidFinishRequestForBillingProductsEvent += OnDidFinishRequestForBillingProducts;
        //    Billing.DidFinishProductPurchaseEvent += OnDidFinishProductPurchase;
        //    Billing.DidFinishRestoringPurchasesEvent += OnDidFinishRestoringPurchases;
        //}

        //void OnDisable()
        //{
        //    Billing.DidFinishRequestForBillingProductsEvent -= OnDidFinishRequestForBillingProducts;
        //    Billing.DidFinishProductPurchaseEvent -= OnDidFinishProductPurchase;
        //    Billing.DidFinishRestoringPurchasesEvent -= OnDidFinishRestoringPurchases;
        //}

        //public bool IsAvailable()
        //{
        //    return NPBinding.Billing.IsAvailable();
        //}

        //private void RequestBillingProducts(BillingProduct[] _products)
        //{
        //    NPBinding.Billing.RequestForBillingProducts(_products);
        //}

        //private void RestorePurchases()
        //{
        //    NPBinding.Billing.RestorePurchases();
        //}

        //private bool CanMakePayments()
        //{
        //    return NPBinding.Billing.CanMakePayments();
        //}

        //private void BuyProduct(BillingProduct _product)
        //{
        //    NPBinding.Billing.BuyProduct(_product);
        //}

        //private bool IsProductPurchased(BillingProduct _product)
        //{
        //    return NPBinding.Billing.IsProductPurchased(_product);
        //}

        //private void OnDidFinishRequestForBillingProducts(BillingProduct[] _products, string _error)
        //{
        //    Debug.Log(string.Format("Billing products request finished. Error = {0}.", _error.GetPrintableString()));

        //    if (_products != null)
        //    {
        //        m_productRequestFinished = true;
        //        Debug.Log(string.Format("Totally {0} billing products information were received.", _products.Length));

        //        foreach (BillingProduct _currentProduct in _products)
        //            Debug.Log(_currentProduct.ToString());
        //    }
        //}

        //private void OnDidFinishProductPurchase(BillingTransaction _transaction)
        //{
        //    string log = "";
        //    log += "Received product purchase response.";
        //    log += "\n Product Identifier = " + _transaction.ProductIdentifier;
        //    log += "\n Transaction State = " + _transaction.TransactionState;
        //    log += "\n Verification State = " + _transaction.VerificationState;
        //    log += "\n Transaction Date[UTC] = " + _transaction.TransactionDateUTC;
        //    log += "\n Transaction Date[Local] = " + _transaction.TransactionDateLocal;
        //    log += "\n Transaction Identifier = " + _transaction.TransactionIdentifier;
        //    log += "\n Transaction Receipt = " + _transaction.TransactionReceipt;
        //    log += "\n Error = " + _transaction.Error.GetPrintableString();
        //    Debug.Log(log);

        //    if(_transaction.VerificationState == eBillingTransactionVerificationState.SUCCESS && _transaction.Error == null)
        //    {
        //        if (SuccessPurchase != null) SuccessPurchase(_transaction.ProductIdentifier);
        //    }
        //    else
        //    {
        //        if (ErrorPurchase != null) ErrorPurchase(_transaction.ProductIdentifier);
        //    }
        //}

        //private void OnDidFinishRestoringPurchases(BillingTransaction[] _transactions, string _error)
        //{
        //    Debug.Log(string.Format("Received restore purchases response. Error = {0}.", _error.GetPrintableString()));

        //    if (_error.GetPrintableString() != "NULL")
        //    {
        //        if (ErrorRestoring != null) ErrorRestoring(_error);
        //        return;
        //    }

        //    List<string> productIds = new List<string>();
        //    if (_transactions != null)
        //    {
        //        Debug.Log(string.Format("Count of transaction information received = {0}.", _transactions.Length));

        //        foreach (BillingTransaction _currentTransaction in _transactions)
        //        {
        //            string log = "";
        //            log += "Product Identifier = " + _currentTransaction.ProductIdentifier;
        //            log += "\n Transaction State = " + _currentTransaction.TransactionState;
        //            log += "\n Verification State = " + _currentTransaction.VerificationState;
        //            log += "\n Transaction Date[UTC] = " + _currentTransaction.TransactionDateUTC;
        //            log += "\n Transaction Date[Local] = " + _currentTransaction.TransactionDateLocal;
        //            log += "\n Transaction Identifier = " + _currentTransaction.TransactionIdentifier;
        //            log += "\n Transaction Receipt = " + _currentTransaction.TransactionReceipt;
        //            log += "\n Error = " + _currentTransaction.Error.GetPrintableString();
        //            Debug.Log(log);

        //            if (_currentTransaction.VerificationState == eBillingTransactionVerificationState.SUCCESS && _currentTransaction.Error == null)
        //            {
        //                productIds.Add(_currentTransaction.ProductIdentifier);
        //            }
        //        }

        //        if (SuccessRestoring != null) SuccessRestoring(productIds);
        //    }
        //}

        //#region Misc. Methods

        //private BillingProduct GetCurrentProduct()
        //{
        //    return m_products[m_productIter];
        //}

        //private BillingProduct GetProductById(string id)
        //{
        //    BillingProduct result = null;
        //    for (int i = 0; i < m_products.Length; i++)
        //    {
        //        if (m_products[i].ProductIdentifier == id)
        //        {
        //            result = m_products[i];
        //        }
        //    }
        //    return result;
        //}

        //private void GotoNextProduct()
        //{
        //    m_productIter++;

        //    if (m_productIter >= m_products.Length)
        //        m_productIter = 0;
        //}

        //private void GotoPreviousProduct()
        //{
        //    m_productIter--;

        //    if (m_productIter < 0)
        //        m_productIter = m_products.Length - 1;
        //}

        //private int GetProductsCount()
        //{
        //    return m_products.Length;
        //}

        //#endregion

        //public void BuyNoAds()
        //{
        //    Debug.Log(_logPrefix + "BuyNoAds() / IsAvailable: " + IsAvailable());
        //    if (!IsAvailable()) return;

        //    BillingProduct removeAdProduct = GetProductById("remove_ad");
        //    Debug.Log(_logPrefix + "BuyNoAds() / removeAdProduct: " + removeAdProduct);
        //    if (removeAdProduct == null) return;

        //    Debug.Log(_logPrefix + "BuyNoAds() / CanMakePayments: " + CanMakePayments());
        //    if (!CanMakePayments()) return;

        //    Debug.Log(_logPrefix + "BuyNoAds() / IsProductPurchased: " + IsProductPurchased(removeAdProduct));
        //    if (IsProductPurchased(removeAdProduct)) return;
            
        //    BuyProduct(removeAdProduct);
        //}

        //public void Restore()
        //{
        //    RestorePurchases();
        //}
    }