﻿namespace Com.Mod.ThumbRun.Analytics.Application
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Domain;
    using LuckyChance.Domain;

    public class AnalyticsService : MonoBehaviour
    {
        public delegate void AnalyticsServiceEvent();
        public event AnalyticsServiceEvent Prepared;

        [SerializeField]
        private Analytics _analytics;
        private bool _isPrepared = false;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }





        private void Awake()
        {
            _analytics.Prepared += OnAnalyticsPrepared;
        }

        void Start()
        {

        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _analytics.Prepared -= OnAnalyticsPrepared;
        }





        private void OnAnalyticsPrepared()
        {
            _isPrepared = true;
            _analytics.AddExecuteCount();
        }




        // 실행 수
        public int GetExecuteCount()
        {
            return _analytics.GetExecuteCount();
        }

        // 경기참가 수
        public void AddParticipantCount(int? entryIndex)
        {
            _analytics.AddParticipantCount(entryIndex);
        }

        public int GetspecificParticipantCount(int entryIndex)
        {
            return _analytics.GetspecificParticipantCount(entryIndex);
        }

        public int GetParticipantCount()
        {
            return _analytics.GetParticipantCount();
        }

        // 배지 획득 수
        public void CountBadge(LuckyChanceBadge.badge badge)
        {
            _analytics.CountBadge(badge);
        }

        public int GetTotalBadgeCount()
        {
            return _analytics.GetTotalBadgeCount();
        }

        public int GetSpecificBadgeCount(LuckyChanceBadge.badge badge)
        {
            return _analytics.GetSpecificBadgeCount(badge);
        }

        // 순위
        public void CountRanking(int ranking)
        {
            _analytics.CountRanking(ranking);
        }

        public int GetSuccessionRankingCount()
        {
            return _analytics.GetSuccessionRankingCount();
        }

        public int GetSuccessionFirstRankingCount()
        {
            return _analytics.GetSuccessionFirstRankingCount();
        }

        // 상금 획득 수
        public void CountTotalPrizeGold(int prizeGold)
        {
            _analytics.CountTotalPrizeGold(prizeGold);
        }

        public int GetTotalPrizeGold()
        {
            return _analytics.GetTotalPrizeGold();
        }

        // 적 플레이어에게 입힌 피해
        public void CountKilledPlayerWithBomb()
        {
            _analytics.CountKilledPlayerWithBomb();
        }

        public void CountKilledPlayerWithMissile()
        {
            _analytics.CountKilledPlayerWithMissile();
        }

        public int GetKilledPlayerCountWithBomb()
        {
            return _analytics.GetKilledPlayerCountWithBomb();
        }

        public int GetKilledPlayerCountWithMissile()
        {
            return _analytics.GetKilledPlayerCountWithMissile();
        }

        // 광고 시청 수
        public void CountWatchedAd()
        {
            _analytics.CountWatchedAd();
        }

        public int GetWatchedAdCount()
        {
            return _analytics.GetWatchedAdCount();
        }

        // 경기 별 최고기록
        public void SetBestRecord(string stageId, float record)
        {
            _analytics.SetBestRecord(stageId, record);
        }

        public float GetBestRecord(string stageId)
        {
            return _analytics.GetBestRecord(stageId);
        }
    }
}