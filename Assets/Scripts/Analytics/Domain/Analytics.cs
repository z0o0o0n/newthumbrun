﻿namespace Com.Mod.ThumbRun.Analytics.Domain
{
    using GameDataEditor;
    using LuckyChance.Domain;
    using System.Collections;
    using System.Collections.Generic;
    using TapjoyUnity;
    using UnityEngine;

    public class Analytics : MonoBehaviour
    {
        public delegate void AnalyticsEvent();
        public event AnalyticsEvent Prepared;

        private GDEAnalyticsData _rawData;
        private bool _isPrepared = false;

        private void Start()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Analytics_Analytics, out _rawData))
            {
                _isPrepared = true;
                if (Prepared != null) Prepared();

                //_rawData.ResetAll();
            }
            else
            {
                Debug.LogWarning("Analytics의 GDE data를 읽을 수 없습니다.");
            }

            DontDestroyOnLoad(gameObject);
        }

        void Update()
        {

        }





        public void AddExecuteCount()
        {
            _rawData.executeCount += 1;
        }

        public int GetExecuteCount()
        {
            return _rawData.executeCount;
        }

        public void AddParticipantCount(int? entryIndex)
        {
            if (entryIndex == 0)
            {
                _rawData.participationCount_0 += 1;
                Tapjoy.TrackEvent("Map.Random");
            }
            else if (entryIndex == 1)
            {
                _rawData.participationCount_1 += 1;
                Tapjoy.TrackEvent("Map.PracticeField");
            }
            else if (entryIndex == 2)
            {
                _rawData.participationCount_2 += 1;
                Tapjoy.TrackEvent("Map.SkyCity");
            }
            else if (entryIndex == 3)
            {
                _rawData.participationCount_3 += 1;
                Tapjoy.TrackEvent("Map.FieldOfMonsters");
            }
            else if (entryIndex == 4)
            {
                _rawData.participationCount_4 += 1;
                Tapjoy.TrackEvent("Map.Area52");
            }
            else if (entryIndex == 5)
            {
                _rawData.participationCount_5 += 1;
                Tapjoy.TrackEvent("Map.MoonCave");
            }
            else if (entryIndex == 6)
            {
                _rawData.participationCount_6 += 1;
                Tapjoy.TrackEvent("Map.CoralReefHill");
            }

            _rawData.participationCount += 1;
        }

        public int GetspecificParticipantCount(int entryIndex)
        {
            int result = 0;
            if (entryIndex == 0) result = _rawData.participationCount_0;
            else if (entryIndex == 1) result = _rawData.participationCount_1;
            else if (entryIndex == 2) result = _rawData.participationCount_2;
            else if (entryIndex == 3) result = _rawData.participationCount_3;
            else if (entryIndex == 4) result = _rawData.participationCount_4;
            else if (entryIndex == 5) result = _rawData.participationCount_5;
            else if (entryIndex == 6) result = _rawData.participationCount_6;
            return result;
        }

        public int GetParticipantCount()
        {
            return _rawData.participationCount;
        }



        public void CountTotalPrizeGold(int prizeGold)
        {
            _rawData.totalPrizeGold += prizeGold;
        }

        public int GetTotalPrizeGold()
        {
            return _rawData.totalPrizeGold;
        }



        public void CountBadge(LuckyChanceBadge.badge badge)
        {
            if (badge == LuckyChanceBadge.badge.NONE) return;

            _rawData.badgeCount += 1;

            if (badge == LuckyChanceBadge.badge.GOLD) _rawData.badgeCountGold += 1;
            else if (badge == LuckyChanceBadge.badge.SILVER) _rawData.badgeCountSilver += 1;
            else if (badge == LuckyChanceBadge.badge.BRONZE) _rawData.badgeCountBronze += 1;
        }

        public int GetTotalBadgeCount()
        {
            return _rawData.badgeCount;
        }

        public int GetSpecificBadgeCount(LuckyChanceBadge.badge badge)
        {
            if (badge == LuckyChanceBadge.badge.GOLD) return _rawData.badgeCountGold;
            else if (badge == LuckyChanceBadge.badge.SILVER) return _rawData.badgeCountSilver;
            else if (badge == LuckyChanceBadge.badge.BRONZE) return _rawData.badgeCountBronze;

            return -1;
        }


        // 순위
        public void CountRanking(int ranking)
        {
            // 순위
            if (ranking == 0)
            {
                _rawData.ranking1Count += 1;
            }
            else if (ranking == 1)
            {
                _rawData.ranking2Count += 1;
            }
            else if (ranking == 2)
            {
                _rawData.ranking3Count += 1;
            }
            else
            {
            }

            // 연속 순위
            if(ranking < 3) _rawData.successionRankingCount += 1;
            else _rawData.successionRankingCount = 0;

            // 연속 1위
            if (ranking == 0) _rawData.successionRanking1Count += 1;
            else _rawData.successionRanking1Count = 0;
        }

        public int GetSuccessionRankingCount()
        {
            return _rawData.successionRankingCount;
        }

        public int GetSuccessionFirstRankingCount()
        {
            return _rawData.successionRanking1Count;
        }


        // 적에게 입힌 피해
        public void CountKilledPlayerWithBomb()
        {
            _rawData.killedPlayerCountWithBomb += 1;
        }

        public void CountKilledPlayerWithMissile()
        {
            _rawData.killedPlayerCountWithMissile += 1;
        }

        public int GetKilledPlayerCountWithBomb()
        {
            return _rawData.killedPlayerCountWithBomb;
        }

        public int GetKilledPlayerCountWithMissile()
        {
            return _rawData.killedPlayerCountWithMissile;
        }


        // 광고 시청 수
        public void CountWatchedAd()
        {
            _rawData.adWatchingCount += 1;
        }

        public int GetWatchedAdCount()
        {
            return _rawData.adWatchingCount;
        }

        // 경기 별 최고기록
        public void SetBestRecord(string stageId, float record)
        {
            if(stageId == "0") // 연습경기장
            {
                if(record < _rawData.bestRecord_0) _rawData.bestRecord_0 = record;
            }
            else if (stageId == "1") // 하늘도시
            {
                if (record < _rawData.bestRecord_1) _rawData.bestRecord_1 = record;
            }
            else if (stageId == "2") // 몬스터 필드
            {
                if (record < _rawData.bestRecord_2) _rawData.bestRecord_2 = record;
            }
            else if (stageId == "3") // 52구역
            {
                if (record < _rawData.bestRecord_3) _rawData.bestRecord_3 = record;
            }
            else if (stageId == "4") // 달 동굴
            {
                if (record < _rawData.bestRecord_4) _rawData.bestRecord_4 = record;
            }
            else if (stageId == "5") // 산호초 언덕
            {
                if (record < _rawData.bestRecord_5) _rawData.bestRecord_5 = record;
            }
            else if (stageId == "6")
            {

            }
        }

        public float GetBestRecord(string stageId)
        {
            if (stageId == "0") // 연습경기장
            {
                return _rawData.bestRecord_0;
            }
            else if (stageId == "1") // 하늘도시
            {
                return _rawData.bestRecord_1;
            }
            else if (stageId == "2") // 몬스터 필드
            {
                return _rawData.bestRecord_2;
            }
            else if (stageId == "3") // 52구역
            {
                return _rawData.bestRecord_3;
            }
            else if (stageId == "4") // 달 동굴
            {
                return _rawData.bestRecord_4;
            }
            else if (stageId == "5") // 산호초 언덕
            {
                return _rawData.bestRecord_5;
            }
            else if (stageId == "6")
            {
                return -1;
            }
            else
            {
                return -1f;
            }
        }
    }
}