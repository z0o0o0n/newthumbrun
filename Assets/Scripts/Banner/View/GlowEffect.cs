﻿namespace Com.Mod.ThumbRun.Banner.View
{
    using UnityEngine;
    using System.Collections;
    using DG.Tweening;

    public class GlowEffect : MonoBehaviour
    {
        [SerializeField]
        private Color _padeOutColor;
        private UISprite _image;

        void Start()
        {
            _image = gameObject.GetComponent<UISprite>();
            DOTween.To(() => _image.color, x => _image.color = x, _padeOutColor, 0.5f).SetLoops(-1, LoopType.Yoyo);
        }

        void Update()
        {

        }
    }
}