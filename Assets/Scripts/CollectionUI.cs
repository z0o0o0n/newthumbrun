﻿using UnityEngine;
using System.Collections;

public class CollectionUI : MonoBehaviour
{
    public delegate void CollectionUIEvent();
    public event CollectionUIEvent OnCloseButtonUp;

    private bool _isPressedCloseButton = false;

    void Awake ()
    {
    }

    public void OnButtonUp()
    {
        if(!_isPressedCloseButton)
        {
            _isPressedCloseButton = true;
            if(OnCloseButtonUp != null) OnCloseButtonUp();
        }
    }

	void Start ()
    {
	
	}
	
	void Update ()
    {
	
	}
}
