﻿using UnityEngine;
using System.Collections;
using LitJson;

public class UploadTestButton : MonoBehaviour
{
    public ServerMessenger _serverMessenger;
    public DummyRacePlayDataCreator _dummyRacePlayDataCreator;

	void Start ()
    {
	
	}
	
	void Update ()
    {
	
	}

    public void PressUploadButton()
    {
        string stageId = "0";
        int level = 1;
        int recordMs = 12345;
        string replayDataId = "TestStageId_1";
        RacePlayData racePlayData = _dummyRacePlayDataCreator.CreateDefault(stageId, level, recordMs, replayDataId, "123,123,123");

        _serverMessenger.Uploaded += OnUploaded;
        _serverMessenger.UploadRacePlayData(racePlayData);
    }

    private void OnUploaded(string wwwText)
    {
        _serverMessenger.Uploaded -= OnUploaded;
        //Debug.Log(wwwText);
    }


    public void PressSearchButton()
    {
        _serverMessenger.Found += OnFound;
        _serverMessenger.RequestMatchingData("ReplayData_0_1", 5);
    }

    private void OnFound(string wwwText)
    {
        _serverMessenger.Found -= OnFound;
        //Debug.Log(www.text);

        JsonData json = JsonMapper.ToObject(wwwText);
        //int count = items.Count;
        //Debug.Log("result: " + json["result"].ToJson());
        //Debug.Log("player count: " + json["player"].Count);

        //for(int i = 0; i < json["player"].Count; i++)
        //{
        //    JsonData player = json["player"][i];
        //    Debug.Log("player: " + player.ToJson());
        //    Debug.Log("gameData: " + player["game_data"].ToString());
        //}
    }
}
