﻿using UnityEngine;
using System.Collections.Generic;
using Junhee.Utils;

public class DummyRacePlayDataCreator : MonoBehaviour
{
    [SerializeField]
    private List<string> _nicknames;
    [SerializeField]
    private List<int> _countryCodes;

    void Start()
    {

    }

    void Update()
    {
        //if(Input.GetKeyUp(KeyCode.Alpha1))
        //   {
        //       RacePlayData data = Create();
        //       Debug.Log(data.nickname);
        //       Debug.Log(data.countryCode);
        //       Debug.Log(data.recordMs);
        //       Debug.Log(data.stageId);
        //       Debug.Log(data.rawReplayData);
        //   }
    }



    public RacePlayData Create1(int stageIndex, int level, int recordMs, string replayDataId)
    {
        int replayDataIndex = (int)Random.Range(0, 4);
        TextAsset data = (TextAsset)Resources.Load("ReplayData/ReplayData_" + stageIndex + "_" + replayDataIndex) as TextAsset;

        int randomIndex = Random.Range(0, _nicknames.Count);
        RacePlayData racePlayData = new RacePlayData();
        racePlayData.uuid = "c0047b982239877c64847ba7a1bc" + Format.ConvertDigit(randomIndex.ToString(), 3, "x", "front");
        racePlayData.nickname = _nicknames[randomIndex];
        racePlayData.countryCode = _countryCodes[randomIndex].ToString();
        racePlayData.rawReplayData = data.text;
        racePlayData.level = level;
        racePlayData.recordMs = recordMs;
        racePlayData.replayDataId = replayDataId;

        Debug.Log(racePlayData.ToString());

        return racePlayData;
    }

    public RacePlayData CreateDefault(string stageId, int level, int recordMs, string replayDataId, string replayData)
    {
        int randomIndex = Random.Range(0, _nicknames.Count);
        RacePlayData racePlayData = new RacePlayData();
        racePlayData.uuid = "c0047b982239877c64847ba7a1bc" + Format.ConvertDigit(randomIndex.ToString(), 3, "x", "front");
        racePlayData.nickname = _nicknames[randomIndex];
        racePlayData.countryCode = _countryCodes[randomIndex].ToString();
        racePlayData.rawReplayData = replayData;
        racePlayData.level = level;
        racePlayData.recordMs = recordMs;
        racePlayData.replayDataId = replayDataId;

        //Debug.Log(racePlayData.ToString());

        return racePlayData;
    }

    public RacePlayData GetDummyUser()
    {
        int randomIndex = Random.Range(0, _nicknames.Count);
        RacePlayData racePlayData = new RacePlayData();
        racePlayData.uuid = "c0047b982239877c64847ba7a1bc" + Format.ConvertDigit(randomIndex.ToString(), 3, "x", "front");
        racePlayData.nickname = _nicknames[randomIndex];
        racePlayData.countryCode = _countryCodes[randomIndex].ToString();
        return racePlayData;
    }

    public List<string> GetNickname()
    {
        return _nicknames;
    }

    public List<int> GetCountryCode()
    {
        return _countryCodes;
    }
}
