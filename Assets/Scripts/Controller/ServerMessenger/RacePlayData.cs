﻿using UnityEngine;
using System.Collections;

public class RacePlayData
{
    public string uuid;
    public string nickname;
    public int level;
    public string countryCode;
    public int recordMs;
    public string replayDataId;
    public string rawReplayData;

    override public string ToString()
    {
        string result = "RacePlayData \n";
        result += "uuid: " + uuid + "\n";
        result += "nickname: " + nickname + "\n";
        result += "level: " + level + "\n";
        result += "countryCode: " + countryCode + "\n";
        result += "recordMs: " + recordMs + "\n";
        result += "replayDataId: " + replayDataId + "\n";
        result += "rawReplayData: " + rawReplayData + "\n";
        return result;
    }
}
