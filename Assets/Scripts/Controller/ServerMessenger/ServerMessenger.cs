﻿using Com.Mod.NGUI.LogConsole;
using LitJson;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using Com.Mod.ThumbRun.Util;
using Com.Mod.ThumbRun.App;

public class ServerMessenger : MonoBehaviour 
{
    /*
     * host : https://game-api.nanoo.so
     * upload : /thumbrun/v1/match/record/
     * matching : /thumbrun/v1/match/search/
     * clientID : tvUSfDlTmW8awJ1eDBaX7wdIcnFGBVHDzrB1RlMFqfnugrqyjL
     * secretKey : 4WtuH1Obo3159qGKYvOA4f3e0TiykP13
     */

    public delegate void ServerEventHandler(String wwwText);
    public event ServerEventHandler Uploaded;
    public event ServerEventHandler Found;
    public event ServerEventHandler CrossPromotionUpdated;

    [SerializeField]
	private string _host;
	[SerializeField]
	private string _replayDataUploadRequestURI;
	[SerializeField]
	private string _replayDataMatchingRequestURI;
    [SerializeField]
    private string _crossPromotionRequestURI;
	[SerializeField]
	private string _clientID;
	[SerializeField]
	private string _secretKey;
    [SerializeField]
    private DummyRacePlayDataCreator _dummyRacePlayDataCreator;
    private string _testURL = "http://api.androidhive.info/contacts/";
	private WWWHelper _wwwHelper;
	

	public static GameObject _instance;

	void Awake ()
	{
		if(_instance == null)
		{
			_instance = gameObject;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
            Destroy(gameObject);			
		}
	}

	void Start () 
	{
	
	}
	
	void Update () 
	{
	}

	void OnDestroy ()
	{
	}



    public void UploadRacePlayData(RacePlayData racePlayData)
    {
        _wwwHelper = WWWHelper.instance;
        _wwwHelper.OnHttpRequest += OnUploaded;

        //String uuid = SystemInfo.deviceUniqueIdentifier;
        int unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        String hashToken = TokenManager.CreateToken(_clientID + racePlayData.uuid + racePlayData.replayDataId + racePlayData.countryCode.ToString() + racePlayData.recordMs.ToString() + unixTimestamp, _secretKey);

        Dictionary<string, string> stringArgs = new Dictionary<string, string>();
        stringArgs.Add("client_id", _clientID);
        stringArgs.Add("uuid", racePlayData.uuid);
        stringArgs.Add("level", racePlayData.level.ToString());
        stringArgs.Add("nickname", racePlayData.nickname);
        stringArgs.Add("country", racePlayData.countryCode.ToString());
        stringArgs.Add("map", racePlayData.replayDataId);
        stringArgs.Add("record", racePlayData.recordMs.ToString());
        stringArgs.Add("game_data", racePlayData.rawReplayData);
        stringArgs.Add("ts", unixTimestamp.ToString());
        stringArgs.Add("hash", hashToken);
        
        string uri = _host + _replayDataUploadRequestURI;
        _wwwHelper.Post("uploadRacePlayData", uri, stringArgs);

        Debug.Log("ServerMessenger - [UploadRacePlayData] \n stageID: " + racePlayData.replayDataId + " \n clientID: " + _clientID + " \n nickname: " + racePlayData.nickname + "\n uri: " + uri + "\n uuid: " + racePlayData.uuid + "\n ts: " + unixTimestamp + "\n hash: " + hashToken + "\n country: " + racePlayData.countryCode.ToString() + "\n record: " + racePlayData.recordMs.ToString() + "\n replay data: " + racePlayData.rawReplayData);
    }

    public void RequestMatchingData(string replayDataId, int limit)
    {
        _wwwHelper = WWWHelper.instance;
        _wwwHelper.OnHttpRequest += OnFound;

        String uuid = SystemInfo.deviceUniqueIdentifier;
        int unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        String hashToken = TokenManager.CreateToken(_clientID + uuid + replayDataId + unixTimestamp.ToString(), _secretKey);

        Dictionary<string, string> stringArgs = new Dictionary<string, string>();
        stringArgs.Add("client_id", _clientID);
        stringArgs.Add("uuid", uuid);
        stringArgs.Add("map", replayDataId);
        stringArgs.Add("limit", limit.ToString());
        stringArgs.Add("ts", unixTimestamp.ToString());
        stringArgs.Add("hash", hashToken);

        string uri = _host + _replayDataMatchingRequestURI;
        _wwwHelper.Post("requestMatchingData", uri, stringArgs);

        Debug.Log("---------- uri: " + uri + "\n clientID: " + _clientID + "\n uuid: " + uuid + "\n map: " + replayDataId + "\n limit: " + limit + " \n ts: " + unixTimestamp + " \n hash: " + hashToken);
    }

    private void OnUploaded(string id, string wwwText)
	{
        if(id == "uploadRacePlayData")
        {
            _wwwHelper.OnHttpRequest -= OnUploaded;
        }

        if (Uploaded != null) Uploaded(wwwText);
    }

    private void OnFound(string id, string wwwText)
    {
        if (id == "requestMatchingData")
        {
            _wwwHelper.OnHttpRequest -= OnFound;
        }
        if (Found != null) Found(wwwText);
    }
}
