﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.IO;
using DG.Tweening;
using Com.Mod.ThumbRun.Race.Application;
using Com.Mod.ThumbRun.User.Application;
using Com.Mod.ThumbRun.Item.View;
using Com.Mod.NGUI.LogConsole;

public class ReplayDataRecorder : MonoBehaviour 
{
    [SerializeField]
    private UserService _userService;
    [SerializeField]
    private PlayerManager _player;
    [SerializeField]
    private RaceRecordService _raceRecordService;
    [SerializeField]
    private ServerMessenger _serverMessenger;
    [SerializeField]
    private DummyRacePlayDataCreator _dummyRacePlayDataCreator;
    [SerializeField]
    private UploadMode.mode _currentUploadMode = UploadMode.mode.DUMMY_USER;

    private GameManager _gameManager;
    private RaceManager _raceManager;
    private bool _isDebug = true;
    private string _logPrefix = "ReplayDataRecorder - ";
    private bool _isRec = false;
    private Animator _playerAnim;
    private ReplayData _replayData;
    private string _mode = "Binary"; // CSV, Binary
    private int _frameCnt = 0;

    void Awake ()
    {
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
        _raceManager.RaceStart += OnRaceStart;
        _raceManager.RacePause += OnRacePause;
        _raceManager.RaceStop += OnRaceStop;
        _raceManager.RaceTimeout += OnRaceTimeout;
        _raceManager.RaceEnd += OnRaceEnd;

        _player.PlayerSwooned += OnPlayerSwooned;
        _player.PlayerRecovered += OnPlayerRecovered;
    }

	void Start ()
    {
        if (RaceModeInfo.raceMode == RaceMode.mode.SINGLE)
        {
            _currentUploadMode = UploadMode.mode.DUMMY_USER;
        }
    }

    void FixedUpdate()
    {
        if(_isRec)
        {
            if (_replayData != null && _playerAnim != null)
            {
                int layerIndex = 0;
                AnimatorStateInfo animStateInfo = _playerAnim.GetCurrentAnimatorStateInfo(layerIndex);
                _replayData.stateHash.Add(animStateInfo.fullPathHash);

                if (_frameCnt % 2 == 0)
                {
                    _replayData.trailPosition.Add(new Vector3(_player.transform.position.x, _player.transform.position.y, 0.0f));  
                }
                _frameCnt++;
            }
        }
    }

    void OnDestroy()
    {
        _raceManager.RaceStart -= OnRaceStart;
        _raceManager.RacePause -= OnRacePause;
        _raceManager.RaceStop -= OnRaceStop;
        _raceManager.RaceTimeout -= OnRaceTimeout;
        _raceManager.RaceEnd -= OnRaceEnd;

        _player.PlayerSwooned -= OnPlayerSwooned;
        _player.PlayerRecovered -= OnPlayerRecovered;
    }



    private void OnRaceStart()
    {
        Record(_player.gameObject);
    }

    private void OnRacePause()
    {

    }

    private void OnRaceStop()
    {
        LogConsole.instance.Log("Race Stop.");
        Stop();
    }

    private void OnRaceTimeout()
    {
        LogConsole.instance.Log("Time Out.");
        Stop();
    }

    private void OnRaceEnd()
    {
        // 골인 지점을 터치 후 2초간 추가 녹화.
        DOVirtual.DelayedCall(2.0f, OnPlayerMovementEnd);
    }

    private void OnPlayerMovementEnd()
    {
        Stop();
        UploadData();
    }

    private void OnPlayerSwooned(int playerID, DamageType.type damageType)
    {
        Pause();
    }

    private void OnPlayerRecovered(int playerID, DamageType.type damageType)
    {
        Resume();
    }

    private void Record(GameObject target)
    {
        if(_isDebug) Debug.Log(_logPrefix + "Record()");
        _replayData = new ReplayData(null, null);
        _replayData.stateHash = new List<int>();
        _replayData.trailPosition = new List<Vector3>();

        _frameCnt = 0;

        if (!_isRec)
        {
            _isRec = true;
        }
    }

    private void Pause()
    {
        if(_isRec) _isRec = false;
    }

    private void Resume()
    {
        if(!_isRec) _isRec = true;
    }

    private void Stop()
    {
        if(_isDebug) Debug.Log(_logPrefix + "Stop()");
        _isRec = false;

#if UNITY_EDITOR
        byte[] ba = WriteToBinary();
        var fileName = "replayData" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm").ToString() + ".txt";
        //File.WriteAllText(fileName, System.Convert.ToBase64String(ba));
#endif
    }

    private void UploadData()
    {
        string stageId = RaceModeInfo.stageId;
        StageData stageData = StageDataManager.instance.GetStageDataByID(stageId);
        int level = _userService.GetLevel();
        float record = (_raceRecordService.GetRaceRecord() - _player.swoonedTimeAmount);
        int recordMs = ScoreFormat.ConvertMS(record);
        string replayDataId = "ReplayData_" + stageData.id + "_" + stageData.replayDataVer;
        string replayData = System.Convert.ToBase64String(WriteToBinary());

        RacePlayData racePlayData;
        if (_currentUploadMode == UploadMode.mode.REAL_USER)
        {
            racePlayData = new RacePlayData();
            racePlayData.uuid = SystemInfo.deviceUniqueIdentifier;
            racePlayData.nickname = _userService.GetNickname();
            racePlayData.countryCode = _userService.GetCountryCode();
            racePlayData.rawReplayData = replayData;
            racePlayData.level = level;
            racePlayData.recordMs = recordMs;
            racePlayData.replayDataId = replayDataId;
        }
        else
        {
            racePlayData = _dummyRacePlayDataCreator.CreateDefault(stageId, level, recordMs, replayDataId, replayData);
        }

        Debug.Log("upload race play data : " + racePlayData.ToString());

        _serverMessenger.Uploaded += OnUploaded;
        _serverMessenger.UploadRacePlayData(racePlayData);
    }

    private void OnUploaded(string wwwText)
    {
        Debug.Log("ReplayDataRecorder - replay data uploaded: " + wwwText);
    }

    private void WriteToFile()
    {
        string line1 = "private List<int> stateHash = new List<int> {";
        for (int i = 0; i < _replayData.stateHash.Count; i++)
        {
            if (i >= _replayData.stateHash.Count - 1) line1 += _replayData.stateHash[i].ToString() + "};";
            else line1 += _replayData.stateHash[i].ToString() + ",";
        }

        List<string> lines = new List<string>();
        string line2 = "private List<Vector3> trailPosition = new List<Vector3> {";
        for (int j = 0; j < _replayData.trailPosition.Count; j++)
        {
            if (j >= _replayData.trailPosition.Count - 1) line2 += "new Vector3(" + Math.Round(_replayData.trailPosition[j].x, 2) + "f, " + Math.Round(_replayData.trailPosition[j].y, 2) + "f, 0f)};";
            else line2 += "new Vector3(" + Math.Round(_replayData.trailPosition[j].x, 2) + "f, " + Math.Round(_replayData.trailPosition[j].y, 2) + "f, 0f),";

            if (j % 300 == 0)
            {
                lines.Add(line2);
                line2 = "";
            }
        }
        lines.Add(line2);

        var fileName = "data.txt";
        if (File.Exists(fileName))
        {
            Debug.Log(fileName + " already exists.");
            return;
        }
        var sr = File.CreateText(fileName);
        sr.WriteLine(line1);
        for (int k = 0; k < lines.Count; k++)
        {
            sr.WriteLine(lines[k]);
        }
        sr.Close();
    }

    private void WriteToCSVFile()
    {
        string prevStateHash = "";
        string stateHashLine = "";
        for (int i = 0; i < _replayData.stateHash.Count; i++)
        {
            string currentStateHash = _replayData.stateHash[i].ToString();
            if (prevStateHash != currentStateHash) stateHashLine += currentStateHash;

            stateHashLine += ",";

            prevStateHash = _replayData.stateHash[i].ToString();
        }

        string prevPosX = "";
        string posXLine = "";
        for (int j = 0; j < _replayData.trailPosition.Count; j++)
        {
            string currentPosX = Math.Round(_replayData.trailPosition[j].x, 2).ToString();
            if (prevPosX != currentPosX) posXLine += currentPosX;

            posXLine += ",";

            prevPosX = currentPosX;
        }

        string prevPosY = "";
        string posYLine = "";
        for (int j = 0; j < _replayData.trailPosition.Count; j++)
        {
            string currentPosY = Math.Round(_replayData.trailPosition[j].y, 2).ToString();
            if (prevPosY != currentPosY) posYLine += currentPosY;

            posYLine += ",";

            prevPosY = currentPosY;
        }

        var fileName = "replayData" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm").ToString() + ".csv";
        if (File.Exists(fileName))
        {
            Debug.Log(fileName + " already exists.");
            return;
        }
        var sr = File.CreateText(fileName);
        sr.WriteLine(stateHashLine);
        sr.WriteLine(posXLine);
        sr.WriteLine(posYLine);
        sr.Close();       
    }

    private byte[] WriteToBinary()
    {
        short[] hashAry = CompressHash(_replayData.stateHash).ToArray();
        short[] xposAry = new short[_replayData.trailPosition.Count];
        short[] yposAry = new short[_replayData.trailPosition.Count];
        short [] recordAry = new short[2];
        short[] stageIndexAry = new short[1];
        for (int j = 0; j < _replayData.trailPosition.Count; j++)
        {
            xposAry[j] = (short)(Math.Round(_replayData.trailPosition[j].x, 2) * 100f);
            yposAry[j] = (short)(Math.Round(_replayData.trailPosition[j].y, 2) * 100f);
        }

        //Debug.Log("----------- _raceRecordService.GetRaceRecord(): " + _raceRecordService.GetRaceRecord() + " / _player.swoonedTimeAmount: " + _player.swoonedTimeAmount);
        float raceRecord = _raceRecordService.GetRaceRecord() - _player.swoonedTimeAmount;
        char[] delimiterChar = { '.' };
        string[] splitRaceRecord = ScoreFormat.ConvertString(raceRecord).Split(delimiterChar);
        //Debug.Log("-------------- raceRecord: " + splitRaceRecord[0] + "." + splitRaceRecord[1]);
        recordAry[0] = short.Parse(splitRaceRecord[0]);
        recordAry[1] = short.Parse(splitRaceRecord[1]);
        stageIndexAry[0] = short.Parse(RaceModeInfo.stageId);
        //recordAry[0] = 5;
        //recordAry[1] = 5;

        //Debug.Log("---------------------- raceRecord: " + raceRecord);
        //Debug.Log("---------------------- record[0]: " + recordAry[0] + " / record[1]: " + recordAry[1]);

        int index = 0;
        int shortByteLength = 2; // short 타입의 byte 길이
        int arrayNum = 3; // bytearray에서 array들의 length를 각 array들 가장 앞에 기록하기 위해 필요한 공간
        int totalByteLength = ((hashAry.Length + xposAry.Length + yposAry.Length + recordAry.Length + stageIndexAry.Length + arrayNum) * shortByteLength);
        byte[] ba = new byte[totalByteLength];
        //Debug.Log("totalByteLength: " + totalByteLength);

        // Hash -----
        // hashLength복사
        byte[] hashLengthBA = BitConverter.GetBytes((short)hashAry.Length);
        Buffer.BlockCopy(hashLengthBA, 0, ba, index, hashLengthBA.Length);
        index += hashLengthBA.Length;
        //Debug.Log("index: " + index);

        // hashAry복사
        Buffer.BlockCopy(hashAry, 0, ba, index, hashAry.Length * shortByteLength);
        index += hashAry.Length * shortByteLength;
        //Debug.Log("index: " + index);

        // X Pos -----
        // xposAryLength복사
        byte[] xposLengthBA = BitConverter.GetBytes((short)xposAry.Length);
        Buffer.BlockCopy(xposLengthBA, 0, ba, index, xposLengthBA.Length);
        index += xposLengthBA.Length;
        //Debug.Log("index: " + index);

        // xposAry복사
        Buffer.BlockCopy(xposAry, 0, ba, index, xposAry.Length * shortByteLength);
        index += xposAry.Length * shortByteLength;
        //Debug.Log("index: " + index);

        // Y Pos -----
        // yposAryLength복사
        byte[] yposLengthBA = BitConverter.GetBytes((short)yposAry.Length);
        Buffer.BlockCopy(yposLengthBA, 0, ba, index, yposLengthBA.Length);
        index += yposLengthBA.Length;
        //Debug.Log("index: " + index);

        // yposAry복사
        Buffer.BlockCopy(yposAry, 0, ba, index, yposAry.Length * shortByteLength);
        index += yposAry.Length * shortByteLength;
        //Debug.Log("index: " + index);

        // record 복사
        Buffer.BlockCopy(recordAry, 0, ba, index, recordAry.Length * shortByteLength);
        index += recordAry.Length * shortByteLength;

        // stageId 복사
        Buffer.BlockCopy(stageIndexAry, 0, ba, index, stageIndexAry.Length * shortByteLength);
        index += stageIndexAry.Length * shortByteLength;

        return ba;
    }



    private List<short> CompressHash(List<int> hashList)
    {
        int current = 0;
        int prev = 0;
        int sameCnt = 0;
        List<short> result = new List<short>();
        for (int i = 0; i < hashList.Count; i++)
        {
            current = hashList[i];
            if (current == prev)
            {
                sameCnt++;
                //Debug.Log("hashList.Count: " + result.Count);

                if (sameCnt == 1) result.Add((short)-sameCnt);
                else result[result.Count - 1] = (short)-sameCnt;
            }
            else
            {
                sameCnt = 0;
                result.Add((short)PlayerStateHash.GetHashToID(current));
            }
            prev = current;
        }

        //string log = "Compressed Hash List";
        //for (int i = 0; i < result.Count; i++)
        //{
        //    log += result[i] + ",";
        //}
        //Debug.Log(log);

        return result;
    }

    private void LogReplayData()
    {
        bool isStopStateHash = false;
        for (int c = 0; c < 10; c++)
        {
            string log = "state ============================={ ";
            for (int i = c * 300; i < (c * 300) + 300; i++)
            {
                if (i >= _replayData.stateHash.Count)
                {
                    isStopStateHash = true;
                    break;
                }
                log += _replayData.stateHash[i].ToString() + ",";
            }
            Debug.Log(log);
            if (isStopStateHash) break;
        }

        bool stop = false;
        for (int x = 0; x < 10; x++)
        {
            string logB = "pos " + _replayData.trailPosition.Count + " ============================={ ";
            for (int j = x * 300; j < (x * 300) + 300; j++)
            {
                if (j >= _replayData.trailPosition.Count)
                {
                    stop = true;
                    break;
                }
                logB += "new Vector3(" + _replayData.trailPosition[j].x + "f, " + _replayData.trailPosition[j].y + "f, " + _replayData.trailPosition[j].z + "f),";
            }

            Debug.Log(logB);
            if (stop) break;
        }
    }



    public void SetPlayerAnim(Animator anim)
    {
        _playerAnim = anim;
    }

    public ReplayData GetReplayData()
    {
        return _replayData;
    }

    public void ChangeUploadMode(UploadMode.mode uploadMode)
    {
        _currentUploadMode = uploadMode;
    }
}