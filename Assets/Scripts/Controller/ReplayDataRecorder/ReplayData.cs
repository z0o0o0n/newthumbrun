﻿using UnityEngine;
using System.Collections.Generic;

public class ReplayData 
{
    public List<int> stateHash;
    public List<Vector3> trailPosition;

    public ReplayData(List<int> stateHash, List<Vector3> trailPosition)
    {
        this.stateHash = stateHash;
        this.trailPosition = trailPosition;
    }

    public string ToString()
    {
        string result = "< Replay Data >\n";
        result += "stateHash count: " + stateHash.Count + " \n";
        result += "trailPosition count: " + trailPosition.Count + " \n";
        return result;
    }
}
