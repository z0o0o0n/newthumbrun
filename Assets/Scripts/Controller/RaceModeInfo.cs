﻿using UnityEngine;
using System.Collections;

public class RaceModeInfo : MonoBehaviour 
{
    private static GameObject _instance;

    public static RaceMode.mode raceMode = RaceMode.mode.MULTI;
    public static string stageId = null;
    public static int? entryIndex = null;
    public static bool isTutorialMode = false;

	void Awake ()
	{
        if (_instance == null)
        {
            _instance = gameObject;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
	}

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}
}
