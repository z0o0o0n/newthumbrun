﻿using UnityEngine;
using System.Collections;

public class PlayerControlButton : MonoBehaviour
{
    public delegate void PlayerControlButtonEvent();
    public event PlayerControlButtonEvent LeftPressed;
    public event PlayerControlButtonEvent RightPressed;
    public GameObject _leftButton;
    public GameObject _rightButton;

    private bool _isDebug = false;
    private string _logPrefix = "PlayerControlButton2 - ";
    private bool _isEnabled = false;
    private RaceManager _raceManager;

    void Awake ()
    {
        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
        _raceManager.RaceEnd += OnRaceEnd;

        ResizeButtons();
        Enable();
    }

	void Start ()
    {
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (_isDebug) Debug.Log(_logPrefix + "Press Left");
            if (LeftPressed != null) LeftPressed();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (_isDebug) Debug.Log(_logPrefix + "Press Right");
            if (RightPressed != null) RightPressed();
        }
    }

    void OnDestroy()
    {
        _raceManager.RaceEnd -= OnRaceEnd;

        _leftButton.GetComponent<UIButtonEventDispatcher>().On_ButtonPress -= OnLeftButtonPress;
        _rightButton.GetComponent<UIButtonEventDispatcher>().On_ButtonPress -= OnRightButtonPress;
    }



    #region Private Functions
    private void OnRaceEnd()
    {
        Deactivate();
    }

    private void OnLeftButtonPress(bool isDown)
    {
        if(isDown)
        {
            if (_isDebug) Debug.Log(_logPrefix + "Press Left");
            if (LeftPressed != null) LeftPressed();
        }
    }

    private void OnRightButtonPress(bool isDown)
    {
        if (isDown)
        {
            if (_isDebug) Debug.Log(_logPrefix + "Press Right");
            if (RightPressed != null) RightPressed();
        }
    }

    private void ResizeButtons()
    {
        Vector2 buttonSize = new Vector3(UIScreen.instance.GetWidth() / 2, UIScreen.instance.GetHeight(), 0f);
        UISprite leftBtnSprite = _leftButton.GetComponent<UISprite>();
        BoxCollider leftBtnCollider = _leftButton.GetComponent<BoxCollider>();
        UISprite rightBtnSprite = _rightButton.GetComponent<UISprite>();
        BoxCollider rightBtnCollider = _rightButton.GetComponent<BoxCollider>();

        leftBtnSprite.width = (int) buttonSize.x;
        leftBtnSprite.height = (int) buttonSize.y;

        leftBtnCollider.size = buttonSize;
        leftBtnCollider.center = new Vector3(buttonSize.x/2, -buttonSize.y/2, 0f);

        rightBtnSprite.width = (int)buttonSize.x;
        rightBtnSprite.height = (int)buttonSize.y;

        rightBtnCollider.size = buttonSize;
        rightBtnCollider.center = new Vector3(-buttonSize.x / 2, -buttonSize.y / 2, 0f);
    }
    #endregion



    #region Public Functions
    private void Enable()
    {
        if(!_isEnabled)
        {
            _isEnabled = true;
            _leftButton.GetComponent<UIButtonEventDispatcher>().On_ButtonPress += OnLeftButtonPress;
            _rightButton.GetComponent<UIButtonEventDispatcher>().On_ButtonPress += OnRightButtonPress;    
        }
    }

    private void Disable1()
    {
        if(_isEnabled)
        {
            _isEnabled = false;
            _leftButton.GetComponent<UIButtonEventDispatcher>().On_ButtonPress -= OnLeftButtonPress;
            _rightButton.GetComponent<UIButtonEventDispatcher>().On_ButtonPress -= OnRightButtonPress;
        }
    }

    public void Activate()
    {
        gameObject.SetActive(true);
        ResizeButtons();
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void PressRight()
    {
        if (RightPressed != null) RightPressed();
    }

    public void PressLeft()
    {
        if (LeftPressed != null) LeftPressed();
    }
    #endregion
}
