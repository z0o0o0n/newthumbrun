﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class StartCounter : MonoBehaviour 
{
    public delegate void CounterEventHandler();
    public event CounterEventHandler CountStart;
    public event CounterEventHandler Tick;
    public event CounterEventHandler CountEnd;

    private bool _isDebug = false;
    private string _logPrefix = "StartCounter - ";
    private int _count = 3;
    private float _countdownTime = -3.0f;

    void Awake()
    {
        Reset();
    }

    void Start()
    {

    }



    #region Private Functions
    private void OnTick()
    {
        if (_isDebug) Debug.Log(_logPrefix + "Countdown: " + _count);
        if (Tick != null) Tick();
    }

    private void OnCounting()
    {
        if (_countdownTime > -2.0f && _count == 3)
        {
            _count = 2;
            OnTick();
        }
        else if (_countdownTime > -1.0f && _count == 2)
        {
            _count = 1;
            OnTick();
        }
        else if (_countdownTime > 0f && _count == 1)
        {
            _count = 0;
            OnTick();
            OnEndCount();
        }
    }

    private void OnEndCount()
    {
        _countdownTime = 0;
        if (CountEnd != null) CountEnd();
    }
    #endregion



    #region Public Functions
    public void StartCount()
    {
        Reset();

        if (CountStart != null) CountStart();
        DOTween.To(() => _countdownTime, x => _countdownTime = x, 1f, 4).SetId("Counter_StartCounter_CountdownTime").SetEase(Ease.Linear).OnUpdate(OnCounting);
        OnTick();
    }

    public void Reset()
    {
        DOTween.Kill("Counter_StartCounter_CountdownTime");
        _count = 3;
        _countdownTime = -3.0f;
    }

    public int GetCurrentCount()
    {
        return _count;
    }

    public float GetCountdownTime()
    {
        return _countdownTime;
    }
    #endregion
}
