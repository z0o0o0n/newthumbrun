﻿using UnityEngine;
using System.Collections;

public class MoveManager : MonoBehaviour
{	
    [SerializeField]
    private PlayerManager _player;
    [SerializeField]
    private PlayerControlButton playerControlButton;
    [SerializeField]
    private AxisController axisController;
    [SerializeField]
    private bool isSimpleController = false;

    private bool _isDebug = false;
    private bool _isActivation = true;
    private bool _isBoosting = false;
    private string _directionState = PlayerMoveDirection.STOP; // 현재 방향으로 주문 후 변경까지 시차가 있음
    private string _orderDirectionState = PlayerMoveDirection.STOP; // 주문된 방향으로 조작 즉시 변경 됨
    private float runSpeed = 0.0f;
	private float boostRunSpeed = 0.0f;
    private float _speed = 0.0f;
	private float _maxMoveSpeed = 0.0f; //현재 능력 내에서 가능한 최대 속도

	public float GetAxis()
	{
        return axisController.GetAxis();   
	}

    public float GetSpeed()
    {
        return _speed;
    }

	public float GetMaxMoveSpeed()
	{
		return _maxMoveSpeed;
	}

    public string GetDirectionState()
    {
        return _directionState;
    }

    public string GetOrderDirectionState()
    {
        return _orderDirectionState;
    }

	public void StartBoost()
	{
        _isBoosting = true;
        _maxMoveSpeed = boostRunSpeed;
	}

	public void EndBoost()
	{
        _isBoosting = false;
        _maxMoveSpeed = runSpeed;
	}

	void Start ()
	{
        _maxMoveSpeed = runSpeed;

        isSimpleController = GameManager.isSimpleControlMode;
	}

    private void OnLeftButtonPress()
    {
        if (isSimpleController)
        {
            if (_player.GetLocationState() == LocationStaete.LOCATION_ON_FLOOR || _player.isDubbleJumpable)
            {
                _orderDirectionState = PlayerMoveDirection.LEFT;
                axisController.Decrease();
            }
        }
        else
        {
            _orderDirectionState = PlayerMoveDirection.LEFT;
            axisController.Decrease();
        }        
    }

    private void OnRightButtonPress()
    {
        if(isSimpleController)
        {
            if (_player.GetLocationState() == LocationStaete.LOCATION_ON_FLOOR || _player.isDubbleJumpable)
            {
                if (_isDebug) Debug.Log("BasicMoveController - OnRightButtonPress()");
                _orderDirectionState = PlayerMoveDirection.RIGHT;
                axisController.Increase();
            }
        }
        else
        {
            if (_isDebug) Debug.Log("BasicMoveController - OnRightButtonPress()");
            _orderDirectionState = PlayerMoveDirection.RIGHT;
            axisController.Increase();
        }
    }

	void Update ()
	{
        UpdateDirectionState(axisController.GetAxis());
	}

	void FixedUpdate()
	{
        // Move Player
        //Debug.Log("BasicMoveController - FixedUpdate() / axis: " + axisController.GetAxis());
        if (_isActivation)
        {
            float velocityX = CalculateSpeed();
            _player.Move(axisController.GetAxis(), velocityX, _isBoosting);
        }
        else
        {
            _player.Move(0.0f, 0.0f, false);
        }
	}

    public void StopMove()
    {
        axisController.Zero(0);
    }

    public void SmoothStopMvmt(float time)
    {
        axisController.Zero(time);
    }
    
    public void UpdateDirectionState(float axis)
	{
        if (axis > 0) _directionState = PlayerMoveDirection.RIGHT;
        else if (axis < 0) _directionState = PlayerMoveDirection.LEFT;
        else _directionState = PlayerMoveDirection.STOP;
	}

    private float CalculateSpeed()
    {
		float moveSpeed = axisController.GetAxis() * _maxMoveSpeed;
		if (moveSpeed <= -_maxMoveSpeed) moveSpeed = -_maxMoveSpeed;
		else if (moveSpeed >= _maxMoveSpeed) moveSpeed = _maxMoveSpeed;
        return moveSpeed;
    }

    public void Activate()
    {
        _isActivation = true;
        playerControlButton.LeftPressed += OnLeftButtonPress;
        playerControlButton.RightPressed += OnRightButtonPress;
    }

    public void Deactivate()
    {
        _isActivation = false;
        playerControlButton.LeftPressed -= OnLeftButtonPress;
        playerControlButton.RightPressed -= OnRightButtonPress;
    }

    public void SetRunSpeed(float value)
    {
        runSpeed = value;
        _maxMoveSpeed = runSpeed;
    }

    public void SetBoostRunSpeed(float value)
    {
        boostRunSpeed = value;
    }

    public void HandOperatedMove(int direction)
    {
        if (direction == -1) OnLeftButtonPress();
        else if (direction == 1) OnRightButtonPress();
    }

    void OnDestroy()
    {
        playerControlButton.LeftPressed -= OnLeftButtonPress;
        playerControlButton.RightPressed -= OnRightButtonPress;
    }
}
