﻿using Com.Mod.ThumbRun.Controller;
using UnityEngine;

/**
/ PlayerControlButton의 신호와 Player의 상태를 고려해 Jump 및 WallJump를 판단하고 Player에게 점프를 요청함.
**/
public class JumpManager : MonoBehaviour
{
    [SerializeField]
    private PlayerManager _player;
    [SerializeField]
    private PlayerControlButton _playerControlButton;
    [SerializeField]
    private AxisController _axisController;
    [SerializeField]
    private WJBooster _wallJumpBooster;

    private bool _debug = false;
    private int _wallJumpBuffer = 0; // 벽점프가능영역에서 점프가 눌렸을 경우 방향정보가 버퍼에 저장됨. 이후 플레이어가 벽에 도달하면 버퍼정보를 바탕으로 벽점프를 시도 후 초기화. 바닦에 도달 시 초기화.
	private float _maxJumpHeight;

    void Awake ()
    {
        _playerControlButton.LeftPressed += OnLeftPressed;
        _playerControlButton.RightPressed += OnRightPressed;
        _player.Landed += OnPlayerLanded;
        _player.WallTouchBegin += OnPlayerWallTouchBegin;
    }

    void Start ()
    {
		_maxJumpHeight = _player.abilityData.jump;
    }

    void Update ()
    {

    }

    void OnDestroy ()
    {
        if(_playerControlButton != null)
        {
            _playerControlButton.LeftPressed -= OnLeftPressed;
            _playerControlButton.RightPressed -= OnRightPressed;
        }
        _player.Landed -= OnPlayerLanded;
        _player.WallTouchBegin -= OnPlayerWallTouchBegin;
    }





	public void ChangeBoostState (bool isBoosting)
	{
        if(isBoosting) _maxJumpHeight = _player.abilityData.boostJump;
        else _maxJumpHeight = _player.abilityData.jump;
        //Debug.Log("???????????? maxJumpHeight: " + _maxJumpHeight);
	}





    #region Private Functions
    private void OnLeftPressed()
    {
        int direction = -1;
        if (GameManager.isSimpleControlMode)
        {
            if (IsOnFloor() && !IsIdling())
            {
                Jump(direction);
            }
            else if (_player.isDubbleJumpable)
            {
                _wallJumpBooster.BoostWallJump(direction);
                Jump(direction);
            }
            else if (_player.isRightWJAreaTouched)
            {
                _wallJumpBuffer = direction;
            }
        }
        else
        {
            if (IsRunning() && _player.GetOrderDirection() == PlayerMoveDirection.LEFT)
            {
                Jump(direction);
            }
            else if (_player.isDubbleJumpable)
            {
                _wallJumpBooster.BoostWallJump(direction);
                Jump(direction);
            }
        }
    }

    private void OnRightPressed()
    {
        int direction = 1;
        if (GameManager.isSimpleControlMode)
        {
            if (IsOnFloor() && !IsIdling())
            {
                Jump(direction);
            }
            else if (_player.isDubbleJumpable)
            {
                _wallJumpBooster.BoostWallJump(direction);
                Jump(direction);
            }
            else if (_player.isLeftWJAreaTouched)
            {
                _wallJumpBuffer = direction;
            }
        }
        else
        {
            if (IsRunning() && _player.GetOrderDirection() == PlayerMoveDirection.RIGHT)
            {
                Jump(direction);
            }
            else if (_player.isDubbleJumpable)
            {
                _wallJumpBooster.BoostWallJump(direction);
                Jump(direction);
            }
        }
    }

    private void OnPlayerWallTouchBegin()
    {
        //Debug.Log("OnPlayerWallTouchBegin --------------------- wallJump: " + _wallJumpBuffer.ToString());
        if(HasWallJumpBuffer())
        {
            Jump(_wallJumpBuffer);
        }
    }

    private void OnPlayerLanded()
    {
        //Debug.Log("land --------------------- land: " + _wallJumpBuffer.ToString());
        _wallJumpBuffer = 0;
    }

    private bool HasWallJumpBuffer()
    {
        if(_wallJumpBuffer == 0) return false;
        else return true;
    }

    private bool IsOnFloor()
    {
        if(_player.GetLocationState() == LocationStaete.LOCATION_ON_FLOOR) return true;
        else return false;
    }

    private bool IsMidAir()
    {
        if(_player.GetLocationState() == LocationStaete.LOCATION_MID_AIR) return true;
        else return false;
    }

    private bool IsIdling()
    {
        if(_player.GetMovementState() == MovementState.MOVEMENT_IDLE) return true;
        else return false;
    }

    private bool IsRunning()
    {
        if(_player.GetMovementState() == MovementState.MOVEMENT_RUNNING) return true;
        else return false;
    }

	private void Jump (int direction)
	{
        string diractionString = "";

        if(IsRight(direction))
        {
            diractionString = "Right";
            _axisController.Increase();
        }
        else if(IsLeft(direction))
        {
            diractionString = "Left";
            _axisController.Decrease();
        }

        _player.Jump(_maxJumpHeight, direction);
        _wallJumpBuffer = 0;

        if (_debug) Debug.Log("JumpManager - Jump / direction: " + diractionString);
	}

    private bool IsRight(int direction)
    {
        if(direction == 1) return true;
        else return false;
    }

    private bool IsLeft(int direction)
    {
        if(direction == -1) return true;
        else return false;
    }
    #endregion
}
