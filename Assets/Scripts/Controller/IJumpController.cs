﻿public interface IJumpController
{
	void SetPlayerManager(PlayerManager playManager);
    void SetJump(float value);
    void SetBoostJump(float value);
	void PressJumpButton(int direction);
	void ReleaseJumpButton();
	void StartBoost();
	void EndBoost();
}

[System.Serializable]
public class IJumpControllerContainer : IUnifiedContainer<IJumpController> {
}
