﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.IO.IsolatedStorage;
using System.Collections.Generic;
using Com.Mod.Game.LanguageManager;
using Com.Mod.ThumbRun.Setting.Domain;
using Com.Mod.ThumbRun.Setting.Application;
using Com.Mod.ThumbRun.Participation.View;

public class MainLoaderScene : MonoBehaviour
{
    public GameManager _gameManager;
    public UIWidget _logoUI;

    [SerializeField]
    private LanguageManager _languageManager;
    [SerializeField]
    private SettingService _settingService;
    [SerializeField]
    private InfomationConfirmPopup _infoConfirmPopup;
    [SerializeField]
    private NetworkErrorPopup _networkErrorPopup;
    [SerializeField]
    private UIWidget _startButtonWidget;
    private bool _isDebug = true;
    private float _titleDisplayTime = 2.0f;
    private bool _isTitleDisplayEnd = false;
	
    void Awake()
    {
        if (_isDebug) Debug.Log("MainLoaderScene - Awake()");

        _languageManager.Initialized += OnPrepared;
        _gameManager.sceneLoader.ShutterClosed += OnShutterClosed;
        _settingService.Prepared += OnPrepared;
        _settingService.Error += OnSettingError;

		_startButtonWidget.gameObject.SetActive(false);

		if (!GameData.instance.IsInit()) GameData.instance.Init();
        if (!CharacterPrefabProvider.instance.IsInit()) CharacterPrefabProvider.instance.Init(GetCharacterPrefabPool().characterPrefabDataList);

        GameData.instance.GetConfigData().PlayCount += 1;
        Application.targetFrameRate = 60;
    }

    private CharacterPrefabPool GetCharacterPrefabPool()
    {
        return GameObject.FindGameObjectWithTag("CharacterPrefabPool").GetComponent<CharacterPrefabPool>();
    }

	void Start ()
    {
        DOVirtual.DelayedCall(2f, OnTitleShowed).SetUpdate(true);
	}

    void OnDestroy()
    {
        _languageManager.Initialized -= OnPrepared;
        _gameManager.sceneLoader.ShutterClosed -= OnShutterClosed;
        _settingService.Prepared -= OnPrepared;
        _settingService.Error -= OnSettingError;
    }





    public void OnStartButtonClick()
    {
		Debug.Log("????????????????????????????????? click");

        if(GameData.instance.GetConfigData().confirmPersonaiInformation)
        {
            LoadMainScene();
        }
        else
        {
			_infoConfirmPopup.Show();
        }
    }





    //private void CheckJailbroken()
    //{
    //    if (IsJailbroken())
    //    {
    //        if(GameData.instance.GetConfigData().isJailbroken == false)
    //        {
    //            GameData.instance.GetConfigData().isJailbroken = true;
    //        }
    //        return;
    //    }
    //    if (_isDebug) Debug.Log("MainLoaderScene - Start()");
    //}

    private void OnPrepared()
    {
        Debug.Log("OnPrepared / _languageManager.isInitialized: " + _languageManager.isInitialized + " / _isTitleDisplayEnd: " + _isTitleDisplayEnd + " / _settingService.isPrepared: " + _settingService.isPrepared);
        if (!_languageManager.isInitialized) return;
        if (!_isTitleDisplayEnd) return;
        if (!_settingService.isPrepared) return;

		_startButtonWidget.gameObject.SetActive(true);
		//LoadMainScene();
	}

    private void OnSettingError()
    {
        _networkErrorPopup.Click += OnErrorPopupClick;
        _networkErrorPopup.ShowPopup();
    }

    private void OnErrorPopupClick()
    {
        _networkErrorPopup.Click -= OnErrorPopupClick;
        Application.Quit();
    }

    private void OnTitleShowed()
    {
        _isTitleDisplayEnd = true;
        OnPrepared();
    }

    private void OnShutterClosed()
    {
        _logoUI.gameObject.SetActive(false);
        _startButtonWidget.gameObject.SetActive(false);
    }

    public void LoadMainScene()
    {
        _gameManager.sceneLoader.Load(SceneName.MAIN);
        GameData.instance.GetConfigData().confirmPersonaiInformation = true;
    }

    //private bool IsJailbroken(){
    //     string[] paths = new string[22] {
    //         "/Applications/Cydia.app",
    //         "/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",
    //         "/Applications/IntelliScreen.app",
    //         "/Applications/MxTube.app",
    //         "/Applications/RockApp.app",
    //         "/private/var/lib/apt",
    //         "/Applications/blackra1n.app",
    //         "/Library/MobileSubstrate/DynamicLibraries/Veency.plist",
    //         "/Applications/Icy.app",
    //         "/private/var/lib/cydia",
    //         "/private/var/tmp/cydia.log",
    //         "/private/var/stash",
    //         "/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",
    //         "/usr/libexec/sftp-server",
    //         "/usr/bin/sshd",
    //         "/usr/sbin/sshd",
    //         "/Applications/FakeCarrier.app",
    //         "/Applications/SBSettings.app",
    //         "/private/var/mobile/Library/SBSettings/Themes",
    //         "/System/Library/LaunchDaemons/com.ikey.bbot.plist",
    //         "/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",
    //         "/Applications/WinterBoard.app"};
    //     int i;
    //     bool jailbroken = false;
    //     for (i = 0; i < paths.Length; i++) {
    //         try
    //         {
    //             System.IO.File.GetAttributes(paths[i]);
    //             Debug.Log("Has jailbroken directory: " + paths[i]);
    //             jailbroken = true;
    //             break;
    //         }
    //         catch(System.Exception e)
    //         {
    //             //Debug.Log("exception: " + e);
    //         }
    //     }

    //     Debug.Log("<< IsJailbroken: " + jailbroken + " >>");
    //     return jailbroken;
    // }
}
