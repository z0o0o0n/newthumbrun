﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Event;
using Com.Mod.ThumbRun.Data;
using DG.Tweening;

namespace Com.Mod.ThumbRun.Controller
{
    public class Booster : MonoBehaviour
    {
        public event BoosterEvent.BoosterEventHandler Init;
        public event BoosterEvent.BoosterEventHandler Activated;
        public event BoosterEvent.BoosterEventHandler Deactivated;
        public event BoosterEvent.BoosterEventHandler BoostStarted;
        public event ProgressEvent.ProgressEventHandler BoostProgressing;
        public event BoosterEvent.BoosterEventHandler BoostEnded;
        public event BoosterEvent.BoosterEventHandler Reset;
        public event BoosterEvent.BoosterEventHandler Emptied;

        [SerializeField]
        private PlayScene _playScene;
        [SerializeField] 
        private PlayerManager _player;
        [SerializeField] 
        private BoosterData _boosterData;
        private int _useableQuantity = 0;
        private bool _isBoosting = false;
        private float _progress = 0.0f;

        void Awake()
        {
            _boosterData.Prepared += OnDataPrepared;

            _playScene.RaceStarted += OnRaceStarted;
            _playScene.RacePause += OnRacePause;
            _playScene.RaceResume += OnRaceResume;
            _playScene.RaceFinished += OnRaceFinished;
        }

        void Start()
        {

        }

        void Update()
        {

        }

        void OnDestroy()
        {
            _boosterData.Prepared -= OnDataPrepared;

            _playScene.RaceStarted -= OnRaceStarted;
            _playScene.RacePause -= OnRacePause;
            _playScene.RaceResume -= OnRaceResume;
            _playScene.RaceFinished -= OnRaceFinished;
        }



        #region Properties
        public bool hasBoost
        {
            get
            {
                if (_useableQuantity > 0) return true;
                else return false;
            }
        }

        public int useableQuantity
        {
            get { return _useableQuantity; }
        }

        public bool isBoosting
        {
            get { return _isBoosting; }
        }

        public float progress
        {
            get { return _progress; }
        }
        #endregion



        #region Private Functions
        private void OnDataPrepared()
        {
            ResetBooster();
            if (Init != null) Init();
        }

        private void OnRaceStarted()
        {
            ResetBooster();
            if (Activated != null) Activated();
        }

        private void OnRacePause()
        {
            if (Deactivated != null) Deactivated();
        }

        private void OnRaceResume()
        {
            if (Activated != null) Activated();
        }

        private void OnRaceFinished(bool isTimeout)
        {
            if (Deactivated != null) Deactivated();
        }

        private void OnBoostProgressing()
        {
            if (BoostProgressing != null) BoostProgressing(_progress);
        }

        private void OnBoostEnded()
        {
            _isBoosting = false;
            _progress = 0.0f;
            if (BoostEnded != null) BoostEnded();
        }

        private void OnBoostEmptied()
        {
            if (Emptied != null) Emptied();
        }
        #endregion



        #region Public Functions
        public void StartBoost()
        {
            if (hasBoost)
            {
                if (!_isBoosting)
                {
                    _isBoosting = true;
                    _useableQuantity -= 1;

                    float duraction = _boosterData.GetRawData().boostDurationTime;
                    DOTween.Kill("duraction." + GetInstanceID());
                    DOTween.To(() => _progress, x => _progress = x, 1, duraction).SetId("duraction." + GetInstanceID()).OnUpdate(OnBoostProgressing).OnComplete(OnBoostEnded);

                    if (BoostStarted != null) BoostStarted();

                    if (!hasBoost)
                    {
                        _useableQuantity = 0;
                        OnBoostEmptied();
                    }
                }
            }
        }

        public void ResetBooster()
        {
            DOTween.Kill("duraction." + GetInstanceID());
            //_useableQuantity = _boosterData.GetRawData().givenBoostQty;
            _useableQuantity = 1;
            _isBoosting = false;
            _progress = 0.0f;

            // Player의 능력치 설정
            float incSpeed = (_player.abilityData.run / 100f) * _boosterData.GetRawData().mvmtSpeedIncRate;
            float boostMvmtSpeed = _player.abilityData.run + incSpeed;
            _player.GetMoveManager().SetBoostRunSpeed(boostMvmtSpeed);

            if (Reset != null) Reset();
        }
        #endregion
    }
}