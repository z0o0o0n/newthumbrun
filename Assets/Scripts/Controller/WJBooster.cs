﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Com.Mod.ThumbRun.Test;
using Com.Mod.ThumbRun.Data;
using Com.Mod.ThumbRun.Event;

namespace Com.Mod.ThumbRun.Controller
{
    /**
     * wj - wall jump
     * ms - movement speed
     * wjms - wall jump movement speed
     * cml - cumulative
     * inc - increase
     */

    public class WJBooster : MonoBehaviour
    {
        public event WJBoosterEvent.WJBoosterEventHandler ReactionTimeStart;
        public event ProgressEvent.ProgressEventHandler ReactionTimeProgressing;
        public event WJBoosterEvent.WJBoosterEventHandler ReactionTimeStop;
        public event WJBoosterEvent.WJBoosterEventHandler ReactionTimeOut;
        public event WJBoosterEvent.CumulativeBoostEventHandler BoostAccumulated;
        public event WJBoosterEvent.BoostEventHandler Boost;

        public PlayerManager playerManager;
        public WJBoosterData wjBoosterData;

        [SerializeField]
        private BBBooster _bBBooster;
        [SerializeField]
        private Booster _booster;

        [Header("Tester")]
        public WJBoostTester wjBoostTester;
        
        private bool _canWallJumpBoost = false;
        private int _cmlBoostQty = 0;
        private float _cmlWJMSIncRate = 0f;
        private float _itemBoostRate = 0f;
        private float _reactionTimeProgress = 0f;

        void Awake()
        {
            if (wjBoostTester)
            {
                wjBoostTester.WallTouchBegin += OnWallTouchBegin;
                wjBoostTester.WallTouchEnd += OnWallTouchEnd;
            }
            else
            {
                playerManager.WallTouchBegin += OnWallTouchBegin;
                playerManager.WallTouchEnd += OnWallTouchEnd;
                playerManager.Landed += OnPlayerLanded;
            }

            _booster.BoostStarted += OnItemBoosterStarted;
            _booster.BoostEnded += OnItemBoosterEnded;
        }

        void Start()
        {

        }

        void Update()
        {
        }

        void OnDestroy()
        {
            if (wjBoostTester)
            {
                wjBoostTester.WallTouchBegin -= OnWallTouchBegin;
                wjBoostTester.WallTouchEnd -= OnWallTouchEnd;
            }
            else
            {
                playerManager.WallTouchBegin -= OnWallTouchBegin;
                playerManager.WallTouchEnd -= OnWallTouchEnd;
                playerManager.Landed -= OnPlayerLanded;
            }

            _booster.BoostStarted -= OnItemBoosterStarted;
            _booster.BoostEnded -= OnItemBoosterEnded;
        }




        public int cumulativeBoostQty
        {
            get
            {
                return _cmlBoostQty;
            }
        }

        public float cmlWJMSIncRate
        {
            get
            {
                return _cmlWJMSIncRate;
            }
        }

        public float maxWJMSIncRate
        {
            get
            {
                return wjBoosterData.GetRawData().maxWJMSIncRate;
            }
        }



        private void OnWallTouchBegin()
        {
            if (_bBBooster.inBeatBoard) return;
            _canWallJumpBoost = true;

            StartReactionTime();
        }

        private void OnReactionTimeUpdate()
        {
            if (ReactionTimeProgressing != null) ReactionTimeProgressing(_reactionTimeProgress);
        }

        private void OnWallTouchEnd()
        {
            _canWallJumpBoost = false;
            _reactionTimeProgress = 0.0f;
        }

        private void OnItemBoosterStarted()
        {
            _itemBoostRate = 20f;
        }

        private void OnItemBoosterEnded()
        {
            _itemBoostRate = 0;
        }

        private void OnPlayerLanded()
        {
            ResetBooster();
        }





        private void StartReactionTime()
        {
            DOTween.Kill("ReactionLimitedTimeTween." + GetInstanceID());
            float reactionLimitedTime = wjBoosterData.GetRawData().reactionLimitedTime;
            DOTween.To(() => _reactionTimeProgress, x => _reactionTimeProgress = x, 1, reactionLimitedTime).SetId("ReactionLimitedTimeTween." + GetInstanceID()).SetEase(Ease.Linear).OnUpdate(OnReactionTimeUpdate).OnComplete(OnReactionTimeOut);
            
            if (ReactionTimeStart != null) ReactionTimeStart();
        }

        // 벽점프 부스트를 위한 반응 제한 시간 초과
        private void OnReactionTimeOut()
        {
            ResetBooster();
            if (ReactionTimeOut != null) ReactionTimeOut();
        }

        private void StopReactionTime()
        {
            DOTween.Kill("ReactionLimitedTimeTween." + GetInstanceID());
            
            if (ReactionTimeStop != null) ReactionTimeStop();
        }

        private void ResetBooster()
        {
            DOTween.Kill("ReactionLimitedTimeTween." + GetInstanceID());
            _canWallJumpBoost = false;
            _reactionTimeProgress = 0f;
            _cmlBoostQty = 0;
            _cmlWJMSIncRate = 0f;

            float targetJumpSpeed = playerManager.abilityData.jumpSpeed;
            playerManager.SetTargetJumpSpeed(targetJumpSpeed);
        }





        public void BoostWallJump(int direction)
        {
            if (_canWallJumpBoost)
            {
                _canWallJumpBoost = false;
                _cmlBoostQty += 1;

                // 누적 벽점프 이동속도 증가율
                _cmlWJMSIncRate = wjBoosterData.GetRawData().wjmsIncRate * _cmlBoostQty;
                if (_cmlWJMSIncRate > wjBoosterData.GetRawData().maxWJMSIncRate) _cmlWJMSIncRate = wjBoosterData.GetRawData().maxWJMSIncRate;

                float currentDefaultMvmtSpeed = playerManager.currentDefaultMvmtSpeed;
                //if(playerManager.isBoost)
                //float playerJumpSpeed = playerManager.abilityData.jumpSpeed + ((playerManager.abilityData.jumpSpeed / 100) * _itemBoostRate);
                //float targetJumpSpeed = playerJumpSpeed + ((playerJumpSpeed / 100) * _cmlWJMSIncRate);

                float playerJumpSpeed = currentDefaultMvmtSpeed + ((currentDefaultMvmtSpeed / 100) * _itemBoostRate);
                float targetJumpSpeed = playerJumpSpeed + ((playerJumpSpeed / 100) * _cmlWJMSIncRate);
                //Debug.Log("***************** targetJumpSpeed: " + targetJumpSpeed);
                playerManager.SetTargetJumpSpeed(targetJumpSpeed);

                StopReactionTime();

                if (Boost != null) Boost(playerManager.transform.localPosition, direction);
                if (BoostAccumulated != null) BoostAccumulated(_cmlBoostQty, _cmlWJMSIncRate);
            }
        }
    }
}
