﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemProvider : MonoBehaviour 
{
    [SerializeField]
    private RaceParticipationManager _participationManager;
    [SerializeField]
    private List<GameObject> _itemPrefabList;
    [SerializeField]
    private RaceManager _raceManager;
    private List<int> _groupA = new List<int>();
    private List<int> _groupB = new List<int>();
    private List<int> _groupC = new List<int>();

    void Awake ()
    {
        _groupA.Add(0);
        _groupA.Add(0);
        _groupB.Add(2);
        _groupA.Add(4);
        _groupA.Add(4);
        _groupA.Add(4);
        _groupA.Add(4);
        _groupA.Add(4);
        _groupA.Add(5);
        _groupA.Add(5);
        _groupA.Add(5);
        //_groupA.Add(9);
        //_groupA.Add(9);
        //_groupA.Add(9);
        //_groupA.Add(9);
        //_groupA.Add(9);

        _groupB.Add(0);
        _groupB.Add(0);
        _groupB.Add(2);
        _groupB.Add(2);
        _groupB.Add(2);
        //_groupB.Add(3);
        _groupB.Add(4);
        _groupB.Add(4);
        _groupB.Add(5);
        _groupB.Add(5);
        _groupB.Add(6);
        _groupB.Add(6);

        _groupC.Add(1);
        _groupC.Add(1);
        _groupC.Add(1);
        _groupB.Add(2);
        _groupB.Add(2);
        //_groupC.Add(3);
        _groupC.Add(4);
        _groupB.Add(5);
        _groupC.Add(6);
        //_groupC.Add(7);
        //_groupC.Add(7);
        //_groupC.Add(7);
        _groupC.Add(8);
        //_groupC.Add(1);
        //_groupC.Add(1);
        //_groupC.Add(1);
        //_groupC.Add(3);
        //_groupC.Add(8);
        //_groupC.Add(8);
        //_groupC.Add(8);
        //_groupC.Add(8);
        //_groupC.Add(8);
        //_groupC.Add(8);

        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
        _raceManager.RoundEnd += OnRaceRoundEnd;
    }

	void Start () 
    {
	    
	}
	
	void Update () 
    {

    }

    void OnDestroy()
    {
        _raceManager.RoundEnd -= OnRaceRoundEnd;
    }


    private int _currentRound = 1;
    private void OnRaceRoundEnd(int round)
    {
        _currentRound = round;
    }



    #region Public Functions
    public IItem GetItem(int playerRank)
    {
        int itemID = 0;
        int playerCount = _participationManager.GetReplayerList().Count + 1;
        int randomValue;

        if (0 <= playerRank && playerRank <= 1)
        {
            randomValue = (int) Mathf.Floor(Random.Range(0f, _groupA.Count));
            itemID = _groupA[randomValue];
        }
        else if(2 <= playerRank && playerRank <= 3)
        {
            randomValue = (int)Mathf.Floor(Random.Range(0f, _groupB.Count));
            itemID = _groupB[randomValue];
        }
        else if(4 <= playerRank && playerRank <= 5)
        {
            randomValue = (int)Mathf.Floor(Random.Range(0f, _groupC.Count));
            itemID = _groupC[randomValue];
        }

        //itemID = 1;
        //itemID = 4; // 미사일
        //itemID = 5; // 얼음 폭탄
        //itemID = 6; // 트랩
        //itemID = 0; // 부스터

        // if(Mathf.Floor(Random.Range(0f, 1.9f)) == 0) itemID = 6;
        // else itemID = 4;
        //itemID = 0;
        return CreateItem(itemID);
    }

    public IItem GetSpecipicItem(int itemId)
    {
        return CreateItem(itemId);
    }

    private IItem CreateItem(int itemID)
    {
        GameObject itemInstance;
        IItem item;

        if(itemID == 0)
        {
            itemInstance = GameObject.Instantiate(_itemPrefabList[itemID]);
            item = itemInstance.GetComponent<Boost>();
        }
        else if (itemID == 1)
        {
            itemInstance = GameObject.Instantiate(_itemPrefabList[itemID]);
            item = itemInstance.GetComponent<BoostS>();
        }
        else if (itemID == 2)
        {
            itemInstance = GameObject.Instantiate(_itemPrefabList[itemID]);
            item = itemInstance.GetComponent<Shield>();
        }
        else if (itemID == 3)
        {
            itemInstance = GameObject.Instantiate(_itemPrefabList[itemID]);
            item = itemInstance.GetComponent<ShieldS>();
        }
        else if(itemID == 4)
        {
            itemInstance = GameObject.Instantiate(_itemPrefabList[itemID]);
            item = itemInstance.GetComponent<Missile>();
        }
        else if(itemID == 5)
        {
            itemInstance = GameObject.Instantiate(_itemPrefabList[itemID]);
            item = itemInstance.GetComponent<Bomb>();
        }
        else if (itemID == 6)
        {
            itemInstance = GameObject.Instantiate(_itemPrefabList[itemID]);
            item = itemInstance.GetComponent<Trap>();
        }
        else if (itemID == 7)
        {
            itemInstance = GameObject.Instantiate(_itemPrefabList[itemID]);
            item = itemInstance.GetComponent<ShieldBoost>();
        }
        else if (itemID == 8)
        {
            itemInstance = GameObject.Instantiate(_itemPrefabList[itemID]);
            item = itemInstance.GetComponent<TimeStopWatch>();
        }
        else
        {
            itemInstance = GameObject.Instantiate(_itemPrefabList[itemID]);
            item = itemInstance.GetComponent<ExpBox>();
        }

        return item;
    }
    #endregion
}
