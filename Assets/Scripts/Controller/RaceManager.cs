﻿using Com.Mod.ThumbRun.Race.Application;
using DG.Tweening;
using UnityEngine;

public class RaceManager : MonoBehaviour 
{
    public event RaceEvent.RaceEventHandler RaceOpen; //경기 개최;
    public event RaceEvent.RaceEventHandler RaceInfoPanelShow;
    public event RaceEvent.RaceEventHandler ParticipantsRecruited;
    public event RaceEvent.RaceEventHandler RacePrepare; // 경기 준비. (경기소개, 경로미리보기 등 포함)
    public event RaceEvent.RaceEventHandler RoutePreviewStart;
    public event RaceEvent.RaceEventHandler RoutePreviewEnd;
    public event RaceEvent.RaceEventHandler CountdownStart;
    public event RaceEvent.RaceEventHandler CountdownEnd;
    public event RaceEvent.RaceEventHandler RaceStart;
    public event RaceEvent.RaceRoundEventHandler RoundEnd;
    public event RaceEvent.RaceEventHandler RacePause;
    public event RaceEvent.RaceEventHandler RaceResume;
    public event RaceEvent.RaceEventHandler RaceStop; // 강제로 종료된경우
    public event RaceEvent.RaceEventHandler RaceTimeout; // 제한시간이 초과된경우
    public event RaceEvent.RaceEventHandler RaceEnd; // 결승선을 통과한경우
    public event RaceEvent.RaceEventHandler RaceClose; // 경기 폐막.

    [SerializeField]
    private RaceParticipationManager _raceParticipationManager;
    [SerializeField]
    private TimeOutService _timeOutService;
    private bool _isDebug = true;
    private string _className = "RaceManager - ";
    

	void Awake ()
	{
        _timeOutService.TimeOut += OnTimeOut;
    }

	void Start ()
	{
        OpenRace();
	}
	
	void Update () 
	{
	
	}

	void OnDestroy()
	{
        _timeOutService.TimeOut -= OnTimeOut;
    }



    private void OnTimeOut()
    {
        TimeoutRace();
    }



    public void OpenRace()
    {
        if(_isDebug) Debug.Log(_className + "Open Race");
        ShowRaceInfoPanel();
        if(RaceOpen != null) RaceOpen();
    }

    private void ShowRaceInfoPanel()
    {
        if(_isDebug) Debug.Log(_className + "Show Race Info Panel");
        if (RaceInfoPanelShow != null) RaceInfoPanelShow();
    }

    public void PrepareRace()
    {
        if(_isDebug) Debug.Log(_className + "Prepare Race");
        if (RacePrepare != null) RacePrepare();
        DroppedTrapManager.DestroyAllTraps();
        StartRoutePreview();
    }

    // Preview Route
    public void StartRoutePreview()
    {
        if (RoutePreviewStart != null) RoutePreviewStart();
    }

    public void EndRoutePreview()
    {
        if (RoutePreviewEnd != null) RoutePreviewEnd();
        StartCountdown();
    }

    // Start Countdown
    private void StartCountdown()
    {
        Debug.Log("********** StartCountdown");
        // 여기서 결정
        if (CountdownStart != null) CountdownStart();
    }

    public void EndCountdown()
    {
        Debug.Log("********** EndCountdown");
        // Play Scene에서 결정
        if (CountdownEnd != null) CountdownEnd();
        StartRace();
    }

    // Start, Pause, Resume, End Race
    public void StartRace()
    {
        Debug.Log("********** StartRace");
        if (RaceStart != null) RaceStart();
    }

    public void PauseRace()
    {
        Debug.Log("********** PauseRace");
        // PlayScene.OnApplicationPause
        // BackButtonManager
        // UI.PauseButtonClick
        if (RacePause != null) RacePause();
    }

    public void ResumeRace()
    {
        Debug.Log("********** ResumeRace");
        if (RaceResume != null) RaceResume();
    }

    public void EndRound(int currentLap)
    {
        Debug.Log("********** EndRound");
        if (RoundEnd != null) RoundEnd(currentLap);
    }

    public void StopRace()
    {
        Debug.Log("********** StopRace");
        if (RaceStop != null) RaceStop();
    }

    private void TimeoutRace()
    {
        Debug.Log("********** TimeoutRace");
        if (RaceTimeout != null) RaceTimeout();
        DOTween.Kill("CloseRace." + GetInstanceID());
        DOVirtual.DelayedCall(2f, CloseRace).SetId("CloseRace." + GetInstanceID()).SetUpdate(true);
    }

    public void EndRace()
    {
        Debug.Log("********** EndRace");
        if (RaceEnd != null) RaceEnd();
        DOTween.Kill("CloseRace." + GetInstanceID());
        DOVirtual.DelayedCall(2f, CloseRace).SetId("CloseRace." + GetInstanceID()).SetUpdate(true);
    }

    private void CloseRace()
    {
        Debug.Log("********** CloseRace");
        if (RaceClose != null) RaceClose();
    }

    public void ShowResultScreen()
    {

    }

    public void HideResultScreen()
    {
        Debug.Log("********** HideResultScreen");
        OpenRace();
    }
}
