﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SceneLoader : MonoBehaviour 
{
    public delegate void SceneLoaderEvent();
    public event SceneLoaderEvent LoadCompleted;
    public event SceneLoaderEvent ShutterClosed;
    public event SceneLoaderEvent ShutterOpened;

    public LoadingShutter loadingShutter;

    private bool _isDebug = false;
    private bool _isLoadCompleted = false;
    private string _sceneName;
    private AsyncOperation _asyncOperation;

    void Awake ()
    {
        DontDestroyOnLoad(this);
    }

	void Start () 
	{
	
	}

    void Update()
    {
        if (_asyncOperation != null)
        {
            if (_asyncOperation.isDone && !_isLoadCompleted)
            {
                _isLoadCompleted = true;
                _asyncOperation = null;

                if (LoadCompleted != null) LoadCompleted();
            }
        }
    }

    public void Load(string sceneName)
    {
        if (_isDebug) Debug.Log("SceneLoader - LoadScene() / sceneName: " + sceneName);
        _isLoadCompleted = false;
        _sceneName = sceneName;

        CloseShutter(0.3f);
    }

    // Close
    private void CloseShutter(float time)
    {
        loadingShutter.Closed += OnShutterClosed;
        loadingShutter.Close(time);
    }

    private void OnShutterClosed()
    {
        if (_isDebug) Debug.Log("SceneLoader - OnShutterClosed()");
        loadingShutter.Closed -= OnShutterClosed;
        _asyncOperation = Application.LoadLevelAsync(_sceneName);
        if (ShutterClosed != null) ShutterClosed();
    }

    // Open
    public void OpenShutter(float time)
    {
        loadingShutter.Opened += OnShutterOpened;
        loadingShutter.Open(time);
    }

    private void OnShutterOpened()
    {
        loadingShutter.Opened -= OnShutterOpened;
        if (ShutterOpened != null) ShutterOpened();
    }


    void OnDestroy()
    {
        loadingShutter.Closed -= OnShutterClosed;
        loadingShutter.Opened -= OnShutterOpened;
    }
}
