﻿public interface IMoveController
{
	void SetPlayerManager(PlayerManager playerManager);
    void SetRunSpeed(float value);
    void SetBoostRunSpeed(float value);
	float GetAxis();
    float GetSpeed();
	float GetMaxMoveSpeed();
    string GetDirectionState();
    string GetOrderDirectionState();
    void StopMove();
    void SmoothStopMvmt(float time);
    void UpdateDirectionState(float axis);
	void StartBoost();
	void EndBoost();
    void Activate();
    void HandOperatedMove(int direction);
    void Deactivate();
}

[System.Serializable]
public class IMoveControllerContainer : IUnifiedContainer<IMoveController> {
}
