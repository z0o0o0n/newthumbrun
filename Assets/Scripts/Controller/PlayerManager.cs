﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Com.Mod.ThumbRun.Event;
using System;
using Com.Mod.Util;
using Com.Mod.ThumbRun.Race.Application;
using Com.Mod.ThumbRun.User.Application;
using Com.Mod.ThumbRun.Item.View;
using Junhee.Utils;
using Com.Mod.ThumbRun.Race.View;

public class PlayerManager : MonoBehaviour, IRaceParticipant
{
    public delegate void JumpEvent(float direction, Vector3 position);
    public event JumpEvent On_WallJump;
    public event PlayerEvent.PlayerEventHandler Initialized;
    public event PlayerEvent.PlayerEventHandler WallTouchBegin;
    public event PlayerEvent.PlayerEventHandler WallTouchEnd;
    public event PlayerEvent.PlayerJumpEventHandler WallJumpBegin;
    public event PlayerEvent.PlayerEventHandler Landed;
    public event PlayerEvent.StateEventHandler PlayerSwooned;
    public event PlayerEvent.StateEventHandler PlayerRecovered;
    public event PlayerEvent.PassPointEventHandler PassStartPoint;
    public event PlayerEvent.PassPointEventHandler PassCheckPoint;
    public event PlayerEvent.RankEventHandler RankChanged;

    public BoostEffect boostEffect;
    public ReplayDataRecorder replayDataRecorder;
    public AudioClip _jumpSound;
    public AudioClip _dubbleJumpSound;
    public AudioClip _landingSound;
    public AudioClip _wallSound;
    public AbilityData abilityData;

    [SerializeField]
    private MoveManager _moveManager;
    [SerializeField]
    private JumpManager _jumpManager;
    //[SerializeField]
    //private RankData _rankData;
    [SerializeField]
    private StartingBoostManager _startingBoostManager;
    [SerializeField]
    private StopwatchModel _stopwatch;
    [SerializeField]
    private ItemManager _itemManager;
    [SerializeField]
    private ItemProvider _itemProvider;
    [SerializeField]
    private UserService _userService;
    [SerializeField]
    private RaceRecordService _raceRecordService;
    [SerializeField]
    private int _lastAnimationHash;
    [SerializeField]
    private GameObject _itemDamageEffectPrefab;
    [SerializeField]
    private AutoShieldService _autoShieldService;

    private bool _isDebug = false;
    private string _logPrefix = "PlayerManager - ";
    private string _nickname = "User";
    private int _playerID = 0;
    private Animator _anim;
    private Rigidbody _rigidbody;
    private AudioSource _audioSource;    
    private bool _isPlayRunAudio = false;
    private string _movementState = MovementState.MOVEMENT_IDLE;
    private string _locationState = LocationStaete.LOCATION_ON_FLOOR;
    private float _moveAxis = 0.0f;
    private bool _isBoost = false;
    private bool _isTouchedWall = false;
	private float _currentJumpDirection = 0.0f;
	private float _fallStartAxis = 0.0f;
    private bool _canWallJump = false; // 더블 점프 가능한 시간내에 있는지 여부
    private int _wallTriggerNum = 0;
	private SpeedChecker _speedChecker;
    private Vector3 _startPosition;
    private GameObject _characterGO;
    private DontGoThroughThings _dontGoThroughThings;
    private bool _isSwoon = false;
    private bool _isProtecting = false;
    private bool _isStartPointTouched = false;   
    private bool _isCheckPointTouched = false;
    private bool _isRightWJAreaTouched = false;
    private bool _isLeftWJAreaTouched = false;
    private float _currentCheckPointValue = 0f; // start point는 정수 (1, 2, 3, 4) check point는 (1.5, 2.5, 3.5)
    private int _currentRound = 0; // round (1, 2, 3)
    private float _targetJumpSpeed = 0f;
    private float _currentDefaultMvmtSpeed = 0f;
    private ParticipantData _participantData;
    private RaceManager _raceManager;
    private float _swoonedTimeAmount = 0f;
    private bool _isFinishedRace = false;
    private bool _isWallJumping = false;

    // 추가 능력치
    private float _additionalFloorSpeed = 0f;
    private float _additionalBoostFloorSpeed = 0f;
    private float _additionalFallingSpeed = 0f;
    private float _additionalJumpSpeed = 0f;
    private float _bombRecoveryRate = 1;
    private float _missileRecoveryRate = 1;
    private float _trapRecoveryRate = 1;

    public bool isDubbleJumpable
    {
        get{ return _canWallJump; }
    }

    public bool isRightWJAreaTouched
    {
        get{ return _isRightWJAreaTouched; }
    }

    public bool isLeftWJAreaTouched
    {
        get{ return _isLeftWJAreaTouched; }
    }

    public MoveManager GetMoveManager()
    {
        return _moveManager;
    }

    public bool isBoost
    {
        get{ return _isBoost; }
    }



	void Awake ()
	{
        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
        _raceManager.RaceEnd += OnRaceEnd;
        _raceManager.RaceTimeout += OnRaceTimeout;
        _raceManager.RaceStop += OnRaceStop;

        _userService.Prepared += OnUserServicePrepared;
        _raceRecordService.Finished += OnRaceRecordFinished;

        _rigidbody = GetComponent<Rigidbody>();
		_speedChecker = GetComponent<SpeedChecker>();
        _dontGoThroughThings = GetComponent<DontGoThroughThings>();
        

        _audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        Character character = _characterGO.GetComponent<Character>();
        character.ChangeMaterialProp(Color.black, 0f);
    }

    void Update()
    {
    }

    void OnDestroy()
    {
        _raceManager.RaceEnd -= OnRaceEnd;
        _raceManager.RaceTimeout -= OnRaceTimeout;
        _raceManager.RaceStop -= OnRaceStop;
        _raceRecordService.Finished -= OnRaceRecordFinished;
        _userService.Prepared -= OnUserServicePrepared;
    }




    private void OnUserServicePrepared()
    {
        _participantData = new ParticipantData(1.1f);
        _participantData.characterId = _userService.GetCharacterId();
        //_participantData.characterIndex = 2;
        _participantData.countryCode = _userService.GetCountryCode();
        _participantData.isUser = true;
        _participantData.level = _userService.GetLevel();
        _participantData.nickname = _userService.GetNickname();
        
        OnPrepared();
    }

    private void OnPrepared()
    {
        CreateCharacter();
        InitCharacterAbility();
        CreateItemDamageEffectPrefab();

        if (Initialized != null) Initialized();
    }

    private void CreateItemDamageEffectPrefab()
    {
        GameObject instance = GameObject.Instantiate(_itemDamageEffectPrefab);
        instance.transform.parent = transform;
        instance.transform.localScale = Vector3.one;
        instance.transform.localPosition = Vector3.zero;

        ItemDamageEffect itemDamageEffect = instance.GetComponent<ItemDamageEffect>();
        itemDamageEffect.Init(gameObject);
    }

    private void OnRaceEnd()
    {
        _isFinishedRace = true;
        SmoothStopMvmt(2f);
        _itemManager.DestroyItem();
        if (_autoShieldService.HasAutoShield()) _autoShieldService.SetAutoShield(false);
    }

    private void OnRaceTimeout()
    {
        _isFinishedRace = true;
        SmoothStopMvmt(2f);
        _itemManager.DestroyItem();
        if (_autoShieldService.HasAutoShield()) _autoShieldService.SetAutoShield(false);

        _participantData.ChangeRecord(60f);
    }

    private void OnRaceStop()
    {
        _isFinishedRace = true;
        SmoothStopMvmt(1f);
        _itemManager.DestroyItem();
        if (_autoShieldService.HasAutoShield()) _autoShieldService.SetAutoShield(false);
    }

    private void OnRaceRecordFinished(int round, float record)
    {
        _participantData.ChangeRecord(_raceRecordService.GetRaceRecord());
    }



    public void SetStartPoint(Vector2 startPosition)
    {
        _startPosition = new Vector3(startPosition.x, startPosition.y, 1);
        transform.localPosition = _startPosition;
    }

    private void CreateCharacter()
    {
        GameObject characterPrefab = CharacterPrefabProvider.instance.Get(GetMainCharacterID());
        _characterGO = GameObject.Instantiate(characterPrefab);
        _characterGO.transform.parent = this.transform;
        _characterGO.transform.localPosition = new Vector3(0, 0, 0);
        _characterGO.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));

        _anim = _characterGO.GetComponent<Animator>();

        SetAnimInRecorder(_anim);
    }

    private void SetAnimInRecorder(Animator anim)
    {
        replayDataRecorder.SetPlayerAnim(anim);
    }

    private void InitCharacterAbility()
    {
        abilityData = GetCharacterData(GetMainCharacterID()).GetAbilityData();

        _currentDefaultMvmtSpeed = abilityData.run;
        _targetJumpSpeed = abilityData.run;

        _moveManager.SetRunSpeed(abilityData.run);
        _moveManager.SetBoostRunSpeed(abilityData.boostRun);

        if (_isDebug)
        {
            string log = _logPrefix + "InitCharacterAbility() / Ability Data \n";
            log += " - run: " + abilityData.run + "\n";
            log += " - jump: " + abilityData.jump + "\n";
            log += " - jump speed: " + abilityData.jumpSpeed + "\n";
            log += " - boostRun: " + abilityData.boostRun + "\n";
            log += " - boostJump: " + abilityData.boostJump + "\n";
            log += " - boostJumpSpeed: " + abilityData.boostJumpSpeed + "\n";
            Debug.Log(log);
        }
    }

    private CharacterData GetCharacterData(int characterID)
    {
        return CharacterDataManager.instance.GetCharacterDataByID(characterID);
    }

    public int GetMainCharacterID()
    {
        return _userService.GetCharacterId();
    }

    void FixedUpdate()
    {
        _speedChecker.UpdatePos(transform.position);

        CheckOnFloor();
        CheckRun();
        CheckFalling();
        CheckDubbleJumpable();
        CheckLocationStaete();
        if(!_isSwoon) ControlAnimationSpeed();

        if(_movementState == MovementState.MOVEMENT_RUNNING)
        {
            if(!_isPlayRunAudio)
            {
                _isPlayRunAudio = true;
                //_audioSource.Play();
            }
        }
        else
        {
            if(_isPlayRunAudio)
            {
                _isPlayRunAudio = false;
                //_audioSource.Stop();
            }
        }
    }

    private void CheckOnFloor()
    {
        // Create Raycasts
        Vector3 leftRayPos = new Vector3(transform.position.x - 0.07f, transform.position.y + 0.02f, 1.0f);
        Vector3 rightRayPos = new Vector3(transform.position.x + 0.07f, transform.position.y + 0.02f, 1.0f);
        Ray leftRay = new Ray(leftRayPos, Vector3.down * 100.0f);
        Ray rightRay = new Ray(rightRayPos, Vector3.down * 100.0f);
        RaycastHit leftRayHit = new RaycastHit();
        RaycastHit rightRayHit = new RaycastHit();

        // Draw Ray
        Debug.DrawRay(leftRayPos, Vector3.down * 100.0f);
        Debug.DrawRay(rightRayPos, Vector3.down * 100.0f);

        // Check On Floor
        if (Physics.Raycast(leftRay, out leftRayHit) && Physics.Raycast(rightRay, out rightRayHit))
        {
            // if (leftRayHit.collider.tag == "Trap") return;
            // if (leftRayHit.collider.tag == "Wall") return;
            // if (rightRayHit.collider.tag == "Wall") return;
            // if (leftRayHit.collider.tag == "RightWallJumpArea") return;
            // if (rightRayHit.collider.tag == "RightWallJumpArea") return;
            // if (leftRayHit.collider.tag == "LeftWallJumpArea") return;
            // if (rightRayHit.collider.tag == "LeftWallJumpArea") return;

            //Debug.Log(leftRayHit.collider.tag + ": " + leftRayHit.distance + " / " + rightRayHit.collider.tag + ": " + rightRayHit.distance);

            //Debug.Log("leftRay distance: " + leftRayHit.distance + " / rightRay distance: " + rightRayHit.distance + " vx :" + _rigidbody.velocity.x + " vy: " + _rigidbody.velocity.y + " py : " + transform.position.y + " / left tag: " + leftRayHit.collider.tag + " / right tag: " + rightRayHit.collider.tag);

            if ((leftRayHit.collider.tag == "Ground" && leftRayHit.distance < 0.1f) || (rightRayHit.collider.tag == "Ground" && rightRayHit.distance < 0.1f))
            {
                // On Floor

                if (_locationState == LocationStaete.LOCATION_MID_AIR)
                {
                    if (_movementState != MovementState.MOVEMENT_JUMP_GOING_UP)
                    {
                        _locationState = LocationStaete.LOCATION_ON_FLOOR;
                        OnFloor();
                        Debug.Log("============= floor");
                        //Debug.Log("leftRay distance: " + leftRayHit.distance + " / rightRay distance: " + rightRayHit.distance + " vx :" + _rigidbody.velocity.x + " vy: " + _rigidbody.velocity.y + " py : " + transform.position.y + " / left tag: " + leftRayHit.collider.tag + " / right tag: " + rightRayHit.collider.tag);
                    }
                }
            }
            else
            {
                // Mid Air
                if (_locationState == LocationStaete.LOCATION_ON_FLOOR)
                {
                    _locationState = LocationStaete.LOCATION_MID_AIR;
                    OnMidAir();
                    Debug.Log("============= air");
                }
            }
        }
    }

    private void CheckRun()
    {
        if (_movementState != MovementState.MOVEMENT_JUMP_GOING_UP && _movementState != MovementState.MOVEMENT_JUMP_FALLING)
        {
            if (_movementState != MovementState.MOVEMENT_RUN_FALLING)
            {
                if (_moveAxis == 0)
                {
                    _movementState = MovementState.MOVEMENT_IDLE;
                }
                else
                {
                    _movementState = MovementState.MOVEMENT_RUNNING;
                }
            }
        }
    }

    private void CheckFalling()
    {
        if (_locationState == LocationStaete.LOCATION_MID_AIR)
        {
            if (GetComponent<Rigidbody>().velocity.y < 0)
            {
                if (_movementState == MovementState.MOVEMENT_RUNNING)
                {
                    _fallStartAxis = _moveAxis;
                    _movementState = MovementState.MOVEMENT_RUN_FALLING;

                }
                else if (_movementState == MovementState.MOVEMENT_JUMP_GOING_UP)
                {
                    _movementState = MovementState.MOVEMENT_JUMP_FALLING;
                }

            }
        }
    }

    private void CheckDubbleJumpable()
    {
        if (_movementState == MovementState.MOVEMENT_JUMP_GOING_UP || _movementState == MovementState.MOVEMENT_JUMP_FALLING)
        {
            if (_isTouchedWall)
            {
                if (!_canWallJump)
                {
                    _canWallJump = true;
                    if (WallTouchBegin != null) WallTouchBegin();
                    //_audioSource.clip = _wallSound;
                    //_audioSource.Play();
                    //wallJumpIcon.renderer.enabled = true;
                    //Debug.Log("----- dubbleJumpable: true");
                }
            }
            else
            {
                if (_canWallJump)
                {
                    _canWallJump = false;
                    if (WallTouchEnd != null) WallTouchEnd();
                     //Debug.Log("----- dubbleJumpable: false");
                    //wallJumpIcon.renderer.enabled = false;
                }
            }
        }
        else
        {
            if (_canWallJump)
            {
                _canWallJump = false;
                if (WallTouchEnd != null) WallTouchEnd();
                 //Debug.Log("----- dubbleJumpable: false");
                //wallJumpIcon.renderer.enabled = false;
            }
        }
    }

    // Check Player State
    private string _prevMovementState = "";
    private string _prevLocationState = "";
    private bool _prevTouchWall = false;
    private void CheckLocationStaete()
    {
        if (_movementState != _prevMovementState || _locationState != _prevLocationState || _isTouchedWall != _prevTouchWall)
        {
            //Debug.Log("Player State : " + _movementState + " | " + _locationState + " | Touch Wall : " + _isTouchedWall);
            _prevMovementState = _movementState;
            _prevLocationState = _locationState;
            _prevTouchWall = _isTouchedWall;
        }
    }

    private void ControlAnimationSpeed()
    {
        if (_movementState == MovementState.MOVEMENT_JUMP_GOING_UP || _movementState == MovementState.MOVEMENT_JUMP_FALLING)
        {
            _anim.speed = 1.2f;
        }
        else if (_movementState == MovementState.MOVEMENT_RUN_FALLING)
        {
            _anim.speed = 1.0f;
        }
        else if (_locationState == LocationStaete.LOCATION_ON_FLOOR)
        {
            _anim.speed = (_speedChecker.GetSpeed() * 15) + 0.8f;
        }
        else _anim.speed = 1.0f;

        if (_anim.speed > 1.8f) _anim.speed = 1.8f;
    }

    private void OnFloor()
    {
        if (Landed != null) Landed();
        _movementState = MovementState.MOVEMENT_IDLE;
        _anim.SetBool("IsJump", false);
        _hasJumpDirection = false;

        if(_isDebug) Debug.Log(_logPrefix + "OnFloor()");
    }

    private void OnMidAir()
    {
    }

    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Wall")
        {
            _wallTriggerNum += 1;
            if (_wallTriggerNum > 0)
            {
                //Debug.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! touch wall / " + collider.gameObject.name);
                _isTouchedWall = true;
            }
        }

        if(collider.tag == "LeftWallJumpArea")
        {
            if(!_isLeftWJAreaTouched) {
                _isLeftWJAreaTouched = true;
                //Debug.Log("*************** _isLeftWJAreaTouched: " + _isLeftWJAreaTouched + " / _isRightWJAreaTouched: " + _isRightWJAreaTouched);
            }
        }

        if(collider.tag == "RightWallJumpArea")
        {
            if(!_isRightWJAreaTouched) {
                _isRightWJAreaTouched = true;
                //Debug.Log("*************** _isLeftWJAreaTouched: " + _isLeftWJAreaTouched + " / _isRightWJAreaTouched: " + _isRightWJAreaTouched);
            }
        }

        if(collider.name == "StartPoint")
        {
            if(!_isStartPointTouched)
            {
                _isStartPointTouched = true;
                _isCheckPointTouched = false;

                if(_currentCheckPointValue != 0f)
                {
                    // 1, 2, 3 라운드 통과시
                    _currentCheckPointValue += 0.5f;
                    _currentRound = (int) Mathf.Floor(_currentCheckPointValue);

                    //float roundRecord = (_stopwatch.GetSec() + _stopwatch.GetMsec()) - _rankData.totalRecord;
                    //_rankData.totalRecord += roundRecord;
                    //_rankData.roundRecords.Add(roundRecord);

                    //Debug.Log("********* round: " + _currentRound + " / roundRecord: " + roundRecord + " / totalRecord: " + _rankData.totalRecord);
                }

                //Debug.Log("========== pass start point");
                if (PassStartPoint != null) PassStartPoint(_playerID);
            }
        }
        else if(collider.name == "CheckPoint")
        {
            if(!_isCheckPointTouched)
            {
                _isCheckPointTouched = true;
                _isStartPointTouched = false;
                _currentCheckPointValue += 0.5f;

                if (PassCheckPoint != null) PassCheckPoint(_playerID);
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if(collider.tag == "Wall")
        {
            _wallTriggerNum -= 1;
            //Debug.Log("---------------------- exit wall / " + collider.gameObject.name + " / _wallTriggerNum: " + _wallTriggerNum);
            if (_wallTriggerNum <= 0)
            {
                _wallTriggerNum = 0;
                
                _isTouchedWall = false;
            }
        }

        if(collider.tag == "LeftWallJumpArea")
        {
            if(_isLeftWJAreaTouched) {
                _isLeftWJAreaTouched = false;
                //Debug.Log("*************** _isLeftWJAreaTouched: " + _isLeftWJAreaTouched + " / _isRightWJAreaTouched: " + _isRightWJAreaTouched);
            }
        }

        if(collider.tag == "RightWallJumpArea")
        {
            if(_isRightWJAreaTouched) {
                _isRightWJAreaTouched = false;
                //Debug.Log("*************** _isLeftWJAreaTouched: " + _isLeftWJAreaTouched + " / _isRightWJAreaTouched: " + _isRightWJAreaTouched);
            }
        }
    }

    // Jump
	// private int _prveDirction = 0;
    private float _wallJumpCooltime = 0.1f;
    private bool _isWallJumpCooled = true; //벽 점프가 연속적으로 발생하는 것을 방지
	public void Jump(float velocityY, int jumpDirection)
	{
        if (_isSwoon) return;

        _isWallJumping = false;

        if (_locationState == LocationStaete.LOCATION_ON_FLOOR) // 바닦에서 점프할 때
        {
            _movementState = MovementState.MOVEMENT_JUMP_GOING_UP;

            _currentJumpDirection = jumpDirection;
            _anim.SetBool("IsJump", true);
            if(_currentJumpDirection > 0) _anim.Play("Main.RightJump", -1, 0.0f);
            else if (_currentJumpDirection < 0) _anim.Play("Main.LeftJump", -1, 0.0f);
            GetComponent<Rigidbody>().velocity = new Vector3(GetComponent<Rigidbody>().velocity.x, velocityY, 0.0f);

            _audioSource.clip = _jumpSound;
            _audioSource.Play();
        }
        else if(_canWallJump) // 벽 점프 할때
        {
            //Debug.Log ("----- Wall Jump Axis: " + _currentJumpDirection + " / direction: " + jumpDirection);
            if (_currentJumpDirection == jumpDirection) return;
            if (!_isWallJumpCooled) return;

            _isWallJumping = true;

            _movementState = MovementState.MOVEMENT_JUMP_GOING_UP;
            _isWallJumpCooled = false;
            _currentJumpDirection = jumpDirection;
            DOTween.Kill("WallJumpCoolTween." + GetInstanceID());
            DOTween.To(() => _wallJumpCooltime, x => _wallJumpCooltime = x, _wallJumpCooltime, _wallJumpCooltime).SetId("WallJumpCoolTween." + GetInstanceID()).OnComplete(OnWallJumpCooled);

            //if (_isDebug) Debug.Log(_logPrefix + "Dubble Jump - Jump Start Axis: " + _currentJumpDirection + " / Direction: " + jumpDirection);
            
            _anim.SetBool("IsJump", true);
            if (_currentJumpDirection > 0) _anim.Play("Main.RightJump", -1, 0.0f);
            else if (_currentJumpDirection < 0) _anim.Play("Main.LeftJump", -1, 0.0f);
            GetComponent<Rigidbody>().velocity = new Vector3(GetComponent<Rigidbody>().velocity.x, velocityY, 0.0f);

            _audioSource.clip = _dubbleJumpSound;
            _audioSource.Play();

            if (On_WallJump != null) On_WallJump(jumpDirection, transform.localPosition);
            if (WallJumpBegin != null) WallJumpBegin(jumpDirection);
        }
        _moveAxis = jumpDirection;
	}

    private void OnWallJumpCooled()
    {
        _isWallJumpCooled = true;
    }

    // 기절하다
    private Vector2 _tempVelocity;
    private DamageType.type _currentDamageType;
    public void Swoon(DamageType.type damageType, float duration, IRaceParticipant attacker)
    {
        if (_isFinishedRace) return;
        if (_isProtecting) return;
        if (_autoShieldService.HasAutoShield())
        {
            _autoShieldService.UseAutoShield(this);
            return;
        }

        if(!_isSwoon)
        {
            //Debug.Log("********* _rigidbody.x: " + _rigidbody.velocity.x + " / .y: " + _rigidbody.velocity.y);
            _currentDamageType = damageType;
            _tempVelocity = _rigidbody.velocity;
            _isSwoon = true;
            _rigidbody.Sleep();
            _lastAnimationHash = _anim.GetCurrentAnimatorStateInfo(0).nameHash;

            if (_currentDamageType == DamageType.type.BOMB) _swoonedTimeAmount = duration * _bombRecoveryRate;
            else if (_currentDamageType == DamageType.type.MISSILE) _swoonedTimeAmount = duration * _missileRecoveryRate;
            else if (_currentDamageType == DamageType.type.TRAP) _swoonedTimeAmount = duration * _trapRecoveryRate;
            else _swoonedTimeAmount = duration;

            DOTween.Kill("PlayerManager.Swoon." + GetInstanceID());
            DOVirtual.DelayedCall(_swoonedTimeAmount, OnSwoonEnd).SetId("PlayerManager.Swoon." + GetInstanceID());

            if(PlayerSwooned != null) PlayerSwooned(_playerID, _currentDamageType);
        }
        
        // 이동정지
        // 동작 일시정지
        // 녹화정지
        // 컨트롤러 사용 불가
        // 아이템 사용 불가
        
    }

    private void OnSwoonEnd()
    {
        Recover();
    }

    // 정신차리다
    private void Recover()
    {
        _isSwoon = false;
        _rigidbody.WakeUp();
        _rigidbody.velocity = _tempVelocity;
        _anim.Play(_lastAnimationHash);
        _anim.speed = 1f;

        if (PlayerRecovered != null) PlayerRecovered(_playerID, _currentDamageType);
    }

    public void MoveToPortal(Vector3 offsetPos)
    {
        Vector3 pos = gameObject.transform.localPosition;
        pos.x = offsetPos.x;
        pos.y = offsetPos.y;
        gameObject.transform.localPosition = pos;
    }

    // Move
    private bool _hasJumpDirection = false;
    private float _prevAdditionalFloorSpeed = 0f;
    private float _prevAdditionalBoostFloorSpeed = 0f;
    public void Move(float axis, float velocityX, bool isBoosting)
	{
        if(!_isSwoon)
        {
            _moveAxis = axis;
            float velocityY = _rigidbody.velocity.y;

            if (_movementState == MovementState.MOVEMENT_JUMP_GOING_UP) // 점프를 뛰어 오를 때
            {
                // 점프시 이동 (달리던 속도에 따라)
                //float jumpSpeed = abilityData.jumpSpeed;
                //if (isBoost) jumpSpeed = abilityData.boostJumpSpeed;

                //Debug.Log("????? _targetJumpSpeed: " + _targetJumpSpeed);
                float jumpSpeed;
                if (_isWallJumping) jumpSpeed = _targetJumpSpeed + _additionalJumpSpeed;
                else jumpSpeed = _targetJumpSpeed;
                
                if (_currentJumpDirection > 0) GetComponent<Rigidbody>().velocity = new Vector3(jumpSpeed, velocityY, 0.0f);
                else if (_currentJumpDirection < 0) GetComponent<Rigidbody>().velocity = new Vector3(-jumpSpeed, velocityY, 0.0f);
                //else if (_currentJumpDirection < 0) GetComponent<Rigidbody>().velocity = new Vector3(-moveController.Result.GetMaxMoveSpeed(), vy, 0.0f);
            }
            else if(_movementState == MovementState.MOVEMENT_JUMP_FALLING) // 점프 후 떨어질 때
            {
                if (_currentJumpDirection > 0) GetComponent<Rigidbody>().velocity = new Vector3(_targetJumpSpeed, velocityY - _additionalFallingSpeed, 0.0f);
                else if (_currentJumpDirection < 0) GetComponent<Rigidbody>().velocity = new Vector3(-_targetJumpSpeed, velocityY - _additionalFallingSpeed, 0.0f);
            }
            else if(_movementState == MovementState.MOVEMENT_RUN_FALLING) // 뛰다가 떨어질 때
            {
                // 떨어질 때 이동 (고정)
                GetComponent<Rigidbody>().velocity = new Vector3(_fallStartAxis, velocityY + 0.05f - (_additionalFallingSpeed / 2), 0.0f);
            }
            else if(_locationState == LocationStaete.LOCATION_ON_FLOOR)
            {
                // 바닦에서 달릴 때

                float velocityOnFloorX = 0f;

                if (isBoosting) velocityOnFloorX = velocityX + (_moveAxis * _additionalBoostFloorSpeed);
                else velocityOnFloorX = velocityX + (_moveAxis * _additionalFloorSpeed);

                _rigidbody.velocity = new Vector3(velocityOnFloorX, velocityY, 0.0f);
                if(_anim != null) _anim.SetFloat("Direction", _moveAxis);

                //if(_prevAdditionalFloorSpeed != _additionalFloorSpeed)
                //{
                //    Debug.Log("+++++++++++++++++++++++ 평지 추가 속도 변경 / 이전속도: " + _prevAdditionalFloorSpeed + " / 현재속도: " + _additionalFloorSpeed);
                //    _prevAdditionalFloorSpeed = _additionalFloorSpeed;
                //}

                if(_prevAdditionalBoostFloorSpeed != _additionalBoostFloorSpeed)
                {
                    Debug.Log("+++++++++++++++++++++++ 부스터 추가 평지 속도 변경 / 이전속도: " + _prevAdditionalBoostFloorSpeed + " / 현재속도: " + _additionalBoostFloorSpeed);
                    _prevAdditionalBoostFloorSpeed = _additionalBoostFloorSpeed;
                }
            }
        }
	}

    public void StopMove()
    {
        _moveManager.StopMove();
    }

    public void StartRace()
    {
        ActivateMoveController();
    }

    private void SmoothStopMvmt(float time)
    {
        _moveManager.SmoothStopMvmt(time);
    }

    private void ReplaceToStartPosition()
    {
        _dontGoThroughThings.enabled = false;
        gameObject.transform.localPosition = _startPosition;
        _dontGoThroughThings.Init();
        _dontGoThroughThings.enabled = true;
    }


    //public Vector3 GetMoveVelocity()
    //{
    //    return GetComponent<Rigidbody>().velocity;
    //}

    public string GetLocationState()
    {
        return _locationState;
    }

    public string GetMovementState()
    {
        return _movementState;
    }

    

    public string GetOrderDirection()
    {
        return _moveManager.GetOrderDirectionState();
    }

    public float GetSpeed()
    {
        return _speedChecker.GetSpeed();
    }

    public float GetSpeedX()
    {
        return _speedChecker.GetSpeedX();
    }

    public float GetSpeedY()
    {
        return _speedChecker.GetSpeedY();
    }

    //public RankData GetRankData()
    //{
    //    return _rankData;
    //}

    // Start / End Starting Boost
    private void StartBoost(float time)
    {
        _isBoost = true;

        _moveManager.StartBoost();
        _jumpManager.ChangeBoostState(true);

        boostEffect.ChangeBoostMode(time);

        //trail.startWidth = 0.3f;
    }

    private void EndBoost(float time)
    {
        _isBoost = false;

        _moveManager.EndBoost();
        _jumpManager.ChangeBoostState(false);

        boostEffect.ChangeNormalMode(time);

        //trail.startWidth = 0.05f;
    }

    private void ActivateMoveController()
    {
        _moveManager.Activate();
        boostEffect.ShowEffect(0);
    }

    public void DeactivateMoveController()
    {
        _moveManager.Deactivate();
        boostEffect.HideEffect(0);
    }

    public void Reset()
    {
        if (_isDebug) Debug.Log(_logPrefix + "Reset()");
        DeactivateMoveController();
        ReplaceToStartPosition();
        _currentRound = 0;
        _currentCheckPointValue = 0f;
        _swoonedTimeAmount = 0f;
        _wallTriggerNum = 0;
        _isTouchedWall = false;
        _isFinishedRace = false;
        _startingBoostManager.Reset();
    }

    public bool IsSwoon()
    {
        return _isSwoon;
    }



    #region Public Functions
    public void SetTargetJumpSpeed(float speed)
    {
        // Debug.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!: " + speed);
        _targetJumpSpeed = speed;
    }

    public ItemManager GetItemManager()
    {
        return _itemManager;
    }

    public AbilityData GetAbilityData()
    {
        return abilityData;
    }

    public float GetCurrentCheckPointValue()
    {
        return _currentCheckPointValue;
    }

    public int GetCurrentRound()
    {
        return _currentRound;
    }

    public int GetPlayerID()
    {
        // 참가 ID
        return _playerID;
    }

    public string GetNickname()
    {
        return _nickname;
    }

    public float GetDirection()
    {
        int hash = _anim.GetCurrentAnimatorStateInfo(0).nameHash;
        float result = 0.0f;
        if (hash == Animator.StringToHash("Main.RightJump") || hash == Animator.StringToHash("Main.RightRun")) result = 1;
        else if (hash == Animator.StringToHash("Main.LeftJump") || hash == Animator.StringToHash("Main.LeftRun")) result = -1;
        //Debug.Log("-------------------------- result: " + result);
        return result;
    }

    public float swoonedTimeAmount
    {
        get { return _swoonedTimeAmount; }
    }

    public bool isFinishedRace
    {
        get { return _isFinishedRace; }
    }


    public void UpdateRank(int rank)
    {
        //_rankData.rank = rank;
        _participantData.rank = rank;

        //_rankData.totalRecord = raceRecord;
        //_participantData.record = raceRecord;

        // Start / Check Point에서 Item 지급
        //Debug.Log("-------------- UpdateRank / _currentCheckPointValue: " + _currentCheckPointValue);
        if(_currentCheckPointValue != 0 && _currentCheckPointValue != 3) // 시작시점, 마지막 통과시점이 아니라면
        {
            //Debug.Log("RequestItemProvision--------------------");
            RequestItemProvision(); // 아이템 제공을 요청함
        }
        if (RankChanged != null) RankChanged(rank);
        // Start Point에서만 Item 지급
        // if(_currentCheckPointValue % 1 == 0) // 시작포인트를 통과했고
        // {
        //     if(_currentRound != 0) // 시작시점이 아니고
        //     {
        //         RequestItemProvision(); // 아이템 제공을 요청함
        //     }
        // }
    }

    private int _itemGetCount = 0;

    private void RequestItemProvision()
    {
        //Debug.Log("????????????????????????? 아이템 내놔!");
        IItem item;

        if(RaceModeInfo.isTutorialMode)
        {
            if (_itemGetCount == 0)
            {
                item = _itemProvider.GetSpecipicItem(0);
                _itemGetCount = 1;
            }
            else if(_itemGetCount == 1)
            {
                item = _itemProvider.GetSpecipicItem(5);
                _itemGetCount = 2;
            }
            else
            {
                item = _itemProvider.GetItem(_participantData.rank);
            } 
        }
        else
        {
            item = _itemProvider.GetItem(_participantData.rank);
        }

        if(!_itemManager.SetItem(item))
        {
            Destroy(((MonoBehaviour)item).gameObject);
        }
    }


    public void Boost(float duration, float mvmtSpeedIncRate)
    {
        float incSpeed = (abilityData.run / 100f) * mvmtSpeedIncRate;
        float boostMvmtSpeed = abilityData.run + incSpeed;
        _moveManager.SetBoostRunSpeed(boostMvmtSpeed);
        _currentDefaultMvmtSpeed = boostMvmtSpeed;
        _targetJumpSpeed = boostMvmtSpeed;
        StartBoost(0f);
        DOTween.Kill("Player.Boost." + GetInstanceID());
        DOVirtual.DelayedCall(duration, OnBoostEnded).SetId("Player.Boost." + GetInstanceID());
    }

    private void OnBoostEnded()
    {
        EndBoost(0.1f);
        _currentDefaultMvmtSpeed = abilityData.run;
        _targetJumpSpeed = abilityData.run;
    }


    public void Protect(float duration)
    {
        if(!_isProtecting)
        {
            _isProtecting = true;
            DOTween.Kill("Player.Protect." + GetInstanceID());
            DOVirtual.DelayedCall(duration, OnProtectionEnded).SetId("Player.Protect." + GetInstanceID());
        }
    }

    private void OnProtectionEnded()
    {
        _isProtecting = false;
    }

    public ParticipantData GetParticipantData()
    {
        return _participantData;
    }

    public float currentDefaultMvmtSpeed
    {
        get { return _currentDefaultMvmtSpeed; }
    }

    public Character GetCharacter()
    {
        return _characterGO.GetComponent<Character>();
    }



    // 추가적인 평지 속도
    public void SetAdditionalFloorSpeed(float speed)
    {
        _additionalFloorSpeed = speed;
    }

    // 추가적인 평지 부스트 속도
    public void SetAdditionalBoostFloorSpeed(float speed)
    {
        _additionalBoostFloorSpeed = speed;
    }

    // 추가적인 하강 속도
    public void SetAdditionalFallingSpeed(float speed)
    {
        _additionalFallingSpeed = speed;
    }

    public void SetAdditionalJumpSpeed(float speed)
    {
        _additionalJumpSpeed = speed;
    }

    // 폭탄 피해 기절시간 감소 비율
    public void SetBombRecoveryRate(float rate)
    {
        _bombRecoveryRate = rate;
    }

    public void SetMissileRecoveryRate(float rate)
    {
        _missileRecoveryRate = rate;
    }

    public void SetTrapRecoveryRate(float rate)
    {
        _trapRecoveryRate = rate;
    }
    #endregion
}