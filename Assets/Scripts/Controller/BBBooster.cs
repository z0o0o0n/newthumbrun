﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Data;
using Com.Mod.ThumbRun.Event;

namespace Com.Mod.ThumbRun.Controller
{
    public class BBBooster : MonoBehaviour
    {
        public event BBBoosterEvent.BoostEventHandler Boost;

        [SerializeField]
        private BBBoosterData _bbBoosterData;
        [SerializeField]
        private PlayerManager _playerManager;
        private bool _inBeatBoard = false;

        public bool inBeatBoard
        {
            get { return _inBeatBoard; }
        }



        void Awake()
        {
            _bbBoosterData.Prepared += OnDataPrepared;
            _playerManager.WallJumpBegin += OnPlayerWallJumpBegin;
        }

        void Start()
        {

        }

        void Update()
        {

        }

        void OnDestroy()
        {
            _bbBoosterData.Prepared -= OnDataPrepared;
            _playerManager.WallJumpBegin -= OnPlayerWallJumpBegin;
        }



        #region Private Functions
        private void OnDataPrepared()
        {

        }

        private void OnPlayerWallJumpBegin(int direction)
        {
            if(_inBeatBoard)
            {
                float playerJumpSpeed = _playerManager.abilityData.jumpSpeed;
                float targetJumpSpeed = playerJumpSpeed + ((playerJumpSpeed / 100) * _bbBoosterData.GetRawData().bbBoostSpeedIncRate);
                _playerManager.SetTargetJumpSpeed(targetJumpSpeed);

                if(Boost != null) Boost(_playerManager.transform.localPosition, direction);
            }
        }
        #endregion



        #region Public Functions
        public void EnterPlayer(BBDirection.type direction)
        {
            _inBeatBoard = true;
        }

        public void ExitPlayer(BBDirection.type direction)
        {
            _inBeatBoard = false;
        }
        #endregion
    }
}