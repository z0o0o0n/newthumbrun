﻿using UnityEngine;
using Com.Mod.TimeStamp;
using System;
using DG.Tweening;
using Com.Mod.ThumbRun.User.View;
using Com.Mod.ThumbRun.User.Application;
using UnityEngine.Advertisements;
using Com.Mod.ThumbRun.Analytics.Application;

namespace Com.Mod.ThumbRun.Controller
{
    public class FreeGoldProvider : MonoBehaviour 
    {
        public delegate void FreeGoldProvisionEvent();
        public event FreeGoldProvisionEvent FreeGoldTimeOut;
        public event FreeGoldProvisionEvent RewardADTimeOut;

        [SerializeField]
        private FreeGoldProvisionData _data;
        [SerializeField]
        private GoldDisplay _goldManager;
        [SerializeField]
        private UIButton _popupButton;
        [SerializeField]
        private GoldInfoPopup _popup; 
		[SerializeField]
		private UserService _userService;
        private AnalyticsService _analyticsService;

        void Awake ()
        {
            UIEventListener.Get(_popupButton.gameObject).onClick += OnPopupButtonClick;

            _analyticsService = GameObject.FindGameObjectWithTag("Analytics").GetComponent<AnalyticsService>();
            _analyticsService.Prepared += OnAnalyticsServicePrepared;
        }

        void Start () 
        {
            TimeStampManager.instance.Initialized += OnTimeStampInitialized;
            TimeStampManager.instance.Timeout += OnTimeStampTimeout;

            CoolingTimeStampManager.instance.CoolingEnded += OnCoolingTimeStampEnd;

            NewADManager.instance.Success += OnADSuccess;
        }
        
        void Update () 
        {
            // string timeString = "End";
            // TimeSpan remainingTime = _timeStampManager.GetRemainingTime("FreeGold");
            // if (remainingTime.TotalSeconds > 0) {
			// timeString = string.Format("{0}:{1:D2}:{2:D2}", remainingTime.Hours, remainingTime.Minutes, remainingTime.Seconds);
            // Debug.Log("FreeGold: " + timeString);
		    // }

            //if(Input.GetKeyUp(KeyCode.F))
            //{
            //    PlayAD();
            //}
        }

        void OnGUI()
        {

        }

        void OnDestroy()
        {
            if(TimeStampManager.instance != null)
            {
                TimeStampManager.instance.Initialized -= OnTimeStampInitialized;
                TimeStampManager.instance.Timeout -= OnTimeStampTimeout;
            }

            if (CoolingTimeStampManager.instance != null)
            {
                CoolingTimeStampManager.instance.CoolingEnded += OnCoolingTimeStampEnd;
            }

            if (_popupButton) UIEventListener.Get(_popupButton.gameObject).onClick -= OnPopupButtonClick;

            NewADManager.instance.Success -= OnADSuccess;
            _analyticsService.Prepared -= OnAnalyticsServicePrepared;
        }



        #region Properties
        public int rewardADGold
        {
            get{
                return _data.rewardADGold;
            }
        }

        public int freeGold
        {
            get{
                return _data.freeGold;
            }
        }
        #endregion



        #region Private Functions
        private void OnAnalyticsServicePrepared()
        {
            if(_analyticsService.GetExecuteCount() <= 1)
            {
                CoolingTimeStampManager.instance.ResetAllCoolingTimestamp();
            }
        }

        private void OnTimeStampInitialized()
        {
            Debug.Log("FreeGoldProvicer - Init TimeStamp");
        }

        private void OnTimeStampTimeout(string timeStampKey)
        {
            if(timeStampKey == "FreeGold")
            {
                if(FreeGoldTimeOut != null) FreeGoldTimeOut();
            }
            else if(timeStampKey == "RewardAD")
            {
                //if(RewardADTimeOut != null) RewardADTimeOut();
            }
        }

        private void OnCoolingTimeStampEnd(CoolingTimeStamp coolingTimeStamp)
        {
            if(coolingTimeStamp.key == "RewardAdGold")
            {
                if (RewardADTimeOut != null) RewardADTimeOut();
            }
        }

        private void OnPopupButtonClick(GameObject sender)
        {
            _popup.Open();
        }

        private void OnADSuccess(string key)
        {
            if(key == "GoldRewardAD")
            {
                _userService.Save(_data.rewardADGold);
            }
        }
        #endregion



        #region Public Functions
        public void OpenOfferWall()
        {
            // 오퍼월 연결
        }

        public void ProvideFreeGold()
        {
            TimeStampManager.instance.Reset("FreeGold", false);
            _userService.Save(_data.freeGold);
            //_goldManager.Save(_data.freeGold);
        }

        public bool PlayAD()
        {
            bool result = NewADManager.instance.PlayAD("GoldRewardAD");
            if (result)
            {
                Debug.Log("play AD.");
                TimeStampManager.instance.Reset("RewardAD", false);
                CoolingTimeStampManager.instance.Use("RewardAdGold");
            }
            return result;
        }

        public bool IsADReady()
        {
            return NewADManager.instance.IsReady();
        }

        public string GetFreeGoldRemainingTime()
        {
            string result;
            TimeSpan remainingTime = TimeStampManager.instance.GetRemainingTime("FreeGold");
			result = string.Format("{0:D2}:{1:D2}", remainingTime.Minutes, remainingTime.Seconds);
            // Debug.Log("FreeGold: " + result);
            return result;
        }

        public bool IsFreeGoldWaiting()
        {
            bool result = false;
            TimeSpan remainingTime = TimeStampManager.instance.GetRemainingTime("FreeGold");
            if(remainingTime.TotalSeconds > 0) result = true;
            return result;
        }

        public string GetRewardADRemainingTime()
        {
            string result;
            TimeSpan remainingTime = CoolingTimeStampManager.instance.GetRemainingTime("RewardAdGold");
			result = string.Format("{0:D2}:{1:D2}", remainingTime.Minutes, remainingTime.Seconds);
            // Debug.Log("FreeGold: " + result);
            return result;
        }

        public bool IsRewardADWaiting()
        {
            bool result = false;
            TimeSpan remainingTime = TimeStampManager.instance.GetRemainingTime("RewardAD");
            if(remainingTime.TotalSeconds > 0) result = true;
            return result;
        }
        #endregion
    }
}