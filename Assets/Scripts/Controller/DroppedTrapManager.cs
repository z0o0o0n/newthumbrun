﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DroppedTrapManager : MonoBehaviour 
{
	private static List<IItem> trapList = new List<IItem>();

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

	public static void PushTrap(IItem trap)
	{
		trapList.Add(trap);
	}

	public static void RemoveTrap(IItem trap)
	{
		for(int i = 0; i < trapList.Count; i++)
		{
			if(((MonoBehaviour)trapList[i]).gameObject == ((MonoBehaviour)trap).gameObject)
            {
                trapList.RemoveAt(i);
				break;
			}
		}
	}

	public static void DestroyAllTraps()
	{
		for(int i = 0; i < trapList.Count; i++)
		{
            if ((MonoBehaviour)trapList[i] != null) Destroy(((MonoBehaviour)trapList[i]).gameObject);
		}
		trapList.Clear();
	}
}
