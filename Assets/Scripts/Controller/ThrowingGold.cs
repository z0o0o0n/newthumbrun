﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ThrowingGold : MonoBehaviour 
{
	[SerializeField]
	private AudioSource _audioSource;
	[SerializeField]
	private List<AudioClip> _throwGoldAudios;
	private UISprite _sprite;

	public int index;

	void Awake ()
	{
		_sprite = GetComponent<UISprite>();
	}

	void Start () 
	{

	}
	
	void Update () 
	{
	
	}

	public void SetDepth(int depth)
	{
		_sprite.depth = depth;
	}

	public void PlayThrowAudio(float delay)
	{
		int index = (int)Random.Range(0f, 2.9f);
		_audioSource.clip = _throwGoldAudios[index];
		_audioSource.PlayDelayed(delay);
		// _audioSource.Play();
	}
}
