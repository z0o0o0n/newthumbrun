﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Setting.View;
using Com.Mod.ThumbRun.Setting.Application;
using Com.Mod.ThumbRun.SignUp.View;
using Com.Mod.ThumbRun.UI.Popup;
using Com.Mod.ThumbRun.Coupon.View;

namespace Com.Mod.ThumbRun.Controller
{
	public class SettingMenuPopup : BasicPopup 
	{
		[SerializeField]
		private UIButton _popupButton;
		[SerializeField]
		private UIBasicButton _closeButton;
        [SerializeField]
		private UIOnOffButton _bgmOnOffButton;
        [SerializeField]
        private UIDynamicButton _ratingButton;
        [SerializeField]
        private UIDynamicButton _facebookButton;
        [SerializeField]
        private UIDynamicButton _customerButton;
        [SerializeField]
        private UIDynamicButton _twitterButton;
        [SerializeField]
        private UIDynamicButton _moreGamesButton;
        [SerializeField]
        private UIDynamicButton _couponButton;
        [SerializeField]
        private LanguageSettingPopup _languageSettingPopup;
		[SerializeField]
		private CouponPopup _couponPopup;
		[SerializeField]
		private CreditPopup _creditPopup;
		[SerializeField]
		private SignUpPopup _signUpPopup;
		[SerializeField]
		private BgmManager _bgmManager;
        [SerializeField]
        private string _facebookURL;
        [SerializeField]
        private string _twitterURL;
        [SerializeField]
        private string _customerURL;
        [SerializeField]
        private string _iOSRatingURL;
        [SerializeField]
        private string _androidRatingURL;
        [SerializeField]
        private string _iOSMoreGamesURL;
        [SerializeField]
        private string _androidMoreGaemsURL;
        private SettingService _settingService;


        void Awake ()
		{
			gameObject.transform.localPosition = Vector3.zero;
			UIEventListener.Get(_popupButton.gameObject).onClick += OnPopupButtonClick;
			_closeButton.On_Click += OnCloseButtonClick;

            _settingService = GameObject.FindGameObjectWithTag("Setting").GetComponent<SettingService>();
            _settingService.Prepared += OnSettingServicePrepared;
            if (_settingService.isPrepared) OnSettingServicePrepared();

            _bgmOnOffButton.On += OnBgmButtonOn;
			_bgmOnOffButton.Off += OnBgmButtonOff;

            _ratingButton.Click += OnRatingButtonClick;
            _facebookButton.Click += OnFacebookButtonClick;
            _customerButton.Click += OnCustomerButtonClick;
            _twitterButton.Click += OnTwitterButtonClick;
            _moreGamesButton.Click += OnMoreGamesButtonClick;
            
            ClosePopup();
        }

		void Start () 
		{
		
		}
		
		void Update () 
		{
		}

		void OnDestroy()
		{
			UIEventListener.Get(_popupButton.gameObject).onClick -= OnPopupButtonClick;
			_closeButton.On_Click -= OnCloseButtonClick;
			
			_settingService.Prepared -= OnSettingServicePrepared;

			_bgmOnOffButton.On -= OnBgmButtonOn;
			_bgmOnOffButton.Off -= OnBgmButtonOff;

            _ratingButton.Click -= OnRatingButtonClick;
            _facebookButton.Click -= OnFacebookButtonClick;
            _customerButton.Click -= OnCustomerButtonClick;
            _twitterButton.Click -= OnTwitterButtonClick;
            _moreGamesButton.Click -= OnMoreGamesButtonClick;
        }





		public void OpenPopup()
		{
			base.Open();
		}

		public void ClosePopup()
		{
			base.Close();
		}

        public void PressLanguageSettingButton()
        {
			ClosePopup();
            _languageSettingPopup.OpenPopup();
        }

		public void PressCreaditButton()
		{
			ClosePopup();
			_creditPopup.OpenPopup();
		}

		public void PressNicknameAndNationButton()
		{
			ClosePopup();
			_signUpPopup.OpenPopup(true);
		}

		public void PressCouponPopup()
		{
			ClosePopup();
			_couponPopup.OpenPopup();
		}





		private void OnBgmButtonOn()
		{
            _settingService.SetBGMState(1);
            _bgmManager.Play(0);
        }

		private void OnBgmButtonOff()
		{
            _settingService.SetBGMState(-1);
            _bgmManager.Stop();
		}

		private void OnSettingServicePrepared()
		{
            if (!_settingService.isPrepared) return;

			if(_settingService.IsBGMOn() == 1)
			{
                _bgmManager.SetOnOff(true);
                _bgmManager.Play(0);
                _bgmOnOffButton.ChangeOn();
            }
			else
			{
                _bgmManager.SetOnOff(false);
                _bgmManager.Stop();
                _bgmOnOffButton.ChangeOff();
			}

            if (_settingService.IsReviewVersion()) _couponButton.gameObject.SetActive(false);
        }

		private void OnPopupButtonClick(GameObject sender)
		{
            // Debug.Log("*************** sender: " + sender.name);
			OpenPopup();
		}

		private void OnCloseButtonClick()
		{
			ClosePopup();
		}

        private void OnRatingButtonClick(GameObject target)
        {
            string url;
#if UNITY_IPHONE
            url = _iOSRatingURL;
#elif UNITY_ANDROID
            url = _androidRatingURL;
#endif
            Application.OpenURL(url);
        }

        private void OnFacebookButtonClick(GameObject target)
        {
            Application.OpenURL(_facebookURL);
        }

        private void OnCustomerButtonClick(GameObject target)
        {
            Application.OpenURL(_customerURL);
        }

        private void OnTwitterButtonClick(GameObject target)
        {
            Application.OpenURL(_twitterURL);
        }

        private void OnMoreGamesButtonClick(GameObject target)
        {
            string url;
#if UNITY_IPHONE
            url = _iOSMoreGamesURL;
#elif UNITY_ANDROID
            url = _androidMoreGaemsURL;
#endif
            Application.OpenURL(url);
        }
    }
}