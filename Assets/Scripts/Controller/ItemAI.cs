﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class ItemAI : MonoBehaviour 
{
    private IItem _currentItem;
    private int _currentItemID = -1;
    private PlayerManager _playerManager;
    private RaceParticipationManager _raceParticipationManager;
    private List<IRaceParticipant> _replayerList;
    private IRaceParticipant _user;
    private List<IRaceParticipant> _others;
    private RaceManager _raceManager;
    [SerializeField]
    private bool _isDisabled = true;

    void Awake ()
    {
        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
        _raceManager.RaceEnd += OnRaceEnd;
        _raceManager.RaceTimeout += OnRaceTimeOut;
        _raceManager.RaceClose += OnRaceClose;
    }

	void Start () 
    {
        _playerManager = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerManager>();
        _raceParticipationManager = GameObject.FindGameObjectWithTag("RaceParticipationManager").GetComponent<RaceParticipationManager>();

        _others = new List<IRaceParticipant>();
        _others.Add(_playerManager);

        _replayerList = _raceParticipationManager.GetReplayerList();
        for(int i = 0; i < _replayerList.Count; i++)
        {
            if(((MonoBehaviour)_replayerList[i]).gameObject == gameObject)
            {
                _user = _replayerList[i];
            }
            else
            {
                _others.Add(_replayerList[i]);
            }
        }

        //Debug.Log("??????????///////////// tutorial: " + RaceModeInfo.isTutorialMode);
        if(!RaceModeInfo.isTutorialMode) RunAI();
    }
	
	void Update () 
    {
        if(_isDisabled) return; 
        
        if(_currentItemID == 5)
        {
            IdentifyVictim(_user, _others);
        }
    }

    private void OnDestroy()
    {
        _raceManager.RaceEnd -= OnRaceEnd;
        _raceManager.RaceTimeout -= OnRaceTimeOut;
        _raceManager.RaceClose -= OnRaceClose;
    }




    private void OnRaceEnd()
    {
        StopAI();
    }

    private void OnRaceTimeOut()
    {
        StopAI();
    }

    private void OnRaceClose()
    {

    }





    private void RunAI()
    {
        //Debug.Log("==========> _isDisabled: " + _isDisabled);
        _isDisabled = false;
    }

    private void StopAI()
    {
        _isDisabled = true;
        DOTween.Kill("MissileShot." + GetInstanceID());
        DOTween.Kill("TrapDropped." + GetInstanceID());
        DOTween.Kill("TimeStopped." + GetInstanceID());
    }

    public bool SetItem(IItem item)
    {
        //Debug.Log("==========> _isDisabled: " + _isDisabled);
        if (_isDisabled) return false;
        if(HasItem()) return false;

        if(!HasItem())
        {
            _currentItem = item;
            _currentItemID = item.id;
            GameObject itemGO = ((MonoBehaviour)item).gameObject; 
            itemGO.transform.parent = ((MonoBehaviour)_user).gameObject.transform;
            itemGO.transform.localPosition = Vector3.zero;
            itemGO.transform.localScale = Vector3.one;

            if (_currentItemID == 4)
            {
                float randomDelay = Random.Range(1f, 10f);
                DOTween.Kill("MissileShot." + GetInstanceID());
                DOVirtual.DelayedCall(randomDelay, OnMissileShot).SetId("MissileShot." + GetInstanceID());
            }
            else if (_currentItemID == 6)
            {
                float randomDelay = Random.Range(1f, 10f);
                DOTween.Kill("TrapDropped." + GetInstanceID());
                DOVirtual.DelayedCall(randomDelay, OnTrapDropped).SetId("TrapDropped." + GetInstanceID());
            }
            else if(_currentItemID == 8)
            {
                float randomDelay = Random.Range(1f, 10f);
                DOTween.Kill("TimeStopped." + GetInstanceID());
                DOVirtual.DelayedCall(randomDelay, OnTimeStopped).SetId("TimeStopped." + GetInstanceID());
            }
        }
        return true;
    }

    public bool Protect()
    {
        if (_isDisabled) return false;

        if(_currentItemID == 2 || _currentItemID == 3) 
        {
            _currentItem.Use(_user, _others);
            _currentItemID = -1;
            return true;
        }
        else return false;
    }



    private bool HasItem()
    {
        bool result;
        if (_currentItemID == -1) result = false;
        else result = true;
        return result;
    }

    private void OnMissileShot()
    {
        _currentItem.Use(_user, _others);
        _currentItemID = -1;
    }

    private void OnTrapDropped()
    {
        _currentItem.Use(_user, _others);
        _currentItemID = -1;
    }

    private void OnTimeStopped()
    {
        _currentItem.Use(_user, _others);
        _currentItemID = -1;
    }

    private void IdentifyVictim(IRaceParticipant user, List<IRaceParticipant> others)
    {
        GameObject userGO = ((MonoBehaviour)_user).gameObject;

        for (int i = 0; i < others.Count; i++)
        {
            IRaceParticipant raceParticipant = others[i];
            GameObject otherGO = ((MonoBehaviour)raceParticipant).gameObject;

            float distance = Vector2.Distance(userGO.transform.localPosition, otherGO.transform.localPosition);
            float minArea = Random.Range(0f, 0.5f);
            float maxArea = Random.Range(1f, 2f);
            if (minArea < distance && distance < maxArea)
            {
                _currentItem.Use(_user, _others);
                _currentItemID = -1;
                break;
            }
        }
    }
}
