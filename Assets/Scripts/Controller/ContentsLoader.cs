﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ContentsLoader : MonoBehaviour
{
    private string _sceneName;
    private bool _isLoading = false;
    private AsyncOperation _asyncOperaction;

	void Start ()
    {

	}
	
    public void Load(string sceneName)
    {
        _sceneName = sceneName;
        _asyncOperaction = Application.LoadLevelAsync(_sceneName);
    }

    

    

    private void OnCompleteStartMotion()
    {
        if (!_isLoading)
        {
            _isLoading = true;
            LoadLevel();
        }

        //if (!_asyncOperaction.isDone) loadingImage.PlayLoadingMotion();
    }

    private void OpenUIBg()
    {
        //shutter.transform.DOLocalMove(new Vector3(0.0f, shutter.height, 0.0f), 0.5f).SetEase(Ease.InCubic);
    }

    private void LoadLevel()
    {
        _asyncOperaction = Application.LoadLevelAsync(_sceneName);
    }

    void OnDestroy()
    {
        //loadingImage.OnCompleteLoadingMotion -= OnCompleteStartMotion;
    }
}
