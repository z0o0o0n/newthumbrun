﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Data;
using Com.Mod.ThumbRun.Test;
using Com.Mod.ThumbRun.User.View;
using Com.Mod.ThumbRun.User.Application;

namespace Com.Mod.ThumbRun.Control
{
	public class LuckyChance : MonoBehaviour 
	{
		public event LuckyChanceEvent.LuckyChanceEventHandler Prepared;
		public event LuckyChanceEvent.LuckyChanceEventHandler OutcomeAdded;
		public event LuckyChanceEvent.LuckyChanceEventHandler ChanceActivated;

		public LuckyChanceData data;
		
		private static GameObject _instance;

		[SerializeField]
		private UserService _userService;
		//private GoldDisplay _goldDisplay;

		void Awake ()
		{
			if(_instance == null)
			{
				_instance = gameObject;
				data.Prepared += OnDataPrepared;
				DontDestroyOnLoad(gameObject);
			}
			else
			{
				Destroy(gameObject);
			}
		}

		void Start () 
		{
			
		}

		void Update () 
		{
		}

		void OnDestroy()
		{
			data.Prepared -= OnDataPrepared;
		}



		#region Public Functions
		public void Prepare()
		{
			//_goldDisplay = GameObject.FindGameObjectWithTag("GoldDisplay").GetComponent<GoldDisplay>();

			if(!data.isPrepared)  data.Prepare();
			else OnDataPrepared();
		}

		public void SaveGold()
		{
            _userService.Save(data.bonusGold);
		}

		public void ResetLuckyChance()
		{
			data.Reset();
		}
		#endregion



		#region Private Function
		private void UpdateLuckyChance()
		{
			if (!data.isChanceActivated) 
			{
				UpdateRemainingRaceCount ();
				UpdateDiameter ();
				LogOutcomeList ();

				if (data.raceOutcomeList.Count >= data.goalRaceCount) 
				{
					data.isChanceActivated = true;
					if (ChanceActivated != null) ChanceActivated();
				}
			}
		}

		private void OnDataPrepared()
		{
			UpdateLuckyChance();
			if(Prepared != null) Prepared();
		}

		private void OnRaceFinished(bool outCome)
		{
			AddOutcome(outCome);
		}

		private void AddOutcome (bool outcome)
		{
			if (!data.isChanceActivated) 
			{
				data.AddRaceOutcome(outcome);
				UpdateLuckyChance();
				if (OutcomeAdded != null) OutcomeAdded ();
			}
		}

		private void LogOutcomeList()
		{
			string log = "raceOutcomeList - count: " + data.raceOutcomeList.Count + " - ";
			for (int i = 0; i < data.raceOutcomeList.Count; i++) 
			{
				if (i == data.raceOutcomeList.Count - 1) log += data.raceOutcomeList [i].ToString ();
				else log += data.raceOutcomeList [i].ToString () + ", ";
			}
			Debug.Log (log);
		}

		private void OnReset()
		{
			ResetLuckyChance();
		}
		#endregion



		#region Methods
		private void UpdateRemainingRaceCount()
		{
			data.remainingRaceCount = data.goalRaceCount - data.raceOutcomeList.Count;
		}

		private void UpdateDiameter()
		{
			int winCount = 0;
			for (int i = 0; i < data.raceOutcomeList.Count; i++) 
			{
				if (data.raceOutcomeList[i]) winCount++;
			}
			data.diameter = data.diameterList [winCount];
		}
		#endregion
	}
}