﻿using UnityEngine;
using System.Collections.Generic;
using Com.Mod.Io;
using System;
using DG.Tweening;
using Junhee.Utils;
using LitJson;
using Com.Mod.NGUI.LogConsole;
using Com.Mod.ThumbRun.User.Application;
using Com.Mod.ThumbRun.Analytics.Application;
using Com.Mod.ThumbRun.CharacterManagement.Application;
using Com.Mod.ThumbRun.LuckyChance.Application;

public class RaceParticipationManager : MonoBehaviour 
{
    public delegate void RaceParticipationEvent(IRaceParticipant player, List<IRaceParticipant> replayers);
    public event RaceParticipationEvent Recruited;

    [SerializeField]
    private PlayerManager _player;
    [SerializeField]
    private Shader _characterShader;
    [SerializeField]
    private CSVLoader _csvLoader;
    [SerializeField]
    private GameObject _replayerPrefab;
    [SerializeField]
    private TerrainManager _terrainManager;
    [SerializeField]
    private ServerMessenger _serverMessenger;
    [SerializeField]
    private DummyRacePlayDataCreator _dummyPlayerData;
    [SerializeField]
    private UserService _userService;
    private AnalyticsService _analyticsService;
    private CharacterManagementService _characterManagementService;
    private bool _isRecruied = false;
    private GameManager _gameManager;
    private StageData _stageData;
    private List<ParticipantData> _participantDataList = new List<ParticipantData>();
    private List<IRaceParticipant> _replayerList = new List<IRaceParticipant>();

    public bool isRecruited
    {
        get{ return _isRecruied; }
    }



	void Awake ()
	{
        _analyticsService = GameObject.FindGameObjectWithTag("Analytics").GetComponent<AnalyticsService>();
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        _characterManagementService = GameObject.FindGameObjectWithTag("CharacterManagement").GetComponent<CharacterManagementService>();
    }

	void Start () 
	{
        //LogConsole.instance.Log("current stage Id: " + RaceModeInfo.stageId);
        //LogConsole.instance.Log("current entry index: " + RaceModeInfo.entryIndex);
        _analyticsService.AddParticipantCount(RaceModeInfo.entryIndex);
    }
	
	void Update () 
	{

    }

    void OnDestroy()
    {
    }



    #region Private Functions
    private bool HasParticipant()
    {
        if(_replayerList.Count == 0) return false;
        else return true;
    }

    private void ClearParticipants()
    {
        for (int i = 0; i < _replayerList.Count; i++)
        {
            Replayer replayer = ((MonoBehaviour)_replayerList[i]).GetComponent<Replayer>();
            Destroy(replayer.gameObject);
        }

        _replayerList.Clear();
        _participantDataList.Clear();
        
        _clonedNicknameList.Clear();
        _clonedNicknameList = null;

        _clonedCharacterIdList.Clear();
        _clonedCharacterIdList = null;
    }

    private void RecruitTutorialPlayers()
    {
        for (int i = 0; i < 5; i++)
        {
            Debug.Log("!!!!!!!!!! replay data path: " + "ReplayData/ReplayData_" + RaceModeInfo.stageId + "_" + i);
            TextAsset data = (TextAsset)Resources.Load("ReplayData/ReplayData_" + RaceModeInfo.stageId + "_" + i) as TextAsset;
            byte[] ba = Convert.FromBase64String(data.text);

            short[] shortArray = new short[ba.Length / 2];
            Buffer.BlockCopy(ba, 0, shortArray, 0, ba.Length);
            List<short> shortList = new List<short>(shortArray);

            short hashLength = shortList[0];
            shortList.RemoveAt(0);
            List<short> hashList = shortList.GetRange(0, hashLength);
            shortList.RemoveRange(0, hashLength);

            short xposLength = shortList[0];
            shortList.RemoveAt(0);
            List<short> xposList = shortList.GetRange(0, xposLength);
            shortList.RemoveRange(0, xposLength);

            short yposLength = shortList[0];
            shortList.RemoveAt(0);
            List<short> yposList = shortList.GetRange(0, yposLength);
            shortList.RemoveRange(0, yposLength);

            float record = 10.000f;
            if (shortList.Count > 0)
            {
                List<short> recordList = shortList.GetRange(0, 2); // record는 2개 고정
                shortList.RemoveRange(0, 2);
                Debug.Log("--------- recordList[0]: " + recordList[0] + " / recordList[1]: " + recordList[1]);
                string recordString = Format.ConvertDigit(recordList[0].ToString(), 2, "0", "front") + "." + Format.ConvertDigit(recordList[1].ToString(), 3, "0", "front");
                record = float.Parse(recordString);
            }

            Debug.Log("----------------- record: " + record.ToString());
            ParticipantData participantData = new ParticipantData(record);
            participantData.nickname = _tutorialNicknames[i];
            participantData.level = 1;
            participantData.characterId = 55 + i;
            participantData.stateHashList = DecompressHash(hashList);
            participantData.countryCode = "001";
            participantData.posXList = xposList.ToArray();
            participantData.posYList = yposList.ToArray();
            _participantDataList.Add(participantData);

            GameObject instance = GameObject.Instantiate(_replayerPrefab);
            instance.transform.parent = transform.parent;
            instance.transform.localPosition = Vector3.zero;
            _replayerList.Add(instance.GetComponent<Replayer>());

            Replayer replayer = instance.GetComponent<Replayer>();
            replayer.SetID(i + 1); // 0은 user 1부터 replayer
            replayer.SetParticipantData(participantData);

            Vector3 startPosition = _terrainManager.GetTerrain().GetCheckPointManager().startPoint.transform.localPosition;
            startPosition.z = 1.5f;
            replayer.PrepareForRace(startPosition);
        }
        DOTween.Kill("Recruit." + GetInstanceID());
        DOVirtual.DelayedCall(3f, OnRecruited).SetId("Recruit." + GetInstanceID());
    }

    private void RecruitSinglePlayers()
    {
        //Debug.Log("---------------------------: RecruitMutiRace / Race Mode" + _raceManager.GetRaceMode());
        for (int i = 0; i < 5; i++)
        {
            Debug.Log("!!!!!!!!!! replay data path: " + "ReplayData/ReplayData_" + RaceModeInfo.stageId + "_" + i);
            TextAsset data = (TextAsset)Resources.Load("ReplayData/ReplayData_" + RaceModeInfo.stageId + "_" + i) as TextAsset;
            byte[] ba = Convert.FromBase64String(data.text);
            // Debug.Log("!!!!!!!!!!!!!!!!!! byte: " + ba.Length);

            short[] shortArray = new short[ba.Length / 2];
            Buffer.BlockCopy(ba, 0, shortArray, 0, ba.Length);
            List<short> shortList = new List<short>(shortArray);
            //debug.log("----------- shortlist.length: " + shortlist.count);

            short hashLength = shortList[0];
            shortList.RemoveAt(0);
            // Debug.Log("---------- hashLenght: " + hashLength);
            List<short> hashList = shortList.GetRange(0, hashLength);
            shortList.RemoveRange(0, hashLength);

            short xposLength = shortList[0];
            shortList.RemoveAt(0);
            // Debug.Log("---------- xposLength: " + xposLength);
            List<short> xposList = shortList.GetRange(0, xposLength);
            shortList.RemoveRange(0, xposLength);

            short yposLength = shortList[0];
            shortList.RemoveAt(0);
            // Debug.Log("---------- yposLength: " + yposLength);
            List<short> yposList = shortList.GetRange(0, yposLength);
            shortList.RemoveRange(0, yposLength);

            float record = 10.000f;
            if (shortList.Count > 0)
            {
                List<short> recordList = shortList.GetRange(0, 2); // record는 2개 고정
                shortList.RemoveRange(0, 2);
                //Debug.Log("--------- recordList[0]: " + recordList[0] + " / recordList[1]: " + recordList[1]);
                string recordString = Format.ConvertDigit(recordList[0].ToString(), 2, "0", "front") + "." + Format.ConvertDigit(recordList[1].ToString(), 3, "0", "front");
                record = float.Parse(recordString);
            }

            //Debug.Log("----------------- record: " + record.ToString());
            ParticipantData participantData = new ParticipantData(record);
            participantData.nickname = GetNickname();
            //participantData.record = record;
            participantData.level = (int) UnityEngine.Random.Range(1, 4);
            participantData.characterId = GetCharacterId(participantData.level);
            participantData.stateHashList = DecompressHash(hashList);
            participantData.countryCode = CountryCode.GetRandomCode();
            participantData.posXList = xposList.ToArray();
            participantData.posYList = yposList.ToArray();
            _participantDataList.Add(participantData);

            GameObject instance = GameObject.Instantiate(_replayerPrefab);
            instance.transform.parent = transform.parent;
            instance.transform.localPosition = Vector3.zero;
            _replayerList.Add(instance.GetComponent<Replayer>());

            Replayer replayer = instance.GetComponent<Replayer>();
            replayer.SetID(i + 1); // 0은 user 1부터 replayer
            replayer.SetParticipantData(participantData);

            Character character = replayer.GetCharacter();
            Material material = new Material(_characterShader);
            material.SetTexture("_Texture", character.tex);
            material.SetColor("_Color", Color.white);
            character.tintMaterial = material;
            character.GetComponent<SkinnedMeshRenderer>().material = material;

            Vector3 startPosition = _terrainManager.GetTerrain().GetCheckPointManager().startPoint.transform.localPosition;
            startPosition.z = 1.5f;
            replayer.PrepareForRace(startPosition);
        }
        DOTween.Kill("Recruit." + GetInstanceID());
        DOVirtual.DelayedCall(3f, OnRecruited).SetId("Recruit." + GetInstanceID());
    }

    private void RecruitMultiPlayers()
    {
        StageData stageData = StageDataManager.instance.GetStageDataByID(RaceModeInfo.stageId);
        _serverMessenger.Found += OnMatchFound;
        _serverMessenger.RequestMatchingData("ReplayData_" + stageData.id + "_" + stageData.replayDataVer, 5);
    }

    private int _matchedPlayerCount = 0;
    private void OnMatchFound(string wwwText)
    {
        JsonData json = JsonMapper.ToObject(wwwText);
        //Debug.Log(">>>>>>>>> player count: " + json["player"].Count);

        // 빠른 로컬 플레이어 참가자 수 결정
        int fastComPlayerCount = 0;
        int successionRankCount = _analyticsService.GetSuccessionRankingCount();
        double goldAmount = _userService.GetGoldAmount();
        if (200000 < goldAmount && goldAmount <= 600000 && successionRankCount >= 3)
        {
            // 보유 골드가 200000 이상, 연속 순위권 3회 이상 = 어려운 플레이어 2명
            Debug.Log("보유골드: " + goldAmount + " / 연속순위권: " + successionRankCount + " / 어려운상대 2명 추가");
            fastComPlayerCount = 0;
            AddComPlayers(fastComPlayerCount, true);
        }
        else if (600000 < goldAmount && goldAmount <= 1100000 && successionRankCount >= 2)
        {
            // 보유 골드가 600000 이상, 연속 순위권 3회 이상 = 어려운 플레이어 4명
            Debug.Log("보유골드: " + goldAmount + " / 연속순위권: " + successionRankCount + " / 어려운상대 3명 추가");
            fastComPlayerCount = 0;
            AddComPlayers(fastComPlayerCount, true);
        }
        else if (1100000 < goldAmount)
        {
            // 보유 골드가 1000000 이상 = 어려운 플레이어 3명
            Debug.Log("보유골드: " + goldAmount + " / 연속순위권: " + successionRankCount + " / 어려운상대 3명 추가");
            fastComPlayerCount = 1;
            AddComPlayers(fastComPlayerCount, true);
        }

        // 참가 플레이어 부족 여부 확인 (서버 + 빠른로컬)
        int serverPlayerCount = json["player"].Count;
        int normalComPlayerCount = 0;
        int usedServerPlayerCount = 0;
        int currentPlayerCount = serverPlayerCount + fastComPlayerCount;
        if (currentPlayerCount < 5)
        {
            int necessaryPlayerCount = 5 - currentPlayerCount; // 부족한 플레이어 수
            normalComPlayerCount = necessaryPlayerCount;
            usedServerPlayerCount = serverPlayerCount;
            AddComPlayers(normalComPlayerCount, false);
        }
        else if(currentPlayerCount > 5)
        {
            int overPlayerCount = currentPlayerCount - 5;
            usedServerPlayerCount = serverPlayerCount - overPlayerCount;
        }
        else
        {
            usedServerPlayerCount = serverPlayerCount;
        }

        Debug.Log("서버에서 가져온 플레이어: " + json["player"].Count + "/ 사용한 서버 플레이어: " + usedServerPlayerCount + " / 빠른 로컬 플레이어: " + fastComPlayerCount + " / 보통 로컬 플레이어: " + normalComPlayerCount);

        for (int i = 0; i < usedServerPlayerCount; i++)
        {
            string log = "< Server Player >";
            //Debug.Log("player " + i + ": " + json["player"][i].ToJson());
            JsonData player = json["player"][i];

            //Debug.Log("player index: " + i);
            byte[] ba = Convert.FromBase64String(player["game_data"].ToString());
            // Debug.Log("!!!!!!!!!!!!!!!!!! byte: " + ba.Length);

            short[] shortArray = new short[ba.Length / 2];
            Buffer.BlockCopy(ba, 0, shortArray, 0, ba.Length);
            List<short> shortList = new List<short>(shortArray);
            //debug.log("----------- shortlist.length: " + shortlist.count);

            short hashLength = shortList[0];
            shortList.RemoveAt(0);
            // Debug.Log("---------- hashLenght: " + hashLength);
            List<short> hashList = shortList.GetRange(0, hashLength);
            shortList.RemoveRange(0, hashLength);

            short xposLength = shortList[0];
            shortList.RemoveAt(0);
            // Debug.Log("---------- xposLength: " + xposLength);
            List<short> xposList = shortList.GetRange(0, xposLength);
            shortList.RemoveRange(0, xposLength);

            short yposLength = shortList[0];
            shortList.RemoveAt(0);
            // Debug.Log("---------- yposLength: " + yposLength);
            List<short> yposList = shortList.GetRange(0, yposLength);
            shortList.RemoveRange(0, yposLength);

            float record = int.Parse(player["record"].ToString()) / 1000f;
            if (shortList.Count > 0)
            {
                List<short> recordList = shortList.GetRange(0, 2); // record는 2개 고정
                shortList.RemoveRange(0, 2);
                //Debug.Log("--------- recordList[0]: " + recordList[0] + " / recordList[1]: " + recordList[1]);
                string recordString = Format.ConvertDigit(recordList[0].ToString(), 2, "0", "front") + "." + Format.ConvertDigit(recordList[1].ToString(), 3, "0", "front");
                record = float.Parse(recordString);
            }

            if(shortList.Count > 0)
            {
                List<short> stageIndexList = shortList.GetRange(0, 1); // stageIndex는 1개 고정
                shortList.RemoveRange(0, 1);
                //LogConsole.instance.Log("> p" + i + " stg i: " + stageIndexList[0].ToString());
            }
            else
            {
                //LogConsole.instance.Log("> p" + i + " stg i: x");
            }
            
            //Debug.Log("----------------- record: " + record.ToString());
            ParticipantData participantData = new ParticipantData(record);
            participantData.level = int.Parse(player["level"].ToString());
            participantData.nickname = player["nickname"].ToString();
            //participantData.record = record;
            participantData.characterId = GetCharacterId(participantData.level);
            participantData.stateHashList = DecompressHash(hashList);
            participantData.countryCode = player["country"].ToString();
            //CountryCode.GetRandomCode();
            participantData.posXList = xposList.ToArray();
            participantData.posYList = yposList.ToArray();
            _participantDataList.Add(participantData);

            log += "\n nickName: " + participantData.nickname;
            log += "\n record: " + record.ToString();

            //GameObject instance = GameObject.Instantiate(_replayerPrefab);
            //instance.transform.parent = transform.parent;
            //instance.transform.localPosition = Vector3.zero;
            //_replayerList.Add(instance.GetComponent<Replayer>());

            GameObject instance = GameObject.Instantiate(_replayerPrefab);
            instance.transform.parent = transform.parent;
            instance.transform.localPosition = Vector3.zero;
            _replayerList.Add(instance.GetComponent<Replayer>());
            
            Replayer replayer = instance.GetComponent<Replayer>();
            replayer.SetID(_matchedPlayerCount + 1); // 0은 user 1부터 replayer
            replayer.SetParticipantData(participantData);
            _matchedPlayerCount += 1;

            Character character = replayer.GetCharacter();
            Material material = new Material(_characterShader);
            material.SetTexture("_Texture", character.tex);
            material.SetColor("_Color", Color.white);
            character.tintMaterial = material;
            character.GetComponent<SkinnedMeshRenderer>().material = material;

            Vector3 startPosition = _terrainManager.GetTerrain().GetCheckPointManager().startPoint.transform.localPosition;
            startPosition.z = 1.5f;
            replayer.PrepareForRace(startPosition);

            Debug.Log(log);
        }
        //LogConsole.instance.Log("\n");

        _isRecruied = true;
        if (Recruited != null) Recruited(_player, _replayerList);
    }

    private List<int> fastReplayDataId = new List<int> { 0, 1, 2, 3, 4, 5 };
    private List<int> normalReplayDataId = new List<int> { 0, 1, 2, 3, 4 };
    private void AddComPlayers(int comPlayerCount, bool isFast = false)
    {
        fastReplayDataId.Sort((a, b) => 1 - 2 * UnityEngine.Random.Range(0, 1));
        normalReplayDataId.Sort((a, b) => 1 - 2 * UnityEngine.Random.Range(0, 1));

        for (int i = 0; i < comPlayerCount; i++)
        {
            string log = "";
            TextAsset data;
            if (isFast)
            {
                // FastReplayData는 총 6개
                data = (TextAsset)Resources.Load("ReplayData/FastReplayData_" + RaceModeInfo.stageId + "_" + fastReplayDataId[i]) as TextAsset;
                log += "< Fast Local Player / stageId: " + RaceModeInfo.stageId + " / replayDataId: " + fastReplayDataId[i] + " >";
            }
            else
            {
                // ReplayData는 총 5개
                data = (TextAsset)Resources.Load("ReplayData/ReplayData_" + RaceModeInfo.stageId + "_" + normalReplayDataId[i]) as TextAsset;
                log += "< Normal Local Player / stageId: " + RaceModeInfo.stageId + " / replayDataId: " + normalReplayDataId[i] + " >";
            }
            byte[] ba = Convert.FromBase64String(data.text);
            // Debug.Log("!!!!!!!!!!!!!!!!!! byte: " + ba.Length);

            short[] shortArray = new short[ba.Length / 2];
            Buffer.BlockCopy(ba, 0, shortArray, 0, ba.Length);
            List<short> shortList = new List<short>(shortArray);
            //debug.log("----------- shortlist.length: " + shortlist.count);

            short hashLength = shortList[0];
            shortList.RemoveAt(0);
            // Debug.Log("---------- hashLenght: " + hashLength);
            List<short> hashList = shortList.GetRange(0, hashLength);
            shortList.RemoveRange(0, hashLength);

            short xposLength = shortList[0];
            shortList.RemoveAt(0);
            // Debug.Log("---------- xposLength: " + xposLength);
            List<short> xposList = shortList.GetRange(0, xposLength);
            shortList.RemoveRange(0, xposLength);

            short yposLength = shortList[0];
            shortList.RemoveAt(0);
            // Debug.Log("---------- yposLength: " + yposLength);
            List<short> yposList = shortList.GetRange(0, yposLength);
            shortList.RemoveRange(0, yposLength);

            float record = 10.000f;
            if (shortList.Count > 0)
            {
                List<short> recordList = shortList.GetRange(0, 2); // record는 2개 고정
                shortList.RemoveRange(0, 2);
                //Debug.Log("--------- recordList[0]: " + recordList[0] + " / recordList[1]: " + recordList[1]);
                string recordString = Format.ConvertDigit(recordList[0].ToString(), 2, "0", "front") + "." + Format.ConvertDigit(recordList[1].ToString(), 3, "0", "front");
                record = float.Parse(recordString);
            }

            //Debug.Log("----------------- record: " + record.ToString());
            ParticipantData participantData = new ParticipantData(record);
            participantData.nickname = GetNickname();
            //participantData.record = record;
            participantData.level = (int)UnityEngine.Random.Range(5, 51);
            participantData.characterId = GetCharacterId(participantData.level);
            participantData.stateHashList = DecompressHash(hashList);
            participantData.countryCode = CountryCode.GetRandomCode();
            participantData.posXList = xposList.ToArray();
            participantData.posYList = yposList.ToArray();
            _participantDataList.Add(participantData);

            log += "\n nickName: " + GetNickname();
            log += "\n record: " + record.ToString();

            GameObject instance = GameObject.Instantiate(_replayerPrefab);
            instance.transform.parent = transform.parent;
            instance.transform.localPosition = Vector3.zero;
            _replayerList.Add(instance.GetComponent<Replayer>());

            Replayer replayer = instance.GetComponent<Replayer>();
            replayer.SetID(_matchedPlayerCount + 1); // 0은 user 1부터 replayer
            replayer.SetParticipantData(participantData);
            _matchedPlayerCount += 1;

            Character character = replayer.GetCharacter();
            Material material = new Material(_characterShader);
            material.SetTexture("_Texture", character.tex);
            material.SetColor("_Color", Color.white);
            character.tintMaterial = material;
            character.GetComponent<SkinnedMeshRenderer>().material = material;

            Vector3 startPosition = _terrainManager.GetTerrain().GetCheckPointManager().startPoint.transform.localPosition;
            startPosition.z = 1.5f;
            replayer.PrepareForRace(startPosition);

            Debug.Log(log);
        }
    }

    //private List<int> _originalCharacterIdList = new List<int>{ 0,1,2,4,6,7,8,18,29,30,23,24,25,3,31,32,44,49,53,54 };
    private List<int> _clonedCharacterIdList;
    private int GetCharacterId(int level)
    {
//        Debug.Log("##################### lv: " + _characterManagementService.GetItemById(1).allowLv);
        _clonedCharacterIdList = _characterManagementService.GetItemIdByLv(level);

        //Debug.Log("User Character Id: " + _userService.GetCharacterId());
        for(int i = 0; i < _clonedCharacterIdList.Count; i++)
        {
            if (_userService.GetCharacterId() == _clonedCharacterIdList[i])
            {
                Debug.Log("Remove same charater id as user in replayer character list. Removed id: " + _clonedCharacterIdList[i]);
                _clonedCharacterIdList.RemoveAt(i);
            }
        }

        int randomId = UnityEngine.Random.Range(0, _clonedCharacterIdList.Count - 1);
        int result = _clonedCharacterIdList[randomId];
        _clonedCharacterIdList.RemoveAt(randomId);
        return result;
    }

    private List<string> _tutorialNicknames = new List<string> { "Com 1", "Com 2", "Com 3", "Com 4", "Com 5" };
    //private List<string> _originalNicknameList = new List<string>{"이진주", "밤밤", "김준희", "임종현", "심진식", "손위석", "나누컴퍼니대표", "모드게임즈대표", "김지훈", "구자민", "Leonard Shin"};
    private List<string> _clonedNicknameList;
    private string GetNickname()
    {
        if(_clonedNicknameList == null) _clonedNicknameList = _dummyPlayerData.GetNickname().GetRange(0, _dummyPlayerData.GetNickname().Count);

        int randomIndex = UnityEngine.Random.Range(0, _clonedNicknameList.Count - 1);
        //Debug.Log("!!!!! Nickname / count: " + _dummyPlayerData.GetNickname().Count + " / randomIndex: " + randomIndex);
        string result = _clonedNicknameList[randomIndex];
        _clonedNicknameList.RemoveAt(randomIndex);
        return result;
    }

    private short[] DecompressHash(List<short> hashList)
    {
        List<short> result = new List<short>();
        short currentHash = 0;
        for (int i = 0; i < hashList.Count; i++)
        {
            if (hashList[i] >= 0)
            {
                currentHash = hashList[i];
                result.Add(hashList[i]);
            }
            else
            {
                int length = Math.Abs(hashList[i]);
                for (int j = 0; j < length; j++)
                {
                    result.Add(currentHash);
                }
            }
        }

        //string log = "Decompressed Hash List";
        //for (int i = 0; i < result.Count; i++)
        //{
        //    log += result[i] + ",";
        //}
        //Debug.Log(log);

        return result.ToArray();
    }
    #endregion



    #region Public Functions
    public void Recruit()
    {
        if (HasParticipant()) ClearParticipants();

        string stageId = RaceModeInfo.stageId;
        _stageData = StageDataManager.instance.GetStageDataByID(stageId);

        if (_gameManager.isRecordMode || (RaceModeInfo.raceMode == RaceMode.mode.SINGLE))
        {
            if (RaceModeInfo.isTutorialMode)
            {
                Debug.Log(">>>>>>>>>>>>> RaceParticipationManager - Tutorial Data");
                RecruitTutorialPlayers();
            }
            else
            {
                Debug.Log(">>>>>>>>>>>>> RaceParticipationManager - (Single) Local Data");
                RecruitSinglePlayers();
            }
        }
        else if(IsBasicUser())
        {
            if(RaceModeInfo.stageId == "0" || RaceModeInfo.stageId == "1")
            {
                Debug.Log(">>>>>>>>>>>>> RaceParticipationManager - (Basic) Local Data");
                RecruitSinglePlayers();
            }
            else
            {
                Debug.Log(">>>>>>>>>>>>> RaceParticipationManager - Online Data");
                RecruitMultiPlayers();
            }
        }
        else
        {
            Debug.Log(">>>>>>>>>>>>> RaceParticipationManager - Online Data");
            RecruitMultiPlayers();
        }
    }

    private bool IsBasicUser()
    {
        return (_analyticsService.GetParticipantCount() < 5) ? true : false;
    }

    private void OnRecruited()
    {
        if (Recruited != null) Recruited(_player, _replayerList);
    }

    public List<IRaceParticipant> GetReplayerList()
    {
        return _replayerList;
    }

    public Replayer GetReplayerByID(int id)
    {
        for(int i = 0; i < _replayerList.Count; i++)
        {
            Replayer replayer = ((MonoBehaviour)_replayerList[i]).GetComponent<Replayer>();
            if(replayer.GetID() == id)
            {
                return replayer;
            }
        }
        return null;
    }

    public PlayerManager GetPlayer()
    {
        return _player;
    }
    #endregion
}