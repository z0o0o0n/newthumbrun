﻿using UnityEngine;
using System.Collections;

public class SceneName : MonoBehaviour
{
    public static string MAIN_LOADER = "MainLoaderScene";
    public static string MAIN = "MainScene";
    public static string PLAY = "PlayScene";
    public static string COLLECTION = "CollectionScene";
    public static string GACHA = "GachaScene";
    public static string MAP = "MapScene";
}
