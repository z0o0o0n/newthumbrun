﻿using UnityEngine;
using System.Collections;

public class MovementState
{
    public static string MOVEMENT_IDLE = "idle";
    public static string MOVEMENT_RUNNING = "running";
    public static string MOVEMENT_RUN_FALLING = "runFalling";
    public static string MOVEMENT_JUMP_GOING_UP = "jumpGoingUp";
    public static string MOVEMENT_JUMP_FALLING = "jumpFalling";
}
