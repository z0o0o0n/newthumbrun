﻿using UnityEngine;
using System.Collections;

public class RecordPlayerData
{
    public string userID;
    public float record;
    public string countryCode;
    public Texture profilePhoto;
    public int rank;
    public float code;

    public string ToString()
    {
        string result = "<RecordPlayerData>";
        result += "userID: " + userID + "\n";
        result += "record: " + record + "\n";
        result += "countryCode: " + countryCode + "\n";
        result += "profilePhoto: " + profilePhoto.ToString() + "\n";
        result += "rank: " + rank + "\n";
        result += "code: " + code;
        return result;
    }
}
