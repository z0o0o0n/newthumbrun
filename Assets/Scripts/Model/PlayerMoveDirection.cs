﻿public static class PlayerMoveDirection
{
    public const string LEFT = "left";
    public const string RIGHT = "right";
    public const string STOP = "stop";
}
