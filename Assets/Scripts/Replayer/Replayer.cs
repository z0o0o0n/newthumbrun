﻿using UnityEngine;
using DG.Tweening;
using Com.Mod.ThumbRun.Event;
using Com.Mod.Util;
using Com.Mod.ThumbRun.Item.View;
using Junhee.Utils;
using Com.Mod.ThumbRun.Analytics.Application;

public class Replayer : MonoBehaviour, IRaceParticipant
{
    public event PlayerEvent.PlayerEventHandler Initialized;
    public event PlayerEvent.StateEventHandler PlayerSwooned;
    public event PlayerEvent.StateEventHandler PlayerRecovered;
    public event PlayerEvent.PassPointEventHandler PassStartPoint;
    public event PlayerEvent.PassPointEventHandler PassCheckPoint;

    //[SerializeField]
    //private RankData _rankData;
    [SerializeField]
    private ItemAI _itemAI;
    [SerializeField]
    private GameObject _itemDamageEffectPrefab;

    private RaceManager _raceManager;
    private ItemProvider _itemProvider;
    private int _id = -1; // -1은 id없음
    private int _currentFrame = 0;
    private int _currentPosFrame = 0;
    private bool _isReplay = false;
    private Character _character;
    private Animator _anim;
    private ParticipantData _participantData;
    private SpeedChecker _speedChecker;
    private int _currentStateHash = 0;
    private int _prevStateHash = 0;
    private bool _isSwoon = false;
    private bool _isStartPointTouched = false;   
    private bool _isCheckPointTouched = false;
    private bool _isProtecting = false;
    private float _currentCheckPointValue = 0f;
    private int _currentRound = 0;    
    private Vector2 _startPosition;
    private bool _isFinishedRace = false;
    private float _swoonedTimeAmount = 0f;
    private StopwatchModel _stopWatchForGlobal;
    private DamageType.type _currentDamageType;
    private AnalyticsService _analyticsServcie;

    public int currentRound
    {
        get{ return _currentRound; }
    }

    void Awake ()
    {
        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
        _raceManager.RaceStart += OnRaceStart;
        _raceManager.RaceEnd += OnRaceEnd;
        _raceManager.RaceTimeout += OnRaceTimeOut;
        _raceManager.RaceClose += OnRaceClose;

        _itemProvider = GameObject.FindGameObjectWithTag("ItemProvider").GetComponent<ItemProvider>();

        _stopWatchForGlobal = GameObject.FindGameObjectWithTag("StopWatchForGlobal").GetComponent<StopwatchModel>();

        _analyticsServcie = GameObject.FindGameObjectWithTag("Analytics").GetComponent<AnalyticsService>();

        CreateItemDamageEffectPrefab();
    }

	void Start () 
    {
        _speedChecker = GetComponent<SpeedChecker>();
        Character character = _character.GetComponent<Character>();
        character.ChangeMaterialProp(Color.black, 0f);
    }

    float prevPosX = 0f;
    float prevPosY = 0f;
    
    void FixedUpdate() 
    {
        if(_isReplay)
        {
            UpdatePos();
            UpdateStateHash();
            UpdateAnimationSpeed();
            
            _currentFrame++;
        }
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name == "StartPoint")
        {
            if (!_isStartPointTouched)
            {
                _isStartPointTouched = true;
                _isCheckPointTouched = false;
                if(_currentCheckPointValue != 0f)
                {
                    _currentCheckPointValue += 0.5f;
                    _currentRound = (int) Mathf.Floor(_currentCheckPointValue);
                }

                if (_currentRound == 3 && _currentCheckPointValue == 3)
                {
                    _isFinishedRace = true;
                    //_rankData.totalRecord = _stopWatchForGlobal.GetSec() + _stopWatchForGlobal.GetMsec();
                    _participantData.ChangeRecord(_stopWatchForGlobal.GetSec() + _stopWatchForGlobal.GetMsec());
                    //_participantData.record = _stopWatchForGlobal.GetSec() + _stopWatchForGlobal.GetMsec();
                }
                if (PassStartPoint != null) PassStartPoint(_id);
            }
        }

        if (collider.gameObject.name == "CheckPoint")
        {
            if (!_isCheckPointTouched)
            {
                _isCheckPointTouched = true;
                _isStartPointTouched = false;
                _currentCheckPointValue += 0.5f;
                //Debug.Log(gameObject.GetInstanceID() + " / check : " + _currentRound);

                if (PassCheckPoint != null) PassCheckPoint(_id);
            }
        }
    }

    void OnDestroy()
    {
        _raceManager.RaceStart -= OnRaceStart;
        _raceManager.RaceEnd -= OnRaceEnd;
        _raceManager.RaceTimeout -= OnRaceTimeOut;
        _raceManager.RaceClose -= OnRaceClose;
    }




    private void CreateItemDamageEffectPrefab()
    {
        GameObject instance = GameObject.Instantiate(_itemDamageEffectPrefab);
        instance.transform.parent = transform;
        instance.transform.localScale = Vector3.one;
        instance.transform.localPosition = Vector3.zero;

        ItemDamageEffect itemDamageEffect = instance.GetComponent<ItemDamageEffect>();
        itemDamageEffect.Init(gameObject);
    }

    private void MoveToStartPoint(Vector3 startPosition)
    {
        transform.localPosition = startPosition;
    }

    private void UpdatePos()
    {
        if (_participantData != null)
        {
            if (_currentPosFrame >= _participantData.posXList.Length)
            {
                OnReplayEnd();
                return;
            }

            if (_currentFrame % 2 == 0)
            {
                Vector3 pos = transform.localPosition;
                pos.x = (float)_participantData.posXList[_currentPosFrame] / 100;
                pos.y = (float)_participantData.posYList[_currentPosFrame] / 100;
                pos.z = 1.4f;
                transform.localPosition = pos;
                _currentPosFrame++;
            }
            else
            {
                Vector3 pos = transform.localPosition;
                float xpos = (float)_participantData.posXList[_currentPosFrame - 1] / 100;
                float ypos = (float)_participantData.posYList[_currentPosFrame - 1] / 100;
                float nextXPos = (float)_participantData.posXList[_currentPosFrame] / 100;
                float nextYPos = (float)_participantData.posYList[_currentPosFrame] / 100;
                pos.x = xpos + ((nextXPos - xpos) / 2);
                pos.y = ypos + ((nextYPos - ypos) / 2);
                pos.z = 1.4f;
                transform.localPosition = pos;
            }
        }

        _speedChecker.UpdatePos(transform.position);
    }

    private void UpdateStateHash()
    {
        if (IsValidStateHash())
        {
            _currentStateHash = PlayerStateHash.GetIDToHash((int)_participantData.stateHashList[_currentFrame]);

            if (_currentStateHash != _prevStateHash)
            {
                _anim.Play(_currentStateHash);
                _prevStateHash = _currentStateHash;
            }
        }
    }

    private bool IsValidStateHash()
    {
        if (_currentFrame < _participantData.stateHashList.Length) return true;
        else return false;
    }

    private void UpdateAnimationSpeed()
    {
        if (_currentStateHash == Animator.StringToHash("Main.RightJump") || _currentStateHash == Animator.StringToHash("Main.LeftJump"))
        {
            _anim.speed = 1.2f;
        }
        else 
        {
            float speed = (_speedChecker.GetSpeed() * 15) + 0.8f;
            if (speed > 1.8f) speed = 1.8f;
            _anim.speed = speed;
        }
    }

    private void OnRaceStart()
    {
        Replay();
    }

    private void OnRaceEnd()
    {
        //DOTween.Kill("ReplayerEndDelay." + GetInstanceID());
        //DOVirtual.DelayedCall(1.5f, End).SetId("ReplayerEndDelay." + GetInstanceID());
        End();
    }

    private void OnRaceTimeOut()
    {
        //DOTween.Kill("ReplayerEndDelay." + GetInstanceID());
        //DOVirtual.DelayedCall(1.5f, End).SetId("ReplayerEndDelay." + GetInstanceID());
        End();
    }

    private void OnRaceClose()
    {
        
    }

    private void OnReplayEnd()
    {
        Stop();
    }

    private void CreateCharacter(int characterId)
    {
        if (_character)
        {
            Destroy(_character);
        }

        GameObject characterPrefab = CharacterPrefabProvider.instance.Get(characterId);
        GameObject instance = GameObject.Instantiate(characterPrefab);
        instance.transform.parent = this.transform;
        instance.transform.localPosition = new Vector3(0, 0, 0);
        instance.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        instance.transform.localScale = Vector3.one;

        _character = instance.GetComponent<Character>();
        _character.ChangeRivalCharacter();

        _anim = _character.gameObject.GetComponent<Animator>();
        _anim.Play(Animator.StringToHash("Main.Idle"));
    }

    private void Replay()
    {
        _currentFrame = 0;
        _currentPosFrame = 0;
        _isReplay = true;
    }

    private void OnSwoonEnd()
    {
        Recover();
    }

    private void Recover()
    {
        _isSwoon = false;
        _anim.Play(_currentStateHash);
        _anim.speed = 1f;
        Resume();

        Character character = _character.GetComponent<Character>();
        character.ChangeMaterialProp(Color.black, 0f);

        if (PlayerRecovered != null) PlayerRecovered(_id, _currentDamageType);
    }



    public void PrepareForRace(Vector3 startPosition)
    {
        MoveToStartPoint(startPosition);
        _currentCheckPointValue = 0f;
        _currentRound = 0;
        _isFinishedRace = false;
        _swoonedTimeAmount = 0f;
    }

    public void SetID(int id)
    {
        _id = id;
    }

    public int GetID()
    {
        return _id;
    }

    public float GetCurrentCheckPointValue()
    {
        return _currentCheckPointValue;
    }

    private void Stop()
    {
        _anim.speed = 0f;
        _isReplay = false;
    }

    private void Resume()
    {
        _anim.speed = 1f;
        _isReplay = true;
    }

    // 리플레이어 경기 종료
    private void End()
    {
        if (!_isFinishedRace)
        {
            Stop();
            //DOTween.Kill("Replayer.Swoon." + GetInstanceID());
            //DOTween.Kill("Replayer.Protect." + GetInstanceID());

            float record = _participantData.record + _swoonedTimeAmount;
            _participantData.ChangeRecord(record);
        }
    }

    public void Swoon(DamageType.type damageType, float duration, IRaceParticipant attacker)
    {
        if(_isProtecting) return;
        //if(_itemAI.Protect()) return;

        if (!_isSwoon)
        {
            _currentDamageType = damageType;
            _isSwoon = true;
            Stop();
            _swoonedTimeAmount += duration;

            if(attacker != null)
            {
                if (attacker.GetParticipantData().isUser)
                {
                    if (_currentDamageType == DamageType.type.BOMB)
                    {
                        _analyticsServcie.CountKilledPlayerWithBomb();
                    }
                    else if (_currentDamageType == DamageType.type.MISSILE)
                    {
                        _analyticsServcie.CountKilledPlayerWithMissile();
                    }
                }
            }
            
            DOTween.Kill("Replayer.Swoon." + GetInstanceID());
            DOVirtual.DelayedCall(duration, OnSwoonEnd).SetId("Replayer.Swoon." + GetInstanceID());

            if(PlayerSwooned != null) PlayerSwooned(_id, _currentDamageType);
        }
    }


    public void Protect(float duration)
    {
        if(!_isProtecting)
        {
            _isProtecting = true;
            DOTween.Kill("Replayer.Protect." + GetInstanceID());
            DOVirtual.DelayedCall(duration, OnProtectionEnded).SetId("Replayer.Protect." + GetInstanceID());
        }
    }

    private void OnProtectionEnded()
    {
        _isProtecting = false;
    }


    public bool IsSwoon()
    {
        return _isSwoon;
    }

    public void SetParticipantData(ParticipantData data)
    {
        _participantData = data;
        CreateCharacter(_participantData.characterId);
        if (Initialized != null) Initialized();
    }

    public ParticipantData GetParticipantData()
    {
        return _participantData;
    }

    public bool isFinishedRace
    {
        get { return _isFinishedRace; }
    }

    public float swoonedTimeAmount
    {
        get { return _swoonedTimeAmount; }
    }

    public float GetDirection()
    {
        float result = 0.0f;
        if (_currentStateHash == Animator.StringToHash("Main.RightJump") || _currentStateHash == Animator.StringToHash("Main.RightRun")) result = 1;
        else if (_currentStateHash == Animator.StringToHash("Main.LeftJump") || _currentStateHash == Animator.StringToHash("Main.LeftRun")) result = -1;
        return result;
    }


    public void UpdateRank(int rank)
    {
        //_rankData.rank = rank;
        _participantData.rank = rank;

        //_rankData.totalRecord = raceRecord;
        //_participantData.record = raceRecord;

        if (_currentCheckPointValue != 0) // 시작시점이 아니고
        {
            RequestItemProvision(); // 아이템 제공을 요청함
        }

        // if(_currentCheckPointValue % 1 == 0) // 시작포인트를 통과했고
        // {
        //     if(_currentRound != 0) // 시작시점이 아니고
        //     {
        //         RequestItemProvision(); // 아이템 제공을 요청함
        //     }
        // }
    }

    private void RequestItemProvision()
    {
        IItem item = _itemProvider.GetItem(_participantData.rank);
        if(!_itemAI.SetItem(item))
        {
            Destroy(((MonoBehaviour)item).gameObject);
        }
    }


    public void Boost(float duration, float mvmtSpeedIncRate)
    {
    }

    public Character GetCharacter()
    {
        return _character;
    }

    public void MoveToPortal(Vector3 offsetPos)
    {
        SkinnedMeshRenderer skinnedMeshRenderer = _character.gameObject.GetComponent<SkinnedMeshRenderer>();
        skinnedMeshRenderer.enabled = false;

        DOVirtual.DelayedCall(0.1f, OnMoved);
    }

    private void OnMoved()
    {
        SkinnedMeshRenderer skinnedMeshRenderer = _character.gameObject.GetComponent<SkinnedMeshRenderer>();
        skinnedMeshRenderer.enabled = true;
    }
}
