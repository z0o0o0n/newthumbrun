﻿using Com.Mod.ThumbRun.Item.View;

public interface IRaceParticipant
{
	bool IsSwoon();
	void UpdateRank(int rank);
	void Swoon(DamageType.type damageType, float duration, IRaceParticipant attackerId);
	void Protect(float duration);
	void Boost(float duration, float mvmtSpeedIncRate);
    float GetDirection();
    Character GetCharacter();
    ParticipantData GetParticipantData();
}
