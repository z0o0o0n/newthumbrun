﻿namespace Com.Mod.ThumbRun.CharacterManagement.Application
{
    using UnityEngine;
    using System.Collections;
    using Domain;
    using System.Collections.Generic;
    using GameDataEditor;
    using User.Application;

    public class CharacterManagementService : MonoBehaviour
    {
        public delegate void CharacterManagementServiceEventHandler();
        public delegate void CMServiceUpdateEvent(int characterId);
        public event CharacterManagementServiceEventHandler Prepared;
        public event CMServiceUpdateEvent Updated;

        [SerializeField]
        private CharacterManagement _characterManagement;
        [SerializeField]
        private bool _isPrepared = false;
        private UserService _userService;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }

        void Awake()
        {
            _userService = GameObject.FindGameObjectWithTag("User").GetComponent<UserService>();
            _characterManagement.Prepared += OnPrepared;
            _characterManagement.Updated += OnUpdated;
        }

        void Start()
        {

        }

        void Update()
        {

        }

        void OnDestroy()
        {
            _characterManagement.Prepared -= OnPrepared;
            _characterManagement.Updated -= OnUpdated;
        }



        private void OnPrepared()
        {
            // ResetData();
            _isPrepared = true;
            if (Prepared != null) Prepared();
        }

        private void OnUpdated(int characterId)
        {
            if (Updated != null) Updated(characterId);
        }



        public int Buy(int characterId)
        {
            _userService = GameObject.FindGameObjectWithTag("User").GetComponent<UserService>();
            GDECharacterManagementItemData gmItem = GetItemById(characterId);
            Debug.Log("User Service: " + _userService + " / gmItem: " + gmItem);

            if (_userService.GetGoldAmount() < gmItem.goldPrice)
            {
                return -1; // 잔액부족
            }
            else
            {
                _userService.Spend(gmItem.goldPrice);
                _characterManagement.Buy(characterId);
                return 1; // 구매
            }
        }

        public void Choose(int characterId)
        {
            _userService = GameObject.FindGameObjectWithTag("User").GetComponent<UserService>();
            _userService.SetCharacterId(characterId);
        }

        public List<GDECharacterManagementItemData> GetItems(bool isOnlyUnlockedItem = false)
        {
            return _characterManagement.GetItems(isOnlyUnlockedItem);
        }

        public GDECharacterManagementItemData GetItemById(int id)
        {
            return _characterManagement.GetItemById(id);
        }

        public int GetCharacterIndex(int characterId)
        {
            return _characterManagement.GetCharacterIndex(characterId);
        }

        public List<int> GetItemIdByLv(int level)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < _characterManagement.GetItems().Count; i++)
            {
                if(_characterManagement.GetItems()[i].allowLv <= level)
                {
                    result.Add(_characterManagement.GetItems()[i].characterId);
                }
            }
            return result;
        }

        public void ResetData()
        {
            _characterManagement.ResetData();
        }




        // Condition
        public void ConsumeHp(int characterId, float consumedHp)
        {
            _characterManagement.ConsumeHp(characterId, consumedHp);
        }

        public void RecoverHp(int characterId)
        {
            _characterManagement.RecoverHp(characterId);
        }

        public void ChangeCondition(int characterId, int condition)
        {
            _characterManagement.ChangeCondition(characterId, condition);
        }




        // Skill
        public void SetSkillA(int characterId, int skillId)
        {
            _characterManagement.SetSkillA(characterId, skillId);
        }

        public void SetSkillB(int characterId, int skillId)
        {
            _characterManagement.SetSkillB(characterId, skillId);
        }

        public void IncSpinCountA(int characterId)
        {
            _characterManagement.IncSpinCountA(characterId);
        }

        public void DecSpinCountA(int characterId)
        {
            _characterManagement.DecSpinCountA(characterId);
        }

        public void IncSpinCountB(int characterId)
        {
            _characterManagement.IncSpinCountB(characterId);
        }

        public void DecSpinCountB(int characterId)
        {
            _characterManagement.DecSpinCountB(characterId);
        }

        public List<GDESkillData> GetSkillList()
        {
            return _characterManagement.GetSkillList();
        }

        public int GetPrevSkillId(int skillId)
        {
            List<GDESkillData> skillList = _characterManagement.GetSkillList();

            int currentSkillIndex = 0;
            for (int i = 0; i < skillList.Count; i++)
            {
                if (skillList[i].id == skillId)
                {
                    currentSkillIndex = i;
                }
            }
            int currentPrevSkillIndex = ((currentSkillIndex - 1) < 0) ? skillList.Count - 1 : currentSkillIndex - 1;
            int currentPrevSkillId = skillList[currentPrevSkillIndex].id;

            return currentPrevSkillId;
        }

        public int GetNextSkillId(int skillId)
        {
            List<GDESkillData> skillList = _characterManagement.GetSkillList();

            int currentSkillIndex = 0;
            for (int i = 0; i < skillList.Count; i++)
            {
                if (skillList[i].id == skillId)
                {
                    currentSkillIndex = i;
                }
            }
            int currentNextSkillIndex = ((currentSkillIndex + 1) > skillList.Count - 1) ? 0 : currentSkillIndex + 1;
            int currentNextSkillId = skillList[currentNextSkillIndex].id;

            return currentNextSkillId;
        }

        public int GetRandomSkillId()
        {
            List<GDESkillData> skillList = _characterManagement.GetSkillList();

            int randomIndex = Random.Range(0, skillList.Count);
            return skillList[randomIndex].id;
        }

        public void UnlockSkillA(int characterId)
        {
            _characterManagement.UnlockSkillA(characterId);
        }

        public void LockSkillA(int characterId)
        {
            _characterManagement.LockSkillA(characterId);
        }

        public void UnlockSkillB(int characterId)
        {
            _characterManagement.UnlockSkillB(characterId);
        }

        public void LockSkillB(int characterId)
        {
            _characterManagement.LockSkillB(characterId);
        }


        public int GetSlotMachineSpinPrice(int characterId)
        {
            return _characterManagement.GetSlotMachineSpinPrice(characterId);
        }

        // 새로운 정보가 존재하는지 확인. (현재는 스킬 언락 여부만)
        public bool HasNewInfo()
        {
            bool hasNew = false;
            for(int i = 0; i < _characterManagement.GetItems().Count; i++)
            {
                if(_characterManagement.GetItems()[i].hasNew)
                {
                    hasNew = true;
                    return hasNew;
                }
            }
            return hasNew;
        }


        public void TakeLookNewInfo(int characterId)
        {
            _characterManagement.TakeLookNewInfo(characterId);
        }

        public List<int> GetSkillIdRandomBoxList()
        {
            return _characterManagement.GetSkillIdRandomBoxList();
        }

        // 슬롯 잠금 관련
        public void LockSlot(int characterId, int slotId)
        {
            _characterManagement.LockSlot(characterId, slotId);
        }

        public void UnlockSlot(int characterId, int slotId)
        {
            _characterManagement.UnlockSlot(characterId, slotId);
        }

        public bool IsSlotLocked(int characterId, int slotId)
        {
            return _characterManagement.IsSlotLocked(characterId, slotId);
        }
    }
}