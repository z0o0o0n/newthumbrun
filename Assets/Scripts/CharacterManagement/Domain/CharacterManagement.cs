﻿namespace Com.Mod.ThumbRun.CharacterManagement.Domain
{
    using UnityEngine;
    using System.Collections;
    using GameDataEditor;
    using System.Collections.Generic;
    using System.IO;

    public class CharacterManagement : MonoBehaviour
    {
        public delegate void CMEventHandler();
        public delegate void CMUpdateEvent(int characterId);
        public event CMEventHandler Prepared;
        public event CMUpdateEvent Updated;

        private static CharacterManagement _instance;

        private bool _isPrepared = false;
        private GDECharacterManagementData _rawData;
        private List<float> _hpRangeRateList = new List<float>() { 0.25f, 0.25f, 0.25f, 0.25f }; // 컨디션 기준에 따라 체력 범위를 나누는 비율
        private List<int> _slotMachineSpinPrice = new List<int> { 100, 500, 2000, 10000, 50000, 200000, 500000, 1000000 };
		private float _basicResilience = 2; // 경기 당 회복력
		private float _additionalResilience = 1;
        private List<int> _skillIdRandomBoxList = new List<int>();

        void Start()
        {
            if(_instance == null)
            {
                _instance = this;
                if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.CharacterManagement_CharacterManagement, out _rawData))
                {
                    Debug.Log("GDECharacterManagementData Prepared");

                    UpdateConditions();
                    Init();

                    // 스킬 및 스킬슬롯잠금 초기화
                    //for (int i = 0; i < getitems().count; i++)
                    //{
                    //    getitems()[i].reset_isskillunlocka();
                    //    getitems()[i].reset_isskillunlockb();
                    //    getitems()[i].reset_skillida();
                    //    getitems()[i].reset_skillidb();
                    //}

                    //for (int i = 0; i < GetItems().Count; i++)
                    //{
                    //    GetItems()[i].ResetAll();
                    //}
                }
                else
                {
                    Debug.LogError("Read Error GDECharacterManagementData");
                }

                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void Update()
        {
            if(Input.GetKeyUp(KeyCode.Z))
            {
                ConsumeHp(3, 10);
            }
            else if (Input.GetKeyUp(KeyCode.X))
            {
                //RecoverHp(3, 10);
            }
        }



        private void Init()
        {
            _isPrepared = true;
            if (Prepared != null) Prepared();

            //RecoverCondition(3);
            //ChangeCondition(3, 4);
            CreateSkillIdRandomBoxList();
        }

        private void UpdateConditions()
        {
            for(int i = 0; i < GetItems().Count; i++)
            {
                GDECharacterManagementItemData cmItem = GetItems()[i];
                UpdateCondition(cmItem.characterId);
            }
        }

        private void SetNewInfo(int characterId, int slotId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);

            cmItem.hasNew = true;

            Debug.Log(">>>>>>>>>>>>>>>>> unlock mission / characterId: " + characterId + " / slotId: " + slotId);

            if (slotId == 0) cmItem.isNewSlotA = true;
            else if (slotId == 1) cmItem.isNewSlotB = true;
        }

        private void CreateSkillIdRandomBoxList()
        {
            List<GDESkillData> skillList = _rawData.skillList;

            for(int i = 0; i < skillList.Count; i++)
            {
                for(int j = 0; j < skillList[i].randomBoxCount; j++)
                {
                    _skillIdRandomBoxList.Add(skillList[i].id);
                }
            }
        }




		// isOnlyUnlockedItem이 true인 경우 unlock 된 캐릭터들만 리턴됨.
        public List<GDECharacterManagementItemData> GetItems(bool isOnlyUnlockedItem = false)
        {
            List<GDECharacterManagementItemData> result = null;
			if(isOnlyUnlockedItem)
			{
				if(_rawData != null)
				{
					result = new List<GDECharacterManagementItemData>();
					for(int i = 0; i < _rawData.items.Count; i++)
					{
						if(_rawData.items[i].isUnlocked)
						{
							result.Add(_rawData.items[i]);
						}
					}
				}
			}
			else
			{
				if (_rawData != null) result = _rawData.items;
			}
            
            return result;
        }

        public GDECharacterManagementItemData GetItemById(int id)
        {
            for (int i = 0; i < _rawData.items.Count; i++)
            {
                if (_rawData.items[i].characterId == id) return _rawData.items[i];
            }

            return null;
        }

        public int GetCharacterIndex(int characterId)
        {
            for (int i = 0; i < _rawData.items.Count; i++)
            {
                if (_rawData.items[i].characterId == characterId) return i;
            }

            return -1;
        }

        public void Buy(int characterId)
        {
            GetItemById(characterId).isUnlocked = true;
        }

        public void ResetData()
        {
            _rawData.ResetAll();
        }



        // Condition
        public void ConsumeHp(int characterId, float consumedHp)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);

            cmItem.hp -= consumedHp;
            if (cmItem.hp < 0) cmItem.hp = 0;

            UpdateCondition(characterId);
        }

        public void RecoverHp(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);

            //float basicResilience = 5f;
            //if (cmItem.basicSkillId == 13 || cmItem.skillIdA == 13 || cmItem.skillIdB == 13) // 커피
            //{
            //    _additionalResilience += 5.2f;
            //}

            cmItem.hp += _basicResilience;
            if (cmItem.hp > cmItem.defaultHp) cmItem.hp = cmItem.defaultHp;

            UpdateCondition(characterId);
        }

		// condition 0: 번개 ~ 4: 별무리
        public void ChangeCondition(int characterId, int condition)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);

            Debug.Log("##### Change Condition - condition: " + cmItem.condition + " / hp: " + cmItem.hp);

            int targetCondition = condition;
            if (targetCondition < 0) targetCondition = 0;
            else if (targetCondition > 4) targetCondition = 4;
            cmItem.condition = targetCondition;
            Debug.Log("##### Change Condition - target condition: " + targetCondition);

            int targetHpRangeIndex = cmItem.condition;
            if (3 < targetHpRangeIndex) targetHpRangeIndex = 3; // hp 범위는 3단계간 Max
            float targetHp = 0;
            for (int i = 0; i < _hpRangeRateList.Count; i++)
            {
                if (i <= targetHpRangeIndex)
                {
                    targetHp += cmItem.defaultHp * _hpRangeRateList[i];
                }
            }

            if (targetCondition == 4) targetHp += 1f;

            cmItem.hp = targetHp;
            Debug.Log("##### Change Condition - target hp: " + targetHp);

            UpdateSkillDeactivation(characterId);
        }

        private void UpdateCondition(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);

            float defaultHp = (float)cmItem.defaultHp;
            float hpRangeA = defaultHp * _hpRangeRateList[0];
            float hpRangeB = defaultHp * _hpRangeRateList[1] + hpRangeA;
            float hpRangeC = defaultHp * _hpRangeRateList[2] + hpRangeB;
            float hpRangeD = defaultHp * _hpRangeRateList[3] + hpRangeC;

            int condition = 0;
            if(0 <= cmItem.hp && cmItem.hp <= hpRangeA)
            {
                condition = 0;
            }
            else if(hpRangeA < cmItem.hp && cmItem.hp <= hpRangeB)
            {
                condition = 1;
            }
            else if (hpRangeB < cmItem.hp && cmItem.hp <= hpRangeC)
            {
                condition = 2;
            }
            else if (hpRangeC < cmItem.hp && cmItem.hp <= hpRangeD)
            {
                condition = 3;
            }
            else if(hpRangeD < cmItem.hp)
            {
                condition = 4;
            }

            cmItem.condition = condition;

            UpdateSkillDeactivation(characterId);

            //Debug.Log("###### condition: " + condition + " / hp: " + cmItem.hp);
        }




        // Skill
        public void SetSkillA(int characterId, int skillId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            cmItem.skillIdA = skillId;
            UpdateSkillDeactivation(characterId);
        }

        public void SetSkillB(int characterId, int skillId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            cmItem.skillIdB = skillId;
            UpdateSkillDeactivation(characterId);
        }

        private void UpdateSkillDeactivation(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            int deactivatedSkillCount = 3 - cmItem.condition;
            if (deactivatedSkillCount < 0) deactivatedSkillCount = 0;

            cmItem.isBasicSkillDeactivated = false;
            cmItem.isSkillDeactivatedA = false;
            cmItem.isSkillDeactivatedB = false;

            for (int i = 0; i < deactivatedSkillCount; i++)
            {
                if (cmItem.isSkillUnlockB && cmItem.skillIdB != 0 && !cmItem.isSkillDeactivatedB)
                {
                    cmItem.isSkillDeactivatedB = true;
                }
                else if (cmItem.isSkillUnlockA && cmItem.skillIdA != 0 && !cmItem.isSkillDeactivatedA)
                {
                    cmItem.isSkillDeactivatedA = true;
                }
                else if(!cmItem.isBasicSkillDeactivated)
                {
                    cmItem.isBasicSkillDeactivated = true;
                }
            }

            if (Updated != null) Updated(characterId);
        }

        public List<GDESkillData> GetSkillList()
        {
            List<GDESkillData> skillList = _rawData.skillList;
            return skillList;
        }

        public void IncSpinCountA(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            cmItem.slotSpinCountA += 1;
            if (cmItem.slotSpinCountA > 7) cmItem.slotSpinCountA = 7;
        }

        public void DecSpinCountA(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            cmItem.slotSpinCountA -= 1;
            if (cmItem.slotSpinCountA < 0) cmItem.slotSpinCountA = 0;
        }

        public void IncSpinCountB(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            cmItem.slotSpinCountB += 1;
            if (cmItem.slotSpinCountB > 7) cmItem.slotSpinCountB = 7;
        }

        public void DecSpinCountB(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            cmItem.slotSpinCountB -= 1;
            if (cmItem.slotSpinCountB < 0) cmItem.slotSpinCountB = 0;
        }

        public int GetSlotMachineSpinPrice(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            int priceA = 0;
            if (cmItem.isSkillUnlockA) priceA = _slotMachineSpinPrice[cmItem.slotSpinCountA];

            int priceB = 0;
            if (cmItem.isSkillUnlockB) priceB = _slotMachineSpinPrice[cmItem.slotSpinCountB];

            return priceA + priceB;
        }

        public List<int> GetSkillIdRandomBoxList()
        {
            return _skillIdRandomBoxList;
        }

        public void UnlockSkillA(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            if (cmItem.isSkillUnlockA) return;

            cmItem.isSkillUnlockA = true;

            SetNewInfo(characterId, 0);
        }

        public void LockSkillA(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            if (!cmItem.isSkillUnlockA) return;

            cmItem.isSkillUnlockA = false;
        }

        public void UnlockSkillB(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            if (cmItem.isSkillUnlockB) return;

            cmItem.isSkillUnlockB = true;

            SetNewInfo(characterId, 1);
        }

        public void LockSkillB(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            if (!cmItem.isSkillUnlockB) return;

            cmItem.isSkillUnlockB = false;
        }

        public void TakeLookNewInfo(int characterId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);

            cmItem.hasNew = false;
            cmItem.isNewSlotA = false;
            cmItem.isNewSlotB = false;
        }

        // 슬롯 잠금 관련. 슬롯A = 0, 슬롯B = 1
        public void LockSlot(int characterId, int slotId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            if (slotId == 0) cmItem.isSlotALocked = true;
            else cmItem.isSlotBLocked = true;
        }

        public void UnlockSlot(int characterId, int slotId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            if (slotId == 0) cmItem.isSlotALocked = false;
            else cmItem.isSlotBLocked = false;
        }

        public bool IsSlotLocked(int characterId, int slotId)
        {
            GDECharacterManagementItemData cmItem = GetItemById(characterId);
            if (slotId == 0) return cmItem.isSlotALocked;
            else return cmItem.isSlotBLocked;
        }
    }
}