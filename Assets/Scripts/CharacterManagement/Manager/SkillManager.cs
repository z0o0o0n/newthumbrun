﻿using Com.Mod.ThumbRun.CharacterManagement.Application;
using Com.Mod.ThumbRun.Race.Application;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillManager : MonoBehaviour
{
    [SerializeField]
    private PlayerManager _player;
    [SerializeField]
    private ItemManager _itemManager;
    [SerializeField]
    private RaceRewardService _raceRewardService;
    private CharacterManagementService _characterManagementService;
    private float _additionalFloorSpeed = 0f; // 평지 스피드
    private float _additionalBoostFloorSpeed = 0f; // 부스트 평지 스피드
    private float _additionalFallingSpeed = 0f; // 하강 스피드
    private float _adddtionalJumpSpeed = 0f; // 점프 이동 스피드
    private float _bombRecoveryRate = 1; // 폭탄 피해 기절시간 감소 비율
    private float _missileRecoveryRate = 1;
    private float _trapRecoveryRate = 1;
    private float _missileSpeed = 0f; // 미사일 발사 속도
    private float _missilePower = 0f; // 미사일 공격력
    private float _bombAttackArea = 0f; // 폭탄 공격 범위
    private float _bombPower = 0f; // 폭단 공격력
    //private float _trapSize = 0f;
    private float _trapPreTime = 0f;
    private float _trapPower = 0f;
    private float _shieldTime = 0f; // 쉴드 사용 시간
    private float _boostTime = 0f;
    private float _boostSTime = 0f;
    private float _rewardGoldRate = 1f;
    //private float _resilienceRate = 1;

    void Start ()
    {
        _characterManagementService = GameObject.FindGameObjectWithTag("CharacterManagement").GetComponent<CharacterManagementService>();
        _characterManagementService.Prepared += OnPrepared;

        _raceRewardService.Prepared += OnPrepared;

        if (_characterManagementService.isPrepared && _raceRewardService.isPrepared) OnPrepared();
    }
	
	void Update ()
    {
		
	}

    private void OnDestroy()
    {
        _characterManagementService.Prepared -= OnPrepared;

        _raceRewardService.Prepared -= OnPrepared;
    }




    private void OnPrepared()
    {
        if (!_characterManagementService) return;
        if (!_raceRewardService) return;

        _player.RankChanged += OnPlayerRankChanged;

        int characterId = _player.GetMainCharacterID();
        GDECharacterManagementItemData cmItem = _characterManagementService.GetItemById(characterId);

        if(!cmItem.isBasicSkillDeactivated) UpdateAbility(cmItem.basicSkillId);
        if (!cmItem.isSkillDeactivatedA) UpdateAbility(cmItem.skillIdA);
        if (!cmItem.isSkillDeactivatedB) UpdateAbility(cmItem.skillIdB);

        ApplySkillAbilitys();
    }

    private void OnPlayerRankChanged(int rank)
    {
        int round = _player.GetCurrentRound();
        float checkPointValue = _player.GetCurrentCheckPointValue();
        int characterId = _player.GetMainCharacterID();
        GDECharacterManagementItemData cmItem = _characterManagementService.GetItemById(characterId);

        if (!cmItem.isBasicSkillDeactivated) UpdateRoundBaseSkill(cmItem.basicSkillId, rank, round, checkPointValue);
        if (!cmItem.isSkillDeactivatedA) UpdateRoundBaseSkill(cmItem.skillIdA, rank, round, checkPointValue);
        if (!cmItem.isSkillDeactivatedB) UpdateRoundBaseSkill(cmItem.skillIdB, rank, round, checkPointValue);

        //Debug.Log("............... round: " + _player.GetCurrentRound() + " / rank: " + rank + " / _additionalFloorSpeed: " + _additionalFloorSpeed);
    }

    private bool _applySkill3 = false;
    private bool _applySkill_13 = false;
    private void UpdateAbility(int skillId)
    {
        if(skillId == 1) // 러닝화 : 평지속도 +1
        {
            _additionalFloorSpeed += 0.2f;
        }
        else if (skillId == 2) // 러닝양말 : 평지속도 +0.5
        {
            _additionalFloorSpeed += 0.12f;
        }
        else if(skillId == 4) // 무게추 : 하강속도 +1
        {
            _additionalFallingSpeed += 0.08f;
        }
        else if(skillId == 5) // 부스터 업그레이드 킷 : 부스터 사용시 평지속도 +0.5
        {
            _additionalBoostFloorSpeed += 0.1f;
        }
        else if (skillId == 6) // 미사일 업그레이드 킷 : 발사속도 증가 +1, 적 기절시간 증가 +0.5
        {
            _missileSpeed += 0.8f;
            _missilePower += 0.2f;
        }
        else if (skillId == 7) // 얼음폭탄 업그레이드 킷 : 피해반경 증가 +0.5, 적 기절시간 증가 +0.5
        {
            _bombAttackArea += 0.5f;
            _bombPower += 0.2f;
        }
        else if (skillId == 8) // 젤리폭탄 : 공격력 0.5f, 사용즉시 활성화
        {
            _trapPower += 0.3f;
            _trapPreTime += 0.5f;
        }
        else if(skillId == 9) // 아이젠 : 벽점프 이동속도 +0.5
        {
            _adddtionalJumpSpeed = 0.5f;
        }
        else if (skillId == 10) // 아이스크림 : 미사일 피해 회복속도 +15%, 얼음폭탄 피해반경 +0.3
        {
            _missileRecoveryRate -= 0.15f;
            _bombAttackArea += 0.3f;
        }
        else if (skillId == 11) // 성냥 : 얼음폭탄 피해 회복속도 +7%, 
        {
            _bombRecoveryRate -= 0.15f;
        }
        else if (skillId == 12) // 얼음망치 : 얼음폭탄 피해 회복속도 +15%
        {
            Debug.Log("----- 스킬적용 / id: 12 / 얼음망치 / 얼음폭탄 피해 회복속도 증가");
            _bombRecoveryRate -= 0.30f;
        }
        else if (skillId == 13) // 커피 : 젤리트랩 피해 회복속도 +10%, 마지막 라운드 평지속도 +0.5
        {
            Debug.Log("----- 스킬적용 / id: 13 / 커피 / 젤리트랩 피해 회복속도 향상");
            _trapRecoveryRate -= 0.1f;
            //_resilienceRate += 0.2f;
        }
        else if (skillId == 14)
        {
            _boostTime += 0.5f;
            _boostSTime += 0.5f;
        }
        else if (skillId == 15)
        {
            _boostTime += 1f;
            _boostSTime += 1f;
        }
        else if (skillId == 16) // 쉴드 배터리 : 쉴드 지속시간 +0.5
        {
            _shieldTime += 0.5f;
        }
        else if (skillId == 17) // 쉴드 배터리 : 쉴드 지속시간 +1
        {
            _shieldTime += 1f;
        }
        else if (skillId == 18) // 별 : 골드 5% 추가획득
        {
            _rewardGoldRate += 0.05f;
        }
        else if (skillId == 19) // 황금열쇠 : 골드 10% 추가획득
        {
            _rewardGoldRate += 0.1f;
        }
        else if (skillId == 20) // 에너지바 : 미사일, 얼음폭탄, 젤리트랩 피해시 회복속도 +10%
        {
            _missileRecoveryRate -= 0.1f;
            _bombRecoveryRate -= 0.1f;
            _trapRecoveryRate -= 0.1f;
        }
    }

    private void UpdateRoundBaseSkill(int skillId, int rank = -1, int round = -1, float checkPointValue = -1f)
    {
        if (skillId == 3) // 음료수 : 4~6위로 시작포인트 또는 체크포인트 통과 시 평지속도향상 +1
        {
            if (checkPointValue > 1)
            {
                if (3 <= rank && rank <= 5)
                {
                    if (!_applySkill3)
                    {
                        _applySkill3 = true;
                        _additionalFloorSpeed += 0.2f;
                        _player.SetAdditionalFloorSpeed(_additionalFloorSpeed);
                    }
                }
                else
                {
                    if (_applySkill3)
                    {
                        _applySkill3 = false;
                        _additionalFloorSpeed -= 0.2f;
                        _player.SetAdditionalFloorSpeed(_additionalFloorSpeed);
                    }
                }
            }
        }
        else if(skillId == 13) // 커피
        {
            if(round >= 2)
            {
                if(!_applySkill_13)
                {
                    _applySkill_13 = true;
                    _additionalFloorSpeed += 0.2f;
                    _player.SetAdditionalFloorSpeed(_additionalFloorSpeed);
                    Debug.Log("----- 스킬적용 / id: 13 / 커피 / 마지막 라운드 속도 증가");
                }
            }
            else
            {
                if(_applySkill_13)
                {
                    _applySkill_13 = false;
                    _additionalFloorSpeed -= 0.2f;
                    _player.SetAdditionalFloorSpeed(_additionalFloorSpeed);
                    Debug.Log("----- 스킬해지 / id: 13 / 커피 / 마지막 라운드 속도 증가");
                }
            }
        }
    }

    private void ApplySkillAbilitys()
    {
        _player.SetAdditionalFloorSpeed(_additionalFloorSpeed);
        _player.SetAdditionalBoostFloorSpeed(_additionalBoostFloorSpeed);
        _player.SetAdditionalFallingSpeed(_additionalFallingSpeed);
        _player.SetAdditionalJumpSpeed(_adddtionalJumpSpeed);
        _player.SetBombRecoveryRate(_bombRecoveryRate);
        _player.SetMissileRecoveryRate(_missileRecoveryRate);
        _player.SetTrapRecoveryRate(_trapRecoveryRate);
        _itemManager.SetBombAdditionalAttackArea(_bombAttackArea);
        _itemManager.SetBombAdditionalPower(_bombPower);
        _itemManager.SetMissileAdditionalSpeed(_missileSpeed);
        _itemManager.SetMissileAdditionalPower(_missilePower);
        _itemManager.SetShieldAdditionalActivationTime(_shieldTime);
        _itemManager.SetTrapAdditionalPreTime(_trapPreTime);
        _itemManager.SetTrapAdditionalPower(_trapPower);
        _itemManager.SetBoostAdditionalTime(_boostTime);
        _itemManager.SetBoostSAdditionalTime(_boostSTime);
        _raceRewardService.SetAdditionalRewardGoldRate(_rewardGoldRate);
        //_characterManagementService.SetAdditionalResilience(_resilienceRate);
        //_itemManager.SetTrapAdditionalSize(_trapSize);
    }
}
