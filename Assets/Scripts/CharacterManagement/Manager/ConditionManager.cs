﻿using Com.Mod.ThumbRun.CharacterManagement.Application;
using Com.Mod.ThumbRun.Race.Application;
using Com.Mod.ThumbRun.User.Application;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionManager : MonoBehaviour
{
    private static ConditionManager _instance;

    [SerializeField]
    private UserService _userService;
    [SerializeField]
    private ConditionDisplay _conditionDisplay;
    [SerializeField]
    private EntryService _entryService;
    [SerializeField]
    private RaceRewardService _raceRewardService;
	//private List<float> _consumeByMapList = new List<float>() { 18f, 5f, 6f, 7f, 8f, 10f, 12f, 14f };
	private List<float> _consumeByMapList = new List<float>() { 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f };
	private RaceManager _raceManager;
    private CharacterManagementService _characterManagementService;

    private void Awake()
    {
        //if(_instance == null)
        //{
        //    _instance = this;
        //    DontDestroyOnLoad(gameObject);
        //}
        //else
        //{
        //    Destroy(gameObject);
        //}
    }

    void Start ()
    {
        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
        _characterManagementService = GameObject.FindGameObjectWithTag("CharacterManagement").GetComponent<CharacterManagementService>();

        _raceManager.RaceStart += OnRaceStart;
        _raceManager.RaceClose += OnRaceClose;

        _userService.Prepared += OnPrepared;
        
        _characterManagementService.Prepared += OnPrepared;

        _raceRewardService.Prepared += OnPrepared;

        if (_userService.isPrepared && _characterManagementService.isPrepared && _raceRewardService.isPrepared) OnPrepared();
    }
	
	void Update ()
    {
		
	}

    private void OnDestroy()
    {
        _raceManager.RaceStart -= OnRaceStart;
        _raceManager.RaceClose -= OnRaceClose;
        _userService.Prepared -= OnPrepared;
        _characterManagementService.Prepared -= OnPrepared;
    }




    private void OnPrepared()
    {
        if (!_userService.isPrepared) return;
        if (!_characterManagementService.isPrepared) return;
        if (!_raceRewardService.isPrepared) return;

        _conditionDisplay.ChangeCondition(_characterManagementService.GetItemById(_userService.GetCharacterId()).condition);
    }




    private void OnRaceStart()
    {
        int characterId = _userService.GetCharacterId();
        int condition = _characterManagementService.GetItemById(characterId).condition;

        if (condition == 4) _raceRewardService.SetConditionBonusGoldRate(1 + 0.3f);
    }

    private void OnRaceClose()
    {
        // 사용중인 캐릭터 체력 소모
        int characterId = _userService.GetCharacterId();

        float consumedHp = _consumeByMapList[_entryService.GetCurrentEntryIndex()];
        _characterManagementService.ConsumeHp(characterId, consumedHp);

        _conditionDisplay.ChangeCondition(_characterManagementService.GetItemById(characterId).condition);

        // 보유한 캐릭터 중 사용하지 않는 캐릭터 회복
        List<GDECharacterManagementItemData> cmItemlist = _characterManagementService.GetItems();
        for(int i = 0; i < cmItemlist.Count; i++)
        {
            if(cmItemlist[i].isUnlocked)
            {
                if(cmItemlist[i].characterId != _userService.GetCharacterId())
                {
                    //if (Random.Range(0, 10) == 1) // 컨디션 변화 발생 확률
                    //{
                    //    int randomCondition = Random.Range(0, 5);
                    //    _characterManagementService.ChangeCondition(cmItemlist[i].characterId, randomCondition);
                    //}
                    //else
                    //{
                        // 회복
                        _characterManagementService.RecoverHp(cmItemlist[i].characterId);
                    //}
                }
            }
        }
    }
}
