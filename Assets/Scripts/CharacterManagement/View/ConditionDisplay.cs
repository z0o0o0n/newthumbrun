﻿using Com.Mod.ThumbRun.CharacterManagement.Application;
using Com.Mod.ThumbRun.User.Application;
using DG.Tweening;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionDisplay : MonoBehaviour
{
    [SerializeField]
    private UISprite _bubbleBg;
    [SerializeField]
    private UISprite _circleBg;
    [SerializeField]
    private ConditionIcon _conditionIcon;
    [SerializeField]
    private List<Color> _bgColors;
    [SerializeField]
    private UserService _userService;
    [SerializeField]
    private ConditionInfoBubble.DisplayDirection _bubbleDirection = ConditionInfoBubble.DisplayDirection.T;

    private int _conditionId = -1;
    private bool _isInfoShowed = false;
    private CharacterManagementService _characterManagementService;

    private void Awake()
    {
        _conditionIcon.Press += OnConditionIconPress;
    }

    void Start ()
    {
        if(_userService != null)
        {
            _userService.Prepared += OnPrepared;

            _characterManagementService = GameObject.FindGameObjectWithTag("CharacterManagement").GetComponent<CharacterManagementService>();
            _characterManagementService.Prepared += OnPrepared;
        }
    }
	
	void Update ()
    {
		
	}

    private void OnDisable()
    {
        HideInfoBubble();
    }

    private void OnDestroy()
    {
        if (_userService != null)
        {
            _userService.Prepared -= OnPrepared;
        }

        _conditionIcon.Press -= OnConditionIconPress;
    }



    private void OnPrepared()
    {
        if (!_userService.isPrepared) return;
        if (!_characterManagementService.isPrepared) return;

        GDECharacterManagementItemData cmItem = _characterManagementService.GetItemById(_userService.GetCharacterId());
        ChangeCondition(cmItem.condition);
    }

    private void OnConditionIconPress(bool isPress)
    {
        if (isPress) ShowInfoBubble();
        else HideInfoBubble();
    }





    private void ShowInfoBubble()
    {
        if (!ConditionInfoBubble.instance.isShowed)
        {
            _isInfoShowed = true;
            ConditionInfoBubble.instance.Show(_conditionId, transform.position, _bubbleDirection);
        }
    }

    private void HideInfoBubble()
    {
        if (_isInfoShowed) ConditionInfoBubble.instance.Hide();
    }





    public void ChangeCondition(int conditionId)
    {
        _conditionId = conditionId;
        _conditionIcon.SetCondition(_conditionId);
        _bubbleBg.color = _bgColors[_conditionId];
        _circleBg.color = _bgColors[_conditionId];

        //transform.DOShakeRotation(2f, new Vector3(0f, 0f, 30f));
    }
}
