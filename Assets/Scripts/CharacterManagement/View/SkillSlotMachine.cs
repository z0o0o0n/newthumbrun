﻿using Com.Mod.ThumbRun.CharacterManagement.Application;
using Com.Mod.ThumbRun.CharacterManagement.View;
using Com.Mod.ThumbRun.UI.Popup;
using Com.Mod.ThumbRun.User.Application;
using DG.Tweening;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillSlotMachine : BasicPopup
{
    [SerializeField]
    private SkillIcon _basicSkillIcon;
    [SerializeField]
    private SkillSlotMachineSlot _slotA;
    [SerializeField]
    private SkillSlotMachineSlot _slotB;
    [SerializeField]
    private UIDynamicButton _spinButton;
    [SerializeField]
    private UIGold _spinButtonUIGold;
    [SerializeField]
    private UIDynamicButton _rollbackButon;
    [SerializeField]
    private UIDynamicButton _okButton;
    [SerializeField]
    private UIButton _closeButton;
    [SerializeField]
    private UIDynamicButton _slotLockButtonA;
    [SerializeField]
    private UISprite _slotLockButtonAreaA;
    [SerializeField]
    private UIDynamicButton _slotLockButtonB;
    [SerializeField]
    private UISprite _slotLockButtonAreaB;
    [SerializeField]
    private SkillSlotMachineProgress _progress;
    [SerializeField]
    private UserService _userService;
    [SerializeField]
    private CharacterManagementPopup _characterManagementPopup;
    [SerializeField]
    private SlotLockIapPopup _slotLockIapPopup;

    private int _characterId = -1;
    private int _rollbackSkillIdA = 0;
    private int _rollbackSkillIdB = 0;
    private int _selectedSkillIdA = 0;
    private int _selectedSkillIdB = 0;
    private CharacterManagementService _characterManagementService;

    private void Awake()
    {
        _characterManagementService = GameObject.FindGameObjectWithTag("CharacterManagement").GetComponent<CharacterManagementService>();
        _characterManagementService.Prepared += OnPrepared;

        _userService = GameObject.FindGameObjectWithTag("User").GetComponent<UserService>();
        _userService.Prepared += OnPrepared;

        if (_characterManagementService.isPrepared && _userService.isPrepared) OnPrepared();

        _spinButton.Click += OnSpinButtonClick;
        _rollbackButon.Click += OnRollBackButtonClick;
        _okButton.Click += OnOkButtonClick;
        _slotLockButtonA.Click += OnSlotLockButtonAClick;
        _slotLockButtonB.Click += OnSlotLockButtonBClick;

        NewADManager.instance.Success += OnAdSuccess;
        NewADManager.instance.Failed += OnFailed;

        transform.localPosition = Vector2.zero;
        ClosePopup();
    }

    void Start ()
    {
        //List<int> filters = new List<int> { 8, 13 };
        //GetRandomSkillId(filters);
    }
	
	void Update () {
		
	}

    private void OnDestroy()
    {
        _spinButton.Click -= OnSpinButtonClick;
        _rollbackButon.Click -= OnRollBackButtonClick;
        _okButton.Click -= OnOkButtonClick;
        _slotLockButtonA.Click -= OnSlotLockButtonAClick;
        _slotLockButtonB.Click -= OnSlotLockButtonBClick;

        NewADManager.instance.Success -= OnAdSuccess;
        NewADManager.instance.Failed -= OnFailed;
    }





    private void OnPrepared()
    {
        if (!_characterManagementService.isPrepared) return;
    }

    //private List<int> _skillIdRandomBox = new List<int> { 1, 2, 3, 4};
    private List<int> _skillIdRandomBox = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
    private int GetRandomSkillId(List<int> filterSkillIds)
    {
        List<int> skillIdListClone = new List<int>();
        //List<int> skillIdRandomBoxList = _skillIdRandomBox;
        List<int> skillIdRandomBoxList = _characterManagementService.GetSkillIdRandomBoxList();

        for (int i = 0; i < skillIdRandomBoxList.Count; i++)
        {
            bool filtered = false;
            for(int j = 0; j < filterSkillIds.Count; j++)
            {
                if(skillIdRandomBoxList[i] == filterSkillIds[j])
                {
                    filtered = true;
                }
            }

            if (!filtered)
            {
                skillIdListClone.Add(skillIdRandomBoxList[i]);
            }
        }

        int randomIndex = Random.Range(0, skillIdListClone.Count);
        return skillIdListClone[randomIndex];
    }

    private void OnSpinButtonClick(GameObject target)
    {
        Spin(_characterId);
    }

    private void OnOkButtonClick(GameObject target)
    {
        ClosePopup();
        _characterManagementPopup.OpenPopup(_characterId);
    }

    private void OnSlotLockButtonAClick(GameObject target)
    {
        ClosePopup();
        _slotLockIapPopup.OpenPopup(_characterId, 0);
    }

    private void OnSlotLockButtonBClick(GameObject target)
    {
        ClosePopup();
        _slotLockIapPopup.OpenPopup(_characterId, 1);
    }

    private void OnRollBackButtonClick(GameObject target)
    {
        Rollback();
    }

    public void OnCloseButtonClick()
    {
        ClosePopup();
        _characterManagementPopup.OpenPopup(_characterId);
    }





    private void ShowSlotLockButtons(int characterId)
    {
        GDECharacterManagementItemData gmItem = _characterManagementService.GetItemById(characterId);

        // 스킬 중 한개가 잠겨있으면 Slot Lock Button 모두 숨김
        if (!gmItem.isSkillUnlockA || !gmItem.isSkillUnlockB)
        {
            HideSlotLockButtons();
            return;
        }

        // 스킬이 비어있는경우 Slot Lock Button 모두 숨김
        if (gmItem.skillIdA == 0 || gmItem.skillIdB == 0)
        {
            HideSlotLockButtons();
            return;
        }

        // 슬롯 중 한개가 잠겼으면 Slot Lock Button 모두 숨김
        if (gmItem.isSlotALocked || gmItem.isSlotBLocked)
        {
            HideSlotLockButtons();
            return;
        }

        _slotLockButtonA.gameObject.SetActive(true);
        _slotLockButtonAreaA.gameObject.SetActive(false);

        _slotLockButtonB.gameObject.SetActive(true);
        _slotLockButtonAreaB.gameObject.SetActive(false);
    }

    private void HideSlotLockButtons()
    {
        _slotLockButtonA.gameObject.SetActive(false);
        _slotLockButtonAreaA.gameObject.SetActive(true);

        _slotLockButtonB.gameObject.SetActive(false);
        _slotLockButtonAreaB.gameObject.SetActive(true);
    }

    private void Rollback()
    {
        if (NewADManager.instance.IsReady())
        {
            NewADManager.instance.PlayAD("SkillRollback");
        }

        _rollbackButon.gameObject.SetActive(false);
        _okButton.gameObject.SetActive(false);

        _progress.gameObject.SetActive(true);
        _progress.Play();
    }

    private void OnAdSuccess(string key)
    {
        if (key == "SkillRollback")
        {
            GDECharacterManagementItemData gmItem = _characterManagementService.GetItemById(_characterId);
            _characterManagementService.SetSkillA(_characterId, _rollbackSkillIdA);
            _characterManagementService.SetSkillB(_characterId, _rollbackSkillIdB);

            _slotA.RollBack();
            _slotB.RollBack();

            DOTween.Kill("SkillSlotMachine.Rollback.Delay." + GetInstanceID());
            DOVirtual.DelayedCall(1.1f, OnRollbackEnded).SetId("SkillSlotMachine.Rollback.Delay." + GetInstanceID());
        }
    }

    private void OnFailed(string key)
    {

    }

    private void OnRollbackEnded()
    {
        _okButton.gameObject.SetActive(true);

        _progress.Stop();
        _progress.gameObject.SetActive(false);
    }

    private void GetSpinPrice()
    {

    }





    public void OpenPopup(int characterId)
    {
        _characterId = characterId;
        GDECharacterManagementItemData gmItem = _characterManagementService.GetItemById(characterId);

        _spinButtonUIGold.SetGold(_characterManagementService.GetSlotMachineSpinPrice(characterId));

        _progress.Stop();
        _progress.gameObject.SetActive(false);

        _basicSkillIcon.SetSkillId(gmItem.basicSkillId);

        List<int> otherSkillIds_A = new List<int>();
        for (int i = 0; i < _slotA.GetOtherIconsCount(); i++)
        {
            otherSkillIds_A.Add(_characterManagementService.GetRandomSkillId());
        }

        List<int> otherSkillIds_B = new List<int>();
        for (int i = 0; i < _slotB.GetOtherIconsCount(); i++)
        {
            otherSkillIds_B.Add(_characterManagementService.GetRandomSkillId());
        }


        ShowSlotLockButtons(characterId);

        // Slot A
        _rollbackSkillIdA = gmItem.skillIdA;
        int prevSkillIdA = _characterManagementService.GetPrevSkillId(gmItem.skillIdA);
        int nextSkillIdA = _characterManagementService.GetNextSkillId(gmItem.skillIdA);

        List<int> filters = new List<int> { gmItem.basicSkillId, gmItem.skillIdB };
        int randomSkillIdA = GetRandomSkillId(filters);
        int lastPrevSkillIdA = _characterManagementService.GetPrevSkillId(randomSkillIdA);
        int lastNextSkillIdA = _characterManagementService.GetNextSkillId(randomSkillIdA);
        _selectedSkillIdA = randomSkillIdA;

        _slotA.Init(prevSkillIdA, gmItem.skillIdA, nextSkillIdA, lastPrevSkillIdA, randomSkillIdA, lastNextSkillIdA, otherSkillIds_A);

        if (!gmItem.isSkillUnlockA) _slotA.LockSkill(characterId);
        if (gmItem.isSlotALocked) _slotA.LockSlot();

        // Slot B
        _rollbackSkillIdB = gmItem.skillIdB;
        int prevSkillIdB = _characterManagementService.GetPrevSkillId(gmItem.skillIdB);
        int nextSkillIdB = _characterManagementService.GetNextSkillId(gmItem.skillIdB);

        filters = new List<int> { gmItem.basicSkillId, randomSkillIdA, gmItem.skillIdA };
        int randomSkillIdB = GetRandomSkillId(filters);
        int lastPrevSkillIdB = _characterManagementService.GetPrevSkillId(randomSkillIdB);
        int lastNextSkillIdB = _characterManagementService.GetNextSkillId(randomSkillIdB);
        _selectedSkillIdB = randomSkillIdB;

        _slotB.Init(prevSkillIdB, gmItem.skillIdB, nextSkillIdB, lastPrevSkillIdB, randomSkillIdB, lastNextSkillIdB, otherSkillIds_B);

        //gameObject.SetActive(true);

        if (!gmItem.isSkillUnlockB) _slotB.LockSkill(characterId);
        if (gmItem.isSlotBLocked) _slotB.LockSlot();

        // Buttons
        _rollbackButon.gameObject.SetActive(false);
        _okButton.gameObject.SetActive(false);
        _spinButton.gameObject.SetActive(true);
        _closeButton.gameObject.SetActive(true);

        _characterManagementPopup.ClosePopup();
        base.Open();
    }

    public void ClosePopup()
    {
        //gameObject.SetActive(false);
        base.Close();
    }

    public void Spin(int characterId)
    {
        int spinPrice = _characterManagementService.GetSlotMachineSpinPrice(characterId);
        bool isPaymentSuccess = _userService.Spend(spinPrice);

        if (!isPaymentSuccess) return;


        _progress.gameObject.SetActive(true);
        _progress.Play();

        _spinButton.gameObject.SetActive(false);
        _closeButton.gameObject.SetActive(false);

        HideSlotLockButtons();

        GDECharacterManagementItemData gmItem = _characterManagementService.GetItemById(characterId);

        HideSlotLockButtons();

        if (!gmItem.isSkillUnlockB || gmItem.isSlotBLocked)
        {
            _slotA.Spined += OnSpined;
            _slotA.Spin(4.5f);

            _characterManagementService.IncSpinCountA(characterId);
        }
        else if(!gmItem.isSkillUnlockA || gmItem.isSlotALocked)
        {
            _slotB.Spined += OnSpined;
            _slotB.Spin(4.5f);

            _characterManagementService.IncSpinCountB(characterId);
        }
        else
        {
            _slotA.Spin(4.5f);
            _slotB.Spined += OnSpined;
            _slotB.Spin(5f);

            _characterManagementService.IncSpinCountA(characterId);
            _characterManagementService.IncSpinCountB(characterId);
        }

        if (gmItem.isSkillUnlockA && !gmItem.isSlotALocked) _characterManagementService.SetSkillA(_characterId, _selectedSkillIdA);
        else if (gmItem.isSlotALocked) _characterManagementService.SetSkillA(_characterId, _rollbackSkillIdA);
        else _characterManagementService.SetSkillA(_characterId, 0);

        if (gmItem.isSkillUnlockB && !gmItem.isSlotBLocked) _characterManagementService.SetSkillB(_characterId, _selectedSkillIdB);
        else if (gmItem.isSlotBLocked) _characterManagementService.SetSkillB(_characterId, _rollbackSkillIdB);
        else _characterManagementService.SetSkillB(_characterId, 0);

        _characterManagementService.UnlockSlot(characterId, 0);
        _characterManagementService.UnlockSlot(characterId, 1);

        //GDECharacterManagementItemData gmItem = _characterManagementService.GetItemById(characterId);
        //int skillIdA = 0;
        //int skillIdB = 0;

        //if(gmItem.isSkillUnlockA)
        //{
        //    List<int> filters = new List<int> { gmItem.basicSkillId, gmItem.skillIdB };
        //    skillIdA = GetRandomSkillId(filters);
        //    Debug.Log("############## skill A: " + skillIdA);

        //    _characterManagementService.SetSkillA(characterId, skillIdA);
        //}

        //if(gmItem.isSkillUnlockB)
        //{
        //    List<int> filters = new List<int> { gmItem.basicSkillId, gmItem.skillIdA };
        //    skillIdB = GetRandomSkillId(filters);

        //    _characterManagementService.SetSkillB(characterId, skillIdB);
        //}
    }

    private void OnSpined()
    {
        _progress.Stop();
        _progress.gameObject.SetActive(false);

        _slotB.Spined -= OnSpined;

        _rollbackButon.gameObject.SetActive(true);
        _okButton.gameObject.SetActive(true);

        //ShowSlotLockButtons(_characterId);
    }
}
