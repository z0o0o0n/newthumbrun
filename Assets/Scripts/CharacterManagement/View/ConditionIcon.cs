﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionIcon : MonoBehaviour
{
    public delegate void ConditionIconEvent(bool isPress);
    public event ConditionIconEvent Press;

    [SerializeField]
    private UISprite _icon;

	void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}




    void OnPress(bool isPress)
    {
        if (Press != null) Press(isPress);
    }





    public void SetCondition(int conditionId)
    {
        _icon.spriteName = "Condition_" + conditionId;

        UIButton uiButton = GetComponent<UIButton>();
        uiButton.normalSprite = "Condition_" + conditionId;
        uiButton.defaultColor = Color.white;
    }
}
