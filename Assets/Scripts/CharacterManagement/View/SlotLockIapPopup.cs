﻿using Com.Mod.ThumbRun.CharacterManagement.Application;
using Com.Mod.ThumbRun.CharacterManagement.Domain;
using Com.Mod.ThumbRun.Common.View;
using Com.Mod.ThumbRun.IAPManager.Domain;
using Com.Mod.ThumbRun.UI.Popup;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class SlotLockIapPopup : BasicPopup
{
    [SerializeField]
    private SkillSlotMachine _skillSlotMachinePopup;
    [SerializeField]
    private UILabel _priceLabel;
    [SerializeField]
    private UIDynamicButton _buyButton;
    [SerializeField]
    private LoadingScreen _loadingScreen;
    private CharacterManagementService _characterManagementService;
    private int _characterId = -1;
    private int _slotId = -1;

	void Start ()
    {
        transform.localPosition = Vector2.zero;
        ClosePopup();

		Product product = IAPManager.instance.GetProductById("unlock_slot_tier1");
		if(product != null)
		{
			_priceLabel.text = product.metadata.localizedPriceString;
		}		

        _buyButton.Click += OnBuyButtonClick;

        _characterManagementService = GameObject.FindGameObjectWithTag("CharacterManagement").GetComponent<CharacterManagementService>();
        _characterManagementService.Prepared += OnPrepared;

        if (_characterManagementService.isPrepared) OnPrepared();
    }
	
	void Update () {
		
	}

    private void OnDestroy()
    {
        _buyButton.Click -= OnBuyButtonClick;
    }





    private void OnPrepared()
    {
        if (!_characterManagementService.isPrepared) return;
    }

    public void OnCloseButtonClick()
    {
        ClosePopup();
        _skillSlotMachinePopup.OpenPopup(_characterId);
    }

    private void OnBuyButtonClick(GameObject target)
    {
        _loadingScreen.gameObject.SetActive(true);

        IAPManager.instance.PurchaseSuccess += OnPurchaseSuccess;
        IAPManager.instance.PurchaseFail += OnPurchaseFail;
        IAPManager.instance.Buy("unlock_slot_tier1");

        //_loadingScreen.gameObject.SetActive(false);
        //_characterManagementService.LockSlot(_characterId, _slotId);
        //OnCloseButtonClick();
    }

    private void OnPurchaseSuccess(string productId)
    {
        _loadingScreen.gameObject.SetActive(false);
        IAPManager.instance.PurchaseSuccess -= OnPurchaseSuccess;
        IAPManager.instance.PurchaseFail -= OnPurchaseFail;

        _characterManagementService.LockSlot(_characterId, _slotId);
        OnCloseButtonClick();
    }

    private void OnPurchaseFail(string productId, string errorId)
    {
        Debug.Log("Roulette Machine - OnPurchaseFail /  errorId: " + errorId);
        _loadingScreen.gameObject.SetActive(false);
        IAPManager.instance.PurchaseSuccess -= OnPurchaseSuccess;
        IAPManager.instance.PurchaseFail -= OnPurchaseFail;

        OnCloseButtonClick();
    }





    public void OpenPopup(int characterId, int slotId)
    {
        _characterId = characterId;
        _slotId = slotId;
        base.Open();
    }

    public void ClosePopup()
    {
        base.Close();
    }
}
