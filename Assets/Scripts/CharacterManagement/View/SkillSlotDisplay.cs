﻿using Com.Mod.ThumbRun.CharacterManagement.Application;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillSlotDisplay : MonoBehaviour
{
    [SerializeField]
    private SkillIcon _basicSkillIcon;
    [SerializeField]
    private SkillIcon _addSkillIconA;
    [SerializeField]
    private SkillIcon _addSkillIconB;
    [SerializeField]
    private UISprite _basicSkillSlotBg;
    [SerializeField]
    private UISprite _addSkillSlotBgA;
    [SerializeField]
    private UISprite _addSkillSlotBgB;
    [SerializeField]
    private SkillLockIcon _skillLockIconA;
    [SerializeField]
    private SkillLockIcon _skillLockIconB;
    [SerializeField]
    private UISprite _skillDeactivationIconBasic;
    [SerializeField]
    private UISprite _skillDeactivationIconA;
    [SerializeField]
    private UISprite _skillDeactivationIconB;
    [SerializeField]
    private Color _basicSkillSlotBgColor;
    [SerializeField]
    private Color _deactivatedSlotBgColor;
    [SerializeField]
    private UIDynamicButton _skillChangeButton;
    [SerializeField]
    private SkillSlotMachine _skillSlotMachine;
    [SerializeField]
    private UISprite _redBeanA;
    [SerializeField]
    private UISprite _redBeanB;
    private CharacterManagementService _characterManagementService;
    private int _characterId = -1;
    private bool _isPrepared = false;

    private void Awake()
    {
        _characterManagementService = GameObject.FindGameObjectWithTag("CharacterManagement").GetComponent<CharacterManagementService>();
        _characterManagementService.Prepared += OnPrepared;
        _characterManagementService.Updated += OnCharcterUpdated;

        if (_characterManagementService.isPrepared) OnPrepared();

        if (_skillChangeButton != null) _skillChangeButton.Click += OnSkillChangeButtonClick;
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    private void OnDestroy()
    {
        _characterManagementService.Prepared -= OnPrepared;
        _characterManagementService.Updated -= OnCharcterUpdated;

        if (_skillChangeButton != null) _skillChangeButton.Click -= OnSkillChangeButtonClick;
    }




    private void OnPrepared()
    {
        if (!_characterManagementService.isPrepared) return;

        _isPrepared = true;
    }

    private void OnCharcterUpdated(int characterId)
    {
        if (!_isPrepared) return;
        DisplaySkillIcons(characterId);
    }

    private void OnSkillChangeButtonClick(GameObject target)
    {
        _skillSlotMachine.OpenPopup(_characterId);
        //_skillSlotMachine.Spin(_characterId);
        //DisplaySkillIcons(_characterId);
    }




    public void DisplaySkillIcons(int characterId)
    {
        if (!_isPrepared) return;
        
        _characterId = characterId;
        _skillLockIconA.SetCharacterId(_characterId);
        _skillLockIconB.SetCharacterId(_characterId);

        GDECharacterManagementItemData gmItem = _characterManagementService.GetItemById(characterId);

        // 스킬 변경 버튼
        if (_skillChangeButton != null)
        {
            if (gmItem.isSkillUnlockA || gmItem.isSkillUnlockB)
            {
                if (gmItem.isUnlocked) _skillChangeButton.gameObject.SetActive(true);
                else _skillChangeButton.gameObject.SetActive(false);
            }
            else _skillChangeButton.gameObject.SetActive(false);
        }

        // 스킬 슬롯
        ResetSlotBg();

        // 스킬 언락 빨콩
        _redBeanA.gameObject.SetActive(false);
        _redBeanB.gameObject.SetActive(false);
        if (gmItem.isNewSlotA) _redBeanA.gameObject.SetActive(true);
        if (gmItem.isNewSlotB) _redBeanB.gameObject.SetActive(true);

        _basicSkillIcon.SetSkillId(gmItem.basicSkillId);
        if (gmItem.isBasicSkillDeactivated)
        {
            _basicSkillIcon.Deactivate();
            _basicSkillSlotBg.color = _deactivatedSlotBgColor;
            _skillDeactivationIconBasic.gameObject.SetActive(true);
        }
        else
        {
            _basicSkillIcon.Activate();
            _basicSkillSlotBg.color = _basicSkillSlotBgColor;
            _skillDeactivationIconBasic.gameObject.SetActive(false);
        }

        //Debug.Log("#$################# character id: " + characterId + " / gmItem.isSkillUnlockA: " + gmItem.isSkillUnlockA);

        if (gmItem.isSkillUnlockA) // 슬롯 A 언락 됨
        {
            if (gmItem.skillIdA == 0) // 슬롯에 보유 스킬 없음
            {
                _addSkillIconA.gameObject.SetActive(false);
                _skillLockIconA.gameObject.SetActive(true);
                _skillLockIconA.FadeOut();
            }
            else // 슬롯에 보유 스킬 있음
            {
                _addSkillIconA.SetSkillId(gmItem.skillIdA);
                //_addSkillIconA.Activate();
                //_addSkillIconA.color = Color.white;
                _addSkillIconA.gameObject.SetActive(true);
                _skillLockIconA.gameObject.SetActive(false);

                if (gmItem.isSkillDeactivatedA) // 스킬 비활성
                {
                    _addSkillIconA.Deactivate();
                    _addSkillSlotBgA.color = _deactivatedSlotBgColor;
                    _addSkillSlotBgA.alpha = 1f;
                    _skillDeactivationIconA.gameObject.SetActive(true);
                }
                else // 스킬 활성
                {
                    _addSkillIconA.Activate();
                    _addSkillSlotBgA.color = Color.black;
                    _addSkillSlotBgA.alpha = 0.3f;
                    _skillDeactivationIconA.gameObject.SetActive(false);
                }
            }
        }
        else // 슬롯 A 언락 안됨
        {
            //_addSkillIconA.spriteName = "SkillIcon_99";
            //_addSkillIconA.color = Color.black;
            //_addSkillIconA.alpha = 0.6f;
            _addSkillIconA.gameObject.SetActive(false);
            _skillLockIconA.FadeIn();
            _skillLockIconA.gameObject.SetActive(true);
            _skillDeactivationIconA.gameObject.SetActive(false);
        }

        if (gmItem.isSkillUnlockB) // 슬롯 B 언락 됨
        {
            _skillLockIconB.gameObject.SetActive(false);

            if (gmItem.skillIdB == 0) // 슬롯에 보유 스킬 없음
            {
                _addSkillIconB.gameObject.SetActive(false);
                _skillLockIconB.gameObject.SetActive(true);
                _skillLockIconB.FadeOut();
            }
            else // 슬롯에 보유 스킬 있음
            {
                _addSkillIconB.SetSkillId(gmItem.skillIdB);
                //_addSkillIconB.color = Color.white;
                _addSkillIconB.gameObject.SetActive(true);

                if (gmItem.isSkillDeactivatedB) // 스킬 비활성
                {
                    _addSkillIconB.Deactivate();
                    _addSkillSlotBgB.color = _deactivatedSlotBgColor;
                    _addSkillSlotBgB.alpha = 1f;
                    _skillDeactivationIconB.gameObject.SetActive(true);
                }
                else // 스킬 활성
                {
                    _addSkillIconB.Activate();
                    _addSkillSlotBgB.color = Color.black;
                    _addSkillSlotBgB.alpha = 0.3f;
                    _skillDeactivationIconB.gameObject.SetActive(false);
                }
            }
        }
        else // 슬롯 B 언락 안됨
        {
            //_addSkillIconB.spriteName = "SkillIcon_99";
            //_addSkillIconB.color = Color.black;
            //_addSkillIconB.alpha = 0.6f;
            _addSkillIconB.gameObject.SetActive(false);
            _skillLockIconB.FadeIn();
            _skillLockIconB.gameObject.SetActive(true);
            _skillDeactivationIconB.gameObject.SetActive(false);
        }
    }

    private void ResetSlotBg()
    {
        _basicSkillSlotBg.color = _basicSkillSlotBgColor;
        _skillDeactivationIconBasic.gameObject.SetActive(false);

        _addSkillSlotBgA.color = Color.black;
        _addSkillSlotBgA.alpha = 0.3f;
        _skillDeactivationIconA.gameObject.SetActive(false);

        _addSkillSlotBgB.color = Color.black;
        _addSkillSlotBgB.alpha = 0.3f;
        _skillDeactivationIconB.gameObject.SetActive(false);
    }
}
