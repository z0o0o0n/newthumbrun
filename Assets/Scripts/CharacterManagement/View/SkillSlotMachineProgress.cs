﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillSlotMachineProgress : MonoBehaviour
{
    [SerializeField]
    private List<UISprite> _dots;

    void Awake ()
    {
        Reset();
    }

	void Start ()
    {
	}
	
	void Update ()
    {
		if(Input.GetKeyUp(KeyCode.A))
        {
            Play();
        }
	}



    private void Reset()
    {
        for(int i = 0; i < _dots.Count; i++)
        {
            DOTween.Kill("SlotMachineDot.Alpha." + _dots[i].GetInstanceID());
            _dots[i].alpha = 0.05f;
        }
    }




    public void Play()
    {
        for (int i = 0; i < _dots.Count; i++)
        {
            UISprite dot = _dots[i];
            DOTween.Kill("SlotMachineDot.Alpha." + dot.GetInstanceID());
            DOTween.To(() => dot.alpha, x => dot.alpha = x, 1f, 0.3f).SetId("SlotMachineDot.Alpha." + dot.GetInstanceID()).SetLoops(-1, LoopType.Yoyo).SetDelay(0.1f * i);
        }
    }

    public void Stop()
    {
        Reset();
    }
}
