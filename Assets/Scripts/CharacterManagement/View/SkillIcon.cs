﻿using Junhee.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillIcon : MonoBehaviour
{
    [SerializeField]
    private UISprite _skillIcon;
    [SerializeField]
    private int _skillId = -1;
    [SerializeField]
    private SkillInfoBubble.DisplayDirection _bubbleDirection = SkillInfoBubble.DisplayDirection.T;

    private GameObject _skillInfoBubbleGO;
    private SkillInfoBubble _skillInfoBubble;
    //private bool _isInfoShowed = false;

    private void Awake()
    {
        _skillInfoBubbleGO = GameObject.FindGameObjectWithTag("SkillInfoBubble");

        if(_skillInfoBubbleGO != null) _skillInfoBubble = _skillInfoBubbleGO.GetComponent<SkillInfoBubble>();
        //SetSkillId(_skillId);
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    private void OnDisable()
    {
        HideInfoBubble();
    }

    void OnPress(bool isPress)
    {
        if (isPress)
        {
            ShowInfoBubble();
        }
        else
        {
            HideInfoBubble();
        }
    }

    private void ShowInfoBubble()
    {
        _skillInfoBubbleGO = GameObject.FindGameObjectWithTag("SkillInfoBubble");
        _skillInfoBubble = _skillInfoBubbleGO.GetComponent<SkillInfoBubble>();

        if (!_skillInfoBubble.isShowed)
        {
            _skillInfoBubble.Show(_skillId, transform.position, _bubbleDirection);
        }
    }

    private void HideInfoBubble()
    {
        _skillInfoBubbleGO = GameObject.FindGameObjectWithTag("SkillInfoBubble");
        _skillInfoBubble = _skillInfoBubbleGO.GetComponent<SkillInfoBubble>();

        if (_skillInfoBubble.isShowed)
        {
            _skillInfoBubble.Hide();
        }
    }




    public void SetSkillId(int skillId)
    {
        _skillId = skillId;
        _skillIcon.spriteName = "SkillIcon_" + skillId;
        _skillIcon.GetComponent<UIButton>().normalSprite = "SkillIcon_" + skillId;
    }

    public void Deactivate()
    {
        _skillIcon.alpha = 0.2f;
        Color deactivationColor = new Color();
        ColorUtility.TryParseHtmlString("#666666FF", out deactivationColor);
        _skillIcon.GetComponent<UIButton>().duration = 0;
        _skillIcon.GetComponent<UIButton>().defaultColor = deactivationColor;
        _skillIcon.GetComponent<UIButton>().hover = deactivationColor;
        _skillIcon.GetComponent<UIButton>().pressed = deactivationColor;
        _skillIcon.GetComponent<UIButton>().disabledColor = deactivationColor;
    }

    public void Activate()
    {
        _skillIcon.alpha = 1f;
        Color activationColor = new Color();
        ColorUtility.TryParseHtmlString("#FFFFFFFF", out activationColor);
        _skillIcon.GetComponent<UIButton>().duration = 0;
        _skillIcon.GetComponent<UIButton>().defaultColor = activationColor;
        _skillIcon.GetComponent<UIButton>().hover = activationColor;
        _skillIcon.GetComponent<UIButton>().pressed = activationColor;
        _skillIcon.GetComponent<UIButton>().disabledColor = activationColor;
    }
}
