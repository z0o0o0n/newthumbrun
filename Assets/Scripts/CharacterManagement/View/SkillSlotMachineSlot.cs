﻿using Com.Mod.ThumbRun.CharacterManagement.Application;
using DG.Tweening;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillSlotMachineSlot : MonoBehaviour
{
    public delegate void SlotEvent();
    public event SlotEvent Spined;

    [SerializeField]
    private int _slotId = -1;
    [SerializeField]
    private SkillIcon _currentSkillIcon;
    [SerializeField]
    private UISprite _emptyIcon;
    [SerializeField]
    private SkillIcon _endSkillIcon;
    [SerializeField]
    private UISprite _prevIcon;
    [SerializeField]
    private UISprite _nextIcon;
    [SerializeField]
    private UISprite _lastPrevIcon;
    [SerializeField]
    private UISprite _lastNextIcon;
    [SerializeField]
    private List<UISprite> _icons;
    [SerializeField]
    private UIWidget _iconContainer;
    [SerializeField]
    private UISprite _bg;
    [SerializeField]
    private Color _lockBgColor;
    [SerializeField]
    private UISprite _lockIcon;
    [SerializeField]
    private SkillLockIcon _skillLockIcon;
    [SerializeField]
    private AnimationCurve _customElastic;

    private bool _isSlotLocked = false;
    private bool _isSkillLocked = false;

    void Start ()
    {
	}
	
	void Update ()
    {
        //if(Input.GetKeyUp(KeyCode.A))
        //{
        //    LockSkill();
        //}
        //if (Input.GetKeyUp(KeyCode.S))
        //{
        //    LockSlot();
        //}
        //if (Input.GetKeyUp(KeyCode.D))
        //{
        //    Reset();
        //}
    }





    public void Init(int prevSkillId, int currentSkillId, int nextSkillId, int lastPrevSkillId, int endSkillId, int lastNextSkillId, List<int> otherSkillIds)
    {
        Reset();

        if (currentSkillId == 0)
        {
            _currentSkillIcon.gameObject.SetActive(false);
            _emptyIcon.gameObject.SetActive(true);
        }
        else
        {
            _currentSkillIcon.SetSkillId(currentSkillId);
            _currentSkillIcon.gameObject.SetActive(true);
            _emptyIcon.gameObject.SetActive(false);
        }

        _prevIcon.spriteName = "SkillIcon_" + prevSkillId;
        _nextIcon.spriteName = "SkillIcon_" + nextSkillId;
        
        _endSkillIcon.SetSkillId(endSkillId);

        _lastPrevIcon.spriteName = "SkillIcon_" + lastPrevSkillId;
        _lastNextIcon.spriteName = "SkillIcon_" + lastNextSkillId;

        for(int i = 0; i < _icons.Count; i++)
        {
            _icons[i].spriteName = "SkillIcon_" + otherSkillIds[i];
        }
    }

    public void Spin(float time)
    {
        if (_isSlotLocked) return;
        if (_isSkillLocked) return;

        DOTween.Kill("IconContainer.MoveY." + GetInstanceID());
        DOTween.Kill("IconContainer.MoveY1." + GetInstanceID());
        _iconContainer.transform.DOLocalMoveY(-1125f, time).SetId("IconContainer.MoveY." + GetInstanceID()).SetEase(Ease.OutCubic).OnComplete(OnSpined);
        _iconContainer.transform.DOLocalMoveY(-1120f, 0.3f).SetId("IconContainer.MoveY1." + GetInstanceID()).SetDelay(time).SetEase(Ease.InOutCubic);
    }

    private void OnSpined()
    {
        if (Spined != null) Spined();
    }

    public void LockSlot()
    {
        if(!_isSlotLocked)
        {
            _isSlotLocked = true;

            _bg.color = _lockBgColor;
            _lockIcon.gameObject.SetActive(true);
            _currentSkillIcon.gameObject.SetActive(true);

            _skillLockIcon.gameObject.SetActive(false);
            _prevIcon.gameObject.SetActive(false);
            _nextIcon.gameObject.SetActive(false);
            _lastPrevIcon.gameObject.SetActive(false);
            _lastNextIcon.gameObject.SetActive(false);
        }
    }

    public void LockSkill(int characterId)
    {
        if(!_isSkillLocked)
        {
            _isSkillLocked = true;

            _bg.color = _lockBgColor;
            _skillLockIcon.gameObject.SetActive(true);
            _skillLockIcon.SetCharacterId(characterId);

            _currentSkillIcon.gameObject.SetActive(false);
            _lockIcon.gameObject.SetActive(false);
            _prevIcon.gameObject.SetActive(false);
            _nextIcon.gameObject.SetActive(false);
            _lastPrevIcon.gameObject.SetActive(false);
            _lastNextIcon.gameObject.SetActive(false);
        }        
    }

    // 슬롯을 돌리기 전 상태로 되돌림
    public void RollBack()
    {
        DOTween.Kill("IconContainer.MoveY." + GetInstanceID());
        DOTween.Kill("IconContainer.MoveY1." + GetInstanceID());
        _iconContainer.transform.DOLocalMoveY(0f, 1f).SetId("IconContainer.MoveY." + GetInstanceID()).SetEase(_customElastic);
    }

    public void Reset()
    {
        DOTween.Kill("IconContainer.MoveY." + GetInstanceID());
        DOTween.Kill("IconContainer.MoveY1." + GetInstanceID());
        _iconContainer.transform.DOLocalMoveY(0f, 0f).SetId("IconContainer.MoveY." + GetInstanceID());

        _isSkillLocked = false;
        _isSlotLocked = false;

        _bg.color = Color.white;
        _lockIcon.gameObject.SetActive(false);
        _skillLockIcon.gameObject.SetActive(false);

        _currentSkillIcon.gameObject.SetActive(true);
        _prevIcon.gameObject.SetActive(true);
        _nextIcon.gameObject.SetActive(true);
        _lastPrevIcon.gameObject.SetActive(true);
        _lastNextIcon.gameObject.SetActive(true);
    }

    public int GetOtherIconsCount()
    {
        return _icons.Count;
    }
}
