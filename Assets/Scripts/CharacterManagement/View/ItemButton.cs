﻿namespace Com.Mod.ThumbRun.CharacterManagement.View
{
    using UnityEngine;
    using System.Collections;

    public class ItemButton : MonoBehaviour
    {
        // Item은 캐릭터 관리 팝업의 캐릭터를 말함.
        public delegate void ItemButtonEventHandler(int characterId);
        public event ItemButtonEventHandler Pressed;

        [SerializeField]
        private UISprite _characterImage;
        [SerializeField]
        private Color _enableColor;
        [SerializeField]
        private Color _disableColor;
        [SerializeField]
        private ConditionDisplay _conditionDisplay;
        [SerializeField]
        private UISprite _newIcon;
        private int _characterId = -1;

        private void Awake()
        {
            HideNewIcon();
        }

        public void Press()
        {
            if (Pressed != null) Pressed(_characterId);
        }

        public void SetCharacterId(int characterId, int condition)
        {
            _characterId = characterId;
            _characterImage.spriteName = "CharacterProfileImage_" + characterId;

            _conditionDisplay.ChangeCondition(condition);
        }

        public int GetCharacterId()
        {
            return _characterId;
        }

        public void Enable()
        {
            _characterImage.color = _enableColor;
            _conditionDisplay.gameObject.SetActive(true);
        }

        public void Disable()
        {
            _characterImage.color = _disableColor;
            _conditionDisplay.gameObject.SetActive(false);
        }

        public void ShowNewIcon()
        {
            _newIcon.gameObject.SetActive(true);
        }

        public void HideNewIcon()
        {
            _newIcon.gameObject.SetActive(false);
        }
    }
}