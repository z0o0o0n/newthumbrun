﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillLockIcon : MonoBehaviour
{
    [SerializeField]
    private int _characterId = -1;
    [SerializeField]
    private int _slotId = -1;
    [SerializeField]
    private SkillMissionInfoBubble.DisplayDirection _bubbleDirection = SkillMissionInfoBubble.DisplayDirection.T;

    private GameObject _skillMissionInfoBubbleGO;
    private SkillMissionInfoBubble _skillMissionInfoBubble;

    private void Awake()
    {
        _skillMissionInfoBubbleGO = GameObject.FindGameObjectWithTag("SkillMissionInfoBubble");
        if (_skillMissionInfoBubbleGO != null) _skillMissionInfoBubble = _skillMissionInfoBubbleGO.GetComponent<SkillMissionInfoBubble>();
        //SetSkillId(_skillId);
    }

    void Start()
    {

    }

    void Update()
    {

    }

    void OnPress(bool isPress)
    {
        if (isPress)
        {
            if (_skillMissionInfoBubbleGO != null)
            {
                ShowInfoBubble();
            }
        }
        else
        {
            if (_skillMissionInfoBubbleGO != null)
            {
                HideInfoBubble();
            }
        }
    }

    private void ShowInfoBubble()
    {
        if (!_skillMissionInfoBubble.isShowed)
        {
            _skillMissionInfoBubble.Show(_characterId, _slotId, transform.position, _bubbleDirection);
        }
    }

    private void HideInfoBubble()
    {
        if (_skillMissionInfoBubble.isShowed) _skillMissionInfoBubble.Hide();
    }




    public void SetCharacterId(int characterId)
    {
        _characterId = characterId;
    }

    public void Deactivate()
    {
        //_skillIcon.alpha = 0.2f;
    }

    public void Activate()
    {
        //_skillIcon.alpha = 1f;
    }

    public void FadeOut()
    {
        UISprite image = GetComponent<UISprite>();
        image.alpha = 0.01f;

        UIButton button = GetComponent<UIButton>();
        button.defaultColor = new Color(0, 0, 0, 0.01f);
        button.hover = button.defaultColor;
        button.pressed = button.defaultColor;
        button.disabledColor = button.defaultColor;
    }

    public void FadeIn()
    {
        UISprite image = GetComponent<UISprite>();
        image.alpha = 1f;

        UIButton button = GetComponent<UIButton>();
        button.defaultColor = new Color(0, 0, 0, 1f);
        button.hover = button.defaultColor;
        button.pressed = button.defaultColor;
        button.disabledColor = button.defaultColor;
    }
}
