﻿using Com.Mod.ThumbRun.CharacterManagement.Application;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionRecoveryButton : MonoBehaviour
{
    [SerializeField]
    private List<Color> _buttonBgColor;
    [SerializeField]
    private List<Color> _buttonSideColor;
    [SerializeField]
    private UISprite _currentConditionIcon;
    [SerializeField]
    private UISprite _currentConditionBg;
    [SerializeField]
    private UISprite _recoveredConditionIcon;
    [SerializeField]
    private UISprite _recoveredConditionBg;
    [SerializeField]
    private UISprite _buttonSide;

    void Start ()
    {
        
    }
    




    public void SetCondition(int condition)
    {
        if (condition > 2) condition = 2;
        else if (condition < 0) condition = 0;

        _currentConditionIcon.spriteName = "Condition_" + condition;
        _currentConditionBg.color = _buttonBgColor[condition];

        _recoveredConditionIcon.spriteName = "Condition_" + (condition + 1);
        _recoveredConditionBg.color = _buttonBgColor[condition + 1];

        _buttonSide.color = _buttonSideColor[condition];
    }
}
