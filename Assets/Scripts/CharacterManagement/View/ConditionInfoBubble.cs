﻿using Com.Mod.Game.LanguageManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionInfoBubble : MonoBehaviour
{
    public enum DisplayDirection { L, R, T, B, LT, RT, LB, RB };

    [SerializeField]
    private UILabel _title;
    [SerializeField]
    private UILabel _script;
    [SerializeField]
    private UISprite _bg;
    private UIPanel _uiPanel;
    private bool _isShowed = false;

    private static ConditionInfoBubble _instance;

    public static ConditionInfoBubble instance
    {
        get { return _instance; }
    }

    public bool isShowed
    {
        get { return _isShowed; }
    }





    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            _uiPanel = GetComponent<UIPanel>();
        }
    }
    void Start()
    {
        Hide();
    }

    void Update()
    {

    }




    private void SetConditionId(int conditionId)
    {
        string title = LanguageManager.instance.GetValue("ConditionName_" + conditionId);
        string script = LanguageManager.instance.GetValue("ConditionInfo_" + conditionId);
        //string currentState = MissionManager.instance.GetCurrentState(characterId, skillSlotId);

        _title.text = title;
        _script.text = script;

        ResizeBg();
    }

    private void ResizeBg()
    {
        int titleBottomSpace = 10;
        int infoHeight = _title.height + _script.height + titleBottomSpace;
        int maxInfoWidth = (_title.width > _script.width) ? _title.width : _script.width;

        _bg.width = maxInfoWidth + 40;
        _bg.height = infoHeight + 40;

        _title.transform.localPosition = new Vector2(0f, (infoHeight / 2));

        int lineSpace = 4;
        float scriptPosY = _title.transform.localPosition.y - _title.height - titleBottomSpace - lineSpace;
        _script.transform.localPosition = new Vector2(0f, scriptPosY);
    }

    private void Replace(ConditionInfoBubble.DisplayDirection direction)
    {
        Vector2 pos = transform.localPosition;
        float marginX = 50;
        float marginY = 50;

        if (direction == DisplayDirection.L)
        {
            pos.x -= (_bg.width / 2) + marginX;
        }
        else if (direction == DisplayDirection.R)
        {
            pos.x += (_bg.width / 2) + marginX;
        }
        else if (direction == DisplayDirection.B)
        {
            pos.y -= (_bg.height / 2) + marginY;
        }
        else if (direction == DisplayDirection.T)
        {
            pos.y += (_bg.height / 2) + marginY;
        }
        else if (direction == DisplayDirection.LT)
        {
            pos.x -= (_bg.width / 2) - 30;
            pos.y += (_bg.height / 2) + marginY;
        }
        else if (direction == DisplayDirection.RT)
        {
            pos.x += (_bg.width / 2) - 30;
            pos.y += (_bg.height / 2) + marginY;
        }
        else if (direction == DisplayDirection.LB)
        {
            pos.x -= (_bg.width / 2);
            pos.y -= (_bg.height / 2) + marginY;
        }
        else if (direction == DisplayDirection.RB)
        {
            pos.x += (_bg.width / 2);
            pos.y -= (_bg.height / 2) + marginY;
        }

        transform.localPosition = pos;
    }





    public void Show(int conditionId, Vector2 pos, ConditionInfoBubble.DisplayDirection direction)
    {
        _isShowed = true;
        gameObject.SetActive(true);
        transform.position = pos;

        SetConditionId(conditionId);
        Replace(direction);
    }

    public void Hide()
    {
        _isShowed = false;
        gameObject.SetActive(false);
    }
}
