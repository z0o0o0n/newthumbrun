﻿namespace Com.Mod.ThumbRun.CharacterManagement.View
{
    using UnityEngine;
    using System.Collections;
    using Application;
    using System.Collections.Generic;
    using System.Linq;
    using User.Application;
    using GameDataEditor;
    using Com.Mod.ThumbRun.UI.Popup;
    using Race.View;
    using System;

    public class CharacterManagementPopup : BasicPopup
    {
        [SerializeField]
        private GameObject _characterPrefab;
        [SerializeField]
        private GameObject _characterContainer;
        [SerializeField]
        private UIScrollView _scrollView;
        [SerializeField]
        private GameObject _buyButton;
        [SerializeField]
        private GameObject _chooseButton;
        [SerializeField]
        private UISprite _characterThumb;
        [SerializeField]
        private UISprite _characterThumbBg;
        [SerializeField]
        private UILabel _characterNameLabel;
        [SerializeField]
        private UILabel _infoLabel;
        [SerializeField]
        private UISprite _popupBg;
        [SerializeField]
        private UISprite _spot;
        [SerializeField]
        private UISprite _playerPointer;
        [SerializeField]
        private UILabel _buyButtonPriceLabel;
        [SerializeField]
        private UILabel _allowLvLabel;
        [SerializeField]
        private UILabel _ipaPriceLabel;
        [SerializeField]
        private NoEntryFeePopup _noEntryFeePopup;
        [SerializeField]
        private AudioSource _audioSource;
        [SerializeField]
        private AudioClip _characterSelectionSound;
        [SerializeField]
        private AudioClip _chooseButtonClickSound;
        [SerializeField]
        private AudioClip _buyButtonClickSound;
        [SerializeField]
        private UIAlign _buyButtonUIAlign;
        [SerializeField]
        private GameObject _disabledBuyButton;
        [SerializeField]
        private UILabel _disabledBuyButtonLvLable;
        [SerializeField]
        private GameObject _selectionMark;
        [SerializeField]
        private UILabel _selectionMarkLabel;
        [SerializeField]
        private ConditionDisplay _selectedCharacterConditionDisplay;
        [SerializeField]
        private SkillSlotDisplay _skillSlotDisplay;
        [SerializeField]
        private UIDynamicButton _conditionRecoveryButton;
        [SerializeField]
        private UserInfoDisplay _userInfoDisplay;
        private CharacterManagementService _characterManangementService;
        private UserService _userService;

        private Dictionary<int, GameObject> _characters = new Dictionary<int, GameObject>();
        private int _currentCharacterId = -1;


        void Awake()
        {
            gameObject.transform.localPosition = Vector2.zero;

            _characterManangementService = GameObject.FindGameObjectWithTag("CharacterManagement").GetComponent<CharacterManagementService>();
            _characterManangementService.Prepared += OnCMServicePrepared;
            _characterManangementService.Updated += OnCMServiceUpdated;
            _userService = GameObject.FindGameObjectWithTag("User").GetComponent<UserService>();
            _userService.Prepared += OnUserServicePrepared;
            _buyButton.GetComponent<UIDynamicButton>().Click += OnBuyButtonPressed;

            _conditionRecoveryButton.Click += OnRecoveryButtonClick;

            NewADManager.instance.Success += OnAdSuccess;
            NewADManager.instance.Failed += OnFailed;

            if (_characterManangementService.isPrepared && _userService.isPrepared) OnPrepared();
        }

        void Start()
        {

        }

        void Update()
        {
        }

        void OnDestroy()
        {
            _characterManangementService.Prepared -= OnCMServicePrepared;
            _characterManangementService.Updated -= OnCMServiceUpdated;
            _userService.Prepared -= OnUserServicePrepared;
            _buyButton.GetComponent<UIDynamicButton>().Click -= OnBuyButtonPressed;

            NewADManager.instance.Success -= OnAdSuccess;
            NewADManager.instance.Failed -= OnFailed;

            _conditionRecoveryButton.Click -= OnRecoveryButtonClick;

            _characters.Clear();
        }





        private void OnCMServicePrepared()
        {
            if (_userService.isPrepared) OnPrepared();
        }

        private void OnCMServiceUpdated(int characterId)
        {
            if(_currentCharacterId == characterId)
            {
                ShowCondition(characterId);
            }
        }

        private void OnUserServicePrepared()
        {
            if (_characterManangementService.isPrepared) OnPrepared();
        }

        private void OnPrepared()
        {
            int itemCount = _characterManangementService.GetItems().Count;
            for (int i = 0; i < itemCount; i++)
            {
                GDECharacterManagementItemData gmItem = _characterManangementService.GetItems()[i];
                int characterId = gmItem.characterId;

                GameObject instance = GameObject.Instantiate(_characterPrefab);
                instance.transform.parent = _characterContainer.transform;
                instance.transform.localScale = Vector3.one;
                instance.transform.localPosition = new Vector3(i * 100f, 0f, 0f);

                //UISprite sprite = instance.GetComponent<UISprite>();
                //sprite.depth = 1;

                UIDragScrollView udsv = instance.GetComponent<UIDragScrollView>();
                udsv.scrollView = _scrollView;

                ItemButton itemButton = instance.GetComponent<ItemButton>();
                itemButton.Pressed += OnCharacterPressed;
                itemButton.SetCharacterId(characterId, gmItem.condition);

                if (!gmItem.isUnlocked)
                {
                    itemButton.Disable();
                }

                _characters.Add(characterId, instance);
            }

            int[] keys = _characters.Keys.ToArray();
            OnCharacterPressed(keys[0]);

            int userCharacterId = _userService.GetCharacterId();
            _playerPointer.transform.parent = _characters[userCharacterId].transform;
            _playerPointer.transform.localPosition = new Vector2(0f, -70f);

            gameObject.SetActive(false);
        }

        private void OnCharacterPressed(int characterId)
        {
            SelectCharacter(characterId);
            _audioSource.clip = _characterSelectionSound;
            _audioSource.Play();
        }

        private void SelectCharacter(int characterId)
        {
            _currentCharacterId = characterId;
            ItemButton itemButton;
            GDECharacterManagementItemData gmItem = _characterManangementService.GetItemById(characterId);

            MissionManager.instance.UpdateMissionState();
            Debug.Log("gmItem: " + gmItem + " / characterId: " + characterId);
            _selectedCharacterConditionDisplay.ChangeCondition(gmItem.condition);
            _skillSlotDisplay.DisplaySkillIcons(characterId);

            if (_userService.GetLevel() < gmItem.allowLv) // 레벨 미달
            {
                // 비활성버튼 표시
                _disabledBuyButton.SetActive(true);
                _buyButton.gameObject.SetActive(false);
                _chooseButton.gameObject.SetActive(false);
                //_buyButton.GetComponent<UIDynamicButton>().Disable();
                _selectedCharacterConditionDisplay.gameObject.SetActive(false);
                //_conditionRecoveryButton.gameObject.SetActive(false);
                _selectionMark.gameObject.SetActive(false);
            }
            else
            {
                _disabledBuyButton.SetActive(false);

                if (gmItem.isUnlocked) //언락됨.
                {
                    if (characterId == _userService.GetCharacterId())
                    {
                        // 선택 표시
                        _buyButton.gameObject.SetActive(false);
                        _chooseButton.gameObject.SetActive(false);
                        _selectionMark.gameObject.SetActive(true);
                    }
                    else
                    {
                        // 선택버튼 표시
                        _buyButton.gameObject.SetActive(false);
                        _chooseButton.gameObject.SetActive(true);
                        _selectionMark.gameObject.SetActive(false);
                    }

                    ShowCondition(characterId);
                }
                else
                {
                    itemButton = _characters[characterId].GetComponent<ItemButton>();
                    if (!gmItem.isUnlocked)
                    {
                        itemButton.Disable();
                    }

                    // 구매버튼 표시
                    _buyButton.gameObject.SetActive(true);
                    _chooseButton.gameObject.SetActive(false);
                    _selectedCharacterConditionDisplay.gameObject.SetActive(false);
                    //_conditionRecoveryButton.gameObject.SetActive(false);
                    _selectionMark.gameObject.SetActive(false);
                }
            }

            CharacterData characterData = CharacterDataManager.instance.GetCharacterDataByID(characterId);
            _characterThumb.spriteName = "CharacterProfileThumb_" + characterId;
            _characterThumbBg.color = characterData.bgColor;
            _characterNameLabel.text = characterData.name;
            _infoLabel.text = gmItem.info.ToString();
            _buyButtonPriceLabel.text = String.Format("{0:##,##}", gmItem.goldPrice);
            _allowLvLabel.text = "Lv." + gmItem.allowLv.ToString();
            _disabledBuyButtonLvLable.text = "Lv." + gmItem.allowLv.ToString();
            _ipaPriceLabel.text = gmItem.ipaPrice.ToString();
            _popupBg.color = characterData.bgColor;

            _spot.transform.parent = _characters[characterId].transform;
            _spot.transform.localPosition = new Vector2(0f, 20f);
            //_spot.color = characterData.bgColor;
            _spot.depth = 0;

            _disabledBuyButtonLvLable.color = characterData.bgColor;
            _selectionMarkLabel.color = characterData.bgColor;
            _buyButtonUIAlign.Replace();

            // 변경 된 사항 확인 후 빨콩 표시 삭제
            _characterManangementService.TakeLookNewInfo(characterId);
            itemButton = _characters[characterId].GetComponent<ItemButton>();
            itemButton.HideNewIcon();

            if (_characterManangementService.HasNewInfo()) _userInfoDisplay.ShowNewIcon();
            else _userInfoDisplay.HideNewIcon();
        }

        private void OnBuyButtonPressed(GameObject target)
        {
            int result = _characterManangementService.Buy(_currentCharacterId);

            if (result == 1)
            {
                // 구매완료
                _buyButton.gameObject.SetActive(false);
                _chooseButton.gameObject.SetActive(true);

                ItemButton itemButton = _characters[_currentCharacterId].GetComponent<ItemButton>();
                itemButton.Enable();

                _audioSource.clip = _buyButtonClickSound;
                _audioSource.Play();

                _skillSlotDisplay.DisplaySkillIcons(_currentCharacterId);
            }
            else if (result == -1)
            {
                // 잔액부족
                //GameObject noGoldPopup = GameObject.Instantiate(_noGoldPopup);
                //noGoldPopup.transform.parent = transform;
                //noGoldPopup.transform.localScale = Vector2.one;
                //noGoldPopup.transform.localPosition = Vector2.zero;
                _noEntryFeePopup.OpenPopup();
            }
        }

        private void OnRecoveryButtonClick(GameObject target)
        {
            if(NewADManager.instance.IsReady())
            {
                NewADManager.instance.PlayAD("ConditionRecovery");
            }
        }

        private void OnAdSuccess(string key)
        {
            if(key == "ConditionRecovery")
            {
				List<GDECharacterManagementItemData> unlockedCharacterList = _characterManangementService.GetItems(true);

				for(int i = 0; i < unlockedCharacterList.Count; i++)
				{
					if(unlockedCharacterList[i].condition <= 3) // 별무리가 아니라면
					{
						_characterManangementService.ChangeCondition(unlockedCharacterList[i].characterId, 3);

						ItemButton characterButton = _characters[unlockedCharacterList[i].characterId].GetComponent<ItemButton>();
						characterButton.SetCharacterId(unlockedCharacterList[i].characterId, 3);
					}
				}

				_skillSlotDisplay.DisplaySkillIcons(_currentCharacterId);

				//            GDECharacterManagementItemData gmItem = _characterManangementService.GetItemById(_currentCharacterId);
				//            _characterManangementService.ChangeCondition(_currentCharacterId, gmItem.condition + 1);

				//            _skillSlotDisplay.DisplaySkillIcons(_currentCharacterId);

				//// 캐릭터의 컨디션 상태 업데이트
				//// _characters.itemButton = 캐릭터 팝업의 리스트에있는 캐릭터
				//            _characters[_currentCharacterId].GetComponent<ItemButton>().SetCharacterId(_currentCharacterId, gmItem.condition);

				//            _conditionRecoveryButton.GetComponent<ConditionRecoveryButton>().SetCondition(gmItem.condition);
			}
        }

        private void OnFailed(string key)
        {

        }





        private void UpdateInfo()
        {

        }

        private void ShowCondition(int characterId)
        {
            GDECharacterManagementItemData gmItem = _characterManangementService.GetItemById(characterId);

			_selectedCharacterConditionDisplay.ChangeCondition(gmItem.condition);
			_selectedCharacterConditionDisplay.gameObject.SetActive(true);

			//if (gmItem.condition < 3)
   //         {
   //             // 복구버튼 표시
   //             _selectedCharacterConditionDisplay.gameObject.SetActive(false);

   //             _conditionRecoveryButton.gameObject.SetActive(true);
   //             _conditionRecoveryButton.GetComponent<ConditionRecoveryButton>().SetCondition(gmItem.condition);
   //         }
   //         else
   //         {
   //             // 컨디션 상태 표시
   //             _selectedCharacterConditionDisplay.ChangeCondition(gmItem.condition);
   //             _selectedCharacterConditionDisplay.gameObject.SetActive(true);

   //             _conditionRecoveryButton.gameObject.SetActive(false);
   //         }
        }





        public void OpenPopup(int characterId = -1)
        {
            base.Open();
            
            int currentCharacterIndex = -1;
            int currentCharacterId = -1;
            if (characterId == -1)
            {
                currentCharacterIndex = _characterManangementService.GetCharacterIndex(_userService.GetCharacterId());
                currentCharacterId = _userService.GetCharacterId();
            }
            else
            {
                currentCharacterIndex = _characterManangementService.GetCharacterIndex(characterId);
                currentCharacterId = characterId;
            }

            SelectCharacter(currentCharacterId);

            if (currentCharacterIndex == -1) return;

            float movePosX = 0f;
            float maskPosX = 0f;
            if (currentCharacterIndex < 2)
            {
                movePosX = -260f;
                maskPosX = 130f;
            }
            else if(currentCharacterIndex > _characterManangementService.GetItems().Count - 3)
            {
                movePosX = -1900;
                maskPosX = 1770;
            }
            else
            {
                movePosX = -260f - ((currentCharacterIndex - 1) * 100f - 30f);
                maskPosX = 130f + ((currentCharacterIndex - 1) * 100f - 30f);
            }

            Vector3 scrollViewPos = _scrollView.transform.localPosition;
            scrollViewPos.x =  movePosX;
            _scrollView.transform.localPosition = scrollViewPos;

            UIPanel scrollViewPanel = _scrollView.GetComponent<UIPanel>();
            Vector2 clipOffset = scrollViewPanel.clipOffset;
            clipOffset.x = maskPosX;
            scrollViewPanel.clipOffset = clipOffset;

            for(int i = 0; i < _characterManangementService.GetItems().Count; i++)
            {
                GDECharacterManagementItemData cmItem = _characterManangementService.GetItems()[i];

                ItemButton itemButton = _characters[cmItem.characterId].GetComponent<ItemButton>();
                if (cmItem.hasNew) itemButton.ShowNewIcon();
                else itemButton.HideNewIcon();
            }
        }

        public void ClosePopup()
        {
            base.Close();
        }

        public void Choose()
        {
            Debug.Log("====================== Choose!!!!!!!!");
            Debug.Log("_characterManangementService: " + _characterManangementService);
            _characterManangementService.Choose(_currentCharacterId);
            _playerPointer.transform.parent = _characters[_currentCharacterId].transform;
            _playerPointer.transform.localPosition = new Vector2(0f, -70f);

            _audioSource.clip = _chooseButtonClickSound;
            _audioSource.Play();

            SelectCharacter(_currentCharacterId);
        }
    }
}