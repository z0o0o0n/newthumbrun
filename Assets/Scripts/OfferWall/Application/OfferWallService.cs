﻿using System.Collections;
using System.Collections.Generic;
using Com.Mod.ThumbRun.User.Application;
using UnityEngine;

public class OfferWallService : MonoBehaviour 
{
	public delegate void OfferWallServiceEventHandler();
	public event OfferWallServiceEventHandler Prepared;
	public event OfferWallServiceEventHandler OfferWallLoadStart;
	public event OfferWallServiceEventHandler OfferWallLoadCompleted;
	public event OfferWallServiceEventHandler Closed;
    public event OfferWallServiceEventHandler Error;

    [SerializeField]
	private OfferWall _offerWall;
	[SerializeField]
	private UserService _userService;
	private bool _isPrepared = false;

	void Awake ()
	{
		_offerWall.Prepared += OnPrepared;
		_offerWall.OfferWallLoadStart += OnOfferWallStart;
		_offerWall.OfferWallLoadCompleted += OnOfferWallCompleted;
		_offerWall.EarnedGold += OnEarnedGold;
		_offerWall.Closed += OnClosed;
        _offerWall.Error += OnError;


        _userService.Prepared += OnPrepared;
	}

	void Start () 
	{
		
	}
	
	void Update () 
	{
		
	}

	void OnDestroy()
	{
		_offerWall.Prepared -= OnPrepared;
		_offerWall.OfferWallLoadStart -= OnOfferWallStart;
		_offerWall.OfferWallLoadCompleted -= OnOfferWallCompleted;
		_offerWall.EarnedGold -= OnEarnedGold;
		_offerWall.Closed -= OnClosed;
        _offerWall.Error -= OnError;

        _userService.Prepared -= OnPrepared;
	}




	private void OnPrepared()
	{
		if(!_offerWall.isPrepared) return;
		if(!_userService.isPrepared) return;

		_isPrepared = true;

		_offerWall.UpdateGold();
		
		if(Prepared != null) Prepared();
	}

	private void OnOfferWallStart()
	{
		if(OfferWallLoadStart != null) OfferWallLoadStart();
	}

	private void OnOfferWallCompleted()
	{
		if(OfferWallLoadCompleted != null) OfferWallLoadCompleted();
	}

	private void OnEarnedGold(int goldAmount)
	{
		_userService.Save(goldAmount);
	}

	private void OnClosed()
	{
		if(Closed != null) Closed();
	}

    private void OnError()
    {
        if (Error != null) Error();
    }





    public void OpenOfferWall()
	{
		_offerWall.OpenOfferWall();
	}
}
