﻿using System.Collections;
using TapjoyUnity;
using UnityEngine;

public class OfferWall : MonoBehaviour 
{
	public delegate void OfferWallEventHandler();
	public delegate void OfferWallEarnedGoldEventHandler(int goldAmount);
	public event OfferWallEventHandler Prepared;
	public event OfferWallEventHandler OfferWallLoadStart;
	public event OfferWallEventHandler OfferWallLoadCompleted;
	public event OfferWallEventHandler Closed;
	public event OfferWallEarnedGoldEventHandler EarnedGold;
    public event OfferWallEventHandler Error;

    private TJPlacement _placement;
	private bool _isPrepared = false;

	public bool isPrepared
	{
		get{ return _isPrepared; }
	}




	void Start () 
	{
		TJPlacement.OnRequestSuccess += OnRequestSuccess;
		TJPlacement.OnRequestFailure += OnRequestFailure;
		TJPlacement.OnContentReady += OnContentReady;
		TJPlacement.OnContentShow += OnContentShow;
		TJPlacement.OnContentDismiss += OnContentDismiss;
		TJPlacement.OnRewardRequest += OnRewardRequest;

		TJPlacement.OnVideoStart += HandleVideoStart;
		TJPlacement.OnVideoComplete += HandleVideoComplete;

        Tapjoy.OnEarnedCurrency += HandleEarnedCurrency;
		Tapjoy.OnAwardCurrencyResponse += HandleAwardCurrencyResponse;
    	Tapjoy.OnAwardCurrencyResponseFailure += HandleAwardCurrencyResponseFailure;

		_placement = TJPlacement.CreatePlacement("OfferWall");

        StartCoroutine(CheckTapjoyConnection());
	}
	
	void Update () 
	{
		
	}

	void OnDestroy()
	{
		TJPlacement.OnRequestSuccess -= OnRequestSuccess;
		TJPlacement.OnRequestFailure -= OnRequestFailure;
		TJPlacement.OnContentReady -= OnContentReady;
		TJPlacement.OnContentShow -= OnContentShow;
		TJPlacement.OnContentDismiss -= OnContentDismiss;
		TJPlacement.OnRewardRequest -= OnRewardRequest;

		TJPlacement.OnVideoStart -= HandleVideoStart;
		TJPlacement.OnVideoComplete -= HandleVideoComplete;

        Tapjoy.OnEarnedCurrency -= HandleEarnedCurrency;
		Tapjoy.OnAwardCurrencyResponse -= HandleAwardCurrencyResponse;
    	Tapjoy.OnAwardCurrencyResponseFailure -= HandleAwardCurrencyResponseFailure;
    }




	IEnumerator CheckTapjoyConnection()
	{
		Debug.Log("Connecting to Tapjoy...");
		while(!Tapjoy.IsConnected)
		{
			yield return null;
		}

		Debug.Log("Tapjoy is connected");

		//_placement.RequestContent();
		//Tapjoy.GetCurrencyBalance();

		_isPrepared = true;
		if(Prepared != null) Prepared();
	}

	private void OnRequestSuccess(TJPlacement placement)
	{
		Debug.Log("OnRequestSuccess / name: " + placement.GetName() + " / IsContentAvailable: " + placement.IsContentAvailable());
		if (placement.IsContentAvailable()) {
			if(placement.GetName() == "OfferWall") 
			{
				placement.ShowContent();
			}

		} else {
			Debug.Log("No content available for " + placement.GetName());
		}
	}

	private void OnRequestFailure(TJPlacement placement, string error)
	{
		Debug.Log("OnRequestFailure / name: " + placement.GetName() + " / error: " + error);
        if (Error != null) Error();
    }

	private void OnContentReady(TJPlacement placement)
	{
		Debug.Log("OnContentReady / name: " + placement.GetName());
		if (placement.IsContentAvailable()) {
			if(placement.GetName() == "OfferWall") 
			{
				placement.ShowContent();
			}

		} else {
			Debug.Log("No content available for " + placement.GetName());
		}
	}

	private void OnContentShow(TJPlacement placement)
	{
		Debug.Log("OnContentShow / name: " + placement.GetName());
	}

	private void OnContentDismiss(TJPlacement placement)
	{
		Tapjoy.GetCurrencyBalance();
		if(Closed != null) Closed();
		Debug.Log("OnContentDismiss / name: " + placement.GetName());
	}

    private void OnRewardRequest(TJPlacement placement, TJActionRequest request, string itemId, int quantity)
    {
        Debug.Log("On Offer Wall reward request \n requestID: " + request.requestID + " \n itemId: " + itemId + " \n quantity: " + quantity);
		request.Completed();
    }

	private void HandleVideoStart(TJPlacement placement)
	{
		Debug.Log("C#: HandleVideoStarted for placement " + placement.GetName());
	}

	private void HandleVideoComplete(TJPlacement placement)
	{
		Debug.Log("C#: HandleVideoComplete for placement " + placement.GetName());
	}

    private void HandleEarnedCurrency(string currencyName, int amount)
    {
		Tapjoy.ShowDefaultEarnedCurrencyAlert();
		if(EarnedGold != null) EarnedGold(amount);
        Debug.Log("On handle earned currency \n currencyName: " + currencyName + " \n amount: " + amount);
    }

	private void HandleAwardCurrencyResponse(string currencyName, int balance) 
	{
    	Debug.Log("C#: HandleAwardCurrencySucceeded: currencyName: " + currencyName + ", balance: " + balance);
	}

	private void HandleAwardCurrencyResponseFailure(string error) 
	{
		Debug.Log("C#: HandleAwardCurrencyResponseFailure: " + error);
	}




	public void OpenOfferWall()
	{
		Debug.Log("Open Offer Wall");
		Debug.Log("Offer Wall is ready: " + _placement.IsContentReady());

		if(OfferWallLoadStart != null) OfferWallLoadStart();

		if(_placement.IsContentReady())
		{
			Debug.Log("Show Offer Wall");
			_placement.ShowContent();
		}
		else
		{
			Debug.Log("Request Offer Wall");
			_placement.RequestContent();
		}
	}

	public void UpdateGold()
	{
		Tapjoy.GetCurrencyBalance();
    }
}
