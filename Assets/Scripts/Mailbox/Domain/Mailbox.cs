﻿namespace Com.Mod.ThumbRun.Mailbox.Domain
{
    using GameDataEditor;
    using Infrastructure;
    using LitJson;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class Mailbox : MonoBehaviour
    {
        public delegate void MailboxEventHandler();
        public delegate void MailboxInitEvent(int itemCount);
        public delegate void MailboxItemEventHandler(List<GoldItem> goldItems);
        public event MailboxEventHandler Prepared;
        public event MailboxInitEvent Inited;
        public event MailboxEventHandler InitFail;
        public event MailboxItemEventHandler ItemReadSuccess;
        public event MailboxEventHandler ItemReadFail;
        public event MailboxEventHandler ItemReceiveSuccess;
        public event MailboxEventHandler ItemReceiveFail;


        [SerializeField]
        private MailboxServerManager _mailBoxServerManager;
        private GDEMailboxData _rawData;
        private bool _isPrepared = false;
        private bool _isDebug = false;

        private void Awake()
        {
            _mailBoxServerManager.InitSuccess += OnMailboxInited;
            _mailBoxServerManager.InitFail += OnMailboxInitFail;
            _mailBoxServerManager.ItemReadSuccess += OnItemReadSuccess;
            _mailBoxServerManager.ItemReadFail += OnItemReadFail;
            _mailBoxServerManager.ItemGiveSuccess += OnItemGiveSuccess;
            _mailBoxServerManager.ItemGiveFail += OnItemGiveFail;
            _mailBoxServerManager.ItemReceiveSuccess += OnItemReceiveSuccess;
            _mailBoxServerManager.ItemReceiveFail += OnItemReceiveFail;
        }

        void Start()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Mailbox_Mailbox, out _rawData))
            {
                Debug.Log("Mailbox Data Prepared");
                _isPrepared = true;
                Init();
                //if (!_rawData.isInit) Init();
                if (Prepared != null) Prepared();
                //_rawData.ResetAll(); //지울것
            }
            else
            {
                Debug.LogError("Read Error Mailbox Data");
            }
        }

        void Update()
        {
            if(Input.GetKeyUp(KeyCode.J))
            {
                //GiveItem();
            }
        }

        private void OnDestroy()
        {
            _mailBoxServerManager.InitSuccess -= OnMailboxInited;
            _mailBoxServerManager.InitFail -= OnMailboxInitFail;
            _mailBoxServerManager.ItemReadSuccess -= OnItemReadSuccess;
            _mailBoxServerManager.ItemReadFail -= OnItemReadFail;
            _mailBoxServerManager.ItemGiveSuccess -= OnItemGiveSuccess;
            _mailBoxServerManager.ItemGiveFail -= OnItemGiveFail;
            _mailBoxServerManager.ItemReceiveSuccess -= OnItemReceiveSuccess;
            _mailBoxServerManager.ItemReceiveFail -= OnItemReceiveFail;
        }




        private void OnMailboxInited(string wwwText)
        {
            _rawData.isInit = true;
            JsonData json = JsonMapper.ToObject(wwwText);
            if(_isDebug) Debug.Log("Mail box init / wwwText: " + wwwText);
            if (_isDebug) Debug.Log("Mail box init / mailbox id: " + json["postbox_id"].ToString());
            _rawData.mailboxId = json["postbox_id"].ToString();
            int itemCount = int.Parse(json["postbox_item_count"].ToString());
            if (Inited != null) Inited(itemCount);
        }

        private void OnMailboxInitFail(string wwwText)
        {
            if (InitFail != null) InitFail();
        }

        private void OnItemReadSuccess(string wwwText)
        {
            if (_isDebug) Debug.Log("OnItemReadSuccess / wwwText: " + wwwText);
            JsonData json = JsonMapper.ToObject(wwwText);
            //Debug.Log("-----> json.Count: " + json.Count);

            List<GoldItem> goldItems = new List<GoldItem>();
            for(int i = 0; i < json.Count; i++)
            {
                GoldItem goldItem = new GoldItem(MailboxItemType.type.Gold, json[i]["item_id"].ToString(), int.Parse(json[i]["item_count"].ToString()) * 100, int.Parse(json[i]["expired_sec"].ToString()));
                goldItems.Add(goldItem);
            }

            if (ItemReadSuccess != null) ItemReadSuccess(goldItems);
        }

        private void OnItemReadFail(string wwwText)
        {
            if (ItemReadFail != null) ItemReadFail();
        }

        private void OnItemGiveSuccess(string wwwText)
        {
            if (_isDebug) Debug.Log("-----> OnItemGiveSuccess / wwwText: " + wwwText);
        }

        private void OnItemGiveFail(string wwwText)
        {

        }

        private void OnItemReceiveSuccess(string wwwText)
        {
            Debug.Log("-----> OnItemReceiveSuccess / wwwText: " + wwwText);
            JsonData json = JsonMapper.ToObject(wwwText);
            if(json["result"].ToString() == "success")
            {
                if (ItemReceiveSuccess != null) ItemReceiveSuccess();
            }
            else
            {
                if (ItemReceiveFail != null) ItemReceiveFail();
            }
        }

        private void OnItemReceiveFail(string wwwText)
        {
            if (ItemReceiveFail != null) ItemReceiveFail();
        }





        private void Init()
        {
            _mailBoxServerManager.InitMailbox();
        }

        public void ReadItems()
        {
            if(_rawData.isInit)
            {
                _mailBoxServerManager.ReadItems(_rawData.mailboxId);
            }
        }

        public void ReceiveItem(string itemId)
        {
            if (_rawData.isInit)
            {
                _mailBoxServerManager.ReceiveItem(_rawData.mailboxId, itemId);
            }
        }

        public string GetMailboxAddress()
        {
            return _rawData.mailboxId;
        }




        public bool IsInit()
        {
            return _rawData.isInit;
        }
    }
}