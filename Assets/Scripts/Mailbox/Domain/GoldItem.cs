﻿namespace Com.Mod.ThumbRun.Mailbox.Domain
{
    using Game.LanguageManager;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class GoldItem : MonoBehaviour
    {
        private MailboxItemType.type _mailboxItemType;
        private string _itemId;
        private int _amount;
        private int _expiredSec;
        private string _expiredString;

        public MailboxItemType.type mailboxItemType
        {
            get { return _mailboxItemType; }
        }

        public string itemId
        {
            get { return _itemId; }
        }

        public int amount
        {
            get { return _amount; }
        }

        public int expiredSec
        {
            get { return _expiredSec; }
        }

        public string expiredString
        {
            get { return _expiredString; }
        }




        public GoldItem(MailboxItemType.type mailboxItemType, string itemId, int amount, int expiredSec)
        {
            _mailboxItemType = mailboxItemType;
            _itemId = itemId;
            _amount = amount;
            _expiredSec = expiredSec;
            Debug.Log("@@@@@@@@@@@@@@@@@@@ expired sec: " + expiredSec);

            if (expiredSec == -1) _expiredString = "-1";
            else
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(expiredSec);
                if (timeSpan.Days == 0)
                {
                    if (timeSpan.Hours == 0)
                    {
                        _expiredString = string.Format("{0:D}m", timeSpan.Minutes);
                        return;
                    }
                    _expiredString = string.Format("{0:D}h {1:D}m", timeSpan.Hours, timeSpan.Minutes);
                    return;
                }
                _expiredString = string.Format("{0:D}d {1:D}h {2:D}m", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes);
            }
        }
    }
}