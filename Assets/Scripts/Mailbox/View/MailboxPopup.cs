﻿namespace Com.Mod.ThumbRun.Mailbox.View
{
    using Application;
    using Domain;
    using Game.LanguageManager;
    using System.Collections;
    using System.Collections.Generic;
    using UI.Popup;
    using UnityEngine;
    using User.Application;

    public class MailboxPopup : BasicPopup
    {
        [SerializeField]
        private GameObject _mailboxItemPrefab;
        [SerializeField]
        private UIScrollView _mailListScrollView;
        [SerializeField]
        private UIGrid _mailListGrid;
        [SerializeField]
        private MailboxService _mailboxService;
        [SerializeField]
        private MailboxResultBox _mailboxResultBox;
        [SerializeField]
        private GameObject _loadingPanel;
        [SerializeField]
        private UILabel _emptyLabel;
        [SerializeField]
        private UserService _userServie;
        [SerializeField]
        private UIDynamicButton _mailboxButton;
        private List<MailboxItem> _mailboxItemList = new List<MailboxItem>();
        private string _currentReceiveItemId = "";

        private void Awake()
        {
            ClosePopup();

            _mailboxService.Inited += OnMailboxInited;
            _mailboxService.ItemReadSuccess += OnItemReadSuccess;
            _mailboxService.ItemReadFail += OnFail;
            _mailboxService.ItemReceiveSuccess += OnItemReceiveSuccess;
            _mailboxService.ItemReceiveFail += OnItemReceiveFail;
        }

        void Start()
        {
            transform.localPosition = Vector2.zero;
            _emptyLabel.gameObject.SetActive(false);
            _mailboxResultBox.Hide();
        }

        void Update()
        {

        }

        private void OnDisable()
        {
            
        }

        private void OnDestroy()
        {
            _mailboxService.Inited -= OnMailboxInited;
            _mailboxService.ItemReadSuccess -= OnItemReadSuccess;
            _mailboxService.ItemReadFail -= OnFail;
            _mailboxService.ItemReceiveSuccess -= OnItemReceiveSuccess;
            _mailboxService.ItemReceiveFail -= OnItemReceiveFail;
        }





        private void OnMailboxInited(int itemCount)
        {
            //Debug.Log("-----> itemCount: " + itemCount);
            if(itemCount > 0) _mailboxButton.ShowNewIcon();
        }

        public void OnCopyButtonClick()
        {
            Debug.Log("MailboxPopup - OnCopyButtonClick / mailboxAdd: " + _mailboxService.GetMailboxAddress());
            _mailboxResultBox.Show(MailboxResultType.type.COPY_SUCCESS);
            UniPasteBoard.SetClipBoardString(_mailboxService.GetMailboxAddress());
        }

        private void OnItemReadSuccess(List<GoldItem> goldItems)
        {
            _loadingPanel.gameObject.SetActive(false);

            if(goldItems.Count == 0)
            {
                _emptyLabel.gameObject.SetActive(true);
            }
            else
            {
                _emptyLabel.gameObject.SetActive(false);
                _mailboxItemList = new List<MailboxItem>();
                for (int i = 0; i < goldItems.Count; i++)
                {
                    GameObject instance = Instantiate(_mailboxItemPrefab);
                    instance.transform.parent = _mailListGrid.transform;
                    instance.transform.localScale = Vector3.one;
                    instance.transform.localPosition = new Vector3(0f, -90f * i, 0f);

                    string expiredString;
                    if (goldItems[i].expiredString == "-1") expiredString = LanguageManager.instance.GetValue("NoExpirationDate");
                    else expiredString = goldItems[i].expiredString;

                    MailboxItem mailboxItme = instance.GetComponent<MailboxItem>();
                    mailboxItme.SetData(i, goldItems[i].itemId, goldItems[i].amount, expiredString);
                    Debug.Log("Expired String: " + goldItems[i].expiredString);
                    mailboxItme.GetButtonClick += OnGetButtonClick;
                    _mailboxItemList.Add(mailboxItme);

                    UIDragScrollView dragScrollView = instance.GetComponent<UIDragScrollView>();
                    dragScrollView.scrollView = _mailListScrollView;
                }
            }
        }

        private void OnFail()
        {
            _mailboxResultBox.Show(MailboxResultType.type.CONNECTION_FAIL);
        }

        private void OnGetButtonClick(string itemId)
        {
            _loadingPanel.gameObject.SetActive(true);

            _currentReceiveItemId = itemId;
            _mailboxService.ReceiveItem(itemId);
        }

        private void OnItemReceiveSuccess()
        {
            _loadingPanel.gameObject.SetActive(false);
            for(int i = 0; i < _mailboxItemList.Count; i++)
            {
                if(_mailboxItemList[i].itemId == _currentReceiveItemId)
                {
                    _userServie.Save(_mailboxItemList[i].amount);

                    _mailboxItemList[i].GetButtonClick -= OnGetButtonClick;
                    Destroy(_mailboxItemList[i].gameObject);
                    _mailboxItemList.RemoveAt(i);
                }
            }

            for(int i = 0; i < _mailboxItemList.Count; i++)
            {
                _mailboxItemList[i].transform.localPosition = new Vector3(0f, -90f * i, 0f);
            }

            if(_mailboxItemList.Count == 0) _emptyLabel.gameObject.SetActive(true);
        }

        private void OnItemReceiveFail()
        {
            _currentReceiveItemId = "";
            _loadingPanel.gameObject.SetActive(false);
            // 서버 연결 오류. 문구 출력.
        }





        public void OpenPopup()
        {
            base.Open();

            gameObject.SetActive(true);

            _mailboxResultBox.Hide();

            if (!_mailboxService.IsInit())
            {
                _mailboxResultBox.Show(MailboxResultType.type.CONNECTION_FAIL);
            }
            else
            {
                ReadItems();
            }
        }

        public void ClosePopup()
        {
            base.Close();

            gameObject.SetActive(false);
            _emptyLabel.gameObject.SetActive(false);

            for (int i = 0; i < _mailboxItemList.Count; i++)
            {
                _mailboxItemList[i].GetButtonClick -= OnGetButtonClick;
                Destroy(_mailboxItemList[i].gameObject);
            }
            _mailboxItemList.Clear();
        }




        private void ReadItems()
        {
            _loadingPanel.gameObject.SetActive(true);
            _mailboxService.ReadItems();
        }
    }
}