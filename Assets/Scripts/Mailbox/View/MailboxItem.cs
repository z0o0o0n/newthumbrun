﻿namespace Com.Mod.ThumbRun.Mailbox.View
{
    using DG.Tweening;
    using Game.LanguageManager;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class MailboxItem : MonoBehaviour
    {
        public delegate void MailboxItemEvent(string itemId);
        public event MailboxItemEvent GetButtonClick;

        [SerializeField]
        private UILabel _nameLabel;
        [SerializeField]
        private UILabel _valueLabel;
        [SerializeField]
        private UILabel _limitTimeLabel;
        [SerializeField]
        private UIDynamicButton _getButton;
        [SerializeField]
        private UISprite _bg;
        private string _itemId;
        private int _amount;

        public string itemId
        {
            get { return _itemId; }
        }

        public int amount
        {
            get { return _amount; }
        }



        void Start()
        {

        }

        void Update()
        {

        }




        public void OnGetButtonClick()
        {
            //Debug.Log("------> id: " + _itemId);
            if(GetButtonClick != null) GetButtonClick(_itemId);
        }




        public void SetData(int index, string itemId, int amount, string expiredString)
        {
            _nameLabel.text = LanguageManager.instance.GetValue("Gold");
            _itemId = itemId;
            _amount = amount;
            if (index % 2 == 1) _bg.gameObject.SetActive(false);
            _valueLabel.text = amount.ToString();
            _valueLabel.transform.DOLocalMoveX(_nameLabel.transform.localPosition.x + _nameLabel.width + 5f, 0f);
            _limitTimeLabel.text = expiredString;
        }
    }
}