﻿namespace Com.Mod.ThumbRun.Mailbox.View
{
    using System.Collections;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.Coupon.Domain;
    using UnityEngine;
    using DG.Tweening;
    using Domain;

    public class MailboxResultBox : MonoBehaviour 
	{
		[SerializeField]
		private UILabel _copyCompleteLabel;
        [SerializeField]
        private UILabel _connectionFailLabel;
        [SerializeField]
		private UISprite _bg;
		[SerializeField]
		private Color _successBgColor;
		[SerializeField]
		private Color _failBgColor;


		void Start () 
		{
			Hide();
		}
		
		void Update () 
		{
			// if(Input.GetKeyUp(KeyCode.Q))
			// {
			// 	Show(CouponResultBox.result.SUCCESS);
			// }
			// if(Input.GetKeyUp(KeyCode.W))
			// {
			// 	Show(CouponResultBox.result.FAIL);
			// }
			// if(Input.GetKeyUp(KeyCode.E))
			// {
			// 	Hide();
			// }
		}




		public void Show(MailboxResultType.type resultType)
		{
			HideAll();

			_bg.color = _failBgColor;
			_bg.gameObject.SetActive(true);

            if(resultType == MailboxResultType.type.COPY_SUCCESS)
            {
                _copyCompleteLabel.gameObject.SetActive(true);
                _bg.color = _successBgColor;
            }
            else if(resultType == MailboxResultType.type.CONNECTION_FAIL)
            {
                _connectionFailLabel.gameObject.SetActive(true);
                _bg.color = _failBgColor;
            }

            DOTween.Kill("MailboxAutoHide." + GetInstanceID());
            DOVirtual.DelayedCall(5f, Hide).SetId("MailboxAutoHide." + GetInstanceID());
		}

		public void Hide()
		{
            DOTween.Kill("MailboxAutoHide." + GetInstanceID());
            HideAll();
            _copyCompleteLabel.gameObject.SetActive(false);
            _bg.gameObject.SetActive(false);
		}




        private void HideAll()
        {
            _copyCompleteLabel.gameObject.SetActive(false);
            _connectionFailLabel.gameObject.SetActive(false);
        }
    }
}