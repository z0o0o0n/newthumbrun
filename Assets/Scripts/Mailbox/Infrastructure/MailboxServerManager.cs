﻿using System;
namespace Com.Mod.ThumbRun.Mailbox.Infrastructure
{
    using System.Collections;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.User.Application;
    using UnityEngine;
    using Util;
    using App;

    public class MailboxServerManager : MonoBehaviour
    {
        public delegate void MailboxServerEventHandler(string wwwText);
        public event MailboxServerEventHandler InitSuccess;
        public event MailboxServerEventHandler InitFail;
        public event MailboxServerEventHandler ItemReadSuccess;
        public event MailboxServerEventHandler ItemReadFail;
        public event MailboxServerEventHandler ItemGiveSuccess;
        public event MailboxServerEventHandler ItemGiveFail;
        public event MailboxServerEventHandler ItemReceiveSuccess;
        public event MailboxServerEventHandler ItemReceiveFail;

        [SerializeField]
        private string _host;
        [SerializeField]
        private string _mailboxInitRequestURI; //사용자의 우편함 정보 세팅 및 이벤트 여부에 따른 아이템 지급이 처리됩니다. 
        [SerializeField]
        private string _mailboxItemRequestURI; //사용자의 우편함 아이템 정보를 조회한다. 
        [SerializeField]
        private string _mailboxReceiveRequestURI; //사용자의 우편함 아이템을 사용처리 한다.
        [SerializeField]
        private string _mailboxGiveRequestURI; //사용자의 우편함 아이템을 지급 한다.
        [SerializeField]
        private string _mailboxSaveRequestURI; //구독 상품 구매에 따른 아이템 지급 요청을 기록합니다
        [SerializeField]
        private string _clientID;
        [SerializeField]
        private string _secretKey;
        private HttpRequest _httpRequest;
        private bool _isDebug = false;

        private void Awake()
        {
            _httpRequest = gameObject.AddComponent(typeof(HttpRequest)) as HttpRequest;
            _httpRequest.Complete += OnCompleted;
            _httpRequest.TimeOut += OnError;
            _httpRequest.Error += OnError;
        }

        void Start()
        {

        }

        void Update()
        {
        }

        private void OnDestroy()
        {
            _httpRequest.Complete -= OnCompleted;
            _httpRequest.TimeOut -= OnError;
            _httpRequest.Error -= OnError;
        }




        private void OnCompleted(string id, string wwwText)
        {
            if (id == "InitMailbox")
            {
                if(_isDebug) Debug.Log("-----> InitMailbox success: " + wwwText);
                if (InitSuccess != null) InitSuccess(wwwText);
            }
            else if(id == "ReadItems")
            {
                if (_isDebug) Debug.Log("-----> ReadItems success: " + wwwText);
                if (ItemReadSuccess != null) ItemReadSuccess(wwwText);
            }
            else if(id == "GiveItem")
            {
                if (_isDebug) Debug.Log("-----> GiveItem success: " + wwwText);
                if (ItemGiveSuccess != null) ItemGiveSuccess(wwwText);
            }
            else if(id == "ReceiveItem")
            {
                if (_isDebug) Debug.Log("-----> ReceiveItem success: " + wwwText);
                if (ItemReceiveSuccess != null) ItemReceiveSuccess(wwwText);
            }
        }

        private void OnError(string id, string wwwText)
        {
            if (id == "InitMailbox")
            {
                if (_isDebug) Debug.Log("-----> InitMailbox error: " + wwwText);
                if (InitFail != null) InitFail(wwwText);
            }
            else if (id == "ReadItems")
            {
                if (_isDebug) Debug.Log("-----> ReadItems error: " + wwwText);
                if (ItemReadFail != null) ItemReadFail(wwwText);
            }
            else if (id == "GiveItem")
            {
                if (_isDebug) Debug.Log("-----> GiveItem success: " + wwwText);
                if (ItemGiveFail != null) ItemGiveFail(wwwText);
            }
            else if (id == "ReceiveItem")
            {
                if (_isDebug) Debug.Log("-----> ReceiveItem success: " + wwwText);
                if (ItemReceiveFail != null) ItemReceiveFail(wwwText);
            }
        }




        public void InitMailbox()
        {
            string platform = Platform.GetPlatformType().ToString();
            string uuid = SystemInfo.deviceUniqueIdentifier;
            string call = Random.Range(1000, 9999).ToString();
            string hash = TokenManager.CreateToken(_clientID + uuid + call, _secretKey);
            Dictionary<string, string> stringArgs = new Dictionary<string, string>();
            stringArgs.Add("client_id", _clientID);
            stringArgs.Add("uuid", uuid);
            stringArgs.Add("platform", platform);
            stringArgs.Add("version", Application.version);
            stringArgs.Add("call", call);
            stringArgs.Add("hash", TokenManager.CreateToken(_clientID + uuid + call, _secretKey));

            string uri = _host + _mailboxInitRequestURI;
            _httpRequest.Post("InitMailbox", uri, stringArgs);
        }

        public void ReadItems(string postboxId)
        {
            string call = Random.Range(1000, 9999).ToString();
            string hash = TokenManager.CreateToken(_clientID + postboxId + call, _secretKey);
            Dictionary<string, string> stringArgs = new Dictionary<string, string>();
            stringArgs.Add("client_id", _clientID);
            stringArgs.Add("postbox_id", postboxId);
            stringArgs.Add("call", call);
            stringArgs.Add("hash", TokenManager.CreateToken(_clientID + postboxId + call, _secretKey));

            string uri = _host + _mailboxItemRequestURI;
            _httpRequest.Post("ReadItems", uri, stringArgs);
        }

        public void ReceiveItem(string postboxId, string itemId)
        {
            string uuid = SystemInfo.deviceUniqueIdentifier;
            string call = Random.Range(1000, 9999).ToString();
            Dictionary<string, string> stringArgs = new Dictionary<string, string>();
            stringArgs.Add("client_id", _clientID);
            stringArgs.Add("postbox_id", postboxId);
            stringArgs.Add("item_id", itemId);
            stringArgs.Add("uuid", uuid);
            stringArgs.Add("call", call);
            stringArgs.Add("hash", TokenManager.CreateToken(_clientID + postboxId + itemId + call, _secretKey));

            string uri = _host + _mailboxReceiveRequestURI;
            _httpRequest.Post("ReceiveItem", uri, stringArgs);
        }
        
        //public void GiveItem(string postboxId, string itemId, string messageCode, int count, bool isLimit = false, string expiredDay = "")
        //{
        //    string expiredYN = (isLimit) ? "Y" : "N";
        //    string uuid = SystemInfo.deviceUniqueIdentifier;
        //    string call = Random.Range(1000, 9999).ToString();
        //    Dictionary<string, string> stringArgs = new Dictionary<string, string>();
        //    stringArgs.Add("client_id", _clientID);
        //    stringArgs.Add("postbox_id", postboxId);
        //    stringArgs.Add("message_code", messageCode);
        //    stringArgs.Add("item_code", itemId);
        //    stringArgs.Add("item_count", count.ToString());
        //    stringArgs.Add("expired_yn", expiredYN);
        //    stringArgs.Add("expired_add_day", expiredDay);
        //    stringArgs.Add("operator", "김준희(ClientSystem)");
        //    stringArgs.Add("etc", "");
        //    stringArgs.Add("call", call);
        //    stringArgs.Add("hash", TokenManager.CreateToken(_clientID + postboxId + call, _secretKey));

        //    string uri = _host + _mailboxGiveRequestURI;
        //    _httpRequest.Post("GiveItem", uri, stringArgs);
        //}

        //public void SaveItem()
        //{

        //}




        public string GetClientId()
        {
            return _clientID;
        }

        public string GetSecretKey()
        {
            return _secretKey;
        }
    }
}