﻿namespace Com.Mod.ThumbRun.Mailbox.Application
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Domain;

    public class MailboxService : MonoBehaviour
    {
        public delegate void MailboxServiceEvent();
        public delegate void MailboxInitEvent(int itemCount);
        public delegate void MailboxServiceItemEvent(List<GoldItem> goldItems);
        public event MailboxInitEvent Inited;
        public event MailboxServiceItemEvent ItemReadSuccess;
        public event MailboxServiceEvent ItemReadFail;
        public event MailboxServiceEvent ItemReceiveSuccess;
        public event MailboxServiceEvent ItemReceiveFail;

        [SerializeField]
        private Mailbox _mailbox;

        private void Awake()
        {
            _mailbox.Inited += OnInited;
            _mailbox.ItemReadSuccess += OnItemReadSuccess;
            _mailbox.ItemReadFail += OnItemReadFail;
            _mailbox.ItemReceiveSuccess += OnItemReceiveSuccess;
            _mailbox.ItemReceiveFail += OnItemReceiveFail;
        }

        void Start()
        {

        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _mailbox.Inited -= OnInited;
            _mailbox.ItemReadSuccess -= OnItemReadSuccess;
            _mailbox.ItemReadFail -= OnItemReadFail;
            _mailbox.ItemReceiveSuccess -= OnItemReceiveSuccess;
            _mailbox.ItemReceiveFail -= OnItemReceiveFail;
        }





        private void OnInited(int itemCount)
        {
            //Debug.Log("-----> itemCount: " + itemCount);
            if (Inited != null) Inited(itemCount);
        }

        private void OnItemReadSuccess(List<GoldItem> goldItems)
        {
            if (ItemReadSuccess != null) ItemReadSuccess(goldItems);
        }

        private void OnItemReadFail()
        {
            if (ItemReadFail != null) ItemReadFail();
        }

        private void OnItemReceiveSuccess()
        {
            if (ItemReceiveSuccess != null) ItemReceiveSuccess();
        }

        private void OnItemReceiveFail()
        {
            if (ItemReceiveFail != null) ItemReceiveFail();
        }





        public bool IsInit()
        {
            return _mailbox.IsInit();
        }

        public string GetMailboxAddress()
        {
            return _mailbox.GetMailboxAddress();
        }

        public void ReadItems()
        {
            _mailbox.ReadItems();
        }

        public void ReceiveItem(string itemId)
        {
            _mailbox.ReceiveItem(itemId);
        }
    }
}