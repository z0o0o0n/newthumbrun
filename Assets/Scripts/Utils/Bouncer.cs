﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;
using DG.Tweening;

public class Bouncer : MonoBehaviour 
{
    public AnimationCurve bounceAnimCurve;

    private Timer _timer;
    private GameObject _target;

	void Start () 
    {
	
	}
	
	void Update () 
    {
        if(_timer != null) _timer.Update();
	}





    public void Start(GameObject target, float delay, int repeatCount)
    {
        _target = target;
        if (_timer == null)
        {
            _timer = new Timer(delay, repeatCount);
            _timer.Tick += OnTick;
        }
        _timer.Start();
        OnTick();
    }

    private void OnTick()
    {
        float time = 0.2f;
        _target.transform.DOLocalMoveY(0.5f, time).SetEase(Ease.OutCubic);
        _target.transform.DOLocalMoveY(0.24f, 0.5f).SetEase(bounceAnimCurve).SetDelay(time);
        transform.DOShakeRotation(1f, new Vector3(10, 0, 0), 10, 0);
    }



    public void Stop()
    {
        _timer.Stop();
    }
}
