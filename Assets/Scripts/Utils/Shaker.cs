﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;
using DG.Tweening;

public class Shaker : MonoBehaviour
{
    public delegate void CompleteDele();

    public float changeTime = 0.0001f;
    public float time = 3;
    public float currentTime = 0;
    public float arrow = 0;
    public float moveDistance = 0.1f;
    public float currentDistance = 0;
    public AnimationCurve animCurve;

    private Timer _timer;
    private CompleteDele _onCompleteCallback;
    private bool _isComplete = true;
    private Vector3 _startPoint;
	
	void Start () 
    {
	}

	void FixedUpdate () 
    {
        if (!_isComplete)
        {
            if (_timer != null) _timer.Update();
            currentDistance = animCurve.Evaluate(currentTime / time) * moveDistance;
        }
	}

    public void Start(CompleteDele onCompleteCallback)
    {
        Reset();

        _isComplete = false;
        _onCompleteCallback = onCompleteCallback;
        _startPoint = transform.localPosition;

        if (_timer == null)
        {
            _timer = new Timer(changeTime, -1);
            _timer.Tick += OnTick;
        }
        _timer.Start();

        DOTween.To(() => currentTime, x => currentTime = x, time, time).SetEase(Ease.Linear).OnComplete(OnCompleteShake);
    }

    private void Reset()
    {
        currentDistance = 0;
        currentTime = 0;
    }

    private void OnTick()
    {
        Vector3 pos = Vector3.zero;
        Vector3 targetPos = _startPoint + GetRandomPos();
        transform.DOLocalMove(targetPos, changeTime).SetEase(Ease.Linear);
    }

    private Vector3 GetRandomPos()
    {
        return new Vector3(Random.Range(-currentDistance, currentDistance), Random.Range(-currentDistance, currentDistance), Random.Range(-currentDistance, currentDistance));
    }

    private void OnCompleteShake()
    {
        _isComplete = true;
        if (_timer != null) _timer.Stop();
        if(_onCompleteCallback != null) _onCompleteCallback();
    }
}
