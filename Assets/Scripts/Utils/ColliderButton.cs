﻿using UnityEngine;
using System.Collections;
using view.mapScene;

public class ColliderButton : MonoBehaviour 
{
    public delegate void ButtonEvent();
    public event ButtonEvent On_ButtonUp;

    public bool isDebug = true;
    public Camera targetCamera;
    public float raycastDistance = 100.0f;
    public AudioSource audioSource;
    public AudioClip releaseSound;
    public TouchArea touchArea;

    private bool _isActivate = true;

    void Awake()
    {
        Input.simulateMouseWithTouches = true;

        if(touchArea != null) touchArea.On_Click += OnClick;
    }

	void Start () 
    {
        
	}

    private void OnClick()
    {
        if (_isActivate)
        {
            Ray ray = targetCamera.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, raycastDistance, 5))
            {
                if (isDebug) Debug.DrawLine(ray.origin, hit.point);
                Debug.Log(hit.collider.gameObject.name + " / " + gameObject.name);
                if (hit.collider.gameObject.name == gameObject.name)
                {
                    if (audioSource)
                    {
                        if (releaseSound)
                        {
                            audioSource.clip = releaseSound;
                            audioSource.Play();
                        }
                    }
                    if (On_ButtonUp != null) On_ButtonUp();
                }
            }
        }
    }

    void Update()
    {
        if(touchArea == null)
        {
            if(Input.GetMouseButtonUp(0))
            {
                OnClick();
            }
        }
    }

    public void Activate()
    {
        _isActivate = true;
    }

    public void Deactivate()
    {
        _isActivate = false;
    }

    void OnDestroy()
    {
        if (touchArea != null)
        {
            touchArea.On_Click -= OnClick;
        }
    }
}
