﻿using UnityEngine;
using System.Collections;

public class Align2 : MonoBehaviour
{
    public enum State
    {
        TR, TL, BR, B
    }

    public State align;
    public float paddingL = 0.0f;
    public float paddingR = 0.0f;
    public float paddingT = 0.0f;
    public float paddingB = 0.0f;

    void Start()
    {
        if (align.ToString() == "TR")
        {
            transform.localPosition = new Vector3((Screen.width / 2) + (paddingR * (Screen.height / 640f)), (Screen.height / 2) + (paddingT * (Screen.height / 640f)), 0);
        }
        else if (align.ToString() == "TL")
        {
            transform.localPosition = new Vector3(-(Screen.width / 2) + (paddingL * (Screen.height / 640f)), (Screen.height / 2) - (paddingT * (Screen.height / 640f)), 0);
        }
        else if (align.ToString() == "BR")
        {
            transform.localPosition = new Vector3((Screen.width / 2) + (paddingR * (Screen.height / 640f)), -(Screen.height / 2) + (paddingB * (Screen.height / 640f)), 0);
        }
    }

    void Update()
    {
        if (align.ToString() == "TR")
        {
            transform.localPosition = new Vector3((Screen.width / 2) + (paddingR * (Screen.height / 640f)), (Screen.height / 2) + (paddingT * (Screen.height / 640f)), 0);
        }
        else if (align.ToString() == "TL")
        {
            transform.localPosition = new Vector3(-(Screen.width / 2) + (paddingL * (Screen.height / 640f)), (Screen.height / 2) - (paddingT * (Screen.height / 640f)), 0);
        }
        else if (align.ToString() == "BR")
        {
            transform.localPosition = new Vector3((Screen.width / 2) + (paddingR * (Screen.height / 640f)), -(Screen.height / 2) + (paddingB * (Screen.height / 640f)), 0);
        }
    }
}
