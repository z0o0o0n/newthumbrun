﻿using UnityEngine;
using System.Collections;

public class StringFloatDic 
{
    public string name;
    public float value;

    public StringFloatDic(string name, float value)
    {
        this.name = name;
        this.value = value;
    }
}
