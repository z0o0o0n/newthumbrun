﻿namespace Com.Mod.ThumbRun.Util
{
    using System;

    public class GoldFormat
    {
        public static string ConvertString(int gold)
        {
            string result;
            if (gold == 0) result = "0";
            else result = String.Format("{0:##,##}", gold);
            return result;
        }
    }
}