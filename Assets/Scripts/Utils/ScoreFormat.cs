﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;
using System;

public class ScoreFormat : MonoBehaviour
{
    public static string ConvertString(float record)
    {
        if(record == 0)
        {
            return "00.000";
        }
        else
        {
            string[] recordSplit = record.ToString().Split(new string[] { "." }, System.StringSplitOptions.RemoveEmptyEntries);
			if(recordSplit.Length == 1){
				return Format.ConvertDigit(recordSplit[0].ToString(), 2, "0") + ".000";
			}
			else{
            	return Format.ConvertDigit(recordSplit[0].ToString(), 2, "0") + "." + Format.ConvertDigit(recordSplit[1].ToString(), 3, "0", "back");
			}
        }
    }

    public static int ConvertMS(float record)
    {
        string stringRecord = ScoreFormat.ConvertString(record);
        float floatRecord = float.Parse(stringRecord);
        return int.Parse((floatRecord * 1000f).ToString());
    }

	void Start ()
    {

	}
	
	void Update ()
    {
	
	}
}
