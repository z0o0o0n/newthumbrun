﻿using UnityEngine;
using System.Collections;

public class NetworkStatus : MonoBehaviour 
{
    public UISprite icon;
    public NetworkChecker cheker;

	void Start () 
	{
        Check();
	}

    public void callbackFunction(bool isConnection)
    {
        Debug.Log("isConnection : " + isConnection);
        if (isConnection) icon.gameObject.SetActive(false);
        else icon.gameObject.SetActive(true);

        Invoke("Check", 1);
    }

    public void Check()
    {
        cheker.Check(1.0f, callbackFunction);
    }
	
	void Update () 
	{
	
	}
}
