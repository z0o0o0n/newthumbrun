﻿using UnityEngine;
using System.Collections;
using System;

public class UIScreen : MonoBehaviour
{
    [SerializeField]
    private UIWidget _widget;
    private static UIScreen _instance;

    public static UIScreen instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }


    public int GetWidth()
    {
        int result = (int)Math.Round(Screen.width * _widget.root.pixelSizeAdjustment);
        //Debug.Log(">>>>>>>>>>>>>> width: " + Screen.width + " / W: " + Screen.width * _widget.root.pixelSizeAdjustment);
        return result;
    }

    public int GetHeight()
    {
        //Debug.Log(">>>>>>>>>>>>>> height: " + Screen.height + " / H: " + Screen.height * _widget.root.pixelSizeAdjustment);
        int result = (int) (Screen.height * _widget.root.pixelSizeAdjustment);
        return result;
    }

    //public static int GetWidth(int screenWidth)
    //{
    //    int result = screenWidth;
    //    if(result >= 640) result = screenWidth * (640/screenHeight);
    //    return result;
    //}

    //public static int GetHeight(int screenHeight)
    //{
    //    int result = screenHeight;
    //    if(result >= 640) result = 640;
    //    return result;
    //}
}
