﻿using UnityEngine;
using System.Collections;

public class CharacterStatusUitil : MonoBehaviour 
{
    public static CharacterStatusData ConvertStatusDataByGauge(CharacterStatusData statusData)
    {
        CharacterStatusData result = new CharacterStatusData();
        result.run = ConvertStatusValueByGauge(CharacterStatusType.type.Run, statusData.run);
        result.boostRun = ConvertStatusValueByGauge(CharacterStatusType.type.BoostRun, statusData.boostRun);
        result.jump = ConvertStatusValueByGauge(CharacterStatusType.type.Jump, statusData.jump);
        result.boostJump = ConvertStatusValueByGauge(CharacterStatusType.type.BoostJump, statusData.boostJump);
        //result.boostCharging = ConvertStatusValueByGauge(CharacterStatusType.type.BoostCharging, statusData.boostCharging);
        //result.boostConsumption = ConvertStatusValueByGauge(CharacterStatusType.type.BoostConsumption, statusData.boostConsumption);
        return result;
    }

    public static float ConvertStatusValueByGauge(CharacterStatusType.type type, float value)
    {
        float returnValue = value;
        float min = 0.0f;
        float max = 0.0f;

        if (type == CharacterStatusType.type.Run || type == CharacterStatusType.type.BoostRun)
        {
            min = AbilityRange.minRun;
            max = AbilityRange.maxRun;
        }
        else if (type == CharacterStatusType.type.Jump || type == CharacterStatusType.type.BoostJump)
        {
            min = AbilityRange.minJump;
            max = AbilityRange.maxJump;
        }
        //else if (type == CharacterStatusType.type.BoostCharging)
        //{
        //    min = AbilityRange.minBoostCharging;
        //    max = AbilityRange.maxBoostCharging;
        //}
        //else if (type == CharacterStatusType.type.BoostConsumption)
        //{
        //    min = AbilityRange.minBoostCharging;
        //    max = AbilityRange.maxBoostCharging;
        //}

        float validRange = max - min;
        float a = value - min;
        returnValue = (a / validRange);

        return returnValue;
    }
}
