﻿using UnityEngine;
using System.Collections;

public class Align : MonoBehaviour
{
    public enum State
    {
        TR,TL,BR
    }

    public State align;
    public float paddingL = 0.0f;
    public float paddingR = 0.0f;
    public float paddingT = 0.0f;
    public float paddingB = 0.0f;
	
	void Start ()
    {
        if (align.ToString() == "TR")
        {
            transform.localPosition = new Vector3((UIScreen.instance.GetWidth() / 2) + paddingR, (UIScreen.instance.GetHeight() / 2) + paddingT, 0);
        }
        else if(align.ToString() == "TL")
        {
            transform.localPosition = new Vector3(-(UIScreen.instance.GetWidth() / 2) + paddingL, (UIScreen.instance.GetHeight() / 2) - paddingT, 0);
        }
        else if (align.ToString() == "BR")
        {
            transform.localPosition = new Vector3((UIScreen.instance.GetWidth() / 2) + paddingR, -(UIScreen.instance.GetHeight() / 2) + paddingB, 0);
        }
	}
	
	void Update ()
    {
        if (align.ToString() == "TR")
        {
            transform.localPosition = new Vector3((UIScreen.instance.GetWidth() / 2) + paddingR, (UIScreen.instance.GetHeight() / 2) + paddingT, 0);
        }
        else if (align.ToString() == "TL")
        {
            transform.localPosition = new Vector3(-(UIScreen.instance.GetWidth() / 2) + paddingL, (UIScreen.instance.GetHeight() / 2) - paddingT, 0);
        }
        else if (align.ToString() == "BR")
        {
            transform.localPosition = new Vector3((UIScreen.instance.GetWidth() / 2) + paddingR, -(UIScreen.instance.GetHeight() / 2) + paddingB, 0);
        }
	}
}
