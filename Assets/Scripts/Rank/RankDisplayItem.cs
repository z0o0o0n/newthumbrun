﻿using UnityEngine;
using System.Collections;

public class RankDisplayItem : MonoBehaviour 
{
	[SerializeField]
	private UILabel _rankLabel;
	[SerializeField]
	private UILabel _nicknameLabel;
    [SerializeField]
    private UISprite _flagImage;
    [SerializeField]
	private UISprite _profileImage;
	[SerializeField]
	private UISprite _profieBgImage;
	[SerializeField]
	private UISprite _swoonStateIndicator;
    [SerializeField]
    private UISprite _playerMark;
    private int _playerID;
	private int _rank = 0;

	void Start () 
	{
	
	}

	void Update () 
	{
	
	}



	public void SetData(int playerID, int rank, string countryCode, string nickname, int characterIndex, Color characterBgColor, bool isUser)
	{
		_playerID = playerID;
		ChangeRank(rank);
		_nicknameLabel.text = nickname;
        _flagImage.spriteName = "Flag_" + countryCode;
        _profileImage.spriteName = "CharacterProfileThumb_" + characterIndex;
		_profieBgImage.color = characterBgColor;
        if (isUser) _playerMark.gameObject.SetActive(true);
    }

	public void ChangeRank(int rank)
	{
		_rank = rank;
		_rankLabel.text = (_rank + 1).ToString();
	}

	public void ChangeSwoonState()
	{
		_swoonStateIndicator.gameObject.SetActive(true);
	}

	public void ChangeRecoverState()
	{
		_swoonStateIndicator.gameObject.SetActive(false);
	}

	public int GetPlayerID()
	{
		return _playerID;
	}

	public int GetRank()
	{
		return _rank;
	}
}
