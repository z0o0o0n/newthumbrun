﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class RankManager : MonoBehaviour 
{
    public delegate void RankManagerEvent();
    public event RankManagerEvent RankUpdated;

    [SerializeField]
    private RaceParticipationManager _participationManager;
    [SerializeField]
    private StopwatchModel _stopWatch;
    private RaceManager _raceManager;
    private List<RankCompareData> _rankCompareDataList = new List<RankCompareData>();

    void Awake ()
    {
        _participationManager.Recruited += OnParticipationRecruited;

        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
        _raceManager.RacePrepare += OnRacePrepare;
        _raceManager.RaceClose += OnRaceClose; // 골인 또는 타임아웃과 상관없이 경기를 종료하는 시점.
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {

    }

    void OnDestroy ()
    {
        _participationManager.Recruited -= OnParticipationRecruited;

        _raceManager.RacePrepare -= OnRacePrepare;
        _raceManager.RaceClose -= OnRaceClose;

        PlayerManager player = _participationManager.GetPlayer();
        player.PassStartPoint -= OnPassStartPoint;
        player.PassCheckPoint -= OnPassCheckPoint;

        // List<IRaceParticipant> replayerList = _participationManager.GetReplayerList();
        // for (int i = 0; i < replayerList.Count; i++)
        // {
        //     if(replayerList[i] != null)
        //     {
        //         Replayer replayer = ((MonoBehaviour)replayerList[i]).GetComponent<Replayer>();
        //         replayer.PassStartPoint -= OnPassStartPoint;
        //         replayer.PassCheckPoint -= OnPassCheckPoint;
        //     }
        // }
    }





    private int CompareRank(RankCompareData a, RankCompareData b)
    {
        if (a.roundNum < b.roundNum) return 1;
        if (a.roundNum > b.roundNum) return -1;

        if (a.record < b.record) return -1;
        if (a.record > b.record) return 1;

        return 0;
    }

    private int CompareRankWithRecord(RankCompareData a, RankCompareData b)
    {
        if (a.record < b.record) return -1;
        if (a.record > b.record) return 1;

        return 0;
    }

    private void LogRankList(string prefix)
    {
        for (int i = 0; i < _rankCompareDataList.Count; i++)
        {
            Debug.Log("---------- " + prefix + " rankCompareData count: " + _rankCompareDataList.Count + " / id: " + _rankCompareDataList[i].id + " / rank: " + i + " / round: " + _rankCompareDataList[i].roundNum + " / record: " + _rankCompareDataList[i].record);
        }
        Debug.Log("----------");
    }

    private void SendRankToPlayer(int playerID)
    {
        for (int i = 0; i < _rankCompareDataList.Count; i++)
        {
            if (_rankCompareDataList[i].id == playerID)
            {
                if (playerID == 0)
                {
                    PlayerManager player = _participationManager.GetPlayer();
                    player.UpdateRank(i);
                    //Debug.Log("RankManager - SendRankToPlayer: " + playerID + " rank: " + i);
                }
                else
                {
                    Replayer replayer = _participationManager.GetReplayerByID(playerID);
                    replayer.UpdateRank(i);
                    //Debug.Log("RankManager - SendRankToPlayer: " + playerID + " rank: " + i);
                }
            }
        }
    }

    private void UpdateRank(int participationId)
    {
        Replayer replayer = _participationManager.GetReplayerByID(participationId);
        float record = Mathf.Round((_stopWatch.GetSec() + _stopWatch.GetMsec()) / .001f) * .001f;
        bool hasData = false;

        for (int i = 0; i < _rankCompareDataList.Count; i++)
        {
            if (_rankCompareDataList[i].id == participationId)
            {
                hasData = true;
                if (participationId == 0) _rankCompareDataList[i].roundNum = _participationManager.GetPlayer().GetCurrentCheckPointValue();
                else _rankCompareDataList[i].roundNum = replayer.GetCurrentCheckPointValue();
                _rankCompareDataList[i].record = record;
            }
        }

        if (!hasData)
        {
            //Debug.Log("----- Add Compare Data / id: " + id);
            RankCompareData rankCompareData = new RankCompareData();
            rankCompareData.id = participationId;
            if (participationId == 0) rankCompareData.roundNum = _participationManager.GetPlayer().GetCurrentCheckPointValue();
            else rankCompareData.roundNum = replayer.GetCurrentCheckPointValue();
            rankCompareData.record = record;
            _rankCompareDataList.Add(rankCompareData);
        }

        _rankCompareDataList.Sort(CompareRank);
        //LogRankList();
        SendRankToPlayer(participationId);

        if (RankUpdated != null) RankUpdated();
    }

    private void UpdateFinalRank()
    {
        string log = "Final Rank ----------";
        for (int id = 0; id < _rankCompareDataList.Count; id++)
        {
            for (int i = 0; i < _rankCompareDataList.Count; i++)
            {
                if (_rankCompareDataList[i].id == id)
                {
                    if (id == 0)
                    {
                        PlayerManager player = _participationManager.GetPlayer();
                        _rankCompareDataList[i].roundNum = 3; // 최종 순위 결정에서는 기록을 기준으로만 하기위해 모두 같은 roundNum 값을 넣음
                        if (player.isFinishedRace)
                        {
                            _rankCompareDataList[i].record = player.GetParticipantData().record;
                            log += "\n Player id: 0 / finished: true / record: " + _rankCompareDataList[i].record;
                        }
                        else
                        {
                            _rankCompareDataList[i].record = 0.0f;
                            log += "\n Player id: 0 / finished: false / record: 60";
                        }
                    }
                    else
                    {
                        Replayer replayer = _participationManager.GetReplayerByID(id);
                        _rankCompareDataList[i].roundNum = 3;
                        if (replayer.isFinishedRace)
                        {
                            _rankCompareDataList[i].record = replayer.GetParticipantData().record;
                            log += "\n Player id: " + id + " / finished: true / record: " + _rankCompareDataList[i].record;
                        }
                        else
                        {
                            _rankCompareDataList[i].record = replayer.GetParticipantData().record;
                            log += "\n Player id: " + id + " / finished: false / record: " + _rankCompareDataList[i].record;
                        }
                    }
                }
            }
        }

        Debug.Log(log);

        _rankCompareDataList.Sort(CompareRankWithRecord);

        for (int i = 0; i < _rankCompareDataList.Count; i++)
        {
            SendRankToPlayer(i);
        }

        if (RankUpdated != null) RankUpdated();
    }





    private void OnParticipationRecruited(IRaceParticipant player, List<IRaceParticipant> replayers)
    {
        int playerParticipationId = 0;
        PlayerManager user = ((MonoBehaviour)player).GetComponent<PlayerManager>();
        user.PassStartPoint += OnPassStartPoint;
        user.PassCheckPoint += OnPassCheckPoint;        
        UpdateRank(playerParticipationId);

        for(int i = 0; i < replayers.Count; i++)
        {
            int replayerParticipationId = i + 1;
            Replayer replayer = ((MonoBehaviour)replayers[i]).GetComponent<Replayer>();
            replayer.PassStartPoint += OnPassStartPoint;
            replayer.PassCheckPoint += OnPassCheckPoint;
            UpdateRank(replayerParticipationId);
        }
    }

    private void OnRacePrepare()
    {
        if(_rankCompareDataList.Count != 0) _rankCompareDataList.Clear();

        PlayerManager player = _participationManager.GetPlayer();
        RankCompareData playerCompareData = new RankCompareData();
        playerCompareData.id = player.GetPlayerID();
        playerCompareData.roundNum = player.GetCurrentCheckPointValue();
        playerCompareData.record = 0.000f;
        _rankCompareDataList.Add(playerCompareData);

        List<IRaceParticipant> replayers = _participationManager.GetReplayerList();
        for (int i = 0; i < replayers.Count; i++)
        {
            Replayer replayer = (Replayer)replayers[i];
            RankCompareData replayerCompareData = new RankCompareData();
            replayerCompareData.id = replayer.GetID();
            replayerCompareData.roundNum = replayer.GetCurrentCheckPointValue();
            replayerCompareData.record = 0.000f;
            _rankCompareDataList.Add(replayerCompareData);
        }

        if (RankUpdated != null) RankUpdated();
    }

    private void OnPassStartPoint(int id)
    {
        UpdateRank(id);
    }

    private void OnPassCheckPoint(int id)
    {
        UpdateRank(id);
    }

    private void OnRaceClose()
    {
        UpdateFinalRank();
    }
    




    public List<RankCompareData> GetRankList()
    {
        return _rankCompareDataList;
    }
}

public class RankCompareData
{
    public int id;
    public float roundNum;
    public float record;
}