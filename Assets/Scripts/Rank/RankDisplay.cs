﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Com.Mod.ThumbRun.Item.View;

public class RankDisplay : MonoBehaviour 
{
    [SerializeField]
    private GameObject _rankProfilePrefab;
    [SerializeField]
    private RaceParticipationManager _participationManager;
    [SerializeField]
    private RankManager _rankManager;
    [SerializeField]
    private UIWidget _widget;
    [SerializeField]
    private UISprite _bg;
    private RaceManager _raceManager;
    private List<RankDisplayItem> _rankDisplayItemList = new List<RankDisplayItem>();

    void Awake ()
    {
        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();

        _participationManager.Recruited += OnParticipationRecruited;
        _rankManager.RankUpdated += OnRankUpdated;

        _raceManager.RaceOpen += OnRaceOpen;
        _raceManager.RaceStart += OnRaceStart;
        _raceManager.RaceTimeout += OnTimeoutRace;
        _raceManager.RaceEnd += OnEndRace;
        //Hide(0f);
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {

    }

    void OnDestroy()
    {
        _participationManager.Recruited -= OnParticipationRecruited;
        _rankManager.RankUpdated -= OnRankUpdated;

        _raceManager.RaceOpen -= OnRaceOpen;
        _raceManager.RaceStart -= OnRaceStart;
        _raceManager.RaceTimeout -= OnTimeoutRace;
        _raceManager.RaceEnd -= OnEndRace;
    }



    #region Private Functions
    private void OnParticipationRecruited(IRaceParticipant player, List<IRaceParticipant> replayers)
    {
        if(HasRankDisplayItem()) ClearRankDisplayItems();
        CreateRankProfiles();
        ResizeBG();

        //transform.localPosition = new Vector2(0f, 155f);
    }

    private void OnRankUpdated()
    {
        ChangeRank();
    }

    private bool HasRankDisplayItem()
    {
        if(_rankDisplayItemList.Count == 0) return false;
        else return true;
    }

    private void ClearRankDisplayItems()
    {
        for(int i = 0; i < _rankDisplayItemList.Count; i++)
        {
            Destroy(_rankDisplayItemList[i].gameObject);
        }
        _rankDisplayItemList.Clear();
    }

    private void OnRaceOpen()
    {
        PlayerManager player = _participationManager.GetPlayer().GetComponent<PlayerManager>();
        player.PlayerSwooned -= OnPlayerSwooned;
        player.PlayerRecovered -= OnPlayerRecovered;

        int replayerCount = _participationManager.GetReplayerList().Count;
        for(int i = 0; i < replayerCount; i++)
        {
            Replayer replayer = ((MonoBehaviour)_participationManager.GetReplayerList()[i]).GetComponent<Replayer>();
            replayer.PlayerSwooned -= OnPlayerSwooned;
            replayer.PlayerRecovered -= OnPlayerRecovered;
        }

        Hide(0f);
    }

    private void OnRaceStart()
    {
        Show();
    }

    private void OnTimeoutRace()
    {
        Hide(0f);
    }

    private void OnEndRace()
    {
        Hide(0f);
    }

    private void CreateRankProfiles()
    {
        // Player
        PlayerManager player = _participationManager.GetPlayer().GetComponent<PlayerManager>();
        player.PlayerSwooned += OnPlayerSwooned;
        player.PlayerRecovered += OnPlayerRecovered;

        GameObject instance = GameObject.Instantiate(_rankProfilePrefab);
        instance.transform.parent = transform;
        instance.transform.localScale = Vector3.one;
        instance.transform.localPosition = new Vector2(0f, 0 * -50f);

        CharacterData characterData = CharacterDataManager.instance.GetCharacterDataByID(player.GetMainCharacterID());

        RankDisplayItem item = instance.GetComponent<RankDisplayItem>();
        //Debug.Log("-----> Rank Display / player country code: " + player.GetParticipantData().countryCode);
        item.SetData(player.GetPlayerID(), 0, player.GetParticipantData().countryCode, player.GetNickname(), player.GetMainCharacterID(), characterData.bgColor, true);

        _rankDisplayItemList.Add(item);

        // Replayer
        int replayerCount = _participationManager.GetReplayerList().Count;
        for(int i = 0; i < replayerCount; i++)
        {
            Replayer replayer = ((MonoBehaviour)_participationManager.GetReplayerList()[i]).GetComponent<Replayer>();
            replayer.PlayerSwooned += OnPlayerSwooned;
            replayer.PlayerRecovered += OnPlayerRecovered;

            instance = GameObject.Instantiate(_rankProfilePrefab);
            instance.transform.parent = transform;
            instance.transform.localScale = Vector3.one;
            instance.transform.localPosition = new Vector2(0f, (i + 1) * -50f);

            item = instance.GetComponent<RankDisplayItem>();
            ParticipantData participantData = replayer.GetParticipantData();
            characterData = CharacterDataManager.instance.GetCharacterDataByID(participantData.characterId);
            item.SetData(replayer.GetID(), i + 1, participantData.countryCode, participantData.nickname, participantData.characterId, characterData.bgColor, false);

            _rankDisplayItemList.Add(item);
        }
    }

    private void OnPlayerSwooned(int id, DamageType.type damageType)
    {
        _rankDisplayItemList[id].ChangeSwoonState();
    }

    private void OnPlayerRecovered(int id, DamageType.type damageType)
    {
        _rankDisplayItemList[id].ChangeRecoverState();
    }

    private void ChangeRank()
    {
        //Debug.Log("RankDisplay - ChangeRank()");
        List<RankCompareData> _rankList = _rankManager.GetRankList();

        for(int i = 0; i < _rankDisplayItemList.Count; i++)
        {
            RankDisplayItem item = _rankDisplayItemList[i];
            for(int j = 0; j < _rankList.Count; j++)
            {
                RankCompareData rankData = _rankList[j];
                if(item.GetPlayerID() == rankData.id) item.ChangeRank(j);
            }
        }

        for(int i = 0; i < _rankDisplayItemList.Count; i++)
        {
            RankDisplayItem item = _rankDisplayItemList[i];
            DOTween.Kill("RankDisplayItemMoveY." + i);
            item.transform.DOLocalMoveY((item.GetRank() * -50f) - 12f, 0.2f).SetId("RankDisplayItemMoveY." + i);
        }
    }

    private void ResizeBG()
    {
        int playerCount = _rankDisplayItemList.Count;
        _bg.height = (playerCount * 50) + 24;
    }

    private void Show()
    {
        DOTween.Kill("RankDisplay." + GetInstanceID());
        DOTween.To(()=> _widget.alpha, x=> _widget.alpha =x, 1f, 0.5f).SetId("RankDisplay." + GetInstanceID());
    }

    private void Hide(float time)
    {
        DOTween.Kill("RankDisplay." + GetInstanceID());
        DOTween.To(()=> _widget.alpha, x=> _widget.alpha =x, 0f, 0.5f).SetId("RankDisplay." + GetInstanceID());
    }
    #endregion
}
