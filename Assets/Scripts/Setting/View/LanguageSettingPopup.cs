﻿namespace Com.Mod.ThumbRun.Setting.View
{
    using UnityEngine;
    using System.Collections;
    using Game.LanguageManager;
    using System.Collections.Generic;
    using Controller;
    using Application;
    using Com.Mod.ThumbRun.UI.Popup;

    public class LanguageSettingPopup : BasicPopup
    {
        [SerializeField]
        private List<UIDynamicButton> _buttons;
        [SerializeField]
        private SettingMenuPopup _settingPopup;
        private SettingService _settingService;
        private LanguageManager _languageManager;

        private void Awake()
        {
            transform.localPosition = Vector2.zero;
            ClosePopup();

            _settingService = GameObject.FindGameObjectWithTag("Setting").GetComponent<SettingService>();
            _languageManager = GameObject.FindGameObjectWithTag("LanguageManager").GetComponent<LanguageManager>();
        }

        void Start()
        {
            
        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            for (int i = 0; i < _buttons.Count; i++)
            {
                _buttons[i].Click -= OnButtonClick;
            }
        }



        private void OnButtonClick(GameObject target)
        {
            LanguageButton languageButton = target.GetComponent<LanguageButton>();

            if (languageButton.languageType == Language.type.KOREAN) { ChangeLanguage(0, languageButton); }
            else if (languageButton.languageType == Language.type.ENGLISH) { ChangeLanguage(1, languageButton); }
            else if (languageButton.languageType == Language.type.RUSSIAN) { ChangeLanguage(6, languageButton); }
            else if (languageButton.languageType == Language.type.GERMAN) { ChangeLanguage(9, languageButton); }
            else if (languageButton.languageType == Language.type.SPANISH) { ChangeLanguage(12, languageButton); }
            else if (languageButton.languageType == Language.type.FRENCH) { ChangeLanguage(5, languageButton); }
            else if (languageButton.languageType == Language.type.ITALIAN) { ChangeLanguage(8, languageButton); }
            else if (languageButton.languageType == Language.type.PORTUGUESE) { ChangeLanguage(7, languageButton); }
            else if (languageButton.languageType == Language.type.TURKISH) { ChangeLanguage(10, languageButton); }
            else if (languageButton.languageType == Language.type.CHINESE_S) { ChangeLanguage(3, languageButton); }
            else if (languageButton.languageType == Language.type.CHINESE_T) { ChangeLanguage(4, languageButton); }
            else if (languageButton.languageType == Language.type.THAI) { ChangeLanguage(11, languageButton); }
            else if (languageButton.languageType == Language.type.JAPANESE) { ChangeLanguage(2, languageButton); }
        }

        private void ChangeLanguage(int buttonIndex, LanguageButton languageButton)
        {
            ResetAllButtons();
            _languageManager.ChangeLanguage(languageButton.languageType);
            _buttons[buttonIndex].Disable();
            _settingService.SetLanguageType(languageButton.languageType);
        }


        private void ResetAllButtons()
        {
            for(int i = 0; i < _buttons.Count; i++)
            {
                _buttons[i].Enable();
            }
        }



        public void OpenPopup()
        {
            ResetAllButtons();
            Language.type languageType = _languageManager.GetLanguageType();
            if (languageType == Language.type.KOREAN)
            {
                _buttons[0].Disable();
            }
            else if (languageType == Language.type.ENGLISH)
            {
                _buttons[1].Disable();
            }
            else if (languageType == Language.type.RUSSIAN)
            {
                _buttons[6].Disable();
            }

            for (int i = 0; i < _buttons.Count; i++)
            {
                _buttons[i].Click += OnButtonClick;
            }

            base.Open();
        }

        public void ClosePopup()
        {
            base.Close();
        }

        public void PressCloseButton()
        {
            _settingPopup.OpenPopup();
            ClosePopup();
        }
    }
}