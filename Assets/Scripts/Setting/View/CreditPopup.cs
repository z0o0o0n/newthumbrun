﻿using System.Collections;
using System.Collections.Generic;
using Com.Mod.ThumbRun.Controller;
using Com.Mod.ThumbRun.UI.Popup;
using UnityEngine;

public class CreditPopup : BasicPopup 
{
	[SerializeField]
	private SettingMenuPopup _settingPopup;

	void Start () 
	{
		gameObject.transform.localPosition = Vector3.zero;
		base.Close();
	}
	
	void Update () 
	{
		
	}




	public void OpenPopup()
	{
		base.Open();
	}

	public void ClosePopup()
	{
		base.Close();
		_settingPopup.OpenPopup();
	}
}
