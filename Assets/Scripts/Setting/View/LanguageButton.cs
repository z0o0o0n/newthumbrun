﻿namespace Com.Mod.ThumbRun.Setting.View
{
    using UnityEngine;
    using System.Collections;
    using Game.LanguageManager;

    public class LanguageButton : MonoBehaviour
    {
        [SerializeField]
        private Language.type _languageType;

        public Language.type languageType
        {
            get { return _languageType; }
        }
    }
}