﻿using Com.Mod.ThumbRun.Setting.Application;
using Com.Mod.ThumbRun.User.Application;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SNSButton : MonoBehaviour
{
    public enum type { FACEBOOK, TWITTER }

    [SerializeField]
    private UILabel _info;
    [SerializeField]
    private UILabel _rewardInfo;
    [SerializeField]
    private UISprite _snsIcon;
    [SerializeField]
    private UIAlign _snsRewardInfo;
    //[SerializeField]
    //private UILabel _rewardGold;
    [SerializeField]
    private SettingService _settingService;
    [SerializeField]
    private UserService _userService;
    [SerializeField]
    private SNSButton.type _type = type.FACEBOOK;
    private Vector3 _snsIconStartPos;
    private UIDynamicButton _dynamicButton;

    private void Awake()
    {
        _dynamicButton = GetComponent<UIDynamicButton>();
        //_snsIconStartPos = _snsIcon.transform.localPosition;
        _settingService = GameObject.FindGameObjectWithTag("Setting").GetComponent<SettingService>();
        _settingService.Prepared += OnPrepared;
        if (_settingService.isPrepared) OnPrepared();

        _dynamicButton.Click += OnButtonClick;
    }

    void Start ()
    {
    }

    void Update ()
    {
		//if(Input.GetKeyUp(KeyCode.Alpha1))
  //      {
  //          ChangeNormalMode();
  //      }
  //      else if (Input.GetKeyUp(KeyCode.Alpha2))
  //      {
  //          ChangeRewardMode();
  //      }
    }

    private void OnDestroy()
    {
        _settingService.Prepared -= OnPrepared;
        _dynamicButton.Click -= OnButtonClick;
    }





    private void OnPrepared()
    {
        if (!_settingService.isPrepared) return;
        
        if(_type == type.FACEBOOK)
        {
            if (_settingService.IsFacebookPressed())
            {
                ChangeNormalMode();
            }
            else
            {
                ChangeRewardMode();
            }
        }
        else if(_type == type.TWITTER)
        {
            if (_settingService.IsTwitterPressed())
            {
                ChangeNormalMode();
            }
            else
            {
                ChangeRewardMode();
            }
        }
    }

    private void OnButtonClick(GameObject sender)
    {
        ChangeNormalMode();
    }





    private void ChangeRewardMode()
    {
        Debug.Log("SNS Button - ChangeRewardMode()");
        _info.gameObject.SetActive(false);
        _rewardInfo.gameObject.SetActive(true);
        _snsRewardInfo.gameObject.SetActive(true);
        //_rewardGold.gameObject.SetActive(true);
        _snsIcon.gameObject.SetActive(false);

        _dynamicButton.ShowNewIcon();
    }

    private void ChangeNormalMode()
    {
        Debug.Log("SNS Button - ChangeNormalMode()");
        _info.gameObject.SetActive(true);
        _rewardInfo.gameObject.SetActive(false);
        _snsRewardInfo.gameObject.SetActive(false);
        //_rewardGold.gameObject.SetActive(false);
        _snsIcon.gameObject.SetActive(true);

        DOTween.Kill("SNSButton.SaveGoldDelay." + GetInstanceID());
        DOVirtual.DelayedCall(2f, SaveGold).SetId("SNSButton.SaveGoldDelay." + GetInstanceID());
    }

    private void SaveGold()
    {
        if (_type == type.FACEBOOK)
        {
            if (!_settingService.IsFacebookPressed())
            {
                _settingService.PressFacebook();
                _userService.Save(400);
            }
        }
        else if (_type == type.TWITTER)
        {
            if (!_settingService.IsTwitterPressed())
            {
                _settingService.PressTwitter();
                _userService.Save(400);
            }
        }
    }
}
