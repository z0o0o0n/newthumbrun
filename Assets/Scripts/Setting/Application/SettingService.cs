﻿namespace Com.Mod.ThumbRun.Setting.Application
{
    using UnityEngine;
    using System.Collections;
    using Game.LanguageManager;
    using Domain;

    public class SettingService : MonoBehaviour
    {
        public delegate void SettingServieHandler();
        public event SettingServieHandler Prepared;
        public event SettingServieHandler Error;

        private bool _isPrepared = false;
        [SerializeField]
        private Setting _setting;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }




        private void Awake()
        {
            Debug.Log("Awake Setting Service");
            _setting.Prepared += OnPrepared;
            _setting.Error += OnError;
        }

        void Start()
        {

        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _setting.Prepared -= OnPrepared;
            _setting.Error -= OnError;
        }




        private void OnPrepared()
        {
            Debug.Log("Setting Service / isPrepared: true");

            SeasonTimer.instance.SetRemainingSec((int)_setting.GetRemainingTime().TotalSeconds);
            NewADManager.instance.SetAdSettings(_setting.GetAdSettings());

            _isPrepared = true;
            if (Prepared != null) Prepared();
        }

        private void OnError(string wwwText)
        {
            Debug.LogWarning("SettingService - OnError / " + wwwText);
            if (Error != null) Error();
        }





        public Language.type GetLanguageType()
        {
            return _setting.GetLanguageType();
        }

        public void SetLanguageType(Language.type languageType)
        {
            _setting.SetLanguageType(languageType);
        }

        public int IsBGMOn()
        {
            return _setting.IsBGMOn();
        }

        public void SetBGMState(int isBGMOn)
        {
            _setting.SetBGMState(isBGMOn);
        }

        public bool IsFacebookPressed()
        {
            return _setting.IsFacebookPressed();
        }

        public void PressFacebook()
        {
            _setting.PressFacebook();
        }

        public bool IsTwitterPressed()
        {
            return _setting.IsTwitterPressed();
        }

        public void PressTwitter()
        {
            _setting.PressTwitter();
        }

        public bool IsReviewVersion()
        {
            return _setting.IsReviewVersion();
        }

        public bool IsOldVersion()
        {
            return _setting.IsOldVersion();
        }
    }
}