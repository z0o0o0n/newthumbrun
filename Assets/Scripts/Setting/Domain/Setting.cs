﻿namespace Com.Mod.ThumbRun.Setting.Domain
{
    using UnityEngine;
    using System.Collections;
    using GameDataEditor;
    using Game.LanguageManager;
    using LitJson;
    using System.Collections.Generic;
    using App;
    using System;

    public class Setting : MonoBehaviour
    {
        public delegate void SettingEvent();
        public delegate void SettingHttpEvent(string wwwText);
        public event SettingEvent Prepared;
        public event SettingHttpEvent Error;

        private static Setting _instance;

        [SerializeField]
        private string _settingRequestURI;
        private HttpRequest _httpRequest;
        private GDESettingData _rawData;
        private bool _isPrepared = false;
        private string _seasonId;
        private TimeSpan _remainingTime;
        private List<ADSetting> _adSettings;

        void Start()
        {
            if(_instance == null)
            {
                if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Setting_Setting, out _rawData))
                {
                    _httpRequest = gameObject.AddComponent(typeof(HttpRequest)) as HttpRequest;
                    _httpRequest.Complete += OnCompleted;
                    _httpRequest.TimeOut += OnError;
                    _httpRequest.Error += OnError;

                    RequireSettingData();
                    //_rawData.ResetAll(); //지울것
                }
                else
                {
                    Debug.LogError("Read Error Entry Data");
                }

                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _httpRequest.Complete -= OnCompleted;
            _httpRequest.TimeOut -= OnError;
            _httpRequest.Error -= OnError;
        }




        private void OnCompleted(string id, string wwwText)
        {
            if (id == "settingRequest")
            {
                Debug.Log("Setting(settingRequest) load Completed: " + wwwText);

                JsonData json = JsonMapper.ToObject(wwwText);
                _rawData.isReviewVersion = (json["service"].ToString().ToLower() == "true") ? false : true;
                _rawData.isOldVersion = (json["update"].ToString().ToLower() == "true") ? true : false;

                _adSettings = new List<ADSetting>();

                bool use = (json["ad"]["adcolony"]["use"].ToString().ToLower() == "true") ? true : false;
                int rate = int.Parse(json["ad"]["adcolony"]["percent"].ToString());
                _adSettings.Add(new ADSetting(ADBrandName.name.AD_COLONY, use, rate));

                use = (json["ad"]["unityads"]["use"].ToString().ToLower() == "true") ? true : false;
                rate = int.Parse(json["ad"]["unityads"]["percent"].ToString());
                _adSettings.Add(new ADSetting(ADBrandName.name.UNITY_AD, use, rate));

                use = (json["ad"]["tapjoy"]["use"].ToString().ToLower() == "true") ? true : false;
                rate = int.Parse(json["ad"]["tapjoy"]["percent"].ToString());
                _adSettings.Add(new ADSetting(ADBrandName.name.TAPJOY, use, rate));

                use = (json["crosspromotion"]["use"].ToString().ToLower() == "true") ? true : false;
                rate = int.Parse(json["crosspromotion"]["percent"].ToString());
                CrossPromotionSetting.instance.SetValue(use, rate);


                _seasonId = json["season"]["id"].ToString();
                int day = int.Parse(json["season"]["expire"]["day"].ToString());
                int hour = int.Parse(json["season"]["expire"]["hour"].ToString());
                int min = int.Parse(json["season"]["expire"]["minute"].ToString());
                int sec = int.Parse(json["season"]["expire"]["second"].ToString());
                _remainingTime = new TimeSpan(day, hour, min, sec);

                //_weeklyData.Clear();
                //for (int i = 0; i < json["player"].Count; i++)
                //{
                //    PrizeRankItemData data = new PrizeRankItemData();
                //    data.rank = int.Parse(json["player"][i]["rank"].ToString());
                //    data.nickname = json["player"][i]["nickname"].ToString();
                //    data.countryCode = json["player"][i]["country"].ToString();
                //    data.prize = ulong.Parse(json["player"][i]["gold"].ToString());
                //    _weeklyData.Add(data);
                //}

                //if (int.Parse(json["me"]["rank"].ToString()) == -1) _weeklyPlayerData = null;
                //else
                //{
                //    _weeklyPlayerData = new PrizeRankItemData();
                //    _weeklyPlayerData.rank = int.Parse(json["me"]["rank"].ToString());
                //    _weeklyPlayerData.nickname = json["me"]["nickname"].ToString();
                //    _weeklyPlayerData.countryCode = json["me"]["country"].ToString();
                //    _weeklyPlayerData.prize = ulong.Parse(json["me"]["gold"].ToString());
                //}

                //if (WeeklyRankLoadCompleted != null) WeeklyRankLoadCompleted(wwwText);
            }

            Debug.Log("Setting Data Prepared");
            _isPrepared = true;
            if (Prepared != null) Prepared();
        }

        private void OnError(string id, string wwwText)
        {
            if (id == "settingRequest")
            {
                Debug.Log("Setting(settingRequest) load Error:1");
                Debug.Log("Setting(settingRequest) load Error: " + wwwText);
                Debug.Log("Setting(settingRequest) load Error:2");
            }
            Debug.Log("Setting(settingRequest) load Error:3");
            if (Error != null) Error(wwwText);
        }





        private void RequireSettingData()
        {
            string platform = Platform.GetPlatformType().ToString();
            Dictionary<string, string> stringArgs = new Dictionary<string, string>();
            stringArgs.Add("client_id", ServerEnv.clientId);
            stringArgs.Add("version", Application.version);
            stringArgs.Add("platform", platform);

            string uri = ServerEnv.host + _settingRequestURI;
            Debug.Log("uri: " + uri);
            Debug.Log("ServerEnv.clientId: " + ServerEnv.clientId + " / Application.version: " + Application.version + " / platform: " + platform);
            _httpRequest.Post("settingRequest", uri, stringArgs, 15);
        }





        public Language.type GetLanguageType()
        {
            Language.type result;

            if (_rawData.languageType == Language.type.KOREAN.ToString()) result = Language.type.KOREAN;
            else if (_rawData.languageType == Language.type.ENGLISH.ToString()) result = Language.type.ENGLISH;
            else if (_rawData.languageType == Language.type.RUSSIAN.ToString()) result = Language.type.RUSSIAN;
            else if (_rawData.languageType == Language.type.GERMAN.ToString()) result = Language.type.GERMAN;
            else if (_rawData.languageType == Language.type.SPANISH.ToString()) result = Language.type.SPANISH;
            else if (_rawData.languageType == Language.type.FRENCH.ToString()) result = Language.type.FRENCH;
            else if (_rawData.languageType == Language.type.ITALIAN.ToString()) result = Language.type.ITALIAN;
            else if (_rawData.languageType == Language.type.PORTUGUESE.ToString()) result = Language.type.PORTUGUESE;
            else if (_rawData.languageType == Language.type.TURKISH.ToString()) result = Language.type.TURKISH;
            else if (_rawData.languageType == Language.type.CHINESE_S.ToString()) result = Language.type.CHINESE_S;
            else if (_rawData.languageType == Language.type.CHINESE_T.ToString()) result = Language.type.CHINESE_T;
            else if (_rawData.languageType == Language.type.THAI.ToString()) result = Language.type.THAI;
            else if (_rawData.languageType == Language.type.JAPANESE.ToString()) result = Language.type.JAPANESE;
            else result = Language.type.NONE;

            return result;
        }

        public void SetLanguageType(Language.type languageType)
        {
            Debug.Log("Setting - SetLanguageType / _rawData: " + _rawData);
            _rawData.languageType = languageType.ToString();
        }

        public int IsBGMOn()
        {
            return _rawData.bgmOnOff;
        }

        public void SetBGMState(int isBGMOn)
        {
            Debug.Log(">>>>>>>>>>>>>>>>>>>> isBGMOn: " + isBGMOn);
            _rawData.bgmOnOff = isBGMOn;
            Debug.Log(">>>>>>>>>>>>>>>>>>>> isBGMOn: " + _rawData.bgmOnOff);
        }

        public bool IsFacebookPressed()
        {
            return _rawData.isFacebookPressed;
        }

        public void PressFacebook()
        {
            _rawData.isFacebookPressed = true;
        }

        public bool IsTwitterPressed()
        {
            return _rawData.isTwitterPressed;
        }

        public void PressTwitter()
        {
            _rawData.isTwitterPressed = true;
        }

        public bool IsReviewVersion()
        {
            return _rawData.isReviewVersion;
        }

        public bool IsOldVersion()
        {
            return _rawData.isOldVersion;
        }

        public string GetSeasonId()
        {
            return _seasonId;
        }

        public TimeSpan GetRemainingTime()
        {
            return _remainingTime;
        }

        public List<ADSetting> GetAdSettings()
        {
            return _adSettings;
        }
    }
}