﻿using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour
{
    [SerializeField]
    private Portal _connectedPortal;
    [SerializeField]
    private Vector3 _openedPos;

    void Start ()
    {
	    
	}
	
	void Update ()
    {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (_connectedPortal == null) return;

        //Debug.Log("other.gameObject.tag: " + other.gameObject.tag);

        //Debug.Log("+++++ other: " + other.gameObject.tag);
        if (other.gameObject.tag == "Player")
        {
            PlayerManager player = other.gameObject.GetComponent<PlayerManager>();
            //Vector2 offsetPos = transform.localPosition - _connectedPortal.transform.localPosition;
            player.MoveToPortal(_connectedPortal.openedPos);
        }
        else if(other.gameObject.tag == "Replayer")
        {
            Replayer replayer = other.gameObject.GetComponent<Replayer>();
            replayer.MoveToPortal(_connectedPortal.openedPos);
        }
    }

    public Vector3 openedPos
    {
        get { return _openedPos; }
    }
}
