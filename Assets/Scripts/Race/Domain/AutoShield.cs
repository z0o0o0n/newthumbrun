﻿namespace Com.Mod.ThumbRun.Race.Domain
{
    using GameDataEditor;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class AutoShield : MonoBehaviour
    {
        public delegate void AutoShieldEventHandler();
        public event AutoShieldEventHandler Prepared;

        [SerializeField]
        //private User _user;
        private GDEAutoShieldData _rawData;
        private bool _isPrepared = false;

        void Start()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.AutoShield_AutoShield, out _rawData))
            {
                Debug.Log("AutoShield Data Prepared");

                _isPrepared = true;
                if (Prepared != null) Prepared();
            }
            else
            {
                Debug.LogError("Read Error AutoShield Data");
            }
        }

        void Update()
        {

        }





        public bool IsInfoShowed()
        {
            return _rawData.isInfoShowed;
        }

        public void SetInfoShowed(bool isShowed)
        {
            _rawData.isInfoShowed = isShowed;
        }

        public bool HasAutoShield()
        {
            return _rawData.hasAutoShield;
        }

        public void SetAutoShield(bool hasAutoShield)
        {
            _rawData.hasAutoShield = hasAutoShield;
        }
    }
}