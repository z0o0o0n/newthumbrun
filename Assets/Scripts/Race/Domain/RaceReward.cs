﻿namespace Com.Mod.ThumbRun.Race.Domain
{
    using GameDataEditor;
    using UnityEngine;
    using System.Collections;

    public class RaceReward : MonoBehaviour
    {
        public delegate void RaceRewardEventHandler();
        public event RaceRewardEventHandler Prepared;

        private GDERaceRewardData _rawData;
        private bool _isPrepared = false;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }

        void Start()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.RaceReward_RaceReward, out _rawData))
            {
                Debug.Log("RaceReward Data Prepared");

                _isPrepared = true;
                if (Prepared != null) Prepared();
            }
            else
            {
                Debug.LogError("Read Error RaceReward Data");
            }
        }

        void Update()
        {

        }



        public float GetRewardGoldDiameter(int rank)
        {
            float result = 0;

            if(rank == 0)
            {
                result = _rawData.RewardGoldDiameter0;
            }
            else if(rank == 1)
            {
                result = _rawData.RewardGoldDiameter1;
            }
            else if(rank == 2)
            {
                result = _rawData.RewardGoldDiameter2;
            }

            Debug.Log(">>>>>>>>>>>>>>> rank: " + rank + " / diameter: " + result);

            return result;
        }

        public int GetRewardExp(int rank)
        {
            int result = _rawData.RewardExpDefault;

            if (rank == 0)
            {
                result = _rawData.RewardExp0;
            }
            else if (rank == 1)
            {
                result = _rawData.RewardExp1;
            }
            else if (rank == 2)
            {
                result = _rawData.RewardExp2;
            }

            //Debug.Log("+++++ get reward exp: " + result);
            return result;
        }
    }
}