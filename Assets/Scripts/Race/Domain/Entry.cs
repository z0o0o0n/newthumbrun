﻿namespace Com.Mod.ThumbRun.Race.Domain
{
    using UnityEngine;
    using System.Collections;
    using GameDataEditor;
    using User.Domain;
    using System.Collections.Generic;

    public class Entry : MonoBehaviour
    {
        public delegate void EntryEventHandler();
        public event EntryEventHandler Prepared;

        [SerializeField]
        private User _user;
        private GDEEntryItemListData _rawData;
        private bool _isPrepared = false;
        private List<string> _randomRaceStageId = new List<string>() {"1", "2", "3", "4", "5", "6"};

        void Start()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.EntryItemList_EntryItemList, out _rawData))
            {
                Debug.Log("Entry Data Prepared");

                _isPrepared = true;
                if (Prepared != null) Prepared();
            }
            else
            {
                Debug.LogError("Read Error Entry Data");
            }
        }

        void Update()
        {

        }



        public GDEEntryItemData GetEntryInfo(int entryIndex)
        {
            GDEEntryItemData entryItem = _rawData.items[entryIndex];
            return entryItem;
        }

        public List<GDEEntryItemData> GetEntryInfos()
        {
            return _rawData.items;
        }

        public int GetCurrentEntryIndex()
        {
            return _rawData.currentEntryIndex;
        }

        public EntryResult CheckEntryCondition(int entryIndex)
        {
            GDEEntryItemData entryItem = _rawData.items[entryIndex];

            if (entryItem.limitLv > _user.GetLevel())
            {
                return new EntryResult(2);
            }
            else if (entryItem.entryFee > _user.GetGoldAmount())
            {
                return new EntryResult(1);
            }
            
            return new EntryResult(0);
        }

        public EntryResult SendEntry(int entryIndex)
        {
            EntryResult result = CheckEntryCondition(entryIndex);
            if(result.code == 0) _rawData.currentEntryIndex = entryIndex;
            return result;
        }

        public List<string> GetRandomRaceStageIds()
        {
            return _randomRaceStageId;
        }
    }
}