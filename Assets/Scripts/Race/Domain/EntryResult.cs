﻿namespace Com.Mod.ThumbRun.Race.Domain
{
    using UnityEngine;
    using System.Collections;

    public class EntryResult
    {
        private int _code = 0; // 0:success, 1:no money, 2:low lv

        public EntryResult(int code)
        {
            _code = code;
        }

        public int code
        {
            get { return _code; }
        }
    }
}