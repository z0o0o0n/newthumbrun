﻿using Com.Mod.ThumbRun.LuckyChance.Domain;

namespace Com.Mod.ThumbRun.Race.Domain
{
    public class Reward
    {
        private int _gold;
        private int _exp;
        private LuckyChanceBadge.badge _badge;

        public Reward(int gold, int exp, LuckyChanceBadge.badge badge)
        {
            _gold = gold;
            _exp = exp;
            _badge = badge;
        }

        public int gold
        {
            get { return _gold; }
        }

        public int exp
        {
            get { return _exp; }
        }

        public LuckyChanceBadge.badge badge
        {
            get { return _badge; }
        }
    }
}