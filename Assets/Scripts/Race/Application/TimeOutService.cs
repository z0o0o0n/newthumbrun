﻿namespace Com.Mod.ThumbRun.Race.Application
{
    using UnityEngine;
    using System.Collections;

    public class TimeOutService : MonoBehaviour
    {
        public delegate void TimeOutEventHandler();
        public event TimeOutEventHandler TimeOut;

        [SerializeField]
        private StopwatchModel _stopWatchGlobal;

        void Start()
        {
            _stopWatchGlobal.Tick += OnStopWatchTick;
        }

        void Update()
        {
            
        }

        void OnDestroy()
        {
            _stopWatchGlobal.Tick -= OnStopWatchTick;
        }



        private void OnStopWatchTick()
        {
            //if(_stopWatchGlobal.GetSec() == 10)
            if (_stopWatchGlobal.GetMin() == 1)
            {
                _stopWatchGlobal.Tick -= OnStopWatchTick;
                if (TimeOut != null) TimeOut();
            }
        }
    }
}