﻿namespace Com.Mod.ThumbRun.Race.Application
{
    using UnityEngine;
    using System.Collections;
    using Domain;
    using LuckyChance.Domain;

    public class RaceRewardService : MonoBehaviour
    {
        public delegate void RaceRewardServiceEvent();
        public event RaceRewardServiceEvent Prepared;

        [SerializeField]
        private RaceReward _raceReward;
        [SerializeField]
        private EntryService _entryService;
        private bool _isPrepared = false;
        private float _additionalRewardGoldRate = 1;
        private float _conditionBonusGoldRate = 1;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }





        void Start()
        {
            _raceReward.Prepared += OnPrepared;
            if (_raceReward.isPrepared) OnPrepared();
        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _raceReward.Prepared -= OnPrepared;
        }
        




        private void OnPrepared()
        {
            if (!_raceReward.isPrepared) return;

            _isPrepared = true;
            if (Prepared != null) Prepared();
        }





        public Reward CaculateReward(int entryIndex, ParticipantData userParticipantData)
        {
            float diameter = _raceReward.GetRewardGoldDiameter(userParticipantData.rank);
            int entryFee = _entryService.GetEntryInfo(entryIndex).entryFee;
            int rewardGold = (int)((entryFee * diameter) * _additionalRewardGoldRate * _conditionBonusGoldRate);
            int rewardExp = _raceReward.GetRewardExp(userParticipantData.rank);
            //Debug.Log("+++++ entryIndex: " + entryIndex + " / rank: " + userParticipantData.rank + " / rewardGold: " + rewardGold + " / rewardExp: " + rewardExp);

            LuckyChanceBadge.badge badge = LuckyChanceBadge.badge.NONE;
            if (userParticipantData.rank == 0) badge = LuckyChanceBadge.badge.GOLD;
            else if (userParticipantData.rank == 1) badge = LuckyChanceBadge.badge.SILVER;
            else if (userParticipantData.rank == 2) badge = LuckyChanceBadge.badge.BRONZE;

            return new Reward(rewardGold, rewardExp, badge);
        }

        public void SetAdditionalRewardGoldRate(float additionalRewardGoldRate)
        {
            _additionalRewardGoldRate = additionalRewardGoldRate;
        }

        public void SetConditionBonusGoldRate(float conditionBonusGoldRate)
        {
            _conditionBonusGoldRate = conditionBonusGoldRate;
        }
    }
}