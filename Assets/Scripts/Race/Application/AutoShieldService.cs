﻿namespace Com.Mod.ThumbRun.Race.Application
{
    using Domain;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class AutoShieldService : MonoBehaviour
    {
        public delegate void AutoShieldServiceEventHandler();
        public event AutoShieldServiceEventHandler Prepared;
        public event AutoShieldServiceEventHandler ChangeAutoShield;
        public delegate void AutoShieldUseEventHandler(IRaceParticipant player);
        public event AutoShieldUseEventHandler ItemUsed;

        [SerializeField]
        private AutoShield _autoShield;
        private bool _isPrepared = false;
        // private bool _hasAutoShield = false;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }

        // public bool hasAutoShield
        // {
        //     get { return _hasAutoShield; }
        // }






        private void Awake()
        {
            _autoShield.Prepared += OnAutoShieldPrepared;
        }

        void Start()
        {

        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _autoShield.Prepared -= OnAutoShieldPrepared;
        }




        private void OnAutoShieldPrepared()
        {
            _isPrepared = true;
            if(Prepared != null) Prepared();
        }




        // info bubble
        public bool IsInfoShowed()
        {
            return _autoShield.IsInfoShowed();
        }

        public void SetInfoShowed(bool isShowed)
        {
            _autoShield.SetInfoShowed(isShowed);
        }


        // auto shield
        public void SetAutoShield(bool hasAutoShield)
        {
            _autoShield.SetAutoShield(hasAutoShield);
            if(ChangeAutoShield != null) ChangeAutoShield();
        }

        public void UseAutoShield(IRaceParticipant player)
        {
            // _hasAutoShield = false;
            _autoShield.SetAutoShield(false);
            if (ItemUsed != null) ItemUsed(player);
        }

        public bool HasAutoShield()
        {
            return _autoShield.HasAutoShield();
        }
    }
}