﻿namespace Com.Mod.ThumbRun.Race.Application
{
    using UnityEngine;
    using System.Collections;
    using Domain;
    using GameDataEditor;
    using System.Collections.Generic;

    public class EntryService : MonoBehaviour
    {
		public delegate void EntryServiceEventHandler();
		public event EntryServiceEventHandler Prepared;
		
        [SerializeField]
        private Entry _entry;
		private bool _isPrepared = false;

		void Awake()
		{
			_entry.Prepared += OnEntryPrepared;
		}

        void Start()
        {

        }

        void Update()
        {

        }

		void OnDestroy()
		{
			_entry.Prepared -= OnEntryPrepared;
		}



		private void OnEntryPrepared()
		{
			_isPrepared = true;
			if (Prepared != null)
				Prepared ();
		}

        public GDEEntryItemData GetEntryInfo(int entryIndex)
        {
            return _entry.GetEntryInfo(entryIndex);
        }

        public List<GDEEntryItemData> GetEntryInfos()
        {
            return _entry.GetEntryInfos();
        }

        // 0: random, 1~6 map 순서
        public int GetCurrentEntryIndex()
        {
            return _entry.GetCurrentEntryIndex();
        }

        public string GetStageIdByEntryIndex(int entryIndex)
        {
            return GetEntryInfo(entryIndex).stageId;
        }

        // 참가요청한 뒤 결과를 받음. 조건에 부합되면 참가처리됨.
        public EntryResult SendEntry(int entryIndex)
        {
            return _entry.SendEntry(entryIndex);
        }

        // 참가 가능 여부만 확인 후 결과를 받음.
        public EntryResult CheckEntryCondition(int entryIndex)
        {
            return _entry.CheckEntryCondition(entryIndex);
        }

        public bool isPrepared
		{
			get{ return _isPrepared;}
		}

        public List<string> GetRandomRaceStageIds()
        {
            return _entry.GetRandomRaceStageIds();
        }

        public string GetRandomStageId()
        {
            List<string> stageIdList = _entry.GetRandomRaceStageIds();
            return stageIdList[Random.Range(0, stageIdList.Count)];
        }
    }
}