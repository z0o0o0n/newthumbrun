﻿namespace Com.Mod.ThumbRun.Race.Application
{
    using UnityEngine;
    using System.Collections;

    public class RaceRecordService : MonoBehaviour
    {
        public delegate void RaceRecordEventHandler(int round, float record);
        public event RaceRecordEventHandler Added;
        public event RaceRecordEventHandler Finished;

        [SerializeField]
        private PlayerManager _playerManager;
        [SerializeField]
        private RaceManager _raceManager;
        [SerializeField]
        private StopwatchModel _stopWatchForRecord;
        [SerializeField]
        private StopwatchModel _stopWatchForGlobal;
        private float _raceRecord = 0f;

        void Awake()
        {
            _raceManager.RaceStart += OnRaceStart;
            _raceManager.RaceClose += OnRaceClosed;
            _playerManager.PassStartPoint += OnPassStartPoint;
            _playerManager.PassCheckPoint += OnPassCheckPoint;
        }

        void Start()
        {

        }

        void Update()
        {

        }

        void OnDestroy()
        {
            _raceManager.RaceStart -= OnRaceStart;
            _raceManager.RaceClose -= OnRaceClosed;
            _playerManager.PassStartPoint -= OnPassStartPoint;
            _playerManager.PassCheckPoint -= OnPassCheckPoint;
        }



        private void OnRaceStart()
        {
            _raceRecord = 0f;
            _stopWatchForRecord.Play();
        }

        private void OnRaceClosed()
        {
            _stopWatchForGlobal.Stop();
        }

        private void OnPassStartPoint(int playerId)
        {
            int round = _playerManager.GetCurrentRound();
            float checkPointValue = _playerManager.GetCurrentCheckPointValue();
            float roundRecord = 0f;

            if (round == 0) return;

            if (round == 1 && checkPointValue == 1)
            {
                roundRecord = _stopWatchForRecord.GetSec() + _stopWatchForRecord.GetMsec();
                _raceRecord += roundRecord;
                _stopWatchForRecord.Stop();
                _stopWatchForRecord.Play();

                if (Added != null) Added(round - 1, roundRecord);
            } 
            else if(round == 2 && checkPointValue == 2)
            {
                roundRecord = _stopWatchForRecord.GetSec() + _stopWatchForRecord.GetMsec();
                _raceRecord += roundRecord;
                _stopWatchForRecord.Stop();
                _stopWatchForRecord.Play();

                if (Added != null) Added(round - 1, roundRecord);
            }
            else if (round == 3 && checkPointValue == 3)
            {
                roundRecord = _stopWatchForRecord.GetSec() + _stopWatchForRecord.GetMsec();
                _raceRecord += roundRecord;
                _stopWatchForRecord.Stop();

                if (Added != null) Added(round - 1, roundRecord);
                if (Finished != null) Finished(round - 1, _raceRecord);
            }

            //Debug.Log("====== Round " + _playerManager.GetCurrentRound() + " / currentRecord: " + _raceRecord.ToString() + " / roundRecord: " + roundRecord.ToString() + " / checkPointValue: " + _playerManager.GetCurrentCheckPointValue());
        }

        private void OnPassCheckPoint(int playerId)
        {
            //Debug.Log("====== check point : " + playerId + " / round: " + _playerManager.GetCurrentRound() + " / checkPointValue: " + _playerManager.GetCurrentCheckPointValue());
        }



        public float GetRaceRecord()
        {
            return _raceRecord;
        }
    }
}