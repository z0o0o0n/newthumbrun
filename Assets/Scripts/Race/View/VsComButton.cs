﻿namespace Com.Mod.ThumbRun.Race.View
{
    using UnityEngine;
    using System.Collections;

    public class VsComButton : MonoBehaviour
    {
        public delegate void VsComButtonEventHandler(int entryFee);
        public event VsComButtonEventHandler Click;

        [SerializeField]
        private UIDynamicButton _button;
        private int _entryFee;

        void Start()
        {

        }

        void Update()
        {

        }



        public void ChangeEntryInfo(int entryFee, int limitLv)
        {
            _entryFee = entryFee;
        }

        public void PressButton()
        {
            _button.Disable();
            if (Click != null) Click(_entryFee);
        }

        public UIDynamicButton GetButton()
        {
            return _button;
        }
    }
}