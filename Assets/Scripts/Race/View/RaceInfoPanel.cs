﻿using Com.Mod.ThumbRun.Race.Application;
using Com.Mod.ThumbRun.Race.Domain;
using Com.Mod.ThumbRun.Race.View;
using Com.Mod.ThumbRun.User.Application;
using Com.Mod.ThumbRun.User.View;
using GameDataEditor;
using System.Collections.Generic;
using UnityEngine;
using Com.Mod.ThumbRun.Participation.View;
using Com.Mod.ThumbRun.SocialManager.Domain;
using Com.Mod.ThumbRun.Achievement.Application;
using Com.Mod.ThumbRun.RaceStats.Domain;
using Com.Mod.ThumbRun.RaceStats.Application;
using Com.Mod.ThumbRun.PrizeRank.View;
using DG.Tweening;
using Com.Mod.ThumbRun.PrizeRank.Application;
using Com.Mod.ThumbRun.LuckyChance.View;
using Com.Mod.ThumbRun.Analytics.Application;

public class RaceInfoPanel : MonoBehaviour 
{
    public delegate void RaceInfoPanelEventHandler();
    public event RaceInfoPanelEventHandler ResultShowed;

    public enum state { Info, Result };

    [SerializeField]
    private PlayScene _playScene;
    [SerializeField]
    private ParticipantInfoDisplay _participantInfoDisplay;
    [SerializeField]
    private StartButton _startButton;
    [SerializeField]
    private AutoShieldButton _autoShieldButton;
    [SerializeField]
    private UIBasicButton _homeButton;
    [SerializeField]
    private RaceManager _raceManager;
    [SerializeField]
    private RaceParticipationManager _raceParticipationManager;
    [SerializeField]
    private WalletDisplay _walletDisplay;
    [SerializeField]
    private NoEntryFeePopup _noEntryFeePopup;
    [SerializeField]
    private EntryService _entryService;
    [SerializeField]
    private UserService _userService;
    [SerializeField]
    private RaceRewardService _raceRewardService;
    [SerializeField]
    private RaceRecordService _raceRecordService;
    [SerializeField]
    private PrizeRankService _prizeRankService;
    [SerializeField]
    private MatchingLimitTimeView _matchingLimitTimeView;
    [SerializeField]
    private RaceStatsService _raceStatsService;
    [SerializeField]
    private DropOutPopup _dropOutPopup;
    [SerializeField]
    private NextRaceButtonContainer _nextRaceButtonContainer;
    [SerializeField]
    private RankChangeDisplay _rankChangeDisplay;
    [SerializeField]
    private LuckyChanceStatusView _luckyChanceStatusView;
    [SerializeField]
    private SkillSlotDisplay _skillSlotDisplay;
    [SerializeField]
    private AnalyticsService _analyticsService;
    private GameManager _gameManager;
    private SocialManagementService _socialManagementService;
    private RaceInfoPanel.state _panelMode = RaceInfoPanel.state.Info;
    private bool _isDebug = false;


    void Awake ()
	{
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        _socialManagementService = GameObject.FindGameObjectWithTag("SocialManager").GetComponent<SocialManagementService>();
        _analyticsService = GameObject.FindGameObjectWithTag("Analytics").GetComponent<AnalyticsService>();

        gameObject.transform.localPosition = Vector2.zero;

        _prizeRankService.Prepared += OnPrepared;

        _raceManager.RaceInfoPanelShow += OnRaceInfoPanelShow;
        _raceManager.RaceStart += OnRaceStart;
        _raceManager.RaceEnd += OnRaceEnd;
        _raceManager.RaceTimeout += OnRaceTimeOut;
        _raceManager.RaceClose += OnRaceClose;

        _participantInfoDisplay.InfoShowed += OnParticipantInfoShowed;
        _participantInfoDisplay.ResultShowed += OnParticipantDisplayResultShowed;

        _luckyChanceStatusView.ResetBackLight();
        _luckyChanceStatusView.Hide();

        _startButton.Click += OnStartButtonClick;
        _nextRaceButtonContainer.Click += OnNextRaceButtonClick;

        _dropOutPopup.Closed += OnDropOutPopupClosed;

        if (RaceModeInfo.raceMode == RaceMode.mode.SINGLE)
        {
            _matchingLimitTimeView.Hide();
        }
        else
        {
            _matchingLimitTimeView.Show();
            _matchingLimitTimeView.StartTimer();
        }
    }

    void Start()
    {
        _skillSlotDisplay.gameObject.SetActive(false);
    }

    void OnDestroy()
    {
        _prizeRankService.Prepared += OnPrepared;

        _raceManager.RaceInfoPanelShow -= OnRaceInfoPanelShow;
        _raceManager.RaceStart -= OnRaceStart;
        _raceManager.RaceEnd -= OnRaceEnd;
        _raceManager.RaceTimeout -= OnRaceTimeOut;
        _raceManager.RaceClose -= OnRaceClose;

        _participantInfoDisplay.InfoShowed -= OnParticipantInfoShowed;
        _participantInfoDisplay.ResultShowed -= OnParticipantDisplayResultShowed;

        _startButton.Click -= OnStartButtonClick;
        _nextRaceButtonContainer.Click -= OnNextRaceButtonClick;

        _dropOutPopup.Closed -= OnDropOutPopupClosed;
    }





    private void OnPrepared()
    {
        if (!_prizeRankService.isPrepared) return;
    }

    private void OnRaceStart()
    {
        MissionManager.instance.UpdateMissionState();

        _skillSlotDisplay.DisplaySkillIcons(_userService.GetCharacterId());
        _skillSlotDisplay.gameObject.SetActive(true);
    }





    // Participant
    private void OnRaceInfoPanelShow()
    {
        ShowPanel();
    }

    private void ShowPanel()
    {
        _panelMode = RaceInfoPanel.state.Info;

        gameObject.SetActive(true);
        _nextRaceButtonContainer.Hide();
        _walletDisplay.gameObject.SetActive(false);
        // _homeButton.gameObject.SetActive(false);


        // 더미 사용자 데이터
        ParticipantData userData = new ParticipantData(1.1f);
        userData.nickname = _userService.GetNickname();
        userData.countryCode = _userService.GetCountryCode();
        userData.isUser = true;
        userData.level = _userService.GetLevel();
        userData.characterId = _userService.GetCharacterId();
        //userData.characterIndex = 2;
        _participantInfoDisplay.Init(userData, 6);
        
        _startButton.Hide();

        //_autoShieldButton.Hide();
        //_autoShieldButton.UpdateIconState();

        _raceParticipationManager.Recruited += OnParticipantRecruited;
        _raceParticipationManager.Recruit();
    }

    private void OnParticipantRecruited(IRaceParticipant player, List<IRaceParticipant> replayers)
    {
        _raceParticipationManager.Recruited -= OnParticipantRecruited;

        List<ParticipantData> participantDatas = new List<ParticipantData>();
        for(int i = 0; i < replayers.Count; i++)
        {
            participantDatas.Add(replayers[i].GetParticipantData());
        }

        _participantInfoDisplay.SetReplayers(participantDatas);
    }

    private void OnRaceEnd()
    {
        _skillSlotDisplay.gameObject.SetActive(false);
    }

    private void OnRaceTimeOut()
    {
        _skillSlotDisplay.gameObject.SetActive(false);
    }

    private void OnParticipantInfoShowed()
    {
        _startButton.Show();
        _homeButton.gameObject.SetActive(false);

        if (RaceModeInfo.raceMode == RaceMode.mode.MULTI)
        {
            _matchingLimitTimeView.Hide();
            _matchingLimitTimeView.StopTimer();
        }
    }





    // Result
    private void OnRaceClose()
    {
        Debug.Log("Race Close");
        DOVirtual.DelayedCall(0.5f, ShowResultPanel).SetUpdate(true);

        if (_socialManagementService.isPrepared)
        {
            Debug.Log("++++++++++++++++++ RaceModeInfo.stageId: " + RaceModeInfo.stageId);
            if (RaceModeInfo.stageId == "1") _socialManagementService.ReportAchievement(GPGSIds.achievement_a);
            else if (RaceModeInfo.stageId == "2") _socialManagementService.ReportAchievement(GPGSIds.achievement_b);
            else if (RaceModeInfo.stageId == "3") _socialManagementService.ReportAchievement(GPGSIds.achievement_c);
            else if (RaceModeInfo.stageId == "4") _socialManagementService.ReportAchievement(GPGSIds.achievement_d);
            else if (RaceModeInfo.stageId == "5") _socialManagementService.ReportAchievement(GPGSIds.achievement_e);
            else if (RaceModeInfo.stageId == "6") _socialManagementService.ReportAchievement(GPGSIds.achievement_f);
        }
        // RankManager의 마지막 순위 계산이 RaceClose 시점에 발생함. 동시에 패널 오픈시 순서를 보장할 수 없어 우선 델레이.
    }

    private void ShowResultPanel()
    {
        Debug.Log("RaceInfoPanel - ShowResultPanel()");

        _panelMode = RaceInfoPanel.state.Result;
        List<ParticipantData> participantDatas = GetParticipantDatas();
        ParticipantData userParticipantData = participantDatas[0];
        Debug.Log("------------ entry index: " + _entryService.GetCurrentEntryIndex());
        Reward reward = _raceRewardService.CaculateReward(_entryService.GetCurrentEntryIndex(), userParticipantData);

        _walletDisplay.gameObject.SetActive(true);
        _startButton.Hide();
        _homeButton.gameObject.SetActive(false);
        _luckyChanceStatusView.Show();
        _participantInfoDisplay.ShowParticipantResult(participantDatas, reward);
        RequireRankRecording(reward);
        gameObject.SetActive(true);

        _analyticsService.CountBadge(reward.badge);
        _analyticsService.CountTotalPrizeGold(reward.gold);
        _analyticsService.CountRanking(userParticipantData.rank);
        string stageId = _entryService.GetStageIdByEntryIndex(_entryService.GetCurrentEntryIndex());
        _analyticsService.SetBestRecord(stageId, userParticipantData.record);
        // 불필요 할 것 같음. 확인 후 삭제 요망.
        _raceStatsService.CountBadge(reward.badge);
        _raceStatsService.CountPrize(reward.gold);
        _raceStatsService.CountRace();
        // -----------------------

        _skillSlotDisplay.DisplaySkillIcons(_userService.GetCharacterId());
        if (_isDebug) Debug.Log("RaceInfoPanel - ShowResultPanel / race states \n raceCount: " + _raceStatsService.GetRaceCount() + "\n gold badge count: " + _raceStatsService.GetGoldBadgeCount() + "\n prize amount: " + _raceStatsService.GetPrizeAmount() + "\n prize amount week: " + _raceStatsService.GetPrizeAmountWeek() );
    }

    private void OnParticipantDisplayResultShowed()
    {
        _nextRaceButtonContainer.Show();
        _homeButton.gameObject.SetActive(true);

        if (ResultShowed != null) ResultShowed();
    }





    private void OnStartButtonClick()
    {
        _raceManager.PrepareRace();
        Hide();
    }

    private void OnNextRaceButtonClick(int entryIndex, int entryFee)
    {
        int resultCode = _entryService.SendEntry(entryIndex).code;

        if (resultCode == 0) // 참가 가능
        {
            _nextRaceButtonContainer.Click -= OnNextRaceButtonClick;

            RaceModeInfo.stageId = _entryService.GetStageIdByEntryIndex(entryIndex);
            _userService.Spend(entryFee);
            _playScene.ReloadPlayScene();
        }
        else if (resultCode == 1) // 골드 부족
        {
            _noEntryFeePopup.OpenPopup();
        }
    }

    public void PressHomeButton()
    {
        if(_panelMode == RaceInfoPanel.state.Info)
        {
            _dropOutPopup.ShowPopup();
            Time.timeScale = 0;
        }
        else
        {
            _homeButton.SetButtonActive(false);
            _playScene.GoHome();
        }
    }

    private void OnDropOutPopupClosed()
    {
        Time.timeScale = 1;
    }






    private void Hide()
    {
        gameObject.SetActive(false);
    }

    // 플레이어를 포함한 모든 참가자 정보를 가져옴. 0번 인덱스가 플레이어.
    private List<ParticipantData> GetParticipantDatas()
    {
        List<ParticipantData> data = new List<ParticipantData>();

        // Player
        IRaceParticipant player = _raceParticipationManager.GetPlayer();
        data.Add(player.GetParticipantData());
        if(_isDebug) Debug.Log("RaceInfoPanel - GetParticipantDatas / id: 0 / player.rank: " + data[0].rank);

        // Replayer
        for (int i = 0; i < _raceParticipationManager.GetReplayerList().Count; i++)
        {
            IRaceParticipant replayer = _raceParticipationManager.GetReplayerList()[i];
            data.Add(_raceParticipationManager.GetReplayerList()[i].GetParticipantData());
            Debug.Log("RaceInfoPanel - GetParticipantDatas / id: " + (i + 1) + " / replayer.rank: " + replayer.GetParticipantData().rank);
        }
        return data;
    }

    // 순위 산정에 필요한 정보를 서버로 전송함.
    private void RequireRankRecording(Reward reward)
    {
        _prizeRankService.RankRecordingCompleted += OnRankRecordingCompleted;
        _prizeRankService.RequireRankRecording(_userService.GetNickname(), _userService.GetCountryCode(), reward.gold);
    }

    private void OnRankRecordingCompleted(int userPrizeRank, int fluctuationRange)
    {
        _prizeRankService.RankRecordingCompleted -= OnRankRecordingCompleted;
        _rankChangeDisplay.Show(userPrizeRank, fluctuationRange);
    }
}
