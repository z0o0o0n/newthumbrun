﻿namespace Com.Mod.ThumbRun.Race.View
{
    using UnityEngine;
    using System.Collections;
    using DG.Tweening;
    using Domain;
    using Junhee.Utils;

    public class ResultUI : MonoBehaviour
    {
        public delegate void TimeOutUIEvent();
        public event TimeOutUIEvent GoldCountEnded;

        [SerializeField]
        private UISprite _bg;
        [SerializeField]
        private GameObject _goalUI;
        [SerializeField]
        private NGUILocalizeText _goalLocalizeText;
        [SerializeField]
        private UISprite _goalBg;
        [SerializeField]
        private Color _goalBgColor;
        [SerializeField]
        private GameObject _timeOutUI;
        [SerializeField]
        private NGUILocalizeText _timeoutLocalizeText;
        [SerializeField]
        private UISprite _timeoutBg;
        [SerializeField]
        private Color _timeOutBgColor;
        private RaceManager _raceManager;

        private void Awake()
        {
            gameObject.transform.localPosition = Vector3.zero;
            Hide();

            _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
            _raceManager.RaceEnd += OnRaceEnd;
            _raceManager.RaceTimeout += OnRaceTimeout;
            _raceManager.RaceClose += OnRaceClose;

            _goalLocalizeText.Changed += OnTextChanged;
            _timeoutLocalizeText.Changed += OnTextChanged;
        }

        private void Start()
        {
        }

        private void Update()
        {

        }

        private void OnDestroy()
        {
            _raceManager.RaceEnd -= OnRaceEnd;
            _raceManager.RaceTimeout -= OnRaceTimeout;
            _raceManager.RaceClose -= OnRaceClose;
        }




        private void OnTextChanged()
        {
            _goalBg.width = _goalLocalizeText.label.width + 60;
            _timeoutBg.width = _timeoutLocalizeText.label.width + 60;

            if (_goalBg.width < 150) _goalBg.width = 150;
            if (_timeoutBg.width < 150) _timeoutBg.width = 150;
        }

        private void OnRaceEnd()
        {
            Show(ResultUIType.type.GOAL);
        }

        private void OnRaceTimeout()
        {
            Show(ResultUIType.type.TIMEOUT);
        }

        private void OnRaceClose()
        {
            Hide();
        }

        private void Show(ResultUIType.type resultUIType)
        {
            gameObject.SetActive(true);
            if (resultUIType == ResultUIType.type.GOAL)
            {
                _bg.color = _goalBgColor;
                _timeOutUI.SetActive(false);
                _goalUI.SetActive(true);
                _goalUI.transform.localPosition = new Vector3(0f, -300f, 0f);
                _goalUI.transform.DOLocalMoveY(0f, 0.8f).SetEase(CustomEase.instance.OutBangElastic);
            }
            else  if(resultUIType == ResultUIType.type.TIMEOUT)
            {
                _bg.color = _timeOutBgColor;
                _goalUI.SetActive(false);
                _timeOutUI.SetActive(true);
                _timeOutUI.transform.localPosition = new Vector3(0f, -300f, 0f);
                _timeOutUI.transform.DOLocalMoveY(0f, 0.8f).SetEase(CustomEase.instance.OutBangElastic);
            }
        }

        private void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}