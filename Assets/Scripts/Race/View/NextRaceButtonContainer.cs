﻿namespace Com.Mod.ThumbRun.Race.View
{
    using Application;
    using DG.Tweening;
    using Domain;
    using GameDataEditor;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using User.Application;
    using Util;

    public class NextRaceButtonContainer : MonoBehaviour
    {
        public delegate void NextRaceButtonEvent(int entryIndex, int entryFee);
        public event NextRaceButtonEvent Click;

        [SerializeField]
        private EntryService _entryService;
        [SerializeField]
        private UIDynamicButton _nextRaceButton;
        [SerializeField]
        private UIDynamicButton _nextMapButton;
        [SerializeField]
        private UIGold _nextRaceUiGold;
        [SerializeField]
        private UIGold _nextMapUiGold;
        private bool _isPrepared = false;
        private bool _canPlayNextMap = false;

        private void Awake()
        {
            _entryService.Prepared += OnPrepared;
            Hide();
        }

        void Start()
        {

        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _entryService.Prepared -= OnPrepared;
        }




        private void OnPrepared()
        {
            if (!_entryService.isPrepared) return;
            _isPrepared = true;

            SetupEntryCondition();
        }

        public void OnNextRaceButtonClick()
        {
            GDEEntryItemData entryItemData =_entryService.GetEntryInfo(_entryService.GetCurrentEntryIndex());
            if (Click != null) Click(_entryService.GetCurrentEntryIndex(), entryItemData.entryFee);
        }

        public void OnNextMapButtonClick()
        {
            GDEEntryItemData entryItemData = _entryService.GetEntryInfo(_entryService.GetCurrentEntryIndex() + 1);
            if (Click != null) Click(_entryService.GetCurrentEntryIndex() + 1, entryItemData.entryFee);
        }





        private void SetupEntryCondition()
        {
            GDEEntryItemData entryData = _entryService.GetEntryInfo(_entryService.GetCurrentEntryIndex());
            string conditionInfo = "-" + GoldFormat.ConvertString(entryData.entryFee) + "G";
            if (entryData.limitLv > 0) conditionInfo += "  Lv." + entryData.limitLv.ToString();
            _nextRaceUiGold.SetGold(entryData.entryFee);

            if (!HasNextMap()) return;

            GDEEntryItemData nextEntryData = _entryService.GetEntryInfo(_entryService.GetCurrentEntryIndex() + 1);
            string nextConditionInfo = "-" + GoldFormat.ConvertString(nextEntryData.entryFee) + "G";
            if (nextEntryData.limitLv > 0) nextConditionInfo += "  Lv." + nextEntryData.limitLv.ToString();
            _nextMapUiGold.SetGold(nextEntryData.entryFee);
        }





        public void Show()
        {
            if(!_isPrepared || !CanPlayNextMap())
            {
                ShowOnlyNextRaceButton();
                return;
            }

            if (CanPlayNextMap()) ShowWithNextMapButton();
        }

        private void ShowOnlyNextRaceButton()
        {
            _nextRaceButton.gameObject.SetActive(true);
            DOTween.Kill("NextRaceButton.MoveX." + _nextRaceButton.GetInstanceID());
            DOTween.Kill("NextRaceButton.MoveY." + _nextRaceButton.GetInstanceID());
            _nextRaceButton.transform.DOLocalMoveX(0f, 0f).SetId("NextRaceButton.MoveX." + _nextRaceButton.GetInstanceID());
            _nextRaceButton.transform.DOLocalMoveY(70f, 0.3f).SetEase(Ease.OutQuart).SetId("NextRaceButton.MoveY." + _nextRaceButton.GetInstanceID());
        }

        private void ShowWithNextMapButton()
        {
            _nextRaceButton.gameObject.SetActive(true);
            DOTween.Kill("NextRaceButton.MoveX." + _nextRaceButton.GetInstanceID());
            DOTween.Kill("NextRaceButton.MoveY." + _nextRaceButton.GetInstanceID());
            _nextRaceButton.transform.DOLocalMoveX(-105f, 0f).SetId("NextRaceButton.MoveX." + _nextRaceButton.GetInstanceID());
            _nextRaceButton.transform.DOLocalMoveY(70f, 0.3f).SetEase(Ease.OutQuart).SetId("NextRaceButton.MoveY." + _nextRaceButton.GetInstanceID());

            _nextMapButton.gameObject.SetActive(true);
            DOTween.Kill("NextMapButton.MoveX." + _nextMapButton.GetInstanceID());
            DOTween.Kill("NextMapButton.MoveY." + _nextMapButton.GetInstanceID());
            _nextMapButton.transform.DOLocalMoveX(105f, 0f).SetId("NextMapButton.MoveX." + _nextMapButton.GetInstanceID());
            _nextMapButton.transform.DOLocalMoveY(70f, 0.3f).SetEase(Ease.OutQuart).SetId("NextMapButton.MoveY." + _nextMapButton.GetInstanceID());
        }

        public void Hide()
        {
            _nextRaceButton.gameObject.SetActive(false);
            _nextRaceButton.transform.localPosition = new Vector2(-105f, -70f);
            _nextMapButton.gameObject.SetActive(false);
            _nextMapButton.transform.localPosition = new Vector2(105f, -70f);
        }

        private bool CanPlayNextMap()
        {
            //Debug.Log(">>>>>------ current entry index: " + _entryService.GetCurrentEntryIndex());
            //Debug.Log(">>>>>------ has next map: " + HasNextMap());
            if (!HasNextMap()) return false;

            EntryResult entryResult = _entryService.CheckEntryCondition(_entryService.GetCurrentEntryIndex() + 1);
            //Debug.Log(">>>>>------ entryResult " + entryResult.code);
            if (entryResult.code != 0) return false; // 참가 조건에 부합하지 않음.
            
            return true;
        }

        private bool HasNextMap()
        {
            if (_entryService.GetCurrentEntryIndex() == 0) return false; // entry index 0은 xMap으로 Next Map 기능을 제공하지 않음.
            if (_entryService.GetCurrentEntryIndex() >= (_entryService.GetEntryInfos().Count - 1)) return false; // 마지막 Map에서는 Next Map이 없음.
            return true;
        }
    }
}