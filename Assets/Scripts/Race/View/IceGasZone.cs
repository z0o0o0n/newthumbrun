﻿using Com.Mod.ThumbRun.Item.View;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceGasZone : MonoBehaviour
{
    [SerializeField]
    private List<ParticleSystem> _particleSystems;
    [SerializeField]
    private BoxCollider _collider;
    [SerializeField]
    private float _mixY = 0f;
    [SerializeField]
    private float _maxY = 0f;
    [SerializeField]
    private bool _isAlwaysOn = false;
    private PlayerManager _player;
    private Replayer _replayer;



    void Start ()
    {
        if(_isAlwaysOn)
        {
            OnPower();
        }
        else
        {
            float randomStartTime = Random.Range(0f, 3f);
            DOTween.Kill("IceGasZone.OnPower." + GetInstanceID());
            DOVirtual.DelayedCall(randomStartTime, OnPower).SetId("IceGasZone.OnPower." + GetInstanceID());
        }

        Vector3 pos = transform.localPosition;
        pos.y = Random.Range(_mixY, _maxY);
        transform.localPosition = pos;
    }
	
	void Update ()
    {
		
	}





    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            _player = collider.gameObject.GetComponent<PlayerManager>();
            _player.Swoon(DamageType.type.BOMB, 1.5f, null);
            Debug.Log("...................................... cold");
        }
        else if(collider.tag == "Replayer")
        {
            _replayer = collider.gameObject.GetComponent<Replayer>();
            _replayer.Swoon(DamageType.type.BOMB, 1.5f, null);
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
        {
            Debug.Log("...................................... cold end");
        }
    }




    private void OnPower()
    {
        for(int i = 0; i < _particleSystems.Count; i++)
        {
            _particleSystems[i].Play();
        }
        _collider.enabled = true;

        if(!_isAlwaysOn)
        {
            DOTween.Kill("IceGasZone.OffPower." + GetInstanceID());
            DOVirtual.DelayedCall(1f, OffPower).SetId("IceGasZone.OffPower." + GetInstanceID());
        }
    }

    private void OffPower()
    {
        for (int i = 0; i < _particleSystems.Count; i++)
        {
            if (i != 0)
            {
                _particleSystems[i].Stop();
            }
        }
        _collider.enabled = false;

        float randomStartTime = Random.Range(1.6f, 2.5f);
        DOTween.Kill("IceGasZone.OnPower." + GetInstanceID());
        DOVirtual.DelayedCall(randomStartTime, OnPower).SetId("IceGasZone.OnPower." + GetInstanceID());
    }
}
