﻿namespace Com.Mod.ThumbRun.Race.View
{
    using System.Collections;
    using System.Collections.Generic;
    using DG.Tweening;
    using UnityEngine;

    public class StartButton : MonoBehaviour 
	{
		public delegate void StartButtonEventHandler();
		public event StartButtonEventHandler Click;

		[SerializeField]
		private UILabel _uiLabel;
		private float _currentCount = 6f;

		void Start () 
		{
			
		}
		
		void Update () 
		{
			
		}




		public void Show()
		{
			gameObject.SetActive(true);
			StartCountdown();
		}

		public void Hide()
		{
			gameObject.SetActive(false);
		}

		public void PauseCountdown()
		{
			DOTween.Pause("StartCountdown." + GetInstanceID());
		}

		public void ResumeCountdown()
		{
			DOTween.Play("StartCountdown." + GetInstanceID());
		}




		private void StartCountdown()
		{
			DOTween.Kill("StartCountdown." + GetInstanceID());
			DOTween.To(()=>_currentCount, x=>_currentCount=x, 0.9f, 5f).SetId("StartCountdown." + GetInstanceID()).OnUpdate(OnCountdownTick).OnComplete(OnCountdownComplete).SetEase(Ease.Linear);
		}

		private void OnCountdownTick()
		{
			int count = (int)_currentCount;
			_uiLabel.text = count.ToString();
		}

		private void OnCountdownComplete()
		{
			DOTween.Kill("StartCountdown." + GetInstanceID());
			if(Click != null) Click();
		}

		public void OnButtonClick()
		{
			DOTween.Kill("StartCountdown." + GetInstanceID());
			if(Click != null) Click();
		}
	}
}