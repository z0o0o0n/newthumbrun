﻿namespace Com.Mod.ThumbRun.Race.View
{
    using UnityEngine;
    using System.Collections;

    public class DropOutPopup : MonoBehaviour
    {
        public delegate void DropOutPopupEventHandler();
        public event DropOutPopupEventHandler Closed;

        [SerializeField]
        private PlayScene _playScene;
        [SerializeField]
        private UIDynamicButton _dropOutButton;
        private RaceManager _raceManager;

        void Awake()
        {
            _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
            // _raceManager.RaceEnd
            _raceManager.RaceTimeout += OnRaceTimeout;
            _raceManager.RaceClose += OnRaceClose;
        }

        void Start()
        {
            gameObject.transform.localPosition = Vector3.zero;
            HidePopup();
        }

        void Update()
        {

        }

        void OnDestroy()
        {
            _raceManager.RaceTimeout -= OnRaceTimeout;
            _raceManager.RaceClose -= OnRaceClose;
        }



        private void OnRaceTimeout()
        {
            HidePopup();
        }

        private void OnRaceClose()
        {
            HidePopup();
        }



        public void ShowPopup()
        {
            gameObject.SetActive(true);
        }

        public void HidePopup()
        {
            gameObject.SetActive(false);
        }
        



        public void PressDropOutButton()
        {
            if(_dropOutButton.isEnable)
            {
                _dropOutButton.Disable();
                _playScene.GoHome();
            }
        }

        public void PressCancelButton()
        {
            HidePopup();

            if(Closed != null) Closed();
        }
    }
}