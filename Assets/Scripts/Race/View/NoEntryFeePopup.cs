﻿namespace Com.Mod.ThumbRun.Race.View
{
    using UnityEngine;
    using System.Collections;
    using Com.Mod.ThumbRun.Controller;
    using Com.Mod.ThumbRun.UI.Popup;
    using Com.Mod.ThumbRun.User.Application;
    using Store.View;
    using Participation.View;
    using Setting.Application;
    using TimeStamp;
    using System;

    public class NoEntryFeePopup : MonoBehaviour
    {
        [SerializeField]
        private OfferWallService _offerWallService;
        //[SerializeField]
        //private FreeGoldProvider _freeGoldProvider;
        // [SerializeField]
        // private FreeGoldProvider _freeGoldProvider;
        [SerializeField]
        private UIDynamicButton _rewardADButton;
        [SerializeField]
        private UILabel _rewardADRemainingTimeLabel;
        [SerializeField]
        private UIDynamicButton _offerWallButton;
        [SerializeField]
        private UIDynamicButton _storeButton;
        [SerializeField]
        private UserService _userService;
        [SerializeField]
        private StorePopup _storePopup;
        //[SerializeField]
        //private FreeGoldProvisionData _freeGoldProvisionData;
        [SerializeField]
        private UIGold _rewardAdUiGold;
        [SerializeField]
        private NetworkErrorPopup _networkErrorPopup;
        private SettingService _settingService;

        private void Awake()
        {
            _settingService = GameObject.FindGameObjectWithTag("Setting").GetComponent<SettingService>();
            _settingService.Prepared += OnPrepared;
            if (_settingService.isPrepared) OnPrepared();

            _userService.ShortGold += OnShortGold;
            _offerWallService.Error += OnOfferWallError;

            _rewardADButton.Click += OnVideoAdButtonPressed;
        }

        void Start()
        {
            CoolingTimeStampManager.instance.CoolingEnded += OnCoolingTimeStampEnded;

            transform.localPosition = Vector2.zero;
            _rewardAdUiGold.SetGold(500);
            gameObject.SetActive(false);
        }

        void Update()
        {
            CoolingTimeStamp coolingTimeStamp = CoolingTimeStampManager.instance.GetCoolingTimeStamp("RewardAdGold");
            if (coolingTimeStamp.isCooling)
            {
                TimeSpan remainingTime = CoolingTimeStampManager.instance.GetRemainingTime("RewardAdGold");
                _rewardADRemainingTimeLabel.text = string.Format("{0:D2}:{1:D2}", remainingTime.Minutes, remainingTime.Seconds);
            }

            //if (!TimeStampManager.instance.GetTimeStamp("RewardAD").isEnd)
            //{
            //    TimeSpan remainingTime = TimeStampManager.instance.GetRemainingTime("RewardAD");
            //    _rewardADRemainingTimeLabel.text = string.Format("{0:D2}:{1:D2}", remainingTime.Minutes, remainingTime.Seconds);
            //}
        }

        private void OnDestroy()
        {
            _userService.ShortGold -= OnShortGold;
            _offerWallService.Error -= OnOfferWallError;
            _rewardADButton.Click -= OnVideoAdButtonPressed;

            if(CoolingTimeStampManager.instance != null)
            {
                CoolingTimeStampManager.instance.CoolingEnded -= OnCoolingTimeStampEnded;
            }
        }





        private void OnPrepared()
        {
            if (!_settingService.isPrepared) return;

            if (_settingService.IsReviewVersion())
            {
                _offerWallButton.gameObject.SetActive(false);

                _rewardADButton.transform.localPosition = new Vector3(-65f, -65f);
                _storeButton.transform.localPosition = new Vector3(65f, -65f);
            }
        }

        private void OnRewardADSuccess(string key)
        {
            if(key == "GoldRewardAD")
            {
                NewADManager.instance.Success -= OnRewardADSuccess;
                _userService.Save(500);
            }
        }

        private void OnShortGold(int spentGold, long goldAmount)
        {
            OpenPopup();
        }

        private void OnOfferWallError()
        {
            _networkErrorPopup.Click += OnNetworkErrorPopupClick;
            _networkErrorPopup.ShowPopup();
        }

        private void OnNetworkErrorPopupClick()
        {
            _networkErrorPopup.Click -= OnNetworkErrorPopupClick;
            _networkErrorPopup.HidePopup();
        }

        private void OnCoolingTimeStampEnded(CoolingTimeStamp coolingTimeStamp)
        {
            _rewardADButton.HideInfoPanel();
            _rewardADButton.Enable();
        }





        private void OnVideoAdButtonPressed(GameObject target)
        {
            if (NewADManager.instance.PlayAD("GoldRewardAD"))
            {
                NewADManager.instance.Success += OnRewardADSuccess;
                _rewardADButton.Disable();
                _rewardADButton.ShowInfoPanel();

                CoolingTimeStampManager.instance.Use("RewardAdGold");
                //TimeStampManager.instance.Reset("RewardAD", false);
            }
            else
            {
                _networkErrorPopup.Click += OnNetworkErrorPopupClick;
                _networkErrorPopup.ShowPopup();
            }
        }

        public void OpenPopup()
        {
            Debug.Log("??>>>>>>>>>>>>>> isend: " + TimeStampManager.instance.GetTimeStamp("RewardAD").isEnd);
            if(CoolingTimeStampManager.instance.GetCoolingTimeStamp("RewardAdGold").isCooling)
            {
                _rewardADButton.ShowInfoPanel();
                _rewardADButton.Disable();
            }
            else
            {
                _rewardADButton.HideInfoPanel();
                _rewardADButton.Enable();
            }

            gameObject.SetActive(true);
        }

        public void ClosePopup()
        {
            gameObject.SetActive(false);
        }

        public void PressOfferButton()
        {
            _offerWallService.OpenOfferWall();
        }

        public void PressStoreButton()
        {
            _storePopup.OpenPopup();
        }

        public void PressClose()
        {
            gameObject.SetActive(false);
        }
    }
}