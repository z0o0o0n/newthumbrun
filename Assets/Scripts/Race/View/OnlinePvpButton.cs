﻿namespace Com.Mod.ThumbRun.Race.View
{
    using UnityEngine;
    using System.Collections;
    using Util;
    using DG.Tweening;

    public class OnlinePvpButton : MonoBehaviour
    {
        public delegate void OnlinePvpButtonEventHandler(int entryFee);
        public event OnlinePvpButtonEventHandler Click;

        [SerializeField]
        private UIDynamicButton _button;
        [SerializeField]
        private UIGold _uiGold;
        [SerializeField]
        private UILabel _limitLvLabel;
        private int _entryFee;

        void Start()
        {

        }

        void Update()
        {

        }



        public void ChangeEntryInfo(int entryFee, int limitLv)
        {
            _entryFee = entryFee;
            if (limitLv > 0)
            {
                _limitLvLabel.text = "Lv." + limitLv.ToString();
                _limitLvLabel.gameObject.SetActive(true);
                MoveLvLabelY(9);
            }
            else
            {
                _limitLvLabel.gameObject.SetActive(false);
                MoveLvLabelY(0);
            }
            _uiGold.SetGold(entryFee);
        }

        private void MoveLvLabelY(float posY)
        {
            Vector2 pos = _uiGold.transform.localPosition;
            pos.y = posY;
            _uiGold.transform.localPosition = pos;
        }

        public void PressButton()
        {
            if (Click != null) Click(_entryFee);
        }

        public UIDynamicButton GetButton()
        {
            return _button;
        }
    }
}