﻿namespace Com.Mod.ThumbRun.Race.View
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class AutoShieldInfoBg : MonoBehaviour
    {
        [SerializeField]
        private UILabel _autoShieldInfoLabel;
        private NGUILocalizeText _autoShieldInfoLocalizeText;

        void Start()
        {
            _autoShieldInfoLocalizeText = _autoShieldInfoLabel.GetComponent<NGUILocalizeText>();
            _autoShieldInfoLocalizeText.Changed += OnInfoChanged;

            Resize();
        }

        void Update()
        {

        }

        void OnDisable()
        {
            if(_autoShieldInfoLocalizeText) _autoShieldInfoLocalizeText.Changed -= OnInfoChanged;
        }

        private void OnDestroy()
        {
        }




        private void OnInfoChanged()
        {
            Resize();
        }




        private void Resize()
        {
            UISprite bg = GetComponent<UISprite>();
            bg.height = _autoShieldInfoLabel.height + 20;
        }
    }
}