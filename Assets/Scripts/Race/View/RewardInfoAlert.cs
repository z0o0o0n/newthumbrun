﻿using Com.Mod.ThumbRun.LuckyChance.Domain;
using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardInfoAlert : MonoBehaviour
{
    [SerializeField]
    private GameObject _prizeAlert;
    [SerializeField]
    private UILabel _prizeLabel;
    [SerializeField]
    private GameObject _badgeAlert;
    [SerializeField]
    private UISprite _badgeImage;
    [SerializeField]
    private GameObject _levelUpAlert;
    [SerializeField]
    private UILabel _lvRewardLabel;
    [SerializeField]
    private UISprite _bg;

    private void Awake()
    {
        HideAllAlert();
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}




    private void HideAllAlert()
    {
        _prizeAlert.gameObject.SetActive(false);
        _prizeAlert.transform.localPosition = Vector2.zero;
        _badgeAlert.gameObject.SetActive(false);
        _badgeAlert.transform.localPosition = Vector2.zero;
        _levelUpAlert.gameObject.SetActive(false);
        _levelUpAlert.transform.localPosition = Vector2.zero;
        _bg.gameObject.SetActive(false);
        DOTween.Kill("DisplayDelay." + GetInstanceID());
    }




    public void ShowPrizeAlert(int prizeGold)
    {
        HideAllAlert();
        _prizeLabel.text = String.Format("{0:##,##}", prizeGold);
        _prizeAlert.gameObject.SetActive(true);
        _bg.gameObject.SetActive(true);

        DOTween.Kill("DisplayDelay." + GetInstanceID());
        DOVirtual.DelayedCall(1.2f, OnDisplayEnd).SetId("DisplayDelay." + GetInstanceID());
    }

    public void ShowBadgeAlert(LuckyChanceBadge.badge badge)
    {
        HideAllAlert();
        if(badge == LuckyChanceBadge.badge.GOLD) _badgeImage.spriteName = "GoldBadge";
        else if (badge == LuckyChanceBadge.badge.SILVER) _badgeImage.spriteName = "SilverBadge";
        else if (badge == LuckyChanceBadge.badge.BRONZE) _badgeImage.spriteName = "BronzeBadge";
        _badgeAlert.gameObject.SetActive(true);
        _bg.gameObject.SetActive(true);

        DOTween.Kill("DisplayDelay." + GetInstanceID());
        DOVirtual.DelayedCall(1.2f, OnDisplayEnd).SetId("DisplayDelay." + GetInstanceID());
    }

    public void ShowLevelUpAlert(int rewardGold)
    {
        HideAllAlert();
        _lvRewardLabel.text = String.Format("{0:##,##}", rewardGold);
        _levelUpAlert.gameObject.SetActive(true);
        _bg.gameObject.SetActive(true);

        DOTween.Kill("DisplayDelay." + GetInstanceID());
        DOVirtual.DelayedCall(1.2f, OnDisplayEnd).SetId("DisplayDelay." + GetInstanceID());
    }

    private void OnDisplayEnd()
    {
        HideAllAlert();
    }
}
