﻿namespace Com.Mod.ThumbRun.Race.View
{
    using Application;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class AutoShieldButton : MonoBehaviour
    {
        [SerializeField]
        private UIWidget _infoBubble;
        [SerializeField]
        private AutoShieldService _autoShieldService;
        [SerializeField]
        private AutoShieldIcon _autoShieldIcon;
        [SerializeField]
        private AutoShieldIcon _autoShieldIconForRace;

        private void Awake()
        {
            _autoShieldService.Prepared += OnAutoShieldServicePrepared;
            _autoShieldService.ChangeAutoShield += OnChangeAutoShield;
            Hide();
        }

        void Start()
        {
            
        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _autoShieldService.Prepared -= OnAutoShieldServicePrepared;
            _autoShieldService.ChangeAutoShield -= OnChangeAutoShield;
        }




        private void OnAutoShieldServicePrepared()
        {
            UpdateIconState();
        }

        private void OnChangeAutoShield()
        {
            UpdateIconState();
        }

        public void OnButtonClick()
        {
            //if(NewADManager.instance.PlayAD())
            //{
            //    NewADManager.instance.Success += OnAdSuccess;
                
            //    Hide();
            //    _autoShieldService.SetInfoShowed(true);
            //}
        }

        private void OnAdSuccess()
        {
            //NewADManager.instance.Success -= OnAdSuccess;

            _autoShieldService.SetAutoShield(true);
            _autoShieldIcon.Show();
            _autoShieldIconForRace.Show();
        }




        public void Show()
        {
            gameObject.SetActive(true);
            if(!_autoShieldService.IsInfoShowed())
            {
                _infoBubble.gameObject.SetActive(true);
            }
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            _infoBubble.gameObject.SetActive(false);
        }

        public void UpdateIconState()
        {
            if(_autoShieldService.HasAutoShield())
            {
                _autoShieldIcon.Show();
                _autoShieldIconForRace.Show();
            }
            else
            {
                _autoShieldIcon.Hide();
                _autoShieldIconForRace.Hide();
            }
        }
    }
}