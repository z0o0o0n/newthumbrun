﻿namespace Com.Mod.ThumbRun.Race.View
{
    using Application;
    using DG.Tweening;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class AutoShieldIcon : MonoBehaviour
    {
        [SerializeField]
        private UISprite _icon;
        [SerializeField]
        private UISprite _bg;
        [SerializeField]
        private AudioSource _audioSource;
        [SerializeField]
        private AudioClip _ShowSound;
        [SerializeField]
        private AudioClip _HideSound;
        [SerializeField]
        private AutoShieldService _autoShieldService;

        private void Awake()
        {
            _autoShieldService.Prepared += OnPrepared;
            _autoShieldService.ItemUsed += OnItemUsed;
        }

        void Start()
        {
            _icon.transform.localScale = Vector2.zero;
            _bg.transform.localScale = Vector2.zero;
        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _autoShieldService.Prepared -= OnPrepared;
            _autoShieldService.ItemUsed -= OnItemUsed;
        }




        private void OnPrepared()
        {

        }
        
        private void OnItemUsed(IRaceParticipant player)
        {
            _audioSource.clip = _HideSound;
            _audioSource.Play();
            Hide();
        }




        public void Show()
        {
            _bg.transform.DOScale(1f, 0.6f).SetEase(CustomEase.instance.OutBangElastic);
            _icon.transform.DOScale(1f, 1f).SetEase(CustomEase.instance.OutBangElastic);

            _audioSource.clip = _ShowSound;
            _audioSource.Play();
        }

        public void Hide()
        {
            _bg.transform.DOScale(0f, 1f).SetEase(Ease.InCubic);
            _icon.transform.DOScale(0f, 0.6f).SetEase(Ease.InCubic);
        }
    }
}