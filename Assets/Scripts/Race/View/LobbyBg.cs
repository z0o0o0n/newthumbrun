﻿namespace Com.Mod.ThumbRun.Race.View
{
    using UnityEngine;
    using System.Collections;
    using DG.Tweening;
    using System.Collections.Generic;

    public class LobbyBg : MonoBehaviour
    {
        [SerializeField]
        private List<Color> _bgColors;
        [SerializeField]
        private UISprite _bgSprite;

        void Start()
        {

        }

        void Update()
        {

        }

        public void Change(int mapIndex)
        {
            DOTween.Kill("LobbyBgColor." + GetInstanceID());
            DOTween.To(() => _bgSprite.color, x => _bgSprite.color = x, _bgColors[mapIndex], 0.3f).SetId("LobbyBgColor." + GetInstanceID());
        }
    }
}