﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Controller;
using Com.Mod.ThumbRun.View;
using System.Collections.Generic;

public class TerrainContainer : MonoBehaviour
{
    public CheckPointManager checkPointManager;
    public List<BeatBoard> beatBoardList;

    void Awake ()
    {
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void SetBBBooster(BBBooster bbBooster)
    {
        for(int i = 0; i < beatBoardList.Count; i++)
        {
            beatBoardList[i].SetBBBooster(bbBooster);
        }
    }

    public CheckPointManager GetCheckPointManager()
    {
        return checkPointManager;
    }
}