﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TerrainManager : MonoBehaviour 
{
    [SerializeField]
    private List<TerrainPrefabData> terrainPrefabList;
    public WallJumpParticleManager wallJumpParticleManager;

    private TerrainContainer _currentTerrain;

    public void Init(string stageID, Texture winnerProfile, Texture winnerFlag)
    {
        CreateTerrain(stageID, winnerProfile, winnerFlag);
    }

    private void CreateTerrain(string terrainId, Texture winnerProfile, Texture winnerFlag)
    {
        GameObject instance = GameObject.Instantiate(GetTerrainPrefabById(terrainId));
        instance.transform.localPosition = Vector3.zero;
        instance.transform.parent = transform;

        _currentTerrain = instance.GetComponent<TerrainContainer>();
    }
    
    void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void AttachWallJumpParticle(Vector3 position)
    {
        wallJumpParticleManager.Attach(position);
    }

    public TerrainContainer GetTerrain()
    {
        return _currentTerrain;
    }

    public CheckPointManager GetCheckPointManager()
    {
        return _currentTerrain.GetCheckPointManager();
    }

    private GameObject GetTerrainPrefabById(string terrainId)
    {
        for(int i = 0; i < terrainPrefabList.Count; i++)
        {
            if (terrainPrefabList[i].terrainId == terrainId) return terrainPrefabList[i].terrainPrefab;
        }
        return null;
    }
}

[Serializable]
class TerrainPrefabData
{
    public string terrainId;
    public GameObject terrainPrefab;
}