﻿using UnityEngine;
using System.Collections;

public class WallJumpParticle : MonoBehaviour 
{
    public WallParticle[] particles;

    void Awake()
    {
        Reset();
    }

	void Start ()
    {
	
	}
	
	void Update () 
    {
	
	}





    // Play
    public void Play()
    {
        for (int i = 0; i < particles.Length; i++)
        {
            particles[i].GetComponent<Rigidbody>().isKinematic = false;
            particles[i].GetComponent<Rigidbody>().velocity = Vector3.zero;
            particles[i].GetComponent<Rigidbody>().WakeUp();
            particles[i].gameObject.SetActive(true);
        }
    }



    // Reset
    public void Reset()
    {
        for(int i = 0; i < particles.Length; i++)
        {
            particles[i].GetComponent<Rigidbody>().Sleep();
            particles[i].GetComponent<Rigidbody>().isKinematic = true;
            particles[i].transform.localPosition = Vector3.zero;
            particles[i].gameObject.SetActive(false);
        }
    }
}
