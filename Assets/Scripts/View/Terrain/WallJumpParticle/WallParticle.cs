﻿using UnityEngine;
using System.Collections;

public class WallParticle : MonoBehaviour
{
    public float minSize = 0.01f;
    public float maxSize = 0.1f;
	
    void Awake()
    {
        float randomSize = Random.Range(minSize, maxSize);
        transform.localScale = new Vector3(randomSize, randomSize, randomSize);

        transform.localRotation = Random.rotation;
    }

	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
