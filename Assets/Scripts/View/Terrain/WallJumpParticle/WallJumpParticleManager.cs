﻿using UnityEngine;
using System.Collections;

public class WallJumpParticleManager : MonoBehaviour 
{
    public GameObject wallJumpParticlePrefab;
    public int poolNum = 1;
    
    private WallJumpParticle[] _wallJumpParticlePool;
    private int _currentIndex = 0;

    void Awake()
    {
        _wallJumpParticlePool = new WallJumpParticle[poolNum];

        for(int i = 0; i < poolNum; i++)
        {
            GameObject clone = (GameObject) GameObject.Instantiate(wallJumpParticlePrefab, Vector3.zero, Quaternion.identity);

            _wallJumpParticlePool[i] = clone.GetComponent<WallJumpParticle>();
        }
    }

	void Start ()
    {
	
	}
	
	void Update ()
    {
	
	}





    // Attach
    public void Attach(Vector3 position)
    {
        //WallJumpParticle wallJumpParticle = _wallJumpParticlePool[_currentIndex];
        
        //wallJumpParticle.transform.localPosition = position;
        //wallJumpParticle.Play();

        //_currentIndex++;
        //if (_currentIndex >= _wallJumpParticlePool.Length)
        //{
        //    _currentIndex = 0;
        //}
        //_wallJumpParticlePool[_currentIndex].Reset();
    }
}
