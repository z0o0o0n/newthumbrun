﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Cloud : MonoBehaviour 
{
    // 현재 우측방향으로 이동만 가능.
    public float startPosX = -5;
    public float endPosX = 5;
    public float speed = 0.01f;
    //public float time = 40;
    //public float delay = 0.0f;

    private bool _isPlay = false;

	void Start ()
    {
        Play();
	}

    //private void OnCompleteMove()
    //{
    //    Play();
    //}

    public void Play()
    {
        _isPlay = true;
        //if(reset) transform.localPosition = new Vector3(startPosX, transform.localPosition.y, 2);
        //transform.DOLocalMoveX(endPosX, time).OnComplete(OnCompleteMove).SetDelay(delay).SetEase(Ease.Linear);
    }

	void Update ()
    {
        if(_isPlay)
        {
            Vector3 pos = transform.localPosition;
            pos.x += speed;
            transform.localPosition = pos;

            if (transform.localPosition.x > endPosX)
            {
                pos = transform.localPosition;
                pos.x = startPosX;
                transform.localPosition = pos;
            }
        }
	}
}
