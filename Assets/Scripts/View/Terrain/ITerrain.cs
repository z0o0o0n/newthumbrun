﻿public interface ITerrain
{
    CheckPointManager GetCheckPointManager();
}

[System.Serializable]
public class ITerrainContainer : IUnifiedContainer<ITerrain> {
}