﻿namespace Com.Mod.ThumbRun.View
{
	using UnityEngine;
	using Com.Mod.ThumbRun.Control;
	using System.Collections.Generic;
	using DG.Tweening;

	public class LuckyChanceDisplay : MonoBehaviour 
	{
		[SerializeField]
		private UILabel _diameterLabel;
		[SerializeField]
		private GameObject _diameterContainer;
		[SerializeField]
		private List<UISprite> _bedgeList;
		[SerializeField]
		private UIDynamicButton _getButton;
		[SerializeField]
		private UILabel _bonusGoldLabel;
		[SerializeField]
		private UILabel _remainingGamesLabel;
		private LuckyChance _luckyChance;

		void Awake ()
		{
			_luckyChance = GameObject.FindGameObjectWithTag("LuckyChance").GetComponent<LuckyChance>();
			_luckyChance.Prepared += OnPrepared;
		}

		void Start ()
		{
			
		}
		
		void Update () 
		{
		
		}

		void OnDestroy()
		{
			_luckyChance.Prepared -= OnPrepared;
			_getButton.Click -= OnGetButtonClick;
		}



		#region Public Functions
		#endregion



		#region Private Functions
		private void OnPrepared()
		{
			UpdateDisplay();
		}

		private void UpdateDisplay()
		{
			UpdateDiameter();
			UpdateBedge();
			UpdateRemainingGames();
			UpdateBonusGold();
			
			Debug.Log("----- isChanceActivated: " + _luckyChance.data.isChanceActivated);

			if(_luckyChance.data.isChanceActivated)
			{
				_remainingGamesLabel.gameObject.SetActive(false);
				_getButton.gameObject.SetActive(true);

				_getButton.Click += OnGetButtonClick;
			}
			else
			{
				_remainingGamesLabel.gameObject.SetActive(true);
				_getButton.gameObject.SetActive(false);
			}
		}

		private void UpdateDiameter()
		{
			_diameterLabel.text = _luckyChance.data.diameter.ToString();
			
			// Move X
			float xpos = 0;
			if(_diameterLabel.text.Length > 1) xpos = 12;
			_diameterContainer.transform.DOLocalMoveX(xpos, 0f);
		}

		private void UpdateBedge()
		{
			List<bool> list = _luckyChance.data.raceOutcomeList;
			for(int i = 0; i < list.Count; i++)
			{
				bool outcome = list[i]; 
				if(outcome)
				{
					_bedgeList[i].spriteName = "BedgeWin";
					_bedgeList[i].alpha = 1;
				}
				else
				{
					_bedgeList[i].spriteName = "BedgeLose";
					_bedgeList[i].alpha = 1;
				}
			}

			for(int j = list.Count; j < _luckyChance.data.goalRaceCount; j++)
			{
				_bedgeList[j].spriteName = "BedgeEmpty";
				_bedgeList[j].alpha = 0.2f;
			}
		}

		private void UpdateRemainingGames()
		{
			_remainingGamesLabel.text = _luckyChance.data.remainingRaceCount.ToString() + "게임 남음";
		}

		private void UpdateBonusGold()
		{
			_bonusGoldLabel.text = _luckyChance.data.bonusGold.ToString();
		}

		private void OnGetButtonClick(GameObject target)
		{
			_getButton.Click -= OnGetButtonClick;
			_luckyChance.SaveGold();
			_luckyChance.ResetLuckyChance();
			UpdateDisplay();
		}
		#endregion
	}
}