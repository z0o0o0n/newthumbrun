﻿using UnityEngine;
using System.Collections;

public class GachaState
{
    public static string IDLE = "idle";
    public static string SMALLER_BOX_MOTION = "smallerBoxMotion";
    public static string BOX_THROWING_MOTION = "boxThrowingMotion";
    public static string BOX_UNPACK_IDLE = "boxUnpackIdle";
}
