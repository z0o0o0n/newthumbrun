﻿using UnityEngine;
using System.Collections;

public class CharacterBoxPool : MonoBehaviour 
{
    public delegate void CharacterBoxPoolEvent();
    public event CharacterBoxPoolEvent On_Ready;

    public bool isReady = false;
    public GameObject typeE;
    public int countTypeE;
    public GameObject typeA;
    public int countTypeA;
    public GameObject typeD;
    public int countTypeD;
    public GameObject typeS;
    public int countTypeS;

    private GameObject[] _listE;
    private GameObject[] _listA;
    private GameObject[] _listD;
    private GameObject[] _listS;

    void Awake()
    {
        CreateBoxs(typeE, countTypeE, out _listE);
        CreateBoxs(typeA, countTypeA, out _listA);
        CreateBoxs(typeD, countTypeD, out _listD);
        CreateBoxs(typeS, countTypeS, out _listS);

        isReady = true;
        if (On_Ready != null) On_Ready();
    }

	void Start ()
    {
	    
	}
	
	void Update ()
    {
	
	}

    private void CreateBoxs(GameObject boxPrefab, int count, out GameObject[] returnBoxList)
    {
        returnBoxList = new GameObject[count];

        for(int i = 0; i < count; i++)
        {
            returnBoxList[i] = (GameObject) GameObject.Instantiate(boxPrefab);
            returnBoxList[i].transform.parent = transform;
            returnBoxList[i].transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
            returnBoxList[i].SetActive(false);
        }
    }

    private void RemoveList(GameObject[] list)
    {
        for(int i = 0; i < list.Length; i++)
        {
            list[i].transform.parent = transform;
            list[i].SetActive(false);
        }
    }

    


    // Deactivate
    public void Deactivate()
    {
        RemoveList(_listE);
        RemoveList(_listA);
        RemoveList(_listD);
        RemoveList(_listS);
    }



    // Get List
    public GameObject[] GetListE()
    {
        return _listE;
    }

    public GameObject[] GetListA()
    {
        return _listA;
    }

    public GameObject[] GetListD()
    {
        return _listD;
    }

    public GameObject[] GetListS()
    {
        return _listS;
    }
}
