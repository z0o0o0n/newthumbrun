﻿using UnityEngine;
using System.Collections;

public class Aircap : MonoBehaviour {

    public GameObject aircapParticlePrefab;
    public int particleCount;

    private GameObject[] _aircapParticleList;
	
    void Awake()
    {
        CreateAircapParticle();
    }

	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void CreateAircapParticle()
    {
        _aircapParticleList = new GameObject[particleCount];

        for(int i = 0; i < particleCount; i++)
        {
            GameObject particle = (GameObject)GameObject.Instantiate(aircapParticlePrefab);
            particle.transform.parent = transform;
            particle.transform.localPosition = Vector3.zero;
            particle.transform.localRotation = Random.rotation;
            particle.SetActive(false);

            _aircapParticleList[i] = particle;
        }
    }





    public void Explode()
    {
        for(int i = 0; i < particleCount; i++)
        {
            Rigidbody rigidbody = _aircapParticleList[i].GetComponent<Rigidbody>();
            rigidbody.isKinematic = false;
            rigidbody.WakeUp();
            _aircapParticleList[i].SetActive(true);
        }
    }



    // Reset
    public void Reset()
    {
        for (int i = 0; i < _aircapParticleList.Length; i++)
        {
            Rigidbody rigidbody = _aircapParticleList[i].GetComponent<Rigidbody>();
            rigidbody.Sleep();
            rigidbody.isKinematic = true;
            _aircapParticleList[i].transform.localPosition = Vector3.zero;
            _aircapParticleList[i].transform.localRotation = Random.rotation;
            _aircapParticleList[i].SetActive(false);
        }
    }
}
