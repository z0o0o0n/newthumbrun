﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using popupManager;
using System.Collections.Generic;

public class GachaScene : MonoBehaviour
{
    public Camera camera;
    public GachaSceneCamera cameraManager;
    public GachaMachine gachaMachine;
    public GachaSceneUI ui;
    public GachaPopupManager gachaPopupManager;
    public BgmManager bgmManager;

    private string _state;
    private int _gachaPrice;
    private GameManager _gameManager;
    //private LoadingShutter _loadingShutter;
    private OpenableCharacterBox _openableCharacterBox;

    void Awake()
    {
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        //bgmManager.Play(0);

        //GameManager.state = GameState.state.NONE;
        //_loadingShutter = GameObject.FindGameObjectWithTag("LoadingPage").GetComponent<LoadingShutter>();
        
        //_loadingShutter.On_LoadComplete += OnLoadCompleteScene;

        // 임시
        //GameData.instance.GetUserCollectionData().Reset_collectedCharacterIDList();
        //CharacterDataManager.instance.ClearNewCharacterIDList();
    }

    private void OnLoadCompleteScene()
    {
        // TODO: CheckGold 이름이 적합하지 않음
        if (!CheckGold()) OpenPopup(GachaPopupType.type.NoMoney);
    }

	void Start ()
    {
        _gameManager.sceneLoader.ShutterOpened += OnShutterOpened;
        _gameManager.sceneLoader.OpenShutter(0.6f);
        _gachaPrice = GameData.instance.GetConfigData().GachaPrice;
        ui.goldSpeechBubble.Show();
        InitGachaUI();

        if (gachaMachine.isReady) OnGachaMachineReady();
        else gachaMachine.On_Ready += OnGachaMachineReady;
	}

    private void OnShutterOpened()
    {
        //GameManager.state = GameState.state.GACHA;
    }

    private void InitGachaUI()
    {
        ui.HomeButtonClick += OnHomeButtonClick;
        ui.On_PressInfoPanelOKButton += OnPressInfoPanelOKButton;
        ui.goldDisplay.Show();
        ui.ShowHomeButton();
    }

    private void OnGachaMachineReady()
    {
        InitGachaMachine();
    }

    private void InitGachaMachine()
    {
        gachaMachine.On_PressGachaButton += OnPressGachaButton;
        gachaMachine.On_NoMoney += OnNoMoney;
        gachaMachine.Init();
        
        if(CheckGold()) gachaMachine.SetHasCoin();
    }

    private void OnHomeButtonClick()
    {
        bgmManager.Stop();
        ui.SetButtonActive(false);
        gachaMachine.DeactivateButton();
        _gameManager.sceneLoader.Load(SceneName.MAIN);
    }

    private void OnPressInfoPanelOKButton()
    {
        ui.HideCharacterInfoPanel();
        ReturnViewport();
        Invoke("ReturnToIdle", 0.3f);
        //ReturnToIdle();
    }

    private void ReturnToIdle()
    {
        _state = GachaSceneState.END_RETURN_TO_IDLE;

        gachaMachine.Reset();
        if (CheckGold()) gachaMachine.SetHasCoin();
        cameraManager.MoveGachaView(OnCompleteGachaViewMotion);
    }

    private void OnCompleteGachaViewMotion()
    {
        InitIdle();

        //GameManager.state = GameState.state.GACHA;
    }

    private void InitIdle()
    {
        _state = GachaSceneState.IDLE;
        ui.goldDisplay.Show();
        ui.ShowHomeButton(0.3f);
        ui.goldSpeechBubble.Show();
        _openableCharacterBox.Reset();
        gachaMachine.ActivateButton();
    }

    private void OnPressGachaButton()
    {
//        if (ui.goldDisplay.Spend(_gachaPrice))
//        {
//            GameManager.state = GameState.state.NONE;
//
//            Debug.Log("GachaScene - OnPressGachaButton()");
//            _openableCharacterBox = gachaMachine.openableCharacterBox;
//            AddOpenableCharacterBoxStateListener();
//            cameraManager.FollowCharacterBox(_openableCharacterBox.gameObject);
//            ui.HideHomeButton(0.3f);
//            ui.goldSpeechBubble.Hide();
//        }
//        else
//        {
//            // 팝업
//        }
    }

    private void AddOpenableCharacterBoxStateListener()
    {
        _openableCharacterBox.OnSleep += OnSleepCharacterBox;
        _openableCharacterBox.OnUnpacking += OnUnpackingCharacterBox;
        _openableCharacterBox.OnUnpacked += OnUnpackedCharacterBox;
    }

    private void RemoveOpenableCharacterBoxStateListener()
    {
        _openableCharacterBox.OnSleep -= OnSleepCharacterBox;
        _openableCharacterBox.OnUnpacking -= OnUnpackingCharacterBox;
        _openableCharacterBox.OnUnpacked -= OnUnpackedCharacterBox;
    }

    private void OnSleepCharacterBox()
    {
        cameraManager.StopFollowCharacterBox();
    }

    private void OnUnpackingCharacterBox()
    {
        ui.goldDisplay.Hide();
        cameraManager.PlayUnpackingMotion();
    }

    private void OnUnpackedCharacterBox()
    {
        RemoveOpenableCharacterBoxStateListener();

        GetSelectedCharacterData().isCollected = true;
        SaveNewCharacter(GetSelectedCharacterData().id);
        Debug.Log("---------------- RewardData: " + RewardDataManager.instance.GetRewardData().ToString());

        Invoke("OpenInfoPanel", 0.5f);

        GameData.instance.GetConfigData().gachaCount += 1;
        if(GameData.instance.GetConfigData().gachaCount == 10)
        {
        }
        else
        {
        }

        if(GameData.instance.GetConfigData().usedGacha == false)
        {
            GameData.instance.GetConfigData().usedGacha = true;
        }
        //OpenInfoPanel();
    }

    private void OpenInfoPanel()
    {
        CollectionData collectionData = CollectionDataManager.instance.GetCollectionDataByCharacterID(GetSelectedCharacterData().id);
        ui.OpenInfoPanel(GetSelectedCharacterData(), collectionData);

        MoveViewport();
    }

    private void MoveViewport()
    {
        float panelWidth = 320f;
        float tempValue = panelWidth / Screen.width;
        DOTween.To(() => camera.rect, x => camera.rect = x, new Rect(-tempValue, 0f, 1f, 1f), 0.5f).SetEase(Ease.InOutCubic);
    }

    private void ReturnViewport()
    {
        DOTween.To(() => camera.rect, x => camera.rect = x, new Rect(0, 0f, 1f, 1f), 0.3f).SetEase(Ease.OutQuad);
    }

    private CharacterData GetSelectedCharacterData()
    {
        CharacterData result;
        int selectedCharacterID = gachaMachine.GetSelectedCharacterID();
        result = GetCharacterData(selectedCharacterID);
        return result;
    }

    private CharacterData GetCharacterData(int characterID)
    {
        return CharacterDataManager.instance.GetCharacterDataByID(characterID);
    }

    private void SaveNewCharacter(int characterID)
    {
        //GameManager.socialManager.achievementManager.Report("CollectedCharacter_" + characterID, 100);
        #if UNITY_IOS
        //GameManager.achievementHandler.ReportAchievement("CollectedCharacter_" + characterID, 100);
        #endif

        CharacterDataManager.instance.PushNewCharacter(characterID);

        LogCharacterData(characterID);
        LogColletionData(characterID);
    }

    private void LogCharacterData(int characterID)
    {
        CharacterData characterData = CharacterDataManager.instance.GetCharacterDataByID(characterID);
        Debug.Log(characterData.ToString());
    }

    private void LogColletionData(int characterID)
    {
        CollectionData collectionData = CollectionDataManager.instance.GetCollectionDataByCharacterID(characterID);
        Debug.Log(collectionData.ToString());
    }

    private void OnNoMoney()
    {
        OpenPopup(GachaPopupType.type.NoMoney);
    }

    // Check Gold
    // 골드 잔액이 뽑기 가능한 금액인지 확인함
    private bool CheckGold()
    {
        if (ui.goldDisplay.GetGold() >= _gachaPrice) return true;
        else return false;
    }


    // Open Popup
    private void OpenPopup(GachaPopupType.type gachaPopupType)
    {
        gachaPopupManager.On_End += OnEndPopup;
        gachaPopupManager.OpenPopup(gachaPopupType);
        // 골드디스플레이 버튼 비활성화
        gachaMachine.DeactivateButton();// 가챠머신 작동중지
        ui.HideHomeButton(); // 홈버튼 숨기기
        ui.goldSpeechBubble.Hide();
        cameraManager.ActivateBlur(); // 화면 블러 처리
    }

    private void OnEndPopup()
    {
        gachaPopupManager.On_End -= OnEndPopup;

        // 골드디스플레이 버튼 활성화
        gachaMachine.ActivateButton(); // 가챠머신 작동재개
        ui.ShowHomeButton(); // 홈버튼 보이기
        ui.goldSpeechBubble.Show();
        cameraManager.DeactivateBlur(); // 화면 블러 취소
    }

    void OnDestroy()
    {
        //_loadingShutter.On_LoadComplete -= OnLoadCompleteScene;

        _gameManager.sceneLoader.ShutterOpened -= OnShutterOpened;

        gachaMachine.On_Ready -= OnGachaMachineReady;
        gachaMachine.On_PressGachaButton -= OnPressGachaButton;
        gachaMachine.On_NoMoney -= OnNoMoney;

        ui.HomeButtonClick -= OnHomeButtonClick;
        ui.On_PressInfoPanelOKButton -= OnPressInfoPanelOKButton;
    }

    public void GoHome()
    {
        OnHomeButtonClick();
    }
}
