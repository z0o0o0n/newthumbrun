﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GoldSpeechBubble : MonoBehaviour 
{
    public AnimationCurve customElastic;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void Show()
    {
        DOTween.Kill("GoldSpeechBubble");
        gameObject.transform.DOScale(1, 0.4f).SetId("GoldSpeechBubble").SetEase(customElastic);
    }

    public void Hide()
    {
        DOTween.Kill("GoldSpeechBubble");
        gameObject.transform.DOScale(0, 0.3f).SetId("GoldSpeechBubble").SetEase(Ease.InBack);
    }
}
