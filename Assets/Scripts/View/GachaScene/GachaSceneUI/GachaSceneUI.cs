﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Com.Mod.ThumbRun.User.View;

public class GachaSceneUI : MonoBehaviour 
{
    public delegate void GachaSceneUIEvent();
    public event GachaSceneUIEvent HomeButtonClick;
    public event GachaSceneUIEvent On_PressInfoPanelOKButton;

    public GoldDisplay goldDisplay;
    public GoldSpeechBubble goldSpeechBubble;
    public UIBasicButton homeButton;
    public CharacterInfoPanel characterInfoPanel;

    void Awake()
    {
        homeButton.On_Click += OnHomeButtonClick;
        HideHomeButton(0);
    }

	void Start () 
    {
	
	}

    public void OnHomeButtonClick()
    {
        if (HomeButtonClick != null) HomeButtonClick();
    }

    private void OnPressOKButton()
    {
        if (On_PressInfoPanelOKButton != null) On_PressInfoPanelOKButton();
    }

    public void OpenInfoPanel(CharacterData characterData, CollectionData collectionData)
    {
        characterInfoPanel.On_PressOKButton += OnPressOKButton;
        characterInfoPanel.SetData(characterData, collectionData, ButtonAreaType.type.OKButton, false);
        characterInfoPanel.Show(0.3f);
    }

    public void HideCharacterInfoPanel()
    {
        characterInfoPanel.On_PressOKButton -= OnPressOKButton;
        characterInfoPanel.Hide(0.3f);
    }



    public void ShowHomeButton(float time = 0.0f)
    {
        homeButton.transform.DOLocalMoveY(48, time).SetEase(Ease.OutCubic);
    }

    public void HideHomeButton(float time = 0.0f)
    {
        homeButton.transform.DOLocalMoveY(-50, time).SetEase(Ease.InCubic);
    }

    public void SetButtonActive(bool isActivation)
    {
        homeButton.SetButtonActive(isActivation);
    }

    void OnDestroy()
    {
        homeButton.On_Click -= OnHomeButtonClick;
    }
}
