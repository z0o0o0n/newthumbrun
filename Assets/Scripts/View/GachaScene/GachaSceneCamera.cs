﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GachaSceneCamera : MonoBehaviour 
{
    public delegate void OnCompleteDele();
    public OnCompleteDele moveGachaViewMotionCallback;

    public Vector3 gachaViewPosition;
    public Vector3 gachaViewRotation;
    public Vector3 characterBoxOpenViewPosition;
    public Vector3 characterBoxOpenViewRotation;

    private GameObject _target;
    private bool _isFollowCharacterBox = false;
    private Blur _blurEffect;
	
    void Awake()
    {
        _blurEffect = GetComponent<Blur>();
    }

	void Start () 
    {
	    
	}
	
	void FixedUpdate ()
    {
        if (_isFollowCharacterBox)
        {
            Vector3 targetPosition = _target.transform.localPosition;
            targetPosition.y += 6f;
            targetPosition.z += -8;
            transform.localPosition += (targetPosition - transform.localPosition) * 0.1f;
        }
	}

    public void MoveGachaView(OnCompleteDele callback)
    {
        moveGachaViewMotionCallback = callback;

        StopFollowCharacterBox();
        transform.DOLocalMove(gachaViewPosition, 1f).SetEase(Ease.InOutCubic);
        transform.DOLocalRotate(gachaViewRotation, 1f).SetEase(Ease.InOutCubic).OnComplete(OnCompleteGachaViewMotion);
    }

    private void OnCompleteGachaViewMotion()
    {
        moveGachaViewMotionCallback();
    }

    public void MoveCharacterBoxOpenView()
    {
        StopFollowCharacterBox();
    }

    public void FollowCharacterBox(GameObject target)
    {
        _target = target;
        _isFollowCharacterBox = true;

        Vector3 targetRotation = new Vector3(35, 0, 0);
        transform.DOLocalRotate(targetRotation, 1f);
    }

    public void StopFollowCharacterBox()
    {
        _isFollowCharacterBox = false;
    }

    public void PlayUnpackingMotion()
    {
        transform.DOLocalMove(new Vector3(transform.localPosition.x, 2.8f, transform.localPosition.z + 4.5f), 2).SetEase(Ease.OutQuart);
        transform.DOLocalMove(new Vector3(transform.localPosition.x, 4.3f, transform.localPosition.z + 2.5f), 1).SetEase(Ease.OutElastic).SetDelay(2);
    }

    public void ActivateBlur()
    {
        _blurEffect.enabled = true;
    }

    public void DeactivateBlur()
    {
        _blurEffect.enabled = false;
    }
}
