﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Junhee.Utils;
using System.Collections.Generic;

public class OpenableCharacterBox : MonoBehaviour
{
    public delegate void CharacterBoxEvent();
    public event CharacterBoxEvent OnSleep;
    public event CharacterBoxEvent OnUnpacking;
    public event CharacterBoxEvent OnUnpacked;

    public GameObject top;
    public GameObject bottom;
    public GameObject left;
    public GameObject right;
    public GameObject front;
    public GameObject back;
    public Aircap aircap;
    public ColliderButton colliderButton;
    public AudioSource audioSource;
    public AudioClip drumRoll;
    public AudioClip hitSound;
    public AudioClip unpackSound;

    private bool _isCheckSleep = false;
    private GameObject[] _surfaceList;
    private GameObject _selectedCharacter;
    private Bouncer _bouncer;

    void Awake()
    {
        _bouncer = GetComponent<Bouncer>();

        InitSurfaceList();
        Close(0);
    }

    private void InitSurfaceList()
    {
        _surfaceList = new GameObject[6];
        _surfaceList[0] = top;
        _surfaceList[1] = bottom;
        _surfaceList[2] = left;
        _surfaceList[3] = right;
        _surfaceList[4] = front;
        _surfaceList[5] = back;
    }

    void FixedUpdate()
    {
        if (_isCheckSleep)
        {
            if (GetComponent<Rigidbody>().IsSleeping())
            {
                _isCheckSleep = false;
                Sleep();
            }
        }
    }

    void OnDestroy()
    {
    }

    void OnCollisionEnter()
    {
        if(_isCheckSleep)
        {
            audioSource.clip = hitSound;
            audioSource.Play();
        }
    }


    // On Sleep
    private void Sleep()
    {
        if (OnSleep != null) OnSleep();

        colliderButton.On_ButtonUp += OnButtonUpOpenableBox;
        colliderButton.Activate();
        Bounce();
    }

    // On Button Up Openable Box
    private void OnButtonUpOpenableBox()
    {
        StopBounce();

        colliderButton.On_ButtonUp -= OnButtonUpOpenableBox;
        Unpack(1);
    }

    private float openBoxTime = 1;
    private void Unpack(float time)
    {
        if (OnUnpacking != null) OnUnpacking();

        openBoxTime = time;
        transform.DOLocalRotate(Vector3.zero, 2).SetEase(Ease.OutQuart).OnComplete(OnCompleteOpenReadyMotion);
        Shaker shaker = GetComponent<Shaker>();
        shaker.Start(null);

        audioSource.clip = drumRoll;
        audioSource.Play();
    }

    private void OnCompleteOpenReadyMotion()
    {
        top.transform.DOLocalRotate(new Vector3(-5, 0, 0), openBoxTime).SetEase(Ease.OutElastic);
        bottom.transform.DOLocalRotate(new Vector3(5, 0, 0), openBoxTime).SetEase(Ease.OutElastic);
        left.transform.DOLocalRotate(new Vector3(0, 0, -5), openBoxTime).SetEase(Ease.OutElastic);
        front.transform.DOLocalRotate(new Vector3(0, 0, -10), openBoxTime).SetEase(Ease.OutElastic);
        right.transform.DOLocalRotate(new Vector3(0, 0, 5), openBoxTime).SetEase(Ease.OutElastic);

        GetComponent<Collider>().enabled = false;

        aircap.Explode();
        Explode();

        if (OnUnpacked != null) OnUnpacked();
    }



    // Bounce
    private void Bounce()
    {
        GetComponent<Rigidbody>().isKinematic = true;

        _bouncer.Start(gameObject, 2, -1);
    }

    private void StopBounce()
    {
        _bouncer.Stop();
    }



    // Change Texture
    private void ChangeTexture(CharacterType.type type)
    {
        for (int i = 0; i < _surfaceList.Length; i++)
        {
            TPMeshTextureEx tpMesh = _surfaceList[i].GetComponent<TPMeshTextureEx>();
            tpMesh.texture = "CharacterBox" + type + ".psd";
            tpMesh.UpdateView();
        }
    }

    // Explode
    private void Explode()
    {
        //audioSource.clip = unpackSound;
        //audioSource.Play();

        InitSelectedCharacter();
    }

    private void InitSelectedCharacter()
    {
        _selectedCharacter.SetActive(true);
        _selectedCharacter.transform.parent = transform;
        _selectedCharacter.transform.localPosition = new Vector3(0, -0.25f, 0);
        _selectedCharacter.transform.localRotation = Quaternion.Euler(new Vector3(0, 25, 0));
        _selectedCharacter.transform.localScale = new Vector3(1.4f, 1.4f, 1.4f);
    }





    // Init
    public void Init(GameObject characterPrefab)
    {
        Character character = characterPrefab.GetComponent<Character>();
        if (character == null) Debug.LogError("OpenableCharacterBox - Init() / Character prefab의 Character 컴포넌트가 Null입니다.");
        CharacterData characterData = CharacterDataManager.instance.GetCharacterDataByID(character.id);
        if (characterData == null) Debug.LogError("OpenableCharacterBox - Init() / id: " + character.id + "의 Character Data가 Null입니다.");

        gameObject.SetActive(true); // 현재 객체 활성화
        colliderButton.Deactivate(); // 콜라이더 버튼 비활성화
        InitPostion(); // 위치 초기화
        ChangeTexture(characterData.type); // 박스의 텍스쳐를 선택된 타입으로 변경
        CreateCharacter(characterPrefab); // 선택된 캐릭터를 생성
    }

    private void InitPostion()
    {
        transform.localPosition = new Vector3(-0.5f, 0.75f, -0.75f);
        transform.localRotation = Quaternion.Euler(new Vector3(0, 90, 45));
    }

    private void CreateCharacter(GameObject characterPrefab)
    {
        _selectedCharacter = (GameObject)GameObject.Instantiate(characterPrefab);
        _selectedCharacter.SetActive(false);
    }



    // Activate Sleep Check
    public void ActivateSleepCheck()
    {
        _isCheckSleep = true;
    }



    public GameObject GetSelectedCharacter()
    {
        return _selectedCharacter;
    }



    // Reset
    public void Reset()
    {
        GetComponent<Collider>().enabled = true;
        GetComponent<Rigidbody>().isKinematic = false;
        gameObject.SetActive(false);
        InitPostion(); // 위치 초기화
        Close(0); // 박스닫기
        GameObject.Destroy(_selectedCharacter); // 캐릭터 삭제
        aircap.Reset(); // 에어캡 리셋
    }

    private void Close(float time)
    {
        top.transform.DOLocalRotate(new Vector3(-90, 0, 0), time);
        bottom.transform.DOLocalRotate(new Vector3(90, 0, 0), time);
        left.transform.DOLocalRotate(new Vector3(0, 0, -90), time);
        front.transform.DOLocalRotate(new Vector3(0, 0, -90), time);
        right.transform.DOLocalRotate(new Vector3(0, 0, 90), time);
    }
}
