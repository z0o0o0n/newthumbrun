﻿using DG.Tweening;
using System.Collections;
using UnityEngine;

public class GachaButton : MonoBehaviour
{
    public delegate void GachaBtnEvent();
    public event GachaBtnEvent On_Press;

    public ColliderButton colliderButton;

    private bool _isPushed = false;
    private bool _isActivate = true;

    void Awake()
    {
        colliderButton.On_ButtonUp += OnButtonUp;
    }

	void Start ()
    {
        	
	}

    void OnEnable()
    {
        //EasyTouch.On_TouchStart += OnTouchStart;
        //EasyTouch.On_TouchDown += OnTouchDown;
        //EasyTouch.On_TouchUp += OnTouchUp;
    }

    void OnDisalbe()
    {
        //EasyTouch.On_TouchStart -= OnTouchStart;
        //EasyTouch.On_TouchDown -= OnTouchDown;
        //EasyTouch.On_TouchUp -= OnTouchUp;
    }

    void OnDestroy()
    {
        colliderButton.On_ButtonUp -= OnButtonUp;
    }




    private void OnButtonUp()
    {
        if (On_Press != null) On_Press();
        PlayPushMotion();
    }

    //void Update()
    //{
    //    if(_isActivate)
    //    {
    //        if (Input.GetMouseButtonUp(0))
    //        {
    //            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

    //            RaycastHit hit;
    //            if (Physics.Raycast(ray, out hit, 100.0f))
    //            {
    //                Debug.DrawLine(ray.origin, hit.point); // ray를 가시화함
    //                if (hit.collider.gameObject.tag == "GachaButton")
    //                {
    //                    if (On_Press != null) On_Press();
    //                    PlayPushMotion();
    //                }
    //            }
    //        }
    //    }
    //}

    //private void OnTouchStart(Gesture gesture)
    //{
    //    if (!_isPushed)
    //    {
    //        Ray ray = Camera.main.ScreenPointToRay(gesture.position);
    //        RaycastHit hit;
    //        if (gameObject.collider.Raycast(ray, out hit, 100.0f))
    //        {
    //            //_isPushed = true;
    //            if(OnPush != null) OnPush();
    //            PlayPushMotion();
    //            Debug.Log("Gacha Btn Touch Start");
    //        }
    //    }
    //}

    //private void OnTouchDown(Gesture gesture)
    //{
    //    //Debug.Log("Touch Down");
    //}

    //private void OnTouchUp(Gesture gesture)
    //{
    //    //Debug.Log("Touch Up");
    //}

    private void PlayPushMotion()
    {
        // 눌렸다 빠지는 버튼
        transform.localPosition = new Vector3(transform.position.x, transform.position.y, -1.0f);

        transform.DOLocalMove(new Vector3(transform.position.x, transform.position.y, -1.3f), 0.2f).SetDelay(0.05f);
        //HOTween.To(transform, 0.2f, new TweenParms()
        //    .Delay(0.05f)
        //    .Prop("position", new Vector3(transform.position.x, transform.position.y, -1.3f))
        //    .OnComplete(OnCompletePushMotion)
        //    );
    }

    //private void OnCompletePushMotion(TweenEvent tweenEvent)
    //{
    //    Debug.Log("On Complete Tween Motion");
    //}
    
    public void Activate()
    {
        _isActivate = true;
        colliderButton.Activate();
    }

    public void Deactivate()
    {
        _isActivate = false;
        colliderButton.Deactivate();
    }
}
