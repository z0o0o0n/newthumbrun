﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterBoxSelector : MonoBehaviour 
{
    public Vector3 pickerPosition = new Vector3(-0.5f, 1.5f, -1f);

    private CharacterSelector _characterSelector;
    private CharacterBox _selectedBox;
    private int _selectedCharacterID;

    void Awake()
    {
        _characterSelector = GetComponent<CharacterSelector>();
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}



    // Select
    public void Select(List<GameObject> boxList)
    {
        _selectedBox = null;
        SelectCharacterBox(boxList);
        List<int> characterRandomnessList = null;
        SelectCharacter(characterRandomnessList);
    }

    private void SelectCharacterBox(List<GameObject> boxList)
    {
        float shortDistance = 0.0f;
        bool hasSelectedBox = false;
        int selectedIndex = 0;

        for (int i = 0; i < boxList.Count; i++)
        {
            CharacterBox box = boxList[i].GetComponent<CharacterBox>();
            float distance = Vector3.Distance(pickerPosition, boxList[i].transform.localPosition);

            if (!_selectedBox)
            {
                hasSelectedBox = true;
                selectedIndex = i;
                _selectedBox = boxList[i].GetComponent<CharacterBox>();
                shortDistance = distance;
            }
            else
            {
                if (shortDistance > distance)
                {
                    hasSelectedBox = true;
                    selectedIndex = i;
                    _selectedBox = boxList[i].GetComponent<CharacterBox>();
                    shortDistance = distance;
                }
            }
        }
        if (hasSelectedBox)
        {
            boxList.RemoveAt(selectedIndex);
        }
    }

    private void SelectCharacter(List<int> lotteryData)
    {
        _characterSelector.Select(lotteryData);
        _selectedCharacterID = _characterSelector.GetSelectedCharacterID();
    }



    public CharacterBox GetSelectedBox()
    {
        return _selectedBox;
    }

    public int GetSelectedCharacterID()
    {
        return _selectedCharacterID;
    }
}
