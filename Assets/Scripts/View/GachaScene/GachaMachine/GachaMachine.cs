﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using Junhee.Utils;

public class GachaMachine : MonoBehaviour
{
    public delegate void GachaMachineEvent();
    public GachaMachineEvent On_Ready;
    public GachaMachineEvent On_PressGachaButton;
    public GachaMachineEvent On_NoMoney;
   
    public GachaButton button;
    public OpenableCharacterBox openableCharacterBox;
    public AudioSource audioSource;
    public AudioClip shotSound;

    private bool _isReady = false;
    private bool _hasCoin = false;
    private int _selectedCharacterID;
    private CharacterBox _selectedBox;
    private GachaGyro _gachaGyro;
    private CharacterBoxCreator _characterBoxCreator;
    private CharacterBoxDeployer _characterBoxDeployer;
    private CharacterBoxSelector _characterBoxSelector;
    private CharacterLotteryDataCreator _characterLotteryDataCreator;
    private CharacterBoxThrower _characterBoxThrower;

    public bool isReady
    {
        get
        {
            return _isReady;
        }
    }

    // Awake
	void Awake () 
    {
        _gachaGyro = gameObject.GetComponent<GachaGyro>();
        _characterBoxCreator = GetComponent<CharacterBoxCreator>();
        _characterBoxDeployer = GetComponent<CharacterBoxDeployer>();
        _characterBoxSelector = GetComponent<CharacterBoxSelector>();
        _characterLotteryDataCreator = GetComponent<CharacterLotteryDataCreator>();
        _characterBoxThrower = GetComponent<CharacterBoxThrower>();

        _characterBoxCreator.On_Ready += OnCharacterBoxCreatorReady;
	}

    // Character Box Creator 준비 완료
    private void OnCharacterBoxCreatorReady()
    {
        _isReady = true;
        if (On_Ready != null) On_Ready();
    }

    // Destroy
    void OnDestroy()
    {
        _gachaGyro.Deactivate();
        _characterBoxCreator.On_Ready -= OnCharacterBoxCreatorReady;

        button.On_Press -= OnPressButton;
    }

    // Init
    public void Init()
    {
        _gachaGyro.Activate();
        _characterBoxCreator.CreateCharacterBoxs(); // 캐릭터 박스 생성
        _characterBoxDeployer.Deploy(_characterBoxCreator.GetBoxList()); // 캐릭터 박스 배치

        button.On_Press += OnPressButton;
    }

    private void OnPressButton()
    {
        if (_hasCoin)
        {
            _hasCoin = false;
            DeactivateButton();
            _gachaGyro.Deactivate();
            SelectCharacterBox();
            InitOpenableCharacterBox(); // 오픈박스 초기화
            _characterBoxThrower.Throw(openableCharacterBox); // 박스던짐장치를 통해 오픈박스를 던짐
            openableCharacterBox.ActivateSleepCheck(); // 오픈박스의 슬립체크를 활성화함

            audioSource.clip = shotSound;
            audioSource.Play();

            if (On_PressGachaButton != null) On_PressGachaButton();
        }
        else
        {
            if (On_NoMoney != null) On_NoMoney();
        }
    }

    private void SelectCharacterBox()
    {
        _characterBoxSelector.Select(_characterBoxCreator.GetBoxList());
        _selectedBox = _characterBoxSelector.GetSelectedBox();
        _selectedCharacterID = _characterBoxSelector.GetSelectedCharacterID();

        _selectedBox.gameObject.SetActive(false); // 선택 된 박스 비활성화
    }

    private void InitOpenableCharacterBox()
    {
		GameObject selectedCharacterPrefab = CharacterPrefabProvider.instance.Get(_selectedCharacterID);
        Debug.Log("GachaMachine - InitOpenableCharacterBox() / selectedCharacterID: " + _selectedCharacterID + " / selectedCharacterPrefab: " + selectedCharacterPrefab);
        if (selectedCharacterPrefab == null) Debug.LogError("GachaMachine - InitOpenableCharacterBox() / 선택된 캐릭터가 Null일 수 없습니다.");
        openableCharacterBox.Init(selectedCharacterPrefab);
    }
	
    public void SetHasCoin()
    {
        _hasCoin = true;
    }
	
    public int GetSelectedCharacterID()
    {
        return _selectedCharacterID;
    }

    // Get Selected Character Type
    public CharacterType.type GetSelectedCharacterType()
    {
        return _selectedBox.type;
    }

    // Reset
    public void Reset()
    {
        button.On_Press -= OnPressButton;

        Init();
    }

    public void ActivateButton()
    {
        button.Activate();
    }

    public void DeactivateButton()
    {
        button.Deactivate();
    }
}
