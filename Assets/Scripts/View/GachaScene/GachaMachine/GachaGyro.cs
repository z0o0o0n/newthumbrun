﻿using UnityEngine;
using System.Collections;

public class GachaGyro : MonoBehaviour
{
    private bool _isActivate = false;

	void Start ()
    {
	}

    float xpos = 0.01f;
    float ypos = 0.01f;
	void Update ()
    {
        if (_isActivate)
        {
            #if UNITY_EDITOR
            #else 
            xpos = Input.acceleration.x * 20.0f;
            ypos = Input.acceleration.y * 20.0f;

            Physics.gravity = new Vector3(xpos, ypos, 0.0f);
            #endif
        }
    }

    public void Activate()
    {
        _isActivate = true;
    }

    public void Deactivate()
    {
        _isActivate = false;
        Physics.gravity = new Vector3(0, -20, 0);
    }
}
