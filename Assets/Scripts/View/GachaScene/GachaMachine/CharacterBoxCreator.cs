﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterBoxCreator : MonoBehaviour
{
    public delegate void BoxCreatorEvent();
    public event BoxCreatorEvent On_Ready;

    public CharacterBoxPool characterBoxPool;
    public int probabilityTypeS = 4;
    public int probabilityTypeD = 2;

    private List<GameObject> _boxList = new List<GameObject>();

    void Awake()
    {
        if (characterBoxPool.isReady) OnCharacterBoxPoolReady();
        else characterBoxPool.On_Ready += OnCharacterBoxPoolReady;
    }

    void Start()
    {

    }

    void Update()
    {

    }

    void OnDestroy()
    {
        characterBoxPool.On_Ready -= OnCharacterBoxPoolReady;
    }



    private void OnCharacterBoxPoolReady()
    {
        if (On_Ready != null) On_Ready();
    }



    // Create Character Box
    public bool CreateCharacterBoxs()
    {
        if (characterBoxPool.isReady)
        {
            _boxList = new List<GameObject>(); // BoxList 초기화

            CreateBox(characterBoxPool.GetListE(), 5);
            CreateBox(characterBoxPool.GetListA(), 5);
            CreateBox(characterBoxPool.GetListD(), 1);
            //if (Random.Range(0, probabilityTypeD) == 0) CreateBox(characterBoxPool.GetListD(), 4);
            //else CreateBox(characterBoxPool.GetListD(), 2);
            //if (Random.Range(0, probabilityTypeS) == 0) CreateBox(characterBoxPool.GetListS(), 2);
            return true;
        }
        else
        {
            return false;
        }
    }

    private void CreateBox(GameObject[] boxList, int count)
    {
        if (count > boxList.Length) count = boxList.Length;

        for (int i = 0; i < count; i++)
        {
            _boxList.Add(boxList[i]);
        }
    }



    // Get Box List
    public List<GameObject> GetBoxList()
    {
        return _boxList;
    }
}
