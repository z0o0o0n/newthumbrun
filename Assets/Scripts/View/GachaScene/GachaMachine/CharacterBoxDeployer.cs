﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterBoxDeployer : MonoBehaviour 
{
    public float creationY = 2.0f;
    public float creationRange = 0.6f;

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}




    public void Deploy(List<GameObject> boxList)
    {
        for (int i = 0; i < boxList.Count; i++)
        {
            GameObject box = boxList[i];
            box.transform.parent = transform;
            box.transform.localPosition = new Vector3(Random.Range(-creationRange, creationRange), Random.Range(creationY - 0.5f, creationY + 0.5f), Random.Range(-creationRange, creationRange));
            box.transform.rotation = Random.rotation;
            box.SetActive(true);
        }
    }
}
