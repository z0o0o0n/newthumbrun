﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GachaPopupData
{
    public GachaPopupType.type type;
    public Color bgColor;
}
