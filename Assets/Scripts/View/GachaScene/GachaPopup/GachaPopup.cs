﻿namespace popupManager
{
    using UnityEngine;
    using System.Collections;
    using Junhee.Utils;
    using DG.Tweening;
    using popupManager;

    public class GachaPopup : BasePopup
    {
        [SerializeField] private UISprite bg;
        [SerializeField] private UISprite button;
        [SerializeField] private UISprite content;

        public void OnPressOKButton()
        {
            button.enabled = false;
            Close(0.2f);
        }


        public void Init(GachaPopupType.type gachaPopupType, Color color)
        {
            content.spriteName = "Popup_" + gachaPopupType.ToString();
            bg.color = color;
            gameObject.transform.localScale = Vector3.zero;
        }


        public override void Open(float time)
        {
            gameObject.SetActive(true);
            gameObject.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
            gameObject.transform.DOScale(Vector3.one, time).SetEase(Ease.OutBack);
        }


        public override void Close(float time)
        {
            gameObject.transform.DOScale(Vector3.zero, time).SetEase(Ease.InBack).OnComplete(OnCompleteClose);
        }

        private void OnCompleteClose()
        {
            gameObject.SetActive(false);
            //GameObject.Destroy(gameObject);

            this.EndClose();
        }
    }
}