﻿using UnityEngine;
using System.Collections;
using popupManager;

public class GachaPopupManager : MonoBehaviour 
{
    public delegate void GachaPopupMnagerEvent();
    public event GachaPopupMnagerEvent On_End;

    public bool isDebug = true;
    private PopupManager _popupManager;
    private GachaPopupCreator _gachaPopupCreator;

    void Awake()
    {
        _popupManager = GetComponent<PopupManager>();
        _gachaPopupCreator = GetComponent<GachaPopupCreator>();
    }


	void Start () 
    {
	
	}
	

	void Update () 
    {
	
	}


    // OpenPopup
    public void OpenPopup(GachaPopupType.type gachaPopupType)
    {
        if (isDebug) Debug.Log("GachaPopupManager.OpenPopup() - type : " + gachaPopupType);
        BasePopup popup = _gachaPopupCreator.Create(gachaPopupType); // Popup 생성
        _popupManager.PushPopupToCueue(popup); // Popup 대기열에 추가
        Open();
    }

    private void Open()
    {
        if (isDebug) Debug.Log("GachaPopupManager.Open()");
        _popupManager.On_End += OnEndPopupManager;
        _popupManager.Open();
    }

    private void OnEndPopupManager()
    {
        _popupManager.On_End -= OnEndPopupManager;
        if (On_End != null) On_End();
    }
}
