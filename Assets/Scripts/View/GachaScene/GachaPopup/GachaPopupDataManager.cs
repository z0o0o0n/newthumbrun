﻿using UnityEngine;
using System.Collections;

public class GachaPopupDataManager : MonoBehaviour 
{
    public GachaPopupData[] gachaPopupData;

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}

    public GachaPopupData GetDataByType(GachaPopupType.type type)
    {
        for (int i = 0; i < gachaPopupData.Length; i++ )
        {
            if(gachaPopupData[i].type == type)
            {
                return gachaPopupData[i];
            }
        }
        return null;
    }
}
