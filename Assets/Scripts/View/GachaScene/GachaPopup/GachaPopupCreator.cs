﻿using UnityEngine;
using System.Collections;
using popupManager;

public class GachaPopupCreator : MonoBehaviour 
{
    public GameObject gachaPopupObject;
    public GameObject popupContainer;
    public GachaPopupDataManager gachaPopupDataManager;

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}





    // Create
    public GachaPopup Create(GachaPopupType.type gachaPopupType)
    {
        GameObject basicPopupInstance = GameObject.Instantiate(gachaPopupObject) as GameObject; // BasicPopup의 GameObject 인스턴스 생성
        // TODO: GachaPopupData 기반으로 인스턴스를 생성한 뒤 Return만 하면 될 것 같은데, 새성 후 Container 배치를 여기서 하는게 좋은지 고민
        basicPopupInstance.transform.parent = popupContainer.transform; // 컨테이너 설정
        basicPopupInstance.transform.localPosition = Vector3.zero;

        // TODO: GetPopup의 파라미터로 GachaPopupData를 받는 방법이 더 좋은지 고민
        GachaPopupData popupData = gachaPopupDataManager.GetDataByType(gachaPopupType); // gachaPopupDataManager를 통해 Data 가져옴

        GachaPopup gachaPopup = basicPopupInstance.GetComponent<GachaPopup>(); // BasicPopup Script Component 가져옴
        gachaPopup.Init(gachaPopupType, popupData.bgColor); // BasicPopup 초기화

        return gachaPopup;
    }
}
