﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms.GameCenter;
using Com.Mod.ThumbRun.User.View;

public class TestPanel : MonoBehaviour 
{
    public List<LabelButton> testButtonList;
    public UISprite bg;
    public GoldDisplay goldDisplay;
    public UILabel controlModeLabel;

    private bool _isShow = false;
    private bool _hasSponsor = true;
    private ADManager _adManager;
    private bool _isSimpleControlMode = true;

    void Awake()
    {
        HideButtons();
    }

    public void Init(ADManager adManager)
    {
        _adManager = adManager;
        _adManager.SetRewardAdDebug(_hasSponsor);
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void OnChangeControlMode()
    {
        if(GameManager.isSimpleControlMode)
        {
            GameManager.isSimpleControlMode = false;
            controlModeLabel.text = "current control mode : B";
        }
        else
        {
            GameManager.isSimpleControlMode = true;
            controlModeLabel.text = "current control mode : A";
        }
    }

    public void OnChangeSponsorTest()
    {
        if(_hasSponsor)
        {
            _hasSponsor = false;
            testButtonList[1].label.text = "[ Has Sponsor ] OFF";
            testButtonList[1].label.color = Color.white;
            testButtonList[1].sprite.color = Color.black;
            _adManager.SetRewardAdDebug(false);
        }
        else
        {
            _hasSponsor = true;
            testButtonList[1].label.text = "[ Has Sponsor ] ON";
            testButtonList[1].label.color = Color.black;
            testButtonList[1].sprite.color = Color.white;
            _adManager.SetRewardAdDebug(true);
        }
    }

    public void OnLevelUpPointButtonPressed()
    {
        Debug.Log("level up point pressed");
        BonusAbilityModel.instance.levelUpPoint += 1;
    }

    public void OnPressOpenButton()
    {
        if (_isShow) HideButtons();
        else ShowButtons();
    }

    public void OnPressAddGoldButton()
    {
        //goldDisplay.Save(10000);
    }

    private void ShowButtons()
    {
        _isShow = true;
        for (int i = 0; i < testButtonList.Count; i++)
        {
            testButtonList[i].gameObject.SetActive(true);
        }

        bg.gameObject.SetActive(true);
    }

    private void HideButtons()
    {
        _isShow = false;
        for (int i = 0; i < testButtonList.Count; i++)
        {
            testButtonList[i].gameObject.SetActive(false);
        }

        bg.gameObject.SetActive(false);
    }

    public void OnTutorialReset()
    {
        GameData.instance.GetConfigData().IsFirstTime = true;
        GameData.instance.GetConfigData().IsFirstTimeMain = true;
        GameData.instance.GetConfigData().IsFirstTimeGacha = true;
        GameData.instance.GetConfigData().IsFirstTimeCollection = true;
        GameData.instance.GetConfigData().IsFirstTimePlay = true;
    }

    public void OnResetAchivmentData()
    {
        //GameManager.achievementHandler.ResetAchievement();
        //GameCenterPlatform.ResetAllAchievements(OnCompletedAchievementReset);
    }

    private void OnCompletedAchievementReset(bool success)
    {

    }

    public void OnBonusAbilityReset()
    {
        // GameData.instance.GetBonusAbilityData().ResetAll();
    }

    public void OnPressResetCollectionData()
    {
        //GameData.instance.GetConfigData().Reset_SelectedCharacterID();

        //GameData.instance.GetUserCollectionData().Reset_collectedCharacterIDList();
        //GameData.instance.GetUserCollectionData().Reset_completedCollectionIDList();
        //GameData.instance.GetUserCollectionData().Reset_newCharacterIDList();
        //GameData.instance.GetUserCollectionData().Reset_newCompletedCollectionIDList();

        //GameData.instance.GetCharacterDataList().Reset_list();
        //GameData.instance.GetCollectionDataList().Reset_list();

        //GameData.instance.GetGlobalRewardData().ResetAll();

        GameData.instance.ClearAllData();
    }
}
