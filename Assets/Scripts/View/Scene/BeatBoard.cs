﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Controller;
using Com.Mod.ThumbRun.Data;

namespace Com.Mod.ThumbRun.View
{
    public class BeatBoard : MonoBehaviour
    {
        [SerializeField]
        private BBBooster _bbBooster;
        [SerializeField]
        private BBDirection.type _direction = BBDirection.type.LEFT;

        void Start()
        {
        }

        void Update()
        {

        }



        #region Private Functions
        private void OnTriggerEnter(Collider collider)
        {
            if (collider.tag == "Player")
            {
                _bbBooster.EnterPlayer(_direction);
            }
        }

        private void OnTriggerExit(Collider collider)
        {
            if (collider.tag == "Player")
            {
                _bbBooster.ExitPlayer(_direction);
            }
        }
        #endregion



        #region Public Functions
        public void SetBBBooster(BBBooster bbBooster)
        {
            _bbBooster = bbBooster;
        }
        #endregion
    }
}