﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Controller;
using DG.Tweening;

namespace Com.Mod.ThumbRun.View
{
    public class WJBoostCircle : MonoBehaviour
    {
        public SpriteRenderer coreEffect;
        public SpriteRenderer outCircle0;
        public SpriteRenderer outCircle1;

        [Header("Core Circle")]
        public float timeAlpha0 = 0.6f;
        public float timeScale0 = 0.6f;
        public float delay0 = 0f;
        public float startScale0 = 0.1f;
        public float scale0 = 0f;

        [Header("Out Circle A")]
        public float timeAlpha1 = 0.6f;
        public float timeScale1 = 0.6f;
        public float delay1 = 0.05f;
        public float startScale1 = 0.2f;
        public float scale1 = 0.4f;

        [Header("Out Circle B")]
        public float timeAlpha2 = 0.6f;
        public float timeScale2 = 0.6f;
        public float delay2 = 0.1f;
        public float startScale2 = 0.3f;
        public float scale2 = 0.3f;

        public AnimationCurve curveAlpha;
        public AnimationCurve curveScale;

        private float loopTime = 2f;

        void Awake()
        {
            Hide(0f);
        }

        void Start()
        {
            //StartLoop();
        }

        //private void StartLoop()
        //{
        //    OnBoost(new Vector2(0, 0));
        //    DOTween.To(() => loopTime, x => loopTime = x, loopTime, 2f).OnComplete(ResetLoop);
        //}

        //private void ResetLoop()
        //{
        //    Hide(0f);
        //    DOTween.To(() => loopTime, x => loopTime = x, loopTime, 0.5f).OnComplete(StartLoop);
        //}

        void Update()
        {
            //if(Input.GetKeyUp(KeyCode.Alpha1))
            //{
            //    OnBoost(new Vector2(0,0.5f));
            //}
            //else if (Input.GetKeyUp(KeyCode.Alpha2))
            //{
            //    Hide(0f);
            //}
        }

        void OnDestroy()
        {

        }



        #region Private Functions
        public void ShowEffect()
        {
            Show(0.6f);
        }

        private void Show(float time)
        {
            KillTweens();

            Color color = coreEffect.color;
            color.a = 1.0f;
            coreEffect.color = color;
            coreEffect.DOFade(0f, timeAlpha0).SetId("coreEffect.DOFade." + GetInstanceID()).SetDelay(delay0).SetEase(curveAlpha);
            coreEffect.transform.DOScale(scale0, timeScale0).SetId("coreEffect.transform.DOScale." + GetInstanceID()).SetDelay(delay0).SetEase(curveScale);

            color = outCircle0.color;
            color.a = 0.2f;
            outCircle0.color = color;
            outCircle0.DOFade(0f, timeAlpha1).SetId("outCircle0.DOFade." + GetInstanceID()).SetDelay(delay1).SetEase(curveAlpha);
            outCircle0.transform.DOScale(scale1, timeScale1).SetId("outCircle0.transform.DOScale" + GetInstanceID()).SetDelay(delay1).SetEase(curveScale);

            color = outCircle1.color;
            color.a = 0.2f;
            outCircle1.color = color;
            outCircle1.DOFade(0f, timeAlpha2).SetId("outCircle1.DOFade" + GetInstanceID()).SetDelay(delay2).SetEase(curveAlpha);
            outCircle1.transform.DOScale(scale2, timeScale2).SetId("outCircle1.transform.DOScale" + GetInstanceID()).SetDelay(delay2).SetEase(curveScale).OnComplete(OnComplete);
        }

        private void OnComplete()
        {
            GameObject.Destroy(this.gameObject);
        }

        private void Hide(float time)
        {
            KillTweens();

            coreEffect.DOFade(0f, time).SetId("coreEffect.DOFade." + GetInstanceID());
            coreEffect.transform.DOScale(startScale0, time).SetId("coreEffect.transform.DOScale." + GetInstanceID());

            outCircle0.DOFade(0f, time).SetId("outCircle0.DOFade." + GetInstanceID());
            outCircle0.transform.DOScale(startScale1, time).SetId("outCircle0.transform.DOScale" + GetInstanceID());

            outCircle1.DOFade(0f, time).SetId("outCircle1.DOFade" + GetInstanceID());
            outCircle1.transform.DOScale(startScale2, time).SetId("outCircle1.transform.DOScale" + GetInstanceID());
        }

        private void KillTweens()
        {
            DOTween.Kill("coreEffect.DOFade." + GetInstanceID());
            DOTween.Kill("coreEffect.transform.DOScale." + GetInstanceID());
            DOTween.Kill("outCircle0.DOFade." + GetInstanceID());
            DOTween.Kill("outCircle0.transform.DOScale" + GetInstanceID());
            DOTween.Kill("outCircle1.DOFade" + GetInstanceID());
            DOTween.Kill("outCircle1.transform.DOScale" + GetInstanceID());
        }
        #endregion



        #region Public Functions
        public void ChangeColor(Color color)
        {
            Color coreEffectColor = coreEffect.color;
            coreEffectColor.r = color.r;
            coreEffectColor.g = color.g;
            coreEffectColor.b = color.b;
            coreEffect.color = coreEffectColor;

            Color outCircle0Color = outCircle0.color;
            outCircle0Color.r = color.r;
            outCircle0Color.g = color.g;
            outCircle0Color.b = color.b;
            outCircle0.color = outCircle0Color;

            Color outCircle1Color = outCircle1.color;
            outCircle1Color.r = color.r;
            outCircle1Color.g = color.g;
            outCircle1Color.b = color.b;
            outCircle1.color = outCircle1Color;
        }
        #endregion
    }
}