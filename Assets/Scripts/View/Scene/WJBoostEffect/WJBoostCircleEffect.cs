﻿using UnityEngine;
using Com.Mod.ThumbRun.Controller;
using Com.Mod.ThumbRun.View;

public class WJBoostCircleEffect : MonoBehaviour 
{
    public AudioClip wjBoostSound;
    public AudioClip bbBoostSound;
    public AudioSource audioSource;

    [SerializeField]
    private WJBooster _wjBooster;
    [SerializeField]
    private BBBooster _bbBooster;
    [SerializeField]
    private GameObject _circlePrefab;
    [SerializeField]
    private Color _wjBoostColor;
    [SerializeField]
    private Color _bbBoostColor;

    void Awake ()
    {
        _wjBooster.Boost += OnBoost;
        _bbBooster.Boost += OnBBBoost;
    }
	
    void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}

    void OnDestroy ()
    {
        _wjBooster.Boost -= OnBoost;
    }



    #region Private Functions
    private void OnBoost(Vector3 pos, int direction)
    {
        pos.x += -direction / 7f;

        float targetScale = (_wjBooster.cmlWJMSIncRate / 20f);

        GameObject instance = GameObject.Instantiate(_circlePrefab);
        instance.transform.parent = transform;
        instance.transform.localPosition = pos;
        instance.transform.localScale = new Vector3(targetScale, targetScale, 1f);

        WJBoostCircle wjBoostCircle = instance.GetComponent<WJBoostCircle>();
        wjBoostCircle.ChangeColor(_wjBoostColor);
        wjBoostCircle.ShowEffect();

        audioSource.clip = wjBoostSound;
        audioSource.pitch = 0.6f + ((_wjBooster.cmlWJMSIncRate / _wjBooster.maxWJMSIncRate));
        audioSource.Play();
    }

    private void OnBBBoost(Vector3 pos, int direction)
    {
        float targetScale = 1.5f;

        GameObject instance = GameObject.Instantiate(_circlePrefab);
        instance.transform.parent = transform;
        instance.transform.localPosition = pos;
        instance.transform.localScale = new Vector3(targetScale, targetScale, 1f);

        WJBoostCircle wjBoostCircle = instance.GetComponent<WJBoostCircle>();
        wjBoostCircle.ChangeColor(_bbBoostColor);
        wjBoostCircle.ShowEffect();

        audioSource.clip = bbBoostSound;
        audioSource.pitch = 1;
        audioSource.Play();
    }
    #endregion



    #region Public Functions
    public void ChangeColor(Color color)
    {

    }
    #endregion
}
