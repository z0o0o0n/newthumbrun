﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfomationConfirmPopup : MonoBehaviour
{
    [SerializeField]
    private UIBasicButton _closeButton;
    [SerializeField]
    private CheckBoxButton _checkBoxButton;
    [SerializeField]
    private UIDynamicButton _okButton;
    [SerializeField]
    private MainLoaderScene _mainLoaderScene;

    void Start ()
    {
        _okButton.Disable();

        _okButton.Click += OnOkButtonClick;
        _closeButton.On_Click += OnCloseButtonClick;
        _checkBoxButton.Changed += OnCheckBoxChanged;

        Hide();
    }
	
	void Update ()
    {
		
	}

    void OnDestroy()
    {
        _okButton.Click -= OnOkButtonClick;
        _closeButton.On_Click -= OnCloseButtonClick;
        _checkBoxButton.Changed -= OnCheckBoxChanged;
    }





    private void OnOkButtonClick(GameObject target)
    {
        _mainLoaderScene.LoadMainScene();
        Hide();
    }

    private void OnCloseButtonClick()
    {
        Hide();
    }

    public void OnInfoButtonClick()
    {
        Application.OpenURL("https://game-service.playnanoo.com/privacy");
    }

    private void OnCheckBoxChanged(bool isChecked)
    {
        if(isChecked)
        {
            _okButton.Enable();
        }
        else
        {
            _okButton.Disable();
        }
    }




    public void Hide()
    {
        gameObject.SetActive(false);
    }

	public void Show()
	{
		gameObject.SetActive(true);
	}
}
