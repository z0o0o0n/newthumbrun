﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour 
{
    public Camera camera;

    //private BlurEffect _blurEffect;
    private Blur _blurEffect;

    void Awake()
    {
        camera = GetComponent<Camera>();
        //_blurEffect = GetComponent<BlurEffect>();
        _blurEffect = GetComponent<Blur>();
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}

    public void ActivateBlur()
    {
        _blurEffect.enabled = true;
    }

    public void DeactivateBlur()
    {
        _blurEffect.enabled = false;
    }
}
