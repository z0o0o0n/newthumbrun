﻿using UnityEngine;
using System.Collections;
using view.mapScene;
using System.Collections.Generic;
using DG.Tweening;

public class DioramaMovementView : MonoBehaviour 
{
    public Camera camera;
    public TouchArea touchArea;
    public List<CollectionModel> dioramaModelList;
    public CollectionSceneUI ui;

    private const float MOVEMENT_TIME = 0.4f;
    private const float RETURN_TIME = 0.3f;
    private const float CHARACTER_MOVEMENT_TIME = 1.5f;
    private bool _isDebug = false;
    private DioramaMovementModel _model;
    private Vector2 _dragPoint2D = Vector2.zero;
    private Vector3 _dragPoint3D = Vector3.zero;

    void Awake()
    {
        _model = GetComponent<DioramaMovementModel>();
        _model.On_Init += OnInit;
        _model.On_Prev += OnPrev;
        _model.On_Next += OnNext;
        _model.On_StartDrag += OnStartDrag;
        _model.On_Return += OnReturn;
        _model.TouchActivated += OnTouchActivated;
        _model.TouchDeactivated += OnTouchDeactivated;

        touchArea.On_Press += OnPress;
        touchArea.On_Drag += OnDrag;
        touchArea.On_Release += OnRelease;
    }

	void Start () 
	{
	
	}

    void FixedUpdate()
    {
        if (!_model.isMoving)
        {
            Vector3 localPos = dioramaModelList[_model.GetCurrentDioramaIndex()].transform.localPosition;
            localPos.x += ((_dragPoint3D.x) - localPos.x) * 0.9f;
            dioramaModelList[_model.GetCurrentDioramaIndex()].transform.localPosition = localPos;
        }
    }

    private void OnInit()
    {
        UpdateDragPoint2D(dioramaModelList[_model.GetCurrentDioramaIndex()].transform.localPosition);
        UpdateDragPoint3D(_dragPoint2D);
        MoveDiorama(_model.GetCurrentDioramaIndex(), 0, 0);
        //dioramaModelList[_model.GetCurrentDioramaIndex()].gameObject.SetActive(true);
    }

    private void OnPrev(bool isEnd)
    {
        //if (_isDebug) Debug.Log("OnPrev-----");
        MoveDiorama(_model.GetCurrentDioramaIndex(), -1, MOVEMENT_TIME);
        //MoveMap(_model.GetCurrentMapIndex(), MOVEMENT_TIME);

        if(isEnd)
        {

        }
        //if (!isEnd)
        //{
        //    MoveCharacter(-1);
        //    mapBackgroundManager.Move(_model.GetCurrentMapIndex(), MOVEMENT_TIME, -1);
        //}
        //else
        //{
        //    mapBackgroundManager.Return(RETURN_TIME);
        //}
    }

    private void OnNext(bool isEnd)
    {
        //if (_isDebug) Debug.Log("OnNext-----");
        MoveDiorama(_model.GetCurrentDioramaIndex(), 1, MOVEMENT_TIME);
        //MoveMap(_model.GetCurrentMapIndex(), MOVEMENT_TIME);

        //if (!isEnd)
        //{
        //    MoveCharacter(1);
        //    mapBackgroundManager.Move(_model.GetCurrentMapIndex(), MOVEMENT_TIME, 1);
        //}
        //else
        //{
        //    mapBackgroundManager.Return(RETURN_TIME);
        //}
    }

    private void OnStartDrag()
    {
        //Debug.Log("On Start Drag Map");
        //_mapTitle.Hide(0.2f);
    }

    private void OnReturn()
    {
        ReturnDiorama();
        //mapBackgroundManager.Return(RETURN_TIME);
    }

    private void OnInitCharacter()
    {
        //CreateCharacter(_model.GetCharacterID());
    }

    private void CreateCharacter(int id)
    {
        //GameObject character = CharacterPrefabProvider.instance.Get(id);
        //_mapCharacter = GameObject.Instantiate(character);
        //_mapCharacter.transform.localPosition = new Vector3(_model.GetCurrentMapIndex() * 7f, 0.01f, 1f);
        //_mapCharacter.transform.parent = mapGroundContainer.transform;
    }

    private void OnPress()
    {
        _model.Press();

        UpdateDragPoint2D(dioramaModelList[_model.GetCurrentDioramaIndex()].transform.localPosition);
    }

    private void OnDrag(Vector2 delta)
    {
        _model.Drag(delta);

        if (!_model.isMoving)
        {
            _dragPoint2D += delta / 2;
            UpdateDragPoint3D(_dragPoint2D);

        //    mapBackgroundManager.Drag(delta.x);
        }
    }

    private void OnRelease()
    {
        _model.Release();
    }

    private void MoveDiorama(int index, int direction, float time)
    {
        if(_model.GetCurrentDioramaIndex() == _model.GetPreviousDioramaIndex()) return;
        
        _model.isMoving = true;
        
        dioramaModelList[_model.GetPreviousDioramaIndex()].transform.DOLocalMoveX(direction * -3, time).SetEase(Ease.OutCubic);

        ReplaceStartPosition(direction);
        
        dioramaModelList[_model.GetCurrentDioramaIndex()].gameObject.SetActive(true);
        dioramaModelList[_model.GetCurrentDioramaIndex()].transform.DOLocalMoveX(0, time).SetEase(Ease.OutCubic).OnComplete(OnMoved);
    }

    private void ReplaceStartPosition(int direction)
    {
        CollectionModel currentDioramaModel = dioramaModelList[_model.GetCurrentDioramaIndex()];
        Vector3 dioramaModelPos = currentDioramaModel.transform.localPosition;
        
        if (direction == -1) dioramaModelPos.x = -3;
        else if (direction == 0) dioramaModelPos.x = 0;
        else if (direction == 1) dioramaModelPos.x = 3;

        currentDioramaModel.transform.localPosition = dioramaModelPos;
    }

    private void OnMoved()
    {
        //_mapTitle.ChangeTitle(_model.GetCurrentMapIndex());
        //_mapTitle.Show(0.3f);

        dioramaModelList[_model.GetPreviousDioramaIndex()].gameObject.SetActive(false);

        UpdateDragPoint2D(dioramaModelList[_model.GetCurrentDioramaIndex()].transform.localPosition);
        UpdateDragPoint3D(_dragPoint2D);
        _model.isMoving = false;

        _model.OnDioramaMoved();
    }

    private void ReturnDiorama()
    {
        _model.isMoving = true;
        dioramaModelList[_model.GetCurrentDioramaIndex()].transform.DOLocalMoveX(0, RETURN_TIME).SetEase(Ease.OutExpo).OnComplete(OnReturned);
    }

    private void OnReturned()
    {
        //_mapTitle.ChangeTitle(_model.GetCurrentMapIndex());
        //_mapTitle.Show(0.3f);

        UpdateDragPoint2D(dioramaModelList[_model.GetCurrentDioramaIndex()].transform.localPosition);
        UpdateDragPoint3D(_dragPoint2D);
        _model.isMoving = false;

        _model.OnDioramaReturned();
    }

    // 3D DragPoint를 2D로 좌표로 변경해 갱신합니다.
    private void UpdateDragPoint2D(Vector3 pos)
    {
        _dragPoint2D = camera.WorldToScreenPoint(pos);
        _dragPoint2D.x = _dragPoint2D.x - ((Screen.width - ui.characterInfoPanel.GetWidth()) / 2);
    }

    // 2D DragPoint를 3D로 좌표로 변경해 갱신합니다.
    private void UpdateDragPoint3D(Vector2 pos)
    {
        float targetPosX = ((Screen.width - ui.characterInfoPanel.GetWidth()) / 2) + pos.x;
        if (_isDebug) Debug.Log("targetPosX: " + targetPosX);
        _dragPoint3D = camera.ScreenToWorldPoint(new Vector3(targetPosX, 0, 6));
    }

    private void OnTouchActivated()
    {
        //touchArea.ActivateTouch();
    }

    private void OnTouchDeactivated()
    {
        //touchArea.DeactivateTouch();
    }
}
