﻿using UnityEngine;
using System.Collections;
using Com.Mod.Util;

public class DioramaMovementModel : MonoBehaviour 
{
    public delegate void DioramaMovementEvent(bool isEnd);
    public delegate void DioramaMovementStateEvent();
    public event DioramaMovementEvent On_Prev;
    public event DioramaMovementEvent On_Next;
    public event DioramaMovementStateEvent On_Init;
    public event DioramaMovementStateEvent On_StartDrag;
    public event DioramaMovementStateEvent On_Return;
    public event DioramaMovementStateEvent Moved;
    public event DioramaMovementStateEvent Returned;
    public event DioramaMovementStateEvent TouchActivated;
    public event DioramaMovementStateEvent TouchDeactivated;

    public int dioramaCount = 0;
    public bool isPress = false;
    public bool isMoving = false;

    private bool _isStartDrag = false;
    private int _previousDioramaIndex = 0;
    private int _currentDioramaIndex = 0;
    private float _distanceForMovement = 300;
    private float _distance = 0.0f;
    private float _speedForMovement = 10;
    private SpeedChecker _speedChecker;

    void Awake()
    {
        _speedChecker = GetComponent<SpeedChecker>();
    }

    void Start()
    {

    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        _speedChecker.UpdatePos(new Vector2(_distance, 0));
    }

    public void Init(int dioramaIndex)
    {
        _currentDioramaIndex = dioramaIndex;
        if (On_Init != null) On_Init();
    }

    public void Press()
    {
        isPress = true;
        _isStartDrag = false;
        _distance = 0.0f;
        _speedChecker.Reset();
    }

    public void Drag(Vector2 delta)
    {
        if (!_isStartDrag)
        {
            _isStartDrag = true;
            if (On_StartDrag != null) On_StartDrag();
        }

        _distance += delta.x;
        if (isMoving) return;

        if (_distance < -_distanceForMovement)
        {
            _previousDioramaIndex = _currentDioramaIndex;
            _currentDioramaIndex++;
            MoveDiorama(1);
        }
        else if (_distance > _distanceForMovement)
        {
            _previousDioramaIndex = _currentDioramaIndex;
            _currentDioramaIndex--;
            MoveDiorama(-1);
        }
    }

    public void Release()
    {
        isPress = false;

        if (isMoving) return;

        if (_speedChecker.GetSpeedX() < -_speedForMovement)
        {
            _previousDioramaIndex = _currentDioramaIndex;
            _currentDioramaIndex++;
            MoveDiorama(1);
            return;
        }
        else if (_speedChecker.GetSpeedX() > _speedForMovement)
        {
            _previousDioramaIndex = _currentDioramaIndex;
            _currentDioramaIndex--;
            MoveDiorama(-1);
            return;
        }

        if (Mathf.Abs(_distance) < _distanceForMovement)
        {
            On_Return();
        }
    }

    private void MoveDiorama(int direction)
    {
        bool isEnd = false;
        if (_currentDioramaIndex < 0)
        {
            _currentDioramaIndex = dioramaCount - 1;
            isEnd = true;
        }
        else if (_currentDioramaIndex >= dioramaCount)
        {
            _currentDioramaIndex = 0;
            isEnd = true;
        }

        if (direction == 1)
        {
            if (On_Next != null) On_Next(isEnd);
        }
        else if (direction == -1)
        {
            if (On_Prev != null) On_Prev(isEnd);
        }
    }

    public int GetPreviousDioramaIndex()
    {
        return _previousDioramaIndex;
    }

    public int GetCurrentDioramaIndex()
    {
        return _currentDioramaIndex;
    }

    public void OnDioramaMoved()
    {
        if (Moved != null) Moved();
    }

    public void OnDioramaReturned()
    {
        if (Returned != null) Returned();
    }

    public void ActivateTouch()
    {
        if (TouchActivated != null) TouchActivated();
    }

    public void DeactivateTouch()
    {
        if (TouchDeactivated != null) TouchDeactivated();
    }
}
