﻿using UnityEngine;
using System.Collections;

public class CollectionModelBubblePos : MonoBehaviour 
{
    public Vector2 offsetPos;
    public InUseBubbleAlignType.type alignType;
}
