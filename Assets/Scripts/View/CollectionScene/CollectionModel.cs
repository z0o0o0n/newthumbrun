﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectionModel : MonoBehaviour 
{
    public delegate void CollectionModelEvent(int id);
    public event CollectionModelEvent On_PressCollectionCharacter;

    public int collectionID;
    public List<CollectionCharacter> characterList;

    void Awake()
    {
        for(int i = 0; i < characterList.Count; i++)
        {
            characterList[i].On_Press += OnPressCollectionCharacter;
        }
    }

    private void OnPressCollectionCharacter(int id)
    {
        if (On_PressCollectionCharacter != null) On_PressCollectionCharacter(id);
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}

    void OnDestory()
    {
        for (int i = 0; i < characterList.Count; i++)
        {
            characterList[i].On_Press -= OnPressCollectionCharacter;
        }
    }

    public void ActivateTouch()
    {
        SetTouchActivation(true);
    }

    public void DeactivateTouch()
    {
        SetTouchActivation(false);
    }

    private void SetTouchActivation(bool isActivated)
    {
        for(int i = 0; i < characterList.Count; i++)
        {
            if (isActivated) characterList[i].ActivateTouch();
            else characterList[i].DeactivateTouch();
        }
    }
}
