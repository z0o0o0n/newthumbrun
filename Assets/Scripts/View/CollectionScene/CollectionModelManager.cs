﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class CollectionModelManager : MonoBehaviour 
{
    private const float MODEL_POS_Y = -0.34f;

    public delegate void CollectionModelManagerEvent(int id);
    public event CollectionModelManagerEvent On_PressCharacter;

    public DioramaMovementModel _dioramaMovementModel;
    public List<CollectionModel> collectionModelList;
    //public CollectionNameLabel nameLabel;
    public CollectionInfoPanel collectionInfoPanel;
    public Camera camera;
    public List<CollectionCharacter> collectionCharacterList;

    private bool _isDebug = true;
    //private bool _isChanging = false;
    //private int _currentModelIndex = 1;
    private CollectionData _currentCollectionData;

	void Awake () 
    {
        _dioramaMovementModel.On_Init += OnInitedDioramaMovementModel;
        _dioramaMovementModel.On_Next += OnNextDiorama;
        _dioramaMovementModel.On_Prev += OnPrevDiorama;
	}

    void Start()
    {

    }

    private void OnInitedDioramaMovementModel()
    {
        Init();
    }

    private void Init()
    {
        collectionModelList[_dioramaMovementModel.GetCurrentDioramaIndex()].gameObject.SetActive(true);
        collectionModelList[_dioramaMovementModel.GetCurrentDioramaIndex()].On_PressCollectionCharacter += OnPressCollectionCharacter;

        CollectionModel collectionModel = collectionModelList[_dioramaMovementModel.GetCurrentDioramaIndex()].GetComponent<CollectionModel>();
        UpdateCurrentCollectionData(collectionModel.collectionID);
        ChangeCollectionName(_currentCollectionData.name, _currentCollectionData.rewardData, _currentCollectionData.isCompleted);
        camera.backgroundColor = _currentCollectionData.bgColor;
    }

    private void UpdateCurrentCollectionData(int collectionID)
    {
        _currentCollectionData = CollectionDataManager.instance.GetCollectionDataByCollectionID(collectionID);
    }
	
	void Update () 
    {
	    
	}

    public void OnNextDiorama(bool isEnd)
    {
        //if (!_isChanging)
        //{
            //_isChanging = true;
            collectionModelList[_dioramaMovementModel.GetPreviousDioramaIndex()].On_PressCollectionCharacter -= OnPressCollectionCharacter;
            //collectionModelList[_currentModelIndex].transform.DOMoveX(-4, 0.3f).SetEase(Ease.InOutCubic);

            //int nextModelIndex = _currentModelIndex + 1;
            //if (nextModelIndex >= collectionModelList.Count) nextModelIndex = 0;

            //collectionModelList[_dioramaMovementModel.GetCurrentDioramaIndex()].gameObject.SetActive(true);
            //collectionModelList[nextModelIndex].transform.localPosition = new Vector3(4f, MODEL_POS_Y, 4f);
            //collectionModelList[nextModelIndex].transform.DOMoveX(0, 0.3f).SetEase(Ease.InOutCubic).OnComplete(OnCompleteNextMotion);

            collectionModelList[_dioramaMovementModel.GetCurrentDioramaIndex()].On_PressCollectionCharacter += OnPressCollectionCharacter;
            CollectionModel collectionModel = collectionModelList[_dioramaMovementModel.GetCurrentDioramaIndex()].GetComponent<CollectionModel>();
            UpdateCurrentCollectionData(collectionModel.collectionID);
            ChangeCollectionName(_currentCollectionData.name, _currentCollectionData.rewardData, _currentCollectionData.isCompleted);
            camera.backgroundColor = _currentCollectionData.bgColor;
        //}
    }

    //private void OnCompleteNextMotion()
    //{
    //    collectionModelList[_currentModelIndex].gameObject.SetActive(false);

    //    _currentModelIndex++;
    //    if (_currentModelIndex >= collectionModelList.Count) _currentModelIndex = 0;
    //    collectionModelList[_currentModelIndex].On_PressCollectionCharacter += OnPressCollectionCharacter;

    //    _isChanging = false;
    //}

    private void OnPressCollectionCharacter(int id)
    {
        if(On_PressCharacter != null) On_PressCharacter(id);
    }

    private void ChangeCollectionName(string name, CharacterStatusData rewardData, bool isComplete)
    {
        collectionInfoPanel.ChangeData(name, rewardData, isComplete);
    }

    public void OnPrevDiorama(bool isEnd)
    {
        //if (!_isChanging)
        //{
        //    _isChanging = true;
            //collectionModelList[_currentModelIndex].transform.DOMoveX(4, 0.3f).SetEase(Ease.InOutCubic);
        collectionModelList[_dioramaMovementModel.GetPreviousDioramaIndex()].On_PressCollectionCharacter -= OnPressCollectionCharacter;

        //int prevModelIndex = _currentModelIndex - 1;
        //if (prevModelIndex <= -1) prevModelIndex = collectionModelList.Count - 1;

        //collectionModelList[_dioramaMovementModel.GetCurrentDioramaIndex()].gameObject.SetActive(true);
        //collectionModelList[_dioramaMovementModel.GetCurrentDioramaIndex()].transform.localPosition = new Vector3(-4f, MODEL_POS_Y, 4f);
        //collectionModelList[_dioramaMovementModel.GetCurrentDioramaIndex()].transform.DOMoveX(0, 0.3f).SetEase(Ease.InOutCubic).OnComplete(OnCompletePrevMotion);

        collectionModelList[_dioramaMovementModel.GetCurrentDioramaIndex()].On_PressCollectionCharacter += OnPressCollectionCharacter;
        CollectionModel collectionModel = collectionModelList[_dioramaMovementModel.GetCurrentDioramaIndex()].GetComponent<CollectionModel>();
        UpdateCurrentCollectionData(collectionModel.collectionID);
        ChangeCollectionName(_currentCollectionData.name, _currentCollectionData.rewardData, _currentCollectionData.isCompleted);
        camera.backgroundColor = _currentCollectionData.bgColor;
        //}
    }

    //private void OnCompletePrevMotion()
    //{
    //    collectionModelList[_currentModelIndex].gameObject.SetActive(false);

    //    _currentModelIndex--;
    //    if (_currentModelIndex <= -1) _currentModelIndex = collectionModelList.Count - 1;
    //    collectionModelList[_currentModelIndex].On_PressCollectionCharacter += OnPressCollectionCharacter;

    //    _isChanging = false;
    //}

    public void SetCollectedCharacterIDList(List<int> collectedCharacterIDList)
    {
        for (int j = 0; j < collectionCharacterList.Count; j++ )
        {
            collectionCharacterList[j].Dim();
        }
        //Debug.Log("collectedCharacterIDList.Count: " + collectedCharacterIDList.Count);
        for (int i = 0; i < collectedCharacterIDList.Count; i++)
        {
            //if (collectedCharacterIDList[i] == null) Debug.Log("------- null");
            //Debug.Log("collectedCharacterIDList[i]: " + collectedCharacterIDList[i]);
            //Debug.Log("== " + GetCollectionCharacter(collectedCharacterIDList[i]));
            GetCollectionCharacter(collectedCharacterIDList[i]).Distinct();
        }
    }

    //public void SetStartModelIndex(int index)
    //{
    //    _currentModelIndex = index;
    //}

    public CollectionCharacter GetCollectionCharacter(int id)
    {
        CollectionCharacter result = null;

        for (int i = 0; i < collectionCharacterList.Count; i++ )
        {
            //Debug.Log("-----: " + collectionCharacterList[i].getCharacterID() + " / " + id);
            if (collectionCharacterList[i].getCharacterID() == id) result = collectionCharacterList[i];
        }
        return result;
    }

    public void ActivateTouch()
    {
        for (int i = 0; i < collectionCharacterList.Count; i++)
        {
            collectionCharacterList[i].ActivateTouch();
        }
    }

    public void DeactivateTouch()
    {
        for (int i = 0; i < collectionCharacterList.Count; i++)
        {
            collectionCharacterList[i].DeactivateTouch();
        }
    }

    public int GetCollectionModelIndexByCollectionID(int collectionID)
    {
        int result = -1;
        for(int i = 0; i < collectionModelList.Count; i++)
        {
            if(collectionModelList[i].collectionID == collectionID)
            {
                result = i;
                break;
            }
        }
        return result;
    }
}
