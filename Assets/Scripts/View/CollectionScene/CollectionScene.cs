﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class CollectionScene : MonoBehaviour
{
    public CollectionSceneUI ui;
    public CameraManager cameraManager;
    public CollectionPopupManager collectionPopupManager;
    public CollectionModelManager collectionModelManager;
    public DioramaMovementModel dioramaMovementModel;
    public ParticleSystem aura;
    public GameObject spotLight;
    public BgmManager bgmManager;

    private bool _isDebug = true;
    private GameManager _gameManager;

    void Awake()
    {
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        //bgmManager.Play(0);
        //_loadingShutter.On_LoadComplete += OnLoadCompleteScene;

        //GameManager.state = GameState.state.NONE;
    }

    void Start()
    {
        InitDioramaManager();
        ShowSpotLight();

        List<int> collectedCharacterIDList = GameData.instance.GetUserCollectionData().collectedCharacterIDList;
        collectionModelManager.SetCollectedCharacterIDList(collectedCharacterIDList);

        _gameManager.sceneLoader.ShutterOpened += OnShutterOpened;
        _gameManager.sceneLoader.OpenShutter(0.6f);
    }

    private void OnShutterOpened()
    {
        _gameManager.sceneLoader.ShutterOpened -= OnShutterOpened;

        if (GameData.instance.GetConfigData().IsFirstTimeCollection)
        {
            ChangeBlurScreen(0.0f);
            cameraManager.ActivateBlur();
            ui.collectionTutorialBook.Closed += OnTutorialClosed;
            ui.collectionTutorialBook.Show();
            GameData.instance.GetConfigData().IsFirstTimeCollection = false;
        }
        else
        {
            Init();
        }

        //GameManager.state = GameState.state.COLLECTION;
    }

    private ViewportChangeDele _blurScreenChangeCallback;
    private void ChangeBlurScreen(float time, ViewportChangeDele onComplete = null)
    {
        if (onComplete != null) MoveNormalViewport(time, onComplete);
        else MoveNormalViewport(time);
        collectionModelManager.DeactivateTouch();
        ui.HideUI(time);
        ui.CloseInfoPanel(time);
    }

    private void ChangeNormalScreen()
    {
        MoveDioramaViewport(0.5f);
        collectionModelManager.ActivateTouch();
        ui.ShowUI(0.5f);
        OpenInfoPanel();
        cameraManager.DeactivateBlur();
    }

    private void OnTutorialClosed()
    {
        ui.collectionTutorialBook.Closed -= OnTutorialClosed;
        ChangeNormalScreen();
        Init();
    }

    private void InitDioramaManager()
    {
        CollectionData mainCharacterCollectionData = CollectionDataManager.instance.GetCollectionDataByCharacterID(GetMainCharacterID());
        int index = collectionModelManager.GetCollectionModelIndexByCollectionID(mainCharacterCollectionData.id);
        //collectionModelManager.SetStartModelIndex(index);

        dioramaMovementModel.Init(index);
    }

    private void InitUI()
    {
        ui.ChangeExtendDisplay(0);
        ui.HideUI(0);
        ui.ShowLevelUpPointDisplayer(0.3f);
    }

    private void OnLoadCompleteScene()
    {
        //_loadingShutter.On_LoadComplete -= OnLoadCompleteScene;
        
    }

    private void Init()
    {
        InitUI();

        ui.HomeButtonClick += OnHomeButtonClick;
        //ui.On_PressLeftArrowButton += OnPressLeftArrowButton;
        //ui.On_PressRightArrowButton += OnPressRightArrowButton;
        ui.On_PressUseButton += OnPressUseButton;
        ui.On_PressLevelUpPointButton += OnPressLevelUpPointButton;
        ui.On_ClosedBonusAbilityPopup += OnClosedBonusAbilityPopup;
        ui.helpButton.On_Click += OnTutorialButtonClick;

        InitCollectionPopupManager();

        CharacterDataManager.instance.ClearNewCharacterIDList();
        CollectionDataManager.instance.ClearNewCollectionIDList();
    }

    private void InitCollectionPopupManager()
    {
        string log = "CollectionScene - InitCollectionPopupManager() \n";

        List<CharacterData> characterDataList = new List<CharacterData>();
        List<CollectionData> collectionDataList = new List<CollectionData>();

        List<int> newCharacterIDList = GameData.instance.GetUserCollectionData().newCharacterIDList;
        List<int> newCollectionIDList = GameData.instance.GetUserCollectionData().newCompletedCollectionIDList;

        log += "    - newCharacterIDList.Count: " + newCharacterIDList.Count.ToString() + " \n";
        log += "    - newCollectionIDList.Count: " + newCollectionIDList.Count.ToString() + " \n";
        if (_isDebug) Debug.Log(log);
        
        if (newCharacterIDList.Count == 0 && newCollectionIDList.Count == 0)
        {
            OnEndPopupManager();
            return;
        }

        for (int i = 0; i < newCharacterIDList.Count; i++)
        {
            characterDataList.Add(CharacterDataManager.instance.GetCharacterDataByID(newCharacterIDList[i]));
        }

        for (int j = 0; j < newCollectionIDList.Count; j++)
        {
            collectionDataList.Add(CollectionDataManager.instance.GetCollectionDataByCollectionID(newCollectionIDList[j]));
        }

        collectionPopupManager.On_End += OnEndPopupManager;
        collectionPopupManager.CreatePopups(collectionDataList, characterDataList);
        collectionPopupManager.OpenPopup();

        cameraManager.ActivateBlur();
        collectionModelManager.DeactivateTouch();
    }

    private void OnHomeButtonClick()
    {
        bgmManager.Stop();
        ui.SetButtonActive(false);
        _gameManager.sceneLoader.Load(SceneName.MAIN);
    }

    //private void OnPressLeftArrowButton()
    //{
    //    collectionModelManager.PrevModel();
    //}

    //private void OnPressRightArrowButton()
    //{
    //    collectionModelManager.NextModel();
    //}

    private void OnPressUseButton(int characterID)
    {
        GameData.instance.GetConfigData().SelectedCharacterID = characterID;

        ui.ChangeInUseBubbleTarget(GetCurrentDioramaCharacter().gameObject, cameraManager.camera);

        ShowSpotLight();
    }

    private void ShowSpotLight()
    {
        spotLight.transform.parent = collectionModelManager.GetCollectionCharacter(GetMainCharacterID()).transform;
        spotLight.transform.localPosition = new Vector3(0, 0.005f, 0);
    }

    private void OnPressLevelUpPointButton()
    {
        float time = 0.2f;
        ui.SetButtonActive(false);
        collectionModelManager.DeactivateTouch();
        dioramaMovementModel.DeactivateTouch();
        ShowBonusAbilityPopup(time);
    }

    private void ShowBonusAbilityPopup(float time)
    {
        MoveNormalViewport(time);
        ui.ShowBonusAbilityPopup(time);
        cameraManager.ActivateBlur();
    }

    private void OnClosedBonusAbilityPopup()
    {
        ui.SetButtonActive(true);
        collectionModelManager.ActivateTouch();
        dioramaMovementModel.ActivateTouch();
        ShowDioramaView();
    }

    private void OnTutorialButtonClick()
    {
        float time = 0.3f;
        ChangeBlurScreen(time, OnChangedBlur);
    }

    private void OnChangedBlur()
    {
        ui.collectionTutorialBook.Closed += OnTutorialClosed2;
        ui.collectionTutorialBook.Show();
        cameraManager.ActivateBlur();
    }

    private void OnTutorialClosed2()
    {
        ui.collectionTutorialBook.Closed -= OnTutorialClosed2;
        ChangeNormalScreen();
    }



    private void OnEndPopupManager()
    {
        ShowDioramaView();
        //ui.ShowUI(0.3f);
    }

    // Collection Scene의 기본 View
    private void ShowDioramaView()
    {
        if (_isDebug) Debug.Log("CollectionScene - ShowDioramaView()");
        cameraManager.DeactivateBlur();
        MoveDioramaViewport(0.5f);
        OpenInfoPanel();
        collectionModelManager.ActivateTouch();


        ui.ChangeInUseBubbleTarget(GetCurrentDioramaCharacter().gameObject, cameraManager.camera);
        ui.ChangeShrinkDisplay(0.3f, OnCompleteDisplayChange);
    }

    private void OnCompleteDisplayChange()
    {
        ui.ShowUI(0.3f);
        collectionModelManager.On_PressCharacter += OnPressCharacter;
    }

	private void OpenInfoPanel()
	{
        int selectedCharacterID = GameData.instance.GetConfigData().SelectedCharacterID;
        CharacterData characterData = CharacterDataManager.instance.GetCharacterDataByID(selectedCharacterID);
        CollectionData collectionData = CollectionDataManager.instance.GetCollectionDataByCharacterID(selectedCharacterID);
        ui.OpenInfoPanel(characterData, collectionData, ButtonAreaType.type.InUseState, true);
	}

    private void OnPressCharacter(int id)
    {
        if (ui.characterInfoPanel.currentID == id) return;
        CharacterData characterData = CharacterDataManager.instance.GetCharacterDataByID (id);

        ButtonAreaType.type buttonAreaType = SelectButtonAreaType(characterData);
        CollectionData collectionData = CollectionDataManager.instance.GetCollectionDataByCharacterID(characterData.id);
        ui.OpenInfoPanel(characterData, collectionData, buttonAreaType, IsInUseCharacter(characterData.id));
    }

    private ButtonAreaType.type SelectButtonAreaType(CharacterData characterData)
    {
        ButtonAreaType.type result;

        if (characterData.kind == CharacterKind.kind.Item) return ButtonAreaType.type.None;

        if (characterData.isCollected) result = ButtonAreaType.type.InUseState;
        else result = ButtonAreaType.type.LockState;
        return result;
    }

    private bool IsInUseCharacter(int characterID)
    {
        bool result;
        if(GameData.instance.GetConfigData().SelectedCharacterID == characterID) result = true;
        else result = false;
        return result;
    }

	void Update () 
    {
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            BonusAbilityModel.instance.Increase();
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            BonusAbilityModel.instance.Decrease();
        }
	}


    public void GoHome()
    {
        OnHomeButtonClick();
    }



    private int GetMainCharacterID()
    {
        return GameData.instance.GetConfigData().SelectedCharacterID;
    }

    private CollectionCharacter GetCurrentDioramaCharacter()
    {
        return collectionModelManager.GetCollectionCharacter(GetMainCharacterID());
    }

    private void MoveDioramaViewport(float time)
    {
        float panelWidth = ui.characterInfoPanel.GetWidth();
        float tempValue = panelWidth / UIScreen.instance.GetWidth();
        
        DOTween.To(() => cameraManager.camera.rect, x => cameraManager.camera.rect = x, new Rect(-tempValue, 0f, 1f, 1f), time).SetEase(Ease.InOutCubic);
    }

    public delegate void ViewportChangeDele();
    public ViewportChangeDele viewportExpansionCompleteCallback;
    private void MoveNormalViewport(float time, ViewportChangeDele onComplete = null)
    {
        if (onComplete != null)
        {
            viewportExpansionCompleteCallback = onComplete;
            DOTween.To(() => cameraManager.camera.rect, x => cameraManager.camera.rect = x, new Rect(0f, 0f, 1f, 1f), time).SetEase(Ease.OutQuad).OnComplete(OnViewportExpansionComplete);
        }
        else
        {
            DOTween.To(() => cameraManager.camera.rect, x => cameraManager.camera.rect = x, new Rect(0f, 0f, 1f, 1f), time).SetEase(Ease.OutQuad);
        }
    }

    private void OnViewportExpansionComplete()
    {
        viewportExpansionCompleteCallback();
    }

    void OnDestroy()
    {
        //_loadingShutter.On_LoadComplete -= OnLoadCompleteScene;

        ui.HomeButtonClick -= OnHomeButtonClick;
        //ui.On_PressLeftArrowButton -= OnPressLeftArrowButton;
        //ui.On_PressRightArrowButton -= OnPressRightArrowButton;
        ui.On_PressUseButton -= OnPressUseButton;
        ui.On_PressLevelUpPointButton -= OnPressLevelUpPointButton;
        ui.On_ClosedBonusAbilityPopup -= OnClosedBonusAbilityPopup;
        ui.helpButton.On_Click -= OnTutorialButtonClick;

        collectionModelManager.On_PressCharacter -= OnPressCharacter;

        _gameManager.sceneLoader.ShutterOpened -= OnShutterOpened;
    }
}
