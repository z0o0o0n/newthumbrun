﻿using UnityEngine;
using System.Collections;

public class CollectionPopupOKButton : MonoBehaviour 
{
    public UISprite icon;
    public UILabel label;
    public UIBasicButton button;

    void Awake ()
    {
        button = GetComponent<UIBasicButton>();
        button.On_Press += OnButtonPressed;
        button.On_Release += OnButtonRelease;
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    private void OnButtonPressed()
    {
        Vector3 iconPosition = icon.transform.localPosition;
        iconPosition.y -= 8;
        icon.transform.localPosition = iconPosition;

        Vector3 labelPosition = label.transform.localPosition;
        labelPosition.y -= 8;
        label.transform.localPosition = labelPosition;
    }

    private void OnButtonRelease()
    {
        Vector3 iconPosition = icon.transform.localPosition;
        iconPosition.y += 8;
        icon.transform.localPosition = iconPosition;

        Vector3 labelPosition = label.transform.localPosition;
        labelPosition.y += 8;
        label.transform.localPosition = labelPosition;
    }

    void OnDestory ()
    {
        button.On_Press -= OnButtonPressed;
        button.On_Release -= OnButtonRelease;
    }
}
