﻿using UnityEngine;
using System.Collections;
using popupManager;
using DG.Tweening;

public class NewCollectionPopup : BasePopup
{
    public delegate void NewCollectionPopupButtonEvent();
    public event NewCollectionPopupButtonEvent OKButtonPressed;

    [SerializeField]
    private UISprite bg;
    public CollectionPopupOKButton okButtonManager;
    public UILabel completeLabel;
    public UILabel collectionNameLabel;

    public void Init(CollectionData collectionData)
    {
        gameObject.transform.localScale = Vector3.zero;
        completeLabel.color = collectionData.color;
        collectionNameLabel.color = collectionData.color;
        collectionNameLabel.text = collectionData.name.ToUpper();

        HSBColor hsbColor = HSBColor.FromColor(collectionData.bgColor);
        hsbColor.b = hsbColor.b - 0.08f;
        bg.color = hsbColor.ToColor();
        
    }

    public override void Open(float time)
    {
        gameObject.SetActive(true);
        gameObject.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
        gameObject.transform.DOScale(Vector3.one, time).SetEase(Ease.OutBack).OnComplete(test);
    }

    private void test()
    {
    }

    public void OnPressOKButton()
    {
        okButtonManager.button.SetButtonActive(false);
        Close(0.2f);

        if (OKButtonPressed != null) OKButtonPressed();
    }

    public override void Close(float time)
    {
        gameObject.transform.DOScale(Vector3.zero, time).SetEase(Ease.InBack).OnComplete(OnCompleteClose);
    }

    private void OnCompleteClose()
    {
        gameObject.SetActive(false);
        //GameObject.Destroy(gameObject);

        this.EndClose();
    }
}
