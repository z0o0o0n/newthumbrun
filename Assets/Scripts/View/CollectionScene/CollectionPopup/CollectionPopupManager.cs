﻿using UnityEngine;
using System.Collections;
using popupManager;
using System.Collections.Generic;

public class CollectionPopupManager : MonoBehaviour 
{
    public delegate void GachaPopupMnagerEvent();
    public event GachaPopupMnagerEvent On_End;
    
    public LevelUpPointDisplayer _levelUpPointDisplayer;
    public AudioSource audioSource;
    public AudioClip collectedCharacter;
    public AudioClip completeDiorama;

    private bool _isDebug = true;
    private BasePopup _currentPopup;
    private PopupManager _popupManager;
    private CollectionPopupCreator _popupCreator;

    void Awake()
    {
        _popupManager = GetComponent<PopupManager>();
        _popupCreator = GetComponent<CollectionPopupCreator>();
    }

	void Start () 
    {
	
	}

	void Update () 
    {
	
	}

    public void CreatePopups(List<CollectionData> completedCollectionIDList, List<CharacterData> collectedCharacterIDList)
    {
        if(_isDebug) LogCollectedCharacterIDList(collectedCharacterIDList); //for Log
        CreateCollectionPopup(completedCollectionIDList);
        CreateCharacterPopup(collectedCharacterIDList);
    }

    private void LogCollectedCharacterIDList(List<CharacterData> collectedCharacterIDList)
    {
        string result = "CollectionPopupManager - collectedCharacterIDList [" + collectedCharacterIDList.Count + "] : ";
        for (int i = 0; i < collectedCharacterIDList.Count; i++)
        {
            if (i == 0) result += collectedCharacterIDList[i].id;
            else result += ", " + collectedCharacterIDList[i].id;
        }
        if(_isDebug) Debug.Log(result);
    }

    private void CreateCollectionPopup(List<CollectionData> completedCollectionIDList)
    {
        for(int i = 0; i < completedCollectionIDList.Count; i++)
        {
            NewCollectionPopup popup = _popupCreator.CreateNewCollectionPopup(completedCollectionIDList[i]) as NewCollectionPopup;
            _popupManager.PushPopupToCueue(popup);
        }
    }

    private void CreateCharacterPopup(List<CharacterData> collectedCharacterIDList)
    {
        for (int i = 0; i < collectedCharacterIDList.Count; i++)
        {
            NewCharacterPopup popup = _popupCreator.CreateNewCharacterPopup(collectedCharacterIDList[i]) as NewCharacterPopup;
            _popupManager.PushPopupToCueue(popup);
        }
    }

    public void OpenPopup()
    {
        Open();
    }

    private void Open()
    {
        if (_isDebug) Debug.Log("GachaPopupManager.Open()");
        _popupManager.Opened += OnPopupOpened;
        _popupManager.On_End += OnEndPopupManager;
        _popupManager.Open();
    }

    private void OnPopupOpened(BasePopup popup)
    {
        _currentPopup = popup;
        char[] separator = {'_'};
        string[] splitedNameList = _currentPopup.name.Split(separator);
        string popupType = splitedNameList[0];

        Debug.Log("==================== popup : " + _currentPopup.name + " / popupType: " + popupType);

        if(popupType == "NewCollection")
        {
            NewCollectionPopup newCollectionPopup = _currentPopup as NewCollectionPopup;
            newCollectionPopup.OKButtonPressed += OnNewCollectionPopupOKButtonPressed;
            //_levelUpPointDisplayer.ObtainPoint(popup.transform.position);
            audioSource.clip = completeDiorama;
        }
        else if(popupType == "NewCharacter")
        {
            audioSource.clip = collectedCharacter;
        }

        audioSource.Play();
    }

    private void OnNewCollectionPopupOKButtonPressed()
    {
        NewCollectionPopup newCollectionPopup = _currentPopup as NewCollectionPopup;
        newCollectionPopup.OKButtonPressed -= OnNewCollectionPopupOKButtonPressed;
        _levelUpPointDisplayer.ObtainPoint(newCollectionPopup.okButtonManager.icon.transform.position);
    }

    private void OnEndPopupManager()
    {
        _popupManager.On_End -= OnEndPopupManager;
        if (On_End != null) On_End();
    }

    void OnDestory()
    {
        NewCollectionPopup newCollectionPopup = _currentPopup as NewCollectionPopup;
        newCollectionPopup.OKButtonPressed -= OnNewCollectionPopupOKButtonPressed;
    }
}
