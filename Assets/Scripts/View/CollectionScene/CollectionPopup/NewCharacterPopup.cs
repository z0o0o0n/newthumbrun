﻿using UnityEngine;
using System.Collections;
using popupManager;
using DG.Tweening;

public class NewCharacterPopup : BasePopup 
{
    public UISprite bg;
    public UISprite button;
    public UISprite character;
    public CharacterTypeIcon typeIcon;
    public UILabel nameLabel;
    public UILabel collectionNameLabel;

    public void Init(CharacterData characterData)
    {
        character.spriteName = "CharacterProfileImage_" + characterData.id;
        typeIcon.ChangeType(characterData.type);
        nameLabel.text = characterData.name;
        collectionNameLabel.text = CollectionDataManager.instance.GetCollectionDataByCollectionID(characterData.collectionID).name;
        bg.color = characterData.bgColor;
        gameObject.transform.localScale = Vector3.zero;
    }

    public override void Open(float time)
    {
        gameObject.SetActive(true);
        gameObject.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
        gameObject.transform.DOScale(Vector3.one, time).SetEase(Ease.OutBack);
    }

    public void OnPressOKButton()
    {
        button.enabled = false;
        Close(0.2f);
    }

    public override void Close(float time)
    {
        gameObject.transform.DOScale(Vector3.zero, time).SetEase(Ease.InBack).OnComplete(OnCompleteClose);
    }

    private void OnCompleteClose()
    {
        gameObject.SetActive(false);
        //GameObject.Destroy(gameObject);

        this.EndClose();
    }
}
