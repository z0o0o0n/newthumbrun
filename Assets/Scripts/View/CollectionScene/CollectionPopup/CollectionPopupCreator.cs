﻿using UnityEngine;
using System.Collections;
using popupManager;

public class CollectionPopupCreator : MonoBehaviour 
{
    public GameObject newCharacterPopupGO;
    public GameObject newCollectionPopupGO;
    public GameObject popupContainer;

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}

    public BasePopup CreateNewCharacterPopup(CharacterData characterData)
    {
        GameObject instance = GameObject.Instantiate(newCharacterPopupGO) as GameObject;
        instance.transform.parent = popupContainer.transform;
        instance.transform.localPosition = Vector3.zero;
        NewCharacterPopup popup = instance.GetComponent<NewCharacterPopup>();
        popup.name = "NewCharacter_" + characterData.id;
        popup.Init(characterData);
        return popup;
    }

    public BasePopup CreateNewCollectionPopup(CollectionData collectionData)
    {
        GameObject instance = GameObject.Instantiate(newCollectionPopupGO) as GameObject;
        instance.transform.parent = popupContainer.transform;
        instance.transform.localPosition = Vector3.zero;
        NewCollectionPopup popup = instance.GetComponent<NewCollectionPopup>();
        popup.name = "NewCollection_" + collectionData.id;
        popup.Init(collectionData);
        return popup;
    }
}
