﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CollectionNameLabel : MonoBehaviour 
{
    public UILabel label;

    private float _posX;

	void Start () 
    {
        Hide(0);
	}

    public void Hide(float time)
    {
        //transform.DOLocalMoveY(-50, time).SetEase(Ease.InCubic);
    }

    public void Show(float time)
    {
        //transform.DOLocalMoveY(50, time).SetEase(Ease.OutCubic);
    }
	
	void Update () 
    {
	
	}

    public void Change(string name)
    {
        label.text = name + " COLLECTION";
        //transform.DOLocalMoveX(_posX - 20, 0.1f).SetEase(Ease.InOutCubic);
        //transform.DOLocalMoveX(_posX, 0.3f).SetDelay(0.1f).SetEase(Ease.OutBack);
    }
}
