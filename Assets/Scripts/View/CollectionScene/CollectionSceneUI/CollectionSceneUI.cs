﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using view.mapScene;

public class CollectionSceneUI : MonoBehaviour 
{
    public delegate void UICallbackDele();
    public delegate void CollectionSceneUIEvent();
    public event CollectionSceneUIEvent HomeButtonClick;
    public event CollectionSceneUIEvent On_PressLeftArrowButton;
    public event CollectionSceneUIEvent On_PressRightArrowButton;
    public event CollectionSceneUIEvent On_PressLevelUpPointButton;
    public event CollectionSceneUIEvent On_ClosedBonusAbilityPopup;
    public delegate void UseButtonEvent(int id);
    public event UseButtonEvent On_PressUseButton;

    public CollectionModelNavigator navigator;
    //public CollectionNameLabel collectionNameLabel;
    public CollectionInfoPanel collectionInfoPanel;
    public UIBasicButton helpButton;
    public UIBasicButton homeButton;
	public CharacterInfoPanel characterInfoPanel;
    public UIDisplayMode.mode displayMode
    {
        get { return _displayMode; }
    }
    public TipBook collectionTutorialBook;

    public InUseBubble _inUseBubble;
    public TouchArea _touchArea;

    private UIDisplayMode.mode _displayMode = UIDisplayMode.mode.Extension;
    private bool _isShow = false;
    private UICallbackDele _displayModeChangeCallback;

    void Awake()
    {
        navigator.On_PressLeftArrow += OnPressLeftArrow;
        navigator.On_PressRightArrow += OnPressRightArrow;
        homeButton.On_Click += OnHomeButtonClick;
        characterInfoPanel.On_PressOKButton += OnPressOKButton;
        characterInfoPanel.On_PressUseButton += OnPressUseButton;

        float changeTime = 0;
        ChangeExtendDisplay(changeTime);
    }

    private void OnPressLeftArrow()
    {
        On_PressLeftArrowButton();
    }

    private void OnPressRightArrow()
    {
        On_PressRightArrowButton();
    }

    private void OnCloseButtonClick()
    {
        if (On_ClosedBonusAbilityPopup != null) On_ClosedBonusAbilityPopup();
    }

    private void OnClickLevelUpPointButton()
    {
        if (On_PressLevelUpPointButton != null) On_PressLevelUpPointButton();
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}

    public void OnHomeButtonClick()
    {
        if (HomeButtonClick != null) HomeButtonClick();
    }

    private void OnPressOKButton()
	{
	}

	private void OnPressUseButton(int characterID)
	{
        if (On_PressUseButton != null) On_PressUseButton(characterID);
	}

    public void ChangeExtendDisplay(float time)
    {
        _displayMode = UIDisplayMode.mode.Extension;
        ReplaceNavigator(Screen.width - 50f, time);
        ReplaceNameLabel(0, time);
        ReplaceHomeButton(-50f, time);
    }

    private void ReplaceNavigator(float posX, float time)
    {
        float visiblePosX;
        if (navigator.isShow) visiblePosX = 0;
        else visiblePosX = 70;
        navigator.rightArrowButton.transform.DOLocalMoveX(posX + visiblePosX, time).SetEase(Ease.InOutCubic);
        navigator.SetRightButtonDefaultPosX(posX);
    }

    private void ReplaceNameLabel(float posX, float time)
    {
        collectionInfoPanel.transform.DOLocalMoveX(posX, time).SetEase(Ease.InOutCubic);
    }

    private void ReplaceHomeButton(float posX, float time)
    {
        homeButton.transform.DOLocalMoveX(posX, time).SetEase(Ease.InOutCubic).OnComplete(OnCompleteDisplayModeChange);
    }

    private void OnCompleteDisplayModeChange()
    {
        if (_displayModeChangeCallback != null)
        {
            _displayModeChangeCallback();
            _displayModeChangeCallback = null;
        }
    }

    public void ChangeShrinkDisplay(float time, UICallbackDele completeCallback = null)
    {
        _displayModeChangeCallback = completeCallback;

        _displayMode = UIDisplayMode.mode.Shrink;
        ReplaceNavigator((Screen.width - GetPanelWidth()) - 50f, time);
        ReplaceNameLabel(-(GetPanelWidth() / 2), time);
        ReplaceHomeButton(-(GetPanelWidth() + 50f), time);
    }

    private float GetPanelWidth()
    {
        return 320f;
    }

    public void ShowUI(float time)
    {
        _isShow = true;

        navigator.Show(time);
        collectionInfoPanel.Show();
        helpButton.gameObject.SetActive(true);
        homeButton.gameObject.SetActive(true);
        _inUseBubble.Show(time);
        _touchArea.gameObject.SetActive(true);
    }

    public void HideUI(float time)
    {
        _isShow = false;

        navigator.Hide(time);
        collectionInfoPanel.Hide();
        helpButton.gameObject.SetActive(false);
        homeButton.gameObject.SetActive(false);
        _inUseBubble.Hide(time);
        _touchArea.gameObject.SetActive(false);
    }

    public void ShowLevelUpPointDisplayer(float time)
    {
    }

    public void HideLevelUpPointDisplayer(float time)
    {
     
    }

	public void OpenInfoPanel(CharacterData characterData, CollectionData collectionData, ButtonAreaType.type buttonAreaType, bool isUsed)
	{
        Debug.Log("buttonAreaType -----------------------:  " + buttonAreaType);
        characterInfoPanel.SetData(characterData, collectionData, buttonAreaType, isUsed);
        characterInfoPanel.Show(0.3f);
	}

    public void CloseInfoPanel(float time)
    {
        characterInfoPanel.Hide(time);
    }

    public void ShowBonusAbilityPopup(float time)
    {
        CloseInfoPanel(time);
        HideUI(time);
        ChangeExtendDisplay(time);
    }

    public void SetButtonActive(bool isActivation)
    {
        helpButton.SetButtonActive(isActivation);
        homeButton.SetButtonActive(isActivation);
    }

    public void ChangeInUseBubbleTarget(GameObject target, Camera camera)
    {
        _inUseBubble.ChangeTarget(target, camera);
    }

    void OnDestroy()
    {
        navigator.On_PressLeftArrow -= OnPressLeftArrow;
        navigator.On_PressRightArrow -= OnPressRightArrow;
        homeButton.On_Click -= OnHomeButtonClick;
        characterInfoPanel.On_PressOKButton -= OnPressOKButton;
        characterInfoPanel.On_PressUseButton -= OnPressUseButton;
    }
}
