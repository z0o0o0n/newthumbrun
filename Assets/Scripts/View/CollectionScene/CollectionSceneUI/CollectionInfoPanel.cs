﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class CollectionInfoPanel : MonoBehaviour 
{
    public UILabel nameLabel;
    public List<AbilityRewardDisplay> abilityRewardDisplayList;
    public UISprite completeIcon;

    private bool _isComplete = false;

	void Start () 
	{
        //DimRewardDisplay();
	}
	
	void Update () 
	{
	
	} 

    public void ChangeData(string collecitonName, CharacterStatusData statusData, bool isComplete)
    {
        completeIcon.gameObject.SetActive(false);
        nameLabel.text = collecitonName;
        _isComplete = isComplete;
        //InitAbilityRewardDisplay(statusData);
        if (_isComplete)
        {
            //DistinctRewardDisplay();
            completeIcon.gameObject.SetActive(true);
            completeIcon.transform.DOLocalMoveX(nameLabel.transform.localPosition.x + (nameLabel.width / 2) + 30, 0);
        }
    }

    public void Show()
    {
        nameLabel.gameObject.SetActive(true);
        if(_isComplete) completeIcon.gameObject.SetActive(true);
    }

    public void Hide()
    {
        nameLabel.gameObject.SetActive(false);
        completeIcon.gameObject.SetActive(false);
    }

    //private void InitAbilityRewardDisplay(CharacterStatusData statusData)
    //{
    //    DimRewardDisplay();
    //    HideAbilityRewardDisplays();

    //    int count = 0;
    //    for(int i = 0; i < statusData.abilityList.Count; i++)
    //    {
    //        if(statusData.abilityList[i].value != 0)
    //        {
    //            abilityRewardDisplayList[count].gameObject.SetActive(true);
    //            abilityRewardDisplayList[count].SetIcon(statusData.abilityList[i].name);
    //            abilityRewardDisplayList[count].SetValue(statusData.abilityList[i].value);
    //            count++;
    //            if (count >= 3) return;
    //        }
    //    }
    //}

    //private void HideAbilityRewardDisplays()
    //{
    //    for(int i = 0; i < abilityRewardDisplayList.Count; i++)
    //    {
    //        abilityRewardDisplayList[i].gameObject.SetActive(false);
    //    }
    //}

    //private void DimRewardDisplay()
    //{
    //    for (int i = 0; i < abilityRewardDisplayList.Count; i++)
    //    {
    //        abilityRewardDisplayList[i].Dim();
    //    }
    //}

    //private void DistinctRewardDisplay()
    //{
    //    for (int i = 0; i < abilityRewardDisplayList.Count; i++)
    //    {
    //        abilityRewardDisplayList[i].Distinct();
    //    }
    //}
}
