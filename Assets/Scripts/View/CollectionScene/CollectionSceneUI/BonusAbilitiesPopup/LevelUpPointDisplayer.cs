﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Junhee.Utils;

public class LevelUpPointDisplayer : MonoBehaviour 
{
    public delegate void LevelUpPointDisplayEvent();
    public event LevelUpPointDisplayEvent On_ClickButton;

    public float widthAmount = 0.0f;
    public bool isOnlyDisplayer = false;

    public UIBasicButton _button;
    public UISprite _icon;
    public UISprite _multiplicationMark;
    public UILabel _valueLabel;
    public AnimationCurve _customElastic;
    public AudioClip _buttonPressAudio;
    public AudioClip _increaseAudio;

    private bool _isButtonMode = false;
    private UIWidget _widget;
    private BonusAbilityModel _bonusAbilityModel;
    private LUPObtainmentMotionManager _pointObtainmentMotionManager;
    private int _value;
    private AudioSource _audioSource;

    void Awake ()
    {
        _widget = GetComponent<UIWidget>();
        _audioSource = GetComponent<AudioSource>();
        _pointObtainmentMotionManager = GetComponent<LUPObtainmentMotionManager>();

        ReplaceElements();

        if (!isOnlyDisplayer) ChangeButtonMode();
        else ChangeIconMode();
    }

	void Start () 
	{

	}

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            _pointObtainmentMotionManager.Obtain(Vector2.zero, _icon.transform.position);
        }
    }

    public void Init(BonusAbilityModel bonusAbilityModel)
    {
        Debug.Log("LevelUpPointDisplay - Init() / name: " + gameObject.name);
        _bonusAbilityModel = bonusAbilityModel;
        _bonusAbilityModel.LevelUpPointChanged += OnLevelUpPointChanged;
        _bonusAbilityModel.LevelUpPointIncreased += OnLevelUpPointIncreased;
        _bonusAbilityModel.LevelUpPointDecreased += OnLevelUpPointDecreased;

        _pointObtainmentMotionManager.Ended += OnObtainmentMotionEnded;

        SetValue(_bonusAbilityModel.levelUpPoint);
    }

    private void OnLevelUpPointChanged()
    {
        SetValue(_bonusAbilityModel.levelUpPoint);
    }

    private void OnLevelUpPointIncreased()
    {
        if (!isOnlyDisplayer)
        {
            _button.transform.localScale = new Vector3(0f, 0f, 1);
            DOTween.Kill("LevelUpPointDisplayer_Button_Scale");
            _button.transform.DOScale(1, 0.5f).SetId("LevelUpPointDisplayer_Button_Scale").SetEase(_customElastic);

            PlayLabelIncreseMotion();
        }
        else
        {
            _icon.transform.localScale = new Vector3(0f, 0f, 1);
            DOTween.Kill("LevelUpPointDisplayer_Icon_Scale");
            _icon.transform.DOScale(1, 0.5f).SetId("LevelUpPointDisplayer_Icon_Scale").SetEase(_customElastic);

            PlayLabelIncreseMotion();
        }

        if(_audioSource)
        {
            _audioSource.clip = _increaseAudio;
            _audioSource.Play();
        }
    }

    private void OnLevelUpPointDecreased()
    {
        if (!isOnlyDisplayer)
        {
            _button.transform.localScale = new Vector3(0f, 0f, 1);
            DOTween.Kill("LevelUpPointDisplayer_Button_Scale");
            _button.transform.DOScale(1, 0.5f).SetId("LevelUpPointDisplayer_Button_Scale").SetEase(_customElastic);

            PlayLabelDecreaseMotoin();
        }
        else
        {
            _icon.transform.localScale = new Vector3(0f, 0f, 1);
            DOTween.Kill("LevelUpPointDisplayer_Icon_Scale");
            _icon.transform.DOScale(1, 0.5f).SetId("LevelUpPointDisplayer_Icon_Scale").SetEase(_customElastic);

            PlayLabelDecreaseMotoin();
        }
    }

    private void PlayLabelDecreaseMotoin()
    {
        float inTime = 0.08f;
        float outTime = 0.5f;
        float distanceY = -15f;
        _valueLabel.transform.DOLocalMoveY(distanceY, inTime).SetEase(Ease.OutSine);
        _valueLabel.transform.DOLocalMoveY(0, outTime).SetDelay(inTime).SetEase(Ease.OutElastic);
    }

    private void PlayLabelIncreseMotion()
    {
        float inTime = 0.08f;
        float outTime = 0.5f;
        float distanceY = 15f;
        _valueLabel.transform.DOLocalMoveY(distanceY, inTime).SetEase(Ease.OutSine);
        _valueLabel.transform.DOLocalMoveY(0, outTime).SetDelay(inTime).SetEase(Ease.OutElastic);
    }

    private void OnObtainmentMotionEnded()
    {
        _bonusAbilityModel.Increase();
    }

    //public int GetValue()
    //{
    //    return _value;
    //}

    private void SetValue(int value)
    {
        _value = value;
        UpdateValue();
        if (!isOnlyDisplayer)
        {
            if (!_isButtonMode && _value > 0)
            {
                ChangeButtonMode();
            }
            else if (_isButtonMode && _value <= 0)
            {
                ChangeIconMode();
            }
        }
    }

    public void ObtainPoint(Vector2 startPosition)
    {
        Debug.Log("LevelUpPointDisplayer - ObtainPoint() / startPosition: " + startPosition.ToString());
        _pointObtainmentMotionManager.Obtain(startPosition, _icon.transform.position);
    }

    public void ChangeButtonMode()
    {
        _isButtonMode = true;
        _button.gameObject.SetActive(true);
        _button.On_Click += OnButtonClick;
        _icon.gameObject.SetActive(false);
    }

    private void OnButtonClick()
    {
        if(_audioSource)
        {
            _audioSource.clip = _buttonPressAudio;
            _audioSource.Play();
        }
        if (On_ClickButton != null) On_ClickButton();
    }

    public void ChangeIconMode()
    {
        _isButtonMode = false;
        _button.gameObject.SetActive(false);
        _button.On_Click -= OnButtonClick;
        _icon.gameObject.SetActive(true);
    }

    public void Show(float time)
    {
        DOTween.Kill("LevelUpPointDisplayer_ShowAndHide");
        DOTween.To(() => _widget.alpha, x => _widget.alpha = x, 1, time).SetId("LevelUpPointDisplayer_ShowAndHide");
    }

    public void Hide(float time)
    {
        DOTween.Kill("LevelUpPointDisplayer_ShowAndHide");
        DOTween.To(() => _widget.alpha, x => _widget.alpha = x, 0, time).SetId("LevelUpPointDisplayer_ShowAndHide"); ;
    }

    public void SetButtonActive(bool isActivation)
    {
        _button.SetButtonActive(isActivation);
    }

    void OnDestroy()
    {
        _button.On_Release -= OnButtonClick;

        _bonusAbilityModel.LevelUpPointChanged -= OnLevelUpPointChanged;
        _bonusAbilityModel.LevelUpPointIncreased -= OnLevelUpPointIncreased;
        _bonusAbilityModel.LevelUpPointDecreased -= OnLevelUpPointDecreased;

        _pointObtainmentMotionManager.Ended -= OnObtainmentMotionEnded;
    }


    


    private void UpdateValue()
    {
        _valueLabel.text = _value.ToString();
        ReplaceElements();
    }

    private void ReplaceElements()
    {
        float iconW = _icon.width;
        float labelW = _valueLabel.width;
        float spaceW = 10;
        widthAmount = iconW + spaceW + labelW;
        //Debug.LogWarning("------ " + iconW + " / " + spaceW + " / " + labelW);

        _icon.transform.DOLocalMove(new Vector3(0, 0, 0), 0);
        _button.transform.DOLocalMove(new Vector3(0, 0, 0), 0);

        //float xpos = -(widthAmount / 2) + iconW;
        _valueLabel.transform.DOLocalMove(new Vector3(iconW / 2 + spaceW, _valueLabel.transform.localPosition.y, 0), 0);
    }
}
