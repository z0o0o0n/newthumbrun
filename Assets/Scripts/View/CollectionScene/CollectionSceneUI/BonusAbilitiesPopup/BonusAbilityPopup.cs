﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BonusAbilityPopup : MonoBehaviour 
{
    public delegate void BonusAbilityPopupEvent();
    public event BonusAbilityPopupEvent CloseButtonClick;
    public event BonusAbilityPopupEvent Closed;

    public AbilitiesManager _abilitiesManager;
    public LevelUpButton _levelUpButton;
    public UIBasicButton _closeButton;
    public AnimationCurve _customElastic;
    public GameObject _descriptionEN;
    public GameObject _descriptionKR;
    public GameObject _descriptionJP;

    private bool _isDebug = true;
    private BonusAbilityModel _bonusAbilityModel;
    private CharacterAbilityType.type _selectedAbilityType = CharacterAbilityType.type.none;

    void Awake ()
    {
        _bonusAbilityModel = BonusAbilityModel.instance;

        Reset();
    }

	void Start () 
	{
        if (GameManager.phoneLanguage == SystemLanguage.Korean)
        {
            if (_descriptionKR == null) _descriptionEN.SetActive(true);
            else _descriptionKR.SetActive(true);
        }
        else if (GameManager.phoneLanguage == SystemLanguage.Japanese)
        {
            if (_descriptionJP == null) _descriptionEN.SetActive(true);
            else _descriptionJP.SetActive(true);
        }
        else
        {
            _descriptionEN.SetActive(true);
        }
	}
	
	void Update () 
	{
	
	}

    public void Init()
    {
        int levelUpPoint = GameData.instance.GetConfigData().LevelUpPoint;

        _abilitiesManager.AbilityButtonSelected += OnAbilityButtonSelected;
        _abilitiesManager.GaugeFulled += OnAbilityGaugeFulled;
        //_abilitiesManager.Init(_bonusAbilityModel.GetAbilityStepData());

        _levelUpButton.Click += OnLevelUpButtonClick;
        _levelUpButton.Deactivate();

        _closeButton.On_Click += OnCloseButtonClick;
    }

    private void OnAbilityButtonSelected(CharacterAbilityType.type type)
    {
        _selectedAbilityType = type;

        if (_isDebug) Debug.Log("BonusAbilitiesPopup - OnAbilityButtonSelected() type: " + _selectedAbilityType);
        if (_levelUpButton.state == LevelUpButton.stateType.Deactivation) _levelUpButton.Activate();
    }

    private void OnAbilityGaugeFulled(CharacterAbilityType.type type)
    {
        if (_isDebug) Debug.Log("BonusAbilitiesPopup - OnAbilityGaugeFulled() tpye: " + type);
        _selectedAbilityType = CharacterAbilityType.type.none;
        _levelUpButton.Deactivate();
    }

    private void OnLevelUpButtonClick()
    {
        if (_isDebug) Debug.Log("BonusAbilitiesPopup - OnLevelUpButtonReleased()");
        if (_selectedAbilityType != CharacterAbilityType.type.none)
        {
            LevelUp(_selectedAbilityType);
            ConsumptionLevelUpPoint();
        }
    }

    private void LevelUp(CharacterAbilityType.type type)
    {
        // if (type == CharacterAbilityType.type.run) _bonusAbilityModel.run += 1;
        // else if (type == CharacterAbilityType.type.jump) _bonusAbilityModel.jump += 1;
        // else if (type == CharacterAbilityType.type.boostAbility) _bonusAbilityModel.boostAbility += 1;
        //else if (type == CharacterAbilityType.type.boostCharging) _bonusAbilityModel.boostCharging += 1;
        //else if (type == CharacterAbilityType.type.boostConsumption) _bonusAbilityModel.boostConsumption += 1;

        // _abilitiesManager.SetAbilitiesStep(_bonusAbilityModel.GetAbilityStepData());
    }

    private void ConsumptionLevelUpPoint()
    {
        //_bonusAbilityModel.levelUpPoint = _levelUpPointDisplay.GetValue() - 1;
        _bonusAbilityModel.Decrease();
        if (_bonusAbilityModel.levelUpPoint <= 0)
        {
            _abilitiesManager.Deactivate();
            _levelUpButton.Deactivate();
        }
    }

    public void OnCloseButtonClick()
    {
        if (CloseButtonClick != null) CloseButtonClick();
    }

    public void Show(float time)
    {
        this.transform.DOScale(1, time).SetEase(_customElastic);
    }

    public void Hide(float time)
    {
        this.transform.DOScale(0, time).SetEase(Ease.InBack).OnComplete(OnHided);
    }

    private void OnHided()
    {
        Reset();
        if (Closed != null) Closed();
    }

    private void Reset()
    {
        this.transform.localScale = Vector2.zero;
        _selectedAbilityType = CharacterAbilityType.type.none;
        _abilitiesManager.Reset();
        _levelUpButton.Deactivate();
    }

    void OnDestroy()
    {
        _closeButton.On_Click -= OnCloseButtonClick;

        _levelUpButton.Click -= OnLevelUpButtonClick;
    }
}
