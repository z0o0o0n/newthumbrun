﻿using UnityEngine;
using System.Collections;

public class BonusAbilityModel : BaseSingleton<BonusAbilityModel>
{
    public delegate void BonusAbilityModelEvent();
    public event BonusAbilityModelEvent LevelUpPointChanged;
    public event BonusAbilityModelEvent LevelUpPointIncreased;
    public event BonusAbilityModelEvent LevelUpPointDecreased;

    public int levelUpPoint
    {
        get { return GameData.instance.GetConfigData().LevelUpPoint; }
        set 
        {
            if (value != GameData.instance.GetConfigData().LevelUpPoint)
            {
                GameData.instance.GetConfigData().LevelUpPoint = value;
                if (LevelUpPointChanged != null) LevelUpPointChanged();
            }
        }
    }

    // public int run
    // {
    //     get { return GameData.instance.GetBonusAbilityData().run; }
    //     set { GameData.instance.GetBonusAbilityData().run = value; }
    // }
    // public int jump
    // {
    //     get { return GameData.instance.GetBonusAbilityData().jump; }
    //     set { GameData.instance.GetBonusAbilityData().jump = value; }
    // }
    // public int boostAbility
    // {
    //     get { return GameData.instance.GetBonusAbilityData().boostAbility; }
    //     set { GameData.instance.GetBonusAbilityData().boostAbility = value; }
    // }
    //public int boostCharging
    //{
    //    get { return GameData.instance.GetBonusAbilityData().boostCharging; }
    //    set { GameData.instance.GetBonusAbilityData().boostCharging = value; }
    //}
    //public int boostConsumption
    //{
    //    get { return GameData.instance.GetBonusAbilityData().boostConsumption; }
    //    set { GameData.instance.GetBonusAbilityData().boostConsumption = value; }
    //}

    // public AbilityStepData GetAbilityStepData()
    // {
    //     // AbilityStepData result = new AbilityStepData();
    //     // result.run = this.run;
    //     // result.jump = this.jump;
    //     // result.boostAbility = this.boostAbility;
    //     //result.boostCharging = this.boostCharging;
    //     //result.boostConsumption = this.boostConsumption;
    //     return null;
    // }

    public void Increase()
    {
        levelUpPoint += 1;
        if (LevelUpPointIncreased != null) LevelUpPointIncreased();
    }

    public void Decrease()
    {
        if (levelUpPoint > 0)
        {
            levelUpPoint -= 1;
            if (LevelUpPointDecreased != null) LevelUpPointDecreased();
        }
    }
}
