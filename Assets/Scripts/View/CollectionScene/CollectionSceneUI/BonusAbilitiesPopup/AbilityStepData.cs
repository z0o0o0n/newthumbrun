﻿using UnityEngine;
using System.Collections;

public class AbilityStepData 
{
    public int run;
    public int jump;
    public int boostAbility;
    //public int boostCharging;
    //public int boostConsumption;

    public AbilityStepData(int run = 0, int jump = 0, int boostAbility = 0)
    {
        this.run = run;
        this.jump = jump;
        this.boostAbility = boostAbility;
        //this.boostCharging = boostCharging;
        //this.boostConsumption = boostConsumption;
    }

    public string ToString()
    {
        string result = "<Ability Step Data> \n";
        result += "run: " + run + " \n";
        result += "jump: " + jump + " \n";
        result += "boostAbility " + boostAbility + " \n";
        //result += "boostCharging: " + boostCharging + " \n";
        //result += "boostConsumption: " + boostConsumption + " \n";
        return result;
    }
}
