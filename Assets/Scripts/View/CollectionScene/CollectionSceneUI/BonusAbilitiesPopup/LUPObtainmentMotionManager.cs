﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LUPObtainmentMotionManager : MonoBehaviour 
{
    public delegate void LUPObtainmentMotionEvent();
    public event LUPObtainmentMotionEvent Started;
    public event LUPObtainmentMotionEvent Ended;

    public GameObject _obtainIconPrefab;
    public AnimationCurve _customEalstic;

    private bool _isDebug = true;

	void Start () 
	{
	    
	}
	
	void Update () 
	{
	}

    private void OnMotionEnded()
    {
        if (Ended != null) Ended();
    }

    public void Obtain(Vector2 startPosition, Vector2 endPosition)
    {
        if (_isDebug) Debug.Log("LUPOptainmentMotionManager - Optain()");

        GameObject clone = GameObject.Instantiate(_obtainIconPrefab);
        clone.transform.parent = transform;
        clone.transform.position = startPosition;
        clone.transform.localScale = Vector3.one;

        LevelUpPoint levelUpPoint = clone.GetComponent<LevelUpPoint>();
        levelUpPoint.Obtain(endPosition, OnMotionEnded);
    }

    private void StartMotion()
    {

    }
}
