﻿using UnityEngine;
using System.Collections;

public class LevelUpButton : MonoBehaviour 
{
    public delegate void LevelUpButtonEvent();
    //public event LevelUpButtonEvent Pressed;
    //public event LevelUpButtonEvent Released;
    public event LevelUpButtonEvent Click;
    //public UIButtonEventDispatcher buttonDispatcher;
    public enum stateType { Activation, Pressure, Deactivation }
    public stateType state;

    public UIBasicButton _button;
    public UISprite _deactivatedSprite;

    void Awake ()
    {
        _button.On_Click += OnButtonClick;
        //buttonDispatcher.On_ButtonPress += OnButtonPress;
        Activate();
    }

	void Start () 
	{
	    
	}
	
	void Update () 
	{
	    
	}

    private void OnButtonClick()
    {
        if (Click != null) Click();
        //if(isDown)
        //{
        //    Press();
        //    if (Pressed != null) Pressed();
        //}
        //else
        //{
        //    Activate();
        //    if (Released != null) Released();
        //}
    }

    public void Activate()
    {
        state = stateType.Activation;
        _button.gameObject.SetActive(true);
        //_button.spriteName = "LevelUpButton_Normal";
        _deactivatedSprite.gameObject.SetActive(false);
    }

    public void Deactivate()
    {
        state = stateType.Deactivation;
        _button.gameObject.SetActive(false);
        _deactivatedSprite.gameObject.SetActive(true);
    }

    public void Press()
    {
        state = stateType.Pressure;
        _button.gameObject.SetActive(true);
        //_button.spriteName = "LevelUpButton_Press";
        _deactivatedSprite.gameObject.SetActive(false);
    }

    void OnDestroy()
    {
        _button.On_Click -= OnButtonClick;
    }
}
