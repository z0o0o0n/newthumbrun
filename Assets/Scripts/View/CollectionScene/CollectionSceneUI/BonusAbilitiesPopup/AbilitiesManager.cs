﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AbilitiesManager : MonoBehaviour 
{
    public delegate void AbilityButtonEvent(CharacterAbilityType.type type);
    public event AbilityButtonEvent AbilityButtonSelected;
    public event AbilityButtonEvent GaugeFulled;

    public BonusAbilitiesButton[] _buttonList;
    public AbilityGauge[] _gaugeList;

    private bool _isDebug = true;
    private bool _isActivated = true;
    private CharacterAbilityType.type _selectedAbilityType = CharacterAbilityType.type.none;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void Init(AbilityStepData data)
    {
        InitButtons();
        Activate();
        SetAbilitiesStep(data);
    }

    private void InitButtons()
    {
        if (_isDebug) Debug.Log("AbilitiesManager - InitButtons()");
        for(int i = 0; i < _buttonList.Length; i++)
        {
            int id = i;
            _buttonList[i].Init(id, true);
            _buttonList[i].On_Selected += OnSelectedButton;
        }
    }

    private void OnSelectedButton(int id)
    {
        if (_isDebug) Debug.Log("AbilitiesManager - OnSelectedButton id: " + id);

        if(id == 0) _selectedAbilityType = CharacterAbilityType.type.run;
        else if(id == 1) _selectedAbilityType = CharacterAbilityType.type.jump;
        else if (id == 2) _selectedAbilityType = CharacterAbilityType.type.boostAbility;
        //else if (id == 3) _selectedAbilityType = CharacterAbilityType.type.boostCharging;
        //else if (id == 4) _selectedAbilityType = CharacterAbilityType.type.boostConsumption;

        UpdateButtonStateByGaugeFull();

        if (AbilityButtonSelected != null) AbilityButtonSelected(_selectedAbilityType);
    }

    public void SetAbilitiesStep(AbilityStepData data)
    {
        _gaugeList[0].SetStep(data.run);
        _gaugeList[1].SetStep(data.jump);
        _gaugeList[2].SetStep(data.boostAbility);
        //_gaugeList[3].SetStep(data.boostCharging);
        //_gaugeList[4].SetStep(data.boostConsumption);

        if (_isActivated)
        {
            UpdateButtonStateByGaugeFull();
            //SelectButton(_selectedAbilityType);
        }
    }

    //private void SelectButton(CharacterAbilityType.type type)
    //{
    //    if (type == CharacterAbilityType.type.run)
    //    {
    //        _buttonList[0].Select();
    //        //_gaugeList[0].transform
    //    }
    //    else if (type == CharacterAbilityType.type.jump) _buttonList[1].Select();
    //    else if (type == CharacterAbilityType.type.boosAbility) _buttonList[2].Select();
    //    else if (type == CharacterAbilityType.type.boostCharging) _buttonList[3].Select();
    //    else if (type == CharacterAbilityType.type.boostConsumption) _buttonList[4].Select();
    //}

    /**
     * 능력치가 Full인지 확인하고 
     * Full일 경우 Button 비활성화
     * Full이 아니고 선택한 Button일 경우 선택상태로 변경
     * Full도 선택상태도 아닐 경우 Button 활성화
     */
    private void UpdateButtonStateByGaugeFull()
    {
        string log = "AbilitiesManager - CheckGaugeFull() \n";
        for(int i = 0; i < _gaugeList.Length; i++)
        {
            log += "gaugeList[" + i + "].IsFull(): " + _gaugeList[i].IsFull() + " \n";
            if (_gaugeList[i].IsFull())
            {
                _buttonList[i].Deactivate();
                if (_gaugeList[i].type == _selectedAbilityType)
                {
                    GaugeFulled(_selectedAbilityType);
                    _selectedAbilityType = CharacterAbilityType.type.none;
                }
            }
            else if (_gaugeList[i].type == _selectedAbilityType) _buttonList[i].Select();
            else _buttonList[i].Activate();
        }
        if (_isDebug) Debug.Log(log);
    }

    public void Activate()
    {
        _isActivated = true;

        UpdateButtonStateByGaugeFull();
        //SelectButton(_selectedAbilityType);
    }

    public void Deactivate()
    {
        _isActivated = false;
        for (int i = 0; i < _buttonList.Length; i++)
        {
            _buttonList[i].Deactivate();
        }
    }

    public void Reset()
    {
        _selectedAbilityType = CharacterAbilityType.type.none;

        Activate();
    }

    void OnDestroy()
    {
        ResetButtons();
    }

    private void ResetButtons()
    {
        for (int i = 0; i < _buttonList.Length; i++)
        {
            _buttonList[i].On_Selected -= OnSelectedButton;
        }
    }
}
