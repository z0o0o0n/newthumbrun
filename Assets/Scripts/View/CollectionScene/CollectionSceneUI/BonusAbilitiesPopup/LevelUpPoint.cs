﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Com.Mod.Util;

public class LevelUpPoint : MonoBehaviour 
{
    private const float MOVE_TIME = 0.8f;

    public delegate void ObtainmentEndedDele();

    public UISprite _trail;
    public AnimationCurve _customEalstic;

    private ObtainmentEndedDele _obtainmentEndedCallback;
    private SpeedChecker _speedChecker;

    void Awake ()
    {
        _speedChecker = GetComponent<SpeedChecker>();
    }

	void Start () 
	{
	
	}
	
	void FixedUpdate () 
	{
        //_speedChecker.UpdatePos(transform.localPosition);

        //float targetScaleY = _speedChecker.GetSpeed() / 20;
        //Vector3 trailScale = _trail.transform.localScale;
        //trailScale.y += (targetScaleY - trailScale.y) * 0.5f;
        //_trail.transform.localScale = trailScale;
	}

    public void Obtain(Vector2 endPosition, ObtainmentEndedDele callback)
    {
        _obtainmentEndedCallback = callback;

        _trail.transform.localScale = new Vector3(1, 0, 1);
        _trail.transform.DOScaleY(1.5f, 0.2f).SetDelay(0.4f);

        transform.localScale = Vector3.zero;
        transform.DOScale(1, 0.5f).SetEase(_customEalstic);
        transform.DOMove(endPosition, MOVE_TIME).SetDelay(0.4f).SetEase(Ease.InOutSine).OnComplete(OnMotionEnded);
    }

    private void OnMotionEnded()
    {
        if(_obtainmentEndedCallback != null) _obtainmentEndedCallback();
        gameObject.SetActive(false);
        GameObject.Destroy(gameObject);
    }
}
