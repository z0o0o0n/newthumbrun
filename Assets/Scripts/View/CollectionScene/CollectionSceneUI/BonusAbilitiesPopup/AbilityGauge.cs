﻿using UnityEngine;
using System.Collections;

public class AbilityGauge : MonoBehaviour 
{
    public UISprite gauge;
    public CharacterAbilityType.type type;

    public float _gaugeImageHeight;
    public float _gaugeHeight;
    public int _splitNum;

    private float _defaultFillAmount = 0.0f; // 시작 Gauge의 Fill 수치. 디자인상으로 Gauge가 Icon 밑으로 일부 숨겨져 있기 때문
    private float _stepVlaue = 0.0f;
    private int _step = 0;

    void Awake ()
    {
        _defaultFillAmount = 1 / _gaugeImageHeight * (_gaugeImageHeight - _gaugeHeight);
        _stepVlaue = (1 - _defaultFillAmount) / _splitNum;
        Debug.Log("BonusAbilitiesGauge - _defaultFillAmount: " + _defaultFillAmount);
        Debug.Log("BonusAbilitiesGauge - _stepVlaue: " + _stepVlaue);

        UpdateGauge();
    }

	void Start () 
	{
	    
	}
	
	void Update () 
	{
        //if(Input.GetKeyUp(KeyCode.LeftArrow))
        //{
        //    SetStep(GetStep() - 1);
        //    Debug.Log("--------- Step: " + GetStep());
        //}
        //else if(Input.GetKeyUp(KeyCode.RightArrow))
        //{
        //    SetStep(GetStep() + 1);
        //    Debug.Log("--------- Step: " + GetStep());
        //}
	}

    public int GetStep()
    {
        return _step;
    }

    public void SetStep(int step)
    {
        if (step < 0) _step = 0;
        else if (step >= _splitNum) _step = _splitNum;
        else _step = step;
        UpdateGauge();
    }

    private void UpdateGauge()
    {
        Debug.Log(_defaultFillAmount + (_step * _stepVlaue));
        gauge.fillAmount = _defaultFillAmount + (_step * _stepVlaue);
    }

    public bool IsFull()
    {
        bool result = false;
        if (_step == _splitNum) result = true;
        return result;
    }
}
