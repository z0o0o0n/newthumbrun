﻿using UnityEngine;
using System.Collections;

public class BonusAbilitiesButton : MonoBehaviour 
{
    public delegate void BonusAbilitiesButtonEvent(int id);
    public event BonusAbilitiesButtonEvent On_Selected;

    public enum state { Activation, Deactivation, Selection };
    public int id;
    public CharacterAbilityType.type type;

    public UISprite _icon;
    public UISprite _button;
    public Color _selectionColor;
    public Color _deactivationColor;
    public AudioSource _audioSource;
    public AudioClip _selectSound;

    private state _state;
    private bool _debug = false;

    void Awake ()
    {
        Deactivate();
    }

	void Start () 
	{
	}
	
	void Update () 
	{
	
	}

    public void Init(int id, bool isActivation)
    {
        this.id = id;
        if (isActivation) Activate();
    }

    public void OnPressButton()
    {
        if (On_Selected != null) On_Selected(id);
        if (_audioSource && _selectSound)
        {
            _audioSource.clip = _selectSound;
            _audioSource.Play();
        }
        Select();
    }

    public void Select()
    {
        _state = state.Selection;
        _button.gameObject.SetActive(false);
        _icon.gameObject.SetActive(true);
        _icon.color = _selectionColor;
    }

    // 버튼활성화
    public void Activate()
    {
        _state = state.Activation;
        _button.gameObject.SetActive(true);
        _icon.gameObject.SetActive(false);
    }

    // 버튼비활성화
    public void Deactivate()
    {
        if (_debug) Debug.Log("BonusAbilitiesButton - Deactivate()");
        _state = state.Deactivation;
        _button.gameObject.SetActive(false);
        _icon.gameObject.SetActive(true);
        _icon.color = _deactivationColor;
    }
}
