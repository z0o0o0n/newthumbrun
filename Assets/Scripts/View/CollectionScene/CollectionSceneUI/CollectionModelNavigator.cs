﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CollectionModelNavigator : MonoBehaviour 
{
    public delegate void NavigatorEvent();
    public event NavigatorEvent On_PressLeftArrow;
    public event NavigatorEvent On_PressRightArrow;

	public UIButton leftArrowButton;
	public UIButton rightArrowButton;
    public bool isShow
    {
        get { return _isShow; }
    }

    private bool _isShow = false;
    private bool _isActivated = false;
    private float _leftButtonDefaultPosX;
    private float _rightButtonDefaultPosX;

    void Awake()
    {
        Activate();
    }

	void Start () 
	{
        _leftButtonDefaultPosX = leftArrowButton.transform.localPosition.x;
        SetRightButtonDefaultPosX(rightArrowButton.transform.localPosition.x);
	}

	void Update () 
	{
	
	}

    public void SetRightButtonDefaultPosX(float rightButtonPosX)
    {
        _rightButtonDefaultPosX = rightButtonPosX;
    }

    public void Show(float time)
    {
        _isShow = true;
        MoveLeftButton(_leftButtonDefaultPosX, time, Ease.OutCubic);
        MoveRightButton(_rightButtonDefaultPosX, time, Ease.OutCubic);

        Activate();
    }

    private void MoveLeftButton(float targetPosX, float time, Ease ease)
    {
        leftArrowButton.transform.DOLocalMoveX(targetPosX, time).SetEase(ease);
    }

    private void MoveRightButton(float targetPosX, float time, Ease ease)
    {
        rightArrowButton.transform.DOLocalMoveX(targetPosX, time).SetEase(ease);
    }

    public void Hide(float time)
    {
        _isShow = false;
        MoveLeftButton(_leftButtonDefaultPosX - 70f, time, Ease.InCubic);
        MoveRightButton(_rightButtonDefaultPosX + 70f, time, Ease.InCubic);

        Deactivate();
    }

	public void OnPressLeftArrow()
	{
        if(_isActivated) if(On_PressLeftArrow != null) On_PressLeftArrow();
	}

	public void OnPressRightArrow()
	{
        if(_isActivated) if (On_PressRightArrow != null) On_PressRightArrow();
	}

    private void Activate()
    {
        _isActivated = true;
    }

    private void Deactivate()
    {
        _isActivated = false;
    }
}
