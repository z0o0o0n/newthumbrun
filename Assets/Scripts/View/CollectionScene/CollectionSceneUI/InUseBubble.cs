﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class InUseBubble : MonoBehaviour 
{
    public UISprite inUseText;
    public UISprite inUseBubble;

    private GameObject _target;
    private CollectionModelBubblePos _bubblePos;
    private Camera _camera;

    void Awake()
    {
        Hide(0.0f);
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
        if (_target != null)
        {
            float scale = (640f / Screen.height);
            Vector3 targetPos = _camera.WorldToScreenPoint(_target.transform.position);
            //transform.localPosition = new Vector3(( targetPos.x - 1920 / 2) + _bubblePos.offsetPos.x, (targetPos.y - 1080 / 2) + _bubblePos.offsetPos.y, targetPos.z);
            //Debug.LogWarning("X : " + targetPos.x + " / Y: " + targetPos.y);
            float xpos = ((targetPos.x - Screen.width / 2) * scale) + _bubblePos.offsetPos.x;
            float ypos = ((targetPos.y - Screen.height / 2) * scale) + _bubblePos.offsetPos.y;
            transform.localPosition = new Vector3(xpos, ypos, 0);
        }
	}

    public void ChangeTarget(GameObject target, Camera camera)
    {
        if(_target)
        {
            Hide(0.0f);
        }
        _target = target;
        _bubblePos = target.GetComponent<CollectionModelBubblePos>();
        _camera = camera;

        float targetRotation = GetRotationByAlignType(_bubblePos.alignType);
        inUseText.transform.DOLocalRotate(new Vector3(0.0f, 0.0f, -targetRotation), 0);
        inUseBubble.transform.DOLocalRotate(new Vector3(0.0f, 0.0f, targetRotation), 0);

        Show(0.3f);
    }

    public void Show(float time)
    {
        inUseBubble.transform.DOScale(Vector3.one, time).SetEase(Ease.OutBack);
        inUseText.transform.DOScale(Vector3.one, time).SetDelay(0.1f).SetEase(Ease.OutBack);
    }

    private float GetRotationByAlignType(InUseBubbleAlignType.type alignType)
    {
        float result = 0.0f;
        if (alignType == InUseBubbleAlignType.type.LT) result = 0.0f;
        else if (alignType == InUseBubbleAlignType.type.RT) result = -90f;
        else if (alignType == InUseBubbleAlignType.type.LB) result = 90f;
        else if (alignType == InUseBubbleAlignType.type.RB) result = -180f;
        return result;
    }

    public void Hide(float time)
    {
        float delay = 0.1f;
        if (time == 0) delay = 0.0f;

        inUseBubble.transform.DOScale(Vector3.zero, time).SetDelay(delay).SetEase(Ease.InBack);
        inUseText.transform.DOScale(Vector3.zero, time).SetEase(Ease.InBack);
    }
}
