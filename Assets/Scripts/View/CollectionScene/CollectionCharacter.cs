﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;

public class CollectionCharacter : MonoBehaviour 
{
    public delegate void CollectionCharacterEvent(int id);
    public event CollectionCharacterEvent On_Press;

    public Character character;
    public ColliderButton button;

    private bool _isActivatedTouch = false;
    private bool _isDimmed = false;

    void Awake()
    {
        button.On_ButtonUp += OnPress;
    }

    private void OnPress()
    {
        if(_isActivatedTouch) if (On_Press != null) On_Press(character.id);
        //if(_isDimmed) Distinct();
        //else Dim();
    }

	void Start () 
    {
	    
	}

	void Update () 
    {
	
	}

    public int getCharacterID()
    {
        return character.id;
    }

    public void ActivateTouch()
    {
        _isActivatedTouch = true;
        button.Activate();
    }

    public void DeactivateTouch()
    {
        _isActivatedTouch = false;
        button.Deactivate();
    }

    public void Dim()
    {
        CharacterData characterData = CharacterDataManager.instance.GetCharacterDataByID(character.id);

        _isDimmed = true;
        CharacterDimmer dimmer = gameObject.GetComponent<CharacterDimmer>();
        dimmer.Dim(characterData.color);
    }

    public void Distinct()
    {
        _isDimmed = false;
        CharacterDimmer dimmer = gameObject.GetComponent<CharacterDimmer>();
        dimmer.Distinct();
    }
}