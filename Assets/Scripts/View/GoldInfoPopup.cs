﻿using UnityEngine;
using Com.Mod.ThumbRun.Controller;
using System;
using Com.Mod.ThumbRun.UI.Popup;
using Com.Mod.TimeStamp;
using Com.Mod.ThumbRun.Setting.Application;
using Com.Mod.ThumbRun.Participation.View;
using Com.Mod.ThumbRun.ProductBanner.View;

public class GoldInfoPopup : BasicPopup 
{
    [SerializeField]
    private UIDynamicButton _goldInfoPopupButton;
    [SerializeField]
	private FreeGoldProvider _freeGoldProvider;
	[SerializeField]
	private UIButton _closeButton;
	[SerializeField]
	private UIDynamicButton _rewardADButton;
    [SerializeField]
    private UIGold _rewardAdUiGold;
	[SerializeField]
	private UILabel _rewardADRemainingTimeLabel;
	[SerializeField]
	private UIDynamicButton _freeGoldButton;
    [SerializeField]
    private UIDynamicButton _offerWallButton;
    [SerializeField]
	private UIGold _freeGoldUiGold;
	[SerializeField]
	private UILabel _freeGoldRemainingTimeLabel;
	[SerializeField]
	private OfferWallService _offerWallService;
    [SerializeField]
    private NetworkErrorPopup _networkErrorPopup;
    [SerializeField]
    private LargeBannerDisplay _largeBannerDisplay;
    private SettingService _settingService;

    void Awake ()
	{
		UIEventListener.Get(_closeButton.gameObject).onClick += OnCloseButtonClick;

        _settingService = GameObject.FindGameObjectWithTag("Setting").GetComponent<SettingService>();
        _settingService.Prepared += OnPrepared;
        if (_settingService.isPrepared) OnPrepared();

        _freeGoldProvider.FreeGoldTimeOut += OnFreeGoldTimeOut;
		_freeGoldProvider.RewardADTimeOut += OnRewardADTimeOut;

		_rewardADButton.Click += OnRewardADClick;
		_rewardADButton.Disable();

        _rewardAdUiGold.SetGold(_freeGoldProvider.rewardADGold);

		_freeGoldButton.Click += OnFreeGoldClick;
		_freeGoldButton.Disable();

        _freeGoldUiGold.SetGold(_freeGoldProvider.freeGold);

        _largeBannerDisplay.Click += OnLargeBannerClick;

        _offerWallService.Error += OnOfferWallError;
    }

	void Start () 
    {
		Reset();

        if (CoolingTimeStampManager.instance.GetCoolingTimeStamp("RewardAdGold").isCooling)
        {
            _rewardADButton.Disable();
            _rewardADButton.ShowInfoPanel();
        }
        else
        {
            OnRewardADTimeOut();
            _goldInfoPopupButton.ShowNewIcon();
        }


        //if (TimeStampManager.instance.GetTimeStamp("RewardAD").isEnd) OnRewardADTimeOut();
        if (TimeStampManager.instance.GetTimeStamp("FreeGold").isEnd) OnFreeGoldTimeOut();

        if (!_freeGoldProvider.IsFreeGoldWaiting()) _goldInfoPopupButton.ShowNewIcon();
        //if (!_freeGoldProvider.IsRewardADWaiting()) _goldInfoPopupButton.ShowNewIcon();
    }
	
	void Update () 
    {
		if(_freeGoldProvider.IsFreeGoldWaiting()) _freeGoldRemainingTimeLabel.text = _freeGoldProvider.GetFreeGoldRemainingTime();
        //if(_freeGoldProvider.IsRewardADWaiting()) _rewardADRemainingTimeLabel.text = _freeGoldProvider.GetRewardADRemainingTime();
        CoolingTimeStamp coolingTimeStamp = CoolingTimeStampManager.instance.GetCoolingTimeStamp("RewardAdGold");
        //if (coolingTimeStamp.isCooling)
        //{
            _rewardADRemainingTimeLabel.text = _freeGoldProvider.GetRewardADRemainingTime();
        //}
	}

	void OnDestroy()
	{
		UIEventListener.Get(_closeButton.gameObject).onClick -= OnCloseButtonClick;
		_freeGoldProvider.FreeGoldTimeOut -= OnFreeGoldTimeOut;
		_freeGoldProvider.RewardADTimeOut -= OnRewardADTimeOut;

		_rewardADButton.Click -= OnRewardADClick;
		_freeGoldButton.Click -= OnFreeGoldClick;

        _settingService.Prepared -= OnPrepared;

        _largeBannerDisplay.Click -= OnLargeBannerClick;

        _offerWallService.Error -= OnOfferWallError;
    }





    private void OnPrepared()
    {
        if (!_settingService.isPrepared) return;

        if (_settingService.IsReviewVersion())
        {
            _offerWallButton.gameObject.SetActive(false);

            _rewardADButton.transform.localPosition = new Vector3(-65f, -115f);
            _freeGoldButton.transform.localPosition = new Vector3(65f, -115f);
        }
    }

    private void OnCloseButtonClick(GameObject sender)
	{
		Close();
	}

    private void OnOfferWallError()
    {
        _networkErrorPopup.Click += OnNetworkErrorPopupClick;
        _networkErrorPopup.ShowPopup();
    }

    private void OnNetworkErrorPopupClick()
    {
        _networkErrorPopup.Click -= OnNetworkErrorPopupClick;
        _networkErrorPopup.HidePopup();
    }

    private void OnLargeBannerClick(string id)
    {
        Debug.Log(">>>>>>>>>>>>>>>>>>> id: " + id);
        if(id == "2")
        {
            if (CoolingTimeStampManager.instance.GetCoolingTimeStamp("RewardAdGold").isCooling)
            {
                //_networkErrorPopup.Click += OnNetworkErrorPopupClick;
                //_networkErrorPopup.ShowPopup();
                return;
            }

            if (NewADManager.instance.PlayAD("GoldRewardAD"))
            {
                //TimeStampManager.instance.Reset("RewardAD", false);
                CoolingTimeStampManager.instance.Use("RewardAdGold");
                _rewardADButton.Disable();
                _rewardADButton.ShowInfoPanel();
                return;
            }
            else
            {
                _networkErrorPopup.Click += OnNetworkErrorPopupClick;
                _networkErrorPopup.ShowPopup();
                return;
            }
        }
    }





    private void OnRewardADTimeOut()
	{
        _rewardADButton.Enable();
        _rewardADButton.HideInfoPanel();

        _goldInfoPopupButton.ShowNewIcon();
    }

	private void OnRewardADClick(GameObject target)
	{
		if(_freeGoldProvider.PlayAD())
		{
			_rewardADButton.Disable();
			_rewardADButton.ShowInfoPanel();
		}
		else
		{
            _networkErrorPopup.Click += OnNetworkErrorPopupClick;
            _networkErrorPopup.ShowPopup();
        }
	}

	private void OnFreeGoldTimeOut()
	{
		_freeGoldButton.Enable();
		_freeGoldButton.HideInfoPanel();

        _goldInfoPopupButton.ShowNewIcon();
    }

	private void OnFreeGoldClick(GameObject target)
	{
		_freeGoldButton.Disable();
		_freeGoldButton.ShowInfoPanel();

		_freeGoldProvider.ProvideFreeGold();
	}

	public void OnOfferWallButtonClick()
	{
		_offerWallService.OpenOfferWall();
	}




	public override void Close()
	{
		base.Close();
	}

	private void Reset()
	{
		gameObject.transform.localPosition = Vector3.zero;	
		Close();
	}




	public override void Open()
	{
        base.Open();

        Debug.Log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% free gold is end: " + TimeStampManager.instance.GetTimeStamp("FreeGold").isEnd);
        if(CoolingTimeStampManager.instance.GetCoolingTimeStamp("RewardAdGold").isCooling)
        {
            _rewardADButton.ShowInfoPanel();
            _rewardADButton.Disable();
        }
        else
        {
            _rewardADButton.HideInfoPanel();
            _rewardADButton.Enable();
        }

		if(_freeGoldProvider.IsFreeGoldWaiting())
		{
			_freeGoldButton.ShowInfoPanel();
            _freeGoldButton.Disable();
        }
		else
		{
			_freeGoldButton.HideInfoPanel();
            _freeGoldButton.Enable();
        }

		base.Open();
	}
}
