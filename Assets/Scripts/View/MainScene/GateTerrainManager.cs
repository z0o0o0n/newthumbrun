﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GateTerrainManager : MonoBehaviour 
{
    public List<GameObject> gateTerrainPrefabList;

    private GameObject _currentGateTerrain;

    public void Init(int stageID)
    {
        //CreateGateTerrain(stageID);
    }

    private void CreateGateTerrain(int stageID)
    {
        GameObject instance = GameObject.Instantiate(gateTerrainPrefabList[stageID]);
        instance.transform.localPosition = Vector3.zero;
        instance.transform.parent = transform;

        _currentGateTerrain = instance;
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}
}
