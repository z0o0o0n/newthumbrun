﻿using UnityEngine;
using System.Collections;

public class Flag : MonoBehaviour 
{
    public GameObject flagCloth;

    void Awake()
    {
        
    }

	void Start ()
    {
        //Debug.Log("=-= : " + flagCloth);
        
        //renderer.material.mainTexture = tex;
        //ChangeFlag("040");
	}
	
	void Update () 
    {
	}

    public void ChangeFlag(string code)
    {
        flagCloth.GetComponent<Renderer>().material.SetTexture("_node_3", (Texture)Resources.Load("Flag/Flag_" + code));
        Debug.Log("ChangeFlag ======================= : " + code);
        //flagCloth.renderer.material.mainTexture = (Texture)Resources.LoadAssetAtPath("Assets/Resources/Flag/Flag_040", typeof(Texture));
    }
}
