﻿using UnityEngine;
using System.Collections;

public class FlagManager : MonoBehaviour 
{
    public FlagData[] flagData;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void UpdateFlag(int stageIndex, string worldCode, string friendCode)
    {
        flagData[stageIndex].world.ChangeFlag(worldCode);
        flagData[stageIndex].friend.ChangeFlag(friendCode);
    }
}

[System.Serializable]
public class FlagData
{
    public int stageIndex;
    public Flag world;
    public Flag friend;
}
