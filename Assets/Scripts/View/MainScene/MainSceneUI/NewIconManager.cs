﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class NewIconManager : MonoBehaviour 
{
    [SerializeField] private UISprite newIcon;
    
    void Awake()
    {
        
    }

	void Start () 
    {
        //Debug.Log("Collection Buton Start");
        CheckNewNotifications();
	}

    private void CheckNewNotifications()
    {
        //Debug.Log("새로운 공지사항이 있는지 확인함: " + CharacterDataManager.instance.HasNew());
        if (CharacterDataManager.instance.HasNew()) ShowNewIcon();
        else HideNewIcon();
    }
	
	void Update () 
    {
	
	}

    public void ShowNewIcon()
    {
        newIcon.gameObject.SetActive(true);
        newIcon.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        newIcon.transform.DOScale(new Vector3(0.7f, 0.7f, 0.7f), 3f).SetId("newIcon").SetEase(Ease.OutElastic).SetLoops(-1, LoopType.Restart);
    }

    private void StartNotificationMotion()
    {
        float downscaleMotionTime = 0.2f;
        float downscaleSize = 0.5f;
        float elasticMotionTime = 3.0f;
        float elasticSize = 0.7f;
        newIcon.transform.DOScale(new Vector3(downscaleSize, downscaleSize, downscaleSize), downscaleMotionTime).SetId("newIconDownscale").SetEase(Ease.OutCubic);
        newIcon.transform.DOScale(new Vector3(elasticSize, elasticSize, elasticSize), elasticMotionTime).SetId("newIconElastic").SetDelay(downscaleMotionTime).SetEase(Ease.OutElastic);
    }

    public void HideNewIcon()
    {
        newIcon.gameObject.SetActive(false);
    }
}
