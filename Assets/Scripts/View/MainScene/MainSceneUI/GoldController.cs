﻿using UnityEngine;
using System.Collections;
using System;

public class GoldController : MonoBehaviour 
{
    public UISprite goldIcon;

    private UILabel _goldLabel;

	void Start ()
    {
        _goldLabel = GetComponent<UILabel>();

        DisplayGold(1234566);
	}
	
	void Update ()
    {
	
	}

    void Relocate()
    {
        float goldIconTargetPosX = _goldLabel.transform.localPosition.x - ((_goldLabel.localSize.x) + (goldIcon.localSize.x / 2) + 10);
        Vector3 goldIconPos = goldIcon.transform.localPosition;
        goldIconPos.x = goldIconTargetPosX;
        goldIcon.transform.localPosition = goldIconPos;
    }

    public void AddGold(int value)
    {
        
    }

    public void SubGold(int value)
    {

    }

    void DisplayGold(int won)
    {
        _goldLabel.text = String.Format("{0:##,##}", won);
        Relocate();
    }
}
