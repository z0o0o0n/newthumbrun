﻿using UnityEngine;
using System.Collections;

public class SponsorshipStatus : MonoBehaviour 
{
    public UILabel label;

    private int _remainingSponsorshipCount = 0;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void SetRemainingSponsorshipCount(int count)
    {
        _remainingSponsorshipCount = count;
        label.text = count.ToString();
    }

    public int GetRemainingSponsorshipCount()
    {
        return _remainingSponsorshipCount;
    }
}
