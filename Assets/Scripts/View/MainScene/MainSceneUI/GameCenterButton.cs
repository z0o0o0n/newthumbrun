﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GameCenterButton : MonoBehaviour 
{
    private float BUBBLE_MOTION_TIME = 0.5f;

    public UIButton uiButton;
    public UISprite uiButtonSprite;
    public UIBasicButton uiBasicButton;
    public GameObject gameCenterLogoutBubble;
    public GameObject networkErrorBubble;
    public bool useBubbles = true;

    void Awake()
    {
        HideGameCenterLogoutBubble(0);
        HideNetworkErrorBubble(0);
        uiBasicButton.IsEnabled(false);
        
        #if UNITY_ANDROID
            ChangeToGooglePlayButton();
        #endif

        //if (!GameManager.socialManager.isNetworkOn) ShowNetworkErrorBubble(BUBBLE_MOTION_TIME);
        //else if (!GameManager.socialManager.IsAuthenticatedUser()) ShowGameCenterLogoutBubble(BUBBLE_MOTION_TIME);
    }

    private void ChangeToGooglePlayButton()
    {
        uiButton.normalSprite = "GooglePlayButton_Normal";
        uiButton.pressedSprite = "GooglePlayButton_Press";
        uiButton.disabledSprite = "GooglePlayButton_Deactivate";
    }

	void Start () 
	{
	}
	
	void Update () 
	{
	
	}

    private void OnOnline()
    {
        uiBasicButton.IsEnabled(true);
        HideGameCenterLogoutBubble(0);
        HideNetworkErrorBubble(0);
    }

    private void ShowGameCenterLogoutBubble(float time)
    {
        if (useBubbles)
        {
            if (time == 0)
            {
                gameCenterLogoutBubble.transform.localScale = Vector3.one;
            }
            else
            {
                gameCenterLogoutBubble.transform.DOScale(1, time);
            }
        }
    }

    private void HideGameCenterLogoutBubble(float time)
    {
        if (time == 0)
        {
            gameCenterLogoutBubble.transform.localScale = Vector3.zero;
        }
        else
        {
            gameCenterLogoutBubble.transform.DOScale(0, time);
        }
    }

    private void ShowNetworkErrorBubble(float time)
    {
        if(useBubbles)
        {
            if (time == 0)
            {
                networkErrorBubble.transform.localScale = Vector3.one;
            }
            else
            {
                networkErrorBubble.transform.DOScale(1, time);
            }
        }
    }

    private void HideNetworkErrorBubble(float time)
    {
        if (time == 0)
        {
            networkErrorBubble.transform.localScale = Vector3.zero;
        }
        else
        {
            networkErrorBubble.transform.DOScale(0, time);
        }
    }

    void OnDestroy()
    {
    }
}
