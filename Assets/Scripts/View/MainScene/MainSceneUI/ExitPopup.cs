﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Com.Mod.ThumbRun.UI.Popup;

public class ExitPopup : MonoBehaviour 
{
    public delegate void ExitPopupEvent();
    public event ExitPopupEvent Hided;

    public UIBasicButton okButton;
    public UIBasicButton cancelButton;
    public AnimationCurve customElastic;

    void Awake ()
    {
        okButton.On_Click += OnOkButtonClick;
        // cancelButton.On_Click += OnCancelButtonClick;

        GetComponent<BasicPopup>().Close();
        // Reset();
    }

	void Start () 
	{
        // backCollider.size = new Vector2(Screen.width, Screen.height);
        // backSprite.width = Screen.width;
        // backSprite.height = Screen.height;
	}
	
	void Update () 
	{
	}

    void OnDestroy()
    {
        okButton.On_Click -= OnOkButtonClick;
        // cancelButton.On_Click -= OnCancelButtonClick;
    }

    private void OnOkButtonClick()
    {
        // Debug.Log("OK CLICK");
        Application.Quit();
    }

    private void OnCancelButtonClick()
    {
        // Debug.Log("Cancel CLICK");
        // Hide();
    }

    #region < PUBLIC FUNCTION >
    
    // public void Show()
    // {
    //     Debug.Log("Show");
    //     DOTween.Kill("EixtVisibleTween");
    //     transform.DOScale(1, 0.5f).SetEase(customElastic).SetId("EixtVisibleTween").OnComplete(OnShowed);
    // }

    // private void OnShowed()
    // {
    //     okButton.SetButtonActive(true);
    //     cancelButton.SetButtonActive(true);
    // }

    // public void Hide()
    // {
    //     Debug.Log("Hide");
    //     DOTween.Kill("EixtVisibleTween");
    //     transform.DOScale(0, 0.3f).SetId("EixtVisibleTween").SetEase(Ease.InBack).OnComplete(OnHided);

    //     okButton.SetButtonActive(false);
    //     cancelButton.SetButtonActive(false);
    // }

    // private void OnHided()
    // {
    //     if (Hided != null) Hided();
    // }

    // public void Reset()
    // {
    //     transform.localScale = Vector2.zero;

    //     okButton.SetButtonActive(false);
    //     cancelButton.SetButtonActive(false);
    // }

    #endregion
}
