﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using view.mapScene;
using System.Collections.Generic;
using Com.Mod.ThumbRun.User.View;

public class MainSceneUI : MonoBehaviour
{
    /**
     * Main Scene UI의 하위 UI 상태변화 관리
     **/

    public delegate void MainSceneUIEvent();
    public event MainSceneUIEvent On_PressLoginGCPopup;
    public event MainSceneUIEvent On_PressSponsorButton;
    public event MainSceneUIEvent On_PressMapButton;
    public event MainSceneUIEvent MultiRunClick;
    public event MainSceneUIEvent SingleRunClick;
    public event MainSceneUIEvent HelpButtonClick;
    public delegate void CountryEvent(string code);
    public event CountryEvent On_ChangeCountry;

    // public Menu menu; 
    // public UIButton loginGCPopup;
    public MapTitle mapTitle;
    // public UIBasicButton gameCenterErrorNotification;
    // public UISprite jailbrokenNotification;
    // public PageWindow pageWindow;
    // public Flag wrFlag;
    // public Flag frFlag;
    public GoldDisplay goldDisplay;
    public TipBook mainTutorialBook;
    //public TouchArea touchArea;
    // public StageArrowManager stageArrowManager;
    // public NoAdsButton noAdsButton;
    // public UIBasicButton helpButton;
	
    void Awake()
    {
        // menu.On_PressSetting += OnPressSetting;
        // menu.On_PressCountry += OnPressCountry;
        // menu.On_PressInfo += OnPressInfo;

        // pageWindow.On_Close += OnPopupClose;
        // pageWindow.On_ChangeCountry += OnChangeCountry;

        // helpButton.On_Click += OnHelpButtonClick;

        UIRoot uiRoot = GetComponent<UIRoot>();
        Debug.Log(">>>>>>>>>>>>>>>>> uiRoot.manualWidth: " + uiRoot.manualWidth);

        float heightRate = (1f / Screen.width) * Screen.height;
        Debug.Log(">>>>>>>>>>>>>>>>> width: " + Screen.width + " / height: " + Screen.height + " / heightRate: " + heightRate.ToString());



        SetButtonActive(false);
    }

    public void Init(string countryCode)
    {
        goldDisplay.Show(0.3f);
    }

	void Start ()
    {
	}
	
	void Update ()
    {
	}

    void OnDestroy()
    {
        // menu.On_PressSetting -= OnPressSetting;
        // menu.On_PressCountry -= OnPressCountry;
        // menu.On_PressInfo -= OnPressInfo;

        // pageWindow.On_Close -= OnPopupClose;
        // pageWindow.On_ChangeCountry -= OnChangeCountry;

        // helpButton.On_Click -= OnHelpButtonClick;

        // gameCenterErrorNotification.On_Click -= OnGameCenterErrorNotificationClick;
    }

    // private void OnPressSetting()
    // {
    //     pageWindow.ShowPopup(PageWindowName.SETTING);
    //     SetButtonActive(false);
    // }

    // private void OnPressCountry()
    // {
    //     pageWindow.ShowPopup(PageWindowName.COUNTRY);
    //     SetButtonActive(false);
    // }

    // private void OnPressInfo()
    // {
    //     pageWindow.ShowPopup(PageWindowName.INFO);
    //     SetButtonActive(false);
    //     //mainTutorialBook.Show(0.3f);
    // }

    private void OnPopupClose(string popupName)
    {
        SetButtonActive(true);
    }

    private void OnChangeCountry(string code)
    {
        if (On_ChangeCountry != null) On_ChangeCountry(code);
    }

    private void OnMultiRunClick(GameObject sender)
    {
        if (MultiRunClick != null) MultiRunClick();
    }

    private void OnSingleRunClick(GameObject sender)
    {
        if (SingleRunClick != null) SingleRunClick();
    }

    public void OnPressSponsorButton()
    {
        if (On_PressSponsorButton != null) On_PressSponsorButton();
    }

    private void OnHelpButtonClick()
    {
        if (HelpButtonClick != null) HelpButtonClick();
    }

    public void SetButtonActive(bool isActivation)
    {
        // menu.SetButtonActive(isActivation);
        //touchArea.gameObject.SetActive(isActivation);
        // stageArrowManager.SetActivation(isActivation);
    }

    // public void ShowLoginGCPopup(float time)
    // {
    //     DOTween.Kill("loginGCPopupVisible");
    //     loginGCPopup.transform.DOLocalMoveY(-45, time).SetId("loginGCPopupVisible");
    // }

    // public void HideLoginGCPopup(float time)
    // {
    //     DOTween.Kill("loginGCPopupVisible");
    //     loginGCPopup.transform.DOLocalMoveY(45, time).SetId("loginGCPopupVisible");
    // }

    // public void ShowGameCenterErrorPopup(float time)
    // {
    //     gameCenterErrorNotification.On_Click += OnGameCenterErrorNotificationClick;
    //     gameCenterErrorNotification.transform.DOLocalMoveY(-70, time).SetId("gameCenterErrorNotification_visible");
    // }

    // public void HideGameCenterErrorPopup(float time)
    // {
    //     gameCenterErrorNotification.On_Click -= OnGameCenterErrorNotificationClick;
    //     gameCenterErrorNotification.transform.DOLocalMoveY(70, time).SetId("gameCenterErrorNotification_visible");
    // }

    private void OnGameCenterErrorNotificationClick()
    {
        Application.OpenURL("gamecenter:");
    }

    public void PressLoginGCPopup()
    {
        Debug.Log("Login GC Popup");
        if (On_PressLoginGCPopup != null) On_PressLoginGCPopup();
    }

    public void OnPressMapButton()
    {
        if (On_PressMapButton != null) On_PressMapButton();
    }

    public void HideUI()
    {
        // loginGCPopup.gameObject.SetActive(false);
        goldDisplay.gameObject.SetActive(false);
        // menu.gameObject.SetActive(false);
        // pageWindow.gameObject.SetActive(false);
        mapTitle.gameObject.SetActive(false);
        // helpButton.gameObject.SetActive(false);
        //touchArea.gameObject.SetActive(false);
        // stageArrowManager.Hide();
    }

    public void ShowUI()
    {
        // loginGCPopup.gameObject.SetActive(true);
        goldDisplay.gameObject.SetActive(true);
        // menu.gameObject.SetActive(true);
        // pageWindow.gameObject.SetActive(true);
        mapTitle.gameObject.SetActive(true);
        // helpButton.gameObject.SetActive(true);
        //touchArea.gameObject.SetActive(true);
        // stageArrowManager.Show();
    }
}
