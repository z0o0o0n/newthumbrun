﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SponsorButton : MonoBehaviour
{
    public UISprite sprite;
    public UISprite sponsorEffect;

	void Start ()
    {
        
	}
	
	void Update ()
    {
	
	}

    public void Activate()
    {
        PlayEffect();
        sprite.alpha = 1;
    }

    public void Deactivate()
    {
        StopEffect();
        sprite.alpha = 0.3f;
    }

    private void PlayEffect()
    {
        sponsorEffect.alpha = 0.0f;
        DOTween.To(() => sponsorEffect.alpha, x => sponsorEffect.alpha = x, 1.0f, 0.2f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutCubic).SetId("sponsorButtonEffect");
    }

    private void StopEffect()
    {
        sponsorEffect.alpha = 0.0f;
        DOTween.Kill("sponsorButtonEffect");
    }
}
