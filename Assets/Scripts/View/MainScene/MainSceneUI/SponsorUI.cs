﻿using UnityEngine;
using System.Collections;

public class SponsorUI : MonoBehaviour 
{
    public delegate void SponsorUIEvent();
    public event SponsorUIEvent On_PressSponsorButton;

    public SponsorButton sponsorButton;
    public SponsorshipStatus sponsorshipStatus;
    public SponsorSpeechPopup sponsorSpeechPopup;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void OnPressSponsorButton()
    {
        if (On_PressSponsorButton != null) On_PressSponsorButton();
    }

    public void ChangeSponsorshipMode(int remainingSponsorshipCount)
    {
        sponsorButton.gameObject.SetActive(false);
        sponsorshipStatus.gameObject.SetActive(true);
        sponsorshipStatus.SetRemainingSponsorshipCount(remainingSponsorshipCount);
        HideSpeechPopup();
    }

    public void ChangeIdleMode(bool IsActivate)
    {
        sponsorButton.gameObject.SetActive(true);
        sponsorshipStatus.gameObject.SetActive(false);

        if (IsActivate)
        {
            sponsorButton.Activate();
            ShowSpeechPopup();
        }
        else
        {
            sponsorButton.Deactivate();
            HideSpeechPopup();
        }
    }

    public void SetData(int gold, int boosterAmount)
    {
        sponsorSpeechPopup.SetData(gold, boosterAmount);
    }

    private void ShowSpeechPopup()
    {
        sponsorSpeechPopup.Show(0.5f);
    }

    private void HideSpeechPopup()
    {
        sponsorSpeechPopup.Hide();
    }
}
