﻿using UnityEngine;
using System.Collections;

public class RunButton : MonoBehaviour 
{
    public delegate void RunButtonEvent();
    public event RunButtonEvent Pressed;
    public event RunButtonEvent Released;

	void Start ()
    {
	}
	
	void Update ()
    {
	
	}

    public void OnClick()
    {
        if (Released != null) Released();
    }

    void OnPress(bool isDown)
    {
        if (isDown)
        {
            if (Pressed != null) Pressed();
        }
        else
        {
            
        }
    }

}
