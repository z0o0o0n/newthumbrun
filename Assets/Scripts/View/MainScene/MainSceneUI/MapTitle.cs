﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class MapTitle : MonoBehaviour 
{
    [SerializeField]
    private List<UILabel> _titles;
    private UIWidget _widget;

    void Awake()
    {
        _widget = GetComponent<UIWidget>();
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void Show(float time)
    {
        DOTween.Kill("MapTitleVisible." + GetInstanceID());
        DOTween.To(() => _widget.alpha, x => _widget.alpha = x, 1f, 0.3f).SetId("MapTitleVisible." + GetInstanceID());
        //_label.gameObject.SetActive(true);
    }

    public void Hide(float time)
    {
        DOTween.Kill("MapTitleVisible." + GetInstanceID());
        DOTween.To(() => _widget.alpha, x => _widget.alpha = x, 0f, 0.2f).SetId("MapTitleVisible." + GetInstanceID());
        //_label.gameObject.SetActive(false);
    }

    public void ChangeTitle(int mapIndex)
    {
        for(int i = 0; i < _titles.Count; i++)
        {
            if(i == mapIndex)
            {
                _titles[i].gameObject.SetActive(true);
            }
            else
            {
                _titles[i].gameObject.SetActive(false);
            }
        }

        //_titles[mapIndex].gameObject.SetActive(true);
        //_label.text = StageDataManager.instance.GetStageDataByIndex(mapIndex).name;
    }
}
