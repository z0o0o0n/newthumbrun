﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CommingSoon : MonoBehaviour 
{
	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void Show()
    {
        DOTween.Kill("CommingSoon_MoveX");
        transform.localPosition = new Vector3(210, 0, 0);
        transform.DOLocalMoveX(100, 1f).SetId("CommingSoon_MoveX").SetEase(Ease.OutElastic).OnComplete(OnShowComplete);
    }

    private float dealyForHide = 2.0f;
    private void OnShowComplete()
    {
        DOTween.Kill("CommingSoon_DelayForHide");
        DOTween.To(() => dealyForHide, x => dealyForHide = x, dealyForHide, dealyForHide).SetId("CommingSoon_DelayForHide").OnComplete(Hide);
    }

    public void Hide()
    {
        DOTween.Kill("CommingSoon_MoveX");
        transform.DOLocalMoveX(210, 0.3f).SetId("CommingSoon_MoveX").SetEase(Ease.InBack);
    }
}
