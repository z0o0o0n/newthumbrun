﻿using UnityEngine;
using System.Collections;

public class NewSponsorButton : MonoBehaviour 
{
    public delegate void NewSponsorButtonEvent();
    public event NewSponsorButtonEvent On_Press;

    public UIBasicButton _button;
    public UISprite _buttonPressSprite;
    public UISprite _sponsorTextSprite;
    public UISprite _goldTextSprite;

    void Awake()
    {
        _button.On_Click += OnButtonClick;
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void OnButtonClick()
    {
        Debug.Log("=================== press");
        if (On_Press != null) On_Press();
    }

    public void Activate()
    {
        _button.gameObject.SetActive(true);
        _buttonPressSprite.gameObject.SetActive(false);
    }

    public void Deactivate()
    {
        _button.gameObject.SetActive(false);
        _buttonPressSprite.gameObject.SetActive(true);
    }

    public void SetButtonActive(bool isActivation)
    {
        _button.SetButtonActive(isActivation);
    }

    void OnDestory()
    {
        _button.On_Click -= OnButtonClick;
    }
}
