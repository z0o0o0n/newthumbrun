﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class Menu : MonoBehaviour
{
    public delegate void MenuEvent();
    public event MenuEvent GameCenterButtonClick;
    public event MenuEvent GachaButtonClick;
    public event MenuEvent CollectionButtonClick;
    public event MenuEvent On_PressSetting;
    public event MenuEvent On_PressCountry;
    public event MenuEvent On_PressInfo;
    public event MenuEvent SubMenuOpen;
    public event MenuEvent SubMenuClosed;
    public delegate void Callback();

    public bool isDebug = false;

    [Header("Menu")]
    
    public UIBasicButton gachaButton;
    public UIBasicButton collectionButton;
    public UIBasicButton menuButton;
    public UIBasicButton menuOnButton;
    public UISprite arrowSprite;
    public NewIconManager collectionButtonNewIconManager;

    [Header("Sub Menu")]
    public UIBasicButton aboutPageButton;
    public UIBasicButton settingPageButton;
    public UIBasicButton helpButton;
    public UIBasicButton gameCenterButton;
    public UIBasicButton flagPageButton;
    public float spaceH = 8;
    public AnimationCurve curve;


    private bool _isOpen = false;
    private List<UIBasicButton> _subMenuList = new List<UIBasicButton>();
    private UISprite _menuButtonSprite;
    private UISprite _menuOnButtonSprite;
    private Callback _onShowMotionCompleteCallback;
    private Callback _onHideMotionCompleteCallback;

    void Awake()
    {
        _menuButtonSprite = menuButton.gameObject.GetComponent<UISprite>();
        _menuOnButtonSprite = menuOnButton.gameObject.GetComponent<UISprite>();

        aboutPageButton.On_Click += OnAboutPageButtonClick;
        _subMenuList.Add(aboutPageButton);
        settingPageButton.On_Click += OnSettingPageButtonClick;
        _subMenuList.Add(settingPageButton);
        _subMenuList.Add(helpButton);
        _subMenuList.Add(gameCenterButton);
        flagPageButton.On_Click += OnFlagPageButtonClick;
        _subMenuList.Add(flagPageButton);

        #if UNITY_IOS
        #elif UNITY_ANDROID && !UNITY_EDITOR
            flagPageButton.gameObject.SetActive(false);
        #endif

        // visible false
        _menuOnButtonSprite.enabled = false;
        arrowSprite.enabled = false;
        HideSubMenus(0.1f);

        gameCenterButton.On_Click += OnGameCenterButtonClick;
        gachaButton.On_Click += OnGachaButtonClick;
        collectionButton.On_Click += OnCollectionButtonClick;
        menuButton.On_Click += OnMenuButtonClick;
        menuOnButton.On_Click += OnMenuOnButtonClick;
    }

    void Start()
    {
    }

    private void OnSettingPageButtonClick()
    {
        if (On_PressSetting != null) On_PressSetting();
    }

    private void OnFlagPageButtonClick()
    {
        if (On_PressCountry != null) On_PressCountry();
    }

    private void OnAboutPageButtonClick()
    {
        if (On_PressInfo != null) On_PressInfo();
    }

    private void OnGameCenterButtonClick()
    {
        if (GameCenterButtonClick != null) GameCenterButtonClick();
    }

    private void OnGachaButtonClick()
    {
        if (GachaButtonClick != null) GachaButtonClick();
    }

    private void OnCollectionButtonClick()
    {
        if (CollectionButtonClick != null) CollectionButtonClick();
    }

    private void OnMenuButtonClick()
    {
        if (!_isOpen)
        {
            if (isDebug) Debug.Log("메인 메뉴 열기");
            _menuButtonSprite.enabled = false;
            _menuOnButtonSprite.enabled = true;
            //SetButtonActive(false);
            ShowSubMenus();
        }
    }

    private void OnMenuOnButtonClick()
    {
        if (_isOpen)
        {
            if (isDebug) Debug.Log("메인 메뉴 닫기");
            _menuButtonSprite.enabled = true;
            _menuOnButtonSprite.enabled = false;
            //SetButtonActive(false);
            HideSubMenus();
        }
    }



    public void SetButtonActive(bool isActivation)
    {
        //Debug.LogWarning(isActivation);
        // Menu
        gachaButton.SetButtonActive(isActivation);
        collectionButton.SetButtonActive(isActivation);
        menuButton.SetButtonActive(isActivation);
        menuOnButton.SetButtonActive(isActivation);

        // SubMenu
        for (int i = 0; i < _subMenuList.Count; i++)
        {
            UIBasicButton subMenuButton = _subMenuList[i];
            subMenuButton.SetButtonActive(isActivation);
        }
    }

    //private void SetVisibleSubMenus(bool value)
    //{
    //    for (int i = 0; i < _subMenuList.Count; i++)
    //    {
    //        UISprite uiSprite = _subMenuList[i].GetComponent<UISprite>();
    //        uiSprite.enabled = value;
    //    }
    //}

    private void ShowSubMenus(float time = 0.4f, Callback onCompleteCallback = null)
    {
        if (onCompleteCallback != null) _onShowMotionCompleteCallback = onCompleteCallback;

        arrowSprite.enabled = true;
        arrowSprite.transform.localScale = new Vector3(0, 0, 1);
        DOTween.Kill("arrow");
        arrowSprite.transform.DOScale(new Vector3(1, 1, 1), time).SetId("arrow").SetEase(curve);

        for (int i = 0; i < _subMenuList.Count; i++)
        {
            if (!_subMenuList[i].enabled)
            {
                _subMenuList[i].enabled = true;
            }
            _subMenuList[i].transform.localScale = new Vector3(0, 0, 1);
            UIBasicButton targetMenu = _subMenuList[i];

            if (i == _subMenuList.Count - 1)
            {
                _subMenuList[i].transform.DOScale(new Vector3(1, 1, 1), time).SetDelay(i / 20.0f).SetEase(curve).OnComplete(OnCompleteShowSubMenus);
            }
            else
            {
                _subMenuList[i].transform.DOScale(new Vector3(1, 1, 1), time).SetDelay(i / 20.0f).SetEase(curve);
            }
        }

        if (SubMenuOpen != null) SubMenuOpen();
    }
    private void OnCompleteShowSubMenus()
    {
        _isOpen = true;
        //SetButtonActive(true);
        if(_onShowMotionCompleteCallback != null) _onShowMotionCompleteCallback();
    }

    private void HideSubMenus(float time = 0.2f, Callback onCompleteCallback = null)
    {
        if (onCompleteCallback != null) _onHideMotionCompleteCallback = onCompleteCallback;

        int cnt = 0; // for Delay
        for (int i = _subMenuList.Count - 1; i >= 0; i--)
        {
            if (i == 0)
            {
                DOTween.Kill("arrow");
                arrowSprite.transform.DOScale(new Vector3(0, 0, 1), time).SetId("arrow").SetDelay(cnt / 30.0f).SetEase(Ease.InBack).OnComplete(OnCompleteHideSubMenus);
            }

            UIBasicButton targetMenu = _subMenuList[i];
            _subMenuList[i].transform.DOScale(new Vector3(0, 0, 1), time).SetDelay(cnt / 30.0f).SetEase(Ease.InBack);
            cnt++;
        }
    }
    private void OnCompleteHideSubMenus()
    {
        _isOpen = false;
        arrowSprite.enabled = false;
        //SetButtonActive(true);
        if(_onHideMotionCompleteCallback != null) _onHideMotionCompleteCallback();
        if(SubMenuClosed != null) SubMenuClosed();
    }

    

    void OnDestroy()
    {
        flagPageButton.On_Click -= OnFlagPageButtonClick;
        aboutPageButton.On_Click -= OnAboutPageButtonClick;

        gachaButton.On_Click -= OnGachaButtonClick;
        collectionButton.On_Click -= OnCollectionButtonClick;
        menuButton.On_Click -= OnMenuButtonClick;
        menuOnButton.On_Click -= OnMenuOnButtonClick;
    }
}
