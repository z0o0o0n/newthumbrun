﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SponsorSpeechPopup : MonoBehaviour 
{
    public AnimationCurve customElastic;
    public UILabel getLabel1;
    public UISprite goldIcon;
    public UILabel goldLabel;
    public UILabel getLabel2;
    public UISprite boosterIcon;
    public UISprite multiplicationMark;
    public UILabel boosterLabel;

    void Awake()
    {
        Hide();
    }

	void Start () 
	{
        
	}
	
	void Update () 
	{
	
	}

    public void SetData(int gold, int boosterAmount)
    {
        goldLabel.text = gold.ToString();
        boosterLabel.text = boosterAmount.ToString();
        Replace();
    }

    private void Replace()
    {
        Vector3 goldIconPos = goldIcon.transform.localPosition;
        goldIconPos.x = goldLabel.transform.localPosition.x - goldLabel.width - (goldIcon.width / 2) - 2;
        goldIcon.transform.localPosition = goldIconPos;

        Vector3 get1Pos = getLabel1.transform.localPosition;
        get1Pos.x = goldIcon.transform.localPosition.x - (goldIcon.width / 2) - (getLabel1.width / 2) - 5;
        getLabel1.transform.localPosition = get1Pos;

        Vector3 multiplicationMarkPos = multiplicationMark.transform.localPosition;
        multiplicationMarkPos.x = boosterLabel.transform.localPosition.x - boosterLabel.width - (multiplicationMark.width / 2);
        multiplicationMark.transform.localPosition = multiplicationMarkPos;

        Vector3 goldBoosterIconPos = boosterIcon.transform.localPosition;
        goldBoosterIconPos.x = multiplicationMark.transform.localPosition.x - (multiplicationMark.width / 2) - (boosterIcon.width / 2) - 3;
        boosterIcon.transform.localPosition = goldBoosterIconPos;

        Vector3 get2Pos = getLabel2.transform.localPosition;
        get2Pos.x = boosterIcon.transform.localPosition.x - (boosterIcon.width / 2) - (getLabel2.width / 2) - 8;
        getLabel2.transform.localPosition = get2Pos;
    }

    public void Show(float time = 0.0f)
    {
        transform.localScale = Vector3.zero;
        transform.DOScale(1, time).SetEase(customElastic);
    }

    public void Hide(float time = 0.0f)
    {
        transform.DOScale(0, time);
    }
}
