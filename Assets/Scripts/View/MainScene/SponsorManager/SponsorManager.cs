﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Junhee.Utils;
using System.Collections.Generic;
using Com.Mod.ThumbRun.User.View;

public class SponsorManager : MonoBehaviour
{
    public delegate void SponsorManagerEvent();
    public event SponsorManagerEvent On_OpenedSponsorship; // 스폰서쉽이 가능할 때
    public event SponsorManagerEvent On_ClosedSponsorship; // 스폰서쉽이 불가능할 때
    public bool isTestMode = false; // Sponsor AD 유무를 Test Panel에서 Test할 경우
    public int refreshTimerValue = 0;

    private bool _isDebug = false;
    private bool _isOpendSponsorship = false;
    private bool _isSponsoring = false;
    private int _refreshTime = 2;
    private SponsorData _sponsorData;
    //private ADManager _adManager;
    //private SponsorUI _ui;
    //private NewSponsorButton _sponsorButton;
    private GoldDisplay _goldDisplay;

    void Start()
    {

    }

    public void Init(SponsorData sponsorData, ADManager adManager, NewSponsorButton sponsorButton, GoldDisplay goldDisplay)
    {
        _sponsorData = sponsorData;
        //_adManager = adManager;
        //_sponsorButton = sponsorButton;
        _goldDisplay = goldDisplay;

        _isOpendSponsorship = false;
        //_ui.SetData(_sponsorData.rewardGold, _sponsorData.rewardSponsorshipCount);

        //if (IsSponsorshipMode())
        //{
        //    if(_isDebug) Debug.Log("*** Init");
        //    ChangeSponsorshipModeUI();
        //}
		Refresh();
    }

    private void ChangeSponsorshipModeUI()
    {
        //_ui.ChangeSponsorshipMode(_sponsorData.goldBoosterAmount);
        //_sponsorButton.Deactivate();
    }

    public void Refresh()
    {
        if(_isDebug) Debug.Log("refresh. hasAD: " + HasAD() + " / isOpendSponsorship: " + _isOpendSponsorship);
        if (HasAD())
        {
            if (!_isOpendSponsorship)
            {
                _isOpendSponsorship = true;
                OpenSponsorship();
                if (On_OpenedSponsorship != null) On_OpenedSponsorship();
            }
        }
        else
        {
            if (_isOpendSponsorship)
            {
                _isOpendSponsorship = false;
                CloseSponsorship();
                if (On_ClosedSponsorship != null) On_ClosedSponsorship();
            }
        }

        StartRefreshTimer();
    }

    private bool HasAD()
    {
        return false;
        //return _adManager.HasRewardAD(isTestMode);
    }

    private void OpenSponsorship()
    {
        if(_isDebug) Debug.Log("Open Sponsor Ship");
        //_ui.On_PressSponsorButton += OnPressSponsorButton;
        //_ui.ChangeIdleMode(true);

        //_sponsorButton.On_Press += OnPressSponsorButton;
        //_sponsorButton.Activate();
    }

    private void OnPressSponsorButton()
    {
        EndRefreshTimer();
        ShowAD();
    }

    private void ShowAD()
    {
        //_adManager.On_CompletedAD += OnCompletedAD;
        //_adManager.ShowRewardAD();
    }

    //private void OnCompletedAD(ShowResult result)
    //{
    //    if(_isDebug) Debug.Log("AD Show Result: " + result);
    //    //_adManager.On_CompletedAD -= OnCompletedAD;
    //    if(result == ShowResult.Failed)
    //    {
    //        if (IsIdleMode()) Refresh();
    //    }
    //    else if(result == ShowResult.Skipped)
    //    {
    //        if (IsIdleMode()) Refresh();
    //    }
    //    else if(result == ShowResult.Finished)
    //    {
    //        // Analytics
    //        GameData.instance.GetConfigData().SponsorCount += 1;
    //        //if (GameData.instance.GetConfigData().SponsorCount == 5) UnityAnalytics.CustomEvent("AD", new Dictionary<string, object> { { "AD 5 Times", 1 } });
    //        //else if (GameData.instance.GetConfigData().SponsorCount == 10) UnityAnalytics.CustomEvent("AD", new Dictionary<string, object> { { "AD 10 Times", 1 } });
    //        //else if (GameData.instance.GetConfigData().SponsorCount == 20) UnityAnalytics.CustomEvent("AD", new Dictionary<string, object> { { "AD 20 Times", 1 } });

    //        ChangeSponsorshipMode();
    //    }
    //}

    private void ChangeSponsorshipMode()
    {
        //_sponsorData.goldBoosterAmount = _sponsorData.rewardSponsorshipCount;
//        if (GameManager.isFreeVersion) _goldDisplay.Save(_sponsorData.rewardGoldForFree);
//        else _goldDisplay.Save(_sponsorData.rewardGold);

        _sponsorData.isSponsoring = true;
        ChangeSponsorshipModeUI();
    }

    private void CloseSponsorship()
    {
        if(_isDebug) Debug.Log("Close Sponsor Ship");
        //_ui.ChangeIdleMode(false);
        //_sponsorButton.Deactivate();
    }

    private void StartRefreshTimer()
    {
        if(_isDebug) Debug.Log("Start Refresh Timer");
        DOTween.To(() => this.refreshTimerValue, x => this.refreshTimerValue = x, 0, _refreshTime).SetId("RefreshTimer").OnComplete(Refresh);
    }

    private void EndRefreshTimer()
    {
        if(_isDebug) Debug.Log("End Refresh Timer");
        DOTween.Kill("RefreshTimer");
    }

    void OnDestroy()
    {
        if (IsIdleMode()) EndRefreshTimer();
        //_sponsorButton.On_Press -= OnPressSponsorButton;
        //_adManager.On_CompletedAD -= OnCompletedAD;
    }

    public bool IsIdleMode()
    {
        //bool result;
        //if (IsSponsorshipMode())
        //{
        //    if(_isDebug) Debug.Log("*** IsIdleMode");
        //    result = false;
        //}
        //else result = true;
        return true;
    }
}
