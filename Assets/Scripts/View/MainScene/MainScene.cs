﻿using UnityEngine;
using DG.Tweening;
using Com.Mod.ThumbRun.Control;
using System.Collections.Generic;
using Com.Mod.ThumbRun.LuckyChance.View;
using Com.Mod.ThumbRun.LuckyChance.Application;
using Com.Mod.Game.LanguageManager;
using Com.Mod.ThumbRun.User.Application;
using Com.Mod.ThumbRun.Race.Application;
using GameDataEditor;
using Com.Mod.ThumbRun.Tutorial.Application;
using System;
using Com.Mod.ThumbRun.RouletteGame.Application;
using Com.Mod.ThumbRun.Adid.Infrastructure;
using TapjoyUnity;

public class MainScene : MonoBehaviour
{
    public delegate void MainSceneEvent();
    public event MainSceneEvent ShutterOpen;

    public MainSceneUI ui;
    // public Flag wrFlag;
    // public Flag frFlag;
    public TestPanel testPanel;
    public SponsorManager sponsorManager;
    public MapMovementModel mapMovementModel;
    public Camera _camera;

    [SerializeField]
    private RouletteGameService _rouletteGameService;
    [SerializeField]
    private LuckyChanceStatusView _luckyChanceStatusView;
    [SerializeField]
    private LuckyChanceService _luckyChanceService;
    [SerializeField]
    private UserService _userService;
    [SerializeField]
    private EntryService _entryService;
    [SerializeField]
    private TutorialService _tutorialService;
    [SerializeField]
    private AdidServerManager _adidServerManager;
    private bool _isDebug = true;
    private GameManager _gameManager;
    private WinnerDataManager _winnerDataManager;
    private LuckyChance _luckyChance;
    //private StageData _stageData;
    private bool _isOnline;
    private bool _isMovingToPlayScene = false;
       

    void Awake()
    {
        Debug.Log(DateTime.UtcNow);

        if (_isDebug) Debug.Log("MainScene - Awake()");


        //if(GameManager.isFirstTimeLobbyTemp)
        //{
        //    GameManager.isFirstTimeLobbyTemp = false;
        //    Tapjoy.SetGcmSender("957778974846");
        //}

        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        _winnerDataManager = GameObject.FindGameObjectWithTag("WinnerDataManager").GetComponent<WinnerDataManager>();
        _luckyChance = GameObject.FindGameObjectWithTag("LuckyChance").GetComponent<LuckyChance>();

        if(GameManager.useDebugPannel)InitTestPannel();

        _luckyChanceService.Prepared += OnPrepared;
        _userService.Prepared += OnPrepared;
        _entryService.Prepared += OnPrepared;
        _tutorialService.Prepared += OnPrepared;

        //TimeSpan timeSpan = TimeSpan.FromSeconds(604800 + 604800 + 86400 + 86400 + 18000 + 1980);
        //TimeSpan timeSpan = TimeSpan.FromSeconds(50);
        //Debug.Log(string.Format("{0:D}d {1:D1}h {2:D2}m", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes));
        //Debug.Log(string.Format("{0:D}m", timeSpan.Minutes));
    }

    private void InitTestPannel()
    {
        testPanel.gameObject.SetActive(true);
        testPanel.Init(_gameManager.adManager);
    }

    private int GetSelectedCharacterID()
    {
        return GameData.instance.GetConfigData().SelectedCharacterID;
    }

	void Start ()
    {
        ui.Init(GameData.instance.GetConfigData().CountryCode);
        ui.SetButtonActive(false);

        mapMovementModel.On_Next += OnChangeMap;
        mapMovementModel.On_Prev += OnChangeMap;

        mapMovementModel.SetCharacterID(GetSelectedCharacterID());

        OpenShutter();

        _luckyChance.Prepare();

#if UNITY_ANDROID
        if(GameManager.isFirstTimeLobbyTemp)
        {
            Application.RequestAdvertisingIdentifierAsync(
                (string advertisingId, bool trackingEnabled, string error) =>
                {
                    Debug.Log("MainScene - advertisingId " + advertisingId + " " + trackingEnabled + " " + error);
                    _adidServerManager.RequestAdid(advertisingId);
                }
            );
        }
#endif
    }

    void OnDestroy()
    {
        _gameManager.sceneLoader.ShutterOpened -= OnShutterOpened;

        mapMovementModel.On_Next -= OnChangeMap;
        mapMovementModel.On_Prev -= OnChangeMap;

        _luckyChanceService.Prepared -= OnPrepared;
        _userService.Prepared -= OnPrepared;
        _entryService.Prepared -= OnPrepared;
        _tutorialService.Prepared -= OnPrepared;
    }




    private void OnPrepared()
    {
        if (!_luckyChanceService.isPrepared) return;
        if (!_userService.isPrepared) return;
        if (!_entryService.isPrepared) return;
        if (!_tutorialService.isPrepared) return;

        int entryIndex = GetRightEntryIndex();

        string stageId = _entryService.GetEntryInfo(entryIndex).stageId;
        _luckyChanceStatusView.Show();
        mapMovementModel.Init(entryIndex);
        RaceModeInfo.stageId = stageId;

        GameData.instance.ClearAllData();
    }

    private int GetRightEntryIndex()
    {
        //Debug.Log("entryInfo.count: " + _entryService.GetEntryInfos().Count);
        List<GDEEntryItemData> entryInfos = _entryService.GetEntryInfos();
        int currentEntryIndex = 1;
        for(int i = 0; i < entryInfos.Count; i++)
        {
            //Debug.Log("gold: " + _userService.GetGoldAmount() + " / entryInfos[i].entryFee: " + entryInfos[i].entryFee);
            if (_userService.GetGoldAmount() >= entryInfos[i].entryFee) 
            {
                if(_userService.GetLevel() >= entryInfos[i].limitLv)
                {
                    currentEntryIndex = i;
                }
            }
        }

        return currentEntryIndex;
        //_userService.GetGoldAmount
    }
    
    private void OpenShutter()
	{
        if (_isDebug) Debug.Log("MainScene - OpenShutter()");
        _gameManager.sceneLoader.ShutterOpened += OnShutterOpened;
        _gameManager.sceneLoader.OpenShutter(0.6f);

        if(ShutterOpen != null) ShutterOpen();
	}

    private void OnShutterOpened()
    {
        _gameManager.sceneLoader.ShutterOpened -= OnShutterOpened;
        _rouletteGameService.OpenGame();
    }

    private bool IsFirstTime()
    {
        return GameData.instance.GetConfigData().IsFirstTime;
    }

    private bool IsFirstTimeMain()
    {
        return GameData.instance.GetConfigData().IsFirstTimeMain;
    }

    private void OnChangeMap(bool isEnd)
    {
        if (IsRandomStage()) RaceModeInfo.stageId = _entryService.GetRandomStageId();
        else RaceModeInfo.stageId = _entryService.GetStageIdByEntryIndex(mapMovementModel.GetCurrentMenuIndex());
    }

    private bool IsRandomStage()
    {
        return (mapMovementModel.GetCurrentMenuIndex() == 0) ? true : false;
    }

    public void MoveToRaceScene(RaceMode.mode raceMode, bool isTutorialMode = false, string stageId = null)
    {
        if(!_isMovingToPlayScene)
        {
            _isMovingToPlayScene = true;
            RaceModeInfo.entryIndex = mapMovementModel.GetCurrentMenuIndex();
            if(stageId != null) RaceModeInfo.stageId = stageId;
            RaceModeInfo.raceMode = raceMode;
            RaceModeInfo.isTutorialMode = isTutorialMode;
            ui.SetButtonActive(false);

            // 튜토리얼 모드일 경우 Entry Index를 1로 변경함. 아닌경우 현재 선택 된 맵 Index를 Entry Index로 지정.
            int entryIndex;
            if (isTutorialMode) entryIndex = 1;
            else entryIndex = mapMovementModel.GetCurrentMenuIndex();
            _entryService.SendEntry(entryIndex);

            _gameManager.sceneLoader.Load(SceneName.PLAY);
        }
    }
}
