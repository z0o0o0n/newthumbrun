﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LocalPersonalRecordReportor : MonoBehaviour 
{
    public delegate void LocalPersonalRecordReportEvent();
    public event LocalPersonalRecordReportEvent ReportCompleted;
    public event LocalPersonalRecordReportEvent ReportFailed;
    public event LocalPersonalRecordReportEvent NoPersonalRecord;

    private bool _isDebug = true;
    private string _logPrefix = "LocalPersonalRecordReportor - ";
    private int _currentReportedCount = 0;
    private List<StageData> _stageDataList;

	void Awake ()
	{
	}

	void Start () 
	{

 	}
	
	void Update () 
	{
	
	}

    public void ReportPersonalRecords()
    {
        if (_isDebug) Debug.Log(_logPrefix + "Report()");

        _stageDataList = StageDataManager.instance.GetStageDataList();

        Report();
    }

    private void Report()
    {
        if (HasPersonalRecord())
        {
            string leaderboardId = GetLeaderboardID();
            //float record = _stageDataList[_currentReportedCount].personalRecord.record;
            string countryCode = GameData.instance.GetConfigData().CountryCode;

            // Country Code 확인
			if(_isDebug) Debug.Log(_logPrefix + "countryCode == ''" + (countryCode == ""));
            if (countryCode == "")
            {
                GameData.instance.GetConfigData().CountryCode = "001"; // 선택된 CountryCode가 없다면 기본값으로 재설정
                Debug.Log(_logPrefix + "Report() / 선택된 CountryCode가 없어 기본(001)로 재설정함.");
            }
        }
        else
        {
            // 현재 Stage에 PersonalRecord(개인기록)가 없다면
            if (_isDebug) Debug.Log(_logPrefix + "Report() / " + _stageDataList[_currentReportedCount].id + " 스테이지에 개인기록이 없어 Pass 함.");
            
            _currentReportedCount++;
            if (_currentReportedCount >= _stageDataList.Count) OnAllRecordReportCompleted();
            else Report();
        }
    }

    private bool HasPersonalRecord()
    {
        return false;
        //return _stageDataList[_currentReportedCount].hasPersonalRecord;
    }

    private string GetLeaderboardID()
    {
        string result = "";
        //#if UNITY_IOS
        //    result = _stageDataList[_currentReportedCount].id;
        //#elif UNITY_ANDROID && !UNITY_EDITOR
        //    result = _stageDataList[_currentReportedCount].gplbID;
        //#endif
        return result;
    }

    private bool IsJailbroken()
    {
		Debug.Log (_logPrefix + "IsJailbroken() / Step 3 / ConfigData: " + GameData.instance.GetConfigData());
        return GameData.instance.GetConfigData().isJailbroken;
    }

    private void OnRecordReportCompleted()
    {
        if (_isDebug) Debug.Log(_logPrefix + "OnRecordReportCompleted()");

        _currentReportedCount++;
        if (_currentReportedCount >= _stageDataList.Count) OnAllRecordReportCompleted();
        else Report();
    }

    private void OnAllRecordReportCompleted()
    {
        if (_isDebug) Debug.Log("LocalPersonalRecordReportor - OnAllRecordReportCompleted()");

        _currentReportedCount = 0;

        if (ReportCompleted != null) ReportCompleted();
    }

    private void OnRecordReportFailed()
    {
        if (_isDebug) Debug.Log("LocalPersonalRecordReportor - OnRecordReportFailed()");

        _currentReportedCount = 0;

        if (ReportFailed != null) ReportFailed();
    }

	void OnDestroy()
	{
	}
}
