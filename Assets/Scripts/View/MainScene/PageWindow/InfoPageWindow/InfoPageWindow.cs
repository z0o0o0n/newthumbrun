﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;
using System.Collections.Generic;

public class InfoPageWindow : MonoBehaviour 
{
    public delegate void InfoPageWindowEvent();
    public event InfoPageWindowEvent ClickInstagramButton;
    public event InfoPageWindowEvent ClickEmailButton;
    public event InfoPageWindowEvent ClickRatingButton;
    public UIBasicButton _instagramButton;
    public UIBasicButton _emailButton;
    public UIBasicButton _ratingButton;
    public UIBasicButton okButton;

    void Awake()
    {
        _instagramButton.On_Click += OnInstagramButtonClick;
        _emailButton.On_Click += OnEmailButtonClick;
        _ratingButton.On_Click += OnRatingButtonClick;
    }

    void OnEnable()
    {
    }

    void OnDisable()
    {
    }

    private void OnInstagramButtonClick()
    {
        Application.OpenURL("https://www.instagram.com/thumb.kim/");
        if (ClickInstagramButton != null) ClickInstagramButton();
    }

    private void OnEmailButtonClick()
    {
        Application.OpenURL("mailto:kimjunheeman@gmail.com");
        if (ClickEmailButton != null) ClickEmailButton();
    }

    private void OnRatingButtonClick()
    {
        #if UNITY_IOS
        Application.OpenURL("itms-apps://itunes.apple.com/app/id997232976");
        #elif UNITY_ANDROID
        Application.OpenURL("market://details?id=com.modgames.thumb.run");
        #endif

        if (ClickRatingButton != null) ClickRatingButton();
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    void OnDestory()
    {
        _instagramButton.On_Click -= OnInstagramButtonClick;
        _emailButton.On_Click -= OnEmailButtonClick;
        _ratingButton.On_Click -= OnRatingButtonClick;
    }
}
