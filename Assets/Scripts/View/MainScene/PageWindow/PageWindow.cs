﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Junhee.Utils;

public class PageWindow : MonoBehaviour
{
    [Header("Popup")]
    public SettingPageWindow settingPageWindow;
    public CountryPageWindow countryPageWindow;
    public InfoPageWindow infoPageWindow;
    public UIBasicButton closeButton;
	public UIWidget uiWidget;

    public delegate void OnMotionCompleteDele();
    private OnMotionCompleteDele _onShowMotionCompleteCallback;
    private OnMotionCompleteDele _onHideMotionCompleteCallback;

    public delegate void PopupEvent(string popupName);
    public event PopupEvent Open;
    public event PopupEvent On_Close;
    public delegate void CountryEvent(string code);
    public event CountryEvent On_ChangeCountry;

    private bool _isOpen = false;
    private string _currentPopupName = "";
    private GameObject _currentPopup;

    void Awake()
    {
        countryPageWindow.On_ChangeCountry += OnChangeCountry;
        closeButton.On_Click += OnCloseButtonClick;
		uiWidget.width = UIScreen.instance.GetWidth();
        uiWidget.height = UIScreen.instance.GetHeight();
    }

    void OnEnable()
    {
     
    }

	void Start () 
    {
        transform.localPosition = new Vector3(0, -UIScreen.instance.GetWidth(), 0);
	}

	void Update () 
    {
        
	}

    private void OnCloseButtonClick()
    {
        ClosePopup();
    }

    private void OnChangeCountry(string code)
    {
        closeButton.gameObject.SetActive(true);
        if (On_ChangeCountry != null) On_ChangeCountry(code);
        ClosePopup();
    }

    private bool ChagePrefab(string popupName)
    {
        if(_currentPopup)
        {
            _currentPopup.gameObject.SetActive(false);
        }

        closeButton.gameObject.SetActive(true);

        _currentPopupName = popupName;

        if (popupName == PageWindowName.SETTING)
        {
            _currentPopup = settingPageWindow.gameObject;
            _currentPopup.gameObject.SetActive(true);
            ChangeCloseButtonColor(ColorUtil.ConvertHexStringToColor("0x444444"));
            return true;
        }
        else if (popupName == PageWindowName.COUNTRY)
        {
            if (!countryPageWindow.HasCountryCode()) closeButton.gameObject.SetActive(false);
            _currentPopup = countryPageWindow.gameObject;
            _currentPopup.gameObject.SetActive(true);
            ChangeCloseButtonColor(ColorUtil.ConvertHexStringToColor("0xffffff"));
            return true;
        }
        else if (popupName == PageWindowName.INFO)
        {
            _currentPopup = infoPageWindow.gameObject;
            _currentPopup.gameObject.SetActive(true);
            ChangeCloseButtonColor(ColorUtil.ConvertHexStringToColor("0x444444"));
            return true;
        }
        else
        {
            return false;
        }
    }

    // Show Popup
    public bool ShowPopup(string popupName, float time = 0.5f, OnMotionCompleteDele callback = null)
    {
        if(!_isOpen)
        {
            Debug.Log("국기 설정 팝업 보이기");

            if (ChagePrefab(popupName))
            {
                _isOpen = true;
                if (callback != null) _onShowMotionCompleteCallback = callback;
                if (Open != null) Open(popupName);
                transform.DOLocalMoveY(0, time).SetEase(Ease.OutCubic).OnComplete(OnCompleteShowMotion);
                return true;
            }
            else
            {
                Debug.LogError("No Popup. Named " + popupName + ".");
                return false;
            }
        }
        else
        {
            Debug.LogError("Already opend Popup.");
            return false;
        }
    }

    private void OnCompleteShowMotion()
    {
        if(_onShowMotionCompleteCallback != null) _onShowMotionCompleteCallback();
    }

    // Close Popup
    public void ClosePopup(float time = 0.3f, OnMotionCompleteDele callback = null)
    {
        if(_isOpen)
        {
            if (callback != null) _onHideMotionCompleteCallback = callback;
            transform.DOLocalMoveY(-UIScreen.instance.GetHeight(), time).SetEase(Ease.InCubic).OnComplete(OnCompleteHideMotion);
        }
    }

    private void OnCompleteHideMotion()
    {
        _isOpen = false;
        if(_onHideMotionCompleteCallback != null) _onHideMotionCompleteCallback();
        if (On_Close != null) On_Close(_currentPopupName);
    }

    private void ChangeCloseButtonColor(Color color)
    {
        UIButton uiButton = closeButton.GetComponent<UIButton>();
        uiButton.pressed = color;
        uiButton.hover = color;
        uiButton.disabledColor = color;
        uiButton.defaultColor = color;
        UISprite uiSprite = closeButton.GetComponent<UISprite>();
        uiSprite.color = color;
    }

    void OnDestroy()
    {
        countryPageWindow.On_ChangeCountry -= OnChangeCountry;
        closeButton.On_Click -= OnCloseButtonClick;
    }
}
