﻿using UnityEngine;
using System.Collections;

public class FlagGrid : MonoBehaviour
{
    public delegate void FlagGridEvent(string code);
    public event FlagGridEvent On_PressFlagButton;

    public GameObject flagButton;
    public UIScrollView scrollView;
    public AudioSource buttonAudioSource;

    public string[] flagNames;
	
	void Start ()
    {
        InitItems();
        //scrollView.ResetPosition();
	}
	
    private int _count = 0;
    private void InitItems()
    {
        for(int i = 0; i < flagNames.Length; i++)
        {
            GameObject clone = (GameObject) GameObject.Instantiate(flagButton, Vector3.zero, Quaternion.identity);
            clone.transform.parent = transform;
            clone.transform.localScale = Vector3.one;
            clone.GetComponent<FlagButton>().Init(flagNames[i]);
            //clone.GetComponent<FlagButton>().On_PressFlagButton += OnPressFlagButton;
            UISprite uiSprite = clone.GetComponent<UISprite>();
            uiSprite.spriteName = "Flag_" + flagNames[i];
            UIBasicButton uiBasicButton = clone.GetComponent<UIBasicButton>();
            uiBasicButton._audioSource = buttonAudioSource;
        }

        GetComponent<UIGrid>().Reposition();
        scrollView.ResetPosition();
        scrollView.gameObject.GetComponent<UIPanel>().baseClipRegion = new Vector4(0, 0, 300, Screen.height);
    }
	
	void Update () {
	
	}

    private void OnPressFlagButton(string code)
    {
        if (On_PressFlagButton != null) On_PressFlagButton(code);
    }

    void OnDestroy()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.GetComponent<FlagButton>().On_PressFlagButton -= OnPressFlagButton;
        }
    }
}
