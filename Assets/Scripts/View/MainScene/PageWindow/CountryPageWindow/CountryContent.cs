﻿using UnityEngine;
using System.Collections;

public class CountryContent : MonoBehaviour 
{
    public FlagMenuPannelBg menuPanel;

	void Start () 
	{
        float halfWidth = ((Screen.width - menuPanel.panelWidth) / 2);
        float centerPos = (Screen.width / 2) - halfWidth;
        Vector3 pos = transform.localPosition;
        pos.x = centerPos;
        transform.localPosition = pos;
	}
	
	void Update () 
	{
	
	}
}
