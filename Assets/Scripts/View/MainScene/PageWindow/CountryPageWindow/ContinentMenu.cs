﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;
using System.Collections.Generic;

public class ContinentMenu : MonoBehaviour 
{
    public delegate void ContinentMenuEvent(int selectedIndex);
    public event ContinentMenuEvent On_PressMenu;

    public UIBasicButton americaButton;
    public UIBasicButton africaButton;
    public UIBasicButton asiaAnAustButton;
    public UIBasicButton europeButton;
    public List<UIBasicButton> uiButtonList;

    void Awake ()
    {
        uiButtonList.Add(americaButton);
        uiButtonList.Add(africaButton);
        uiButtonList.Add(asiaAnAustButton);
        uiButtonList.Add(europeButton);

        americaButton.On_Click += OnPressAmerica;
        africaButton.On_Click += OnPressAfrica;
        asiaAnAustButton.On_Click += OnPressAsiaAndAust;
        europeButton.On_Click += OnPressEurope;

        transform.localPosition = new Vector3((-UIScreen.instance.GetWidth()/2) + 210, 0, 0);
    }
	
	void Start () 
    {
       
	}

    public void Select(int index)
    {
        //Debug.LogWarning("uiButtonList.Count : " + uiButtonList.Count);
        for (int i = 0; i < uiButtonList.Count; i++)
        {
            UIButton uibutton = uiButtonList[i].GetComponent<UIButton>();
            //Debug.LogWarning("index------------i " + index);
            if (i == index)
            {
                uibutton.enabled = false;
                uibutton.gameObject.GetComponent<UISprite>().color = ColorUtil.ConvertHexToColor(0x444444);
            }
            else
            {
                uibutton.enabled = true;
                uibutton.gameObject.GetComponent<UISprite>().color = ColorUtil.ConvertHexToColor(0xb4b4b4);
            }
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void OnPressAmerica()
    {
        Select(0);
        if (On_PressMenu != null) On_PressMenu(0);
    }

    public void OnPressAfrica()
    {
        Select(1);
        if (On_PressMenu != null) On_PressMenu(1);
    }

    public void OnPressAsiaAndAust()
    {
        Select(2);
        if (On_PressMenu != null) On_PressMenu(2);
    }

    public void OnPressEurope()
    {
        Select(3);
        if (On_PressMenu != null) On_PressMenu(3);
    }

    void OnDestory()
    {
        americaButton.On_Click -= OnPressAmerica;
        africaButton.On_Click -= OnPressAfrica;
        asiaAnAustButton.On_Click -= OnPressAsiaAndAust;
        europeButton.On_Click -= OnPressEurope;
    }
}
