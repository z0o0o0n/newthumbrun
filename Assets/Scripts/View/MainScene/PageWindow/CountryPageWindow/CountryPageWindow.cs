﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;

public class CountryPageWindow : MonoBehaviour 
{
    public delegate void CountryPopupEvent(string code);
    public event CountryPopupEvent On_ChangeCountry;

    //public GameObject flagButton;
    public ContinentMenu continentMenu;
    public FlagScrollView flagScrollView;
    public int defaultSelectionIndex = 0;
    public UIBasicButton okButton;
    public string selectedCountryCode;

    [Header("Flag Preview")]
    public UIWidget popup;
    public Camera highLevelCamera;
    public FlagGround flagGround;
    public Flag previewFlag;
    public UISprite flagPreviewBg;
    public float offsetX = 0;
    public float offsetY = 0;


    //private string[] flagNames = { "662", "704", "702" };

    void Awake()
    {
        continentMenu.On_PressMenu += OnPressContinentMenu;
        flagScrollView.On_PressFlagButton += OnPressFlagButton;
        okButton.On_Click += OnClickOkbutton;
    }

    void OnEnable()
    {
        if (flagGround) flagGround.gameObject.SetActive(true);
        previewFlag.ChangeFlag(GameData.instance.GetConfigData().CountryCode);
        okButton.On_Click += OnClickOkbutton;

        //Debug.LogWarning ("----- COUNTRY CODE: " + GameData.instance.GetConfigData ().CountryCode);
        //Debug.LogWarning (HasCountryCode ());
        if (!HasCountryCode())
        {
            Debug.Log("CountryPageWindow - OnEnable() / 현재 선택 된 CountryCode가 업습니다.");
			okButton.IsEnabled(false);
        }
    }

    void OnDisable()
    {
        if (flagGround) flagGround.gameObject.SetActive(false);
    }

    void Start ()
    {
        continentMenu.Select(defaultSelectionIndex);
        flagScrollView.ShowFlagGrid(defaultSelectionIndex);

        //for(int i = 0; i < flagNames.Length; i++)
        //{
        //    GameObject clone = (GameObject)GameObject.Instantiate(flagButton, Vector3.zero, Quaternion.identity);
        //    clone.transform.parent = transform;
        //    clone.transform.localPosition = new Vector3(0, i * 90, 0);
        //    clone.transform.localScale = Vector3.one;
        //    UISprite uiSprite = clone.GetComponent<UISprite>();
        //    uiSprite.spriteName = "Flag_" + flagNames[i];
        //}
	}

    public bool HasCountryCode()
    {
        Debug.Log("CountryPageWindow - HasCountryCode() / countryCode: " + GameData.instance.GetConfigData().CountryCode);
        if (GameData.instance.GetConfigData().CountryCode == "001") return false;
        else if(GameData.instance.GetConfigData().CountryCode == "") return false;
        else return true;
    }
	
	void Update ()
    {
        UpdatePosition();
	}

    private void OnPressFlagButton(string code)
    {
        selectedCountryCode = code;
        previewFlag.ChangeFlag(selectedCountryCode);

        okButton.SetButtonActive(true);
		okButton.IsEnabled(true);
    }

    private void OnClickOkbutton()
    {
        okButton.On_Click -= OnClickOkbutton;

        if (On_ChangeCountry != null) On_ChangeCountry(selectedCountryCode);
    }

    private void UpdatePosition()
    {
        float xpos = (Screen.width / 2) + flagPreviewBg.transform.localPosition.x;
        //float ypos = (Screen.height / 2) + positionReference.transform.localPosition.y;
        float ypos = (Screen.height / 2) + popup.transform.localPosition.y - 40;

        Vector3 newPos = highLevelCamera.ScreenToWorldPoint(new Vector3(xpos, ypos, 4));

        flagGround.transform.localPosition = new Vector3(newPos.x + offsetX, newPos.y + offsetY, previewFlag.transform.localPosition.z);
        //Debug.Log("xpos: " + xpos + " / ypos: " + ypos + " / " + flagPreviewBg.transform.localPosition.x);
    }

    private void OnPressContinentMenu(int selectedIndex)
    {
        Debug.Log("selected: " + selectedIndex);
        flagScrollView.ShowFlagGrid(selectedIndex);

        //if(selectedIndex == 0)
        //{
        //    flagScrollView.ShowFlagGrid(0);
        //}
        //else if(selectedIndex == 1)
        //{
        //    flagScrollView.ShowFlagGrid(1);
        //}
    }

    void OnDestroy()
    {
        continentMenu.On_PressMenu -= OnPressContinentMenu;
        flagScrollView.On_PressFlagButton -= OnPressFlagButton;
        okButton.On_Click -= OnClickOkbutton;
    }
}
