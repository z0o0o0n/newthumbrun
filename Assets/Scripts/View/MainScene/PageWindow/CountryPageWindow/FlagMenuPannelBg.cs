﻿using UnityEngine;
using System.Collections;

public class FlagMenuPannelBg : MonoBehaviour 
{
    public UISprite uiSprite;
    public UISprite pannelShadow;
    public int panelWidth;

    void Awake()
    {
        uiSprite.transform.localPosition = new Vector3(-(UIScreen.instance.GetWidth()) / 2, 0, 0);
        uiSprite.height = UIScreen.instance.GetHeight();

        pannelShadow.transform.localPosition = new Vector3(-(UIScreen.instance.GetWidth()) / 2, 0, 0);
        pannelShadow.height = UIScreen.instance.GetHeight() - 40;

        //Debug.LogWarning("----- screen width: " + UIScreen.instance.GetWidth());
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}
}
