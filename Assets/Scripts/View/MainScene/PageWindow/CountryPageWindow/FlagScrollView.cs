﻿using UnityEngine;
using System.Collections;

public class FlagScrollView : MonoBehaviour 
{
    public delegate void FlagScrollViewEvent(string code);
    public event FlagScrollViewEvent On_PressFlagButton;

    public FlagGrid[] flagGrids;
	
    void Awake()
    {
        transform.localPosition = new Vector3((-UIScreen.instance.GetWidth() / 2) + 290, 0, 0);
    }

	void Start ()
    {
	    
	}
	
	void Update ()
    {
	
	}

    public void ShowFlagGrid(int index)
    {
        for (int i = 0; i < flagGrids.Length; i++ )
        {
            if(i == index)
            {
                flagGrids[i].gameObject.SetActive(true);
                flagGrids[i].GetComponent<UIGrid>().Reposition();
                flagGrids[i].On_PressFlagButton += OnPressFlagButton;
            }
            else
            {
                flagGrids[i].gameObject.SetActive(false);
                flagGrids[i].On_PressFlagButton -= OnPressFlagButton;
            }
        }
        GetComponent<UIScrollView>().ResetPosition();
    }

    private void OnPressFlagButton(string code)
    {
        if (On_PressFlagButton != null) On_PressFlagButton(code);
    }

    void OnDestroy()
    {
        for (int i = 0; i < flagGrids.Length; i++)
        {
            flagGrids[i].On_PressFlagButton -= OnPressFlagButton;
        }
    }
}
