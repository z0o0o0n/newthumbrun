﻿using UnityEngine;
using System.Collections;

public class FlagButton : MonoBehaviour
{
    public delegate void FlagButtonEvent(string code);
    public event FlagButtonEvent On_PressFlagButton;

    private UISprite _uiSprite;
    private string _code;

    public void Init(string flagCode)
    {
        _code = flagCode;
        _uiSprite = GetComponent<UISprite>();
        _uiSprite.spriteName = "Flag_" + _code;
    }

	void Start ()
    {
        
	}
	
	void Update ()
    {
	
	}

    public void OnPressButton()
    {
        if(On_PressFlagButton != null) On_PressFlagButton(_code);
    }
}
