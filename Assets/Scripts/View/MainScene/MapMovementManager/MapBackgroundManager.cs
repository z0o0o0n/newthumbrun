﻿using UnityEngine;
using System.Collections;

public class MapBackgroundManager : MonoBehaviour 
{
    public MapBackground[] mapBackgroundList;

    private int _currentIndex = 0;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	}

    public void Init(int mapIndex)
    {
        for (int i = 0; i < mapBackgroundList.Length; i++)
        {
            if (i != mapIndex)
            {
                mapBackgroundList[i].Hide(0, 1);
            }
        }
    }

    public void Drag(float deltaX)
    {
        mapBackgroundList[_currentIndex].Drag(deltaX);
    }

    public void Return(float time)
    {
        mapBackgroundList[_currentIndex].Return(time);
    }

    public void Move(int index, float time, int direction = 0)
    {
        mapBackgroundList[_currentIndex].Hide(time, -direction);
        mapBackgroundList[index].Show(time, direction);
        _currentIndex = index;
    }
}
