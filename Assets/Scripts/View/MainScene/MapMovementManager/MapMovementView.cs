﻿using UnityEngine;
using System.Collections;
using view.mapScene;
using DG.Tweening;
using Com.Mod.ThumbRun.Race.View;
using Com.Mod.ThumbRun.Race.Application;
using GameDataEditor;
using Com.Mod.ThumbRun.User.Application;
using System.Collections.Generic;

public class MapMovementView : MonoBehaviour 
{
    public Camera camera;
    public GameObject mapGroundContainer;
    public MapTitle _mapTitle;
    public AudioSource _audioSource;
    public AudioClip _moveSound;

    private const float MOVEMENT_TIME = 0.6f;
    private const float END_RETURN_TIME = 0.8f;
    private const float RETURN_TIME = 0.3f;
    private const float CHARACTER_MOVEMENT_TIME = 1.5f;

    [SerializeField]
    private MainScene _mainScene;
    [SerializeField]
    private TouchArea _touchArea;
    [SerializeField]
    private OnlinePvpButton _onlinePvpButton;
    [SerializeField]
    private VsComButton _vsComButton;
    [SerializeField]
    private NoEntryFeePopup _noEntryFeePopup;
    [SerializeField]
    private LimitLvPopup _limitLvPopup;
    [SerializeField]
    private LobbyBg _lobbyBg;
    [SerializeField]
    private EntryService _entryService;
    [SerializeField]
    private UserService _userService;
    private bool _isDebug = false;
    private MapMovementModel _model;
    private Vector2 _dragPoint2D = Vector2.zero;
    private Vector3 _dragPoint3D = Vector3.zero;
    private float _mapThumbnailWidth = 500f;
    //private GameObject _mapCharacter;

    void Awake()
    {
        _model = GetComponent<MapMovementModel>();
        _model.On_Init += OnInit;
        _model.On_Prev += OnPrev;
        _model.On_Next += OnNext;
        _model.On_StartDrag += OnStartDrag;
        _model.On_Return += OnReturn;
        //_model.On_InitCharacter += OnInitCharacter;

        _touchArea.On_Press += OnPress;
        _touchArea.On_Drag += OnDrag;
        _touchArea.On_Release += OnRelease;

		_entryService.Prepared += OnEntryServicePrepared;

        _onlinePvpButton.Click += OnOnlinePvpButtonClick;
        _vsComButton.Click += OnVsComButtonClick;

        UpdateDragPoint2D(mapGroundContainer.transform.localPosition);
    }

    void Start()
    {
        //mapBackgroundManager.Init(_model.GetCurrentMenuIndex());
    }

    void FixedUpdate()
    {
        if (!_model.isMoving)
        {
            //Vector3 localPos = mapGroundContainer.transform.localPosition;
            //localPos.x += ((_dragPoint3D.x) - localPos.x) * 0.9f;
            //mapGroundContainer.transform.localPosition = localPos;
        }
    }

    void OnDestroy()
    {
        _model.On_Init -= OnInit;
        _model.On_Prev -= OnPrev;
        _model.On_Next -= OnNext;
        _model.On_StartDrag -= OnStartDrag;
        _model.On_Return -= OnReturn;

        _touchArea.On_Press -= OnPress;
        _touchArea.On_Drag -= OnDrag;
        _touchArea.On_Release -= OnRelease;

		_entryService.Prepared -= OnEntryServicePrepared;

        _onlinePvpButton.Click -= OnOnlinePvpButtonClick;
        _vsComButton.Click -= OnVsComButtonClick;
    }

    private void OnInit()
    {
        //Debug.Log("+++++ Map Movement View. Init");
		if (_entryService.isPrepared) {
			MoveMap(_model.GetCurrentMenuIndex(), 0);
		}
        
        //mapBackgroundManager.Move(_model.GetCurrentMapIndex(), 0);

        //_mapCharacter.transform.localPosition = new Vector3(_model.GetCurrentMapIndex() * _mapThumbnailWidthf, 0.01f, 1f);
    }

	private void OnEntryServicePrepared()
	{
		if (_model.isInit) {
			MoveMap(_model.GetCurrentMenuIndex(), 0);
		}
	}

    private void OnPrev(bool isEnd)
    {
        if(!isEnd)
        {
            MoveMap(_model.GetCurrentMenuIndex(), MOVEMENT_TIME);

            _audioSource.clip = _moveSound;
            _audioSource.Play();

            //MoveCharacter(-1);
            //mapBackgroundManager.Move(_model.GetCurrentMenuIndex(), MOVEMENT_TIME, -1);

            
        }
        else
        {
            _model.isMoving = true;
            DOTween.Kill("MapMovement_End_Return");
            mapGroundContainer.transform.DOLocalMoveX(_model.GetCurrentMenuIndex() * -_mapThumbnailWidth, END_RETURN_TIME).SetId("MapMovement_End_Return").SetEase(Ease.OutSine).OnComplete(OnMoved);

            //mapBackgroundManager.Return(END_RETURN_TIME);
            _mapTitle.Show(0.3f);

            _onlinePvpButton.gameObject.SetActive(true);
            _vsComButton.gameObject.SetActive(true);
        }
    }

    private void OnNext(bool isEnd)
    {
        if (!isEnd)
        {
            MoveMap(_model.GetCurrentMenuIndex(), MOVEMENT_TIME);

            _audioSource.clip = _moveSound;
            _audioSource.Play();

            //MoveCharacter(1);
            //mapBackgroundManager.Move(_model.GetCurrentMenuIndex(), MOVEMENT_TIME, 1);

            
        }
        else
        {
            _model.isMoving = true;
            DOTween.Kill("MapMovement_End_Return");
            mapGroundContainer.transform.DOLocalMoveX(_model.GetCurrentMenuIndex() * -_mapThumbnailWidth, END_RETURN_TIME).SetId("MapMovement_End_Return").SetEase(Ease.OutSine).OnComplete(OnMoved);

            //mapBackgroundManager.Return(END_RETURN_TIME);
            _mapTitle.Show(0.3f);

            _onlinePvpButton.gameObject.SetActive(true);
            _vsComButton.gameObject.SetActive(true);

            
        }
    }

    private void OnStartDrag()
    {
        if (_isDebug) Debug.Log("On Start Drag Map");
        DOTween.Kill("MapMovement");
        _mapTitle.Hide(0.2f);

        _onlinePvpButton.gameObject.SetActive(false);
        _vsComButton.gameObject.SetActive(false);
    }

    private void OnReturn()
    {
        ReturnMap();
        _mapTitle.Show(0.3f);
        _onlinePvpButton.gameObject.SetActive(true);
        _vsComButton.gameObject.SetActive(true);
        //mapBackgroundManager.Return(RETURN_TIME);
    }

    //private void OnInitCharacter()
    //{
    //    CreateCharacter(_model.GetCharacterID());
    //}

    //private void CreateCharacter(int id)
    //{
    //    GameObject character = CharacterPrefabProvider.instance.Get(id);
    //    _mapCharacter = GameObject.Instantiate(character);
    //    _mapCharacter.transform.localPosition = new Vector3(_model.GetCurrentMenuIndex() * _mapThumbnailWidthf, 0.01f, 1f);
    //    _mapCharacter.transform.parent = mapGroundContainer.transform;
    //}

    private void OnPress()
    {
        if (_isDebug) Debug.Log("================= >>>>> press");
        _model.Press();
        UpdateDragPoint2D(mapGroundContainer.transform.localPosition);
    }

    private void OnDrag(Vector2 delta)
    {
        if (_model.isDragEnd) return;

        _model.Drag(delta);

        if (!_model.isMoving)
        {
            _dragPoint2D += delta/2;
            UpdateDragPoint3D(_dragPoint2D);

            //mapBackgroundManager.Drag(delta.x);

            Vector3 localPos = mapGroundContainer.transform.localPosition;
            localPos.x += delta.x;
            mapGroundContainer.transform.localPosition = localPos;
        }
    }
    
    private void OnRelease()
    {
        _model.Release();
    }

    private void OnOnlinePvpButtonClick(int entryFee)
    {
        if (_isDebug) Debug.Log("online pvp button click. current stage(map) index: " + _model.GetCurrentMenuIndex());
        int _resultCode = _entryService.SendEntry(_model.GetCurrentMenuIndex()).code;
        Debug.Log("???????????????????????????????????????????? online pvp: " + _resultCode);
        if(_resultCode == 0)
        {
            _onlinePvpButton.GetButton().Disable();
            _userService.Spend(entryFee);
            //string stageId = _entryService.GetStageIdByEntryIndex(_model.GetCurrentMenuIndex());
            _mainScene.MoveToRaceScene(RaceMode.mode.MULTI);
        }
        else if(_resultCode == 1)
        {
            if (_isDebug) Debug.Log("No Money!!!!!");
            _noEntryFeePopup.OpenPopup();
        }
        else if(_resultCode == 2)
        {
            if (_isDebug) Debug.Log("Your level is Low!!!!!");
            _limitLvPopup.gameObject.SetActive(true);
        }
    }

    private void OnVsComButtonClick(int entryFee)
    {
        int _resultCode = _entryService.SendEntry(_model.GetCurrentMenuIndex()).code;
        if (_isDebug) Debug.Log("-----> EntryFee: " + entryFee + " / resultCode: " + _resultCode + " / menu index: " + _model.GetCurrentMenuIndex());
        if (_resultCode == 0)
        {
            _vsComButton.GetButton().Disable();
            _userService.Spend(entryFee);
            _mainScene.MoveToRaceScene(RaceMode.mode.SINGLE);
        }
        else if (_resultCode == 1)
        {
            if (_isDebug) Debug.Log("No Money!!!!!");
            _noEntryFeePopup.OpenPopup();
        }
        else if (_resultCode == 2)
        {
            if (_isDebug) Debug.Log("Your level is Low!!!!!");
            _limitLvPopup.gameObject.SetActive(true);
        }
    }

    private bool IsRandomStage()
    {
        return (_model.GetCurrentMenuIndex() == 0) ? true : false;
    }

    private void MoveMap(int index, float time)
    {
        if (_isDebug) Debug.Log("+++++ Move Map: " + index);
        _model.isMoving = true;
        mapGroundContainer.transform.DOLocalMoveX(index * -_mapThumbnailWidth, time).SetEase(Ease.OutExpo).OnComplete(OnMoved);

        if (index > _entryService.GetEntryInfos().Count - 1) index = 0;
        if (index < 0) index = 0;
        GDEEntryItemData entryItemData = _entryService.GetEntryInfo(index);
        _onlinePvpButton.ChangeEntryInfo(entryItemData.entryFee, entryItemData.limitLv);
        _vsComButton.ChangeEntryInfo(entryItemData.entryFee, entryItemData.limitLv);
        if (_isDebug) Debug.Log("model menu index: " + _model.GetCurrentMenuIndex());
        _lobbyBg.Change(_model.GetCurrentMenuIndex());

        // //Debug.Log("INDEX: " + _model.GetCurrentMenuIndex());
        // if (_model.GetCurrentMenuIndex() == 0)
        // {
        //     //Debug.Log("STEP: 1");
        //     // _stageArrowManager.leftStageArrow.Off();
        // }
        // else if(_model.GetCurrentMenuIndex() == (_model.mapCount - 1))
        // {
        //     //Debug.Log("STEP: 2");
        //     _stageArrowManager.rightStageArrow.Off();
        // }
        // else
        // {
        //     //Debug.Log("STEP: 3");
        //     _stageArrowManager.leftStageArrow.On();
        //     _stageArrowManager.rightStageArrow.On();
        // }
    }

    //private void ReturnEnd(int index, float time)
    //{
        
    //}

    private void ReturnMap()
    {
        _model.isMoving = true;
        mapGroundContainer.transform.DOLocalMoveX(_model.GetCurrentMenuIndex() * -_mapThumbnailWidth, RETURN_TIME).SetEase(Ease.OutExpo).OnComplete(OnReturned);
    }

    private void OnMoved()
    {
        _mapTitle.ChangeTitle(_model.GetCurrentMenuIndex());
        _mapTitle.Show(0.3f);

        _onlinePvpButton.gameObject.SetActive(true);
        _vsComButton.gameObject.SetActive(true);

        UpdateDragPoint2D(mapGroundContainer.transform.localPosition);
        UpdateDragPoint3D(_dragPoint2D);
        _model.isMoving = false;

        _model.OnMapMoved();
    }

    private void OnReturned()
    {
        _mapTitle.ChangeTitle(_model.GetCurrentMenuIndex());
        _mapTitle.Show(0.3f);

        _onlinePvpButton.gameObject.SetActive(true);
        _vsComButton.gameObject.SetActive(true);

        UpdateDragPoint2D(mapGroundContainer.transform.localPosition);
        UpdateDragPoint3D(_dragPoint2D);
        _model.isMoving = false;

        _model.OnMapReturned();
    }

    private void UpdateDragPoint2D(Vector3 pos)
    {
        _dragPoint2D = camera.WorldToScreenPoint(pos);
    }

    private void UpdateDragPoint3D(Vector2 pos)
    {
        _dragPoint3D = camera.ScreenToWorldPoint(new Vector3(pos.x, 0, 6));
    }

    //private void MoveCharacter(float direction)
    //{
    //    _mapCharacter.GetComponent<Animator>().SetFloat("Direction", direction);
    //    _mapCharacter.GetComponent<Animator>().speed = 2f;
    //    DOTween.Kill("mapCharacterMovement");
    //    _mapCharacter.transform.DOLocalMoveX(_model.GetCurrentMenuIndex() * _mapThumbnailWidthf, CHARACTER_MOVEMENT_TIME).SetId("mapCharacterMovement").SetEase(Ease.OutSine).OnComplete(EndCharacterMovement);
    //    DOTween.To(() => _mapCharacter.GetComponent<Animator>().speed, x => _mapCharacter.GetComponent<Animator>().speed = x, 1, CHARACTER_MOVEMENT_TIME).SetEase(Ease.OutSine);
    //}

    //private void EndCharacterMovement()
    //{
    //    _mapCharacter.GetComponent<Animator>().SetFloat("Direction", 0);
    //}
}
