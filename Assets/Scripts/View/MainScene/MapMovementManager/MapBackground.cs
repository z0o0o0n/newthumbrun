﻿using UnityEngine;
using System.Collections;

public class MapBackground : MonoBehaviour 
{
    public float distanceScale = 100;
    public MapBackgroundElement[] mapBackgroundElementList;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void Drag(float deltaX)
    {
        for (int i = 0; i < mapBackgroundElementList.Length; i++)
        {
            mapBackgroundElementList[i].StopTween();
            Vector3 pos = mapBackgroundElementList[i].transform.localPosition;
            pos.x += deltaX / distanceScale;
            mapBackgroundElementList[i].transform.localPosition = pos;
        }
    }

    public void Return(float time)
    {
        for (int i = 0; i < mapBackgroundElementList.Length; i++)
        {
            mapBackgroundElementList[i].Retrun(time);
        }
    }

    public void Show(float time, int startDirection)
    {
        for(int i = 0; i < mapBackgroundElementList.Length; i++)
        {
            mapBackgroundElementList[i].Show(time, startDirection);
        }
    }

    public void Hide(float time, int targetDirection)
    {
        for (int i = 0; i < mapBackgroundElementList.Length; i++)
        {
            mapBackgroundElementList[i].Hide(time, targetDirection);
        }
    }
}
