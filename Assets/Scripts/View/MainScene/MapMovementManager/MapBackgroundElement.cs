﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MapBackgroundElement : MonoBehaviour
{
    public float hideDistanceX = 0;

    private float _startPosX = 0;

    void Awake()
    {
        _startPosX = transform.localPosition.x;
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void Retrun(float time)
    {
        DOTween.Kill("MapBackgroundElement_" + gameObject.GetInstanceID() + "_return");

        transform.DOLocalMoveX(_startPosX, time).SetId("MapBackgroundElement_" + gameObject.GetInstanceID() + "_return").SetEase(Ease.OutSine);
    }

    public void Show(float time, int startDirection)
    {
        Vector3 pos = transform.localPosition;
        pos.x = startDirection * (hideDistanceX/2);
        transform.localPosition = pos;

        float slowValue = (((transform.localPosition.z / 100) * 3f) / 2);
        if (time == 0) slowValue = 0;

        DOTween.Kill(this.name + "_MoveX");
        transform.DOLocalMoveX(_startPosX, time + slowValue).SetId(this.name + "_MoveX").SetEase(Ease.OutExpo);
    }

    public void Hide(float time, int targetDirection)
    {
        DOTween.Kill(this.name + "_MoveX");
        transform.DOLocalMoveX((hideDistanceX/2) * targetDirection, time).SetId(this.name + "_MoveX").SetEase(Ease.OutExpo);
    }

    public void StopTween()
    {
        DOTween.Kill(this.name + "_MoveX");
    }
}
