﻿using UnityEngine;
using System.Collections;
using Com.Mod.Util;

public class MapMovementModel : MonoBehaviour 
{
    public delegate void MapMovementEvent(bool isEnd);
    public delegate void MapMovementStateEvent();
    public event MapMovementEvent On_Prev;
    public event MapMovementEvent On_Next;
    public event MapMovementStateEvent On_Init;
    public event MapMovementStateEvent On_StartDrag;
    public event MapMovementStateEvent On_Return;
    public event MapMovementStateEvent On_InitCharacter;
    public event MapMovementStateEvent Moved;
    public event MapMovementStateEvent Returned;

    public int mapCount = 0;
    public bool isPress = false;
    public bool isMoving = false;
    public bool isDragEnd = false;

	private bool _isInit = false;
    private bool _isStartDrag = false;
    private int _currentMenuIndex = 0;
    private float _distanceForMovement = 300;
    private float _distance = 0.0f;
    private float _speedForMovement = 10;
    private int _characterID = 0;
    private SpeedChecker _speedChecker;

    void Awake()
    {
        _speedChecker = GetComponent<SpeedChecker>();
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    void FixedUpdate()
    {
        _speedChecker.UpdatePos(new Vector2(_distance, 0));
    }

    public void Init(int menuIndex)
    {
        //Debug.Log("+++++ Map Movement Model Init");
        _currentMenuIndex = menuIndex;
        _isInit = true;
        if (On_Init != null) On_Init();
    }

    public void Press()
    {
        isPress = true;
        _isStartDrag = false;
        _distance = 0.0f;
        _speedChecker.Reset();
    }

    public void Drag(Vector2 delta)
    {
        if (isDragEnd) return;
        if (!_isStartDrag)
        {
            _isStartDrag = true;
            if (On_StartDrag != null) On_StartDrag();
        }

        _distance += delta.x;
        if (isMoving) return;

        if (_distance < -_distanceForMovement)
        {
            if (mapCount-1 <= _currentMenuIndex)
            {
                isDragEnd = true;
                if (On_Next != null) On_Next(true);
                return;
            }
            _currentMenuIndex++;
            MoveMap(1);
        }
        
        if (_distance > _distanceForMovement)
        {
            if (0 >= _currentMenuIndex)
            {
                isDragEnd = true;
                if (On_Prev != null) On_Prev(true);
                return;
            }
            _currentMenuIndex--;
            MoveMap(-1);
        }
    }

    public void Release()
    {
        isPress = false;
        isDragEnd = false;

        if (isMoving) return;

        if (_speedChecker.GetSpeedX() < -_speedForMovement)
        {
            if (mapCount-1 <= _currentMenuIndex)
            {
                //if (On_Next != null) On_Next(true);
                return;
            }
            _currentMenuIndex++;
            MoveMap(1);
            return;
        }
        else if (_speedChecker.GetSpeedX() > _speedForMovement)
        {
            if (0 >= _currentMenuIndex)
            {
                //if (On_Prev != null) On_Prev(true);
                return;
            }
            _currentMenuIndex--;
            MoveMap(-1);
            return;
        }

        if (Mathf.Abs(_distance) < _distanceForMovement)
        {
            if(On_Return != null) On_Return();
        }
    }

    public void MovePrev()
    {
        if (!isMoving)
        {
            _currentMenuIndex--;
            MoveMap(-1);
        }
    }

    public void MoveNext()
    {
        if (!isMoving)
        {
            _currentMenuIndex++;
            MoveMap(1);
        }
    }

    private void MoveMap(int direction)
    {
        bool isEnd = false;
        if (_currentMenuIndex < 0)
        {
            _currentMenuIndex = 0;
            isEnd = true;
        }
        else if (_currentMenuIndex >= mapCount)
        {
            _currentMenuIndex = mapCount - 1;
            isEnd = true;
        }

        if (direction == 1)
        {
            if (On_Next != null) On_Next(isEnd);
        }
        else if (direction == -1)
        {
            if (On_Prev != null) On_Prev(isEnd);
        }
    }

    public int GetCurrentMenuIndex()
    {
        return _currentMenuIndex;
    }

    public void SetCharacterID(int id)
    {
        _characterID = id;
        if (On_InitCharacter != null) On_InitCharacter();
    }

    public int GetCharacterID()
    {
        return _characterID;
    }

    public void OnMapMoved()
    {
        if (Moved != null) Moved();
    }

    public void OnMapReturned()
    {
        if (Returned != null) Returned();
    }

	public bool isInit
	{
		get{ return _isInit;}
	}
}
