﻿using Com.Mod.ThumbRun.LuckyChance.Domain;
using Com.Mod.ThumbRun.Race.Domain;
using Com.Mod.ThumbRun.User.Application;
using Com.Mod.ThumbRun.User.Domain;
using DG.Tweening;
using GameDataEditor;
using Junhee.Utils;
using System;
using UnityEngine;

public class MatchingProfileItem : MonoBehaviour 
{
	public delegate void MatchingProfileItemEvent();
    public event MatchingProfileItemEvent InfoShowed;
    public event MatchingProfileItemEvent ResultShowed;
    public event MatchingProfileItemEvent LevelUp;
    public event MatchingProfileItemEvent ExpCountEnd;

    [SerializeField]
	private UISprite _bg;
	[SerializeField]
	private Color _bgDefaultColor;
	[SerializeField]
	private UISprite _darkLine;
	[SerializeField]
	private ProfileFirework _firework;
	[SerializeField]
	private UISprite _progress;
	[SerializeField]
	private UILabel _record;
	[SerializeField]
	private UISprite _character;
	[SerializeField]
	private UIWidget _goldStatusWidget;
	[SerializeField]
	private UILabel _goldLabel;
	[SerializeField]
	private UIWidget _lvStatusWidget;
	//[SerializeField]
	//private UIPanel _winnerBadgePanel;
	//[SerializeField]
	//private UISprite _winnerBadge;
	[SerializeField]
	private UILabel _rankLabel;
	[SerializeField]
	private UISprite _countryFlag;
	[SerializeField]
	private UIPanel _lvGaugePanel;
	[SerializeField]
	private UISprite _lvGaugeBg;
	[SerializeField]
	private UISprite _lvGauge;
	[SerializeField]
	private UILabel _lvLabel;
	[SerializeField]
	private UILabel _nameLabel;
	[SerializeField]
	private UISprite _nameEmptyBox;
	[SerializeField]
	private AudioSource _audioSource;
	[SerializeField]
	private AudioClip _acquiringAudio;
	[SerializeField]
	private AudioClip _levelUpAudio;
	[SerializeField]
	private AudioClip _participantEnterAudio;
    [SerializeField]
    private UserService _userService;
    [SerializeField]
    private UISprite _badgeColorBg;
    [SerializeField]
    private Color _goldColor;
    [SerializeField]
    private Color _silverColor;
    [SerializeField]
    private Color _bronzeColor;
    private string _userNickname = "JUNHEE";
	private int _prizeGold = 0;
	private float _rankMoveDuration = 0.5f;
	private float _goldStatusVisibleDuration = 1.5f;
    private float _expAcquirementDuration = 0.5f;
    private ParticipantData _testParticipantData;
	private ParticipantData _participantData;
	private RaceResult _result;

    void Start () 
	{
	}





	public void ShowProgress()
	{
		_bg.color = _bgDefaultColor;
		_darkLine.gameObject.SetActive(false);
		_firework.gameObject.SetActive(false);
		_progress.gameObject.SetActive(true);
		_progress.alpha = 0.5f;
		DOTween.Kill("progressAlpha." + GetInstanceID());
		DOTween.To(()=>_progress.alpha, x=>_progress.alpha=x, 0f, 0.2f).SetLoops(-1, LoopType.Yoyo).SetId("progressAlpha." + GetInstanceID());
		_record.gameObject.SetActive(false);
		_character.gameObject.SetActive(false);
		_goldStatusWidget.gameObject.SetActive(false);
		_lvStatusWidget.gameObject.SetActive(false);
		_badgeColorBg.gameObject.SetActive(false);
		_rankLabel.gameObject.SetActive(false);
		_countryFlag.spriteName = "Flag_000";
		_countryFlag.color = ColorUtil.ConvertHexToColor(0x9f9789);
		_lvGaugePanel.gameObject.SetActive(false);
		_lvLabel.gameObject.SetActive(false);
		_nameLabel.gameObject.SetActive(false);
		_nameEmptyBox.gameObject.SetActive(true);
		transform.DOLocalMoveY(0f, 0f);
	}

	public void ShowInfo(ParticipantData data)
	{
        _participantData = data;
        float delay = 0;
		if(!data.isUser) delay = UnityEngine.Random.Range(0.2f, 1f);
		DOVirtual.DelayedCall(delay, ShowDetailInfo);
	}

    private void ShowDetailInfo()
	{
        CharacterData characterData = CharacterDataManager.instance.GetCharacterDataByID(_participantData.characterId);

        _bg.color = characterData.bgColor;
        _darkLine.gameObject.SetActive(false);
		_firework.gameObject.SetActive(false);
		_progress.gameObject.SetActive(false);
		DOTween.Kill("progressAlpha." + GetInstanceID());
		_record.gameObject.SetActive(false);
		_character.gameObject.SetActive(true);
		_character.spriteName = "CharacterProfileImage_" + _participantData.characterId;
		_goldStatusWidget.gameObject.SetActive(false);
		_lvStatusWidget.gameObject.SetActive(false);
        _badgeColorBg.gameObject.SetActive(false);
		_rankLabel.gameObject.SetActive(false);
		_countryFlag.spriteName = "Flag_" + _participantData.countryCode;
		_countryFlag.color = ColorUtil.ConvertHexToColor(0xffffff);
		if(_participantData.isUser)
		{
            _lvGaugePanel.gameObject.SetActive(true);
            Vector3 pos = _lvLabel.transform.localPosition;
            pos.y = -223f;
            _lvLabel.transform.localPosition = pos;
            _lvLabel.text = "Lv." + _userService.GetLevel();

            float currentExpRate = (float)_userService.GetExp() / (float)_userService.GetGoalExp();
            float targetWidth = (64f * currentExpRate) + 10f;
            _lvGauge.width = (int)targetWidth;

            //Debug.Log("????????????????");
        }
		else
		{
			_lvGaugePanel.gameObject.SetActive(false);
            Vector3 pos = _lvLabel.transform.localPosition;
            pos.y = -238f;
            _lvLabel.transform.localPosition = pos;
            _lvLabel.text = "Lv." + _participantData.level.ToString();
        }
		_lvLabel.gameObject.SetActive(true);
		_nameLabel.text = _participantData.nickname;
		_nameLabel.gameObject.SetActive(true);
		_nameEmptyBox.gameObject.SetActive(false);

		transform.DOLocalMoveY(0f, 0f);

		_audioSource.clip = _participantEnterAudio;
		_audioSource.loop = false;
		_audioSource.Play();

		if(InfoShowed != null) InfoShowed();
	}

    // 게임 결과 모드로 출력
    public void ShowRaceResult(RaceResult result)
    {
        _result = result;
        int rank = _result.participantData.rank;

        _badgeColorBg.gameObject.SetActive(true);
        if (rank == 0) _badgeColorBg.color = _goldColor;
        else if (rank == 1) _badgeColorBg.color = _silverColor;
        else if (rank == 2) _badgeColorBg.color = _bronzeColor;
        else if (rank >= 3)
        {
            _rankLabel.transform.localPosition = new Vector2(-50f, -23f);
            _darkLine.gameObject.SetActive(true);
            _badgeColorBg.gameObject.SetActive(false);

        }

        _rankLabel.text = (rank + 1).ToString();
        _rankLabel.gameObject.SetActive(true);

        _record.text = ScoreFormat.ConvertString(_result.participantData.record);
        _record.gameObject.SetActive(true);

        MoveRank(rank);
    }

    private void MoveRank(int rank)
    {
        //transform.localPosition = new Vector2(0f, rank * -20f);
        DOTween.Kill("MoveRank." + GetInstanceID());
        float posY = (rank - 3) * -20f;
        transform.DOLocalMoveY(posY, _rankMoveDuration).SetDelay(0.2f).SetId("MoveRank." + GetInstanceID()).SetEase(Ease.OutCubic).OnComplete(OnRankMoved);
    }

    private void OnRankMoved()
	{
		if(ResultShowed != null) ResultShowed();
	}





	public void ShowPrizeWidget(int prizeGold)
    {
        _goldLabel.text = String.Format("{0:##,##}", prizeGold);
        _goldStatusWidget.gameObject.SetActive(true);
    }

    private int _currentExpValue = 0;
    public void CountExp(int exp)
    {
        _currentExpValue = _userService.GetExp();
        
        if (IsOverMaxExp(exp))
        {
            int remainingGoalExp = _userService.GetGoalExp() - _userService.GetExp(); // 목표경험치에 도달하기까지 남은 경험치
            int overExp = exp - remainingGoalExp; // 획득한 경험치 중 목표경험치를 넘어선 경험치
            float duraction = _expAcquirementDuration * remainingGoalExp / exp;
            DOTween.To(() => _currentExpValue, x => _currentExpValue = x, _userService.GetGoalExp(), duraction).SetEase(Ease.Linear).OnUpdate(OnExpCounting).OnComplete(() => OnLevelUp(exp, overExp));
        }
        else
        {
            _userService.AddExp(exp);
            DOTween.To(() => _currentExpValue, x => _currentExpValue = x, _userService.GetExp(), _expAcquirementDuration).SetEase(Ease.Linear).OnUpdate(OnExpCounting).OnComplete(OnExpCountEnd);
        }

        _audioSource.clip = _acquiringAudio;
        _audioSource.loop = true;
        _audioSource.Play();
    }

    private bool IsOverMaxExp(int exp)
    {
        return (_userService.GetExp() + exp >= _userService.GetGoalExp()) ? true : false;
    }

    private void OnExpCounting()
	{
        float currentExpRate = (float)_currentExpValue / (float)_userService.GetGoalExp();
		float targetWidth = (64f * currentExpRate) + 10f;
		_lvGauge.width = (int)targetWidth;
	}

    private void OnLevelUp(int acquiredExp, int overExp)
    {
        _currentExpValue = 0;
        _userService.ResetExp();
        _userService.AddExp(overExp);
        _userService.AddLevel(1);
        _userService.SetGoalExp(LvExpTable.instance.GetGoalExp(_userService.GetLevel()));
        _lvLabel.text = "Lv." + _userService.GetLevel().ToString();
        ShowLevelUpWidget();

        if (LevelUp != null) LevelUp();

        float duraction = _expAcquirementDuration * overExp / acquiredExp;
        DOTween.To(() => _currentExpValue, x => _currentExpValue = x, overExp, duraction).SetEase(Ease.Linear).OnUpdate(OnExpCounting).OnComplete(OnExpCountEnd);
    }

    private void OnExpCountEnd()
	{
		_audioSource.Stop();
        if (ExpCountEnd != null) ExpCountEnd();
    }
    
    private void ShowLevelUpWidget()
	{
		_lvStatusWidget.gameObject.SetActive(true);
		DOTween.Kill("ShowLevelStatusWidget." + GetInstanceID());
		DOVirtual.DelayedCall(1f, OnLevelUpWidgetHided).SetId("ShowLevelStatusWidget." + GetInstanceID());
	}

	private void OnLevelUpWidgetHided()
	{
		_lvStatusWidget.gameObject.SetActive(false);
	}





	private bool IsUser()
	{
		if(_participantData.isUser) return true;
		else return false;
	}

	private bool HasPrizeGold()
	{
		if(_result.reward.gold > 0) return true;
		else return false;
	}
}
