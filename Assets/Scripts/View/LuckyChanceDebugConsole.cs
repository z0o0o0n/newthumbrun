﻿namespace Com.Mod.ThumbRun.View
{
	using UnityEngine;
	using System.Collections;
	using Com.Mod.ThumbRun.Control;
	using UnityEngine.UI;

    public class LuckyChanceDebugConsole : MonoBehaviour
    {
        private LuckyChance _luckChance;
        [SerializeField]
		private Text _diameterText;
		[SerializeField]
		private Text _raceOutcomeText;
		[SerializeField]
		private Text _remainingRaceCountText;
		[SerializeField]
		private Text _standardGoldText;
		[SerializeField]
		private Text _isChanceActivatedText;
		[SerializeField]
		private Text _bonusGoldText;

        //public Text

        void Awake()
        {
            _luckChance = GameObject.FindWithTag("LuckyChance").GetComponent<LuckyChance>();
            _luckChance.OutcomeAdded += OnOutcomeAdded;
			_luckChance.ChanceActivated += OnChanceActivated;
        }

        void Start()
        {

        }

        void Update()
        {

        }

        void OnDestroy()
        {
			_luckChance.OutcomeAdded -= OnOutcomeAdded;
			_luckChance.ChanceActivated -= OnChanceActivated;
        }



        #region Private Functions
        private void OnOutcomeAdded()
        {
			_diameterText.text = "Diameter : x" + _luckChance.data.diameter;
			string outcomeLog = "Outcome : ";
			for(int i = 0; i < _luckChance.data.raceOutcomeList.Count; i++)
			{
				outcomeLog += _luckChance.data.raceOutcomeList[i].ToString();
			}
			_raceOutcomeText.text = outcomeLog;
			_remainingRaceCountText.text = "Remaining Race Count : " + _luckChance.data.remainingRaceCount;
			_standardGoldText.text = "Standard Gold : " + _luckChance.data.standardGold;
        }

		private void OnChanceActivated()
		{
			_isChanceActivatedText.text = "Is Chance Activated : " + _luckChance.data.isChanceActivated.ToString();
			_bonusGoldText.text = "Bonus Gold : " + _luckChance.data.bonusGold.ToString();
		}
        #endregion
    }
}