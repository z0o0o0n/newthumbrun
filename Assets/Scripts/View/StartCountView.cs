﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;
using DG.Tweening;

public class StartCountView : MonoBehaviour
{
    public StartCountDisplayer countDisplayer;

    [SerializeField]
    private StartCounter _startCounter;

    private bool _isDebug = false;
    private string _logPrefix = "Counter - ";

	void Awake()
	{
        _startCounter.CountStart += OnCountStart;
        _startCounter.Tick += OnTick;
        _startCounter.CountEnd += OnCountEnd;
	}

	void Start ()
	{

	}

    void OnDestroy()
    {
        _startCounter.CountStart -= OnCountStart;
        _startCounter.Tick -= OnTick;
        _startCounter.CountEnd -= OnCountEnd;
    }



    #region Private Functions
    private void Show(int index)
    {
        countDisplayer.Show(index);
    }

    private void OnCountStart()
    {
        //countDisplayer.gameObject.SetActive(true);
    }

    private void OnTick()
    {
        Show(_startCounter.GetCurrentCount());
    }

    private void OnCountEnd()
    {
        //countDisplayer.gameObject.SetActive(false);
    }
    #endregion



    #region Public Functions
    
    #endregion
}
