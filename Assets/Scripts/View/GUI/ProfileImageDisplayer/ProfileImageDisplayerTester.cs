﻿using UnityEngine;
using System.Collections;

public class ProfileImageDisplayerTester : MonoBehaviour 
{
    public ProfileImageDisplayer _profileImageDisplayer;
    public Texture _textureA;
    public Texture _textureB;

	void Start () 
	{
	
	}
	
	void Update () 
	{
        if (Input.GetKeyUp(KeyCode.S))
        {
            _profileImageDisplayer.Show(0.4f);
        }
        else if (Input.GetKeyUp(KeyCode.H))
        {
            _profileImageDisplayer.Hide(0.2f);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            _profileImageDisplayer.ChangeTexture(_textureA);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            _profileImageDisplayer.ChangeTexture(_textureB);
        }
	}
}
