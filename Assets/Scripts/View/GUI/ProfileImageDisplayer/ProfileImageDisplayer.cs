﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ProfileImageDisplayer : MonoBehaviour 
{
    public GameObject imagePanel;
    public UITexture imageTexture;
    public UISprite imageBg;

    void Awake()
    {
        Hide(0);
    }

	void Start () 
	{
        
	}
	
	void Update () 
	{

	}

    public void Show(float time)
    {
        //Debug.Log("Profile Image Displayer (" + this.GetInstanceID() + ") / Show()");
        DOTween.Kill("profileImagePanelTween" + this.GetInstanceID());
        DOTween.Kill("profileImageBgTween" + this.GetInstanceID());
        imagePanel.transform.DOScale(1, time).SetId("profileImagePanelTween" + this.GetInstanceID()).SetDelay(0.1f).SetEase(Ease.OutBack);
        imageBg.transform.DOScale(1, time).SetId("profileImageBgTween" + this.GetInstanceID()).SetEase(Ease.OutBack);
    }

    public void Hide(float time)
    {
        //Debug.Log("Profile Image Displayer (" + this.GetInstanceID() + ") / Hide()");
        DOTween.Kill("profileImagePanelTween" + this.GetInstanceID());
        DOTween.Kill("profileImageBgTween" + this.GetInstanceID());
        
        if (time == 0)
        {
            imagePanel.transform.localScale = Vector3.zero;
            imageBg.transform.localScale = Vector3.zero;
        }
        else
        {
            imagePanel.transform.DOScale(0, time).SetId("profileImagePanelTween" + this.GetInstanceID()).SetEase(Ease.InBack);
            imageBg.transform.DOScale(0, time).SetId("profileImageBgTween" + this.GetInstanceID()).SetDelay(0.05f).SetEase(Ease.InBack);
        }
    }

    public void ChangeTexture(Texture texture)
    {
        imageTexture.mainTexture = texture;
        if (texture != null)
        {
            if (texture.height == 76) imageTexture.mainTexture = (Texture)Resources.Load("ProfilePhoto_Basic");
            else imageTexture.mainTexture = texture;
        }
        else imageTexture.mainTexture = (Texture)Resources.Load("ProfilePhoto_Basic");
    }
}
