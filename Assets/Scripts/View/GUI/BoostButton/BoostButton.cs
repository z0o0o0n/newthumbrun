﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Controller;
using DG.Tweening;
using Junhee.Utils;

namespace Com.Mod.ThumbRun.GUI
{
    public class BoostButton : MonoBehaviour
    {
        [SerializeField]
        private Booster _booster;
        [SerializeField]
        private UISprite _onButton;
        [SerializeField]
        private UISprite _offButton;
        [SerializeField]
        private UISprite _gauge;

        void Awake()
        {
            transform.DOLocalMoveY(80f, 0f);

            _booster.Activated += OnBoosterActivated;
            _booster.Deactivated += OnBoosterDeactivated;
            _booster.BoostStarted += OnStarted;
            _booster.BoostProgressing += OnProgressing;
            _booster.BoostEnded += OnEnded;
            _booster.Emptied += OnEmptied;
            _booster.Reset += OnBoosterReset;
        }

        void Start()
        {
            Hide();
            DeactivateButton();
        }

        void Update()
        {
            if(Input.GetKeyUp(KeyCode.Space))
            {
                _booster.StartBoost();
            }
            // else if (Input.GetKeyUp(KeyCode.Alpha2))
            // {
            //     _booster.ResetBooster();
            // }
        }

        void OnDestroy()
        {
            _booster.Activated -= OnBoosterActivated;
            _booster.Deactivated -= OnBoosterDeactivated;
            _booster.BoostStarted -= OnStarted;
            _booster.BoostProgressing -= OnProgressing;
            _booster.BoostEnded -= OnEnded;
            _booster.Emptied -= OnEmptied;
            _booster.Reset -= OnBoosterReset;
        }



        #region Private Functions
        private void OnStarted()
        {
        }

        private void OnProgressing(float progress)
        {
        }

        private void OnEnded()
        {
            if (_booster.useableQuantity > 0) ActivateButton();
        }

        private void OnEmptied()
        {
            DeactivateButton();
        }

        private void OnBoosterReset()
        {
        }

        private void OnBoosterActivated()
        {
            Show();
            ActivateButton();
        }

        private void OnBoosterDeactivated()
        {
            Hide();
            DeactivateButton();
        }
        #endregion



        #region Public Functions
        public void OnPressBoostBtn()
        {
            Boost();
        }

        public void Show()
        {
            DOTween.Kill("boostButton." + GetInstanceID());
            transform.DOScale(new Vector3(1, 1, 1), 0.5f).SetId("boostButton." + GetInstanceID()).SetEase(Ease.OutBack);
        }

        public void Hide(float time = 0.15f)
        {
            DOTween.Kill("boostButton." + GetInstanceID());
            transform.DOScale(new Vector3(0, 0, 1), time).SetId("boostButton." + GetInstanceID()).SetEase(Ease.InBack).SetUpdate(true);
        }
        #endregion



        #region Methods
        private void Boost()
        {
            if (!_booster.isBoosting)
            {
                _booster.StartBoost();
                DeactivateButton();
            }
        }

        private void ActivateButton()
        {
            _onButton.gameObject.SetActive(true);
            _offButton.gameObject.SetActive(false);
            _gauge.transform.DOLocalMoveY(16, 0);
            _gauge.color = ColorUtil.ConvertHexToColor(0xB55757);
        }

        private void DeactivateButton()
        {
            _onButton.gameObject.SetActive(false);
            _offButton.gameObject.SetActive(true);
            _gauge.transform.DOLocalMoveY(-5, 0);
            _gauge.color = ColorUtil.ConvertHexToColor(0xaaaaaa);
        }
        #endregion
    }
}
