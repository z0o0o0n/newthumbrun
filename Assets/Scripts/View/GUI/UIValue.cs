﻿using UnityEngine;
using System.Collections;

public class UIValue : MonoBehaviour 
{
    public UILabel nameLabel;
    public UILabel valueLabel;
    public UIButton minusButton;
    public UIButton plusButton;
    public float defaultValue;
    public float offset = 0.0f;
    private float _value = 0.0f;

    void Awake()
    {
        _value = defaultValue;
        UpdateValueLabel();
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void OnPressMinus()
    {
        _value -= offset;
        UpdateValueLabel();
    }

    public void OnPressPlus()
    {
        Debug.Log(_value + " / " + offset);
        _value += offset;
        UpdateValueLabel();
    }

    private void UpdateValueLabel()
    {
        valueLabel.text = System.Math.Round(_value, 2).ToString();
    }

    public float GetValue()
    {
        return _value;
    }

    public void SetDefaultValue(float defaultValue)
    {
        _value = defaultValue;
        UpdateValueLabel();
    }
}
