﻿using UnityEngine;
using System.Collections;

public class UIBg : MonoBehaviour
{
	void Start ()
    {
        UITexture uiBackgroundTexture = GetComponent<UITexture>();
        uiBackgroundTexture.enabled = true;
        uiBackgroundTexture.width = Screen.width;
        uiBackgroundTexture.height = Screen.height;
	}
}
