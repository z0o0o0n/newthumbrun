﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LanguageMediator : MonoBehaviour 
{
    public List<GameObject> enList;
    public List<GameObject> krList;
    public List<GameObject> jpList;

	void Start () 
	{
        if (GameManager.phoneLanguage == SystemLanguage.Korean)
        {
            if (krList.Count == 0) ShowEN();
            else ShowKR();
        }
        else if (GameManager.phoneLanguage == SystemLanguage.Japanese)
        {
            if (jpList.Count == 0) ShowEN();
            else ShowJP();
        }
        else
        {
            ShowEN();
        }
	}

    private void ShowEN()
    {
        for(int i = 0; i < enList.Count; i++)
        {
            enList[i].SetActive(true);
        }
    }

    private void ShowKR()
    {
        for (int i = 0; i < krList.Count; i++)
        {
            krList[i].SetActive(true);
        }
    }

    private void ShowJP()
    {
        for (int i = 0; i < jpList.Count; i++)
        {
            jpList[i].SetActive(true);
        }
    }
	
	void Update () 
	{
	
	}
}
