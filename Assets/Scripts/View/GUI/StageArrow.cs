﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class StageArrow : MonoBehaviour 
{
    public float targetPosX = 0.0f;

	void Start () 
	{
        transform.DOLocalMoveX(targetPosX, 0.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
	}
	
	void Update () 
	{
	
	}

    void OnClick()
    {
    }

    public void On()
    {
        transform.localScale = Vector3.one;
    }

    public void Off()
    {
        transform.localScale = Vector3.zero;
    }
}
