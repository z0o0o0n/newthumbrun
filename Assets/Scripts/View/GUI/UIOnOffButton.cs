﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Setting.Application;

public class UIOnOffButton : MonoBehaviour 
{
    public delegate void OnOffButtonEventHandler();
    public event OnOffButtonEventHandler On;
    public event OnOffButtonEventHandler Off;

    [SerializeField]
    private UIBasicButton _buttonOn;
    [SerializeField]
    private UIBasicButton _buttonOff;

    void Awake ()
    {
        _buttonOn.gameObject.SetActive(true);
        _buttonOff.gameObject.SetActive(false);

        _buttonOn.On_Release += OnReleaseOnButton;
        _buttonOff.On_Release += OnReleaseOffButton;
    }

	void Start () 
	{
	    
	}
	
	void Update () 
	{
	
	}

    void OnDestroy()
    {
        _buttonOn.On_Release -= OnReleaseOnButton;
        _buttonOff.On_Release -= OnReleaseOffButton;
    }





    private void OnReleaseOnButton()
    {
        ChangeOff();
        if(Off != null) Off();
    }

    private void OnReleaseOffButton()
    {
        ChangeOn();
        if(On != null) On();
    }





    public void ChangeOn()
    {
        _buttonOn.gameObject.SetActive(true);
        _buttonOff.gameObject.SetActive(false);
    }

    public void ChangeOff()
    {
        _buttonOn.gameObject.SetActive(false);
        _buttonOff.gameObject.SetActive(true);
    }
}
