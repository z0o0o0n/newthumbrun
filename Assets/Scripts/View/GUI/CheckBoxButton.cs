﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckBoxButton : MonoBehaviour
{
    public delegate void CheckBoxEvent(bool isChecked);
    public event CheckBoxEvent Changed;

    [SerializeField]
    private UILabel _label;
    [SerializeField]
    private UIButton _button;
    [SerializeField]
    private UISprite _checkBoxBg;
    [SerializeField]
    private UISprite _checkMark;
    private bool _isChecked = false;

    void Start ()
    {
        _label.onChange += OnLabelChanged;
    }
	
	void Update ()
    {
		
	}

    void OnDestroy()
    {
        _label.onChange -= OnLabelChanged;
    }





    public void OnButtonClick()
    {
        if(_isChecked)
        {
            _isChecked = false;
            _checkMark.gameObject.SetActive(false);
        }
        else
        {
            _isChecked = true;
            _checkMark.gameObject.SetActive(true);
        }

        if (Changed != null) Changed(_isChecked);
    }

    private void OnLabelChanged()
    {
        Vector3 bgPos = _checkBoxBg.transform.localPosition;
        bgPos.x = (-_label.width / 2) - 10f;
        _checkBoxBg.transform.localPosition = bgPos;

        Vector3 markPos = _checkMark.transform.localPosition;
        markPos.x = (-_label.width / 2) - 10f;
        _checkMark.transform.localPosition = markPos;

        Vector3 labelPos = _label.transform.localPosition;
        labelPos.x = 20f;
        _label.transform.localPosition = labelPos;
    }
}
