﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class HomeButton : MonoBehaviour 
{
    public delegate void BasicButtonEvent();
    public event BasicButtonEvent On_Click;

    private float defaultY;
    private bool _isActivated = false;

    void Awake()
    {
        defaultY = transform.localPosition.y;
        Activate();
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}

    public void Show(float time)
    {
        transform.DOLocalMoveY(defaultY, time).SetEase(Ease.OutCubic);
        Activate();
    }

    public void Hide(float time)
    {
        transform.DOLocalMoveY(defaultY-100f, time).SetEase(Ease.InCubic);
        Deactivate();
    }

    private void Activate()
    {
        _isActivated = true;
    }

    private void Deactivate()
    {
        _isActivated = false;
    }

    public void OnClick()
    {
        if (_isActivated) if (On_Click != null) On_Click();
    }
}
