﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Junhee.Utils;

public class LoadingShutter : MonoBehaviour 
{
    public delegate void LoadingShutterEvent();
    public event LoadingShutterEvent Initialized;
    public event LoadingShutterEvent Opened;
    public event LoadingShutterEvent Closed;

    public bool isInitialized = false;
	public UILabel _loadingLabel;
	public ShutterBg _shutterBg;
    public TipAndStoryDataEditor _tipAndStoryDataEditor;

    private bool _isDebug = false;
    private GameObject _currentTipPrefab;
    private int _currentTipIndex = 0;
	
    void Awake()
    {
        if (_isDebug) Debug.Log("LoadingShutter - Awake()");
        _shutterBg.Initialized += OnShutterBgInitialized;
    }

    private void OnShutterBgInitialized()
    {
        if (_isDebug) Debug.Log("LoadingShutter - OnShutterBgInitialized()");
        _shutterBg.Initialized -= OnShutterBgInitialized;

        isInitialized = true;
        if (Initialized != null) Initialized();
    }

	void Start ()
	{
        _currentTipIndex = GameData.instance.GetConfigData().TipIndex;
        if (_currentTipIndex >= (_tipAndStoryDataEditor.tipPrefabs.Length))
        {
            _currentTipIndex = 0;
            GameData.instance.GetConfigData().TipIndex = 0;
        }
        transform.localPosition = new Vector3(0, UIScreen.instance.GetHeight(), 0.0f);

        _loadingLabel.transform.localPosition = new Vector3(0, - ((UIScreen.instance.GetHeight() / 2) - 65f), 0.0f);
    }

    public void SetBgColor(Color color, Color loadingColor)
    {
        _shutterBg.ChangeColor(color);
        _loadingLabel.color = loadingColor;
    }


    // Close
	public void Close(float time = 0.5f)
	{
        if (_isDebug) Debug.Log("LoadingShutter - Close()");
        ChangeTip();
        transform.DOLocalMove(new Vector3(0, 0, 0.0f), time).SetId("shutterBg").SetEase(Ease.OutCubic).OnComplete(OnCompleteClose).SetUpdate(true);
	}

    private void ChangeTip()
    {
        if (_isDebug) Debug.Log("LoadingShutter - ChangeTip()");
        SetTipPrefab(_currentTipIndex);
        _currentTipIndex++;
        if (_currentTipIndex >= _tipAndStoryDataEditor.tipPrefabs.Length) _currentTipIndex = 0;
        GameData.instance.GetConfigData().TipIndex = _currentTipIndex;
    }

    private void SetTipPrefab(int tipIndex)
    {
        if (_isDebug) Debug.Log("LoadingShutter - SetTipPrefab() / tipIndex: " + tipIndex);
        if (_currentTipPrefab)
        {
            Destroy(_currentTipPrefab);
        }
        _currentTipPrefab = (GameObject)Instantiate(_tipAndStoryDataEditor.tipPrefabs[tipIndex]);
        _currentTipPrefab.layer = 13;
        foreach (Transform child in _currentTipPrefab.transform)
        {
            //if (_isDebug) Debug.Log("======= :" + child.name);
            child.gameObject.layer = 13;   
        }
        _currentTipPrefab.transform.parent = gameObject.transform;
        _currentTipPrefab.transform.localPosition = Vector3.zero;
        _currentTipPrefab.transform.localScale = Vector3.one;

        TipPage tipPage = _currentTipPrefab.GetComponent<TipPage>();

        SetBgColor(tipPage.bgColor, tipPage.pointColor);
    }

	private void OnCompleteClose()
	{
        if (_isDebug) Debug.Log("LoadingShutter - OnCompleteClose()");
        Time.timeScale = 0;
        if (Closed != null) Closed();
	}
	

    // Open
	public void Open(float time = 0.5f)
	{
        if (_isDebug) Debug.Log("LoadingShutter - Open()");
        Time.timeScale = 1;
        transform.DOLocalMove(new Vector3(0, UIScreen.instance.GetHeight(), 0.0f), time).SetId("shutterBg").SetDelay(1f).SetEase(Ease.InQuint).OnComplete(OnCompleteOpen).SetUpdate(true);
	}

    private void OnCompleteOpen()
    {
        if (_isDebug) Debug.Log("LoadingShutter - OnCompleteOpen()");
        if (Opened != null) Opened();
    }
}
