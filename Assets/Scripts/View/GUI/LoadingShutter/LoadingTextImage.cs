﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LoadingTextImage : MonoBehaviour
{
    public UISprite _uiSprite;

    private const float MOTION_SPEED = 0.2f;
    private const float BOTTOM_SPACE = 40f;

    void Awake ()
    {
        InitPosition();
    }

    private void InitPosition()
    {
        float ScreenHightEnd = -UIScreen.instance.GetHeight() / 2f;
        transform.localPosition = new Vector3(0f, ScreenHightEnd + BOTTOM_SPACE, 0f);
    }

    public void PlayLoadingMotion()
    {
        DOTween.To(() => _uiSprite.alpha, x => _uiSprite.alpha = x, 1f, MOTION_SPEED).SetId("loadingImageMotion").SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutCubic).SetAutoKill(true).SetUpdate(true);
    }

    public void ChangeColor(Color color)
    {
        _uiSprite.color = color;
    }

    void OnDestroy()
    {
        DOTween.Kill("loadingImageMotion");
    }
}
