﻿using UnityEngine;
using System.Collections;

public class ShutterBg : MonoBehaviour 
{
    public delegate void ShutterBgEvent();
    public event ShutterBgEvent Initialized;

    public UISprite _uiSprite;

    void Awake()
    {
        
    }

    private void Start()
    {
        InitSize();
    }

    private void Update()
    {

    }

    private void InitSize()
    {
        //_uiSprite.width = UIScreen.instance.GetWidth();
        //_uiSprite.height = UIScreen.instance.GetHeight();

        if (Initialized != null) Initialized();
    }

    public void ChangeColor(Color color)
    {
        _uiSprite.color = color;
    }
}
