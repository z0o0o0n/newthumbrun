﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class StartPointView : MonoBehaviour, IStartPointView
{
    public StartPoint startPoint;
    public GameObject effect;
    public AudioSource audioSource;
    public AudioClip sound;

    void Awake ()
    {
        //GetComponent<Renderer>().material.color = Color.red;

        startPoint.StartPointActivate += Activate;
        startPoint.EffectShow += OnEffectShow;
        startPoint.StartPointReset += Reset;

        effect.transform.DOScaleX(0, 0);
        effect.transform.DOLocalMoveY(0.2f, 0f);
    }

	void Start ()
    {
	}

	void Update ()
    {
	
	}

    private void OnEffectShow()
    {
        DOTween.Kill("StartPointView_Effect_Scale_0");
        DOTween.Kill("StartPointView_Effect_Move_0");
        DOTween.Kill("StartPointView_Effect_Scale_1");
        DOTween.Kill("StartPointView_Effect_Move_1");

        effect.transform.DOScaleX(0.5f, 0.3f).SetEase(Ease.OutCubic).SetId("StartPointView_Effect_Scale_0_" + gameObject.GetInstanceID());
        effect.transform.DOLocalMoveY(0.157f, 0.3f).SetEase(Ease.OutCubic).SetId("StartPointView_Effect_Move_0" + gameObject.GetInstanceID());

        effect.transform.DOScaleX(0, 0).SetDelay(3).SetId("StartPointView_Effect_Scale_1");
        effect.transform.DOLocalMoveY(0.2f, 0f).SetDelay(3).SetId("StartPointView_Effect_Move_1");
    }

    public void Activate()
    {
        audioSource.clip = sound;
        audioSource.Play();
    }

    public void Reset()
    {
        //DOTween.Kill("StartPointView_Effect_Scale_0");
        //DOTween.Kill("StartPointView_Effect_Move_0");
        //DOTween.Kill("StartPointView_Effect_Scale_1");
        //DOTween.Kill("StartPointView_Effect_Move_1");

        //effect.transform.DOScaleX(0, 0).SetId("StartPointView_Effect_Scale_1");
        //effect.transform.DOLocalMoveY(0.2f, 0f).SetId("StartPointView_Effect_Move_1");

        //effect.transform.DOScaleX(0, 0);
        //effect.transform.DOLocalMoveY(0.2f, 0f);
    }

    void OnDestroy()
    {
        //DOTween.Kill("StartPointMoveY");
    }
}
