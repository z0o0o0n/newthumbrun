﻿using UnityEngine;
using System.Collections;

public class StageArrowManager : MonoBehaviour 
{
    public UIBasicButton leftArrowButton;
    public UIBasicButton rightArrowButton;
    public StageArrow leftStageArrow;
    public StageArrow rightStageArrow;
    public MapMovementModel mapMovementModel;

    void Awake ()
    {
        leftArrowButton.On_Click += OnLeftArrowClick;
        rightArrowButton.On_Click += OnRightArrowClick;
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    void OnDestroy()
    {
        leftArrowButton.On_Click -= OnLeftArrowClick;
        rightArrowButton.On_Click -= OnRightArrowClick;
    }

    public void SetActivation(bool value)
    {
        leftArrowButton.SetButtonActive(value);
        rightArrowButton.SetButtonActive(value);
    }

    public void Show()
    {
        leftArrowButton.gameObject.SetActive(true);
        rightArrowButton.gameObject.SetActive(true);
    }

    public void Hide()
    {
        leftArrowButton.gameObject.SetActive(false);
        rightArrowButton.gameObject.SetActive(false);
    }

    private void OnLeftArrowClick()
    {
        mapMovementModel.MovePrev();
    }

    private void OnRightArrowClick()
    {
        mapMovementModel.MoveNext();
    }
}
