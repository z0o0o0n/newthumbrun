﻿using UnityEngine;
using System.Collections;

public class AbilityRewardDisplay : MonoBehaviour 
{
    public UISprite icon;
    public UISprite plus;
    public UILabel valueLabel;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void SetIcon(string statusType)
    {
        icon.spriteName = statusType.ToString() + "Icon";
    }

    public void SetValue(float value)
    {
        valueLabel.text = value + "%";
    }

    public void Dim()
    {
        icon.alpha = 0.2f;
        plus.alpha = 0.2f;
        valueLabel.alpha = 0.2f;
    }

    public void Distinct()
    {
        icon.alpha = 1f;
        plus.alpha = 1f;
        valueLabel.alpha = 1f;
    }
}
