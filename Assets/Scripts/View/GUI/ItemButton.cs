﻿using UnityEngine;
using System.Collections;

public class ItemButton : MonoBehaviour 
{
    [SerializeField]
    private ItemManager _itemManager;
    [SerializeField]
    private UISprite _buttonBG;
    [SerializeField]
    private UISprite _buttonSide;
    [SerializeField]
    private GameObject _buttonArea;
    [SerializeField]
    private UISprite _itemIcon;
    [SerializeField]
    private Color[] _bgColors;
    [SerializeField]
    private Color[] _sideColors;

    void Awake ()
    {
        // transform.localPosition = new Vector3(0f, -250f, 0f);
        UIEventListener.Get(_buttonArea).onClick += OnClick;
        _itemManager.ItemGot += OnItemGot;
        _itemManager.ItemDestroyed += OnItemDestroyed;
        Hide();
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {
        if(Input.GetKeyUp(KeyCode.Space))
        {
            _itemManager.UseItem();
            Hide();
        }
    }

    void OnDestroy()
    {
        UIEventListener.Get(_buttonArea).onClick -= OnClick;
        _itemManager.ItemGot -= OnItemGot;
        _itemManager.ItemDestroyed -= OnItemDestroyed;
    }



    #region Private Functions
    private void OnClick(GameObject sender)
    {
        _itemManager.UseItem();
        Hide();
    }

    private void OnItemGot(int itemID)
    {
        ChangeIcon(itemID);
        Show();
    }

    private void OnItemDestroyed(int itemID)
    {
        Hide();
    }

    private void ChangeIcon(int itemID)
    {
        _itemIcon.spriteName = "ItemButton_" + itemID;
        _buttonBG.color = _bgColors[itemID];
        _buttonSide.color = _sideColors[itemID];
    }
    #endregion



    #region Public Functions
    public void Show()
    {
        if(_itemManager.HasItem()) gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void PressButton()
    {
        _itemManager.UseItem();
        Hide();
    }
    #endregion
}
