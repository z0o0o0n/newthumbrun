﻿using UnityEngine;
using System.Collections;

public class UIButtonSound : MonoBehaviour 
{
    public UIButton uiButton;
    public AudioSource audioSource;
    public AudioClip pressSound;
    public AudioClip releaseSound;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

	void Start () 
	{
	
	}

    void OnPress(bool isDown)
    {
        if (uiButton.enabled)
        {
            if (isDown)
            {
                if (audioSource)
                {
                    if (pressSound)
                    {
                        audioSource.clip = pressSound;
                        audioSource.Play();
                    }
                }
            }
            else
            {
                if (audioSource)
                {
                    if (releaseSound)
                    {
                        audioSource.clip = releaseSound;
                        audioSource.Play();
                    }
                }
            }
        }
    }
}
