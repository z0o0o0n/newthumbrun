﻿namespace view.mapScene
{
    using UnityEngine;
    using System.Collections;

    public class TouchArea : MonoBehaviour
    {
        public delegate void TouchAreaDragEvent(Vector2 delta);
        public delegate void TouchAreaEvent();
        public event TouchAreaDragEvent On_Drag;
        public event TouchAreaEvent On_Press;
        public event TouchAreaEvent On_Release;
        public event TouchAreaEvent On_Click;

        public float currentDragDeltaX = 0.0f;
        public float prevDragDeltaX = 0.0f;

        private bool _isDebug = false;
        private bool _isPress = false;
        private bool _isActivation = true;
        private float _dragDistanceX = 0.0f;

        void Start()
        {
            Input.simulateMouseWithTouches = false;
        }

        void Update()
        {
            if (_isPress)
            {
                //Debug.Log("Touch Count: " + Input.touchCount);
                //if(Input.touchCount > 0)
                //{
                //    Debug.Log(Input.touches[0].position + " : " + Input.touches[0].rawPosition);
                //}
            }
        }

        void OnPress(bool isPress)
        {
            _isPress = isPress;
            if (_isPress)
            {
                if(_isDebug) Debug.Log("OnPress");
                if (On_Press != null) On_Press();
            }
            else
            {
                if (_isDebug) Debug.Log("OnRelease");
                if(On_Release != null) On_Release();
            }
        }

        void OnDrag(Vector2 delta)
        {
            //Debug.Log("OnDrag");
            //Debug.Log(UICamera.currentTouch.pos.ToString());

			//Debug.Log ("UIScreen.instance.GetWidth(): " + UIScreen.instance.GetWidth () + " / " + Screen.width);
            if (currentDragDeltaX == 0.0f) currentDragDeltaX = delta.x + (Screen.width / 2);
            else
            {
                currentDragDeltaX = delta.x + (Screen.width / 2);
                _dragDistanceX = currentDragDeltaX - prevDragDeltaX;
                if (_isDebug) Debug.Log("distanceX: " + _dragDistanceX);
            }

            prevDragDeltaX = currentDragDeltaX;

			if(On_Drag != null) On_Drag(delta);
        }

        public void OnClick()
        {
            if (_isDebug) Debug.Log("OnClick");
            if (On_Click != null) On_Click();
        }

        public float GetDragDistanceX()
        {
            return _dragDistanceX;
        }

        public void ActivateTouch()
        {
            gameObject.SetActive(true);
        }

        public void DeactivateTouch()
        {
            gameObject.SetActive(false);
        }
    }
}