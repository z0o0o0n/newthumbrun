﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MiniatureSkyCity : MonoBehaviour 
{
    public GameObject skyBike;
    public GameObject waterCreator1;
    public GameObject waterCreator2;
    public ColliderButton button;
    public Flag flag;

    private float _currentRotation = 0.0f;
    private Vector3[] skyBikePath = new Vector3[8];


    void Awake()
    {
        button.On_ButtonUp += OnButtonUp;
    }

    private void OnButtonUp()
    {

    }

	void Start () 
	{
        float waterCreator1TargetY = waterCreator1.transform.localPosition.y - 0.1f;
        waterCreator1.transform.DOLocalMoveY(waterCreator1TargetY, 1).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);

        float waterCreator2TargetY = waterCreator2.transform.localPosition.y - 0.1f;
        waterCreator2.transform.DOLocalMoveY(waterCreator2TargetY, 1.2f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);

        
	}
	
	void Update () 
	{
        //_currentRotation += 0.1f;

        //transform.rotation = Quaternion.Euler(new Vector3(0, _currentRotation, 0));
	}

    
}
