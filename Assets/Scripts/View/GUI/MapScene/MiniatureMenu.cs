﻿using UnityEngine;
using System.Collections;
using view.mapScene;
using DG.Tweening;

public class MiniatureMenu : MonoBehaviour 
{
    public delegate void MiniatureMenuEvent(int currentIndex);
    public event MiniatureMenuEvent On_Changing;
    public event MiniatureMenuEvent On_Changed;
    public event MiniatureMenuEvent On_Click;

    public TouchArea touchArea;
    public AnimationCurve ease;

    private Vector2 _dragPoint = Vector2.zero;
    private Vector3 _worldDragPoint = Vector3.zero;
    private bool _isPress = false;

    private Vector2 _dragDistance = Vector2.zero;
    private int _currentIndex = 0;
    

    void Awake()
    {
        touchArea.On_Press += OnPress;
        touchArea.On_Release += OnRelease;
        touchArea.On_Drag += OnDrag;
        touchArea.On_Click += OnClick;
    }

    private void OnPress()
    {
        //if (On_Changing != null) On_Changing(_currentIndex);

        DOTween.Kill("move");

        _dragDistance = Vector2.zero;
        
        _isPress = true;
        _dragPoint = Camera.main.WorldToScreenPoint(transform.localPosition);
    }

    private void OnRelease()
    {
        //Debug.Log("_dragDistance: " + _dragDistance.ToString());

        if(Mathf.Abs(_dragDistance.x) > 50)
        {
            float xpos = transform.localPosition.x;
            float targetXpos = 0.0f;
            float distance = 0.0f;
            float time = 0.0f;

            if(_dragDistance.x > 0)
            {
                if (_currentIndex == 0)
                {
                    ReturnPos();
                    return;
                }
                targetXpos = ((_currentIndex-1) * -6) - 2;
                distance = Mathf.Abs(targetXpos - xpos);
                time = distance / 20;
                DOTween.Kill("move");
                transform.DOLocalMoveX(targetXpos, time).SetId("move").SetEase(ease).OnUpdate(OnMoveUpdate).OnComplete(OnPrevMoveComplete);
            }
            else if(_dragDistance.x < 0)
            {
                if (_currentIndex == 2)
                {
                    ReturnPos();
                    return;
                }
                targetXpos = ((_currentIndex+1) * -6) - 2;
                distance = Mathf.Abs(targetXpos - xpos);
                time = distance / 20;
                DOTween.Kill("move");
                transform.DOLocalMoveX(targetXpos, time).SetId("move").SetEase(ease).OnUpdate(OnMoveUpdate).OnComplete(OnNextMoveComplete);
            }
            else
            {
                ReturnPos();
                Debug.Log("1");
            }
        }
        else
        {
            ReturnPos();
            //Debug.Log("2");
        }

        _isPress = false;
    }

    private void OnPrevMoveComplete()
    {
        _currentIndex--;
        if (On_Changed != null) On_Changed(_currentIndex);
    }

    private void OnNextMoveComplete()
    {
        _currentIndex++;
        if (On_Changed != null) On_Changed(_currentIndex);
    }

    private void ReturnPos()
    {
        float xpos = transform.localPosition.x;
        //float targetXpos = Mathf.Ceil(xpos / 6) * 6 + -2;
        float targetXpos = (_currentIndex * -6) - 2;
        //Debug.Log("targetXpos: " + targetXpos);

        //if (targetXpos > -2) targetXpos = -2;
        //else if (targetXpos < -10) targetXpos = -10;

        float distance = Mathf.Abs(targetXpos - xpos);
        float time = distance / 10 + 0.2f;
        transform.DOLocalMoveX(targetXpos, time).SetId("move").SetEase(Ease.OutCubic).OnUpdate(OnMoveUpdate).OnComplete(OnReturnComplete);
    }

    private void OnMoveUpdate()
    {
        _worldDragPoint = transform.localPosition;
    }

    private void OnReturnComplete()
    {
        if (On_Changed != null) On_Changed(_currentIndex);
    }

    private void OnDrag(Vector2 delta)
    {
        _dragDistance += delta;
        _dragPoint += delta;
        _worldDragPoint = Camera.main.ScreenToWorldPoint(_dragPoint);

        if(Mathf.Abs(_dragDistance.x) > 50) if (On_Changing != null) On_Changing(_currentIndex);
    }

    private void OnClick()
    {
        if (On_Click != null) On_Click(_currentIndex);
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
        if(_isPress)
        {
            _worldDragPoint = Camera.main.ScreenToWorldPoint(_dragPoint);
            Vector3 localPos = transform.localPosition;
            localPos.x += (_worldDragPoint.x - localPos.x) * 0.1f;
            transform.localPosition = localPos;
        }
	}
}
