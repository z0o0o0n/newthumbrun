﻿namespace view.mapScene.ui
{
    using UnityEngine;
    using System.Collections;
    using DG.Tweening;
using System.Collections.Generic;
    using Junhee.Utils;

    public class MapSceneUI : MonoBehaviour
    {
        public MiniatureMenu miniatureMenu;

        private Color[] colorList = {Color.black, Color.red, Color.green};

        void Awake()
        {
        }

        void Start()
        {
            miniatureMenu.On_Changing += OnMiniatureMenuChanging;
            miniatureMenu.On_Changed += OnMiniatureMenuChanged;
            miniatureMenu.On_Click += OnMiniatureMenuClick;

            //Vector2 uiPoint2D = Camera.main.WorldToScreenPoint(miniatureMenu.uiPoint.transform.position);
            //uiPoint2D.x -= Screen.width / 2;
            //uiPoint2D.y -= Screen.height / 2;
            //stageTitle.transform.localPosition = uiPoint2D;
        }

        private void OnMiniatureMenuChanging(int currentIndex)
        {
            
        }

        private void OnMiniatureMenuChanged(int currentIndex)
        {
            
        }

        private void OnMiniatureMenuClick(int currentIndex)
        {

        }

        void Update()
        {

        }
    }
}