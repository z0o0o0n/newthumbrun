﻿using UnityEngine;
using System.Collections;

public class StageTitle : MonoBehaviour 
{
    public UISprite bg;

    private UILabel _titleLabel;

    void Awake()
    {
        _titleLabel = GetComponent<UILabel>();
    }

	void Start () 
	{
	    
	}
	
	void Update () 
	{
	    
	}

    public void Init(string title, Color color)
    {
        ChangeTitle(title, color);
    }

    private void ResizeBG()
    {
        bg.width = _titleLabel.width + 60;
    }

    public void ChangeTitle(string title, Color color)
    {
        _titleLabel.text = title;
        bg.color = color;
        ResizeBG();
    }
}
