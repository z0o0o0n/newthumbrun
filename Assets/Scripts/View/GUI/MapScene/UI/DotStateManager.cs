﻿namespace view.mapScene.ui
{
    using UnityEngine;
    using System.Collections;
    using DG.Tweening;

    public class DotStateManager : MonoBehaviour
    {
        private UISprite _uiSprite;

        void Awake()
        {
            _uiSprite = GetComponent<UISprite>();
        }

        void Start()
        {

        }

        void Update()
        {

        }

        public void Activate()
        {
            transform.DOScale(1f, 0.75f).SetEase(Ease.OutElastic);
            _uiSprite.alpha = 1;
        }

        public void Deactivate()
        {
            transform.DOScale(0.5f, 0.75f).SetEase(Ease.OutElastic);
            _uiSprite.alpha = 0.5f;
        }
    }
}
