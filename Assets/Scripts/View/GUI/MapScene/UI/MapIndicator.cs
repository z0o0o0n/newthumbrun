﻿namespace view.mapScene.ui
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;

    public class MapIndicator : MonoBehaviour
    {
        public GameObject dotPrefab;

        private List<DotStateManager> dotStateManagerList;

        void Awake()
        {
            dotStateManagerList = new List<DotStateManager>();
        }

        void Start()
        {

        }

        void Update()
        {

        }

        public void Init(int currentIndex, int totalCount)
        {
            CreateDots(totalCount);
            ChangeDot(currentIndex);
            Replace();
        }

        private void CreateDots(int totalCount)
        {
            for(int i = 0; i < totalCount; i++)
            {
                GameObject dotInstance = GameObject.Instantiate(dotPrefab);
                dotInstance.transform.parent = transform;
                dotInstance.transform.localPosition = new Vector3(30*i, 0, 0);
                dotInstance.transform.localScale = Vector3.one;

                DotStateManager dotStateManager = dotInstance.GetComponent<DotStateManager>();
                dotStateManagerList.Add(dotStateManager);
            }
        }

        public void ChangeDot(int index)
        {
            for(int i = 0; i < dotStateManagerList.Count; i++)
            {
                if(i == index)
                {
                    dotStateManagerList[i].Activate();
                }
                else
                {
                    dotStateManagerList[i].Deactivate();
                }
            }
        }

        private void Replace()
        {
            Vector3 pos = transform.localPosition;
            pos.x = -(30 * dotStateManagerList.Count) / 2;
            transform.localPosition = pos;
        }

        void OnDestory()
        {
            if (dotStateManagerList != null)
            {
                dotStateManagerList.Clear();
                dotStateManagerList = null;
            }
        }
    }
}