﻿namespace view.mapScene
{
    using UnityEngine;
    using System.Collections;
    using view.mapScene.ui;

    public class MapScene : MonoBehaviour 
    {
        public MapSceneUI ui;

        private GameManager _gameManager;
        private LoadingShutter _loadingShutter;

        void Awake()
        {
            _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
            GameObject loadingPageGameObject = GameObject.FindGameObjectWithTag("LoadingPage");
            if (loadingPageGameObject != null)
            {
                //_loadingShutter = loadingPageGameObject.GetComponent<LoadingShutter>();
                //_loadingShutter.On_LoadComplete += OnLoadComplete;
            }
        }

	    void Start () 
        {
            ui.miniatureMenu.On_Click += OnMiniatureMenuClick;
	    }

        //public void OnPressTempButton()
        //{
        //    _loadingPage.Load(SceneName.MAIN);
        //}

        //private void OnButtonUp()
        //{
        //    _loadingPage.Load(SceneName.MAIN);
        //}

        private void OnLoadComplete()
        {
            //mainSceneUI.SetButtonActive(true);
        }

        private void OnMiniatureMenuClick(int currentIndex)
        {
        }
	
	    void Update () 
	    {
	
	    }
    }
}