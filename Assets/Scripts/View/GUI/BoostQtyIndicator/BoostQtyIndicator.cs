﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Controller;
using System.Collections.Generic;
using DG.Tweening;

namespace Com.Mod.ThumbRun.GUI
{
    public class BoostQtyIndicator : MonoBehaviour
    {
        [SerializeField]
        private Booster _booster;
        [SerializeField]
        private GameObject _iconPrefab;
        [SerializeField]
        private UISprite _bgSprite;
        private int _leftMargin = 10;
        private int _spaceX = 10;
        private int _iconWidth;
        private List<BoostQtyIndicatorIcon> iconList = new List<BoostQtyIndicatorIcon>();

        void Awake()
        {
            transform.DOLocalMoveY(75f, 0f);

            _booster.Activated += OnBoosterActivated;
            _booster.Deactivated += OnBoosterDeactivated;
            _booster.Init += OnBoosterInit;
            _booster.BoostStarted += OnBoostStarted;
            _booster.Reset += OnBoosterReset;
        }

        void Start()
        {
            
        }

        void Update()
        {
            // if(Input.GetKey(KeyCode.Alpha1))
            // {
            //     Show();
            // }
            // else if (Input.GetKey(KeyCode.Alpha1))
            // {
            //     Hide();
            // }
        }

        void OnDestroy()
        {
            _booster.Activated -= OnBoosterActivated;
            _booster.Deactivated -= OnBoosterDeactivated;
            _booster.Init -= OnBoosterInit;
            _booster.BoostStarted -= OnBoostStarted;
            _booster.Reset -= OnBoosterReset;
        }



        #region Private Functions
        private void OnBoosterInit()
        {
            CreateIcons();
            ResizeBg(0f);
            Hide(0f);
        }

        private void CreateIcons()
        {
            for (int i = 0; i < _booster.useableQuantity; i++)
            {
                GameObject instance = GameObject.Instantiate(_iconPrefab);

                UISprite uiSprite = instance.GetComponent<UISprite>();
                float startPosX = uiSprite.localSize.x / 2f + _leftMargin;

                instance.transform.parent = transform;
                instance.transform.localScale = Vector2.one;
                instance.transform.localPosition = new Vector2(((uiSprite.localSize.x + _spaceX) * i) + startPosX, 0f);

                BoostQtyIndicatorIcon icon = instance.GetComponent<BoostQtyIndicatorIcon>();
                iconList.Add(icon);

                if (i == 0) _iconWidth = uiSprite.width;
            }
        }

        private void ResizeBg(float time)
        {
            int boostQty = iconList.Count;
            int leftRightMargin = _leftMargin * 2;
            int width = (boostQty * _iconWidth) + ((boostQty - 1) * _spaceX) + leftRightMargin;
            DOTween.To(() => _bgSprite.width, x => _bgSprite.width = x, width, time);
        }

        private void OnBoostStarted()
        {
            RemoveIcon();
        }

        private void OnBoosterReset()
        {
            
        }

        private void OnBoosterActivated()
        {
            Show();
        }

        private void OnBoosterDeactivated()
        {
            Hide();
        }
        #endregion



        #region Methods
        private void Show()
        {
            float time = 0.5f;
            DOTween.To(() => _bgSprite.alpha, x => _bgSprite.alpha = x, 0.3f, time).SetUpdate(true);

            for(int i = 0; i < _booster.useableQuantity; i++)
            {
                UISprite sprite = iconList[i].gameObject.GetComponent<UISprite>();
                sprite.transform.DOScale(1f, time).SetEase(CustomEase.instance.OutBangElastic).SetUpdate(true);
            }
        }

        private void Hide(float time = 0.15f)
        {
            DOTween.To(() => _bgSprite.alpha, x => _bgSprite.alpha = x, 0f, time).SetUpdate(true);

            for (int i = 0; i < iconList.Count; i++)
            {
                UISprite sprite = iconList[i].gameObject.GetComponent<UISprite>();
                sprite.transform.DOScale(0f, time).SetEase(Ease.InBack).SetUpdate(true);
            }
        }

        private void RemoveIcon()
        {
            int index = _booster.useableQuantity;
            UISprite sprite = iconList[index].gameObject.GetComponent<UISprite>();
            sprite.transform.DOScale(0f, 0.2f).SetEase(Ease.InBack);
        }
        #endregion
    }
}
