﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CheckPointView : MonoBehaviour, ICheckPointView
{
    public CheckPoint checkPoint;
    public GameObject effect;
    public AudioSource audioSource;
    public AudioClip sound;
	
    void Awake()
    {
        //GetComponent<Renderer>().material.color = Color.red;

        Reset();

        checkPoint.CheckPointActivate += Activate;
        checkPoint.CheckPointReset += Reset; 
    }

	void Start ()
    {    
	}
	
	void Update () 
    {
	
	}

    public void Activate()
    {
        //transform.DOLocalMoveY(0.3f, 0.3f);

        DOTween.Kill("CheckPointView_Effect_Scale_" + gameObject.GetInstanceID());
        DOTween.Kill("CheckPointView_Effect_Move_" + gameObject.GetInstanceID());

        audioSource.clip = sound;
        audioSource.Play();

        effect.transform.DOScaleX(0.5f, 0.3f).SetId("CheckPointView_Effect_Scale_" + gameObject.GetInstanceID()).SetEase(Ease.OutCubic);
        effect.transform.DOLocalMoveY(0.157f, 0.3f).SetId("CheckPointView_Effect_Move_" + gameObject.GetInstanceID()).SetEase(Ease.OutCubic);
    }

    public void Reset()
    {
        DOTween.Kill("CheckPointView_Effect_Scale_" + gameObject.GetInstanceID());
        DOTween.Kill("CheckPointView_Effect_Move_" + gameObject.GetInstanceID());

        effect.transform.DOScaleX(0, 0).SetId("CheckPointView_Effect_Scale_" + gameObject.GetInstanceID());
        effect.transform.DOLocalMoveY(0.2f, 0f).SetId("CheckPointView_Effect_Move_" + gameObject.GetInstanceID());
        //transform.DOLocalMoveY(-0.3f, 0);
    }
}
