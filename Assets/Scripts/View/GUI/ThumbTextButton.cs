﻿using UnityEngine;
using System.Collections;

public class ThumbTextButton : MonoBehaviour 
{
    public GameObject text;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    void OnPress (bool isDown)
    {
        if(isDown)
        {
            Vector2 pos = text.transform.localPosition;
            pos.y -= 6;
            text.transform.localPosition = pos;
        }
        else
        {
            Vector2 pos = text.transform.localPosition;
            pos.y += 6;
            text.transform.localPosition = pos;
        }
    }
}
