﻿using UnityEngine;
using System.Collections;

public class ProfileThumb : MonoBehaviour 
{
    public UISprite thumb;
    public UISprite bg;

	void Start () 
	{
	}
	
	void Update () 
	{
	
	}

    public void Init(int characterID, Color bgColor)
    {
        thumb.spriteName = "CharacterProfileThumb_" + characterID;
        bg.color = bgColor;
    }
}
