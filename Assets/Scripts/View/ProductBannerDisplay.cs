﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GameDataEditor;
using DG.Tweening;
using Com.Mod.ThumbRun.ProductBanner.Application;

namespace Com.Mod.ThumbRun.View
{
	public class ProductBannerDisplay : MonoBehaviour 
	{
		[SerializeField]
		private ProductBannerService _productBannerService;
		[SerializeField]
		private List<UIWidget> _bannerList;
		[SerializeField]
		private List<Color> _bannerBgColorList;
		[SerializeField]
		private float _changeTime = 3;
		[SerializeField]
		private float _moveDistance = 100f;
		[SerializeField]
		private List<UILabel> _remainingTimeLabelList;
		[SerializeField]
		private AnimationCurve _customElastic;
		private int _prevIndex = 0;
		private int _currentIndex = 0;
		private bool _isDebug = true;

        public int currentIndex
        {
            get { return _currentIndex; }
        }



        void Awake ()
		{
            transform.localPosition = new Vector2(-100f, -150f);
            _productBannerService.Prepared += OnPrepared;
			Reset();
		}

		void Start () 
		{
		
		}
		
		void Update () 
		{
			//for(int i = 0; i < _remainingTimeLabelList.Count; i++)
			//{
			//	if(_remainingTimeLabelList[i] != null)
			//	{
			//		_remainingTimeLabelList[i].text = _productBannerService.GetRemainingTime(i);
			//	}
			//}

			//List<GDEProductBannerItemData> bannerItemDataList = _productBannerService.GetBannerItemDataList();
			//for(int i = 0; i < bannerItemDataList.Count; i++)
			//{
			//	if(bannerItemDataList[i].isShow) 
			//	{
			//		_bannerList[i].alpha = 1f;
			//		if(_remainingTimeLabelList[i]) _remainingTimeLabelList[i].gameObject.SetActive(true);
			//	}
			//	else if(!bannerItemDataList[i].isShow) 
			//	{
			//		_bannerList[i].alpha = 0.3f;
			//		if(_remainingTimeLabelList[i]) _remainingTimeLabelList[i].gameObject.SetActive(false);
			//	}
			//}
		}

		void OnDestroy()
		{
            _productBannerService.Prepared -= OnPrepared;
			DOTween.Kill("BannerChangeTween." + GetInstanceID());
		}





        private void OnPrepared()
        {
            int startIndex = UnityEngine.Random.Range(0, _bannerList.Count);
			_currentIndex = GetActivationIndex(startIndex); // index에 해당하는 배너가 활성상태인지 확인
			
			ShowBanner(_currentIndex, true);
			DOVirtual.DelayedCall(_changeTime, OnBannerChange).SetId("BannerChangeTween." + GetInstanceID());
        }

		private int GetActivationIndex(int targetIndex)
		{
			// if(_isDebug) Debug.Log("===== target index: " + targetIndex);
			//List<GDEProductBannerItemData> bannerItemDataList = _productBannerService.GetBannerItemDataList();

			int index = targetIndex;
			//for(int i = 0; i < bannerItemDataList.Count; i++)
			//{
			//	// if(_isDebug) Debug.Log("===== useable index: " + index);
			//	if(index >= bannerItemDataList.Count) index = 0;
			//	if(bannerItemDataList[index].isShow) break;
			//	index++;
			//}

			return index;
		}

		private void ShowBanner(int targetIndex, bool isDefault = false)
		{
			if(isDefault)
			{
				_bannerList[targetIndex].transform.localPosition = Vector3.zero;
			}
			else
			{
				_bannerList[_prevIndex].transform.DOLocalMoveX(200f, 0.5f).SetEase(_customElastic);
				
				_bannerList[targetIndex].transform.localPosition = new Vector3(200f, 0f, 0f);
				_bannerList[targetIndex].transform.DOLocalMoveX(0, 0.5f).SetEase(_customElastic);

				//DOTween.To(()=> _bannerBg.color, x=> _bannerBg.color =x, _bannerBgColorList[targetIndex], 0.3f);
			}
		}
		
		private void OnBannerChange()
		{
			int useableIndex = GetActivationIndex(_currentIndex + 1);
			if(useableIndex != _currentIndex)
			{
				_prevIndex = _currentIndex;
				_currentIndex = useableIndex;
				ShowBanner(_currentIndex);
			}
			DOVirtual.DelayedCall(_changeTime, OnBannerChange).SetId("BannerChangeTween." + GetInstanceID());
		}

		private void Reset()
		{
			for(int i = 0; i < _bannerList.Count; i++)
			{
				_bannerList[i].transform.localPosition = new Vector3(200f, 0f, 0f);
			}
		}
    }
}