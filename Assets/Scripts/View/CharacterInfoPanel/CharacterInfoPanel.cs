﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CharacterInfoPanel : MonoBehaviour 
{
    public delegate void CharacterInfoPanelEvent();
    public event CharacterInfoPanelEvent On_PressOKButton;
    public delegate void UseButtonEvent(int id);
    public event UseButtonEvent On_PressUseButton;

    public int panelWidth = 320;
    public ProfileInfoArea profileInfoArea;
    public ProfileImageArea profileImageArea;
    public HeroStatusArea heroStatusArea;
    public AbilityDisplayerContainer abilityDisplayerContainer;
    public ItemStatusArea itemStatusArea;
    public UseButtonArea useButtonArea;
    public OKButtonArea okButtonArea;
    public PanelBg _background;
    public int currentID { get { return _currentID; } }

    private int _currentID;
    private IStatusArea _currentStatusArea;

    void Awake ()
    {
    }

	void Start () 
    {
	}

    void Update () 
    {

	}




    public void SetData(CharacterData characterData, CollectionData collectionData, ButtonAreaType.type buttonAreaType, bool inUse = false)
    {
        _currentID = characterData.id;
        profileInfoArea.UpdateData(characterData.name, characterData.type, collectionData.name, collectionData.isCompleted);
        profileImageArea.UpdateData(characterData.id, characterData.meshType, characterData.color, characterData.bgColor, characterData.isCollected);
        UpdateStatusAreaData(characterData);
        SetButtonArea(buttonAreaType, inUse);

        _background.ChangeColor(characterData.bgColor);

        //heroStatusArea.SetRunValue(characterData.statusData.run, 0.1f);
        //heroStatusArea.SetBoostRunValue(characterData.statusData.boostRun, 0.15f);
        //heroStatusArea.SetJumpValue(characterData.statusData.jump, 0.1f);
        //heroStatusArea.SetBoostJumpValue(characterData.statusData.boostJump, 0.3f);
        //heroStatusArea.SetBoostChargingValue(characterData.statusData.boostCharging, 0.1f);
        //heroStatusArea.SetBoostConsumptionValue(characterData.statusData.boostConsumption, 0.3f);
    }

    private void UpdateStatusAreaData(CharacterData characterData)
    {
        if (characterData.kind == CharacterKind.kind.Hero)
        {
            //itemStatusArea.gameObject.SetActive(false);

            abilityDisplayerContainer.Activate();
            //abilityDisplayerContainer.SetData(characterData.basicAbilityStepData, BonusAbilityModel.instance.GetAbilityStepData());

            //heroStatusArea.UpdateData(characterData.basicAbilityStepData);
            //heroStatusArea.UpdateAdditionStatus(characterData.GetRewardedStatusData());
            //heroStatusArea.gameObject.SetActive(true);
        }
        else if (characterData.kind == CharacterKind.kind.Item)
        {
            //heroStatusArea.gameObject.SetActive(false);

            abilityDisplayerContainer.Deactivate();
            //itemStatusArea.UpdateData(characterData.statusData);
            //itemStatusArea.gameObject.SetActive(true);
        }
    }

    private void SetButtonArea(ButtonAreaType.type type, bool inUse)
    {
        if(type == ButtonAreaType.type.InUseState)
        {
            DeactivateAllButtonArea();
            useButtonArea.gameObject.SetActive(true);
            useButtonArea.SetUse(inUse);
            useButtonArea.On_PressUseButton += OnPressUseButton;
        }
        else if(type == ButtonAreaType.type.OKButton)
        {
            DeactivateAllButtonArea();
            okButtonArea.gameObject.SetActive(true);
            okButtonArea.On_PressOKButton += OnPressOKButton;
        }
        else if(type == ButtonAreaType.type.LockState)
        {
            DeactivateAllButtonArea();
            useButtonArea.gameObject.SetActive(true);
            useButtonArea.ChangeLockStatus();
        }
        else if(type == ButtonAreaType.type.None)
        {
            DeactivateAllButtonArea();
            useButtonArea.gameObject.SetActive(true);
            useButtonArea.ChangeDeactivationStatus();
        }
    }

    private void DeactivateAllButtonArea()
    {
        useButtonArea.gameObject.SetActive(false);
        useButtonArea.On_PressUseButton -= OnPressUseButton;
        okButtonArea.gameObject.SetActive(false);
        okButtonArea.On_PressOKButton -= OnPressOKButton;
    }

    private void OnPressUseButton()
    {
        if (On_PressUseButton != null) On_PressUseButton(_currentID);
    }

    private void OnPressOKButton()
    {
        //Hide();
        if (On_PressOKButton != null) On_PressOKButton();
    }

    public void Show(float time = 0.5f)
    {
        transform.DOLocalMoveX(0, time).SetEase(Ease.InOutCubic);
    }

    public void Hide(float time = 0.3f)
    {
        transform.DOLocalMoveX(panelWidth + 25, time).SetEase(Ease.InCubic);
    }

    public float GetWidth()
    {
        return panelWidth;
    }
}
