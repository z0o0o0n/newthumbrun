﻿using UnityEngine;
using System.Collections;

public class OKButtonArea : MonoBehaviour 
{
    public delegate void OKButtonAreaEvent();
    public event OKButtonAreaEvent On_PressOKButton;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void OnPressOKButton()
    {
        Debug.Log("ok버튼 눌렸다");
        if (On_PressOKButton != null) On_PressOKButton();
    }
}
