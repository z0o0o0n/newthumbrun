﻿using UnityEngine;
using System.Collections;

public class UseButtonArea : MonoBehaviour 
{
    public delegate void UseButtonAreaEvent();
    public event UseButtonAreaEvent On_PressUseButton;

    public UIBasicButton useButton;
    public UISprite inUseButton;
    public UISprite deactivationBg;
    public UISprite lockIcon;
    //public AudioSource audioSource;

    public void SetUse(bool inUse)
    {
        if(inUse)
        {
            useButton.gameObject.SetActive(false);
            inUseButton.gameObject.SetActive(true);
            deactivationBg.gameObject.SetActive(false);
            lockIcon.gameObject.SetActive(false);
        }
        else
        {
            useButton.On_Click += OnUseButtonClick;
            useButton.gameObject.SetActive(true);
            inUseButton.gameObject.SetActive(false);
            deactivationBg.gameObject.SetActive(false);
            lockIcon.gameObject.SetActive(false);
        }
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    private void OnUseButtonClick()
    {
        SetUse(true);
        if (On_PressUseButton != null) On_PressUseButton();
    }

    public void ChangeLockStatus()
    {
        useButton.gameObject.SetActive(false);
        inUseButton.gameObject.SetActive(false);
        deactivationBg.gameObject.SetActive(true);
        lockIcon.gameObject.SetActive(true);
    }

    public void ChangeDeactivationStatus()
    {
        useButton.gameObject.SetActive(false);
        inUseButton.gameObject.SetActive(false);
        deactivationBg.gameObject.SetActive(true);
        lockIcon.gameObject.SetActive(false);
    }

    void OnDestroy()
    {
        useButton.On_Click -= OnUseButtonClick;
    }
}
