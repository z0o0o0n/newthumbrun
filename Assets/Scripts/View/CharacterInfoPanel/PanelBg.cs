﻿using UnityEngine;
using System.Collections;

public class PanelBg : MonoBehaviour 
{
    public UISprite _shadow;

    private UISprite _sprite;

    void Awake ()
    {
        _sprite = GetComponent<UISprite>();
        _sprite.height = 640;

        _shadow.height = 640 - 30;
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}

    public void ChangeColor(Color color)
    {
        _sprite.color = color;
    }
}
