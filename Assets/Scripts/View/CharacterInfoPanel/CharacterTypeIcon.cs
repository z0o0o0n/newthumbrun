﻿using UnityEngine;
using System.Collections;

public class CharacterTypeIcon : MonoBehaviour 
{
    private UISprite _sprite;

    void Awake()
    {
        _sprite = GetComponent<UISprite>();
    }

	void Start () 
    {
	    
	}
	
	void Update () 
    {
	
	}





    // Change Type
    public void ChangeType(CharacterType.type type)
    {
        _sprite.spriteName = "CharacterTypeIcon_" + type.ToString();
    }
}
