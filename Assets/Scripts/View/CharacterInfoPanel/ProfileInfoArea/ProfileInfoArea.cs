﻿using UnityEngine;
using System.Collections;

public class ProfileInfoArea : MonoBehaviour 
{
    public UILabel characterNameLabel;
    public CharacterTypeIcon typeIcon;
    public UILabel collectionNameLabel;
    public UISprite completeIcon;

    private bool _isCompletedCollection = false;

    public void UpdateData(string characterName, CharacterType.type type, string collectionName, bool isCompletedCollection)
    {
        SetCharacterName(characterName);
        SetCharacterType(type);
        SetCollectionName(collectionName);
        _isCompletedCollection = isCompletedCollection;
        completeIcon.gameObject.SetActive(false);
    }

    private void SetCharacterName(string name)
    {
        characterNameLabel.text = name;
    }

    private void SetCharacterType(CharacterType.type type)
    {
        typeIcon.ChangeType(type);
    }

    private void SetCollectionName(string name)
    {
        collectionNameLabel.text = name;
        //UpdateCompleteIconPos();
    }

    private void UpdateCompleteIconPos()
    {
        Vector3 pos = completeIcon.transform.localPosition;
        pos.x = -(collectionNameLabel.width + 30 + 20);
        completeIcon.transform.localPosition = pos;
    }

	void Start () 
    {
	
	}
}
