﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;

public class ProfileImageArea : MonoBehaviour 
{
    public UISprite profileImage;
    public UISprite dimProfileImage;
    public UISprite bg;

    public void UpdateData(int id, string meshType, Color color, Color bgColor, bool isCollected)
    {
        SetProfileImage(id);
        SetDimProfileImage(meshType, color);
        SetBgColor(bgColor);
        if (isCollected)
        {
            profileImage.gameObject.SetActive(true);
            dimProfileImage.gameObject.SetActive(false);
        }
        else
        {
            profileImage.gameObject.SetActive(false);
            dimProfileImage.gameObject.SetActive(true);
        }
    }

    private void SetProfileImage(int id)
    {
        profileImage.spriteName = "CharacterProfileImage_" + id;
    }

    private void SetDimProfileImage(string meshType, Color color)
    {
        dimProfileImage.spriteName = meshType.ToString();
        dimProfileImage.color = color;
    }

    private void SetColor(Color color)
    {

    }

    private void SetBgColor(Color color)
    {
        bg.color = color;
    }

    void Start()
    {

    }
}
