﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;

public class AbilityDisplayer : MonoBehaviour 
{
    public CharacterAbilityType.type type;
    public UISprite icon;
    public AbilityGauge2 basicGauge;
    public AbilityGauge2 bonusGauge;
    public Color32 deactivationColor;

	void Start () 
	{
        //ChageBasicStep(3);
        //ChageBonusValue(7);
	}
	
	void Update () 
	{
	
	}

    public void ChageBasicStep(int step)
    {
        basicGauge.SetStep(step);
    }

    public void ChageBonusValue(int step)
    {
        bonusGauge.SetStep(step);
    }

    public int GetBasicStep()
    {
        return basicGauge.GetStep();
    }

    public int GetBonusStep()
    {
        return bonusGauge.GetStep();
    }

    public void Deactivate()
    {
        //icon.color = ColorUtil.ConvertHexToColor(deactivationColor);
        basicGauge.SetStep(0);
        bonusGauge.SetStep(0);
    }

    public void Activate()
    {
        icon.color = ColorUtil.ConvertHexToColor(0xffffff);
    }
}
