﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeroStatusArea : MonoBehaviour, IStatusArea
{
    //public StatusGauge[] specGaugeList;
    public StatusGauge runGauge;
    public StatusGauge boostRunGauge;
    public StatusGauge jumpGauge;
    public StatusGauge boostJumpGauge;
    public StatusGauge boostChargingGauge;
    public StatusGauge boostConsumptionGauge;

	void Start () 
    {
	}
	
    public void UpdateData(AbilityStepData statusData)
    {
        //CharacterStatusData convertedStatusData = CharacterStatusUitil.ConvertStatusDataByGauge(statusData);

        //runGauge.SetBasic(convertedStatusData.run);
        //boostRunGauge.SetBasic(convertedStatusData.boostRun);
        //jumpGauge.SetBasic(convertedStatusData.jump);
        //boostJumpGauge.SetBasic(convertedStatusData.boostJump);
        //boostChargingGauge.SetBasic(convertedStatusData.boostCharging);
        //boostConsumptionGauge.SetBasic(convertedStatusData.boostConsumption);
    }

    public void UpdateAdditionStatus(CharacterStatusData statusData)
    {
        CharacterStatusData convertedStatusData = CharacterStatusUitil.ConvertStatusDataByGauge(statusData);

        runGauge.SetAddition(convertedStatusData.run);
        boostRunGauge.SetAddition(convertedStatusData.boostRun);
        jumpGauge.SetAddition(convertedStatusData.jump);
        boostJumpGauge.SetAddition(convertedStatusData.boostJump);
        //boostChargingGauge.SetAddition(convertedStatusData.boostCharging);
        //boostConsumptionGauge.SetAddition(convertedStatusData.boostConsumption);
    }
}
