﻿using UnityEngine;
using System.Collections;

public class AbilityDisplayerContainer : MonoBehaviour 
{
    public AbilityDisplayer run;
    public AbilityDisplayer jump;
    public AbilityDisplayer boostAbility;
    //public AbilityDisplayer boostCharging;
    //public AbilityDisplayer boostConsumption;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void SetData(AbilityStepData basicAbilityStepData, AbilityStepData bonusAbilityStepData)
    {
        Debug.Log(basicAbilityStepData.ToString());
        Debug.Log(bonusAbilityStepData.ToString());
        Debug.Log(basicAbilityStepData.run);
        run.ChageBasicStep(basicAbilityStepData.run);
        jump.ChageBasicStep(basicAbilityStepData.jump);
        boostAbility.ChageBasicStep(basicAbilityStepData.boostAbility);
        //boostCharging.ChageBasicStep(basicAbilityStepData.boostCharging);
        //boostConsumption.ChageBasicStep(basicAbilityStepData.boostConsumption);

        run.bonusGauge.SetStep(basicAbilityStepData.run + bonusAbilityStepData.run);
        jump.bonusGauge.SetStep(basicAbilityStepData.jump + bonusAbilityStepData.jump);
        boostAbility.bonusGauge.SetStep(basicAbilityStepData.boostAbility + bonusAbilityStepData.boostAbility);
        //boostCharging.bonusGauge.SetStep(basicAbilityStepData.boostCharging + bonusAbilityStepData.boostCharging);
        //boostConsumption.bonusGauge.SetStep(basicAbilityStepData.boostConsumption + bonusAbilityStepData.boostConsumption);
    }

    public void Activate()
    {
        run.Activate();
        jump.Activate();
        boostAbility.Activate();
        //boostCharging.Activate();
        //boostConsumption.Activate();
    }

    public void Deactivate()
    {
        run.Deactivate();
        jump.Deactivate();
        boostAbility.Deactivate();
        //boostCharging.Deactivate();
        //boostConsumption.Deactivate();
    }
}
