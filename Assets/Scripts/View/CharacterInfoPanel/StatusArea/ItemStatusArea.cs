﻿using UnityEngine;
using System.Collections;

public class ItemStatusArea : MonoBehaviour, IStatusArea
{
    public ImprovementItem runII;
    public ImprovementItem boostRunII;
    public ImprovementItem jumpII;
    public ImprovementItem boostJumpII;
    public ImprovementItem boostChargingII;
    public ImprovementItem boostConsumptionII;

	void Start () 
    {
	}

    public void UpdateData(AbilityStepData abilityStepData)
    {
        //SetImprovement(runII, statusData.run);
        //SetImprovement(boostRunII, statusData.boostRun);
        //SetImprovement(jumpII, statusData.jump);
        //SetImprovement(boostJumpII, statusData.boostJump);
        //SetImprovement(boostChargingII, statusData.boostCharging);
        //SetImprovement(boostConsumptionII, statusData.boostConsumption);
    }

    private void SetImprovement(ImprovementItem item, float value)
    {
        if (value == 0) item.Deactivate();
        else
        {
            item.Activate();
            item.SetValue(value);
        }
    }
}
