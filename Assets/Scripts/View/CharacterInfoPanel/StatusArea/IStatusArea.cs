﻿using UnityEngine;
using System.Collections;

interface IStatusArea
{
    void UpdateData(AbilityStepData abilityStepData);
}
