﻿using UnityEngine;
using System.Collections;

public class ImprovementItem : MonoBehaviour 
{
    public UILabel plusValueLabel;
    public UISprite specIcon;
    public UILabel minusValueLabel;

    private float _value;

    void Awake()
    {
        Deactivate();
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}





    public float GetValue()
    {
        return _value;
    }

    public void SetValue(float value)
    {
        _value = value;
        if(_value > 0)
        {
            plusValueLabel.gameObject.SetActive(true);
            minusValueLabel.gameObject.SetActive(false);
            plusValueLabel.text = value.ToString() + "%";
        }
        else if(_value < 0)
        {
            plusValueLabel.gameObject.SetActive(false);
            minusValueLabel.gameObject.SetActive(true);
            minusValueLabel.text = (value * -1).ToString() + "%"; 
        }
        else
        {
            Deactivate();
        }
    }



    public void Activate()
    {
        specIcon.alpha = 1;
    }

    public void Deactivate()
    {
        specIcon.alpha = 0.1f;
        plusValueLabel.gameObject.SetActive(false);
        minusValueLabel.gameObject.SetActive(false);
    }
}
