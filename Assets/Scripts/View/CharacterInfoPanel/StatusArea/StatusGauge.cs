﻿using UnityEngine;
using System.Collections;

public class StatusGauge : MonoBehaviour 
{
    public UISprite basicGaugeBar;
    public UISprite additionGaugeBar;
    public UISprite bg;

    private float _value2 = 0;

	void Start () 
    {
	    
	}
	
	void Update () 
    {
	
	}

    



    public void SetBasic(float value)
    {
        basicGaugeBar.fillAmount = value;

		SetAddition(_value2);
    }

    public void SetAddition(float value)
    {
        _value2 = value;
        float totalValue = _value2;
        if (totalValue >= 1)
        {
            totalValue = 1;
            bg.gameObject.SetActive(false);
        }
        else
        {
            bg.gameObject.SetActive(true);
        }
        additionGaugeBar.fillAmount = totalValue;
    }
}
