﻿using UnityEngine;
using System.Collections;

public class UISpriteBg : MonoBehaviour
{
    public delegate void UISpriteBgEvent();
    public event UISpriteBgEvent Initialized;

	void Awake ()
    {
        
	}

    private void Start()
    {
        InitSize();
    }

    private void Update ()
    {
        UISprite uiSprite = GetComponent<UISprite>();
        uiSprite.width = UIScreen.instance.GetWidth();
        uiSprite.height = UIScreen.instance.GetHeight();
    }

    private void InitSize()
    {
        UISprite uiSprite = GetComponent<UISprite>();
        uiSprite.width = UIScreen.instance.GetWidth();
        uiSprite.height = UIScreen.instance.GetHeight();

        if (Initialized != null) Initialized();
    }
}
