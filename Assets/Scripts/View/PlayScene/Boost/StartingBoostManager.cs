﻿using UnityEngine;
using System.Collections;

public class StartingBoostManager : MonoBehaviour 
{
    public PlayerManager _player;
    public StartCounter _startCounter;
    public PlayerControlButton _playerControlButton;

    private int _direction = 0;

    void Awake ()
    {
        _startCounter.CountStart += OnCountStarted;
        _startCounter.CountEnd += OnCountCompleted;
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	    
	}

    void OnDestroy()
    {
        _playerControlButton.LeftPressed -= OnLeftButtonPress;
        _playerControlButton.RightPressed -= OnRightButtonPress;
    }



    #region Private Functions
    private void OnCountStarted()
    {
        _playerControlButton.LeftPressed += OnLeftButtonPress;
        _playerControlButton.RightPressed += OnRightButtonPress;
    }

    private void OnLeftButtonPress()
    {
        if (-0.2f <= _startCounter.GetCountdownTime() && _startCounter.GetCountdownTime() < 0f)
        {
            _direction = -1;
        }
        else if (0 <= _startCounter.GetCountdownTime() && _startCounter.GetCountdownTime() < 0.1f)
        {
            // _playerManager.StartBoost(0);
            _player.Boost(0.5f, 30f);
            //Invoke("EndBoost", 0.5f);
        }
    }

    private void OnRightButtonPress()
    {
        if (-0.2f <= _startCounter.GetCountdownTime() && _startCounter.GetCountdownTime() < 0)
        {
            _direction = 1;
        }
        if (0 < _startCounter.GetCountdownTime() && _startCounter.GetCountdownTime() < 0.1f)
        {
            _player.Boost(0.5f, 30f);
            //Invoke("EndBoost", 0.5f);
        }
    }

    private void EndBoost()
    {
        //_player.EndBoost(0.5f);
    }

    private void OnCountCompleted()
    {
        _player.GetMoveManager().HandOperatedMove(_direction);
    }
    #endregion



    #region Public Function
    public void Reset()
    {
        _direction = 0;

        _playerControlButton.LeftPressed -= OnLeftButtonPress;
        _playerControlButton.RightPressed -= OnRightButtonPress;
    }
    #endregion
}