﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class BoostEffect : MonoBehaviour
{
    public GameObject trail;
    public GameObject boostTrail;
    public float offsetY = 0;
    public AudioSource _audioSource;
    public AudioClip _boostAudio;

    private float[] normalModeAlphas = { 0.4f, 0.3f, 0.2f, 0.1f, 0.0f };
    private float[] boostModeAlphas = { 0.6f, 0.45f, 0.3f, 0.15f, 0.0f };
    private TrailRenderer _trailRenderer;
    private TrailRenderer _boostTrailRenderer;

    void Awake()
    {
        _trailRenderer = trail.GetComponent<TrailRenderer>();
        _boostTrailRenderer = boostTrail.GetComponent<TrailRenderer>();
        
        ChangeNormalMode(0);
    }



    #region Public Functions
    public void ShowEffect(float time)
    {
        DOTween.To(() => _boostTrailRenderer.time, x => _boostTrailRenderer.time = x, 0.1f, time);
    }

    public void HideEffect(float time)
    {
        DOTween.To(() => _boostTrailRenderer.time, x => _boostTrailRenderer.time = x, 0, time);
    }

    public void ChangeNormalMode(float time)
    {
        DOTween.To(() => _boostTrailRenderer.time, x => _boostTrailRenderer.time = x, 0.1f, 0.5f);
        DOTween.To(() => _boostTrailRenderer.startWidth, x => _boostTrailRenderer.startWidth = x, 0.2f, 0.5f);

        _audioSource.Stop();
    }

    public void ChangeBoostMode(float time)
    {
        //_boostTrailRenderer.time = 1f;
        //DOTween.To(() => _boostTrailRenderer.time, x => _boostTrailRenderer.time = x, 1f, 0.5f);
        //DOTween.To(() => _boostTrailRenderer.startWidth, x => _boostTrailRenderer.startWidth = x, 0.4f, 0.5f);
        _boostTrailRenderer.time = 0f;
        DOTween.To(() => _boostTrailRenderer.time, x => _boostTrailRenderer.time = x, 0f, 0.5f);
        DOTween.To(() => _boostTrailRenderer.startWidth, x => _boostTrailRenderer.startWidth = x, 0f, 0.5f);

        _audioSource.clip = _boostAudio;
        _audioSource.Play();
    }
    #endregion
}
