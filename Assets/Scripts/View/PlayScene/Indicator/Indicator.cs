﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Indicator : MonoBehaviour 
{
    public IndicatePointer playerIndigator;
    public IndicatePointer checkPointIndicator;
    public IndicatePointer startPointIndicator;

    private IndicatorModel _model;

    void Awake()
    {
        _model = GetComponent<IndicatorModel>();
        _model.On_Init += OnInitModel;
        _model.On_Show += OnShow;
        _model.On_Hide += OnHide;
        
        _model.On_ShowPlayerIndicator += OnShowPlayerIndicator;
        _model.On_HidePlayerIndicator += OnHidePlayerIndicator;
        
        _model.On_ShowStartPointIndicator += OnShowStartPointIndicator;
        _model.On_HideStartPointIndicator += OnHideStartPointIndicator;
        
        _model.On_ShowCheckPointIndicator += OnShowCheckPointIndicator;
        _model.On_HideCheckPointIndicator += OnHideCheckPointIndicator;

        _model.On_StartPointEnterScreen += OnStartPointEnterScreen;
        _model.On_StartPointOutScreen += On_StartPointOutScreen;

        OnHide();
    }

    void Start()
    {
    }

    private void OnInitModel()
    {
        _model.On_Init -= OnInitModel;
        //playerIndigator.thumb.spriteName = "CharacterProfileThumb_" + _model.characterID;
        //playerIndigator.thumb.mainTexture = CharacterDataManager.instance.GetCharacterDataByID(_model.characterID).profileImage;
        //playerIndigator.bg.color = _model.characterBgColor;
    }

    private void OnShow()
    {
        float time = 0.2f;
        if (_model.isShowPlayerIndicator) playerIndigator.Show(time);
        checkPointIndicator.Show(time);
        if (_model.isShowStartPointIndicator) startPointIndicator.Show(time);
    }

    private void OnHide()
    {
        float time = 0f;
        playerIndigator.Hide(time);
        checkPointIndicator.Hide(time);
        startPointIndicator.Hide(time);
    }

    private void OnShowPlayerIndicator()
    {
        playerIndigator.Show(0.2f);
    }

    private void OnHidePlayerIndicator()
    {
        playerIndigator.Hide(0.2f);
    }

    private void OnShowStartPointIndicator()
    {
        startPointIndicator.Show(0.2f);
    }

    private void OnHideStartPointIndicator()
    {
        startPointIndicator.Hide(0.2f);
    }

    private void OnShowCheckPointIndicator()
    {
        checkPointIndicator.Show(0.2f);
    }

    private void OnHideCheckPointIndicator()
    {
        checkPointIndicator.Hide(0.2f);
    }

    private void OnStartPointEnterScreen()
    {

    }

    private void On_StartPointOutScreen()
    {

    }

    void FixedUpdate() 
	{
        playerIndigator.transform.localPosition = _model.playerPos2D;

        startPointIndicator.transform.localPosition = _model.startPointPos2D;
        
        if(!_model.isStartPointInScreen)
        {
            startPointIndicator.transform.localEulerAngles = new Vector3(0, 0, _model.startPointAngle);
            startPointIndicator.thumb.transform.localEulerAngles = new Vector3(0, 0, -_model.startPointAngle);
        }
        else
        {
            startPointIndicator.transform.localEulerAngles = Vector3.zero;
            startPointIndicator.thumb.transform.localEulerAngles = Vector3.zero;
        }

        checkPointIndicator.transform.localPosition = _model.checkPointPos2D;

        if (!_model.isCheckPointInScreen)
        {
            checkPointIndicator.transform.localEulerAngles = new Vector3(0, 0, _model.checkPointAngle);
            checkPointIndicator.thumb.transform.localEulerAngles = new Vector3(0, 0, -_model.checkPointAngle);
        }
        else
        {
            checkPointIndicator.transform.localEulerAngles = Vector3.zero;
            checkPointIndicator.thumb.transform.localEulerAngles = Vector3.zero;
        }
	}

    void OnDestroy()
    {
        _model.On_Init -= OnInitModel;
        _model.On_Show -= OnShow;
        _model.On_Hide -= OnHide;

        _model.On_ShowPlayerIndicator -= OnShowPlayerIndicator;
        _model.On_HidePlayerIndicator -= OnHidePlayerIndicator;

        _model.On_ShowStartPointIndicator -= OnShowStartPointIndicator;
        _model.On_HideStartPointIndicator -= OnHideStartPointIndicator;

        _model.On_ShowCheckPointIndicator -= OnShowCheckPointIndicator;
        _model.On_HideCheckPointIndicator -= OnHideCheckPointIndicator;

        _model.On_StartPointEnterScreen -= OnStartPointEnterScreen;
        _model.On_StartPointOutScreen -= On_StartPointOutScreen;
    }
}
