﻿using UnityEngine;
using System.Collections;

public class IndicatorModel : MonoBehaviour 
{
    public delegate void IndicatorModelEvent();
    public event IndicatorModelEvent On_Init;
    public event IndicatorModelEvent On_Show;
    public event IndicatorModelEvent On_Hide;
    public event IndicatorModelEvent On_ShowPlayerIndicator;
    public event IndicatorModelEvent On_HidePlayerIndicator;
    public event IndicatorModelEvent On_ShowStartPointIndicator;
    public event IndicatorModelEvent On_HideStartPointIndicator;
    public event IndicatorModelEvent On_ShowCheckPointIndicator;
    public event IndicatorModelEvent On_HideCheckPointIndicator;
    public event IndicatorModelEvent On_StartPointEnterScreen;
    public event IndicatorModelEvent On_CheckPointEnterScreen;
    public event IndicatorModelEvent On_StartPointOutScreen;
    public event IndicatorModelEvent On_CheckPointOutScreen;

    public CameraController cameraController;
    public GameObject player;
    public int characterID;
    public Color characterBgColor;
    public Vector2 playerPos2D;
    public Vector2 checkPointPos2D;
    public Vector2 startPointPos2D;
    public float startPointAngle = 0;
    public float checkPointAngle = 0;
    public bool isStartPointInScreen = false;
    public bool isCheckPointInScreen = false;
    public bool isShowPlayerIndicator = false;
    public bool isShowStartPointIndicator = false;

    private GameObject startPoint;
    private GameObject checkPoint;

    void Awake()
    {
        playerPos2D = Vector2.zero;
        checkPointPos2D = Vector2.zero;
    }

    public void Init(int currentCharacterID, Color currentCharacterBgColor)
    {
        characterID = currentCharacterID;
        characterBgColor = currentCharacterBgColor;
        if (On_Init != null) On_Init();
    }

	void Start () 
	{
	
	}
	
	void FixedUpdate () 
	{
        float scale = 640f / Screen.height;

        Vector3 playerPos3D = player.transform.localPosition;
        playerPos3D.y = playerPos3D.y + 0.5f;
        Vector2 playerTempPos = cameraController.camera.WorldToScreenPoint(playerPos3D);
        playerTempPos = new Vector2((playerTempPos.x - Screen.width / 2) * scale, (playerTempPos.y - Screen.height / 2) * scale);
        playerPos2D.x = playerTempPos.x;
        playerPos2D.y = playerTempPos.y;

        Vector3 startPointPos3D = startPoint.transform.localPosition;
        startPointPos3D.y = startPointPos3D.y + 0.5f;
        Vector2 startPointTempPos = cameraController.camera.WorldToScreenPoint(startPointPos3D);
        startPointTempPos = new Vector2((startPointTempPos.x - Screen.width / 2) * scale, (startPointTempPos.y - Screen.height / 2) * scale);
        bool isStartPositionImmured = false;
        ImmurePosition(startPointTempPos, out startPointPos2D, out isStartPositionImmured);
        CheckInScreen("StartPoint", isStartPositionImmured);

        Vector3 checkPointPos3D = checkPoint.transform.localPosition;
        checkPointPos3D.y = checkPointPos3D.y + 0.5f;
        Vector2 checkPointTempPos = cameraController.camera.WorldToScreenPoint(checkPointPos3D);
        checkPointTempPos = new Vector2((checkPointTempPos.x - Screen.width / 2) * scale, (checkPointTempPos.y - Screen.height / 2) * scale);
        bool isCheckPositionImmured = false;
        ImmurePosition(checkPointTempPos, out checkPointPos2D, out isCheckPositionImmured);
        CheckInScreen("CheckPoint", isCheckPositionImmured);

        startPointAngle = GetAngle(playerTempPos, startPointTempPos);
        checkPointAngle = GetAngle(playerTempPos, checkPointTempPos);

        if (cameraController.GetZoom() > -20)
        {
            if(isShowPlayerIndicator)
            {
                isShowPlayerIndicator = false;
                if (On_HidePlayerIndicator != null) On_HidePlayerIndicator();
            }
        }
        else
        {
            if(!isShowPlayerIndicator)
            {
                isShowPlayerIndicator = true;
                if (On_ShowPlayerIndicator != null) On_ShowPlayerIndicator();
            }
        }

        if(cameraController.GetZoom() > -20)
        {
            if(!isShowStartPointIndicator)
            {
                isShowStartPointIndicator = true;
                if (On_ShowStartPointIndicator != null) On_ShowStartPointIndicator();
            }
        }
        else
        {
            if(isShowStartPointIndicator)
            {
                isShowStartPointIndicator = false;
                if (On_HideStartPointIndicator != null) On_HideStartPointIndicator();
            }
        }
	}

    private void CheckInScreen(string name, bool isImmured)
    {
        if (isImmured)
        {
            if (name == "StartPoint")
            {
                if (!isStartPointInScreen)
                {
                    isStartPointInScreen = true;
                    if (On_StartPointEnterScreen != null) On_StartPointEnterScreen();
                }
            }
            else if (name == "CheckPoint")
            {
                if(!isCheckPointInScreen)
                {
                    isCheckPointInScreen = true;
                    if (On_CheckPointEnterScreen != null) On_CheckPointEnterScreen();
                }
            }
        }
        else
        {
            if (name == "StartPoint")
            {
                if (isStartPointInScreen)
                {
                    isStartPointInScreen = false;
                    if (On_StartPointOutScreen != null) On_StartPointOutScreen();
                }
            }
            else if (name == "CheckPoint")
            {
                if (isCheckPointInScreen)
                {
                    isCheckPointInScreen = false;
                    if (On_CheckPointOutScreen != null) On_CheckPointOutScreen();
                }
            }
        }
    }

    private void ImmurePosition(Vector2 samplePos, out Vector2 resultPos, out bool isPositionImmured)
    {
        float space = 30;
        bool isImmured = true;
        resultPos = Vector2.zero;
        if (samplePos.x < -(UIScreen.instance.GetWidth() / 2) + space)
        {
            samplePos.x = -UIScreen.instance.GetWidth() / 2 + space;
            isImmured = false;
        }
        if (samplePos.x > UIScreen.instance.GetWidth() / 2 - space)
        {
            samplePos.x = UIScreen.instance.GetWidth() / 2 - space;
            isImmured = false;
        }
        if (samplePos.y < -(UIScreen.instance.GetHeight() / 2 + space))
        {
            samplePos.y = -UIScreen.instance.GetHeight() / 2 + space;
            isImmured = false;
        }
        if (samplePos.y > UIScreen.instance.GetHeight() / 2 - space)
        {
            samplePos.y = UIScreen.instance.GetHeight() / 2 - space;
            isImmured = false;
        }
        resultPos = samplePos;
        isPositionImmured = isImmured;
    }

    public float GetAngle(Vector2 p1, Vector2 p2)
    {
        float xdf = p2.x - p1.x;
        float ydf = p2.y - p1.y;

        float ang = RadianToDegree(Mathf.Atan2(ydf, xdf));
        return ang + 90;
    }

    public float RadianToDegree(float rad)
    {
        return rad * (180 / Mathf.PI);
    }

    public void SetStartPoint(GameObject startPoint)
    {
        this.startPoint = startPoint;
    }

    public void SetCheckPoint(GameObject checkPoint)
    {
        this.checkPoint = checkPoint;
    }

    public void Show()
    {
        if(On_Show != null) On_Show();
    }

    public void Hide()
    {
        if (On_Hide != null) On_Hide();
    }
}
