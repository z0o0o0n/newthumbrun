﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class IndicatePointer : MonoBehaviour 
{
    public UISprite thumb;

    void Awake()
    {
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	}

    public void Show(float time = 0.0f)
    {
        transform.DOScale(1, time).SetUpdate(true);
    }

    public void Hide(float time = 0.0f)
    {
        transform.DOScale(0, time).SetUpdate(true);
    }
}
