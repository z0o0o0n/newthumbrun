﻿using UnityEngine;
using System.Collections;

public class VersusThumb : MonoBehaviour 
{
    public UIPanel panel;
    public UISprite stampX;
    public UISprite thumbShape;
    public UISprite thumb;
    public UISprite bg;

    private CharacterData _characterData;

    void Awake ()
    {
        //stampX.gameObject.SetActive(false);
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void SetData(int characterID)
    {
        _characterData = CharacterDataManager.instance.GetCharacterDataByID(characterID);
        thumbShape.spriteName = "CharacterProfileThumb_Shape_" + characterID;
        thumbShape.color = _characterData.color;
        thumb.spriteName = "CharacterProfileThumb_" + characterID;
        bg.color = _characterData.bgColor;
    }

    public void LockCharacter()
    {
        stampX.gameObject.SetActive(false);
        thumbShape.gameObject.SetActive(true);
        thumb.gameObject.SetActive(false);

        panel.alpha = 0.4f;
    }

    public void UnlockCharacter()
    {
        stampX.gameObject.SetActive(false);
        thumbShape.gameObject.SetActive(false);
        thumb.gameObject.SetActive(true);

        panel.alpha = 1f;
    }

    public void CloseCharacter()
    {
        stampX.gameObject.SetActive(true);
        thumbShape.gameObject.SetActive(false);
        thumb.gameObject.SetActive(true);

        panel.alpha = 0.5f;
    }

    public void Stamp()
    {
        stampX.gameObject.SetActive(true);
    }
}
