﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VersusListViewer : MonoBehaviour 
{
    public List<VersusThumb> versusThumbList;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void SetData(List<int> characterIDList)
    {
        int count = versusThumbList.Count;
        if(characterIDList.Count <= 5) count = characterIDList.Count;

        for (int i = 0; i < versusThumbList.Count; i++)
        {
            if (i >= count)
            {
                versusThumbList[i].gameObject.SetActive(false);
                break;
            }
            versusThumbList[i].SetData(characterIDList[i]);
        }
    }

    public void Select(int index)
    {
        for(int i = 0; i < versusThumbList.Count; i++)
        {
            if(index > i)
            {
                versusThumbList[i].CloseCharacter();
            }
            else if(index < i)
            {
                versusThumbList[i].LockCharacter();
            }
            else
            {
                versusThumbList[i].UnlockCharacter();
            }
        }
    }

    public void Stamp(int index)
    {
        versusThumbList[index].Stamp();
    }
}
