﻿using UnityEngine;
using System.Collections;

public class DifficultyViewer : MonoBehaviour 
{
    public UISprite uiSprite;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void Change(VersusDifficultyType.type type)
    {
        if(type == VersusDifficultyType.type.Easy)
        {
            uiSprite.spriteName = "Difficulty_Easy";
        }
        else if (type == VersusDifficultyType.type.Normal)
        {
            uiSprite.spriteName = "Difficulty_Normal";
        }
        else if (type == VersusDifficultyType.type.Hard)
        {
            uiSprite.spriteName = "Difficulty_Hard";
        }
        else if (type == VersusDifficultyType.type.VeryHard)
        {
            uiSprite.spriteName = "Difficulty_VeryHard";
        }
        else if (type == VersusDifficultyType.type.Intense)
        {
            uiSprite.spriteName = "Difficulty_Intense";
        }
        uiSprite.color = DifficultyColor.GetDifficultyColorByType(type);
    }
}
