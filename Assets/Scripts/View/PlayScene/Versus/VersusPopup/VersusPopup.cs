﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Junhee.Utils;

public class VersusPopup : MonoBehaviour 
{
    public Color TITLE_BG_GREEN;
    public Color TITLE_BG_RED;

    public delegate void VersusPopupEvent();
    public event VersusPopupEvent RetryButtonClick;
    public event VersusPopupEvent NextRunButtonClick;
    public event VersusPopupEvent WinPopupEnded;
    public event VersusPopupEvent Hided;

    public UIPanel uiPanel;
    public UISprite vsLogo;
    public UILabel titleLabel_A;
    public UILabel nameLabel_A;
    public UILabel levelLabel_A;
    //public DifficultyViewer difficultyViewer_A;
    public UILabel titleLabel_B;
    public UILabel nameLabel_B;
    public UILabel levelLabel_B;
    //public DifficultyViewer difficultyViewer_B;
    public VersusPopupCharacterViewer versusPopupCharacterViewer;
    public GoldLabel goldLabel;
    public UIBasicButton retryButton;
    public UIBasicButton nextRunButton;
    public UIWidget speechBubble;
    public UISprite topBg;
    public UISprite bottomBg;
    public UISprite bg;
    public Camera highLevelCamera;
    public AnimationCurve customElastic;
    //public AudioSource audioSource;
    //public AudioClip versusSound;

    public float offsetX;
    public float offsetY;
    public bool isExpand = false;

    private VersusPopupData _currentVersusPopupData;
    private VersusPopupData _nextVersusPopupData;
    private float _popupPosY = 0;

    void Awake()
    {
    }

	void Start () 
	{
        goldLabel.Show(0.2f);
        Hide(0);
	}
	
	void Update () 
	{
        float scale = 640f / Screen.height;
       
        float xpos = (Screen.width / 2) + transform.localPosition.x;
        float ypos = ((640f / 2) + transform.localPosition.y) * (Screen.height / 640f);

        Vector3 newPos = highLevelCamera.ScreenToWorldPoint(new Vector3(xpos, ypos, 2.2f));
        //Vector3 newPos = highLevelCamera.ScreenToWorldPoint(new Vector3(xpos, ypos, 2.2f));

        versusPopupCharacterViewer.transform.localPosition = new Vector3((newPos.x + offsetX) * scale, (newPos.y + offsetY), versusPopupCharacterViewer.transform.localPosition.z);
	}

    public void SetVersusData(VersusPopupData currentData, VersusPopupData nextData)
    {
        _currentVersusPopupData = currentData;
        _nextVersusPopupData = nextData;

        //titleLabel_A.color = DifficultyColor.GetDifficultyColorByType(currentData.difficultyType);
        nameLabel_A.color = currentData.color;
        nameLabel_A.text = currentData.name;
        levelLabel_A.text = currentData.level;
        //difficultyViewer_A.Change(currentData.difficultyType);

        //titleLabel_B.color = DifficultyColor.GetDifficultyColorByType(nextData.difficultyType);
        //Debug.LogWarning("VersusPopup - SetVersusData() / nextData: " + nextData);
        if (nextData != null)
        {
            nameLabel_B.color = nextData.color;
            nameLabel_B.text = nextData.name;
            levelLabel_B.text = nextData.level;
            Debug.LogWarning("level label b: " + levelLabel_B.text);
            versusPopupCharacterViewer.SetCharacter(currentData.characterID, nextData.characterID);
        }
        else
        {
            versusPopupCharacterViewer.SetCharacter(currentData.characterID, -1);
        }
        
        //difficultyViewer_B.Change(nextData.difficultyType);

        goldLabel.SetValue(currentData.rewardGold);
        bg.color = currentData.bgColor;
    }

    public void ChangeVersusData()
    {

    }

    public void Show(float time)
    {
        //audioSource.clip = versusSound;
        //audioSource.Play();
        transform.DOLocalMoveY(_popupPosY, time).SetEase(customElastic);
    }

    public void ReplacePopup(float time)
    {
        transform.DOLocalMoveY(_popupPosY, time).SetEase(customElastic);
    }

    public void Hide(float time)
    {
        if (time == 0)
        {
            transform.localPosition = new Vector3(0, -640, 0);
            if (Hided != null) Hided();
        }
        else
        {
            transform.DOLocalMoveY(-640, time).SetEase(Ease.InBack).OnComplete(OnHided);
        }
    }

    private void OnHided()
    {
        if (Hided != null) Hided();
    }

    private void ExpandBg(float time)
    {
        isExpand = true;
        bottomBg.transform.DOLocalMoveY(-238, time);
        DOTween.To(() => bottomBg.alpha, x => bottomBg.alpha = x, 0.2f, time);

        DOTween.To(() => bg.height, x => bg.height = x, 420, time);
        _popupPosY = 50;
        //transform.DOLocalMoveY(50, time);
    }

    private void ShrinkBg(float time)
    {
        isExpand = false;
        bottomBg.transform.DOLocalMoveY(-138, time);
        DOTween.To(() => bottomBg.alpha, x => bottomBg.alpha = x, 0, time);

        DOTween.To(() => bg.height, x => bg.height = x, 320, time);
        _popupPosY = 0;
        //transform.DOLocalMoveY(0, 0);
    }

    private void ShowRetryButton(float time)
    {
        retryButton.On_Click += OnRetryButtonClick;
        Vector3 pos = retryButton.transform.localPosition;
        //pos.y = -250;
        retryButton.transform.DOLocalMoveY(-270, time);

        UISprite sprite = retryButton.GetComponent<UISprite>();
        //sprite.alpha = 0;
        DOTween.To(() => sprite.alpha, x => sprite.alpha = x, 1, time);
    }

    private void OnRetryButtonClick()
    {
        retryButton.On_Click -= OnRetryButtonClick;
        if (RetryButtonClick != null) RetryButtonClick();
    }

    private void HideRetryButton(float time)
    {
        retryButton.On_Click -= OnRetryButtonClick;
        retryButton.transform.DOLocalMoveY(-250, time);

        UISprite sprite = retryButton.GetComponent<UISprite>();
        DOTween.To(() => sprite.alpha, x => sprite.alpha = x, 0, time);
    }

    private void ShowNextRunButton(float time)
    {
        nextRunButton.On_Click += OnNextRunButtonClick;
        Vector3 pos = nextRunButton.transform.localPosition;
        //pos.y = -250;
        nextRunButton.transform.DOLocalMoveY(-270, time);

        UISprite sprite = nextRunButton.GetComponent<UISprite>();
        //sprite.alpha = 0;
        DOTween.To(() => sprite.alpha, x => sprite.alpha = x, 1, time);
    }

    private void OnNextRunButtonClick()
    {
        nextRunButton.On_Click -= OnNextRunButtonClick;
        if (NextRunButtonClick != null) NextRunButtonClick();
    }

    private void HideNextRunButton(float time)
    {
        nextRunButton.On_Click -= OnNextRunButtonClick;
        nextRunButton.transform.DOLocalMoveY(-250, time);

        UISprite sprite = nextRunButton.GetComponent<UISprite>();
        DOTween.To(() => sprite.alpha, x => sprite.alpha = x, 0, time);
    }

    private void ShowContentsB()
    {
        titleLabel_B.gameObject.SetActive(true);
        nameLabel_B.gameObject.SetActive(true);
        levelLabel_B.gameObject.SetActive(true);
    }

    private void HideContentsB()
    {
        titleLabel_B.gameObject.SetActive(false);
        nameLabel_B.gameObject.SetActive(false);
        levelLabel_B.gameObject.SetActive(false);
    }

    private void ShowContentsA()
    {
        titleLabel_A.gameObject.SetActive(true);
        nameLabel_A.gameObject.SetActive(true);
        levelLabel_A.gameObject.SetActive(true);
    }



    public void ChangeVSView()
    {
        HideContentsB();
        ShowContentsA();

        titleLabel_A.gameObject.SetActive(false);
        titleLabel_A.alpha = 1;
        titleLabel_A.transform.DOLocalMoveX(0, 0);
        vsLogo.gameObject.SetActive(true);

        nameLabel_A.alpha = 1;
        nameLabel_A.transform.DOLocalMoveX(0, 0);

        levelLabel_A.alpha = 0.2f;
        levelLabel_A.transform.DOLocalMoveX(0, 0);

        goldLabel.SetValue(_currentVersusPopupData.rewardGold);
        goldLabel.Show(0);
        topBg.color = TITLE_BG_RED;
        bg.color = _currentVersusPopupData.bgColor;
        versusPopupCharacterViewer.ChangeVSCharacter();
        HideRetryButton(0);
        ShowNextRunButton(0);
        speechBubble.gameObject.SetActive(false);
        ExpandBg(0);
    }

    public void ChangeWinView()
    {
        HideContentsB();
        ShowContentsA();

        ShrinkBg(0);
        HideNextRunButton(0);

        titleLabel_A.gameObject.SetActive(true);
        titleLabel_A.text = "YOU WIN!!";

        vsLogo.gameObject.SetActive(false);
        titleLabel_B.gameObject.SetActive(false);

        goldLabel.Hide(0);
        goldLabel.SetValue(0);

        topBg.color = TITLE_BG_GREEN;

        versusPopupCharacterViewer.ChangeWinCharacter();
    }

    public void Stamp()
    {
        versusPopupCharacterViewer.Stamp();
    }

    public void CountGold()
    {
        goldLabel.Ended += OnCountEnded;
        goldLabel.Show(0.2f);
        goldLabel.Count(_currentVersusPopupData.rewardGold, 2f);
    }

    public void ShowSpeechBubble()
    {
        speechBubble.gameObject.SetActive(true);
    }

    private void OnCountEnded()
    {
        goldLabel.Ended -= OnCountEnded;
        if (WinPopupEnded != null) WinPopupEnded();
    }

    public void MoveToVSNextView(float time)
    {
        titleLabel_A.transform.DOLocalMoveX(-100, time);
        DOTween.To(() => titleLabel_A.alpha, x => titleLabel_A.alpha = x, 0, time);

        nameLabel_A.transform.DOLocalMoveX(-100, time);
        DOTween.To(() => nameLabel_A.alpha, x => nameLabel_A.alpha = x, 0, time);

        levelLabel_A.transform.DOLocalMoveX(-100, time);
        DOTween.To(() => levelLabel_A.alpha, x => levelLabel_A.alpha = x, 0, time);

        ShowContentsB();
        
        Vector3 vsLogoPos = vsLogo.transform.localPosition;
        vsLogoPos.x = 100;
        vsLogo.gameObject.SetActive(true);
        vsLogo.alpha = 0;
        vsLogo.transform.localPosition = vsLogoPos;
        vsLogo.transform.DOLocalMoveX(0, time);
        DOTween.To(() => vsLogo.alpha, x => vsLogo.alpha = x, 1, time);

        titleLabel_B.gameObject.SetActive(false);
        topBg.color = TITLE_BG_RED;

        Vector3 nameLabelBPos = nameLabel_B.transform.localPosition;
        nameLabelBPos.x = 100;
        nameLabel_B.transform.localPosition = nameLabelBPos;
        nameLabel_B.transform.DOLocalMoveX(0, time);
        nameLabel_B.alpha = 0;
        DOTween.To(() => nameLabel_B.alpha, x => nameLabel_B.alpha = x, 1, time);

        Vector3 levelLabelBPos = levelLabel_B.transform.localPosition;
        levelLabelBPos.x = 100;
        levelLabel_B.transform.localPosition = levelLabelBPos;
        levelLabel_B.transform.DOLocalMoveX(0, time);
        levelLabel_B.alpha = 0;
        DOTween.To(() => levelLabel_B.alpha, x => levelLabel_B.alpha = x, 0.2f, time);

        goldLabel.SetValue(_nextVersusPopupData.rewardGold);
        //goldLabel.Count(_nextVersusPopupData.rewardGold, 1f);

        DOTween.To(() => bg.color, x => bg.color = x, _nextVersusPopupData.bgColor, time);

        versusPopupCharacterViewer.MoveToVSNextCharacter();

        ShowNextRunButton(0.5f);

        ExpandBg(0.5f);

        ReplacePopup(0.5f);
    }

    public void ChangeVSRetryView()
    {
        HideContentsB();
        ShowContentsA();

        vsLogo.gameObject.SetActive(false);

        titleLabel_A.text = "YOU LOSE!!";
        goldLabel.SetValue(_currentVersusPopupData.rewardGold);
        goldLabel.Show(0);

        versusPopupCharacterViewer.ChangeVSRetryCharacter();

        HideNextRunButton(0);
        ShowRetryButton(0);

        topBg.color = TITLE_BG_RED;
        ExpandBg(0);
    }



    void OnDestroy()
    {
        goldLabel.Ended -= OnCountEnded;
        retryButton.On_Click -= OnRetryButtonClick;
        nextRunButton.On_Click -= OnNextRunButtonClick;
    }
}
