﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class VersusPopupCharacterViewer : MonoBehaviour 
{
    public GameObject currentVersusCharacter;
    public GameObject nextVersusCharacter;
    public Animator currentAnimator;
    public Animator nextAnimator;
    public GameObject winStamp;

    private float _scale;

    void Awake()
    {
    }

	void Start () 
	{
	}
	
	void Update () 
	{
	
	}

    public void SetCharacter(int characterID, int nextCharacterID)
    {
        if (currentVersusCharacter) Destroy(currentVersusCharacter);
        if (nextVersusCharacter) Destroy(nextVersusCharacter);

        //_scale = 1136f / Screen.width;

        //Debug.LogWarning("scale ===== : " + _scale);
        GameObject characterPrefab = CharacterPrefabProvider.instance.Get(characterID);
        currentVersusCharacter = GameObject.Instantiate(characterPrefab);
        currentVersusCharacter.layer = 8;
        currentVersusCharacter.transform.parent = this.transform;
        currentVersusCharacter.transform.localPosition = new Vector3(0, 0, -0.05f);
        currentVersusCharacter.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        //currentVersusCharacter.transform.localScale = new Vector3(_scale, _scale, _scale);

        Character currentCharacter = currentVersusCharacter.GetComponent<Character>();
        currentCharacter.ChangeRivalCharacter();

        currentAnimator = currentVersusCharacter.GetComponent<Animator>();
        currentAnimator.Play(Animator.StringToHash("Main.Idle"));
        //currentCharacter.animation

        //Debug.LogWarning("VersusPopupCharacterViewer - SetCharacter() / nextCharacterID: " + nextCharacterID);
        if (nextCharacterID == -1)
        {
            nextVersusCharacter = null;
            return;
        }
        GameObject nextCharacterPrefab = CharacterPrefabProvider.instance.Get(nextCharacterID);
        nextVersusCharacter = GameObject.Instantiate(nextCharacterPrefab);
        nextVersusCharacter.layer = 8;
        nextVersusCharacter.transform.parent = this.transform;
        nextVersusCharacter.transform.localPosition = new Vector3(0, 0, -0.05f);
        nextVersusCharacter.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        //nextVersusCharacter.transform.localScale = new Vector3(_scale, _scale, _scale);
        nextVersusCharacter.SetActive(false);
        //nextVersusCharacter.transform.localScale = Vector3.zero;

        Character nextCharacter = nextVersusCharacter.GetComponent<Character>();
        nextCharacter.ChangeRivalCharacter();

        nextAnimator = nextVersusCharacter.GetComponent<Animator>();
        nextAnimator.Play(Animator.StringToHash("Main.Idle"));
    }

    public void ChangeVSCharacter()
    {
        currentVersusCharacter.SetActive(true);
        //currentVersusCharacter.transform.DOScale(1, 0);
        currentAnimator.Play(Animator.StringToHash("Main.Idle"));

        if(nextVersusCharacter != null) nextVersusCharacter.SetActive(false);
        
        winStamp.SetActive(false);
        winStamp.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
        

        //nextVersusCharacter.transform.DOScale(0, 0);
    }

    public void ChangeWinCharacter()
    {
        currentAnimator.Play(Animator.StringToHash("Main.Lose"));

        //nextVersusCharacter.transform.DOScale(1, 0);
        //nextAnimator.Play(Animator.StringToHash("Main.Win"));
    }

    public void Stamp()
    {
        winStamp.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
        winStamp.SetActive(true);
        winStamp.transform.DOScale(0.3f, 0.1f).SetEase(Ease.InCubic);
    }

    public void MoveToVSNextCharacter()
    {
        currentVersusCharacter.SetActive(false);
        if(nextVersusCharacter != null) nextVersusCharacter.SetActive(true);

        winStamp.SetActive(false);
    }

    public void ChangeVSRetryCharacter()
    {
        currentAnimator.Play(Animator.StringToHash("Main.Win"));

        //nextVersusCharacter.transform.DOScale(1, 0);
        //nextAnimator.Play(Animator.StringToHash("Main.Lose"));
    }
}
