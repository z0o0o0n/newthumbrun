﻿using UnityEngine;
using System.Collections;
using Junhee.Utils;

public class VersusManagerTester : MonoBehaviour 
{
    public VersusManager versusManager;

	void Start () 
	{
        
	}
	
	void Update () 
	{
	    if(Input.GetKeyUp(KeyCode.A))
        {
            versusManager.ShowVersusPopup();
        }
        else if(Input.GetKeyUp(KeyCode.S))
        {
            versusManager.HideVersusPopup();
        }
        else if(Input.GetKeyUp(KeyCode.Q))
        {
            versusManager.ChangeVSView();
        }
        else if (Input.GetKeyUp(KeyCode.W))
        {
            versusManager.ChangeWinView();
        }
        else if (Input.GetKeyUp(KeyCode.E))
        {
            versusManager.MoveToVSNextView();
        }
        else if (Input.GetKeyUp(KeyCode.R))
        {
            versusManager.ChangeVSRetryView();
        }
	}
}
