﻿using UnityEngine;
using System.Collections;

public class VersusPopupData 
{
    public string name;
    public string level;
    public int characterID;
    public int rewardGold;
    public Color color;
    public Color bgColor;
}
