﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VersusData : MonoBehaviour 
{
    public int difficulty;
    public int rewardGold;
    public float record;
    public ReplayData replayData;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public virtual List<int> GetStateHash()
    {
        return null;
    }

    public virtual List<Vector3> GetTrailPosition()
    {
        return null;
    }
}
