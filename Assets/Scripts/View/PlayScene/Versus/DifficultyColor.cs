﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Junhee.Utils;

public class DifficultyColor : MonoBehaviour 
{
    public static List<string> colors = new List<string> { "0x6aa1c3", "0x3a9d72", "0xfcda7f", "0xd78360", "0xb64e59" };

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public static Color GetDifficultyColorByType(VersusDifficultyType.type type)
    {
        Color result = Color.black;

        if(type == VersusDifficultyType.type.Easy)
        {
            result = ColorUtil.ConvertHexStringToColor(colors[0]);
        }
        else if (type == VersusDifficultyType.type.Normal)
        {
            result = ColorUtil.ConvertHexStringToColor(colors[1]);
        }
        else if (type == VersusDifficultyType.type.Hard)
        {
            result = ColorUtil.ConvertHexStringToColor(colors[2]);
        }
        else if (type == VersusDifficultyType.type.VeryHard)
        {
            result = ColorUtil.ConvertHexStringToColor(colors[3]);
        }
        else if (type == VersusDifficultyType.type.Intense)
        {
            result = ColorUtil.ConvertHexStringToColor(colors[4]);
        }

        return result;
    }
}
