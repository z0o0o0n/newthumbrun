﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms;

public class VersusManager : MonoBehaviour 
{
    public delegate void VersusPopupEvent();
    public event VersusPopupEvent ClosePopup;
    public event VersusPopupEvent ClosedPopup;
    public event VersusPopupEvent RetryButtonClick;
    public event VersusPopupEvent NextRunButtonClick;
    public event VersusPopupEvent EndWinPopup;

    public VersusPopup versusPopup;
    public AudioSource audioSource;
    public AudioClip vsSound;
    public AudioClip winSound;
    public AudioClip loseSound;
    public AudioSource effectAudioSource;
    public AudioClip stampSound;
    public VersusListViewer versusListViewer;

    [Header("Dynamic Allocation Comp")]
    [SerializeField]
    private RaceManager _raceManager;

    private StageData _stageData;
    private VersusData _versusData;
    private CharacterData _versusCharacterData;
    private VersusPopupData _versusPopupData;
    private VersusData _nextVersusData;
    private CharacterData _nextVersusCharacterData;
    private VersusPopupData _nextVersusPopupData;
    //private List<int>
    
    void Awake()
    {
        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();

        versusPopup.RetryButtonClick += OnRetryButtonClick;
        versusPopup.NextRunButtonClick += OnNextRunButtonClick;
    }

	void Start () 
	{
	
	}

    void OnDestroy()
    {
    }



    #region Private Functions
    private void OnRaceInfoScreenShow()
    {
        if(RaceModeInfo.raceMode == RaceMode.mode.SINGLE)
        {
            ChangeVSView();
            ShowVersusPopup();
        }
    }
    #endregion

    //public void Init(int stageIndex)
    //{
    //    _stageData = StageDataManager.instance.GetStageDataByIndex(stageIndex);


    //    int versusStep = _stageData.versusStep;
    //    int nextVersusStep = _stageData.versusStep + 1;

    //    // remove componet
    //    if(_versusData != null) Destroy(_versusData);
    //    if (_nextVersusData != null) Destroy(_nextVersusData);

    //    // CURRENT ----------
    //    // add versusData componet
    //    string versusDataClassName = "VersusData_" + _stageData.index + "_" + versusStep;
    //    Debug.Log("----- versusDataClassName: " + versusDataClassName);
    //    System.Type versusDataClassType = System.Type.GetType(versusDataClassName);
    //    _versusData = gameObject.AddComponent(versusDataClassType) as VersusData;

    //    // get character data
    //    int versusCharacterID = _stageData.versusCharacterIDList[versusStep];
    //    _versusCharacterData = CharacterDataManager.instance.GetCharacterDataByID(versusCharacterID);

    //    //_versusPopupData = GetVersusPopupData(versusStep, _versusCharacterData, _versusData);

    //    //Debug.LogWarning("VersusManager - Init()");
    //    //Debug.LogWarning("versusStep: " + _stageData.versusStep);
    //    //Debug.LogWarning("hasNextVersus: " + hasNextVersus(_stageData)); ;
    //    if(hasNextVersus(_stageData))
    //    {
    //        // NEXT ----------
    //        // add next versusData componet
    //        string nextVersusDataClassName = "VersusData_" + stageIndex + "_" + nextVersusStep;
    //        System.Type nextVersusDataClassType = System.Type.GetType(nextVersusDataClassName);
    //        _nextVersusData = gameObject.AddComponent(nextVersusDataClassType) as VersusData;

    //        // get next character data
    //        int nextVersusCharacterID = _stageData.versusCharacterIDList[nextVersusStep];
    //        _nextVersusCharacterData = CharacterDataManager.instance.GetCharacterDataByID(nextVersusCharacterID);
    //        //_nextVersusPopupData = GetVersusPopupData(nextVersusStep, _nextVersusCharacterData, _nextVersusData);
    //        //Debug.LogWarning("nextVersusCharacterData: " + _nextVersusCharacterData.id);
    //    }
    //    else
    //    {
    //        _nextVersusPopupData = null;
    //    }

    //    // Sset
    //    versusPopup.SetVersusData(_versusPopupData, _nextVersusPopupData);
    //    versusListViewer.SetData(_stageData.versusCharacterIDList);
    //    versusListViewer.Select(versusStep);
    //}

    private void OnRetryButtonClick()
    {
        if (RetryButtonClick != null) RetryButtonClick();
    }

    private void OnNextRunButtonClick()
    {
        if (NextRunButtonClick != null) NextRunButtonClick();
    }

    //private VersusPopupData GetVersusPopupData(int versusStep, CharacterData characterData, VersusData versusData)
    //{
    //    // create versus popup data
    //    VersusPopupData result = new VersusPopupData();
    //    result.name = characterData.name;
    //    //result.difficultyType = VersusDifficultyType.GetTypeByIndex(versusData.difficulty);
    //    result.level = "LEVEL " + (versusStep + 1);
    //    result.characterID = characterData.id;
    //    //result.rewardGold = versusData.rewardGold;
    //    result.rewardGold = versusData.rewardGold;
    //    result.color = characterData.color;
    //    result.bgColor = characterData.bgColor;

    //    return result;
    //}

	void Update () 
	{
	
	}

    public VersusData GetVersusData()
    {
        return _versusData;
    }

    public CharacterData GetVersusCharacterData()
    {
        return _versusCharacterData;
    }

    public void ShowVersusPopup()
    {
        audioSource.Play();

        versusPopup.Show(0.3f);

        //if (autoHide) Invoke("HideVersusPopup", delay);
    }

    public void HideVersusPopup()
    {
        versusPopup.Hided += OnHided;
        versusPopup.Hide(0.3f);

        //_raceManager.HideRaceInfoScreen();

        if (ClosePopup != null) ClosePopup();
    }

    private void OnHided()
    {
        versusPopup.Hided -= OnHided;
        if (ClosedPopup != null) ClosedPopup();
    }

    //private void EndVersusPopup()
    //{
    //    versusPopup.Hide(0.3f);
    //    if (ClosePopup != null) ClosePopup();
    //}

    //public void CountVersusStep()
    //{
        //_stageData.versusStep++;
        
        //if (IsLastLevel())
        //{
        //    _isLastLevel = true;
        //}
    //}

    //private bool IsLastLevel()
    //{
    //    bool result = (_stageData.versusStep >= _stageData.versusCharacterIDList.Count - 1);
    //    return result;
    //}

    public void ChangeVSView()
    {
        audioSource.clip = vsSound;
        versusPopup.NextRunButtonClick += OnStartRunButtonClick;
        versusPopup.ChangeVSView();
    }

    private void OnStartRunButtonClick()
    {
        versusPopup.NextRunButtonClick -= OnStartRunButtonClick;
        HideVersusPopup();
    }

    public void ChangeWinView()
    {
        ChangeVSView();

        audioSource.clip = winSound;

        versusPopup.WinPopupEnded += OnWinPopupEnd;
        versusPopup.ChangeWinView();
        Invoke("Stamp", 1f);
        Invoke("CountGold", 2f);
        //Invoke("MoveToVSNextView", 3f);
    }

    // Count Gold까지 끝났을 때
    private void OnWinPopupEnd()
    {
        versusPopup.WinPopupEnded -= OnWinPopupEnd;

    //    if (hasVersus(_stageData)) Invoke("MoveToVSNextView", 1f);
    //    else
    //    {
    //        versusPopup.ShowSpeechBubble();
    //        #if UNITY_IOS
				////if(GameManager.isFreeVersion) GKAchievementReporter.ReportAchievement("Master_" + _stageData.index + "_free", 100, true);
				////GKAchievementReporter.ReportAchievement("Master_" + _stageData.index, 100, true);
    //        #elif UNITY_ANDROID
    //            // if(_stageData.index == 0) GKAchievementReporter.ReportAchievement(GPGSIds.achievement_winner_of_sky_city, 100, true);
    //            // else if (_stageData.index == 1) GKAchievementReporter.ReportAchievement(GPGSIds.achievement_winner_of_monsters_field, 100, true);
    //            // else if (_stageData.index == 2) GKAchievementReporter.ReportAchievement(GPGSIds.achievement_winner_of_area_52, 100, true);
    //        #endif
    //    }
        
        if (EndWinPopup != null) EndWinPopup();
    }

    private void Stamp()
    {
        #if UNITY_IOS
            //GKAchievementReporter.ReportAchievement("Clear_" + _stageData.index + "_" + (_stageData.versusStep - 1), 100, true);
        #endif

        effectAudioSource.clip = stampSound;
        effectAudioSource.Play();
        
        versusPopup.Stamp();
        //versusListViewer.Stamp(_stageData.versusStep-1);
    }

    private void CountGold()
    {
        versusPopup.CountGold();
    }

    public void MoveToVSNextView()
    {
        audioSource.clip = vsSound;
        audioSource.Play();

        versusPopup.MoveToVSNextView(0.5f);

        //versusListViewer.Select(_stageData.versusStep);
    }

    public void ChangeVSRetryView()
    {
        ChangeVSView();

        audioSource.clip = loseSound;

        versusPopup.ChangeVSRetryView();
    }

    //public bool hasVersus(StageData stageData)
    //{
    //    //Debug.LogWarning("has Versus: " + stageData.versusStep + " / " + (stageData.versusCharacterIDList.Count - 1));
    //    //bool result = (stageData.versusStep <= stageData.versusCharacterIDList.Count - 1);
    //    return result;
    //}

    //public bool hasNextVersus(StageData stageData)
    //{
    //    //Debug.LogWarning("has Next Versus: " + stageData.versusStep + " / " + (stageData.versusCharacterIDList.Count - 1));
    //    //bool result = (stageData.versusStep + 1 <= stageData.versusCharacterIDList.Count - 1);
    //    return result;
    //}

    void OnDestory()
    {
        versusPopup.WinPopupEnded -= OnWinPopupEnd;
        versusPopup.NextRunButtonClick -= OnStartRunButtonClick;
        versusPopup.Hided -= OnHided;
    }
}
