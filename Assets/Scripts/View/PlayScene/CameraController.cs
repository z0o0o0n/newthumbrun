﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    public delegate void CameraEvent();
    public event CameraEvent PreviewZoomOutEnd;

    [Header("Basic")]
    public Camera camera;
	public float minZoomDistance = 0.0f;
	public float maxZoomDistance = 0.0f;
    public float boostZoomDistance = 0.0f;

    [Header("Fallow")]
    public GameObject fallowTarget;
    public GameObject cube;
    public float DSpeed = 0.02f;
    public float leftRightMove = 30;
    public float DCameraMove = 0.05f;
    public float DCameraZoom = 0.05f;
    public float offsetY = 0.4f;

    [Header("Preview")]
    public float previewZoomOutTime = 1f;
    public float previewPosY = 2f;
    public float previewZoom = 32;

    public delegate void CameraControlEvent();
    public event CameraControlEvent On_EndPreviewMotion;

    [Header("Dynamic Allowcation Class")]
    [SerializeField]
    private RaceManager _raceManager;

    private float _zoom = 0.0f;
    private bool _isActivatedPlayerTracking = false;
    private Blur _blurEffect;
    //private BlurEffect _blurEffect;

    void Awake()
    {
        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
        _raceManager.RoutePreviewStart += OnRoutePreviewStart;
        _raceManager.RaceStart += OnRaceStart;

        _blurEffect = camera.GetComponent<Blur>();
        //_blurEffect = camera.GetComponent<BlurEffect>();

        //transform.localPosition = new Vector3(0, fallowTarget.transform.localPosition.y + offsetY, minZoomDistance);
    }

    public void Init(Vector3 startPosition)
    {
        transform.localPosition = new Vector3(startPosition.x, startPosition.y + offsetY, minZoomDistance);
    }

	void Start () 
    {
        camera.backgroundColor = StageDataManager.instance.GetStageDataByID(RaceModeInfo.stageId).bgColor;
    }

    void FixedUpdate()
    {
        UpdatePlayerFlow();
    }

    void OnDestroy()
    {
        _raceManager.RoutePreviewStart -= OnRoutePreviewStart;
        _raceManager.RaceStart -= OnRaceStart;
    }



    #region Private Functions
    private void OnRoutePreviewStart()
    {
        ChangePreviewMode();
    }

    private void OnRaceStart()
    {

    }
    #endregion

    private void ChangePreviewMode()
    {
        DeactivatePlayerTracking();

        _zoom = -previewZoom;
        transform.DOLocalMove(new Vector3(0, previewPosY, _zoom), previewZoomOutTime).SetEase(Ease.InOutCubic).OnComplete(OnCompletePreviewZoomOutMotion).SetUpdate(UpdateType.Fixed);
    }

    private void OnCompletePreviewZoomOutMotion()
    {
        if (PreviewZoomOutEnd != null) PreviewZoomOutEnd();
        PlayPreviewWaitingMotion();
    }

    private void PlayPreviewWaitingMotion()
    {
        Debug.Log("Map Preview를 위해 대기 중 입니다.");

        transform.DOLocalMove(transform.localPosition, 1.5f).OnComplete(OnCompletePreviewWaitingMotion);
    }

    private void OnCompletePreviewWaitingMotion()
    {
        PlayPreviewZoomInMotion();
    }

    private void PlayPreviewZoomInMotion()
    {
        Debug.Log("Game Play를 위해 Zoom In.");
        float time = 1.0f;
        _zoom = minZoomDistance;
        transform.DOLocalMove(new Vector3(fallowTarget.transform.localPosition.x, fallowTarget.transform.localPosition.y + offsetY, _zoom), time).SetEase(Ease.InOutCubic).OnComplete(OnCompletePreviewZoomInMotion).SetUpdate(UpdateType.Fixed);
    }

    private void OnCompletePreviewZoomInMotion()
    {
        _raceManager.EndRoutePreview();
        if (On_EndPreviewMotion != null) On_EndPreviewMotion();
    }



    // Activate / Deactivate Player Tracking
    public void ActivatePlayerTracking()
    {
        _isActivatedPlayerTracking = true;

        _customSpeed = 0;
    }

    public void DeactivatePlayerTracking()
    {
        _isActivatedPlayerTracking = false;
    }

    

    float _customSpeed = 0;
	public void UpdatePlayerFlow()
    {
        if (_isActivatedPlayerTracking)
        {
            //Debug.Log("_isActivatedPlayerTracking: " + _isActivatedPlayerTracking);
            PlayerManager playerManager = fallowTarget.GetComponent<PlayerManager>();
            AxisController axisController = fallowTarget.gameObject.GetComponent<AxisController>();

            float targetZoom = minZoomDistance + ((maxZoomDistance - minZoomDistance) * Mathf.Abs(playerManager.GetSpeed() * 10));

            if (playerManager.isBoost) targetZoom = boostZoomDistance;
            else
            {
                if (targetZoom <= maxZoomDistance) targetZoom = maxZoomDistance;
            }

            _zoom += (targetZoom - transform.position.z) * 0.05f;

            //Vector3 targetPos = new Vector3(playerTransform.position.x, playerTransform.position.y + offsetY, _zoom);

            _customSpeed += (playerManager.GetSpeedX() - _customSpeed) * DSpeed;

            Vector3 targetPos = new Vector3(fallowTarget.transform.localPosition.x + (_customSpeed * leftRightMove), fallowTarget.transform.localPosition.y + offsetY, _zoom);
            //Vector3 targetPos = new Vector3(playerTransform.position.x + (axisController.GetAxis() * 1), playerTransform.position.y + offsetY, _zoom);

            //transform.position = Vector3.Lerp(transform.position, targetPos, 2 * Time.deltaTime);

            Vector3 pos = transform.position;
            if(!_isWall) pos.x += (targetPos.x - pos.x) * DCameraMove;
            pos.y += (targetPos.y - pos.y) * 0.5f;
            pos.z += (targetPos.z - pos.z) * DCameraZoom;
            transform.position = pos;

            //Vector3 cubePos = cube.transform.position;
            //cubePos.x += (targetPos.x - cubePos.x) * DCameraMove;
            //cubePos.y += (targetPos.y - cubePos.y) * 0.5f;
            //cubePos.z = -4;
            //cube.transform.position = cubePos;
            //CheckRay(cubePos);
            //transform.position = targetPos;
        }
	}

    private bool _isWall = false;
    public void CheckRay(Vector3 pos)
    {
        Vector3 RayPos = new Vector3(pos.x, pos.y, pos.z);
        Ray ray = new Ray(RayPos, Vector3.forward * 5.0f);
        RaycastHit rayHit = new RaycastHit();

        //// Draw Ray
        Debug.DrawRay(RayPos, Vector3.forward * 5.0f);

        // Check On Floor
        if (Physics.Raycast(ray, out rayHit))
        {
            if(rayHit.collider.gameObject.tag == "Wall")
            {
                _isWall = true;
                return;
            }
        }
        _isWall = false;
    }

    public float GetZoom()
    {
        return transform.localPosition.z;
    }

    private void ChangeBackroundColor(Color color)
    {
        camera.backgroundColor = color;
    }
}
