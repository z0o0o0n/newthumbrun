﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class WaveBg : MonoBehaviour
{
    [SerializeField]
    private UITexture _texture;
    [SerializeField]
    private float _scrollingSpeedX;
    [SerializeField]
    private float _scrollingSpeedY;

    void Start ()
    {
	
	}
	
	void Update ()
    {
        Rect uvRect = _texture.uvRect;

        uvRect.x += _scrollingSpeedX;
        if (uvRect.x >= 1) uvRect.x = 0;
        else if (uvRect.x <= -1) uvRect.x = 0;

        uvRect.y += _scrollingSpeedY;
        if (uvRect.y >= 1) uvRect.y = 0;
        else if (uvRect.y <= -1) uvRect.y = 0;

        _texture.uvRect = uvRect;

        //DOTween.To(()=> _texture.uvRect.x, x=> _texture.uvRect.x=x, 1f, 1f);
	}
}
