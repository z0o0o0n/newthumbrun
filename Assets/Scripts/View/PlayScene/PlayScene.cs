﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms;
using Com.Mod.ThumbRun.Controller;
using Com.Mod.ThumbRun.Control;
using Com.Mod.ThumbRun.Race.Application;
using Com.Mod.ThumbRun.Setting.Application;

public class PlayScene : MonoBehaviour
{
    public delegate void GameManagerEventHandler();
    public delegate void RaceFinishEventHandler(bool isTimeout);
    public event GameManagerEventHandler On_GameStarted;
    public event GameManagerEventHandler RaceStarted;
    public event GameManagerEventHandler RacePause;
    public event GameManagerEventHandler RaceResume;
    public event RaceFinishEventHandler RaceFinished;

    [SerializeField]
    private PlayerManager _player;

    [Header("Elements")]
    public CameraController cameraController;
    public TerrainManager terrainManager;
    // public Replayer replayer;
    // public Replayer2 replayer2;
    public ReplayDataRecorder replayDataRecorder;
    public PlaySceneUI ui;
    [SerializeField]
    private StopwatchModel _stopWatchForGlobal;
    public IndicatorModel indicatorModel;
    public AudioSource _effectAudioSource;
    public AudioClip _timeOutAudio;
    public AudioClip _newRecordAudio;
    public VersusManager _versusManager;
    public Booster _booster;
    public BBBooster _bbBooster;

    [SerializeField]
    private StartCounter _startCounter;
    [SerializeField]
    private RaceParticipationManager _participationManager;
    [SerializeField]
    private GameObject _dimBg;
    [SerializeField]
    private BgmManager _bgmManager;
    [SerializeField]
    private PlayUI _playUI;
    [SerializeField]
    private EntryService _entryService;
    private SettingService _settingService;
    public AudioClip _bgmAudio0;
    public AudioClip _bgmAudio1;
    public AudioClip _bgmAudio2;

    private bool _isDebug = false;
    private string _logPrefix = "PlayScene - ";
    private int _laps = 1;
    private bool _isStartedGame = false;
    private bool _isPlaying = false;
    private bool _isTimeout = false;
    private bool _hasPersonalRecord = false;
    private GameManager _gameManager;
    private WinnerDataManager _winnerDataManager;
    private LuckyChance _luckyChance;
    private RaceManager _raceManager;
    private StageData _stageData;
    private StageData _currentStageData;
    private int _rankMovementCount = 0;

    void Awake()
    {
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        _winnerDataManager = GameObject.FindGameObjectWithTag("WinnerDataManager").GetComponent<WinnerDataManager>();
        _luckyChance = GameObject.FindGameObjectWithTag("LuckyChance").GetComponent<LuckyChance>();
        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();

        //if (_gameManager.isRandomMap) RaceModeInfo.stageId = Random.Range(0, 5);
        _stageData = StageDataManager.instance.GetStageDataByID(RaceModeInfo.stageId);

        ui.playMenuUI.HelpButtonClick += OnHelpButtonClick;
        ui.playMenuUI.ReplayButtonClick += OnRestartButtonClick;
        ui.playMenuUI.HomeButtonClick += OnHomeButtonClick;
        ui.indicator.SetActive(false);

        _settingService = GameObject.FindGameObjectWithTag("Setting").GetComponent<SettingService>();
        _settingService.Prepared += OnPrepared;
        if (_settingService.isPrepared) OnPrepared();

        if (_isDebug) Debug.Log("PlayScene.currentStageID: " + RaceModeInfo.stageId);
        terrainManager.Init(RaceModeInfo.stageId, _winnerDataManager.profileTexture, _winnerDataManager.nationTexture);
        terrainManager.GetCheckPointManager().CheckPointActivated += OnCheckPointActivated;
        terrainManager.GetCheckPointManager().Complete += OnOneRoundComplete;
        terrainManager.GetTerrain().SetBBBooster(_bbBooster);

        Vector2 playerStartPosition = terrainManager.GetCheckPointManager().GetStartPoint().transform.localPosition;
        _player.SetStartPoint(playerStartPosition);
        _player.On_WallJump += OnWallJump;

        indicatorModel.Init(GetCurrentCharacterData().id, GetCurrentCharacterData().bgColor);
        indicatorModel.SetStartPoint(terrainManager.GetCheckPointManager().GetStartPoint().gameObject);
        indicatorModel.SetCheckPoint(terrainManager.GetCheckPointManager().GetCheckPointByIndex(0).gameObject);
        indicatorModel.Show();

        cameraController.previewPosY = _stageData.previewPosY;
        cameraController.previewZoom = _stageData.previewZoom;
        cameraController.Init(playerStartPosition);
        //cameraController.ChangeBackroundColor(_stageData.bgColor);

        _raceManager.RaceOpen += OnRaceOpen;
        _raceManager.RacePrepare += OnRacePrepare;
        _raceManager.RoutePreviewStart += OnRoutePreviewStart;
        _raceManager.RoutePreviewEnd += OnRoutePreviewEnd;
        _raceManager.CountdownStart += OnCountdownStart;
        _raceManager.RaceStart += OnRaceStart;
        _raceManager.RacePause += OnRacePause;
        _raceManager.RaceResume += OnRaceResume;
        _raceManager.RaceEnd += OnRaceEnd;
        _raceManager.RaceTimeout += OnRaceTimeout;
        _raceManager.RaceClose += OnRaceClose;

        _startCounter.CountEnd += OnCountdownEnd;

        //if (_versusManager.hasVersus(_stageData)) _versusManager.Init(_gameManager.currentStageIndex);
        //GameManager.state = GameState.state.NONE;
    }

    void Start()
    {
        _dimBg.gameObject.SetActive(true);
        ui.indicator.SetActive(false);

        _gameManager.sceneLoader.ShutterOpened += OnShutterOpened;
        _gameManager.sceneLoader.OpenShutter(0.6f);
    }

    void OnApplicationPause()
    {
        if (_isPlaying) _raceManager.PauseRace();
        //GoHome();
    }

    void OnDestroy()
    {
        ui.playMenuUI.HelpButtonClick -= OnHelpButtonClick;
        ui.playMenuUI.ReplayButtonClick -= OnRestartButtonClick;
        ui.playMenuUI.HomeButtonClick -= OnHomeButtonClick;

        // _booster.BoostStarted -= OnStartBoost;
        // _booster.BoostEnded -= OnEndBoost;

        //ui.playTutorialBook.Closed -= OnTutorialBookEnded;
        ui.playTutorialBook.Closed -= OnTutorialBookClosed;

        //ui.resultUI.EndedRank -= OnResultRankEnded;
        //ui.resultUI.Ended -= OnResultGoldCountEnded;

        terrainManager.GetCheckPointManager().CheckPointActivated -= OnCheckPointActivated;
        terrainManager.GetCheckPointManager().Complete -= OnOneRoundComplete;

        _player.On_WallJump -= OnWallJump;

        //_versusManager.EndWinPopup -= OnEndWinPopup;
        //_versusManager.NextRunButtonClick -= OnNextRunButtonClick;
        //_versusManager.RetryButtonClick -= OnRetryButtonClick;
        //_versusManager.ClosedPopup -= OnClosedVersusPopup;

        _raceManager.RaceOpen -= OnRaceOpen;
        _raceManager.RacePrepare -= OnRacePrepare;
        _raceManager.RoutePreviewStart -= OnRoutePreviewStart;
        _raceManager.RoutePreviewEnd -= OnRoutePreviewEnd;
        _raceManager.CountdownStart -= OnCountdownStart;
        _raceManager.RaceStart -= OnRaceStart;
        _raceManager.RacePause -= OnRacePause;
        _raceManager.RaceResume -= OnRaceResume;
        _raceManager.RaceEnd -= OnRaceEnd;
        _raceManager.RaceTimeout -= OnRaceTimeout;
        _raceManager.RaceClose -= OnRaceClose;

        _startCounter.CountEnd -= OnCountdownEnd;
    }



    private void OnPrepared()
    {
        if (!_settingService.isPrepared) return;

        if (_settingService.IsBGMOn() == 1) _bgmManager.SetOnOff(true);
        else _bgmManager.SetOnOff(false);
    }

    private void OnShutterOpened()
    {
        _gameManager.sceneLoader.ShutterOpened -= OnShutterOpened;
        //GameManager.state = GameState.state.PLAY;
    }



    // 레이스 준비
    private void PrepareRace()
    {
        if (!_isStartedGame)
        {
            _isStartedGame = true;
            //CheckHasPersonalRecord();
            GameData.instance.GetConfigData().totalRaceCount += 1;
            _dimBg.gameObject.SetActive(false);
            indicatorModel.Show();
            // SponsorData sponsorData = GameData.instance.GetSponsorData();
            //GameManager.state = GameState.state.PLAY;

            if (On_GameStarted != null) On_GameStarted();
        }
    }


    //private void CheckHasPersonalRecord()
    //{
        //if (_stageData.personalRecord.record != 0) _hasPersonalRecord = true;
        //else _hasPersonalRecord = false;
    //}


    private void OnOneRoundComplete()
    {
        //ui.RecordCurrentTime();
        terrainManager.GetCheckPointManager().Reset();
        
        if (IsFinished())
        {
            Debug.Log("PlayScene - OnOneRoundComplete()");
            _raceManager.EndRound(_laps + 1);
            _raceManager.EndRace();
        }
        else
        {
            //ui.lapInfoNotificator.ShowLapCount(_laps, _playUI.GetRecordDisplay().GetRoundRecordList()[_laps - 1]);
            terrainManager.GetCheckPointManager().Play();
            _raceManager.EndRound(_laps + 1);
        }

        _laps += 1;
    }

    private void OnCheckPointActivated()
    {
        ui.lapInfoNotificator.ShowCheckPoint();
    }

    private bool IsFinished()
    {
        if (_laps >= 3) return true;
        else return false;
    }



    private void ShowOfflineResult()
    {
        _currentStageData = StageDataManager.instance.GetStageDataByID(RaceModeInfo.stageId);
        RecordPlayerData userData = new RecordPlayerData();
        userData.countryCode = GameData.instance.GetConfigData().CountryCode;
        //userData.record = _currentStageData.personalRecord.record;
        //ShowResultUI(false, true, null, null, userData, GetRivalData());

        //ReportPersonalRecord();
    }

    //private void ShowResultUI(bool isOnline, bool isSuccess, RecordPlayerData worldData, RecordPlayerData friendData, RecordPlayerData personalData, RecordPlayerData rivalData)
    //{
    //    //ui.resultUI.HideProgress();
    //    CalcRankMovementCount(worldData, friendData, personalData, rivalData);
        
    //    ResultData resultData = new ResultData();
    //    resultData.isSuccess = isSuccess;
    //    resultData.newScore = ui.GetTotalRecord();
    //    resultData.world = worldData;
    //    resultData.friend = friendData;
    //    resultData.personal = personalData;
    //    if (_versusManager.hasVersus(_stageData))
    //    {
    //        resultData.rival = rivalData;
    //        resultData.rivalCharacterID = _versusManager.GetVersusCharacterData().id;
    //    }
    //    resultData.rankMovementCount = _rankMovementCount;

    //    ui.resultUI.EndedRank += OnResultRankEnded;
    //    ui.resultUI.Ended += OnResultGoldCountEnded;
    //    ui.ShowResultRankUI(isOnline, resultData, GetNewRecordGold(), GetNewRecordGoldCountTime());
    //    //ui.resultUI.ShowRankDisplayer(isOnline, resultData);
    //}

  //  private void CalcRankMovementCount(RecordPlayerData worldData, RecordPlayerData friendData, RecordPlayerData personalData, RecordPlayerData rivalData)
  //  {
  //      // 내림차순 정렬을 위해 List화
  //      List<RecordPlayerData> list = new List<RecordPlayerData>();
  //      if(worldData == null || friendData == null) // Offline인 경우
  //      {
  //          list.Add(personalData);
  //          if (_versusManager.hasVersus(_stageData)) list.Add(rivalData);
  //      }
  //      else
  //      {
  //          //Debug.Log("world data: " + worldData.record);
  //          list.Add(worldData);
  //          //Debug.Log("friend data: " + friendData.record);
  //          list.Add(friendData);
  //          //Debug.Log("personal data: " + personalData.record);
  //          list.Add(personalData);
  //          //Debug.Log("rival data: " + rivalData);
  //          if(rivalData != null) list.Add(rivalData);
  //          //Debug.Log("----- 1");
  //      }

  //      //LogData(list);
  //      //Debug.Log("----- 2");
  //      list.Sort(CompareClass);

  //      _rankMovementCount = 0;
  //      for (int i = 0; i < list.Count; i++)
  //      {
  //          list[i].rank = i;
  //          if (list[i].record >= ui.GetTotalRecord()) _rankMovementCount++;
  //      }
  //      //LogData(list);

		//if(_rankMovementCount <= 2) return;

		//if (_rankMovementCount == list.Count)
  //      {
  //          #if UNITY_IOS
		//		//if(GameManager.isFreeVersion) GKAchievementReporter.ReportAchievement("WorldRecord_free", 100, true);
		//		//GKAchievementReporter.ReportAchievement("WorldRecord", 100, true);
  //          #elif UNITY_ANDROID
  //                  GKAchievementReporter.ReportAchievement(GPGSIds.achievement_world_record_holder, 100, true);
  //          #endif
  //      }

  //      //_rankMovementCount = 1;
  //      //if (friendData != null) if (friendData.record >= ui.GetTotalRecord()) _rankMovementCount = 2;
  //      //if (worldData != null) if (worldData.record >= ui.GetTotalRecord()) _rankMovementCount = 3;
  //  }

    private void LogData(List<RecordPlayerData> data)
    {
        Debug.Log("Log Data / data: " + data);
        string log = "========================= \n";
        for(int i = 0; i < data.Count; i++)
        {
            Debug.Log("Log Data / data[" + i + "]: " + data[i]);
            log += data[i].record + " \n";
        }
        log += "=========================";
        Debug.Log(log);
    }

    static int CompareClass(RecordPlayerData c1, RecordPlayerData c2)
    {
        if ((float)c1.record < (float)c2.record) return -1;
        if ((float)c1.record > (float)c2.record) return 1;
        return 0;
    }

    private int GetNewRecordGold()
    {
        int result = 0;
        if (_rankMovementCount == 1) result = 5000;
        else if (_rankMovementCount == 2) result = 10000;
        else if (_rankMovementCount == 3) result = 20000;
        else if (_rankMovementCount == 4) result = 100000;
        else result = 5000;

        //if(GameManager.isFreeVersion)
        //{
        //    result = result / 2;
        //}

        return result;
    }

    //private float GetNewRecordGoldCountTime()
    //{
    //    float result = 0.0f;
    //    if (_rankMovementCount == 1) result = 1;
    //    else if (_rankMovementCount == 2) result = 1.5f;
    //    else if (_rankMovementCount == 3) result = 2;
    //    else if (_rankMovementCount == 4) result = 3;
    //    return result;
    //}

    //private void OnResultRankEnded()
    //{
    //    ui.resultUI.EndedRank -= OnResultRankEnded;

    //    if (_versusManager.hasVersus(_stageData))
    //    {
    //        //ui.EndCompleteUI += OnEndCompleteUI;
    //        Invoke("ShowVersusWinPopup", 3.0f);
    //    }
    //    else ui.playMenuUI.ShowResultEndMenu(0.3f, true, false);
    //}

    //private void OnResultGoldCountEnded()
    //{
    //    ui.resultUI.Ended -= OnResultGoldCountEnded;
    //}

    //private void ShowVersusWinPopup()
    //{
    //    //ui.EndCompleteUI -= OnEndCompleteUI;
    //    if (_versusManager.hasVersus(_stageData)) _versusManager.CountVersusStep();

    //    _versusManager.EndWinPopup += OnEndWinPopup;
    //    _versusManager.NextRunButtonClick += OnNextRunButtonClick;
    //    _versusManager.ChangeWinView();
    //    _versusManager.ShowVersusPopup();
    //}

    //private void OnEndWinPopup()
    //{
    //    _versusManager.EndWinPopup -= OnEndWinPopup;
    //    //ui.goldDisplay.Save(_versusManager.GetVersusData().rewardGold);

    //    ui.playMenuUI.ShowShareReplayButton(0.3f);
    //    ui.playMenuUI.ShowResultEndMenu(0.3f, false, false);

    //    //GameManager.state = GameState.state.PLAY;
    //}

    //private void OnNextRunButtonClick()
    //{
    //    if (_versusManager.hasVersus(_stageData))
    //    {
    //        _versusManager.NextRunButtonClick -= OnNextRunButtonClick;
    //        //_versusManager.ClosedPopup += OnClosedVersusPopup;
    //        _versusManager.HideVersusPopup();
    //    }
    //    else
    //    {
    //        ui.playMenuUI.SetButtonActivation(false);
            
    //        // if (GameManager.isFreeVersion) ShowDefaultAD();
    //        // else RestartGame();
    //        Debug.Log("****** OnNextRunButtonClick");
    //        RestartGame();
    //    }
    //}

    // On Race Open
    private void OnRaceOpen()
    {
        Reset();
    }

    private void OnRacePrepare()
    {
        PrepareRace();
        ui.indicator.SetActive(true);
    }

    // On Route Preview Start
    private void OnRoutePreviewStart()
    {
        _bgmManager.Play(int.Parse(RaceModeInfo.stageId));
    }

    // On Route Preview End
    private void OnRoutePreviewEnd()
    {
    }

    // On Countdown Start
    private void OnCountdownStart()
    {
        ui.playerControlButton.Activate();
        _startCounter.StartCount();
    }

    // On Start Count End
    private void OnCountdownEnd()
    {
        _raceManager.EndCountdown();
    }

    // On Race Start
    private void OnRaceStart()
    {
        _isPlaying = true;
        _player.StartRace();
        ui.ShowPlayUI();
        ui.playMenuUI.SetButtonActivation(true);
        terrainManager.GetCheckPointManager().Play();
        cameraController.ActivatePlayerTracking();
        _stopWatchForGlobal.Play();
        // replayer.gameObject.SetActive(true);

        // if (_versusManager.hasVersus(_stageData)) replayer.Replay(_versusManager.GetVersusData().replayData);
        // else replayer.Replay(_stageData.personalRecord.replayData);

        if (RaceStarted != null) RaceStarted();

        //GameManager.state = GameState.state.PLAING;
    }

    // On Race Pause
    private void OnRacePause()
    {
        Time.timeScale = 0;
        _dimBg.gameObject.SetActive(true);
        ui.ShowPauseUI();
        indicatorModel.Hide();
        _bgmManager.Pause();

        if (RacePause != null) RacePause();
    }

    // On Race Resume
    private void OnRaceResume()
    {
        Time.timeScale = 1;
        _dimBg.gameObject.SetActive(false);
        ui.ShowPlayUI();
        indicatorModel.Show();
        _bgmManager.Play(int.Parse(RaceModeInfo.stageId));

        //GameManager.state = GameState.state.PLAING;

        if (RaceResume != null) RaceResume();
    }

    // On Race End
    private void OnRaceEnd()
    {
        if (!_isTimeout)
        {
            _isPlaying = false;
            _isStartedGame = false;

            indicatorModel.Hide();
            _bgmManager.Stop();

            // audio
            _effectAudioSource.clip = _newRecordAudio;
            _effectAudioSource.Play();

            //GameManager.state = GameState.state.NONE;
            _luckyChance.data.AddRaceOutcome(true);

            if (RaceFinished != null) RaceFinished(_isTimeout);
        }
    }

    private void OnRaceTimeout()
    {
        _isTimeout = true;
        _isPlaying = false;
        _isStartedGame = false;

        indicatorModel.Hide();
        _bgmManager.Stop();

        // audio
        _effectAudioSource.clip = _timeOutAudio;
        _effectAudioSource.Play();

        //GameManager.state = GameState.state.;
        _luckyChance.data.AddRaceOutcome(false);

        if (RaceFinished != null) RaceFinished(_isTimeout);
    }

    private void OnRaceClose()
    {
        _stopWatchForGlobal.Stop();
    }

    // Restart Race
    private void RestartGame()
    {
        if (_isDebug) Debug.Log("PlayScene - RestartGame()");

        Reset();
        PrepareRace();
    }

    // Reset Race
    private void Reset()
    {
        Time.timeScale = 1;
        _laps = 1;
        _isTimeout = false;
        _isPlaying = false;
        _isStartedGame = false;

        terrainManager.GetCheckPointManager().Reset();

        ui.Reset();
        ui.playMenuUI.HideAllMenus(0.2f);

        _startCounter.Reset();

        _player.StopMove();
        _player.Reset();
        if (_isDebug) Debug.Log("---- player pos: " + transform.position.ToString());

        _bgmManager.Stop();
    }

    private void OnEndTimeoutUI()
    {
        ui.EndTimeoutUI -= OnEndTimeoutUI;
        _versusManager.RetryButtonClick += OnRetryButtonClick;
        _versusManager.ChangeVSRetryView();
        _versusManager.ShowVersusPopup();
    }

    private void OnRetryButtonClick()
    {
        _versusManager.RetryButtonClick -= OnRetryButtonClick;
        _versusManager.HideVersusPopup();
        ui.playMenuUI.SetButtonActivation(false);
        RestartGame();
    }

    private int GetTimeOutGold()
    {
        int result = 0;
        if (_laps == 1) result = 300;
        else if (_laps == 2) result = 600;
        else if (_laps == 3) result = 900;

        //if (GameManager.isFreeVersion)
        //{
        //    result = result / 2;
        //}

        return result;
    }

    private float GetTimeOutGoldCountTime()
    {
        float result = 0.0f;
        //if (GameManager.isFreeVersion)
        //{
        //    result = (float)GetTimeOutGold() / (900 / 2);
        //}
        //else
        //{
            result = (float)GetTimeOutGold() / 900;
        //}
        return result;
    }

    private CharacterData GetCurrentCharacterData()
    {
        return CharacterDataManager.instance.GetCharacterDataByID(GetMainCharacterID());
    }

    private int GetMainCharacterID()
    {
        return GameData.instance.GetConfigData().SelectedCharacterID;
    }

    //private bool CheckRenewPersonalRecord()
    //{
    //    if (_stageData.personalRecord.record > ui.GetTotalRecord()) return true;
    //    else return false;
    //}



    private void OnWallJump(float direction, Vector3 position)
    {
        terrainManager.AttachWallJumpParticle(position);
    }




    // Report Personal Record
    // GC에 먼저 제출 후 GD에 제출해야 함.
    //private void ReportPersonalRecord()
    //{
    //    if (_isDebug) Debug.Log("PlayScene - ReportPersonalRecord() / personalRecord: " + _stageData.personalRecord.record + " / currentRecord: " + ui.GetTotalRecord());
    //    if (_stageData.personalRecord.record > ui.GetTotalRecord())
    //    {
    //        ReportPersonalRecordToGD();

    //        _stageData.hasPersonalRecord = true;
    //    }
    //}

    //void ReportPersonalRecordToGD()
    //{
    //    if (_isDebug) Debug.Log("PlayScene - ReportPersonalRecordToGD() / stage id: " + _currentStageData.id + " / record: " + ui.GetTotalRecord());
    //    ReplayData replayData = new ReplayData(replayDataRecorder.GetReplayData().stateHash, replayDataRecorder.GetReplayData().trailPosition);
    //    _stageData.personalRecord = new PersonalRecord(ui.GetTotalRecord(), replayData, GetCurrentCharacterData().id);
    //}



    //private void UpdateCharacterEXP()
    //{
    //    GetCurrentCharacterData().exp = GetCurrentCharacterData().exp + _stageData.exp;
    //}













    // private void OnStartBoost()
    // {
    //     _player.StartBoost(0.5f);
    // }

    // private void OnEndBoost()
    // {
    //     _player.EndBoost(0.5f);
    // }

    private void OnHelpButtonClick()
    {
        ui.playMenuUI.SetButtonActivation(false);

        ui.playMenuUI.HideAllMenus(0.3f);
        ui.playTutorialBook.Closed += OnTutorialBookClosed;
        ui.playTutorialBook.Show();
    }

    private void OnTutorialBookClosed()
    {
        ui.playMenuUI.SetButtonActivation(true);

        ui.playTutorialBook.Closed -= OnTutorialBookClosed;
        ui.playMenuUI.ShowPauseMenu(0.3f, true, false);
    }

    private void OnRestartButtonClick()
    {
        ui.playMenuUI.SetButtonActivation(false);
        RestartGame();
    }

    private void OnHomeButtonClick()
    {
        ui.SetButtonActive(false);
        RaceModeInfo.stageId = null;
        _gameManager.sceneLoader.Load(SceneName.MAIN);
        Time.timeScale = 1;
    }

    //private RecordPlayerData GetRivalData()
    //{
    //    if(_isDebug) Debug.Log("has Rival Data: " + _versusManager.hasVersus(_stageData));
    //    if (!_versusManager.hasVersus(_stageData)) return null;
    //    RecordPlayerData data = new RecordPlayerData();
    //    data.countryCode = "001";
    //    data.record = _versusManager.GetVersusData().record;
    //    data.profilePhoto = _versusManager.GetVersusCharacterData().profileImage;
    //    return data;
    //}

    public void GoHome()
    {
        OnHomeButtonClick();
    }

    public void ReloadPlayScene()
    {
        if (IsRandomStage())
        {
            _entryService.SendEntry(0);
            RaceModeInfo.stageId = _entryService.GetRandomStageId();
        }
        
        _gameManager.sceneLoader.Load(SceneName.PLAY);
    }

    private bool IsRandomStage()
    {
        return (RaceModeInfo.entryIndex == 0) ? true : false;
    }
}
