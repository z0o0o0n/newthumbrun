﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Junhee.Utils;

public class BoostBtn2 : MonoBehaviour 
{
    public delegate void BoostEvent();
    public event BoostEvent On_StartBoost;
    public event BoostEvent On_EndBoost;

    public UIButton onButton;
    public UISprite pressButton;
    public UISprite gauge;
    public UISprite fullMotionClip;
    //public UISprite gaugeBg;

    public float useableGaugeLimit = 0.0f;
    public float chargingSpeed = 0.0f;
    public float consumptionSpeed = 0.0f;
    public AudioSource audioSource;
    public AudioClip boostFullSound;

    private bool _isActivated = false;
    private bool _isBoosting = false;
    private bool _isUseable = false;
    private float _currentGaugeNum;

    public float gaugeNum
    {
        get
        {
            return _currentGaugeNum;
        }
        set
        {
            _currentGaugeNum = value;
            gauge.fillAmount = _currentGaugeNum;
        }
    }

    void Awake ()
    {
        Hide(0);
        Reset();
    }

	void Start () 
	{
        
	}

    public void Reset()
    {
        gaugeNum = 0;
        DeactivateBtn();

        StopFullMotion();
    }

    public void Activate()
    {
        _isActivated = true;
    }

    public void Deacitvate()
    {
        _isActivated = false;
    }
	
	void Update () 
	{
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (_isUseable) Boost();
        }
	}

    void FixedUpdate()
    {
        if (_isActivated)
        {
            if (_isBoosting) Consume();
            else Charge();

            if (gaugeNum >= 1) gaugeNum = 1;
            if (gaugeNum <= 0) gaugeNum = 0;

            CheckBoostEnd();
            CheckUseable();
        }
    }

    private void Consume()
    {
        gaugeNum -= consumptionSpeed;
    }

    private void Charge()
    {
        gaugeNum += chargingSpeed;
    }

    private void CheckBoostEnd()
    {
        if (gaugeNum <= 0 && _isBoosting)
        {
            _isBoosting = false;
            EndBoost();
        }
    }

    private void EndBoost()
    {
        if (On_EndBoost != null) On_EndBoost();
    }

    private void CheckUseable()
    {
        if (!_isBoosting)
        {
            if (gaugeNum >= useableGaugeLimit)
            {
                if (!_isUseable)
                {
                    _isUseable = true;
                    ActivateBtn();

                    PlayFullMotion();

                    audioSource.clip = boostFullSound;
                    audioSource.Play();
                }
            }
            else
            {
                if (_isUseable)
                {
                    _isUseable = false;
                }
            }
        }
    }

    public void OnPressBoostBtn()
    {
        Debug.Log("---------------------------boost");
        Boost();
    }

    private void Boost()
    {
        if (!_isBoosting)
        {
            _isBoosting = true;
            StopFullMotion();
            DeactivateBtn();

            if (On_StartBoost != null) On_StartBoost();
        }
    }

    public void Show()
    {
        DOTween.Kill("boostButton");
        transform.DOScale(new Vector3(1, 1, 1), 0.5f).SetId("boostButton").SetEase(Ease.OutBack);
    }

    public void Hide(float time = 0.3f)
    {
        DOTween.Kill("boostButton");
        transform.DOScale(new Vector3(0, 0, 1), time).SetId("boostButton").SetEase(Ease.InBack).SetUpdate(true);
    }

    private void PlayFullMotion()
    {
        fullMotionClip.transform.localScale = Vector3.zero;
        fullMotionClip.alpha = 1f;

        float time = 1f;
        fullMotionClip.transform.DOScale(2f, time).SetId("boostFullMotion_Scale").SetLoops(-1);
        DOTween.To(() => fullMotionClip.alpha, x => fullMotionClip.alpha = x, 0, time).SetId("boostFullMotion_Alpha").SetLoops(-1).SetEase(Ease.OutQuad);
    }

    private void StopFullMotion()
    {
        DOTween.Kill("boostFullMotion_Scale");
        DOTween.Kill("boostFullMotion_Alpha");

        fullMotionClip.transform.localScale = Vector3.zero;
        fullMotionClip.alpha = 1f;
    }

    private void ChangeBtnOn()
    {
        //Debug.Log("------ change btn on");
        onButton.gameObject.SetActive(true);
        pressButton.gameObject.SetActive(false);
        gauge.transform.DOLocalMoveY(16, 0);
        gauge.color = ColorUtil.ConvertHexToColor(0xB55757);
        //gaugeBg.transform.DOLocalMoveY(16, 0);
    }

    private void ChangeBtnPress()
    {
        //Debug.Log("------ change btn press");
        onButton.gameObject.SetActive(false);
        pressButton.gameObject.SetActive(true);
        gauge.transform.DOLocalMoveY(-5, 0);
        gauge.color = ColorUtil.ConvertHexToColor(0xaaaaaa);
        //gaugeBg.transform.DOLocalMoveY(0, 0);
    }

    private void ActivateBtn()
    {
        ChangeBtnOn();
    }

    private void DeactivateBtn()
    {
        ChangeBtnPress();
    }
}
