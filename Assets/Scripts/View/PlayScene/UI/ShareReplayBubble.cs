﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ShareReplayBubble : MonoBehaviour 
{
    public UISprite text;
    public UISprite bg;
    public AnimationCurve customElastic;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void Show(float time)
    {
        DOTween.Kill("ShareReplayBubble_Text_Scale");
        DOTween.Kill("ShareReplayBubble_Bg_Scale");

        text.transform.DOScale(1, time).SetId("ShareReplayBubble_Text_Scale").SetDelay(0.1f).SetEase(customElastic);
        bg.transform.DOScale(1, time).SetId("ShareReplayBubble_Bg_Scale").SetEase(customElastic);
    }

    public void Hide(float time)
    {
        DOTween.Kill("ShareReplayBubble_Text_Scale");
        DOTween.Kill("ShareReplayBubble_Bg_Scale");
        
        if(time == 0)
        {
            text.transform.localScale = Vector3.zero;
            bg.transform.localScale = Vector3.zero;
        }
        else
        {
            text.transform.DOScale(0, time).SetId("ShareReplayBubble_Text_Scale").SetEase(Ease.InBack);
            bg.transform.DOScale(0, time).SetId("ShareReplayBubble_Bg_Scale").SetDelay(0.1f).SetEase(Ease.InBack);
        }
    }
}
