﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.GUI;
using Com.Mod.ThumbRun.Race.View;

public class PlayUI : MonoBehaviour
{
    private RaceManager _raceManager;

    [SerializeField]
    private PlayerControlButton _playerControlButton;
    [SerializeField]
    private RecordDisplay _recordDisplay;
    [SerializeField]
    private ItemButton _itemButton;
    [SerializeField]
    private StartCounter _startCounter;
    [SerializeField]
    private UIBasicButton _homeButton;
    [SerializeField]
    private DropOutPopup _dropOutPopup;
    // [SerializeField]
    // private GameObject _autoShieldIconContainer;

    private void Awake()
    {
        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
        _raceManager.RacePause += OnRacePause;
        _raceManager.RaceResume += OnRaceResume;
        _raceManager.RaceTimeout += OnRaceTimeout;
    }

    void Start ()
    {
        
    }
	
	void Update ()
    {
	
	}

    private void OnDestroy()
    {
        _raceManager.RacePause -= OnRacePause;
        _raceManager.RaceResume -= OnRaceResume;
        _raceManager.RaceTimeout -= OnRaceTimeout;
    }



    private void OnRacePause()
    {
        HidePlayUI();
    }

    private void OnRaceResume()
    {
        ShowPlayUI();
    }

    private void OnRaceTimeout()
    {
        HidePlayUI();
    }

	public void OnHomeButtonClick()
	{
		_dropOutPopup.ShowPopup();
	}



	public void ShowPlayUI()
    {
        // _autoShieldIconContainer.transform.parent = transform;

        //Debug.Log(">>>>>>>>>>>>>>>>>>>>>>>>>> Show Play UI");

        _recordDisplay.Show();
        _itemButton.Show();
        _playerControlButton.Activate();
        _homeButton.gameObject.SetActive(true);
    }

    private void HidePlayUI()
    {
        _recordDisplay.Hide(0);
        _itemButton.Hide();
        _playerControlButton.Deactivate();
        _homeButton.gameObject.SetActive(false);
    }

    public void Reset()
    {
        _recordDisplay.Reset();
        _homeButton.gameObject.SetActive(false);
    }

    public RecordDisplay GetRecordDisplay()
    {
        return _recordDisplay;
    }
}
