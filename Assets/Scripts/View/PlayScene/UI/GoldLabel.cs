﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GoldLabel : MonoBehaviour 
{
    public delegate void GoldLabelEvent();
    public event GoldLabelEvent Ended;

    public UISprite _icon;
    public UILabel _label;
    public AnimationCurve _customElastic;
    public AudioClip _goldSound;

    private int _currentCountNum = 0;
    private AudioSource _audioSource;

    void Awake ()
    {
        _audioSource = GetComponent<AudioSource>();
    }

	void Start () 
	{
        Hide(0);
        //Count(50000);
        //Replace();
	}
	
	void Update () 
	{
	
	}

    public void SetValue(int value)
    {
        _label.text = value.ToString();
        Replace();
    }

    public void Show(float time)
    {
        transform.DOScale(1, time).SetEase(_customElastic);
    }

    public void Hide(float time)
    {
        transform.DOScale(0, time).SetEase(Ease.InBack);
    }

    public void Count(int value, float time)
    {
        _currentCountNum = 0;
        DOTween.To(() => _currentCountNum, x => _currentCountNum = x, value, time).OnUpdate(OnCountUpdate).OnComplete(OnCountCompleted);

        _audioSource.clip = _goldSound;
        _audioSource.Play();
    }

    private void OnCountUpdate()
    {
        _label.text = _currentCountNum.ToString();
        Replace();
    }

    private void OnCountCompleted()
    {
        _audioSource.Stop();
        if (Ended != null) Ended();
    }

    private void Replace()
    {
        float space = 10;
        float totalWidth = _icon.width + _label.width + space;

        Vector3 iconPos = _icon.transform.localPosition;
        iconPos.x = -(totalWidth / 2) + (_icon.width / 2);
        _icon.transform.localPosition = iconPos;
        //_icon.transform.DOLocalMoveX(-(totalWidth / 2) + (_icon.width / 2), 0);

        Vector3 labelPos = _label.transform.localPosition;
        labelPos.x = _icon.transform.localPosition.x + ((_icon.width / 2) + space);
        _label.transform.localPosition = labelPos;
        //_label.transform.DOLocalMoveX(_icon.transform.localPosition.x + (_icon.width / 2) + space, 0);
    }
}
