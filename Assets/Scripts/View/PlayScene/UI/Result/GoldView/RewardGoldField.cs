﻿using UnityEngine;
using System.Collections;
using System;

public class RewardGoldField : MonoBehaviour 
{
    public UILabel label;
    public float startPosX = 0.0f;

    private int _value;

    void Awake()
    {
        startPosX = transform.localPosition.x;
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void SetValue(int value)
    {
        _value = value;
        if (value == 0)
        {
            label.text = "0";
        }
        else
        {
            label.text = String.Format("{0:#,#}", value);
        }
    }

    public int GetValue()
    {
        return _value;
    }
}
