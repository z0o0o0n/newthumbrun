﻿using UnityEngine;
using System.Collections;
using System;

public class TotalGoldField : MonoBehaviour 
{
    public UISprite icon;
    public UILabel label;

    private int _value;

    void Awake()
    {
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void SetValue(int value)
    {
        _value = value;
        if(value == 0)
        {
            label.text = "0";
        }
        else
        {
            label.text = String.Format("{0:#,#}", value);
        }
        Replace();
    }

    public int GetValue()
    {
        return _value;
    }

    private void Replace()
    {
        Vector2 iconPos = icon.transform.localPosition;
        iconPos.x = -(GetWidth() / 2);
        icon.transform.localPosition = iconPos;

        Vector2 labelPos = label.transform.localPosition;
        labelPos.x = icon.transform.localPosition.x + icon.width + 5;
        label.transform.localPosition = labelPos;

        Vector2 thisPos = transform.localPosition;
        thisPos.x = (GetWidth() / 2);
        transform.localPosition = thisPos;
    }

    private float GetWidth()
    {
        return icon.width + 5 + label.width;
    }
}
