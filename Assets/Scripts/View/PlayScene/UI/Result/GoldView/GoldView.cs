﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GoldView : MonoBehaviour 
{
    public RewardGoldField rewardGoldField;
    public NewRecordBonusGoldField newRecordBonusGoldField;
    public SponserBonusField sponserBonusField;
    public TotalGoldField totalGoldField;
    public AnimationCurve customElastic1;
    public AnimationCurve customElastic2;
    public bool hasNewRecordBonusGold = false;
    public bool hasSponserBonus = false;

    private UIWidget _thisWidget;
    private int _totalGold = 0;
    private float _startPosY = 0.0f;

    void Awake()
    {
        _thisWidget = GetComponent<UIWidget>();
        _startPosY = transform.localPosition.y;
        Hide();
    }

	void Start () 
	{
	
	}

    public void Init(int rewardGold, int newRecordGold, int sponsorBonus)
    {
        Hide();
        Reset();

        rewardGoldField.SetValue(rewardGold);

        if (newRecordGold == 0) hasNewRecordBonusGold = false;
        else hasNewRecordBonusGold = true;
        newRecordBonusGoldField.SetValue(newRecordGold);

        if (sponsorBonus == 0) hasSponserBonus = false;
        else hasSponserBonus = true;
        sponserBonusField.SetValue(sponsorBonus);
    }

	void Update () 
	{
	
	}

    public void Show(float time = 0.0f)
    {
        //if(_thisWidget)
        //{
            DOTween.To(() => _thisWidget.alpha, x => _thisWidget.alpha = x, 1, time);
            _thisWidget.transform.DOLocalMoveY(_startPosY, time).SetEase(customElastic1);
        //}
    }

    public void Hide(float time = 0.0f)
    {
        //if (_thisWidget)
        //{
            DOTween.To(() => _thisWidget.alpha, x => _thisWidget.alpha = x, 0, time);
            _thisWidget.transform.DOLocalMoveY(_startPosY - 70, time).SetEase(Ease.InBack);
        //}
    }

    public void ShowTotalGold(float time = 0.0f)
    {
        CalculateTotalGold();

        totalGoldField.transform.DOScale(1, time).SetEase(customElastic2);
        UIWidget widget = totalGoldField.GetComponent<UIWidget>();
        DOTween.To(() => widget.alpha, x => widget.alpha = x, 1, time).SetEase(Ease.OutCubic);
    }

    private void CalculateTotalGold()
    {
        _totalGold = 0;
        _totalGold += rewardGoldField.GetValue();

        if(hasNewRecordBonusGold)
        {
            _totalGold += newRecordBonusGoldField.GetValue();
        }

        if(hasSponserBonus)
        {
            _totalGold = _totalGold * sponserBonusField.GetValue();
        }

        totalGoldField.SetValue(_totalGold);
    }

    public void Reset()
    {
        ResetRewardBonus();
        ResetNewRecordBonus();
        ResetSponserBonus();
        HideTotalGold();
    }

    private void ResetRewardBonus()
    {
        Vector3 pos = rewardGoldField.transform.localPosition;
        pos.x = rewardGoldField.startPosX + 50;
        rewardGoldField.transform.localPosition = pos;

        UIWidget widget = rewardGoldField.GetComponent<UIWidget>();
        widget.alpha = 0;

        rewardGoldField.gameObject.SetActive(false);
    }

    private void ResetNewRecordBonus()
    {
        hasNewRecordBonusGold = false;

        Vector3 pos = newRecordBonusGoldField.transform.localPosition;
        pos.x = newRecordBonusGoldField.startPosX + 50;
        newRecordBonusGoldField.transform.localPosition = pos;

        UIWidget widget = newRecordBonusGoldField.GetComponent<UIWidget>();
        widget.alpha = 0;

        newRecordBonusGoldField.gameObject.SetActive(false);
    }

    private void ResetSponserBonus()
    {
        hasSponserBonus = false;

        Vector3 pos = sponserBonusField.transform.localPosition;
        pos.x = sponserBonusField.startPosX + 50;
        sponserBonusField.transform.localPosition = pos;

        UIWidget widget = sponserBonusField.GetComponent<UIWidget>();
        widget.alpha = 0;

        sponserBonusField.gameObject.SetActive(false);
    }

    private void HideTotalGold(float time = 0.0f)
    {
        totalGoldField.transform.DOScale(2, time).SetEase(customElastic2);
        UIWidget widget = totalGoldField.GetComponent<UIWidget>();
        DOTween.To(() => widget.alpha, x => widget.alpha = x, 0, time).SetEase(Ease.OutCubic);
    }

    public void ShowRewardBonus()
    {
        rewardGoldField.gameObject.SetActive(true);
        rewardGoldField.transform.DOLocalMoveX(rewardGoldField.startPosX, 0.3f).SetEase(customElastic2);

        UIWidget widget = rewardGoldField.GetComponent<UIWidget>();
        DOTween.To(() => widget.alpha, x => widget.alpha = x, 1, 0.3f).SetEase(Ease.OutCubic);
    }

    public void ShowNewRecordBonus()
    {
        newRecordBonusGoldField.gameObject.SetActive(true);
        newRecordBonusGoldField.transform.DOLocalMoveX(newRecordBonusGoldField.startPosX, 0.3f).SetEase(customElastic2);

        UIWidget widget = newRecordBonusGoldField.GetComponent<UIWidget>();
        DOTween.To(() => widget.alpha, x => widget.alpha = x, 1, 0.3f).SetEase(Ease.OutCubic);
    }

    public void ShowSponserBonus()
    {
        sponserBonusField.gameObject.SetActive(true);
        sponserBonusField.transform.DOLocalMoveX(sponserBonusField.startPosX, 0.3f).SetEase(customElastic2);

        UIWidget widget = sponserBonusField.GetComponent<UIWidget>();
        DOTween.To(() => widget.alpha, x => widget.alpha = x, 1, 0.3f).SetEase(Ease.OutCubic);
    }
}
