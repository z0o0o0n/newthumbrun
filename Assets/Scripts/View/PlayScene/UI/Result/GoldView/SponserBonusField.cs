﻿using UnityEngine;
using System.Collections;
using System;

public class SponserBonusField : MonoBehaviour 
{
    public UILabel label;
    public float startPosX = 0.0f;

    private int _value;

    void Awake()
    {
        startPosX = transform.localPosition.x;
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void SetValue(int value)
    {
        _value = value;
        label.text = value.ToString();
    }

    public int GetValue()
    {
        return _value;
    }
}
