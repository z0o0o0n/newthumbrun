﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class RecordView : MonoBehaviour 
{
    public UILabel[] recordLabelList;
    public UILabel totalRecordLabel;
    public AnimationCurve customElastic1;
    public AnimationCurve customElastic2;

    private float _startPosY = 0.0f;
    private float _totalRecrodLabelStartPosX = 0.0f;
    private float _totlaRecord = 0.0f;

    void Awake()
    {
        _startPosY = transform.localPosition.y;
        _totalRecrodLabelStartPosX = totalRecordLabel.transform.localPosition.y;
    }

	void Start () 
	{
        Hide(0);
        ResetTotalRecord();
	}

    private void ResetTotalRecord()
    {
        totalRecordLabel.alpha = 0;
        totalRecordLabel.transform.localScale = new Vector3(2f, 2f, 2f);
    }

	void Update () 
	{
	
	}

    public void Init(float[] roundRecordList, bool isSuccess)
    {
        Hide(0);

        SetRoundRecordList(roundRecordList, isSuccess);
    }

    private void SetRoundRecordList(float[] roundRecordList, bool isSuccess)
    {
        _totlaRecord = 0.0f;
        for(int i = 0; i < recordLabelList.Length; i++)
        {
            recordLabelList[i].text = ScoreFormat.ConvertString(roundRecordList[i]);
            _totlaRecord += roundRecordList[i];
        }

        _totlaRecord = (float) Math.Round(_totlaRecord, 3);
        totalRecordLabel.text = ScoreFormat.ConvertString(_totlaRecord);
        ReplaceTotalRecordLabel();

        if (!isSuccess)
        {
            ChangeZeroToHyphen(roundRecordList);
        }
    }

    private void ChangeZeroToHyphen(float[] roundRecordList)
    {
        for (int i = 0; i < recordLabelList.Length; i++)
        {
            if (roundRecordList[i] == 0) recordLabelList[i].text = "--.---";
            totalRecordLabel.text = "--.---";
        }
        ReplaceTotalRecordLabel();
    }

    private void ReplaceTotalRecordLabel()
    {
        Vector3 pos = totalRecordLabel.transform.localPosition;
        pos.x = (totalRecordLabel.width / 2);
        totalRecordLabel.transform.localPosition = pos;
    }

    public void Show(float time = 0)
    {
        UIWidget thisWidget = GetComponent<UIWidget>();
        DOTween.To(() => thisWidget.alpha, x => thisWidget.alpha = x, 1, time).SetId("RecordViewAlphaTween");
        thisWidget.transform.DOLocalMoveY(_startPosY, time).SetId("ProfileTweenY").SetEase(customElastic1);
    }

    public void Hide(float time = 0)
    {
        UIWidget thisWidget = GetComponent<UIWidget>();
        DOTween.To(() => thisWidget.alpha, x => thisWidget.alpha = x, 0, time).SetId("RecordViewAlphaTween");
        thisWidget.transform.DOLocalMoveY(_startPosY-70, time).SetId("ProfileTweenY").SetEase(Ease.InBack);
    }

    public void ShowTotalRecord()
    {
        totalRecordLabel.transform.DOScale(1, 0.3f).SetEase(customElastic2);
        DOTween.To(() => totalRecordLabel.alpha, x => totalRecordLabel.alpha = x, 1, 0.3f).SetEase(Ease.OutCubic);
    }

    public float GetTotalRecrod()
    {
        return _totlaRecord;
    }
}
