﻿using UnityEngine;
using System.Collections;

public class ResultUITitle : MonoBehaviour 
{
    public UISprite _icon;
    public UILabel _title;

    void Awake ()
    {
        Hide(0);
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void Show(float time)
    {
        _icon.gameObject.SetActive(true);
        _title.gameObject.SetActive(true);
    }

    public void Hide(float time)
    {
        _icon.gameObject.SetActive(false);
        _title.gameObject.SetActive(false);
    }
}
