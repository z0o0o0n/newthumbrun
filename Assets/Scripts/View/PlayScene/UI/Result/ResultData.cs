﻿using UnityEngine;
using System.Collections;

public class ResultData : MonoBehaviour 
{
    //public int characterID;
    //public Color characterBgColor;
    //public string characterName;
    //public int currentEXP;
    //public int rewardEXP;

    public bool isSuccess;
    public RecordPlayerData world;
    public RecordPlayerData friend;
    public RecordPlayerData personal;
    public RecordPlayerData rival;
    public int rivalCharacterID;
    public float newScore;
    public int rankMovementCount;
    //public float[] roundRecordList;

    //public int rewardGold;
    //public int newRecordGold;
    //public int sponsorBonus;
    //public int totalGold;

    //public float worldRecord;
    //public bool isRenewWorldRecord;
    //public float personalRecord;
    //public bool isRenewPersonalRecord;
    //public float friendRecord;
    //public bool isRenewFriendRecord;
}
