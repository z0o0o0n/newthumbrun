﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class RankRow : MonoBehaviour 
{
	public UIWidget _widget;

	[Header("+ Icon")]
    public UISprite _icon;

	[Header("+ Nation Flag")]
	public UIPanel _nationPanel;
    public UISprite _nation;

	[Header("+ Profile")]
	public UIPanel _profilePanel;
    public UITexture _profile;
	public UISprite _profileBg;
	public Texture2D _profileMaskForAndroid;
    
	[Header("+ Score")]
	public UILabel _score;

    void Awake ()
    {
    }

	void Start ()
	{
		#if UNITY_ANDROID
		HideNationPanel ();
		#endif
	}

	private void HideNationPanel()
	{
		_nationPanel.gameObject.SetActive (false);
		_nationPanel.transform.localPosition = new Vector2 (86, 0);
		_profilePanel.clipTexture = _profileMaskForAndroid;
		_profilePanel.transform.localPosition = new Vector2 (86, 0);
		_profileBg.spriteName = "Result_RankRow_Profile_Mask_ForAndroid";
		_profileBg.transform.localPosition = new Vector2 (86, 0);
		_score.transform.localPosition = new Vector2 (170, 0);
	}
	
	void Update ()
	{
	}

    public void SetNation(string nationId)
    {
        _nation.spriteName = "Flag_" + nationId;
    }

    public void SetProfile(Texture texture)
    {
        if (texture != null)
        {
            if (texture.height == 76) _profile.mainTexture = (Texture)Resources.Load("ProfilePhoto_Basic");
            else _profile.mainTexture = texture;
        }
        else _profile.mainTexture = (Texture)Resources.Load("ProfilePhoto_Basic");
    }

    public void SetProfileBgColor(Color color)
    {
        _profileBg.color = color;
    }

    public void SetScore(string score)
    {
        _score.text = score;
    }

    public void Show(float time, float delay = 0.0f)
    {
        DOTween.To(() => _widget.alpha, x => _widget.alpha = x, 1, time).SetDelay(delay);
    }

    public void Hide(float time, float delay = 0.0f)
    {
        DOTween.To(() => _widget.alpha, x => _widget.alpha = x, 0, time).SetDelay(delay);
    }
}
