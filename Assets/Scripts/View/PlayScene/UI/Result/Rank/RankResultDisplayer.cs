﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class RankResultDisplayer : MonoBehaviour 
{
    public delegate void RankResultDisplayerEvent();
    public event RankResultDisplayerEvent Ended;

    public RankRow _worldRankRow;
    public RankRow _friendRankRow;
    public RankRow _personalRankRow;
    public RankRow _rivalRankRow;
    public RankRow _newRankRow;
    public AnimationCurve _customElastic;
    public AudioClip _scoreCountAudioClip;

    public Texture tex0;
    public Texture tex1;
    public Texture tex2;
    public Texture tex3;

    private const float RANK_ROW_SHOW_POS_X = -110f;
    private const float RANK_ROW_HIDE_POS_X = -80f;
    private const float RANK_ROW_SPACE_H = 50f;
    private const float NEW_RANK_ROW_SPACE_H = 10f;
    private List<RankRow> _rankRowList = new List<RankRow>();
    private AudioSource _auidoSource;
    private float _newScore = 0;

    void Awake ()
    {
        _auidoSource = GetComponent<AudioSource>();
        InitRankRows();
        Hide(0.0f);
    }

	void Start () 
	{
        //Show(0.5f);
        //Invoke("MoveMove", 1);
	}

    private void MoveMove()
    {
        Move(3);
    }

    private void InitRankRows()
    {
        _worldRankRow.SetNation("000");
        _worldRankRow.SetProfile(tex0);
        _worldRankRow.SetScore("--,---");
        _rankRowList.Add(_worldRankRow);

        _friendRankRow.SetNation("000");
        _friendRankRow.SetProfile(tex1);
        _friendRankRow.SetScore("--,---");
        _rankRowList.Add(_friendRankRow);

        //_personalRankRow.SetNation("000");
        _personalRankRow.SetProfile(tex2);
        _personalRankRow.SetScore("--,---");
        _rankRowList.Add(_personalRankRow);

        _rivalRankRow.SetNation("000");
        _rivalRankRow.SetProfile(tex3);
        _rivalRankRow.SetScore("--,---");
        _rankRowList.Add(_rivalRankRow);

        Vector3 newRankRowPosition = _newRankRow.transform.localPosition;
        newRankRowPosition.y = -(RANK_ROW_SPACE_H * _rankRowList.Count);
        _newRankRow.transform.localPosition = newRankRowPosition;
    }

    public void SetData(RecordPlayerData world, RecordPlayerData friend, RecordPlayerData userData, RecordPlayerData rivalData, int rivalCharacterID, float newScore)
    {
        _newScore = newScore;
        currentCountScore = 0;
        _rankRowList = new List<RankRow>();

        _rankRowList.Add(_worldRankRow);
        _rankRowList.Add(_friendRankRow);
        _rankRowList.Add(_personalRankRow);

        if(world != null)
        {
            _worldRankRow.SetNation(world.countryCode);
            _worldRankRow.SetProfile(world.profilePhoto);
            _worldRankRow.SetScore(ScoreFormat.ConvertString(world.record));
        }
        
        if(friend != null)
        {
            _friendRankRow.SetNation(friend.countryCode);
            _friendRankRow.SetProfile(friend.profilePhoto);
            _friendRankRow.SetScore(ScoreFormat.ConvertString(friend.record));
        }
        
        if(userData != null)
        {
            _personalRankRow.SetNation(userData.countryCode);
            _personalRankRow.SetProfile(userData.profilePhoto);
            _personalRankRow.SetScore(ScoreFormat.ConvertString(userData.record));

            _newRankRow.SetNation(userData.countryCode);
            _newRankRow.SetProfile(userData.profilePhoto);
            _newRankRow.SetScore("00.000");
        }

        if(rivalData != null)
        {
            _rivalRankRow.SetNation(rivalData.countryCode);
            _rivalRankRow.SetProfile(rivalData.profilePhoto);
            _rivalRankRow.SetProfileBgColor(CharacterDataManager.instance.GetCharacterDataByID(rivalCharacterID).bgColor);
            _rivalRankRow.SetScore(ScoreFormat.ConvertString(rivalData.record));
            _rankRowList.Add(_rivalRankRow);
        }
    }

	void Update () 
	{
	    
	}

    private int _rankMovementCount;
    public void Show(float time, int rankMovementCount)
    {
        ResetPosition();

        _rankMovementCount = rankMovementCount;
        float delay = 0.1f;
        for(int i = 0; i < _rankRowList.Count; i++)
        {
            _rankRowList[i].Show(time, delay * i);
            _rankRowList[i].transform.DOLocalMoveX(RANK_ROW_SHOW_POS_X, time).SetDelay(delay * i).SetEase(_customElastic);
        }

        _newRankRow.Show(time, delay * _rankRowList.Count);
        _newRankRow.transform.DOLocalMoveY(-(RANK_ROW_SPACE_H * _rankRowList.Count), time).SetDelay(delay * _rankRowList.Count).SetEase(_customElastic).OnComplete(OnShowed);
    }

    private void OnShowed()
    {
        Move(_rankMovementCount);
    }

    public void Hide(float time, bool isDelay = false)
    {
        float delay = 0.0f;
        if (isDelay) delay = 0.1f;

        for (int i = 0; i < _rankRowList.Count; i++)
        {
            _rankRowList[i].Hide(time, delay * i);
            _rankRowList[i].transform.DOLocalMoveX(RANK_ROW_HIDE_POS_X, time).SetDelay(delay * i);
        }

        _newRankRow.Hide(time, delay * _rankRowList.Count);
    }

    // 1 ~ 3
    public float currentCountScore = 0.0f;
    public void Move(int moveNum)
    {
        float moveTime = 0.8f;
        float targetPosY = -((_rankRowList.Count - moveNum) * RANK_ROW_SPACE_H);
        float time = moveTime * moveNum;
        _newRankRow.transform.DOLocalMoveY(targetPosY, time).SetEase(_customElastic);

        CountScore((moveTime - 0.3f) * moveNum);

        for(int i = 0; i < moveNum; i++)
        {
            RankRow rankRow = _rankRowList[(_rankRowList.Count - 1) - i];
            rankRow.transform.DOLocalMoveY(rankRow.transform.localPosition.y - 50, moveTime).SetDelay(0.1f * i).SetEase(_customElastic);
        }
    }

    private void ResetPosition()
    {
        for (int i = 0; i < _rankRowList.Count; i++)
        {
            _rankRowList[i].transform.DOLocalMove(new Vector3(RANK_ROW_HIDE_POS_X, -RANK_ROW_SPACE_H * i, 0), 0).SetEase(_customElastic);
        }

        _newRankRow.transform.DOLocalMove(new Vector3(RANK_ROW_SHOW_POS_X, -(RANK_ROW_SPACE_H * _rankRowList.Count), 0), 0).SetEase(_customElastic);
    }

    private void CountScore(float time)
    {
        _auidoSource.clip = _scoreCountAudioClip;
        _auidoSource.loop = true;
        _auidoSource.Play();

        DOTween.To(() => this.currentCountScore, x => this.currentCountScore = x, _newScore, time).OnUpdate(OnScoreCountUpdate).OnComplete(OnScoreCountCompleted);
    }

    private void OnScoreCountUpdate()
    {
        _newRankRow.SetScore(ScoreFormat.ConvertString(currentCountScore));
    }

    private void OnScoreCountCompleted()
    {
        _auidoSource.Stop();
        if (Ended != null) Ended();
    }
}
