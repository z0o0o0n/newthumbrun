﻿namespace playScene.ui.result.profile
{
    using UnityEngine;
    using System.Collections;
    using DG.Tweening;

    public class Profile : MonoBehaviour 
    {
        public ProfileThumb thumb;
        public UILabel nameLabel;
        public ExpDisplay expDisplay;
        public AnimationCurve customElastic;

        private float _startPosY = 0.0f;
        private int _rewardEXP = 0;

        void Awake()
        {
            _startPosY = transform.localPosition.y;
        }

	    void Start () 
	    {
            Hide();
	    }
	
	    void Update () 
	    {
	
	    }

        public void Init(int characterID, Color characterBgColor, string characterName, int characterEXP, int rewardEXP)
        {
            Hide();

            thumb.Init(characterID, characterBgColor);
            nameLabel.text = characterName;
            expDisplay.Init(characterEXP);

            _rewardEXP = rewardEXP;
        }

        public void Show(float time = 0)
        {
            UIWidget thisWidget = GetComponent<UIWidget>();
            DOTween.To(() => thisWidget.alpha, x => thisWidget.alpha = x, 1, time);
            thisWidget.transform.DOLocalMoveY(_startPosY, time).SetEase(customElastic);
        }

        public void Hide(float time = 0)
        {
            UIWidget thisWidget = GetComponent<UIWidget>();
            DOTween.To(() => thisWidget.alpha, x => thisWidget.alpha = x, 0, time);
            thisWidget.transform.DOLocalMoveY(_startPosY+100, time).SetEase(Ease.InBack);
        }

        public void AddEXP()
        {
            if(_rewardEXP != 0)
            {
                expDisplay.Add(_rewardEXP);
            }
        }
    }
}