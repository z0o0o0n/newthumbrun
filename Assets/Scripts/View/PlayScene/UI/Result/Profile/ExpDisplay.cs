﻿namespace playScene.ui.result.profile
{
    using UnityEngine;
    using System.Collections;
    using DG.Tweening;
    using System;

    public class ExpDisplay : MonoBehaviour
    {
        public UILabel valueLabel;
        public UILabel plusVlaueLabel;
        public AnimationCurve customElastic;

        void Start()
        {
        }

        void Update()
        {

        }

        public void Init(int currentExp)
        {
            valueLabel.text = currentExp.ToString();
        }

        public void Add(int value)
        {
            ResetAddMotion();
            plusVlaueLabel.text = value.ToString();
            PlayAddMotion();
        }

        private void ResetAddMotion()
        {
            plusVlaueLabel.transform.localPosition = new Vector3(90, -11, 0);
            plusVlaueLabel.transform.localScale = Vector3.zero;
            plusVlaueLabel.alpha = 1;
        }

        private void PlayAddMotion()
        {
            PlayIntroMotion();
        }

        private void PlayIntroMotion()
        {
            plusVlaueLabel.transform.DOScale(1, 0.6f).SetEase(customElastic).OnComplete(PlayAdditionMotion);
        }

        private void PlayAdditionMotion()
        {
            plusVlaueLabel.transform.DOLocalMoveX(valueLabel.transform.localPosition.x, 0.4f).SetEase(Ease.InCubic); // Scale
            DOTween.To(() => plusVlaueLabel.alpha, x => plusVlaueLabel.alpha = x, 0, 0.4f).SetEase(Ease.InCubic).OnComplete(PlayAbsorptionMotion); // Alpha
        }

        private void PlayAbsorptionMotion()
        {
            int value = Convert.ToInt32(valueLabel.text) + Convert.ToInt32(plusVlaueLabel.text);
            valueLabel.text = value.ToString();
            valueLabel.transform.DOScale(0.2f, 0.1f);
            valueLabel.transform.DOScale(1, 0.5f).SetDelay(0.1f).SetEase(customElastic);
            plusVlaueLabel.text = "";
        }
    }
}