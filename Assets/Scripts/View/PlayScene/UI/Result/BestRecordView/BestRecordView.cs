﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BestRecordView : MonoBehaviour 
{
    public BestRecordViewItem worldRecordView;
    public BestRecordViewItem personalRecordView;
    public BestRecordViewItem friendRecordView;
    public AnimationCurve customElastic1;

    private float _startPosY = 0.0f;
    private UIWidget _widget;

    void Awake()
    {
        _startPosY = transform.localPosition.y;
        _widget = GetComponent<UIWidget>();
    }

	void Start () 
	{
        Hide();
	}
	
	void Update () 
	{
	
	}

    public void Init(float worldRecord, float personalRecord, float friendRecord)
    {
        Hide();

        worldRecordView.SetValue(worldRecord);
        personalRecordView.SetValue(personalRecord);
        friendRecordView.SetValue(friendRecord);
    }

    public void UpdateWorldRecord(float newRecord)
    {
        worldRecordView.UpdateRecord(newRecord);
    }

    public void UpdatePersonalRecord(float newRecord)
    {
        personalRecordView.UpdateRecord(newRecord);
    }

    public void UpdateFriendRecord(float newRecord)
    {
        friendRecordView.UpdateRecord(newRecord);
    }

    public void Show(float time = 0.0f)
    {
        DOTween.To(() => _widget.alpha, x => _widget.alpha = x, 1, time);
    }

    public void Hide(float time = 0.0f)
    {
        DOTween.To(() => _widget.alpha, x => _widget.alpha = x, 0, time);
    }
}
