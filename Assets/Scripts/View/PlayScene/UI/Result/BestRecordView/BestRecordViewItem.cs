﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Junhee.Utils;
using System;

public class BestRecordViewItem : MonoBehaviour 
{
    public UISprite icon;
    public UILabel label;
    public AnimationCurve customElastic2;

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    public void SetValue(float record)
    {
        label.text = ScoreFormat.ConvertString(record);
    }

    public void UpdateRecord(float newRecord)
    {
        SetValue(newRecord);
        PlayUpdateRecordMotion(0.3f);
    }

    private void PlayUpdateRecordMotion(float time = 0.0f)
    {
        transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        icon.color = ColorUtil.ConvertHexStringToColor("0xfddf8f");
        label.color = ColorUtil.ConvertHexStringToColor("0xfddf8f");
        transform.DOScale(1, time).SetEase(customElastic2);
    }

    private void ResetUpdateRecordMotion()
    {

    }
}
