﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SuccessStatusView : MonoBehaviour 
{
    public UISprite newRecordSprite;
    public UISprite timeOutSprite;
    public AnimationCurve customElastic2;

    private float _startPosY = 0.0f;
    private UIWidget _widget;

    public void Awake()
    {
        _startPosY = transform.localPosition.y;
        _widget = GetComponent<UIWidget>();
        Hide();
    }

    public void Init(bool isSuccess)
    {
        if(isSuccess)
        {
            newRecordSprite.gameObject.SetActive(true);
            timeOutSprite.gameObject.SetActive(false);
        }
        else
        {
            newRecordSprite.gameObject.SetActive(false);
            timeOutSprite.gameObject.SetActive(true);
        }
    }

	public void Show(float time = 0.0f)
    {
        transform.DOScale(1, time).SetEase(customElastic2);
        DOTween.To(() => _widget.alpha, x => _widget.alpha = x, 1, time);
    }

    public void MoveUp(float time = 0.0f)
    {
        _widget.transform.DOLocalMoveY(_startPosY + 120, time).SetEase(Ease.InOutCubic);
    }

    public void Hide(float time = 0.0f)
    {
        transform.DOScale(1.5f, time).SetEase(Ease.InBack);
        DOTween.To(() => _widget.alpha, x => _widget.alpha = x, 0, time);
        _widget.transform.DOLocalMoveY(_startPosY, time).SetEase(Ease.InBack);
    }
}
