﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LapInfoNotification : MonoBehaviour 
{
    [SerializeField]
    private UILabel _label;
    [SerializeField]
    private NGUILocalizeText _localizeText;
    [SerializeField]
    private UISprite _bg;
    [SerializeField]
    private AnimationCurve _customElastic;

    void Awake ()
    {
        transform.localPosition = new Vector3(0f, -80f, 0f);
        _localizeText.Changed += OnTextChanged;
        Hide(0);
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	    if(Input.GetKeyUp(KeyCode.Alpha1))
        {
            Show(0.5f);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            Hide(0.3f);
        }
	}

    private void OnDestroy()
    {
        _localizeText.Changed -= OnTextChanged;
    }




    private void OnTextChanged()
    {
        _bg.width = _label.width + 50;
    }




    public void Show(float time)
    {
        DOTween.Kill("LapInfoNotification_Text_Scale_" + gameObject.GetInstanceID());
        DOTween.Kill("LapInfoNotification_Bg_Scale_" + gameObject.GetInstanceID());

        _label.transform.DOScale(1, time).SetId("LapInfoNotification_Text_Scale" + gameObject.GetInstanceID()).SetEase(_customElastic);
        _bg.transform.DOScale(1, time).SetId("LapInfoNotification_Bg_Scale" + gameObject.GetInstanceID()).SetDelay(0.1f).SetEase(_customElastic);
    }

    public void Hide(float time)
    {
        DOTween.Kill("LapInfoNotification_Text_Scale_" + gameObject.GetInstanceID());
        DOTween.Kill("LapInfoNotification_Bg_Scale_" + gameObject.GetInstanceID());

        if (time == 0)
        {
            _label.transform.localScale = Vector3.zero;
            _bg.transform.localScale = Vector3.zero;
        }
        else
        {
            _label.transform.DOScale(0, time).SetId("LapInfoNotification_Text_Scale" + gameObject.GetInstanceID()).SetEase(Ease.InBack);
            _bg.transform.DOScale(0, time).SetId("LapInfoNotification_Bg_Scale" + gameObject.GetInstanceID()).SetDelay(0.1f).SetEase(Ease.InBack);
        }
    }
}
