﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LapCountNotification : MonoBehaviour 
{
    public UIWidget text;
    public UILabel lapCountLabel;
    public UILabel recordLabel;
    public UISprite bg;
    public AnimationCurve customElastic;

    void Awake()
    {
        transform.localPosition = new Vector3(0f, -80f, 0f);
        Hide(0);
    }

	void Start () 
	{
	}
	
	void Update () 
	{
	    if(Input.GetKeyUp(KeyCode.Alpha1))
        {
            Show(0.5f, 3, 12.210f);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            Hide(0.3f);
        }
	}

    public void Show(float time, int count, float record)
    {
        DOTween.Kill("LapCountNotification_Text_Scale");
        DOTween.Kill("LapCountNotification_Bg_Scale");

        lapCountLabel.text = " Lap " + count;
        recordLabel.text = ScoreFormat.ConvertString(record);
        text.transform.DOScale(1, time).SetId("LapCountNotification_Text_Scale").SetEase(customElastic);
        bg.transform.DOScale(1, time).SetId("LapCountNotification_Bg_Scale").SetDelay(0.1f).SetEase(customElastic);
    }

    public void Hide(float time)
    {
        DOTween.Kill("LapCountNotification_Text_Scale");
        DOTween.Kill("LapCountNotification_Bg_Scale");
        
        if(time == 0)
        {
            text.transform.localScale = Vector3.zero;
            bg.transform.localScale = Vector3.zero;
        }
        else
        {
            text.transform.DOScale(0, time).SetId("LapCountNotification_Text_Scale").SetEase(Ease.InBack);
            bg.transform.DOScale(0, time).SetId("LapCountNotification_Bg_Scale").SetDelay(0.1f).SetEase(Ease.InBack);
        }
    }
}
