﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Com.Mod.ThumbRun.Race.Application;

public class LapInfoNotificator : MonoBehaviour 
{
    public LapCountNotification lapCountNotification;
    public LapInfoNotification lastLapNotification;
    public LapInfoNotification checkPointNotification;
    public float delayCount = 0.0f;

    [SerializeField]
    private RaceRecordService _raceRecordService;

    private void Awake()
    {
        _raceRecordService.Added += OnRecordAdded;
    }

    void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}

    private void OnDestroy()
    {
        _raceRecordService.Added -= OnRecordAdded;
    }




    private void OnRecordAdded(int round, float roundRecord)
    {
        ShowLapCount(round + 1, roundRecord);
    }




    private int _lapCount;
    private void ShowLapCount(int count, float record)
    {
        DOTween.Kill("LapInfoNotificator_LapCount_Delay");

        _lapCount = count;
        lapCountNotification.Show(0.5f, _lapCount, record);

        delayCount = 0.0f;
        DOTween.To(() => delayCount, x => delayCount = x, 2, 2).SetId("LapInfoNotificator_LapCount_Delay").OnComplete(HideLapCount);
    }

    private void HideLapCount()
    {
        lapCountNotification.Hide(0.3f);

        //if (_lapCount == 2) ShowLastLap();
    }

    //public void ShowLastLap()
    //{
    //    lastLapNotification.Show(0.5f);
    //}

    public void ShowCheckPoint()
    {
        DOTween.Kill("LapInfoNotificator_CheckPoint_Delay");

        checkPointNotification.Show(0.5f);

        delayCount = 0.0f;
        DOTween.To(() => delayCount, x => delayCount = x, 2, 2).SetId("LapInfoNotificator_CheckPoint_Delay").OnComplete(HideCheckPoint);
    }

    private void HideCheckPoint()
    {
        checkPointNotification.Hide(0.3f);
    }
}
