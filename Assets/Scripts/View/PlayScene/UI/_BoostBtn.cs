﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BoostBtn : MonoBehaviour
{
	public delegate void BoostEvent ();
	public event BoostEvent On_StartBoost;
	public event BoostEvent On_EndBoost;

	[Space(10)]
	[Header("Elements")]
    public UITexture gaugeTexture;
    public UISprite btnOn;
    public UISprite btnPress;
    
	[Space(10)]
	[Header("Boost Option")]
	public float useableGaugeLimit = 0.3f;
    public float chargingSpeed = 0.001f;
    public float consumptionSpeed = 0.003f;

    private bool _isBoosting = false;
    private bool _isUseable = false;
    private float _currentGaugeNum = 0;
    private UIButton _button;

    public float gaugeNum
    {
        get
        {
            return _currentGaugeNum;
        }
        set
        {
            _currentGaugeNum = value;
            gaugeTexture.material.SetFloat("_Value", (_currentGaugeNum - 0.5f)*-1);
            gaugeTexture.enabled = false;
            gaugeTexture.enabled = true;
        }
    }

	public void Show()
	{
		DOTween.Kill ("boostButton");
		transform.DOScale (new Vector3 (1, 1, 1), 0.5f).SetId ("boostButton").SetEase (Ease.OutBack);
	}

	public void Hide(float time = 0.3f)
	{
		DOTween.Kill ("boostButton");
		transform.DOScale (new Vector3 (0, 0, 1), time).SetId ("boostButton").SetEase (Ease.InBack).SetUpdate(true);
	}

	void Awake()
	{
		_button = GetComponent<UIButton>();
		Hide (0);
	}

	void Start ()
    {
		Reset ();
	}

	public void Reset()
	{
		gaugeNum = 0;
		DeactivateBtn ();
	}

	void FixedUpdate ()
	{
		if (_isBoosting) Consume ();
		else Charge ();

        if (gaugeNum >= 1) gaugeNum = 1;
        if(gaugeNum <= 0) gaugeNum = 0;

		CheckBoostEnd ();
		CheckUseable ();
	}

	private void CheckBoostEnd()
	{
		if (gaugeNum <= 0 && _isBoosting)
		{
			_isBoosting = false;
			EndBoost();
		}
	}

	private void CheckUseable()
	{
		if(!_isBoosting)
		{
			if(gaugeNum >= 0.3)
			{
				if (!_isUseable)
				{
					_isUseable = true;
					ActivateBtn();
				}
			}
			else
			{
				if(_isUseable)
				{
					_isUseable = false;
				}
			}
		}
	}

	private void Consume()
	{
		gaugeNum -= consumptionSpeed;
	}

	private void Charge()
	{
        gaugeNum += chargingSpeed;
	}

    public void OnPressBoostBtn()
    {
		Boost ();
    }

	private void Boost()
	{
		if(!_isBoosting)
		{
			_isBoosting = true;
			DeactivateBtn();

			if(On_StartBoost != null) On_StartBoost();
		}
	}

	private void EndBoost()
	{
		if(On_EndBoost != null) On_EndBoost();
	}

	private void ChangeBtnOn()
	{
		btnOn.enabled = true;
		btnPress.enabled = false;
		gaugeTexture.transform.DOLocalMoveY(70, 0);
	}

	private void ChangeBtnPress()
	{
		btnOn.enabled = false;
		btnPress.enabled = true;
		gaugeTexture.transform.DOLocalMoveY(55, 0);
	}

	private void ActivateBtn()
	{
		_button.enabled = true;
		ChangeBtnOn();
	}

	private void DeactivateBtn()
	{
		_button.enabled = false;
		ChangeBtnPress ();
	}
}
