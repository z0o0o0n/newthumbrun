﻿namespace Com.Mod.ThumbRun.Race.View
{
    using UnityEngine;
    using System.Collections;
    using Junhee.Utils;
    using DG.Tweening;
    using System;
    using System.Globalization;
    using Application;

    public class RecordDisplay : MonoBehaviour, IStopwatchView
    {
        [SerializeField]
        private RaceRecordService _raceRecordService;
        [SerializeField]
        private StopwatchModel _stopWatch;
        [SerializeField]
        private UILabel _stopwatchField;
        [SerializeField]
        private UILabel _stopwatchLapField;
        [SerializeField]
        private GameObject[] _recordFieldList;
        [SerializeField]
        private AnimationCurve _showMotionCurve;

        private float hidenFieldPosX = 200f;
        private float fieldLineSpace = 35f;
        private UILabel[] recordFieldUILabelList = new UILabel[3];
        private int _currentLap = 1;
        private RaceManager _raceManager;

        void Awake()
        {
            _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
            _raceManager.RaceResume += OnRaceResume;
            _raceManager.RoundEnd += OnRoundEnd;

            _stopWatch.Tick += OnStopWatchTick;
            _raceRecordService.Added += OnRecordAdded;

            for (int i = 0; i < _recordFieldList.Length; i++)
            {
                recordFieldUILabelList[i] = _recordFieldList[i].GetComponent<UILabel>();
            }
        }

        void Start()
        {
            Reset();
        }

        void OnDestroy()
        {
            _raceManager.RaceResume -= OnRaceResume;
            _raceManager.RoundEnd -= OnRoundEnd;
            _stopWatch.Tick -= OnStopWatchTick;
            _raceRecordService.Added -= OnRecordAdded;
        }





        private void OnRaceResume()
        {
            Debug.Log("^^^^^^^^^^^^^ Race Resume - Record Display");
            Show();
        }

        private void OnStopWatchTick()
        {
            _stopwatchField.text = ScoreFormat.ConvertString(_stopWatch.GetSec() + _stopWatch.GetMsec());
        }

        private void OnRecordAdded(int round, float roundRecord)
        {
            recordFieldUILabelList[round].text = ScoreFormat.ConvertString(roundRecord);

            if (round >= recordFieldUILabelList.Length - 1)
            {
                _stopwatchField.gameObject.SetActive(false);

                for (int i = 0; i < _recordFieldList.Length; i++)
                {
                    recordFieldUILabelList[i].gameObject.SetActive(true);
                }
            }
            else
            {
                SelectRecordFieldUILabel(round + 1);

                Vector3 stopwatchFieldPos = _stopwatchField.transform.localPosition;
                stopwatchFieldPos.y = ((round + 1) * -fieldLineSpace);
                _stopwatchField.transform.localPosition = stopwatchFieldPos;
            }
        }

        private void OnRoundEnd(int currentLap)
        {
            _currentLap = currentLap;
            _stopwatchLapField.text = "Lap " + _currentLap;

            for (int i = 0; i < _recordFieldList.Length; i++)
            {
                if (i == _currentLap - 1)
                {
                    DOTween.Kill("recordFieldId" + i);
                    _recordFieldList[i].transform.DOLocalMoveX(0, 1.5f).SetId("recordFieldId" + i).SetUpdate(true);
                }
            }
        }

        //private void RecordCurrentTime()
        //{
        //    if (_currentRecordFieldIndex >= recordFieldUILabelList.Length - 1)
        //    {
        //        stopwatchUILabel.gameObject.SetActive(false);

        //        for (int i = 0; i < _recordFieldList.Length; i++)
        //        {
        //            recordFieldUILabelList[i].gameObject.SetActive(true);
        //        }
        //    }
        //    else
        //    {
        //        SelectRecordFieldUILabel(_currentRecordFieldIndex + 1);

        //        Vector3 stopwatchFieldPos = stopwatchUILabel.transform.localPosition;
        //        stopwatchFieldPos.y = ((_currentRecordFieldIndex + 1) * -fieldLineSpace) - 3.5f;
        //        stopwatchUILabel.transform.localPosition = stopwatchFieldPos;
        //    }

        //    _currentRecordFieldIndex++;
        //}

        private void SelectRecordFieldUILabel(int index)
        {
            for (int i = 0; i < _recordFieldList.Length; i++)
            {
                if (i == index)
                {
                    recordFieldUILabelList[i].gameObject.SetActive(false);
                }
                else
                {
                    recordFieldUILabelList[i].gameObject.SetActive(true);
                }
            }
        }



        public void Show()
        {
            float time = 1.5f;
            float delay = 0.1f;

            for (int i = 0; i < _recordFieldList.Length; i++)
            {
                //Debug.Log("Record Display i: " + i + " / currentLap: " + _currentLap);
                if (i < (_currentLap - 1))
                {
                    DOTween.Kill("recordFieldId" + i);
                    _recordFieldList[i].transform.DOLocalMoveX(0, time).SetId("recordFieldId" + i).SetEase(_showMotionCurve).SetUpdate(true);
                }
                else if(i == (_currentLap - 1))
                {
                    DOTween.Kill("stopwatchField");
                    _stopwatchField.transform.DOLocalMoveX(0, time).SetId("stopwatchField").SetEase(_showMotionCurve).SetUpdate(true);

                    DOTween.Kill("recordFieldId" + i);
                    _recordFieldList[i].transform.DOLocalMoveX(0, time).SetId("recordFieldId" + i).SetEase(_showMotionCurve).SetUpdate(true);
                }
                else if(i > (_currentLap - 1))
                {

                }
            }
            //for (int i = 0; i < _recordFieldList.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        DOTween.Kill("stopwatchField");
            //        _stopwatchField.transform.DOLocalMoveX(0, time).SetId("stopwatchField").SetDelay((i * delay)).SetEase(_showMotionCurve).SetUpdate(true);
            //    }
            //    DOTween.Kill("recordFieldId" + i);
            //    _recordFieldList[i].transform.DOLocalMoveX(0, time).SetId("recordFieldId" + i).SetDelay((i * delay)).SetEase(_showMotionCurve).SetUpdate(true);
            //}
        }

        public void Hide(float time = 0.0f)
        {
            Debug.Log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ HIDE");
            for (int i = 0; i < _recordFieldList.Length; i++)
            {
                if (i == 0)
                {
                    DOTween.Kill("stopwatchField");
                    _stopwatchField.transform.DOLocalMoveX(hidenFieldPosX, time).SetId("stopwatchField").SetUpdate(true);
                }
                DOTween.Kill("recordFieldId" + i);
                _recordFieldList[i].transform.DOLocalMoveX(hidenFieldPosX, time).SetId("recordFieldId" + i).SetUpdate(true);
            }
        }

        public void Reset()
        {
            // 활성화 Reset
            if (_stopwatchField != null)
            {
                _stopwatchField.gameObject.SetActive(true);
                _stopwatchField.text = "00.000";
            }

            // 위치 Reset
            _stopwatchField.transform.DOLocalMove(new Vector3(hidenFieldPosX, 0f, 0f), 0);
            for (int i = 0; i < _recordFieldList.Length; i++)
            {
                _recordFieldList[i].transform.DOLocalMoveX(hidenFieldPosX, 0f);
                _recordFieldList[i].transform.DOLocalMoveY(-fieldLineSpace * i, 0f);
            }

            // 값 Reset
            for (int i = 0; i < _recordFieldList.Length; i++)
            {
                recordFieldUILabelList[i].text = "00.000";
            }

            // 비활성할 Field 선택
            SelectRecordFieldUILabel(0);
        }

        public float GetTotal()
        {
            // 1, 2, 3 라운드 기록을 합친 종합 기록
            return Convert.ToSingle(recordFieldUILabelList[0].text) + Convert.ToSingle(recordFieldUILabelList[1].text) + Convert.ToSingle(recordFieldUILabelList[2].text);
        }

        public float[] GetRoundRecordList()
        {
            float[] value = { Convert.ToSingle(recordFieldUILabelList[0].text), Convert.ToSingle(recordFieldUILabelList[1].text), Convert.ToSingle(recordFieldUILabelList[2].text) };
            return value;
        }
    }
}