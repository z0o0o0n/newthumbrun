﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using playScene.ui.result.profile;
using System;

public class ResultUI : MonoBehaviour
{
    public delegate void ResultUIEvent();
    public event ResultUIEvent EndedRank;
    public event ResultUIEvent Ended;

    public CameraController tempCamera;

    //public BestRecordView bestRecordView;
    //public Profile profile;
    //public SuccessStatusView successStatusView;
    //public RecordView recordView;
    //public GoldView goldView;
    public ResultUITitle _title;
    public UIWidget _progress;
    public RankResultDisplayer _rankResultDisplayer;
    public GoldLabel _goldLabel;

    private bool _isRenewWorldRecord = false;
    private bool _isRenewPersonalRecord = false;
    private bool _isRenewFriendRecord = false;
    private float _sequentialVisualizationDelay = 0.0f;
    private ResultData _resultData;

	void Start ()
    {
	}
	
	void Update () 
    {
	
	}

    public void ShowProgress()
    {
        _progress.gameObject.SetActive(true);
    }

    public void HideProgress()
    {
        _progress.gameObject.SetActive(false);
    }

    private int _gold = 0;
    private float _goldCountTime = 0;
    public void ShowRankDisplayer(bool isOnline, ResultData resultData, int gold, float goldCountTime)
    {
        _gold = gold;
        _goldCountTime = goldCountTime;

        //Reset();
        _title.Show(0.5f);
        _rankResultDisplayer.Ended += OnRankResultDisplayEnded;
        _rankResultDisplayer.SetData(resultData.world, resultData.friend, resultData.personal, resultData.rival, resultData.rivalCharacterID, resultData.newScore);
        _rankResultDisplayer.Show(0.5f, resultData.rankMovementCount);

        //SetProfileData(resultData.characterID, resultData.characterBgColor, resultData.characterName, resultData.currentEXP, resultData.rewardEXP);
        //SetRecordData(resultData.roundRecordList, resultData.isSuccess);
        //SetGoldData(resultData.rewardGold, resultData.newRecordGold, resultData.sponsorBonus);
        //SetBestRecord(resultData.worldRecord, resultData.isRenewWorldRecord, resultData.personalRecord, resultData.isRenewPersonalRecord, resultData.friendRecord, resultData.isRenewFriendRecord);
        //PlayShowMotion();
    }

    private void OnRankResultDisplayEnded()
    {
        _goldLabel.Show(0.5f);
        _goldLabel.Ended += OnGoldCountEnded;
        _goldLabel.Count(_gold, _goldCountTime);

        _rankResultDisplayer.Ended -= OnRankResultDisplayEnded;

        if (EndedRank != null) EndedRank();
    }

    private void OnGoldCountEnded()
    {
        _goldLabel.Ended -= OnGoldCountEnded;

        if (Ended != null) Ended();
    }

    private void Reset()
    {
        Debug.Log("Reset Result UI");
    }

    public void HideRankDisplayer(float time)
    {
        _title.Hide(time);
        _rankResultDisplayer.Hide(time, false);
        _goldLabel.Hide(time);
        //bestRecordView.Hide(time);
        //profile.Hide(time);
        //successStatusView.Hide(time);
        //recordView.Hide(time);
        //goldView.Hide(time);
    }

    public void SetText(string text)
    {
        
    }

    public ResultData GetResultData()
    {
        return _resultData;
    }

    void OnDestory()
    {
        _rankResultDisplayer.Ended -= OnRankResultDisplayEnded;
        _goldLabel.Ended -= OnGoldCountEnded;
    }
}
