﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.User.View;

public class PlaySceneUI : MonoBehaviour
{
    public delegate void PlaySceneUIEvent();
    public event PlaySceneUIEvent EndCompleteUI;
    public event PlaySceneUIEvent EndTimeoutUI;

    public PlayerControlButton playerControlButton;
    public PlayUI playUI;
    public PlayMenuUI playMenuUI;
    public ResultUI resultUI;
    public GoldDisplay goldDisplay;
    public LapInfoNotificator lapInfoNotificator;
    public TipBook playTutorialBook;
    public GameObject indicator;
    public VersusPopup versusPopup;

    [Header("Dynamic Allowcation Class")]
    [SerializeField]
    private RaceManager _raceManager;

    private bool _isDebug = true;
    private string _logPrefix = "PlaySceneUI - ";

    void Awake()
    {
        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();

        UIRoot uiRoot = GetComponent<UIRoot>();
        Debug.Log(">>>>>>>>>>>>>>>>> uiRoot.manualWidth: " + uiRoot.manualWidth);

        playMenuUI.PauseButtonClick += OnPauseClick;
        playMenuUI.ResumeButtonClick += OnResumeClick;
    }

	void Start ()
    {
	}

    void OnDestroy()
    {
        playMenuUI.PauseButtonClick -= OnPauseClick;
        playMenuUI.ResumeButtonClick -= OnResumeClick;
    }



    #region Private Functions
    private void OnPauseClick()
    {
        _raceManager.PauseRace();
    }

    private void OnResumeClick()
    {
        _raceManager.ResumeRace();
    }
    #endregion



    public void Reset()
    {
        playerControlButton.Deactivate();
        playUI.Reset();
        playMenuUI.HideAllMenus(0.3f);
        resultUI.HideRankDisplayer(0.3f);
        goldDisplay.Hide(0.3f);
    }

    public void ShowPreviewUI()
    {

    }

    public void ShowPlayUI()
    {
        playerControlButton.Activate();
        playUI.ShowPlayUI();
        playMenuUI.ShowPlayMenu();
        lapInfoNotificator.gameObject.SetActive(true);
    }

    public void ShowPauseUI()
    {
        playerControlButton.Deactivate();
        //playUI.HidePlayUI();
        playMenuUI.ShowPauseMenu(0, true, false);
        lapInfoNotificator.gameObject.SetActive(false);
    }

    private int _timeoutGold = 0;
    public void ShowTimeoutUI(int gold, float goldCountTime)
    {
        _timeoutGold = gold;

        playerControlButton.Deactivate();
        //playUI.HidePlayUI();

        //playMenuUI.ShowResultEndMenu(0.3f, false);
        goldDisplay.Show(0.3f);
    }

    private void OnTimeOutGoldCountEnded()
    {
        if (EndTimeoutUI != null) EndTimeoutUI();
    }

    public void ShowResultProgressUI()
    {
        playerControlButton.Deactivate();
        //playUI.HidePlayUI();
        
        playMenuUI.ShowResultMenu(0.3f);
        resultUI.ShowProgress();
    }

    private int _newRecordGold = 0;
    public void ShowResultRankUI(bool isOnline, ResultData resultData, int gold, float goldCountTime)
    {
        if (_isDebug) Debug.Log(_logPrefix + "ShowResultRankUI() / isOnline: " + isOnline + " / resultData: " + resultData + " / gold: " + gold + " / goldCountTime: " + goldCountTime);
        _newRecordGold = gold;

        resultUI.HideProgress();

        goldDisplay.Show(0.3f);
        resultUI.Ended += OnResultRankEnded;
        resultUI.ShowRankDisplayer(isOnline, resultData, gold, goldCountTime);
    }

    private void OnResultRankEnded()
    {
        resultUI.Ended -= OnResultRankEnded;

        //goldDisplay.Save(_newRecordGold);
		
        if (EndCompleteUI != null) EndCompleteUI();
    }

    public void ShowResultEndUI()
    {
        playMenuUI.ShowResultEndMenu(0.3f, false, false);
    }

    //public delegate void ShowedResultUI();
    //public ShowedResultUI ShowedResultUICallback;

    //private void OnEndedResultUI()
    //{
    //    resultUI.Ended -= OnEndedResultUI;
    //    goldDisplay.Show(0.3f, OnShowedGoldDisplay);
    //    playMenuUI.ShowResultMenu(0.3f);
    //}

    //private void OnShowedGoldDisplay()
    //{
    //    ShowedResultUICallback();
    //}

    

    public float GetTotalRecord()
    {
        return playUI.GetRecordDisplay().GetTotal();
    }

    public float[] GetRoundRecordList()
    {
        return playUI.GetRecordDisplay().GetRoundRecordList();
    }

    //public void RecordCurrentTime()
    //{
    //    playUI.recordDisplay.RecordCurrentTime();
    //}

    public void SetButtonActive(bool isActivation)
    {
        playMenuUI.SetButtonActivation(isActivation);
    }



    void OnDestory()
    {
        resultUI.Ended -= OnResultRankEnded;
    }
}
