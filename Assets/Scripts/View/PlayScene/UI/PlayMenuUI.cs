﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PlayMenuUI : MonoBehaviour {
    public delegate void PlayUIEvent();
    public event PlayUIEvent PauseButtonClick;
    public event PlayUIEvent HelpButtonClick;
    public event PlayUIEvent ReplayButtonClick;
    public event PlayUIEvent ShareReplayButtonClick;
    public event PlayUIEvent HomeButtonClick;
    public event PlayUIEvent ResumeButtonClick;

    public UIBasicButton pauseButton;
    public UIBasicButton helpButton;
    public UIBasicButton replayButtton;
    public UIBasicButton nextRunButton;
    public UIBasicButton homeButton;
    public UIButton resumeButton;

    public AnimationCurve showMotionCurve;
	
    void Awake()
    {
        BoxCollider resumeButtonCollider = resumeButton.gameObject.GetComponent<BoxCollider>();
        resumeButtonCollider.size = new Vector3(Screen.width, Screen.height, 0);

        pauseButton.On_Click += OnPauseButtonClick;
        helpButton.On_Click += OnHelpButtonClick;
        replayButtton.On_Click += OnReplayButtonClick;
        nextRunButton.On_Click += OnReplayButtonClick;
		homeButton.On_Click += OnHomeButtonClick;
    }

	void Start ()
    {
        HideAllMenus(0);
	}
	
	void Update () {
	    
	}
    
    public void ShowPlayMenu()
    {
        HideAllMenus(0);
        ShowPauseButton(0.5f);
    }

    public void ShowPauseMenu(float time, bool isRetry, bool isNextRun)
    {
        HidePauseButton(time);

        ShowResumeButton(time);
        ShowHelpButton(time);
        if (isRetry) ShowReplayButton(time);
        if (isNextRun) ShowNextRunButton(time);
        
        ShowHomeButton(time);
    }
    
    public void ShowResultMenu(float time)
    {
        HidePauseButton(time);
    }

    public void ShowResultEndMenu(float time, bool isRetry, bool isNextRun)
    {
        HidePauseButton(time);

        if (isRetry) ShowReplayButton(time);
        if (isNextRun) ShowNextRunButton(time);

        ShowShareReplayButton(time);
        ShowHomeButton(time);
    }

    public void HideAllMenus(float time)
    {
        HidePauseButton(time);
        HideResumeButton(time);
        HideHelpButton(time);
        HideReplayButton(time);
        HideNextRunButton(time);
        HideShareReplayButton(time);
        HideHomeButton(time);
    }

    private void ShowPauseButton(float time)
    {
        //DOTween.Kill("pauseButton");
        //pauseButton.transform.DOScale(new Vector3(1, 1, 0), 0).SetId("pauseButton").SetEase(showMotionCurve).SetUpdate(true);
        pauseButton.gameObject.SetActive(true);
    }

    private void HidePauseButton(float time)
    {
        //DOTween.Kill("pauseButton");
        //pauseButton.transform.DOScale(new Vector3(0, 0, 0), 0).SetId("pauseButton").SetEase(Ease.InBack).SetUpdate(true);
        pauseButton.gameObject.SetActive(false);
    }

    private void ShowResumeButton(float time)
    {
        //DOTween.Kill("resumeButton");
        //resumeButton.transform.DOScale(new Vector3(1, 1, 0), time).SetId("resumeButton").SetEase(showMotionCurve).SetUpdate(true);
        resumeButton.gameObject.SetActive(true);
    }

    private void HideResumeButton(float time)
    {
        //DOTween.Kill("resumeButton");
        //resumeButton.transform.DOScale(new Vector3(0, 0, 0), time).SetId("resumeButton").SetEase(Ease.InBack).SetUpdate(true);
        resumeButton.gameObject.SetActive(false);
    }

    private void ShowHelpButton(float time)
    {
        DOTween.Kill("helpButton");
        helpButton.transform.DOScale(new Vector3(1, 1, 0), time).SetId("helpButton").SetEase(showMotionCurve).SetUpdate(true);
    }

    private void HideHelpButton(float time)
    {
        DOTween.Kill("helpButton");
        helpButton.transform.DOScale(new Vector3(0, 0, 0), time).SetId("helpButton").SetEase(Ease.InBack).SetUpdate(true);
    }

    private void ShowReplayButton(float time)
    {
        DOTween.Kill("replayButton");
        replayButtton.transform.DOScale(new Vector3(1, 1, 0), time).SetId("replayButton").SetEase(showMotionCurve).SetUpdate(true);
    }

    private void HideReplayButton(float time)
    {
        DOTween.Kill("replayButton");
        replayButtton.transform.DOScale(new Vector3(0, 0, 0), time).SetId("replayButton").SetEase(Ease.InBack).SetUpdate(true);
    }

    private void ShowNextRunButton(float time)
    {
        DOTween.Kill("nextRunButton");
        nextRunButton.transform.DOScale(new Vector3(1, 1, 0), time).SetId("nextRunButton").SetEase(showMotionCurve).SetUpdate(true);
    }

    private void HideNextRunButton(float time)
    {
        DOTween.Kill("nextRunButton");
        nextRunButton.transform.DOScale(new Vector3(0, 0, 0), time).SetId("nextRunButton").SetEase(Ease.InBack).SetUpdate(true);
    }

    public void ShowShareReplayButton(float time)
    {
        DOTween.Kill("shareReplayButton");
    }

    public void HideShareReplayButton(float time)
    {
        DOTween.Kill("shareReplayButton");
    }

    private void ShowHomeButton(float time)
    {
        DOTween.Kill("homeButton");
		homeButton.transform.DOScale(new Vector3(1, 1, 0), time).SetId("homeButton").SetEase(showMotionCurve).SetUpdate(true);
    }

    private void HideHomeButton(float time)
    {
        DOTween.Kill("homeButton");
		homeButton.transform.DOScale(new Vector3(0, 0, 0), time).SetId("homeButton").SetEase(Ease.InBack).SetUpdate(true);
    }



    public void OnPauseButtonClick()
    {
        if (PauseButtonClick != null) PauseButtonClick();
    }

    public void OnHelpButtonClick()
    {
        if (HelpButtonClick != null) HelpButtonClick();
    }
        
    public void PressResumeButton()
    {
        if (ResumeButtonClick != null) ResumeButtonClick();
    }

    public void OnReplayButtonClick()
    {
        if (ReplayButtonClick != null) ReplayButtonClick();
    }

    public void OnShareReplayButtonClick()
    {
        if (ShareReplayButtonClick != null) ShareReplayButtonClick();
    }

	public void OnHomeButtonClick()
    {
        //if (HomeButtonClick != null) HomeButtonClick();
    }

    public void SetButtonActivation(bool isActivation)
    {
        pauseButton.SetButtonActive(isActivation);
        helpButton.SetButtonActive(isActivation);
        replayButtton.SetButtonActive(isActivation);
        homeButton.SetButtonActive(isActivation);
    }



    void OnDestory()
    {
        pauseButton.On_Click -= OnPauseButtonClick;
        helpButton.On_Click -= OnHelpButtonClick;
        nextRunButton.On_Click -= OnReplayButtonClick;
        replayButtton.On_Click -= OnReplayButtonClick;
        homeButton.On_Click -= OnHomeButtonClick;
    }
}
