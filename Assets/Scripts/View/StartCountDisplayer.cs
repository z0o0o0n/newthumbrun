﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class StartCountDisplayer : MonoBehaviour 
{
    public UISprite number;
    public UISprite bg;
    public AnimationCurve customElastic;
    public Color32 countColor;
    public Color32 goColor;
    public AudioSource audioSource;
    public AudioClip countSound;
    public AudioClip goSound;

    void Awake()
    {
        number.transform.localScale = Vector3.zero;
        bg.transform.localScale = Vector3.zero;
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
        //if(Input.GetKeyUp(KeyCode.Alpha4))
        //{
        //    Show(3);
        //}
        //else if (Input.GetKeyUp(KeyCode.Alpha3))
        //{
        //    Show(2);
        //}
        //else if (Input.GetKeyUp(KeyCode.Alpha2))
        //{
        //    Show(1);
        //}
        //else if (Input.GetKeyUp(KeyCode.Alpha1))
        //{
        //    Show(0);
        //}
        //else if(Input.GetKeyUp(KeyCode.Alpha5))
        //{
        //    Hide(0.3f);
        //}
	}

    public void Show(int count)
    {
        DOTween.Kill("CountDisplayer_Number_Scale_Tween_0");
        DOTween.Kill("CountDisplayer_Number_Scale_Tween_1");
        DOTween.Kill("CountDisplayer_Bg_Scale_Tween_0");
        DOTween.Kill("CountDisplayer_Bg_Scale_Tween_1");
        
        if(count == 0)
        {
            audioSource.clip = goSound;
            audioSource.Play();

            number.spriteName = "CountGo";
            number.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            number.transform.DOScale(1, 0.4f).SetId("CountDisplayer_Number_Scale_Tween_0").SetEase(Ease.OutElastic);
            number.transform.DOScale(0, 0.3f).SetDelay(0.6f).SetId("CountDisplayer_Number_Scale_Tween_1").SetEase(Ease.InBack);

            bg.color = goColor;
            bg.transform.localScale = Vector3.zero;
            bg.transform.DOScale(1, 0.4f).SetId("CountDisplayer_Bg_Scale_Tween_0").SetEase(Ease.OutElastic);
            bg.transform.DOScale(0, 0.3f).SetDelay(0.6f).SetId("CountDisplayer_Bg_Scale_Tween_1").SetEase(Ease.InBack);
        }
        else
        {
            audioSource.clip = countSound;
            audioSource.Play();

            number.spriteName = "Count" + count;
            number.transform.localScale = new Vector3(0f, 0f, 0f);
            number.transform.DOScale(1, 0.5f).SetDelay(0.1f).SetId("CountDisplayer_Number_Scale_Tween_0").SetEase(customElastic);

            bg.color = countColor;
            bg.transform.localScale = Vector3.zero;
            bg.transform.DOScale(0.8f, 0.5f).SetId("CountDisplayer_Bg_Scale_Tween_0").SetEase(customElastic);
        }

        
        //number.transform.DOScale(0, 0.3f).SetDelay(0.3f).SetId("CountDisplayer_Number_Scale_Tween_1");
    }

    public void Hide(float time)
    {
        DOTween.Kill("CountDisplayer_Number_Scale_Tween_0");
        DOTween.Kill("CountDisplayer_Number_Scale_Tween_1");
        DOTween.Kill("CountDisplayer_Bg_Scale_Tween_0");
        DOTween.Kill("CountDisplayer_Bg_Scale_Tween_1");

        number.transform.DOScale(0, time).SetId("CountDisplayer_Number_Scale_Tween_0").SetEase(Ease.InBack);
        bg.transform.DOScale(0, time).SetId("CountDisplayer_Bg_Scale_Tween_0").SetEase(Ease.InBack);
    }
}
