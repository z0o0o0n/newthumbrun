﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Com.Mod.ThumbRun.User.View;
using Com.Mod.ThumbRun.User.Application;
using Com.Mod.ThumbRun.LuckyChance.Application;
using Com.Mod.ThumbRun.LuckyChance.Domain;
using Com.Mod.ThumbRun.LuckyChance.View;
using Com.Mod.ThumbRun.Race.Domain;
using Com.Mod.ThumbRun.User.Domain;

public class ParticipantInfoDisplay : MonoBehaviour 
{
	public delegate void ParticipantInfoDisplayEvent();
	public event ParticipantInfoDisplayEvent InfoShowed;
	public event ParticipantInfoDisplayEvent ResultShowed;

	[SerializeField]
	private List<MatchingProfileItem> _matchingProfileItems;
    [SerializeField]
    private UserService _userService;
    [SerializeField]
    private LuckyChanceService _luckyChanceService;
    [SerializeField]
    private RewardInfoAlert _rewardInfoAlert;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _levelUpAudio;
    [SerializeField]
    private AudioClip _badgeAcquiringAudio;
    private int _participantQty;
	private List<ParticipantData> _participantDatas;
	private List<RaceResult> _raceResults;
	private int _prizeGold;
    private bool _isDebug = true;

	void Start () 
	{
	
	}





	public void Init(ParticipantData userData, int participantQty)
	{
		_participantQty = participantQty;


        for (int i = 0; i < _matchingProfileItems.Count; i++)
		{
			if(i < participantQty)
			{
				_matchingProfileItems[i].gameObject.SetActive(true);
				_matchingProfileItems[i].InfoShowed += OnProfileItemShowed;
				if(i == 0) _matchingProfileItems[i].ShowInfo(userData); 
				else _matchingProfileItems[i].ShowProgress();
				
				Vector3 position = transform.localPosition;
				position.x = -((150 * participantQty) - 10) / 2;
				transform.localPosition = position;
			}
			else
			{
				_matchingProfileItems[i].gameObject.SetActive(false);
			}
		}		
	}

    private int detailShowedCnt = 0;
    private void OnProfileItemShowed()
    {
        if (detailShowedCnt >= _participantQty - 1)
        {
            for (int i = 0; i < _participantQty; i++)
            {
                _matchingProfileItems[i].InfoShowed -= OnProfileItemShowed;
            }
            detailShowedCnt = 0;
            if (InfoShowed != null) InfoShowed();
            return;
        }
        detailShowedCnt++;
    }

    public void SetReplayers(List<ParticipantData> participantDatas)
	{
        //Debug.Log("participantDatas.Count---------------: " + participantDatas.Count);

        _participantDatas = participantDatas;

		for(int i = 0; i < _matchingProfileItems.Count; i++)
		{
			if(i < participantDatas.Count)
			{
				_matchingProfileItems[i+1].ShowInfo(participantDatas[i]);
			}
		}
	}





	public void ShowParticipantResult(List<ParticipantData> participantDatas, Reward raceReward)
	{
        if (_isDebug) Debug.Log("ParticipantInfoDisplay - ShowResult / participantDatas.count: " + participantDatas.Count);

		_participantDatas = participantDatas;
		_prizeGold = raceReward.gold;
		_raceResults = new List<RaceResult>();
        
        for (int i = 0; i < _participantDatas.Count; i++)
		{				
			RaceResult raceResult = new RaceResult();
			raceResult.participantData = _participantDatas[i];
			if(i == 0)
			{
                raceResult.reward = raceReward;
			}
			_raceResults.Add(raceResult);
            _matchingProfileItems[i].ShowRaceResult(raceResult);
		}

		DOVirtual.DelayedCall(0.5f, OnRankMoved);
	}

    // 순위 변동 완료
    private void OnRankMoved()
	{
        // 받을 상금이 없다면
        if (IsPrizeEmpty())
        {
            // 경험치 획득
            CountExp();
        }
        else
        {
            // 상금지급
            _matchingProfileItems[0].ShowPrizeWidget(_raceResults[0].reward.gold);
            _rewardInfoAlert.ShowPrizeAlert(_raceResults[0].reward.gold);
            _userService.Save(_raceResults[0].reward.gold);
            DOVirtual.DelayedCall(1.4f, CountExp);
        }
    }

    // 경험치 획득
    private void CountExp()
    {
        _matchingProfileItems[0].LevelUp += OnLevelUp;
        _matchingProfileItems[0].ExpCountEnd += OnExpCountEnd;
        _matchingProfileItems[0].CountExp(_raceResults[0].reward.exp);
    }

    private void OnLevelUp()
    {
        _rewardInfoAlert.ShowLevelUpAlert(LvExpTable.instance.GetLevelUpReward(_userService.GetLevel()));

        _userService.Save(LvExpTable.instance.GetLevelUpReward(_userService.GetLevel()));

        _audioSource.clip = _levelUpAudio;
        _audioSource.Play();
    }

    private void OnExpCountEnd()
    {
        _matchingProfileItems[0].LevelUp -= OnLevelUp;
        _matchingProfileItems[0].ExpCountEnd -= OnExpCountEnd;

        if (_luckyChanceService.IsBadgeFull()) OnEnd();
        else DOVirtual.DelayedCall(1.2f, AwardBadge);
    }

    private void AwardBadge()
    {
        _luckyChanceService.AddBadge(_raceResults[0].reward.badge);

        _audioSource.clip = _badgeAcquiringAudio;
        _audioSource.Play();

        if (_raceResults[0].reward.badge == LuckyChanceBadge.badge.NONE)
        {
            OnEnd();
        }
        else
        {
            _rewardInfoAlert.ShowBadgeAlert(_raceResults[0].reward.badge);
            DOVirtual.DelayedCall(1.4f, OnEnd);
        }
    }

	private void OnEnd()
	{
        if (ResultShowed != null) ResultShowed();
    }





    private bool IsPrizeEmpty()
    {
        return (_raceResults[0].reward.gold == 0) ? true : false;
    }
}
