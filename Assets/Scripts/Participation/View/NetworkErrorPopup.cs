﻿namespace Com.Mod.ThumbRun.Participation.View
{
	using System.Collections;
	using System.Collections.Generic;
    using Com.Mod.ThumbRun.Race.Application;
    using Com.Mod.ThumbRun.User.Application;
    using GameDataEditor;
    using UnityEngine;

	public class NetworkErrorPopup : MonoBehaviour 
	{
        public delegate void NetworkErrorPopupEvent();
        public event NetworkErrorPopupEvent Click;

        //[SerializeField]
        //private PlayScene _playScene;
        //[SerializeField]
        //private UserService _userService;
        //[SerializeField]
        //private EntryService _entryServie;

        void Start()
        {
            gameObject.transform.localPosition = Vector3.zero;
            HidePopup();
        }

        void Update()
        {

        }



        public void ShowPopup()
        {
            gameObject.SetActive(true);
        }

        public void HidePopup()
        {
            gameObject.SetActive(false);
        }



        public void PressGoLobbyButton()
        {
            if (Click != null) Click();
            
            // Play Scene에서
            //GDEEntryItemData entryInfo = _entryServie.GetEntryInfo(_entryServie.GetCurrentEntryIndex());
            //_userService.Save(entryInfo.entryFee);
            //_playScene.GoHome();
        }
	}
}