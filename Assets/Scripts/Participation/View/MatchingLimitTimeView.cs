﻿namespace Com.Mod.ThumbRun.Participation.View
{
    using System.Collections;
    using System.Collections.Generic;
    using DG.Tweening;
    using UnityEngine;
    using Race.Application;
    using GameDataEditor;
    using User.Application;

    public class MatchingLimitTimeView : MonoBehaviour 
	{
		[SerializeField]
		private UILabel _remainingTimeLabel;
		[SerializeField]
		private int limitTime = 23;
		[SerializeField]
		private NetworkErrorPopup _networkErrorPopup;
        [SerializeField]
        private EntryService _entryService;
        [SerializeField]
        private UserService _userService;
        [SerializeField]
        private PlayScene _playScene;
        
        private int remainingTime = 0;

		void Awake ()
		{
		}

		void Start () 
		{
		}
		
		void Update () 
		{
			
		}

		void OnDestroy()
		{
		}		




		private void OnTimeOut()
		{
            _networkErrorPopup.Click += OnNetworkErrorPopupClick;
            _networkErrorPopup.ShowPopup();
			Hide();
		}

        private void OnNetworkErrorPopupClick()
        {
            _networkErrorPopup.Click -= OnNetworkErrorPopupClick;
            GDEEntryItemData entryInfo = _entryService.GetEntryInfo(_entryService.GetCurrentEntryIndex());
            _userService.Save(entryInfo.entryFee);
            _playScene.GoHome();
        }


        private void OnUpdate()
		{
			_remainingTimeLabel.text = remainingTime.ToString();
        }





        public void StartTimer()
		{
			remainingTime = limitTime;
			DOTween.Kill("MatchingLimitTime." + GetInstanceID());
			DOTween.To(()=>remainingTime, x=>remainingTime=x, 0, limitTime).SetId("MatchingLimitTime." + GetInstanceID()).SetEase(Ease.Linear).OnUpdate(OnUpdate).OnComplete(OnTimeOut);
		}

		public void StopTimer()
		{
			DOTween.Kill("MatchingLimitTime." + GetInstanceID());
			remainingTime = limitTime;
		}

		public void Show()
		{
			gameObject.SetActive(true);
		}

		public void Hide()
		{
			gameObject.SetActive(false);
		}
	}
}