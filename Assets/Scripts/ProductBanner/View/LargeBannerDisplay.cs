﻿namespace Com.Mod.ThumbRun.ProductBanner.View
{
    using UnityEngine;
    using ThumbRun.View;
    using Store.View;
    using Application;
    using System.Collections.Generic;
    using DG.Tweening;
    using TimeStamp;
    using Participation.View;

    public class LargeBannerDisplay : MonoBehaviour
    {
        public delegate void LargeBannerDisplayEvent(string id);
        public event LargeBannerDisplayEvent Click;

        [SerializeField]
        private List<LargeBannerButton> _largeBannerButtons;
        [SerializeField]
        private List<UILabel> _remainingTimeLabelList;
        private int _currentDisplayIndex = 0;
        [SerializeField]
        private StorePopup _storepopup;
        [SerializeField]
        private NetworkErrorPopup _networkErrorPopup;
        private bool _isDebug = false;
        private ProductBannerService _productBannerService;

        private void Awake()
        {
            _productBannerService = GameObject.FindGameObjectWithTag("ProductBanner").GetComponent<ProductBannerService>();
            _productBannerService.Prepared += OnPreapred;
            _productBannerService.BannerHide += OnBannerHide;
            if (_productBannerService.isPrepared) OnPreapred();

            for (int i = 0; i < _largeBannerButtons.Count; i++)
            {
                _largeBannerButtons[i].Click += OnBannerClick;
            }
        }

        void Start()
        {

        }

        void Update()
        {
            for (int i = 0; i < _remainingTimeLabelList.Count; i++)
            {
                if (_remainingTimeLabelList[i] != null && _productBannerService.GetRemainingTimeByIndex(i) != null)
                {
                    _remainingTimeLabelList[i].text = _productBannerService.GetRemainingTimeByIndex(i);
                }
            }
        }

        private void OnDestroy()
        {
            _productBannerService.Prepared -= OnPreapred;
            _productBannerService.BannerHide -= OnBannerHide;

            for (int i = 0; i < _largeBannerButtons.Count; i++)
            {
                _largeBannerButtons[i].Click -= OnBannerClick;
            }
        }





        private void OnPreapred()
        {
            if (!_productBannerService.isPrepared) return;

            ShowBanner(_currentDisplayIndex);
            DOVirtual.DelayedCall(4f, Next).SetId("ProductSmallBanner.Interval." + GetInstanceID());
        }

        private void OnBannerHide(int bannerIndex, string bannerName)
        {
            if (_currentDisplayIndex == bannerIndex) Next();
        }

        private void OnBannerClick(string id)
        {
            if (id == "0")
            {
                _storepopup.OpenPopup();
            }
            else if (id == "1")
            {
                _storepopup.OpenPopup();
            }
            else if (id == "2")
            {
                
            }

            if (Click != null) Click(id);
        }

        private void OnNetworkErrorPopupClick()
        {
            _networkErrorPopup.Click -= OnNetworkErrorPopupClick;
            _networkErrorPopup.HidePopup();
        }





        private void Next()
        {
            _currentDisplayIndex++;
            if (_currentDisplayIndex >= _largeBannerButtons.Count) _currentDisplayIndex = 0;
            ShowBanner(_currentDisplayIndex);
        }

        private void ShowBanner(int index)
        {
            if (_isDebug) Debug.Log(">>>>> LargeBannerDisplay - ShowBanner / _currentDisplayIndex: " + _currentDisplayIndex + " / isShow: " + _productBannerService.GetBannerList()[index].isShow);
            if (!_productBannerService.GetBannerList()[index].isShow)
            {
                //Debug.Log(">>>>> Force Next / index: " + index);
                Next();
                return;
            }

            //if(index == 2) // AD
            //{
            //    if(NewADManager.instance.IsReady())
            //    {
            //        Debug.Log(">>>>> Force Next / index: " + index);
            //        Next();
            //        return;
            //    }
            //}

            for (int i = 0; i < _largeBannerButtons.Count; i++)
            {
                if (i == index)
                {
                    _largeBannerButtons[i].Show();
                }
                else
                {
                    _largeBannerButtons[i].Hide();
                }
            }
            DOTween.Kill("ProductSmallBanner.Interval." + GetInstanceID());
            DOVirtual.DelayedCall(4f, Next).SetId("ProductSmallBanner.Interval." + GetInstanceID());
        }
    }
}