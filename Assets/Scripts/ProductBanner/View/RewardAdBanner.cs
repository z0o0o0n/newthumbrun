﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardAdBanner : MonoBehaviour
{
    [SerializeField]
    private UIGold _uiGold;
    [SerializeField]
    private FreeGoldProvisionData _freeGoldProvisionData;

    void Start ()
    {
        _uiGold.SetGold(_freeGoldProvisionData.rewardADGold);
    }
	
	void Update ()
    {
		
	}
}
