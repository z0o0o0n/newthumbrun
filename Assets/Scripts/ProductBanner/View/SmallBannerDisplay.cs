﻿namespace Com.Mod.ThumbRun.ProductBanner.View
{
    using UnityEngine;
    using ThumbRun.View;
    using Store.View;
    using Application;
    using System.Collections.Generic;
    using DG.Tweening;
    using Participation.View;
    using TimeStamp;

    public class SmallBannerDisplay : MonoBehaviour
    {
        [SerializeField]
        private List<SmallBannerButton> _smallBannerButtons;
        [SerializeField]
        private List<UILabel> _remainingTimeLabelList;
        private int _currentDisplayIndex = 0;
        [SerializeField]
        private StorePopup _storepopup;
        [SerializeField]
        private NetworkErrorPopup _networkErrorPopup;
        private ProductBannerService _productBannerService;

        private void Awake()
        {
            _productBannerService = GameObject.FindGameObjectWithTag("ProductBanner").GetComponent<ProductBannerService>();
            _productBannerService.Prepared += OnPreapred;
            _productBannerService.BannerHide += OnBannerHide;
            if (_productBannerService.isPrepared) OnPreapred();

            for(int i = 0; i < _smallBannerButtons.Count; i++)
            {
                _smallBannerButtons[i].Click += OnSmallBannerClick;
            }

            CoolingTimeStampManager.instance.CoolingStarted += OnCoolingTimeStampStarted;
            CoolingTimeStampManager.instance.CoolingEnded += OnCoolingTimeStampEnded;
        }

        void Start()
        {
            
        }

        void Update()
        {
            for(int i = 0; i < _remainingTimeLabelList.Count; i++)
            {
                if(_remainingTimeLabelList[i] != null && _productBannerService.GetRemainingTimeByIndex(i) != null)
                {
                    _remainingTimeLabelList[i].text = _productBannerService.GetRemainingTimeByIndex(i);
                }
            }
        }

        private void OnDestroy()
        {
            _productBannerService.Prepared -= OnPreapred;
            _productBannerService.BannerHide -= OnBannerHide;

            for (int i = 0; i < _smallBannerButtons.Count; i++)
            {
                _smallBannerButtons[i].Click -= OnSmallBannerClick;
            }

            CoolingTimeStampManager.instance.CoolingStarted -= OnCoolingTimeStampStarted;
            CoolingTimeStampManager.instance.CoolingEnded -= OnCoolingTimeStampEnded;
        }





        private void OnPreapred()
        {
            if (!_productBannerService.isPrepared) return;

            if(CoolingTimeStampManager.instance.GetCoolingTimeStamp("RewardAdGold").isCooling)
            {
                _smallBannerButtons[2].gameObject.SetActive(false);
            }
            else
            {
                _smallBannerButtons[2].gameObject.SetActive(true);
            }

            ShowBanner(_currentDisplayIndex);
            DOVirtual.DelayedCall(4f, Next).SetId("ProductSmallBanner.Interval." + GetInstanceID());
        }

        private void OnBannerHide(int bannerIndex, string bannerName)
        {
            if (_currentDisplayIndex == bannerIndex) Next();
        }

        private void OnSmallBannerClick(string id)
        {
            if(id == "0")
            {
                _storepopup.OpenPopup();
            }
            else if (id == "1")
            {
                _storepopup.OpenPopup();
            }
            else if (id == "2")
            {
                Debug.Log(">>>>>>>>>>>>>>>>>>>>> is end: " + TimeStampManager.instance.GetTimeStamp("RewardAD").isEnd);

                if(CoolingTimeStampManager.instance.GetCoolingTimeStamp("RewardAdGold").isCooling)
                {
                    _networkErrorPopup.Click += OnNetworkErrorPopupClick;
                    _networkErrorPopup.ShowPopup();
                    return;
                }

                if (NewADManager.instance.PlayAD("GoldRewardAD"))
                {
                    CoolingTimeStampManager.instance.Use("RewardAdGold");
                    return;
                }
                else
                {
                    _networkErrorPopup.Click += OnNetworkErrorPopupClick;
                    _networkErrorPopup.ShowPopup();
                    return;
                }
            }
        }

        private void OnCoolingTimeStampStarted(CoolingTimeStamp coolingTimeStamp)
        {
            _smallBannerButtons[2].gameObject.SetActive(false);
        }

        private void OnCoolingTimeStampEnded(CoolingTimeStamp coolingTimeStamp)
        {
            _smallBannerButtons[2].gameObject.SetActive(true);
        }

        private void OnNetworkErrorPopupClick()
        {
            _networkErrorPopup.Click -= OnNetworkErrorPopupClick;
            _networkErrorPopup.HidePopup();
        }





        private void Next()
        {
            _currentDisplayIndex++;
            if (_currentDisplayIndex >= _smallBannerButtons.Count) _currentDisplayIndex = 0;
            ShowBanner(_currentDisplayIndex);
        }

        private void ShowBanner(int index)
        {
            //Debug.Log(">>>>> Show Banner / index: " + index);
            if (!_productBannerService.GetBannerList()[index].isShow)
            {
                //Debug.Log(">>>>> Force Next / index: " + index);
                Next();
                return;
            }

            //if(index == 2) // AD
            //{
            //    if(NewADManager.instance.IsReady())
            //    {
            //        Debug.Log(">>>>> Force Next / index: " + index);
            //        Next();
            //        return;
            //    }
            //}

            for(int i = 0; i < _smallBannerButtons.Count; i++)
            {
                if(i == index)
                {
                    _smallBannerButtons[i].Show();
                }
                else
                {
                    _smallBannerButtons[i].Hide();
                }
            }
            DOTween.Kill("ProductSmallBanner.Interval." + GetInstanceID());
            DOVirtual.DelayedCall(4f, Next).SetId("ProductSmallBanner.Interval." + GetInstanceID());
        }




        public void PressBanner()
        {
            //if (_bannerDisplay.currentIndex == 2) _goldInfoPopup.Open();
            //else _storePopup.OpenPopup();
        }
    }
}