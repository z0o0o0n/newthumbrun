﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallBannerButton : MonoBehaviour
{
    public delegate void SmallBannerButtonEvent(string id);
    public event SmallBannerButtonEvent Click;

    [SerializeField]
    private string _id;
    private bool _isEnabled = false;

	void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}




    public void OnClick()
    {
        if(_isEnabled)
        {
            if (Click != null) Click(_id);
        }
    }





    public void Show()
    {
        _isEnabled = true;
        DOTween.Kill("SmallBannerMove." + GetInstanceID());
        transform.DOLocalMoveX(0f, 0.3f).SetId("SmallBannerMove." + GetInstanceID()).SetEase(Ease.InOutCubic);
    }

    public void Hide()
    {
        _isEnabled = false;
        DOTween.Kill("SmallBannerMove." + GetInstanceID());
        transform.DOLocalMoveX(200f, 0.3f).SetId("SmallBannerMove." + GetInstanceID()).SetEase(Ease.InOutCubic);
    }
}
