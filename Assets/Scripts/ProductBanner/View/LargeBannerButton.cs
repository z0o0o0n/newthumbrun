﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LargeBannerButton : MonoBehaviour
{
    public delegate void LargeBannerButtonEvent(string id);
    public event LargeBannerButtonEvent Click;

    [SerializeField]
    private string _id;
    private bool _isEnabled = false;

    void Start()
    {

    }

    void Update()
    {

    }




    public void OnClick()
    {
        if (_isEnabled)
        {
            if (Click != null) Click(_id);
        }
    }





    public void Show()
    {
        _isEnabled = true;
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        _isEnabled = false;
        gameObject.SetActive(false);
    }
}
