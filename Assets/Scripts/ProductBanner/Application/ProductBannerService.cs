﻿namespace Com.Mod.ThumbRun.ProductBanner.Application
{
    using System;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.Data;
    using Com.Mod.TimeStamp;
    using DG.Tweening;
    using GameDataEditor;
    using UnityEngine;
    using Domain;

    public class ProductBannerService : MonoBehaviour 
	{
		public delegate void ProductBannerEvent();
        public delegate void ProductBannerVisibleEvent(int bannerIndex, string bannerName);
        public event ProductBannerEvent Prepared;
        public event ProductBannerVisibleEvent BannerShow;
        public event ProductBannerVisibleEvent BannerHide;

        [SerializeField]
        private ProductBanner _productBanner;
        private bool _isPrepared = false;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }

		void Awake ()
		{
            _productBanner.Prepared += OnPrepared;
            _productBanner.BannerShow += OnBannerShow;
            _productBanner.BannerHide += OnBannerHide;

        }

		void Start () 
		{
			
		}
		
		void Update () 
		{
		}

		void OnDeatroy()
		{
            _productBanner.Prepared -= OnPrepared;
            _productBanner.BannerShow -= OnBannerShow;
            _productBanner.BannerHide -= OnBannerHide;
        }





        public List<GDEProductBannerItemData> GetBannerList()
        {
            return _productBanner.GetBannerList();
        }

        public string GetRemainingTime(string bannerName)
		{
            return _productBanner.GetRemainingTimeByName(bannerName);
        }

        public string GetRemainingTimeByIndex(int bannerIndex)
        {
            return _productBanner.GetRemainingTimeByIndex(bannerIndex);
        }





        private void OnPrepared()
		{
            _isPrepared = true;

            if (Prepared != null) Prepared();
		}

        private void OnBannerShow(int bannerIndex, string bannerName)
        {
            if (BannerShow != null) BannerShow(bannerIndex, bannerName);
        }

        private void OnBannerHide(int bannerIndex, string bannerName)
        {
            if (BannerHide != null) BannerHide(bannerIndex, bannerName);
        }
    }
}