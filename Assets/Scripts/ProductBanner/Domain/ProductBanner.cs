﻿using Com.Mod.ThumbRun.Event;
using Com.Mod.TimeStamp;
using GameDataEditor;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Com.Mod.ThumbRun.ProductBanner.Domain
{
	public class ProductBanner : MonoBehaviour 
	{
        public delegate void ProductBannerEvent(int bannerIndex, string bannerName);
        public event DataEvent.DataEventHandler Prepared;
        public event ProductBannerEvent BannerShow;
        public event ProductBannerEvent BannerHide;

        private GDEProductBannerData _rawData;
        private string _logPrefix = "################---- ";
        private bool _isDebug = true;
        private static ProductBanner _instance;

        private void Awake()
        {
            if(_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void Start()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.ProductBanner_ProductBanner, out _rawData))
            {
                Debug.Log("ProductBannerData Prepared");
                //CreateTimeStamps();
                //ResetTimeStamp();
                TimeStampManager.instance.Timeout += OnTimeOut;

                Init();
                if (Prepared != null) Prepared();
            }
            else
            {
                Debug.LogError("Read Error ProductBannerData");
            }
        }
		
		void Update () 
		{
		
		}

        private void OnDestroy()
        {
            if(TimeStampManager.instance != null)
            {
                TimeStampManager.instance.Timeout -= OnTimeOut;
            }
        }




        private void Init()
        {
            List<GDEProductBannerItemData> bannerList = _rawData.bannerItemList;

            for (int i = 0; i < bannerList.Count; i++)
            {
                if(!bannerList[i].isManual)
                {
                    if (IsEnd(bannerList[i].name + "_Display")) // 디스플레이 종료
                    {
                        bannerList[i].isShow = false;
                    }
                    if (IsEnd(bannerList[i].name + "_Cool")) // 쿨타임 종료
                    {
                        bannerList[i].isShow = true;
                        TimeStampManager.instance.Reset(bannerList[i].name + "_Display", false);
                        TimeStampManager.instance.Reset(bannerList[i].name + "_Cool", false);
                    }
                }
                else
                {
                    bannerList[i].isShow = true;
                }
            }
        }





        private void OnTimeOut(string key)
        {
            List<GDEProductBannerItemData> bannerList = _rawData.bannerItemList;

            //Debug.Log(_logPrefix + "TimeOut / key: " + key);

            for (int i = 0; i < bannerList.Count; i++)
            {
                if (!bannerList[i].isManual)
                {
                    if (key == bannerList[i].name + "_Display") // 디스플레이 종료
                    {
                        bannerList[i].isShow = false;
                        // _activatedBannerList.Remove(bannerList[i].name);
                        //LogActivatedBannerList(GetActivatedBannerList());
                        if (BannerHide != null) BannerHide(i, bannerList[i].name);
                    }
                    if (key == bannerList[i].name + "_Cool") // 쿨타임 종료
                    {
                        bannerList[i].isShow = true;
                        TimeStampManager.instance.Reset(bannerList[i].name + "_Display", false);
                        TimeStampManager.instance.Reset(bannerList[i].name + "_Cool", false);
                        //if (BannerShow != null) BannerShow(i, bannerList[i].name);
                        // _activatedBannerList.Add(bannerList[i].name, bannerList[i]);
                        //LogActivatedBannerList(GetActivatedBannerList());
                    }
                }
                else
                {
                    bannerList[i].isShow = true;
                }
            }
        }





        //private void CreateTimeStamps()
        //{
            //List<GDEProductBannerItemData> bannerList = _rawData.bannerItemList;
            //for (int i = 0; i < bannerList.Count; i++)
            //{
            //    if (!bannerList[i].isManual) //상시노출이 아니라면 (리워드 AD 처럼)
            //    {
            //        TimeStampManager.instance.Add(bannerList[i].name + "_Display", bannerList[i].displayTime);
            //        TimeStampManager.instance.Add(bannerList[i].name + "_Cool", bannerList[i].displayTime + bannerList[i].coolTime);
            //    }
            //}
            //TimeStampManager.instance.Init();
        //}

        private void ResetTimeStamp()
        {
            List<GDEProductBannerItemData> bannerList = _rawData.bannerItemList;
            for (int i = 0; i < bannerList.Count; i++)
            {
                if (!bannerList[i].isManual) //상시노출이 아니라면 (리워드 AD 처럼)
                {
                    bannerList[i].isShow = true;
                    TimeStampManager.instance.Reset(bannerList[i].name + "_Display", false);
                    TimeStampManager.instance.Reset(bannerList[i].name + "_Cool", false);
                }
            }
        }

        public bool IsEnd(string key)
        {
            //if (_isDebug) Debug.Log(_logPrefix + "key: " + key + " / isEnd: " + TimeStampManager.instance.GetTimeStamp(key).isEnd);
            return TimeStampManager.instance.GetTimeStamp(key).isEnd;
        }

        //private void LogActivatedBannerList(Dictionary<string, GDEProductBannerItemData> list)
        //{
        //    string log = "LogActivatedBannerList [";
        //    foreach (string key in list.Keys)
        //    {
        //        log += list[key].name + ",";
        //    }
        //    Debug.Log(log + "]");
        //}





        public List<GDEProductBannerItemData> GetBannerList()
        {
            return _rawData.bannerItemList;
        }

        public string GetRemainingTimeByName(string bannerName)
        {
            string result = "";
            TimeSpan remainingTime = TimeStampManager.instance.GetRemainingTime(bannerName + "_Display");
            if (remainingTime.TotalSeconds > 0) result = string.Format("{0:D2}:{1:D2}:{2:D2}", remainingTime.Hours, remainingTime.Minutes, remainingTime.Seconds);
            else result = "00:00:00";
            return result;
        }

        public string GetRemainingTimeByIndex(int bannerIndex)
        {
            if (_rawData == null) return null;
            //Debug.Log("banner index: " + bannerIndex + " / name: " + _rawData.bannerItemList[bannerIndex].name);
            return GetRemainingTimeByName(_rawData.bannerItemList[bannerIndex].name);
        }

        //private Dictionary<string, GDEProductBannerItemData> GetActivatedBannerList()
        //{
        //    Dictionary<string, GDEProductBannerItemData> result = new Dictionary<string, GDEProductBannerItemData>();
        //    List<GDEProductBannerItemData> bannerList = _productBanner.GetRawData().bannerItemList;
        //    for (int i = 0; i < bannerList.Count; i++)
        //    {
        //        if (bannerList[i].isShow)
        //        {
        //            result.Add(bannerList[i].name, bannerList[i]);
        //        }
        //    }
        //    return result;
        //}
    }
}