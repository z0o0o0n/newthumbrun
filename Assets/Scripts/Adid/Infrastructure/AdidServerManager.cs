﻿namespace Com.Mod.ThumbRun.Adid.Infrastructure
{
    using App;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class AdidServerManager : MonoBehaviour
    {
        public delegate void CouponServerEventHandler(string wwwText);
        public event CouponServerEventHandler Error;

        [SerializeField]
        private string _host;
        [SerializeField]
        private string _adidRequestURI;
        [SerializeField]
        private string _clientID;
        private HttpRequest _httpRequest;

        private void Awake()
        {
            _httpRequest = gameObject.AddComponent(typeof(HttpRequest)) as HttpRequest;
            _httpRequest.Complete += OnCompleted;
            _httpRequest.TimeOut += OnError;
            _httpRequest.Error += OnError;
        }

        void Start()
        {
        }

        void Update()
        {
        }

        private void OnDestroy()
        {
            _httpRequest.Complete -= OnCompleted;
            _httpRequest.TimeOut -= OnError;
            _httpRequest.Error -= OnError;
        }




        public void RequestAdid(string adid)
        {
            string platform = Platform.GetPlatformType().ToString();
            string uuid = SystemInfo.deviceUniqueIdentifier;

            Dictionary<string, string> stringArgs = new Dictionary<string, string>();
            stringArgs.Add("client_id", _clientID);
            stringArgs.Add("id", adid);
            stringArgs.Add("platform", platform);

            string uri = _host + _adidRequestURI;
            _httpRequest.Post("requestAdid", uri, stringArgs);

            Debug.Log("////////////////// requestAdid");
        }




        private void OnCompleted(string id, string wwwText)
        {
            if (id == "requestAdid")
            {
                Debug.Log("//////////////// Adid Completed: " + wwwText);
            }
        }

        private void OnError(string id, string wwwText)
        {
            if (id == "requestAdid")
            {
                Debug.Log("////////////////// Adid Error: " + wwwText);
                if (Error != null) Error(wwwText);
            }
        }
    }
}