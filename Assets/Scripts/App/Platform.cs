﻿namespace Com.Mod.ThumbRun.App
{
    using UnityEngine;

    public class Platform : MonoBehaviour
    {
        public enum type { ANDROID, IOS };

        public static Platform.type GetPlatformType()
        {
            Platform.type result = Platform.type.ANDROID;
#if UNITY_IPHONE
            result = Platform.type.IOS;
#elif UNITY_ANDROID
            result = Platform.type.ANDROID;
#endif
            return result;
        }
    }
}