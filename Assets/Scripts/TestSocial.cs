﻿using UnityEngine;
using UnityEngine.SocialPlatforms;
using System.Collections;

public class TestSocial : MonoBehaviour
{
    public UILabel _textLabel;

    void Start ()
    { 
        // 증명 (콜백함수 등록)
        // This call Needs to BE MADE before we CAN Proceed to Other Calls in the Social API 
        if (!IsUserAuthenticated())
        {
            Debug.Log("Social.localUser.authenticated: " + Social.localUser.authenticated);
            Social.localUser.Authenticate(ProcessAuthentication);
        }
    }

    public bool IsUserAuthenticated()
    {
        if (Social.localUser.authenticated)
        {
            return true;
        }
        else
        {
            Debug.Log("User not Authenticated");
            return false;
        }
    }

    // 성공 시 Social.localUser에는 서버에서 받은 데이터를 담고있다.
    void ProcessAuthentication (bool success)
    {
        if (success)
        { 
            Debug.Log ( "Authenticated, checking achievements" ); 
            
            // 로드된 업적을 요청하고 콜백 등록
            //Social.LoadAchievements (ProcessLoadedAchievements);

            ReportScore(17130, "TestRecord");
        } 
        else
        {
            Debug.Log("Failed to authenticate"); 
        }
    } 
    
    void ProcessLoadedAchievements (IAchievement [] achievements)
    { 
        if (achievements.Length == 0 ) 
        {
            Debug.Log("Error : no achievements found");
        }
        else
        {
            Debug.Log("Got" + achievements.Length + "achievements "); 
        }

        // 이렇게도 콜백함수를 호출할 수 있지롱
        //Social.ReportProgress ( "Achievement01" , 100.0 , result => {
        //        if (result) 
        //            Debug.Log ( "Successfully reported achievement progress" );
        //        else
        //            Debug.Log(" Failed to report achievement "); 
        //}); 
    }

    void ReportScore(long score, string leaderboardID)
    {
        Debug.Log("Reporting score " + score + " on leaderboard " + leaderboardID);

        if (!IsUserAuthenticated())
        {
            Social.ReportScore(score, leaderboardID, ResultReportScore);
        }
    }
    //Debug.Log(success ? "Reported score successfully" : "Failed to report score");

    void ResultReportScore(bool success)
    {
        if(success)
        {
            Debug.Log("Reported score successfully");
            //Social.ShowLeaderboardUI();
            //Social.LoadScores("TestRecord", ResultLoadScores);

            ILeaderboard leaderboard = Social.CreateLeaderboard();
            leaderboard.id = "TestRecord";
            leaderboard.timeScope = TimeScope.Today;
            leaderboard.LoadScores(result =>
            {
                _textLabel.text = leaderboard.scores.Length.ToString();
                Debug.Log("Received" + leaderboard.scores.Length + "scores");
                foreach (IScore score in leaderboard.scores) Debug.Log(score);
            });
        }
        else
        {
            Debug.Log("Failed to report score");
        }
    }

    void ResultLoadScores(IScore[] scores)
    {
        Debug.Log("scores Length: " + scores.Length);
        if (scores.Length > 0)
        {
            for(int i = 0; i < scores.Length; i++ )
            {
                Debug.Log("ID: " + scores[i].userID + " Data: " + scores[i].date);
            }
        }
    }
}
