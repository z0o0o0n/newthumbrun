﻿namespace Com.Mod.ThumbRun.Coupon.Infrastructure
{
    using System.Collections;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.User.Application;
    using UnityEngine;
    using App;

    public class CouponServerManager : MonoBehaviour 
	{
		public delegate void CouponServerEventHandler(string wwwText);
		public event CouponServerEventHandler CheckCompleted;
		public event CouponServerEventHandler CheckError;

		[SerializeField]
		private string _host;
		[SerializeField]
		private string _couponRequestURI;
		[SerializeField]
		private string _clientID;
		[SerializeField]
		private string _secretKey;
		[SerializeField]
		private UserService _userService;
        private HttpRequest _httpRequest;

        private void Awake()
        {
            _httpRequest = gameObject.AddComponent(typeof(HttpRequest)) as HttpRequest;
            _httpRequest.Complete += OnCompleted;
            _httpRequest.TimeOut += OnError;
            _httpRequest.Error += OnError;
        }

        void Start () 
		{
        }
		
		void Update () 
		{
		}

        private void OnDestroy()
        {
            _httpRequest.Complete -= OnCompleted;
            _httpRequest.TimeOut -= OnError;
            _httpRequest.Error -= OnError;
        }




        public void UseCoupon(string couponCode)
		{
            

			string platform = Platform.GetPlatformType().ToString();
			Dictionary<string, string> stringArgs = new Dictionary<string, string>();
			stringArgs.Add("client_id", _clientID);
			stringArgs.Add("coupon", couponCode);
			stringArgs.Add("uuid", SystemInfo.deviceUniqueIdentifier);
			stringArgs.Add("nickname", _userService.GetNickname());
			stringArgs.Add("platform", platform);

			string uri = _host + _couponRequestURI;
            _httpRequest.Post("couponCheck", uri, stringArgs);
		}

		public string GetClientId()
		{
			return _clientID;
		}

		public string GetSecretKey()
		{
			return _secretKey;
		}

		


		private void OnCompleted(string id, string wwwText)
		{
            if (id == "couponCheck")
            {
                Debug.Log("-----> Coupon Completed: " + wwwText);
                if (CheckCompleted != null) CheckCompleted(wwwText);
            }
		}

        private void OnError(string id, string wwwText)
		{
            if (id == "couponCheck")
            {
                if (CheckError != null) CheckError(wwwText);
            }
		}
	}
}