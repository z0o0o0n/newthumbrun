﻿namespace Com.Mod.ThumbRun.Coupon.Application
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using Domain;

	public class CouponService : MonoBehaviour 
	{
		public delegate void CouponServiceCheckSuccessEvent(CouponResultType.type resultType, string itemCode, string itemType, int value);
        public delegate void CouponServiceFailEvent(CouponResultType.type resultType);
        public event CouponServiceCheckSuccessEvent CheckSuccess;
		public event CouponServiceFailEvent CheckFail;

		[SerializeField]
		private Coupon _coupon;

		void Awake ()
		{
			_coupon.Success += OnCouponCheckSuccess;
			_coupon.Fail += OnCouponCheckFail;
		}

		void Start () 
		{
			
		}
		
		void Update () 
		{
			
		}

		void OnDestroy()
		{
			_coupon.Success -= OnCouponCheckSuccess;
			_coupon.Fail -= OnCouponCheckFail;
		}




		private void OnCouponCheckSuccess(CouponResultType.type resultType, string itemCode, string itemType, int value)
		{
			if(CheckSuccess != null) CheckSuccess(resultType, itemCode, itemType, value);
		}

		private void OnCouponCheckFail(CouponResultType.type resultType)
		{
			if(CheckFail != null) CheckFail(resultType);
		}




		public void CheckCoupon(string couponCode)
		{
			_coupon.CheckCoupon(couponCode);
		}
	}
}