﻿namespace Com.Mod.ThumbRun.Coupon.Domain
{
    using System.Collections;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.Coupon.Infrastructure;
    using Com.Mod.ThumbRun.Util;
    using LitJson;
    using UnityEngine;
    using System;

    public class Coupon : MonoBehaviour 
	{
		public delegate void CouponSuccessEventHandler(CouponResultType.type resultType, string itemCode, string itemTtype, int value);
        public delegate void CouponFailEventHandler(CouponResultType.type resultType);
        public event CouponSuccessEventHandler Success;
		public event CouponFailEventHandler Fail;

		[SerializeField]
		private CouponServerManager _couponServerManager;
		private CouponResultType.type _result;
		private string _itemCode;
		private string _timestamp;
		private string _hash;
		private string _couponCode;


		void Awake ()
		{
			_couponServerManager.CheckCompleted += OnCouponCheckCompleted;
			_couponServerManager.CheckError += OnCheckError;
		}

		void Start () 
		{
			
		}
		
		void Update () 
		{
			
		}

		void OnDestroy()
		{
			_couponServerManager.CheckCompleted -= OnCouponCheckCompleted;
			_couponServerManager.CheckError -= OnCheckError;
		}




		private void OnCouponCheckCompleted(string wwwText)
		{
			JsonData json = JsonMapper.ToObject(wwwText);

			if(json["result"].ToString().ToUpper() == CouponResultType.type.SUCCESS.ToString()) _result = CouponResultType.type.SUCCESS;
			else if(json["result"].ToString().ToUpper() == CouponResultType.type.FAIL.ToString()) _result = CouponResultType.type.FAIL;
			else if(json["result"].ToString().ToUpper() == CouponResultType.type.EXIST.ToString()) _result = CouponResultType.type.EXIST;
			else if(json["result"].ToString().ToUpper() == CouponResultType.type.LIMIT.ToString()) _result = CouponResultType.type.LIMIT;
			else if(json["result"].ToString().ToUpper() == CouponResultType.type.EXPIRED.ToString()) _result = CouponResultType.type.EXPIRED;

			Debug.Log("-----> Server Result: " + json["result"].ToString());
			if(_result == CouponResultType.type.SUCCESS)
			{
				_itemCode = json["item"].ToString();
				_timestamp = json["timestamp"].ToString();
				_hash = json["hash"].ToString();

				string message = _couponServerManager.GetClientId() + json["result"] + SystemInfo.deviceUniqueIdentifier + _couponCode + _itemCode + _timestamp;
				string token = TokenManager.CreateToken(message, _couponServerManager.GetSecretKey());

				if(token == _hash) 
				{
					Debug.Log("-----> Coupon Hash OK!!!");
					_result = CouponResultType.type.SUCCESS;
                    string[] sep = { "_" };
                    string itemType = _itemCode.Split(sep, StringSplitOptions.None)[0];
                    int value = int.Parse(_itemCode.Split(sep, StringSplitOptions.None)[1]);

                    if (Success != null) Success(_result, _itemCode, itemType, value);
					return;
				}
				else 
				{
					Debug.Log("-----> Fuck the Coupon Hash!!!");
					_result = CouponResultType.type.FAIL;
				}
			}

			if(Fail != null) Fail(_result);
		}

		private void OnCheckError(string wwwText)
		{
			_result = CouponResultType.type.FAIL;
			if(Fail != null) Fail(_result);
		}

		


		public void CheckCoupon(string couponCode)
		{
			_couponCode = couponCode;
			_couponServerManager.UseCoupon(couponCode);
		}
	}
}