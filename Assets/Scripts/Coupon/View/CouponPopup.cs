﻿namespace Com.Mod.ThumbRun.Coupon.View
{
    using System.Collections;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.Controller;
    using Com.Mod.ThumbRun.Coupon.Application;
    using Com.Mod.ThumbRun.Coupon.Domain;
    using Com.Mod.ThumbRun.UI.Popup;
    using UnityEngine;
    using User.Application;

    public class CouponPopup : BasicPopup 
	{
		[SerializeField]
		private SettingMenuPopup _settingMenuPopup;
		[SerializeField]
		private CouponResultBox _resultBox;
		[SerializeField]
		private CouponService _couponService;
		[SerializeField]
		private UIInput _uiInput;
		[SerializeField]
		private UILabel _inputLabel;
        [SerializeField]
        private GameObject _loadingPanel;
        [SerializeField]
        private UserService _userService;

        void Awake ()
		{
			_couponService.CheckSuccess += OnCheckSuccess;
			_couponService.CheckFail += OnCheckFail;
            _loadingPanel.gameObject.SetActive(false);
        }

		void Start () 
		{
			transform.localPosition = Vector2.zero;
			ClosePopup();		
		}
		
		void Update () 
		{
			
		}

		void OnDestroy()
		{
			_couponService.CheckSuccess -= OnCheckSuccess;
			_couponService.CheckFail -= OnCheckFail;
		}




		public void OnCloseButtonClick()
		{
			ClosePopup();
			_settingMenuPopup.OpenPopup();
		}

		public void OnOkButtonClick()
		{
            _loadingPanel.gameObject.SetActive(true);
            _couponService.CheckCoupon(_inputLabel.text);
		}

		private void OnCheckSuccess(CouponResultType.type resultType, string itemCode, string itemType, int value)
		{
            _loadingPanel.gameObject.SetActive(false);
            _uiInput.value = _uiInput.defaultText;
            _inputLabel.text = "";
			_resultBox.Show(resultType);

            _userService.Save(value);
        }

		private void OnCheckFail(CouponResultType.type resultType)
		{
            _loadingPanel.gameObject.SetActive(false);
            _uiInput.value = _uiInput.defaultText;
            _inputLabel.text = "";
            _resultBox.Show(resultType);
		}




		public void OpenPopup()
		{
			_resultBox.Hide();
			base.Open();
		}

		public void ClosePopup()
		{
			_inputLabel.text = _inputLabel.GetComponent<NGUILocalizeText>().GetText();
			base.Close();
		}
	}
}