﻿namespace Com.Mod.ThumbRun.Coupon.View
{
    using System.Collections;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.Coupon.Domain;
    using UnityEngine;

    public class CouponResultBox : MonoBehaviour 
	{
		[SerializeField]
		private UILabel _successLabel;
		[SerializeField]
		private UILabel _failLabel;
		[SerializeField]
		private UILabel _existLabel;
		[SerializeField]
		private UILabel _limitLabel;
		[SerializeField]
		private UILabel _expiredLabel;
		[SerializeField]
		private UISprite _bg;
		[SerializeField]
		private Color _successBgColor;
		[SerializeField]
		private Color _failBgColor;


		void Start () 
		{
			Hide();
		}
		
		void Update () 
		{
			// if(Input.GetKeyUp(KeyCode.Q))
			// {
			// 	Show(CouponResultBox.result.SUCCESS);
			// }
			// if(Input.GetKeyUp(KeyCode.W))
			// {
			// 	Show(CouponResultBox.result.FAIL);
			// }
			// if(Input.GetKeyUp(KeyCode.E))
			// {
			// 	Hide();
			// }
		}




		public void Show(CouponResultType.type result)
		{

			Debug.Log("------------> " + result);
			HideAll();

			_bg.color = _failBgColor;
			_bg.gameObject.SetActive(true);

			if(result == CouponResultType.type.SUCCESS)
			{
				_successLabel.gameObject.SetActive(true);
				_bg.color = _successBgColor;
			}
			else if(result == CouponResultType.type.FAIL)
			{
				_failLabel.gameObject.SetActive(true);
			}
			else if(result == CouponResultType.type.EXIST)
			{
				_existLabel.gameObject.SetActive(true);
			}
			else if(result == CouponResultType.type.LIMIT)
			{
				_limitLabel.gameObject.SetActive(true);
			}
			else if(result == CouponResultType.type.EXPIRED)
			{
				_expiredLabel.gameObject.SetActive(true);
			}
		}

		public void Hide()
		{
			HideAll();
			_bg.gameObject.SetActive(false);
		}




		private void HideAll()
		{
			_successLabel.gameObject.SetActive(false);
			_failLabel.gameObject.SetActive(false);
			_existLabel.gameObject.SetActive(false);
			_limitLabel.gameObject.SetActive(false);
			_expiredLabel.gameObject.SetActive(false);
		}
	}
}