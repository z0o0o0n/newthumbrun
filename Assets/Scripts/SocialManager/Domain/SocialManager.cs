﻿namespace Com.Mod.ThumbRun.Achievement.Domain
{
    using System.Collections;
    using System.Collections.Generic;
    using GameDataEditor;
    using GooglePlayGames;
    using UnityEngine;

    public class SocialManager : MonoBehaviour 
	{
		public delegate void SocialManagerEventHandler();
		public event SocialManagerEventHandler Prepared;

		private bool _isAuthenticated = false;
		private bool _isPrepared = false;
		private GDEAchievementData _rawData;
        private bool _isDebug = false;

		public bool isPrepared
		{
			get{ return _isPrepared; }
		}

		void Start ()
		{
			if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Achievement_Achievement, out _rawData))
			{
				//Debug.Log("+++++ User Data Achievement_Achievement");
				#if UNITY_ANDROID
					Debug.Log("SocialManager - PlayGamesPlatform Activate");
					PlayGamesPlatform.Activate();
					Social.localUser.Authenticate(OnAuthenticated);
                #elif UNITY_IPHONE
                    Social.localUser.Authenticate(OnAuthenticated);
                #endif
            }
            else
			{
				Debug.LogError("Read Error User Data");
			}
		}
		
		void Update () 
		{
			
		}




		private void OnAuthenticated(bool success)
		{
            Debug.Log("SocialManager - OnAuthenticated");
			_isAuthenticated = true;
			_isPrepared = true;

            if(_rawData.isEnteredMap0) ReportAchievement(GPGSIds.achievement_a);
			if(_rawData.isEnteredMap1) ReportAchievement(GPGSIds.achievement_b);
			if(_rawData.isEnteredMap2) ReportAchievement(GPGSIds.achievement_c);
			if(_rawData.isEnteredMap3) ReportAchievement(GPGSIds.achievement_d);
			if(_rawData.isEnteredMap4) ReportAchievement(GPGSIds.achievement_e);
            if (_rawData.isEnteredMap5) ReportAchievement(GPGSIds.achievement_f);
            if (Prepared != null) Prepared();
		}

		private void OnAchievementReportCompleted(bool success)
		{
		}

		private void OnGoldPostCompleted(bool success)
		{
		}




		public void ReportAchievement(string id)
		{
            if (!Social.localUser.authenticated) return;
			if(id == GPGSIds.achievement_a) _rawData.isEnteredMap0 = true;
			else if(id == GPGSIds.achievement_b) _rawData.isEnteredMap1 = true;
			else if(id == GPGSIds.achievement_c) _rawData.isEnteredMap2 = true;
			else if(id == GPGSIds.achievement_d) _rawData.isEnteredMap3 = true;
			else if(id == GPGSIds.achievement_e) _rawData.isEnteredMap4 = true;
            else if (id == GPGSIds.achievement_f) _rawData.isEnteredMap5 = true;
            Social.ReportProgress(id, 100.0f, OnAchievementReportCompleted);
		}

		public void PostGoldToLeaderBoard(int goldAmount)
		{
			Social.ReportScore(goldAmount, GPGSIds.leaderboard_rank_of_earned_golds, OnGoldPostCompleted);
		}

		public void OpenLeaderboardUI()
		{
            if (_isDebug) Debug.Log("------------------> OpenLeaderboardUI / _isAuthenticated: " + _isAuthenticated);
			if(_isAuthenticated)
			{
				Social.ShowLeaderboardUI();
			}
		}
	}
}