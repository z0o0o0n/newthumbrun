﻿using System.Collections;
using System.Collections.Generic;
using Com.Mod.ThumbRun.Achievement.Application;
using UnityEngine;

public class LeaderboardButton : MonoBehaviour 
{
    [SerializeField]
    private UISprite _gameCenterIcon;
    [SerializeField]
    private UISprite _googlePlayIcon;

    void Start ()
    {
#if UNITY_ANDROID
        _gameCenterIcon.gameObject.SetActive(false);
        _googlePlayIcon.gameObject.SetActive(true);
#elif UNITY_IOS
        _gameCenterIcon.gameObject.SetActive(true);
        _googlePlayIcon.gameObject.SetActive(false);
#endif
    }

    void Update ()
    {
		
	}

	public void OnButtonClick()
	{
		SocialManagementService.instance.OpenLeaderboardUI();
	}
}
