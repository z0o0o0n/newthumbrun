﻿namespace Com.Mod.ThumbRun.Achievement.Application
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using Domain;

	public class SocialManagementService : MonoBehaviour 
	{
		public delegate void SocialManagementServiceEventHandler();
		public event SocialManagementServiceEventHandler Prepared;

		private static SocialManagementService _instance;
		[SerializeField]
		private SocialManager _socialManager;
		private bool _isPrepared = false;

		public static SocialManagementService instance
		{
			get{ return _instance; }
		}

		public bool isPrepared
		{
			get{ return _isPrepared; }
		}

		void Awake ()
		{
			if(_instance == null)
			{
				_instance = GetComponent<SocialManagementService>();
				DontDestroyOnLoad(gameObject);

				_socialManager.Prepared += OnPrepared;
			}
			else
			{
				Destroy(gameObject);
			}
		}

		void Start () 
		{
			
		}
		
		void Update () 
		{
			
		}
		
		void Destroy()
		{
			_socialManager.Prepared -= OnPrepared;
		}




		private void OnPrepared()
		{
			_isPrepared = true;
			if(Prepared != null) Prepared();
		}

		public void OpenAchievementUI()
		{

		}




		public void OpenLeaderboardUI()
		{
			Debug.Log("-------------> OpenLeaderboardUI");
			_socialManager.OpenLeaderboardUI();
		}

		public void ReportAchievement(string id)
		{
			_socialManager.ReportAchievement(id);
		}

		public void PostGoldToLeaderBoard(int goldAmount)
		{
			_socialManager.PostGoldToLeaderBoard(goldAmount);
		}
	}
}