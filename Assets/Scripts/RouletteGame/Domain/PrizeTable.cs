﻿using System.Collections.Generic;
using UnityEngine;

namespace Com.Mod.ThumbRun.RouletteGame.Domain
{
	public class PrizeTable : MonoBehaviour
	{
		[SerializeField]
		private List<PrizeRow> _prizeRows;

		public int GetPrizeById(int prizeId)
		{
			return _prizeRows[prizeId].gold;
		}
	}
}

[System.SerializableAttribute]
public class PrizeRow
{	
	public int id;
	public int gold;
}