﻿namespace Com.Mod.ThumbRun.RouletteGame.Domain
{
    using GameDataEditor;
    using TimeStamp;
    using UnityEngine;

    public class RouletteGame : MonoBehaviour 
	{
        public delegate void RouletteGameEvent();
        public event RouletteGameEvent Prepared;

        public enum gameType { Basic, Special, LuckyChance };

        private bool _isPrepared = false;
        private bool _isOpened = false;
        private GDERouletteGameData _rawData;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }


        private void Awake()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.RouletteGame_RouletteGame, out _rawData))
            {
                _isPrepared = true;
                if(Prepared != null) Prepared();
            }
        }

        void Start () 
		{
		}
		
		void Update () 
		{
		
		}

        public void OpenGame()
        {
            _isOpened = true;
        }

        public void CloseGame()
        {
            _isOpened = false;
        }

        public void PermitGame()
        {
            _rawData.IsPermission = true;
        }

        public void ForbidGame()
        {
            _rawData.IsPermission = false;
        }

        public bool IsPermission()
        {
            return _rawData.IsPermission;
        }
    }
}