﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.RouletteGame.Application;
using DG.Tweening;
using Com.Mod.Game.Roulette.View;
using Com.Mod.ThumbRun.UI.Popup;

namespace Com.Mod.ThumbRun.RouletteGame.View
{
    using Domain;

	public class RouletteWindow : BasicPopup 
	{
        [SerializeField]
        private UISprite _dimBg;
        [SerializeField]
        private GameObject _bg;
        [SerializeField]
        private RouletteMachine _rouletteMachine;
        [SerializeField]
        private UILabel _title;
        //[SerializeField]
        //private UISprite _character;
        [SerializeField]
        private UIDynamicButton _okButton;
        [SerializeField]
        private RouletteGameService _rouletteGameService;
        [SerializeField]
        private SpecialRouletteWindow _nextRouletteWindow;
        [SerializeField]
        private AudioSource _audioSource;
        [SerializeField]
        private AudioClip _spiningSound;

        void Awake ()
        {
			transform.localPosition = Vector3.zero;
            HideWindow();
            HideOKButton();

            _rouletteMachine.SpinStart += OnRouletteMachineSpinStart;
            _rouletteMachine.SpinEnded += OnRouletteMachineSpinEnded;
            _rouletteGameService.GameOpened += OnGameOpened;
        }

		void Start ()
		{
		
		}
		
		void Update () 
		{
        }

        void OnDestroy()
        {
            _rouletteMachine.SpinStart -= OnRouletteMachineSpinStart;
            _rouletteMachine.SpinEnded -= OnRouletteMachineSpinEnded;
            _rouletteGameService.GameOpened -= OnGameOpened;
        }

        private void OnRouletteMachineSpinStart(int prizeId)
        {
            _audioSource.clip = _spiningSound;
            _audioSource.Play();
        }

        private void OnRouletteMachineSpinEnded(int prizeId)
        {
            _rouletteGameService.ProvidePrize(prizeId, RouletteGame.gameType.Basic);
            ShowOKButton();
        }

        private void OnGameOpened()
        {
            Debug.Log("-----> RouletteWindow - OnGameOpened");
            ShowWindow();
        }

        public void ShowWindow()
        {
            _dimBg.gameObject.SetActive(true);
            _bg.gameObject.SetActive(true);
            _rouletteMachine.gameObject.SetActive(true);
            _title.gameObject.SetActive(true);
            //_character.gameObject.SetActive(true);

            base.SetCloseable(false);
            base.Open();
        }

        private void HideWindow()
        {
            _dimBg.gameObject.SetActive(false);
            _bg.gameObject.SetActive(false);
            _rouletteMachine.gameObject.SetActive(false);
            _title.gameObject.SetActive(false);
            //_character.gameObject.SetActive(false);
            base.SetCloseable(true);
            base.Close();
        }

        public void ShowOKButton()
        {
            _okButton.gameObject.SetActive(true);
        }

        public void HideOKButton()
        {
            _okButton.gameObject.SetActive(false);
        }

        public void PressOKButton()
        {
            HideWindow();
            HideOKButton();
            if(_nextRouletteWindow) _nextRouletteWindow.ShowWindow();
        }
    }
}