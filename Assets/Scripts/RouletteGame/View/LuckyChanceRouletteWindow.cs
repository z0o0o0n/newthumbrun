﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.RouletteGame.Application;
using DG.Tweening;
using Com.Mod.Game.Roulette.View;
using Com.Mod.ThumbRun.UI.Popup;

namespace Com.Mod.ThumbRun.RouletteGame.View
{
    using Domain;

	public class LuckyChanceRouletteWindow : BasicPopup 
	{
        public delegate void RouletteWindowEventHandler();
        public event RouletteWindowEventHandler RollCompleted;

        [SerializeField]
        private UISprite _dimBg;
        [SerializeField]
        private GameObject _bg;
        [SerializeField]
        private RouletteMachine _rouletteMachine;
        [SerializeField]
        private UILabel _title;
        [SerializeField]
        private UIDynamicButton _okButton;
        [SerializeField]
        private AudioSource _audioSource;
        [SerializeField]
        private AudioClip _spiningSound;
        [SerializeField]
        private float _diameter = 1f;
        [SerializeField]
        private UILabel _diameterLabel;
        [SerializeField]
        private RouletteGameService _rouletteGameService;

        void Awake ()
        {
			transform.localPosition = Vector3.zero;
            HideWindow();
            HideOKButton();

            _rouletteMachine.SpinStart += OnRouletteMachineSpinStart;
            _rouletteMachine.SpinEnded += OnRouletteMachineSpinEnded;
        }

		void Start ()
		{
		
		}
		
		void Update () 
		{
            
        }

        void OnDestroy()
        {
            _rouletteMachine.SpinStart -= OnRouletteMachineSpinStart;
            _rouletteMachine.SpinEnded -= OnRouletteMachineSpinEnded;
        }




        public void SetDiameter(float diameter)
        {
            _diameter = diameter;
            _diameterLabel.text = _diameter.ToString();
        }





        private void OnRouletteMachineSpinStart(int prizeId)
        {
            _audioSource.clip = _spiningSound;
            _audioSource.Play();
        }

        private void OnRouletteMachineSpinEnded(int prizeId)
        {
            _rouletteGameService.ProvidePrize(prizeId, RouletteGame.gameType.LuckyChance, _diameter);
            ShowOKButton();
            if(RollCompleted != null) RollCompleted();
        }





        public void ShowWindow()
        {
            _dimBg.gameObject.SetActive(true);
            _bg.gameObject.SetActive(true);
            _rouletteMachine.gameObject.SetActive(true);
            _title.gameObject.SetActive(true);
            //_character.gameObject.SetActive(true);

            base.SetCloseable(false);
            base.Open();
        }

        private void HideWindow()
        {
            _dimBg.gameObject.SetActive(false);
            _bg.gameObject.SetActive(false);
            _rouletteMachine.gameObject.SetActive(false);
            _title.gameObject.SetActive(false);
            //_character.gameObject.SetActive(false);

            base.SetCloseable(true);
            base.Close();
        }

        public void ShowOKButton()
        {
            _okButton.gameObject.SetActive(true);
        }

        public void HideOKButton()
        {
            _okButton.gameObject.SetActive(false);
        }

        public void PressOKButton()
        {
            HideWindow();
            HideOKButton();
            _rouletteMachine.ResetRoulette();
        }
    }
}