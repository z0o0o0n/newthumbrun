﻿namespace Com.Mod.ThumbRun.RouletteGame.View
{
    using UnityEngine;
    using System.Collections;
    using Game.Roulette.View;
    using Application;
    using Com.Mod.ThumbRun.UI.Popup;
    using Domain;

    public class SpecialRouletteWindow : BasicPopup
    {
        [SerializeField]
        private UISprite _dimBg;
        [SerializeField]
        private GameObject _bg;
        [SerializeField]
        private RouletteMachine _rouletteMachine;
        [SerializeField]
        private UILabel _title;
        [SerializeField]
        private UIDynamicButton _okButton;
        [SerializeField]
        private UIBasicButton _closeButton;
        [SerializeField]
        private RouletteGameService _rouletteGameService;
        [SerializeField]
        private AudioSource _audioSource;
        [SerializeField]
        private AudioClip _spiningSound;

        void Awake()
        {
            transform.localPosition = Vector3.zero;
            HideWindow();
            HideOKButton();

            _rouletteMachine.SpinStart += OnRouletteMachineSpinStart;
            _rouletteMachine.SpinEnded += OnRouletteMachineSpinEnded;
            _rouletteMachine.GameClose += OnRouletteMachineClosed;
        }

        void Start()
        {

        }

        void Update()
        {
        }

        void OnDestroy()
        {
            _rouletteMachine.SpinStart -= OnRouletteMachineSpinStart;
            _rouletteMachine.SpinEnded -= OnRouletteMachineSpinEnded;
            _rouletteMachine.GameClose -= OnRouletteMachineClosed;
        }





        private void OnRouletteMachineSpinStart(int prizeId)
        {
            _closeButton.gameObject.SetActive(false);

            _audioSource.clip = _spiningSound;
            _audioSource.Play();
        }

        private void OnRouletteMachineSpinEnded(int prizeId)
        {
            _rouletteGameService.ProvidePrize(prizeId, RouletteGame.gameType.Special);
            ShowOKButton();
        }

        private void OnRouletteMachineClosed()
        {
            HideWindow();
        }





        public void ShowWindow()
        {
            _dimBg.gameObject.SetActive(true);
            _bg.gameObject.SetActive(true);
            _rouletteMachine.gameObject.SetActive(true);
            _title.gameObject.SetActive(true);

            base.Open();
        }

        public void HideWindow()
        {
            _dimBg.gameObject.SetActive(false);
            _bg.gameObject.SetActive(false);
            _rouletteMachine.gameObject.SetActive(false);
            _title.gameObject.SetActive(false);

            base.Close();
        }

        private void ShowOKButton()
        {
            _okButton.gameObject.SetActive(true);
        }

        private void HideOKButton()
        {
            _okButton.gameObject.SetActive(false);
        }

        public void PressOKButton()
        {
            HideWindow();
            HideOKButton();
        }
    }
}