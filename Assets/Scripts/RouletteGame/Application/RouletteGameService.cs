﻿namespace Com.Mod.ThumbRun.RouletteGame.Application
{
    using Analytics.Application;
    using Com.Mod.ThumbRun.RouletteGame.Domain;
    using TimeStamp;
    using Tutorial.Application;
    using UnityEngine;
    using User.Application;

    public class RouletteGameService : MonoBehaviour 
	{
        public delegate void RouletteGameServiceEvent();
        public event RouletteGameServiceEvent Prepared;
        public event RouletteGameServiceEvent GameOpened;
        public event RouletteGameServiceEvent GameClosed;

        [SerializeField]
        private RouletteGame _rouletteGame;
        [SerializeField]
        private TutorialService _tutorialService;
        [SerializeField]
        private PrizeTable _basicPrizeTable;
        [SerializeField]
        private PrizeTable _specialPrizeTable;
        [SerializeField]
        private PrizeTable _luckyChancePrizeTable;
        [SerializeField]
        private UserService _userService;
        private AnalyticsService _analyricsService;
        private bool _isDebug = true;
        private bool _isPrepared = false;
        private bool _isSetupFinish = false;

        private void Awake()
        {
            _analyricsService = GameObject.FindGameObjectWithTag("Analytics").GetComponent<AnalyticsService>();

            _rouletteGame.Prepared += OnPrepared;
            _tutorialService.Prepared += OnPrepared;
        }

        void Start () 
		{
            if(TimeStampManager.instance.GetTimeStamp("EndOfDay").isEnd)
            {
                _rouletteGame.PermitGame();
            }
            else
            {
                _rouletteGame.ForbidGame();
            }
            _isSetupFinish = true;

            OnPrepared();
		}

		void Update () 
		{
		
		}

        void OnDestroy ()
        {
            _tutorialService.Prepared -= OnPrepared;
            _rouletteGame.Prepared -= OnPrepared;
        }





        private void OnPrepared()
        {
            if (!_rouletteGame.isPrepared) return;
            if (!_isSetupFinish) return;
            if (!_tutorialService.isPrepared) return;

            //OpenGame();
        }



        public void OpenGame()
        {
            if(_isDebug) Debug.Log("OpenRouletteGameService - OpenGame / isPermision: " + IsPermision() + " / isBasicTutorialEnd: " + _tutorialService.IsBasicTutorialEnd() + " / executeCount: " + _analyricsService.GetExecuteCount() + " - " + (_analyricsService.GetExecuteCount() <= 1));

            if (_analyricsService.GetExecuteCount() <= 1) return;
            if (!IsPermision()) return;
            if (!_tutorialService.IsBasicTutorialEnd()) return;
            if (!_tutorialService.IsConditionTutorialEnd()) return;

            _rouletteGame.OpenGame();
            TimeStampManager.instance.Reset("EndOfDay", false);
            if (GameOpened != null) GameOpened();
        }

        private void OnGameOpened()
        {
            
        }

        private bool IsPermision()
        {
            if (TimeStampManager.instance.GetTimeStamp("EndOfDay").isEnd)
            {
                _rouletteGame.PermitGame();
            }
            else
            {
                _rouletteGame.ForbidGame();
            }
            return _rouletteGame.IsPermission();
        }

        public void ProvidePrize(int prizeId, RouletteGame.gameType gameType, float diameter = 1f)
        {
            PrizeTable prizeTable;
            if (gameType == RouletteGame.gameType.Special) prizeTable = _specialPrizeTable;
            else if (gameType == RouletteGame.gameType.LuckyChance) prizeTable = _luckyChancePrizeTable;
            else prizeTable = _basicPrizeTable;

            int prize = Mathf.FloorToInt(diameter * prizeTable.GetPrizeById(prizeId));
            // Debug.Log("+++++ Provide Prize: " + prize + " prizeId: " + prizeId + " / diameter: " + diameter + "/ a: " + Mathf.FloorToInt(a));
            _userService.Save(prize);
        }
    }	
}