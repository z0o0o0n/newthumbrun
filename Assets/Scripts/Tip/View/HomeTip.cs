﻿namespace Com.Mod.ThumbRun.Tutorial.View
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class HomeTip : MonoBehaviour
    {
        [SerializeField]
        private GameObject _arrow;

        void Start()
        {

        }

        void Update()
        {

        }




        public void Show(GameObject homeButton)
        {
            gameObject.SetActive(true);

            homeButton.SetActive(false);
            homeButton.transform.parent = transform;
            homeButton.SetActive(true);

            Vector3 homeButtonContainerPos = homeButton.transform.localPosition;
            homeButtonContainerPos.x -= 50f;
            homeButtonContainerPos.y += 60f;
            _arrow.transform.localPosition = homeButtonContainerPos;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}