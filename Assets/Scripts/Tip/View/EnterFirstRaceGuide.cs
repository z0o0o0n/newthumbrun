﻿namespace Com.Mod.ThumbRun.Tutorial.View
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class EnterFirstRaceGuide : MonoBehaviour
    {
        private void Awake()
        {
            Hide();
        }

        void Start()
        {
            
        }

        void Update()
        {

        }




        public void Show(UIWidget vsComButton)
        {
            gameObject.SetActive(true);

            vsComButton.gameObject.SetActive(false);
            vsComButton.transform.parent = transform;
            vsComButton.depth = 2;
            vsComButton.gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}