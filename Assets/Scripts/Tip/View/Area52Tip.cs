﻿namespace Com.Mod.ThumbRun.Tutorial.View
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class Area52Tip : MonoBehaviour
    {
        public delegate void LuckyChanceTipEventHandler();
        public event LuckyChanceTipEventHandler Closed;

        [SerializeField]
        private TipBook _tipBook;
        [SerializeField]
        private UISprite _bg;

        void Start()
        {
            transform.localPosition = Vector3.zero;
            Hide();
        }

        void Update()
        {

        }




        public void OnOkButtonClick()
        {
            Hide();
            if (Closed != null) Closed();
        }




        public void Show()
        {
            _bg.gameObject.SetActive(true);
            _tipBook.Show();
        }

        public void Hide()
        {
            _bg.gameObject.SetActive(false);
            _tipBook.Hide();
        }
    }
}