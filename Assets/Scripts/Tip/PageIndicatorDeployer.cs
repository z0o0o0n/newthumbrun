﻿using UnityEngine;
using System.Collections;

public class PageIndicatorDeployer : MonoBehaviour 
{
    public float bottomSpace = 70;

    void Awake()
    {
        InitPosition();
    }

    private void InitPosition()
    {
        float ScreenHightEnd = -Screen.height / 2f;
        transform.localPosition = new Vector3(0f, ScreenHightEnd + bottomSpace, 0f);
    }

	void Start () 
	{
	
	}
	
	void Update () 
	{
	
	}
}
