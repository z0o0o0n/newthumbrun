﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using view.mapScene;
using DG.Tweening;

public class TipBook : MonoBehaviour 
{
    public delegate void TipBookEvent();
    public event TipBookEvent Closed;

    public List<TipPage> tipPageList;

    public PageMovementModel pageMovementModel;
    public TouchArea touchArea;
    public PageIndicator indicator;
    //public UIBasicButton closeButton;
    public UISprite bg;

    private bool _isEssentials = false;
    private int _prevPageIndex = 0;
    private int _currentPageIndex = 0;

    void Awake ()
    {
        transform.localPosition = Vector2.zero;

        touchArea.On_Drag += OnTouchAreaDrag;
        touchArea.On_Press += OnTouchAreaPress;
        touchArea.On_Release += OnTouchAreaRelease;

        pageMovementModel.On_Prev += OnPrev;
        pageMovementModel.On_Next += OnNext;
        pageMovementModel.On_Return += OnReturn;
        pageMovementModel.Init(0);

        //closeButton.On_Click += OnCloseButtonClick;

        indicator.Init(tipPageList.Count, 0);

        Init();
    }

    void Start()
    {

    }

    void Update()
    {

    }

    void OnDestory()
    {
        touchArea.On_Drag -= OnTouchAreaDrag;
        touchArea.On_Press -= OnTouchAreaPress;
        touchArea.On_Release -= OnTouchAreaRelease;

        pageMovementModel.On_Prev -= OnPrev;
        pageMovementModel.On_Next -= OnNext;
        pageMovementModel.On_Return -= OnReturn;

        //closeButton.On_Click -= OnCloseButtonClick;
    }





    private void Init()
    {
        for(int i = 0; i < tipPageList.Count; i++)
        {
            if (i == 0)
            {
                tipPageList[i].gameObject.SetActive(true);
                //ChangeCloseButtonColor(tipPageList[i].pointColor);
                ChageBgColor(tipPageList[i].bgColor);
                indicator.SetPointerColor(tipPageList[i].pointColor);
            }
            else
            {
                tipPageList[i].gameObject.SetActive(false);
            }
        }

        Hide();
    }





    //private void OnCloseButtonClick()
    //{
    //    Hide();
    //}

    private void OnTouchAreaDrag(Vector2 delta)
    {
        pageMovementModel.Drag(delta);

        if (!pageMovementModel.isMoving)
        {
            TipPage target = tipPageList[pageMovementModel.GetCurrentPageIndex()];
            Vector3 targetPos = target.transform.localPosition;
            targetPos.x += delta.x;
            target.transform.localPosition = targetPos;
        }
    }

    private void OnTouchAreaPress()
    {
        pageMovementModel.Press();
    }

    private void OnTouchAreaRelease()
    {
        pageMovementModel.Release();
    }

    private void OnPrev(bool isEnd)
    {
        if (!isEnd)
        {
            pageMovementModel.isMoving = true;

            _prevPageIndex = _currentPageIndex;
            _currentPageIndex = pageMovementModel.GetCurrentPageIndex();

            TipPage prevPage = tipPageList[_prevPageIndex];
            prevPage.transform.DOLocalMoveX(Screen.width, 0.5f).SetUpdate(true);

            TipPage currentPage = tipPageList[_currentPageIndex];
            currentPage.transform.localPosition = new Vector3(-Screen.width, 0, 0);
            currentPage.gameObject.SetActive(true);
            currentPage.transform.DOLocalMoveX(0, 0.5f).OnComplete(OnPrevComplete).SetUpdate(true);

            indicator.Move(_currentPageIndex);

            //ChangeCloseButtonColor(currentPage.pointColor);
            ChageBgColor(currentPage.bgColor);
            indicator.SetPointerColor(currentPage.pointColor);
        }
        else
        {
            OnReturn();
        }
    }

    private void OnPrevComplete()
    {
        pageMovementModel.isMoving = false;

        TipPage prevPage = tipPageList[_prevPageIndex];
        prevPage.gameObject.SetActive(false);
    }

    private void OnNext(bool isEnd)
    {
        if (!isEnd)
        {
            //Debug.LogWarning("Next: " + pageMovementModel.GetCurrentPageIndex());
            pageMovementModel.isMoving = true;

            _prevPageIndex = _currentPageIndex;
            _currentPageIndex = pageMovementModel.GetCurrentPageIndex();

            TipPage prevPage = tipPageList[_prevPageIndex];
            prevPage.transform.DOLocalMoveX(-Screen.width, 0.5f).SetUpdate(true);

            TipPage currentPage = tipPageList[_currentPageIndex];
            currentPage.transform.localPosition = new Vector3(Screen.width, 0, 0);
            currentPage.gameObject.SetActive(true);
            currentPage.transform.DOLocalMoveX(0, 0.5f).OnComplete(OnPrevComplete).SetUpdate(true);

            indicator.Move(_currentPageIndex);

            //ChangeCloseButtonColor(currentPage.pointColor);
            ChageBgColor(currentPage.bgColor);
            indicator.SetPointerColor(currentPage.pointColor);
        }
        else
        {
            OnReturn();
        }
    }

    private void OnReturn()
    {
        pageMovementModel.isMoving = true;
        TipPage currentPage = tipPageList[_currentPageIndex];
        currentPage.transform.DOLocalMoveX(0, 0.3f).OnComplete(OnReturned).SetUpdate(true);
    }

    private void OnReturned()
    {
        pageMovementModel.isMoving = false;
        pageMovementModel.OnPageReturned();
    }






    private void ChageBgColor(Color color)
    {
        DOTween.To(() => bg.color, x => bg.color = x, color, 0.3f).SetUpdate(true);
    }

    //private void ChangeCloseButtonColor(Color color)
    //{
    //    UIButton uiButton = closeButton.GetComponent<UIButton>();
    //    uiButton.pressed = color;
    //    uiButton.hover = color;
    //    uiButton.disabledColor = color;
    //    uiButton.defaultColor = color;
    //    UISprite uiSprite = closeButton.GetComponent<UISprite>();
    //    uiSprite.color = color;
    //}

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        if (Closed != null) Closed();
    }
}
