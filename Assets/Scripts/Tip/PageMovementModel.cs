﻿using UnityEngine;
using System.Collections;
using Com.Mod.Util;

public class PageMovementModel : MonoBehaviour
{
    public delegate void PageMovementEvent(bool isEnd);
    public delegate void PageMovementStateEvent();
    public event PageMovementEvent On_Prev;
    public event PageMovementEvent On_Next;
    public event PageMovementStateEvent On_Init;
    public event PageMovementStateEvent On_StartDrag;
    public event PageMovementStateEvent On_Return;
    public event PageMovementStateEvent Moved;
    public event PageMovementStateEvent Returned;

    public int pageCount = 0;
    public bool isPress = false;
    public bool isMoving = false;
    public float _distanceForMovement = 300;
    public float _speedForMovement = 10;

    private bool _isStartDrag = false;
    private int _currentPageIndex = 0;
    private float _distance = 0.0f;
    private SpeedChecker _speedChecker;

    void Awake()
    {
        _speedChecker = GetComponent<SpeedChecker>();
    }

    void Start()
    {

    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        _speedChecker.UpdatePos(new Vector2(_distance, 0));
    }

    public void Init(int pageIndex)
    {
        _currentPageIndex = pageIndex;
        if (On_Init != null) On_Init();
    }

    public void Press()
    {
        isPress = true;
        _isStartDrag = false;
        _distance = 0.0f;
        _speedChecker.Reset();
    }

    public void Drag(Vector2 delta)
    {
        if (!_isStartDrag)
        {
            _isStartDrag = true;
            if (On_StartDrag != null) On_StartDrag();
        }

        _distance += delta.x;
        if (isMoving) return;

        if (_distance < -_distanceForMovement)
        {
            _currentPageIndex++;
            MovePage(1);
        }
        else if (_distance > _distanceForMovement)
        {
            _currentPageIndex--;
            MovePage(-1);
        }
    }

    public void Release()
    {
        isPress = false;

        if (isMoving) return;

        if (_speedChecker.GetSpeedX() < -_speedForMovement)
        {
            _currentPageIndex++;
            MovePage(1);
            return;
        }
        else if (_speedChecker.GetSpeedX() > _speedForMovement)
        {
            _currentPageIndex--;
            MovePage(-1);
            return;
        }

        if (Mathf.Abs(_distance) < _distanceForMovement)
        {
            if(On_Return != null) On_Return();
        }
    }

    private void MovePage(int direction)
    {
        bool isEnd = false;
        if (_currentPageIndex < 0)
        {
            _currentPageIndex = 0;
            isEnd = true;
        }
        else if (_currentPageIndex >= pageCount)
        {
            _currentPageIndex = pageCount - 1;
            isEnd = true;
        }

        if (direction == 1)
        {
            if (On_Next != null) On_Next(isEnd);
        }
        else if (direction == -1)
        {
            if (On_Prev != null) On_Prev(isEnd);
        }
    }

    public int GetCurrentPageIndex()
    {
        return _currentPageIndex;
    }

    public void OnPageMoved()
    {
        if (Moved != null) Moved();
    }

    public void OnPageReturned()
    {
        if (Returned != null) Returned();
    }
}
