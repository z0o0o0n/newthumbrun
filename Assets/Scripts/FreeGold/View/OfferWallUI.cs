﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OfferWallUI : MonoBehaviour
{
    [SerializeField]
    private UIAlign _uiAlign;
    [SerializeField]
    private NGUILocalizeText _label;

    private void Awake()
    {
        _label.Changed += OnLabelChanged;
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}




    private void OnLabelChanged()
    {
        _uiAlign.Replace();
    }
}
