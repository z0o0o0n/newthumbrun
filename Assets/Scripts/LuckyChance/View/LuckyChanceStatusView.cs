﻿namespace Com.Mod.ThumbRun.LuckyChance.View
{
    using DG.Tweening;
    using Com.Mod.ThumbRun.LuckyChance.Application;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.LuckyChance.Domain;
    using UnityEngine;
    using System.Collections;
    using User.Application;
    using RouletteGame.View;

    public class LuckyChanceStatusView : MonoBehaviour 
	{
        // [SerializeField]
        // private UserService _userService;
        [SerializeField]
		private LuckyChanceService _luckyChanceService;
        [SerializeField]
        private UIDynamicButton _luckyChanceButton;
        [SerializeField]
        private LuckyChanceRouletteWindow _rouletteWindow;
        //private LuckyChancePopup _luckyChancePopup;
        [SerializeField]
        private UILabel _diameterLabel;
        //private DiameterDisplay _diameterDisplay;
        [SerializeField]
        private GameObject _badgeDisplayPrefab;
        [SerializeField]
        private float _popupCloseTime = 1.5f;
        [SerializeField]
        private UIAlign _infoUiAlign;
        [SerializeField]
        private UILabel _infoTitle;
        [SerializeField]
        private UILabel _infoRemaining;
        [SerializeField]
        private UILabel _infoGameLeft;
        private List<BadgeDisplay> _badgeDisplays;
        private float _diameter = 1f;

        void Awake ()
		{
            CreateBadgeDisplays();
            Hide();

            _luckyChanceService.Prepared += OnPrepared;
            _luckyChanceService.StatusUpdated += OnStatusUpdated;
			_luckyChanceService.BadgeAdded += OnBadgeAdded;

            _luckyChanceButton.Click += OnLuckyChanceButtonPressed;

            _rouletteWindow.RollCompleted += OnRollCompleted;

            _infoTitle.GetComponent<NGUILocalizeText>().Changed += OnInfoChanged;
            _infoGameLeft.GetComponent<NGUILocalizeText>().Changed += OnInfoChanged;

            _rouletteWindow.ShowWindow();
        }

		void Start () 
		{
		}

		void Update () 
		{
            if (Input.GetKeyUp(KeyCode.A))
            {
                _rouletteWindow.SetDiameter(3);
                _rouletteWindow.ShowWindow();
            }
        }

		void OnDestroy()
		{
            _luckyChanceService.Prepared -= OnPrepared;
            _luckyChanceService.StatusUpdated -= OnStatusUpdated;
			_luckyChanceService.BadgeAdded -= OnBadgeAdded;

            _luckyChanceButton.Click -= OnLuckyChanceButtonPressed;

            _rouletteWindow.RollCompleted += OnRollCompleted;

            _infoTitle.GetComponent<NGUILocalizeText>().Changed -= OnInfoChanged;
            _infoGameLeft.GetComponent<NGUILocalizeText>().Changed -= OnInfoChanged;

            DOTween.Kill("Hide." + GetInstanceID());
        }



        private void CreateBadgeDisplays()
        {
            _badgeDisplays = new List<BadgeDisplay>();
            float startPos = -90f;

            for(int i = 0; i < 5; i++)
            {
                GameObject instance = GameObject.Instantiate(_badgeDisplayPrefab);
                instance.transform.parent = transform;
                instance.transform.localScale = Vector3.one;
                instance.transform.localPosition = new Vector3(startPos + (45f * i), 0f, 0f);

                BadgeDisplay badgeDisplay = instance.GetComponent<BadgeDisplay>();
                _badgeDisplays.Add(badgeDisplay);
            }
        }

        private void OnPrepared()
        {
            //Show();
        }

        private void OnStatusUpdated(List<LuckyChanceBadge> badges, bool isFull, float diameter)
        {
            Debug.Log("LuckyChanceStatusView - OnStatusUpdated / diameter: " + _diameter);
            _diameter = diameter;

            LogBadges(badges);
            ChangeBadges(badges);

            //if (badges.Count == 0) _diameterDisplay.gameObject.SetActive(false);
            //else _diameterDisplay.gameObject.SetActive(true);
            //_diameterDisplay.SetDiameter(diameter);
            _diameterLabel.text = "x" + (diameter).ToString();
            _infoUiAlign.Replace();

            UpdateInfo(_badgeDisplays.Count - badges.Count);
            

            //Debug.Log("11111 updated: badges Count: " + badges.Count);
            if (isFull)
            {
                //Hide();
                _luckyChanceButton.gameObject.SetActive(true);
                _infoUiAlign.gameObject.SetActive(false);
            }
        }

        private void LogBadges(List<LuckyChanceBadge> badges)
        {
            string log = "LuckyChanceStatusView - Badges \n";
            for (int i = 0; i < badges.Count; i++)
            {
                log += badges[i].value + "\n";
            }
            //Debug.Log(log);
        }

        private void ChangeBadges(List<LuckyChanceBadge> badges)
        {
            for (int j = 0; j < _badgeDisplays.Count; j++)
            {
                if (j >= badges.Count)
                {
                    //Debug.Log("11111 Reset: " + j.ToString());
                    _badgeDisplays[j].Reset();
                }
                else
                {
                    //Debug.Log ("+++++ value: " + badges [j].value);
                    _badgeDisplays[j].Change(badges[j].value);
                }
            }
        }

        private void UpdateInfo(int remainingGameCount)
        {
            _infoRemaining.text = remainingGameCount.ToString();
            _infoUiAlign.Replace();
            //_infoRemaining.transform.DOLocalMoveX(_infoTitle.width + 10f, 0);

            //_infoGameLeft.transform.DOLocalMoveX(_infoTitle.width + 10f + _infoRemaining.width + 10f, 0);
        }

        private void OnBadgeAdded(List<LuckyChanceBadge> badges, bool isFull, float diameter)
        {
            _diameter = diameter;

            int badgeIndex = badges.Count - 1;
            _badgeDisplays[badgeIndex].Change(badges[badgeIndex].value);
			if(badges[badgeIndex].value != LuckyChanceBadge.badge.NONE) _badgeDisplays[badgeIndex].ShowBackLight();

            //if (badges.Count == 0) _diameterDisplay.gameObject.SetActive(false);
            //else _diameterDisplay.gameObject.SetActive(true);

            UpdateInfo(_badgeDisplays.Count - badges.Count);

            if (isFull)
            {
                //DOTween.Kill("Hide." + GetInstanceID());
                //DOVirtual.DelayedCall(1f, Hide).SetId("Hide." + GetInstanceID());
                _luckyChanceButton.gameObject.SetActive(true);
                _infoUiAlign.gameObject.SetActive(false);
            }

            _diameterLabel.text = "x" + diameter.ToString();
            _infoUiAlign.Replace();
        }

        private void OnLuckyChanceButtonPressed(GameObject sender)
        {
            Debug.Log("????????????????????? _diameter: " + _diameter);
            //_luckyChancePopup.Show(_diameter);
            _rouletteWindow.SetDiameter(_diameter);
            _rouletteWindow.ShowWindow();
            _luckyChanceService.ClearBadges();
            _luckyChanceService.UpdateStatus();
            _luckyChanceButton.gameObject.SetActive(false);
        }

        private void OnRollCompleted()
        {
            Show();
            // _userService.Save(prizeGoldAmount);
            //DOTween.Kill("SlotComplete." + GetInstanceID());
            //DOVirtual.DelayedCall(_popupCloseTime, OnSlotCompleted).SetId("SlotComplete." + GetInstanceID());
        }

        private void OnSlotCompleted()
        {
            //_rouletteWindow.Close
            //_luckyChancePopup.Hide();
            
        }

        private void OnInfoChanged()
        {
            _infoUiAlign.Replace();
            //_infoRemaining.transform.DOLocalMoveX(_infoTitle.width + 10f, 0);
            //_infoGameLeft.transform.DOLocalMoveX(_infoTitle.width + 10f + _infoRemaining.width + 10f, 0);

            //float width = _infoTitle.width + 10f + _infoRemaining.width + 10f + _infoGameLeft.width;
            //_info.transform.DOLocalMoveX(-width / 2f, 0f);
        }



        public void Show()
		{
			gameObject.SetActive (true);
            _infoUiAlign.gameObject.SetActive(true);
            _luckyChanceButton.gameObject.SetActive(false);
			_luckyChanceService.UpdateStatus ();
		}

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void ResetBackLight()
        {
            for (int j = 0; j < _badgeDisplays.Count; j++)
            {
                _badgeDisplays[j].HideBackLight();
            }
        }
	}
}