﻿namespace Com.Mod.ThumbRun.LuckyChance.View
{
    using DG.Tweening;
    using Com.Mod.ThumbRun.LuckyChance.Domain;
	using UnityEngine;
	using System.Collections;

	public class BadgeDisplay : MonoBehaviour 
	{
		[SerializeField]
		private UISprite _badge;
		[SerializeField]
		private UISprite _emptyBadge;
        [SerializeField]
        private UISprite _backLight;

        void Awake ()
        {
            _backLight.gameObject.SetActive(false);
        }

        void Start () 
		{
			
		}
	
		void Update () 
		{
//			if (Input.GetKeyUp (KeyCode.Q)) {
//				Change (LuckyChanceBadge.badge.GOLD);
//			}
//			if (Input.GetKeyUp (KeyCode.W)) {
//				Change (LuckyChanceBadge.badge.SILVER);
//			}
//			if (Input.GetKeyUp (KeyCode.E)) {
//				Change (LuckyChanceBadge.badge.BRONZE);
//			}
//			if (Input.GetKeyUp (KeyCode.R)) {
//				Change (LuckyChanceBadge.badge.NONE);
//			}
		}

		public void Change(LuckyChanceBadge.badge badge)
		{
			if (badge == LuckyChanceBadge.badge.GOLD)
				_badge.spriteName = "GoldBadge";
			else if (badge == LuckyChanceBadge.badge.SILVER)
				_badge.spriteName = "SilverBadge";
			else if (badge == LuckyChanceBadge.badge.BRONZE)
				_badge.spriteName = "BronzeBadge";
			else if (badge == LuckyChanceBadge.badge.NONE)
				_badge.spriteName = "NoneBadge";

            _badge.gameObject.SetActive (true);
		}

        public void ShowBackLight()
        {
            _backLight.gameObject.SetActive(true);
        }

        public void HideBackLight()
        {
            _backLight.gameObject.SetActive(false);
        }

		public void Reset()
		{
			_badge.gameObject.SetActive (false);
            _backLight.gameObject.SetActive(false);
        }
	}
}