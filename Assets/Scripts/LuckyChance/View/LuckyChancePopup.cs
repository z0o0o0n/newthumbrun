﻿namespace Com.Mod.ThumbRun.LuckyChance.View
{
    using UnityEngine;
    using System.Collections;
    using Application;

    public class LuckyChancePopup : MonoBehaviour
    {
        public delegate void LuckyChancePopupEventHandler(int prizeGold);
        public event LuckyChancePopupEventHandler RollCompleted;

        [SerializeField]
        private LuckyChanceService _luckyChanceService;
        [SerializeField]
        private Slot _slot;
        [SerializeField]
        private DiameterDisplay _diameterDisplay;
        private float _diameter;

        void Awake ()
        {
            _luckyChanceService.Prepared += OnPrepared;
            _slot.Completed += OnSlotCompleted;

            Hide();
        }

        void Start()
        {

        }

        void Update()
        {

        }

        void OnDestroy()
        {
            _luckyChanceService.Prepared -= OnPrepared;
            _slot.Completed -= OnSlotCompleted;
        }



        private void OnPrepared()
        {
            
        }



        public void Show(float diameter)
        {
            _diameter = diameter;
            _diameterDisplay.SetDiameter(diameter);

            _slot.Init(_luckyChanceService.GetSlotPrizeList());
            _slot.Roll();

            gameObject.SetActive(true);
        }

        private void OnSlotCompleted(int prizeGold)
        {
            int prizeGoldAmount = (int)(prizeGold * _diameter);
            if (RollCompleted != null) RollCompleted(prizeGoldAmount);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}