﻿namespace Com.Mod.ThumbRun.LuckyChance.View
{
    using UnityEngine;
    using System.Collections;
    using Application;
    using Domain;
    using System.Collections.Generic;
    using DG.Tweening;

    public class LuckyChanceButton : MonoBehaviour
    {
        public delegate void LuckyChanceButtonEventHandler();
        public event LuckyChanceButtonEventHandler Pressed;

        [SerializeField]
        private LuckyChanceService _luckyChanceService;
        [SerializeField]
        private DiameterDisplay _diameterDisplay;
        private float _diameter;

        void Awake ()
        {
            _luckyChanceService.StatusUpdated += OnStatusUpdated;
            _luckyChanceService.BadgeAdded += OnBadgeAdded;
            Hide();
        }

        void Start()
        {

        }

        void Update()
        {

        }

        void OnDestroy()
        {
            DOTween.Kill("Show." + GetInstanceID());
            _luckyChanceService.StatusUpdated -= OnStatusUpdated;
            _luckyChanceService.BadgeAdded -= OnBadgeAdded;
        }



        private void Show()
        {
            gameObject.SetActive(true);
        }

        private void Hide()
        {
            gameObject.SetActive(false);
        }



        private void OnStatusUpdated(List<LuckyChanceBadge> badges, bool isFull, float diameter)
        {
            _diameter = diameter;
            _diameterDisplay.SetDiameter(diameter);

            if (isFull)
            {
                Show();
            }
        }

        private void OnBadgeAdded(List<LuckyChanceBadge> badges, bool isFull, float diameter)
        {
            _diameter = diameter;
            _diameterDisplay.SetDiameter(diameter);

            if (isFull)
            {
                DOTween.Kill("Show." + GetInstanceID());
                DOVirtual.DelayedCall(1f, Show).SetId("Show." + GetInstanceID());
            }
        }



        public void PressButton()
        {
            if (Pressed != null) Pressed();
            //_luckyChancePopup.Show(_diameter);
            //_luckyChanceService.ClearBadges();
            //_luckyChanceStatusView.gameObject.SetActive(true);
            //gameObject.SetActive(false);
        }
    }
}