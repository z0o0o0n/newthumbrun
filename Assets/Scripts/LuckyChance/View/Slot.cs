﻿namespace Com.Mod.ThumbRun.LuckyChance.View
{
    using UnityEngine;
    using System.Collections;
    using DG.Tweening;
    using System.Collections.Generic;

    public class Slot : MonoBehaviour
    {
        public delegate void SlotEventHandler(int prizeGold);
        public event SlotEventHandler Completed;

        [SerializeField]
        private UILabel _label;
        [SerializeField]
        private AudioSource _audioSource;
        [SerializeField]
        private AnimationCurve _rollCurve;
        [SerializeField]
        private AudioClip _rollSound;
        [SerializeField]
        private AudioClip _rollCompleteSound;
        [SerializeField]
        private UISprite _backLight;
        private List<int> _prizes;
        private float _rollValue = 0f;
        private int _rollCount = 0;
        private float _lastRollValue = 0f;

        void Awake()
        {
            _backLight.gameObject.SetActive(false);
        }

        void Start()
        {
            
        }

        void Update()
        {

        }



        public void Init(List<int> prizes)
        {
            _prizes = prizes;
        }

        public void Roll()
        {
            _backLight.gameObject.SetActive(false);
            Reset();
            DOTween.Kill(GetInstanceID());
            DOTween.To(() => _rollValue, x => _rollValue = x, 30f, 2.5f).OnUpdate(RollUpdate).SetEase(_rollCurve).OnComplete(OnRollCompleted).SetId(GetInstanceID());
        }

        private void RollUpdate()
        {
            if (_rollValue > _lastRollValue + 2f)
            {
                _label.text = _prizes[_rollCount].ToString();

                _lastRollValue = _rollValue;
                _rollCount += 1;
                if (_rollCount >= _prizes.Count) _rollCount = 0;
                _audioSource.clip = _rollSound;
                _audioSource.Play();
            }
        }

        private void OnRollCompleted()
        {
            int randomIndex = (int)Random.Range(0, _prizes.Count);
            _label.text = _prizes[randomIndex].ToString();

            _backLight.gameObject.SetActive(true);
            Reset();
            _audioSource.clip = _rollCompleteSound;
            _audioSource.Play();

            if (Completed != null) Completed(_prizes[randomIndex]);
        }

        private void Reset()
        {
            _rollValue = 0;
            _lastRollValue = 0;
            _rollCount = 0;
        }
    }
}