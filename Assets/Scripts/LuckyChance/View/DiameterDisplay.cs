﻿namespace Com.Mod.ThumbRun.LuckyChance.View
{
    using UnityEngine;
    using System.Collections;

    public class DiameterDisplay : MonoBehaviour
    {
        [SerializeField]
        private UISprite _bg;
        [SerializeField]
        private UILabel _label;
        [SerializeField]
        private UISprite _sign;

        void Start()
        {

        }

        void Update()
        {

        }

        

        public void SetDiameter(float diameter)
        {
            _label.text = diameter.ToString();
        }
    }
}