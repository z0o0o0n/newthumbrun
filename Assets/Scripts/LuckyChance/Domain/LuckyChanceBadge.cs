﻿namespace Com.Mod.ThumbRun.LuckyChance.Domain
{
	using UnityEngine;
	using System.Collections;

	public class LuckyChanceBadge 
	{
		public enum badge {GOLD, SILVER, BRONZE, NONE};
		public LuckyChanceBadge.badge value;

		public LuckyChanceBadge(LuckyChanceBadge.badge badge)
		{
			value = badge;
		}
	}
}