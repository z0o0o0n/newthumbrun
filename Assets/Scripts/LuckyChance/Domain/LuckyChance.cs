﻿namespace Com.Mod.ThumbRun.LuckyChance.Domain
{
    using System.Collections.Generic;
    using UnityEngine;
	using System.Collections;
	using GameDataEditor;

	public class LuckyChance : MonoBehaviour 
	{
		public delegate void LuckyChanceHandler ();
		public event LuckyChanceHandler Prepared;
		public event LuckyChanceHandler Added;
		public event LuckyChanceHandler Fulled;

		private GDELuckyChanceData _rawData;
		private bool _isPrepared = false;
		private float _diameter = 1;

		void Start () 
		{
			if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.LuckyChance_LuckyChance, out _rawData))
			{
                //Debug.Log("+++++ LuckyChance Prepared");

                //_rawData.ResetAll();

                CheckBadgeListCount();

				_isPrepared = true;
				if (Prepared != null) Prepared ();
			}
			else
			{
				Debug.LogError("Read Error LuckyChance");
			}
		}
	
		void Update () 
		{
		
		}

		// raceOutcomeList의 수량이 goalRaceCount보다 큰 경우 끝에서 goalRaceCount만큼을 제외한 나머지를 삭제함
		private void CheckBadgeListCount()
		{
			int count = _rawData.badgeList.Count;
			int goalCount = _rawData.goalRaceCount;
			if (count > goalCount)
			{
				_rawData.badgeList.RemoveRange(0, count - goalCount);
				_rawData.Set_badgeList();
			}
		}

        private void ChaculateDiameter()
        {
            float diameter = 0;
            for (int i = 0; i < _rawData.badgeList.Count; i++)
            {
                if (_rawData.badgeList[i] == LuckyChanceBadge.badge.GOLD.ToString())
                    diameter += _rawData.badgeDiameter[0];
                else if (_rawData.badgeList[i] == LuckyChanceBadge.badge.SILVER.ToString())
                    diameter += _rawData.badgeDiameter[1];
                else if (_rawData.badgeList[i] == LuckyChanceBadge.badge.BRONZE.ToString())
                    diameter += _rawData.badgeDiameter[2];
                else if (_rawData.badgeList[i] == LuckyChanceBadge.badge.NONE.ToString())
                    diameter += _rawData.badgeDiameter[3];
            }

            _rawData.diameter = 1 + diameter;
        }



        public List<LuckyChanceBadge> GetBadgeList()
		{
			List<LuckyChanceBadge> badgeList = new List<LuckyChanceBadge>();
			for (int i = 0; i < _rawData.badgeList.Count; i++) {
				if (_rawData.badgeList [i] == LuckyChanceBadge.badge.GOLD.ToString ())
					badgeList.Add(new LuckyChanceBadge (LuckyChanceBadge.badge.GOLD));
				else if (_rawData.badgeList [i] == LuckyChanceBadge.badge.SILVER.ToString ())
					badgeList.Add(new LuckyChanceBadge (LuckyChanceBadge.badge.SILVER));
				else if (_rawData.badgeList [i] == LuckyChanceBadge.badge.BRONZE.ToString())
					badgeList.Add(new LuckyChanceBadge(LuckyChanceBadge.badge.BRONZE));
				else if (_rawData.badgeList [i] == LuckyChanceBadge.badge.NONE.ToString())
					badgeList.Add(new LuckyChanceBadge(LuckyChanceBadge.badge.NONE));
			}
			return badgeList;
		}

		public void AddBadge(LuckyChanceBadge.badge badge)
		{
            if(!IsBadgeFull())
            {
                _rawData.badgeList.Add(badge.ToString());
                _rawData.Set_badgeList();
                CheckBadgeListCount();
                ChaculateDiameter();
                if (Added != null) Added();
                if (IsBadgeFull())
                {
                    //Debug.Log("+++++ lucky chance is full");
                    if (Fulled != null) Fulled();
                }
            }
		}

        public bool IsBadgeFull()
        {
            int count = _rawData.badgeList.Count;
            int goalCount = _rawData.goalRaceCount;
            //Debug.Log("+++++ isBadgeFull: " + count + " / " + goalCount);
            if (count >= goalCount)
                return true;
            else
                return false;
        }

        public float GetDiameter()
        {
            return _rawData.diameter;
        }

        public List<int> GetSlotPrizeList()
        {
            return _rawData.slotPrizeList;
        }

        public void ClearBadges()
        {
            _rawData.Reset_badgeList();
            _rawData.diameter = 1;
        }
    }
}