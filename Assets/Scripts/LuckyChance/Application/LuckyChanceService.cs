﻿namespace Com.Mod.ThumbRun.LuckyChance.Application
{
	using UnityEngine;
	using System.Collections;
	using Com.Mod.ThumbRun.LuckyChance.Domain;
	using System.Collections.Generic;

	public class LuckyChanceService : MonoBehaviour 
	{
		public delegate void LuckyChanceServiceHandler();
		public delegate void LuckyChanceUpdateStatusHandler(List<LuckyChanceBadge> badges, bool isFull, float diameter);
        public event LuckyChanceServiceHandler Prepared;
        public event LuckyChanceServiceHandler BadgeFulled;
        public event LuckyChanceUpdateStatusHandler BadgeAdded;
        public event LuckyChanceUpdateStatusHandler StatusUpdated;
		
		[SerializeField]
		private LuckyChance _luckyChance;
        private bool _isPrepared = false;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }

		void Awake ()
		{
            _luckyChance.Prepared += OnPrepared;
            _luckyChance.Added += OnBadgeAdded;
            _luckyChance.Fulled += OnBadgeFulled;

        }

		void Start ()
		{
		
		}

		void Update () 
		{
		
		}

		void OnDestroy()
		{
            _luckyChance.Prepared -= OnPrepared;
            _luckyChance.Added -= OnBadgeAdded;
            _luckyChance.Fulled -= OnBadgeFulled;
        }

        private void OnPrepared()
        {
            _isPrepared = true;
            if (Prepared != null) Prepared();
        }

        private void OnBadgeAdded()
        {
            List<LuckyChanceBadge> luckyChanceBadgeList = _luckyChance.GetBadgeList();
            bool isBadgeFull = _luckyChance.IsBadgeFull();
            float diameter = _luckyChance.GetDiameter();
            if (BadgeAdded != null) BadgeAdded(luckyChanceBadgeList, isBadgeFull, diameter);
        }

        private void OnBadgeFulled()
        {
            if (BadgeFulled != null) BadgeFulled();
        }





        public void UpdateStatus()
		{
			List<LuckyChanceBadge> luckyChanceBadgeList = _luckyChance.GetBadgeList();
            bool isBadgeFull = _luckyChance.IsBadgeFull();
            float diameter = _luckyChance.GetDiameter();
            if (StatusUpdated != null) StatusUpdated (luckyChanceBadgeList, isBadgeFull, diameter);
//			Debug.Log (" +++++ lucky Chance Badge List Count: " + luckyChanceBadgeList.Count);
        }

        public void AddBadge(LuckyChanceBadge.badge badge)
		{
			_luckyChance.AddBadge (badge);
		}

        public void ClearBadges()
        {
            _luckyChance.ClearBadges();
        }

        public List<int> GetSlotPrizeList()
        {
            return _luckyChance.GetSlotPrizeList();
        }

        public bool IsBadgeFull()
        {
            return _luckyChance.IsBadgeFull();
        }
    }
}