﻿namespace Com.Mod.ThumbRun.LuckyChance.Application
{
	using Com.Mod.ThumbRun.LuckyChance.Domain;
	using UnityEngine;
	using System.Collections;

	public class LuckyChanceTester : MonoBehaviour 
	{
		[SerializeField]
		private LuckyChanceService _luckyChanceService;
		
		void Start () 
		{
		
		}

		void Update () 
		{
			if (Input.GetKeyUp (KeyCode.Q)) {
				_luckyChanceService.AddBadge (LuckyChanceBadge.badge.GOLD);
			} else if (Input.GetKeyUp (KeyCode.W)) {
				_luckyChanceService.AddBadge (LuckyChanceBadge.badge.SILVER);
			} else if (Input.GetKeyUp (KeyCode.E)) {
				_luckyChanceService.AddBadge (LuckyChanceBadge.badge.BRONZE);
			} else if (Input.GetKeyUp(KeyCode.R)) {
				_luckyChanceService.ClearBadges ();
                _luckyChanceService.UpdateStatus();
            }
		}
	}
}