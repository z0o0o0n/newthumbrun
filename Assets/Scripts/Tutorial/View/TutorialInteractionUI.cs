﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialInteractionUI : MonoBehaviour
{
    public delegate void TutorialInteractionEvent();
    public event TutorialInteractionEvent LeftButtonPress;
    public event TutorialInteractionEvent RightButtonPress;
    public event TutorialInteractionEvent BoostButtonPress;

    [SerializeField]
    private PlayerControlButton _playerControlButton;
    [SerializeField]
    private UIBasicButton _boostButtonArea;
    [SerializeField]
    private GameObject _handPointer;

    private void Awake()
    {
        _playerControlButton.LeftPressed += OnLeftPressed;
        _playerControlButton.RightPressed += OnRightPressed;
        _boostButtonArea.On_Click += OnBoostPressed;
        HideAll();
    }

    void Start ()
    {
        _playerControlButton.Activate();
    }
	
	void Update ()
    {
		
	}

    private void OnDestroy()
    {
        _playerControlButton.LeftPressed -= OnLeftPressed;
        _playerControlButton.RightPressed -= OnRightPressed;
        _boostButtonArea.On_Click -= OnBoostPressed;
    }





    private void OnLeftPressed()
    {
        if (LeftButtonPress != null) LeftButtonPress();
    }

    private void OnRightPressed()
    {
        if (RightButtonPress != null) RightButtonPress();
    }

    private void OnBoostPressed()
    {
        if (BoostButtonPress != null) BoostButtonPress();
    }





    public void ShowRightButton()
    {
        HideAll();
        _playerControlButton._rightButton.SetActive(true);
        _handPointer.SetActive(true);
        _handPointer.transform.localPosition = new Vector3((UIScreen.instance.GetWidth() / 4), 0f, 0f);
    }

    public void ShowLeftButton()
    {
        HideAll();
        _playerControlButton._leftButton.SetActive(true);
        _handPointer.SetActive(true);
        _handPointer.transform.localPosition = new Vector3(-(UIScreen.instance.GetWidth() / 4), 0f, 0f);
    }

    public void ShowBoostButton()
    {
        HideAll();
        _boostButtonArea.gameObject.SetActive(true);
        _handPointer.SetActive(true);
        _handPointer.transform.localPosition = new Vector3(0f, -(UIScreen.instance.GetHeight() / 2) + 80f, 0f);
    }

    public void HideRightButton()
    {
        _playerControlButton._rightButton.SetActive(false);
        _handPointer.SetActive(false);
    }

    public void HideLeftButtion()
    {
        _playerControlButton._leftButton.SetActive(false);
        _handPointer.SetActive(false);
    }

    public void HideBoostButton()
    {
        _boostButtonArea.gameObject.SetActive(false);
        _handPointer.SetActive(false);
    }

    private void HideAll()
    {
        HideRightButton();
        HideLeftButtion();
        HideBoostButton();
    }
}
