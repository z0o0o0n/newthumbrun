﻿namespace Com.Mod.ThumbRun.Tutorial.View
{
    using Application;
    using DG.Tweening;
    using Domain;
    using NGUI.LogConsole;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class TutorialGuidePopup : MonoBehaviour
    {
        public delegate void InteractiveButtonEvent(string buttonType);
        public event InteractiveButtonEvent InteractiveButtonClick;

        [SerializeField]
        private GameObject _popup;
        [SerializeField]
        private UILabel _textLabel;
        [SerializeField]
        private UISprite _popupBg;
        [SerializeField]
        private UISprite _guideProfilePhoto;
        [SerializeField]
        private UISprite _nextIndicator;
        [SerializeField]
        private UIDynamicButton _okButton;
        [SerializeField]
        private UIDynamicButton _closeButton;
        [SerializeField]
        private TutorialService _tutorialService;
        [SerializeField]
        private Task _task;
        [SerializeField]
        private GameObject _bg;
        [SerializeField]
        private AudioSource _audioSource;
        private int _currentTextCueIndex = 0;
        private GuideCue _guideCue;
        private bool _hasInteraction = false;

        private void Awake()
        {
            _tutorialService.Prepared += OnPrepared;
            _task.Cue += OnCue;

            _okButton.Click += OnOkButtonClick;
            _closeButton.Click += OnCloseButtonClick;

            transform.localPosition = Vector2.zero;
            HidePopup();
            HideBg();
        }

        void Start()
        {

        }

        void Update()
        {
             
        }

        private void OnDestroy()
        {
            _tutorialService.Prepared -= OnPrepared;
            _task.Cue -= OnCue;

            _okButton.Click -= OnOkButtonClick;
            _closeButton.Click -= OnCloseButtonClick;
        }





        private void OnPrepared()
        {
            if (!_tutorialService.isPrepared) return;
        }

        private void OnCue(int taskIndex, int guideCueIndex, GuideCue guideCue)
        {
            _guideCue = guideCue;
            _currentTextCueIndex = 0;
            Show();
            ShowBg();
        }

        private void OnOkButtonClick(GameObject sender)
        {
            if (InteractiveButtonClick != null) InteractiveButtonClick("OK");
        }

        private void OnCloseButtonClick(GameObject sender)
        {
            if (InteractiveButtonClick != null) InteractiveButtonClick("CLOSE");
        }





        public void PressPopup()
        {
            if (_hasInteraction) return;
            ChangeTextCue();
        }





        private void Show()
        {
            //LogConsole.instance.Log("tutorial show. pos:" + _guideCue.popupPos.x + "/" + _guideCue.popupPos.y);
            _popup.gameObject.SetActive(true);
            _popup.transform.localPosition = _guideCue.popupPos;
            ChangeTextCue();
            _audioSource.Play();

        }

        public void HidePopup()
        {
            _popup.gameObject.SetActive(false);
        }

        private void Reset()
        {

        }

        private void ChangeTextCue()
        {
            TextCue textCue = _guideCue.textCues[_currentTextCueIndex];
            _textLabel.text = textCue.text;

            // Cue의 마지막 Text인 경우
            if(_currentTextCueIndex == _guideCue.textCues.Count - 1)
            {
                _okButton.gameObject.SetActive(_guideCue.useOkButton);
                _closeButton.gameObject.SetActive(_guideCue.useCloseButton);
                if (_guideCue.useCloseButton || _guideCue.useOkButton || _guideCue.useInteraction) _hasInteraction = true;
                else _hasInteraction = false;
            }
            else
            {
                _okButton.gameObject.SetActive(false);
                _closeButton.gameObject.SetActive(false);
                _hasInteraction = false;
            }

            ResizeAndReplace();

            _currentTextCueIndex++;
        }

        private void ResizeAndReplace()
        {
            int buttonHeight = 70;
            int contentHeight = _textLabel.height;
            if (IsLastText())
            {
                if(_guideCue.useOkButton || _guideCue.useCloseButton)
                {
                    contentHeight = _textLabel.height + buttonHeight;
                }
            }

            _popupBg.height = contentHeight + 60;

            if (_popupBg.height < 160) _popupBg.height = 160;

            if (IsLastText())
            {
                if (!_guideCue.useOkButton && _guideCue.useCloseButton)
                {
                    // Close Button만 표시
                    _closeButton.transform.DOLocalMoveX(180f, 0f);
                }
                else if (!_guideCue.useOkButton && _guideCue.useCloseButton)
                {
                    // OK Button만 표시
                    _okButton.transform.DOLocalMoveX(155f, 0f);
                }
                else if (_guideCue.useOkButton && _guideCue.useCloseButton)
                {
                    // OK, Close 둘다 표시
                    _okButton.transform.DOLocalMoveX(155f, 0f);
                    _closeButton.transform.DOLocalMoveX(20f, 0f);
                }

                if(_hasInteraction) _nextIndicator.gameObject.SetActive(false);
                else _nextIndicator.gameObject.SetActive(true);
            }
            else
            {
                _nextIndicator.gameObject.SetActive(true);
            }

            // 글중에 따른 중앙정렬
            if (contentHeight > 104)
            {
                Vector2 pos = _textLabel.transform.localPosition;
                pos.y = 48f;
                _textLabel.transform.localPosition = pos;
            }
            else
            {
                Vector2 pos = _textLabel.transform.localPosition;
                pos.y = (contentHeight / 2) - 4f;
                _textLabel.transform.localPosition = pos;
            }

            //Debug.Log(_textLabel.transform.localPosition.y + " / " + _textLabel.height);
            _okButton.transform.DOLocalMoveY(_textLabel.transform.localPosition.y - _textLabel.height - 45f, 0f);
            _closeButton.transform.DOLocalMoveY(_textLabel.transform.localPosition.y - _textLabel.height - 45f, 0f);
        }

        private bool IsLastText()
        {
            return (_currentTextCueIndex == _guideCue.textCues.Count - 1) ? true : false;
        }

        public void ShowBg()
        {
            _bg.SetActive(true);
        }

        public void ChangeBgAlpha(float alpha)
        {
            Debug.Log("-------------------change a: " + alpha);
            Color color = _bg.GetComponent<UISprite>().color;
            color.a = alpha;
            _bg.GetComponent<UISprite>().color = color;
        }

        public void HideBg()
        {
            _bg.SetActive(false);
        }
    }
}