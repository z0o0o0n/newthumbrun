﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageTutorialPointManger : MonoBehaviour
{
    public delegate void TutorialPointEvent(string id);
    public event TutorialPointEvent Entered;

    [SerializeField]
    private List<StageTutorialPoint> _tutorialPoints;

    private void Awake()
    {
        for(int i = 0; i < _tutorialPoints.Count; i++)
        {
            _tutorialPoints[i].Enter += OnEntered;
        }
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    private void OnDestroy()
    {
        for (int i = 0; i < _tutorialPoints.Count; i++)
        {
            _tutorialPoints[i].Enter -= OnEntered;
        }
    }





    private void OnEntered(string id)
    {
        if (Entered != null) Entered(id);
    }
}
