﻿namespace Com.Mod.ThumbRun.Tutorial.View
{
    using Com.Mod.ThumbRun.Tutorial.Application;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class LobbyTutorialScreen : MonoBehaviour
    {
        [SerializeField]
        private TutorialService _tutorialService;
        [SerializeField]
        private EnterFirstRaceGuide _firstRaceGuide;
        [SerializeField]
        private UIWidget _vsComButton;

        private void Awake()
        {
            transform.localPosition = Vector2.zero;
            _tutorialService.Prepared += OnTutorialServicePrepared;
        }

        void Start()
        {

        }

        void Update()
        {
            _tutorialService.Prepared -= OnTutorialServicePrepared;
        }




        private void OnTutorialServicePrepared()
        {
            //if (_tutorialService.GetData().isFirstTimeLobby)
            //{
            //    ShowFirstRaceGuide();
            //}
        }




        public void ShowFirstRaceGuide()
        {
            _firstRaceGuide.Show(_vsComButton);
        }
    }
}