﻿namespace Com.Mod.ThumbRun.Tutorial.View
{
    using Application;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class RaceTutorialScreen : MonoBehaviour
    {
        [SerializeField]
        private TutorialService _tutorialService;
        [SerializeField]
        private TipBook _raceTipPopup;
        [SerializeField]
        private RaceInfoPanel _raceInfoPanel;
        [SerializeField]
        private UISprite _bg;
        [SerializeField]
        private UIBasicButton _closeButton;
        [SerializeField]
        private LuckyChanceTip _luckyChanceTip;
        [SerializeField]
        private GameObject _homeTipPrefab;
        [SerializeField]
        private GameObject _homeButtonGO;
        private HomeTip _homeTip;
        [SerializeField]
        private Area52Tip _area52Tip;
        [SerializeField]
        private MonsterFieldTip _monsterFieldTip;

        private GameManager _gameManager;
        private RaceManager _raceManager;

        private void Awake()
        {
            //_gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
            //_raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
            //_raceManager.RacePrepare += OnRoutePreviewStart;

            //_tutorialService.Prepared += OnTutorialServicePrepared;

            //_raceInfoPanel.ResultShowed += OnRaceInfoPanelResultShowed;

            //_bg.transform.localPosition = Vector2.zero;

            //HideRaceTipPopup();
        }

        void Start()
        {
            
        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _raceManager.RacePrepare -= OnRoutePreviewStart;

            _tutorialService.Prepared -= OnTutorialServicePrepared;

            _raceInfoPanel.ResultShowed -= OnRaceInfoPanelResultShowed;
        }




        private void OnRoutePreviewStart()
        {
            if (_tutorialService.GetData().isFirstTimeRace)
            {
                ShowRaceTipPopup();
                return;
            }

            if (!_tutorialService.GetData().isFirstTimeRace)
            {
                HideRaceTipPopup();
            }

            if (RaceModeInfo.stageId == "1")
            {
                if (!_tutorialService.GetData().isFirstTimeRace && _tutorialService.GetData().isFirstTimeBBBoard)
                {
                    ShowMonsterFieldTip();
                    _tutorialService.GetData().isFirstTimeBBBoard = false;
                    return;
                }
            }

            if (RaceModeInfo.stageId == "2")
            {
                if (!_tutorialService.GetData().isFirstTimeRace && _tutorialService.GetData().isFirstTimePortal)
                {
                    ShowArea52Tip();
                    _tutorialService.GetData().isFirstTimePortal = false;
                    return;
                }
            }

            _raceManager.StartRoutePreview();
        }

        private void OnTutorialServicePrepared()
        {
            _tutorialService.GetData().isFirstTimeLobby = false;
        }

        public void OnCloseButtonClick()
        {
            HideRaceTipPopup();
            _raceManager.StartRoutePreview();
        }

        private void OnRaceInfoPanelResultShowed()
        {
            //Debug.Log(">>>>>>>>>>>>>>>>>> Result Showed / isFirstTimeRace: " + _tutorialService.GetData().isFirstTimeRace);
            //if (_tutorialService.GetData().isFirstTimeRace)
            //{
            //    ShowLuckyChanceTip();
            //    _tutorialService.GetData().isFirstTimeRace = false;
            //}

            //if(!_tutorialService.IsBasicTutorialEnd())
            //{
            //    _tutorialService.EndBasicTutorial();
            //}
        }




        private void ShowRaceTipPopup()
        {
            _raceTipPopup.Show();
            _bg.gameObject.SetActive(true);
            _closeButton.gameObject.SetActive(true);
        }

        private void HideRaceTipPopup()
        {
            _raceTipPopup.Hide();
            _bg.gameObject.SetActive(false);
            _closeButton.gameObject.SetActive(false);
        }

        private void ShowLuckyChanceTip()
        {
            _luckyChanceTip.Closed += OnLuckyChanceTipClosed;
            _luckyChanceTip.Show();
        }

        private void OnLuckyChanceTipClosed()
        {
            _luckyChanceTip.Closed -= OnLuckyChanceTipClosed;
            ShowHomeTip();
        }

        private void ShowHomeTip()
        {
            _homeTip = GameObject.Instantiate(_homeTipPrefab).GetComponent<HomeTip>();
            _homeTip.transform.parent = transform;
            _homeTip.transform.localPosition = Vector3.zero;
            _homeTip.transform.localScale = Vector3.one;
            _homeTip.Show(_homeButtonGO);
        }

        private void ShowArea52Tip()
        {
            _area52Tip.Closed += OnArea52TipClosed;
            _area52Tip.Show();
        }

        private void OnArea52TipClosed()
        {
            _area52Tip.Closed -= OnArea52TipClosed;
            _raceManager.StartRoutePreview();
        }

        private void ShowMonsterFieldTip()
        {
            _monsterFieldTip.Closed += OnMonsterFieldTipClosed;
            _monsterFieldTip.Show();
        }

        private void OnMonsterFieldTipClosed()
        {
            _monsterFieldTip.Closed -= OnMonsterFieldTipClosed;
            _raceManager.StartRoutePreview();
        }
    }
}