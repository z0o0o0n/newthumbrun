﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageTutorialPoint : MonoBehaviour
{
    public delegate void TutorialPointEvent(string id);
    public event TutorialPointEvent Enter;

    [SerializeField]
    private string _id;
    private bool _isEnter = false;

	void Start ()
    {
        if (!RaceModeInfo.isTutorialMode) gameObject.SetActive(false);
	}
	
	void Update ()
    {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (!_isEnter)
            {
                _isEnter = true;
                if (Enter != null) Enter(_id);
                //Debug.Log("=========== trigger id: " + _id);
            }
        }
    }
}
