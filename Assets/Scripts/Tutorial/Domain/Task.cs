﻿namespace Com.Mod.ThumbRun.Tutorial.Domain
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class Task : MonoBehaviour
    {
        public delegate void TutorialCueEvent(int taskIndex, int guideCueIndex, GuideCue guideCue);
        public event TutorialCueEvent Cue;

        protected List<GuideCue> _guideGues;
        protected string _taskName;

        void Start()
        {

        }

        void Update()
        {

        }





        protected void StartTask()
        {

        }

        protected void StartGuideCue()
        {

        }

        protected void DispatchCue(int taskIndex, int guideCueIndex, GuideCue guideCue)
        {
            if (Cue != null) Cue(taskIndex, guideCueIndex, guideCue);
        }
    }
}