﻿namespace Com.Mod.ThumbRun.Tutorial.Domain
{
    using GameDataEditor;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class Tutorial : MonoBehaviour 
	{
        public delegate void TutorialEventHandler();
        public event TutorialEventHandler Prepared;

        private bool _isPrepared = false;
        private GDETutorialData _rawData;
        private int _cueIndex = 0;

        void Start ()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Tutorial_Tutorial, out _rawData))
            {
                _isPrepared = true;
                if (Prepared != null) Prepared();
                //_rawData.ResetAll(); //지울것
                //EndBasicTutorial();
            }
            else
            {
                Debug.LogError("Read Error Tutorial Data");
            }
        }
		
		void Update () 
		{
			
		}




        public GDETutorialData GetData()
        {
            return _rawData;
        }

        public void ResetRawData()
        {
            Debug.Log("Reset Tutorial Raw Data");
            _rawData.ResetAll();
        }

        // Basic Tutorial
        public bool IsBasicTutorialEnd()
        {
            return _rawData.isBasicTutorialEnd;
        }

        public void StartBasicTutorial()
        {
            _rawData.isBasicTutorialEnd = false;
        }

        public void EndBasicTutorial()
        {
            _rawData.isBasicTutorialEnd = true;
        }

        // Condition Tutorial
        public bool IsConditionTutorialEnd()
        {
            return _rawData.isConditionTutorialEnd;
        }

        public void StartConditionTutorial()
        {
            _rawData.isConditionTutorialEnd = false;
        }

        public void EndConditionTutorial()
        {
            _rawData.isConditionTutorialEnd = true;
        }

        public int GetTaskIndex()
        {
            return _rawData.taskIndex;
        }

        public void SetTaskIndex(int taskIndex)
        {
            _rawData.taskIndex = taskIndex;
        }

        public int GetCueIndex()
        {
            return _cueIndex;
        }

        public void SetCueIndex(int cueIndex)
        {
            _cueIndex = cueIndex;
        }
    }	
}