﻿namespace Com.Mod.ThumbRun.Tutorial.Domain
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class GuideCue : MonoBehaviour
    {
        public int index;
        public List<TextCue> textCues;
        public bool useOkButton = false;
        public bool useCloseButton = false;
        public bool useInteraction = false;
        public Vector2 popupPos = Vector2.zero;

        void Start()
        {

        }

        void Update()
        {

        }
    }
}