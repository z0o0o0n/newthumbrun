﻿using Com.Mod.Game.LanguageManager;
using Com.Mod.ThumbRun.GUI;
using Com.Mod.ThumbRun.Tutorial.Application;
using Com.Mod.ThumbRun.Tutorial.Domain;
using Com.Mod.ThumbRun.Tutorial.View;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceTutorialTask : Task
{
    [SerializeField]
    private TutorialGuidePopup _tutorialGuidePopup;
    [SerializeField]
    private TutorialService _tutorialService;
    [SerializeField]
    private CameraController _cameraController;
    [SerializeField]
    private BgmManager _bgmManager;
    [SerializeField]
    private TutorialInteractionUI _tutorialInteractionUI;
    [SerializeField]
    private PlayerControlButton _playerControlButton;
    [SerializeField]
    private StageTutorialPointManger _stageTutorialPointManager;
    [SerializeField]
    private ItemButton _itemButton;
    [SerializeField]
    private RaceInfoPanel _raceInfoPanel;
    [SerializeField]
    private PlayScene _playScene;
    private RaceManager _raceManager;
    private List<GuideCue> _guideCues;

    private void Awake()
    {
        if (!RaceModeInfo.isTutorialMode)
        {
            _tutorialGuidePopup.gameObject.SetActive(false);
            return;
        }
        base._taskName = "RaceTutorial";

        _raceManager = GameObject.FindGameObjectWithTag("RaceManager").GetComponent<RaceManager>();
        _raceManager.CountdownEnd += OnCountdownEnd;
        _raceManager.RoundEnd += OnRaceRoundEnd;

        _tutorialService.Prepared += OnPrepared;
        _tutorialGuidePopup.InteractiveButtonClick += OnInteractiveButtonClick;

        _cameraController.PreviewZoomOutEnd += OnPreviewZoomOutEnd;

        _raceInfoPanel.ResultShowed += OnResultShowed;

        _stageTutorialPointManager.Entered += OnTutorialPointEntered;

        CreateTask();
    }

    void Start()
    {

    }

    void Update()
    {
    }

    private void OnDestroy()
    {
        if(_raceManager)
        {
            _raceManager.CountdownEnd -= OnCountdownEnd;
            _raceManager.RoundEnd -= OnRaceRoundEnd;
        }

        _tutorialService.Prepared -= OnPrepared;
        _tutorialGuidePopup.InteractiveButtonClick -= OnInteractiveButtonClick;

        _cameraController.PreviewZoomOutEnd -= OnPreviewZoomOutEnd;

        _raceInfoPanel.ResultShowed -= OnResultShowed;

        _stageTutorialPointManager.Entered -= OnTutorialPointEntered;
    }





    private void OnPrepared()
    {
        if (!_tutorialService.isPrepared) return;

        StartRaceTutorial();
    }

    private void OnInteractiveButtonClick(string buttonType)
    {
        if (buttonType == "OK")
        {
            Debug.Log(">>>>>>>>> OnInteractiveButtonClick / cue index: " + _tutorialService.GetCueIndex());
            if (_tutorialService.GetCueIndex() == 0) // 맵 프리뷰 큐 종료
            {
                Time.timeScale = 1;
                _bgmManager.Resume();
                _tutorialGuidePopup.ChangeBgAlpha(0.01f);
            }
            else if (_tutorialService.GetCueIndex() == 1)
            {
                Debug.Log("=====> Go Lobby");
            }
            else if(_tutorialService.GetCueIndex() == 6) // 체크포인트 큐 종료
            {
                Time.timeScale = 1;
                _bgmManager.Resume();
            }
            else if (_tutorialService.GetCueIndex() == 9) // 시작포인트 큐 종료
            {
                Time.timeScale = 1;
                _bgmManager.Resume();
                _tutorialGuidePopup.HideBg();
            }
            else if (_tutorialService.GetCueIndex() == 10) // 결과화면 큐 종료
            {
                Time.timeScale = 1;
                _playScene.GoHome();
                _tutorialService.EndBasicTutorial();
            }
            _tutorialGuidePopup.HidePopup();
        }
        else if (buttonType == "CLOSE")
        {
            if (_tutorialService.GetCueIndex() == 0)
            {
                Debug.Log("=====> End Basic Tutorial");
            }
            _tutorialGuidePopup.HidePopup();
        }
    }

    private void OnTutorialPointEntered(string tutorialPointId)
    {
        if (tutorialPointId == "0")
        {
            _tutorialService.SetCueIndex(2);
            _tutorialInteractionUI.RightButtonPress += OnRightButtonPressed;
            _tutorialInteractionUI.ShowRightButton();

            Time.timeScale = 0;
            _bgmManager.Pause();
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
        else if (tutorialPointId == "1")
        {
            _tutorialService.SetCueIndex(3);
            _tutorialInteractionUI.LeftButtonPress += OnLeftButtonPressed;
            _tutorialInteractionUI.ShowLeftButton();

            Time.timeScale = 0;
            _bgmManager.Pause();
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
        else if (tutorialPointId == "2")
        {
            _tutorialService.SetCueIndex(4);
            _tutorialInteractionUI.RightButtonPress += OnRightButtonPressed;
            _tutorialInteractionUI.ShowRightButton();

            Time.timeScale = 0;
            _bgmManager.Pause();
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
        else if (tutorialPointId == "3")
        {
            _tutorialService.SetCueIndex(5);
            _tutorialInteractionUI.LeftButtonPress += OnLeftButtonPressed;
            _tutorialInteractionUI.ShowLeftButton();

            Time.timeScale = 0;
            _bgmManager.Pause();
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
        else if (tutorialPointId == "4")
        {
            _tutorialService.SetCueIndex(6);
            _tutorialInteractionUI.BoostButtonPress += OnBoostButtonPressed;
            _tutorialInteractionUI.ShowBoostButton();

            Time.timeScale = 0;
            _bgmManager.Pause();
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
        else if (tutorialPointId == "5")
        {
            _tutorialService.SetCueIndex(7);
            _tutorialInteractionUI.LeftButtonPress += OnLeftButtonPressed;
            _tutorialInteractionUI.ShowLeftButton();

            Time.timeScale = 0;
            _bgmManager.Pause();
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
        else if (tutorialPointId == "6")
        {
            _tutorialService.SetCueIndex(8);
            _tutorialInteractionUI.RightButtonPress += OnRightButtonPressed;
            _tutorialInteractionUI.ShowRightButton();

            Time.timeScale = 0;
            _bgmManager.Pause();
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
    }

    private void OnPreviewZoomOutEnd() // 1. 맵 프리뷰 큐 시작
    {
        _tutorialService.SetCueIndex(0);

        _tutorialGuidePopup.ChangeBgAlpha(0.5f);

        Time.timeScale = 0;
        _bgmManager.Pause();
        base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
    }

    private void OnResultShowed() // 결과화면 큐 시작
    {
        Debug.Log("!!!!!!!!!! result showed");
        _tutorialService.SetCueIndex(10);

        _tutorialGuidePopup.ChangeBgAlpha(0.5f);

        Time.timeScale = 0;
        base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
    }

    private void OnCountdownEnd()
    {
        _tutorialService.SetCueIndex(1);
        _tutorialInteractionUI.RightButtonPress += OnRightButtonPressed;
        _tutorialInteractionUI.ShowRightButton();

        _tutorialGuidePopup.ChangeBgAlpha(0.01f);

        Time.timeScale = 0;
        _bgmManager.Pause();
        base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
    }

    private void OnRaceRoundEnd(int currentLap)
    {
        if(currentLap == 2)
        {
            _tutorialService.SetCueIndex(9);

            Time.timeScale = 0;
            _bgmManager.Pause();
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
    }

    private void OnRightButtonPressed()
    {
        Time.timeScale = 1;

        _tutorialInteractionUI.RightButtonPress -= OnRightButtonPressed;
        _tutorialInteractionUI.HideRightButton();

        _playerControlButton.PressRight();

        _bgmManager.Resume();
        _tutorialGuidePopup.HidePopup();
    }

    private void OnLeftButtonPressed()
    {
        Time.timeScale = 1;

        _tutorialInteractionUI.LeftButtonPress -= OnLeftButtonPressed;
        _tutorialInteractionUI.HideLeftButtion();

        _playerControlButton.PressLeft();

        _bgmManager.Resume();
        _tutorialGuidePopup.HidePopup();
    }

    private void OnBoostButtonPressed()
    {
        Time.timeScale = 1;

        _tutorialInteractionUI.BoostButtonPress -= OnBoostButtonPressed;
        _tutorialInteractionUI.HideBoostButton();

        _itemButton.PressButton();

        _bgmManager.Resume();
        _tutorialGuidePopup.HidePopup();
    }





    private void CreateTask()
    {
        _guideCues = new List<GuideCue>();
        Vector2 topAlign = new Vector2(0f, UIScreen.instance.GetHeight() / 2 - 100f);

        // index 0
        List<TextCue> textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_1_0")));
        _guideCues.Add(CreateGuideCue(1, textCues, Vector2.zero, true));
        // index 1
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_2_0")));
        _guideCues.Add(CreateGuideCue(2, textCues, topAlign, false, false, true));
        // index 2
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_3_0")));
        _guideCues.Add(CreateGuideCue(3, textCues, topAlign, false, false, true));
        // index 3
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_4_0")));
        _guideCues.Add(CreateGuideCue(4, textCues, topAlign, false, false, true));
        // index 4
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_5_0")));
        _guideCues.Add(CreateGuideCue(5, textCues, topAlign, false, false, true));
        // index 5
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_6_0")));
        _guideCues.Add(CreateGuideCue(6, textCues, topAlign, false, false, true));
        // index 6
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_7_0")));
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_7_1")));
        _guideCues.Add(CreateGuideCue(7, textCues, topAlign, false, false, true));
        // index 7
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_8_0")));
        _guideCues.Add(CreateGuideCue(8, textCues, topAlign, false, false, true));
        // index 8
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_9_0")));
        _guideCues.Add(CreateGuideCue(9, textCues, topAlign, false, false, true));
        // index 9
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_10_0")));
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_10_1")));
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_10_2")));
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_10_3")));
        _guideCues.Add(CreateGuideCue(10, textCues, topAlign, true));
        // index 10
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_11_0")));
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_11_1")));
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_11_2")));
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_1_11_3")));
        _guideCues.Add(CreateGuideCue(11, textCues, Vector2.zero, true));
    }

    private GuideCue CreateGuideCue(int id, List<TextCue> textCues, Vector2 pos, bool useOkButton = false, bool useCloseButton = false, bool useInteraction = false)
    {
        GuideCue guideCue = new GuideCue();
        guideCue.index = id;
        guideCue.useOkButton = useOkButton;
        guideCue.useCloseButton = useCloseButton;
        guideCue.useInteraction = useInteraction;
        guideCue.textCues = textCues;
        guideCue.popupPos = pos;
        return guideCue;
    }

    private TextCue CreateTextCue(string text)
    {
        TextCue textCue = new TextCue();
        textCue.text = text;
        return textCue;
    }





    private void StartRaceTutorial()
    {
        _tutorialService.SetTaskIndex(1);
        _tutorialService.SetCueIndex(0);
        base.StartTask();
    }
}
