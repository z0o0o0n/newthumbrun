﻿using Com.Mod.Game.LanguageManager;
using Com.Mod.ThumbRun.CharacterManagement.Application;
using Com.Mod.ThumbRun.CharacterManagement.View;
using Com.Mod.ThumbRun.GUI;
using Com.Mod.ThumbRun.Tutorial.Application;
using Com.Mod.ThumbRun.Tutorial.Domain;
using Com.Mod.ThumbRun.Tutorial.View;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionTutorialTask : Task
{
    [SerializeField]
    private TutorialGuidePopup _tutorialGuidePopup;
    [SerializeField]
    private TutorialService _tutorialService;
    [SerializeField]
    private GameObject _characterManagementPopupButtonHP;
    [SerializeField]
    private GameObject _conditionIconHP;
    [SerializeField]
    private GameObject _skillIconHP;
    [SerializeField]
    private GameObject _missionIconHP;
    [SerializeField]
    private GameObject _skillChangeHP;
    [SerializeField]
    private GameObject _spinHP;
    [SerializeField]
    private UIButtonEventDispatcher _characterManagementPopupButton;
    [SerializeField]
    private CharacterManagementPopup _characterManagementPopup;
    [SerializeField]
    private SkillSlotMachine _skillSlotMachinePopup;
    [SerializeField]
    private UIButtonEventDispatcher _conditionIconButton;
    [SerializeField]
    private UIButtonEventDispatcher _skillIconButton;
    [SerializeField]
    private UIButtonEventDispatcher _missionIconButton;
    [SerializeField]
    private UIButtonEventDispatcher _skillChangeButton;
    [SerializeField]
    private UIButtonEventDispatcher _spinButton;
    private List<GuideCue> _guideCues;
    private ConditionInfoBubble _conditionInfoBubble;
    private SkillInfoBubble _skillInfoBubble;
    private SkillMissionInfoBubble _skillMissionInfoBubble;
    private CharacterManagementService _characterManagementService;

    private void Awake()
    {
        base._taskName = "ConditionTutorial";

        _conditionInfoBubble = GameObject.FindGameObjectWithTag("ConditionInfoBubble").GetComponent<ConditionInfoBubble>();
        _skillInfoBubble = GameObject.FindGameObjectWithTag("SkillInfoBubble").GetComponent<SkillInfoBubble>();
        _skillMissionInfoBubble = GameObject.FindGameObjectWithTag("SkillMissionInfoBubble").GetComponent<SkillMissionInfoBubble>();
        _characterManagementService = GameObject.FindGameObjectWithTag("CharacterManagement").GetComponent<CharacterManagementService>();

        _tutorialService.Prepared += OnPrepared;
        _tutorialGuidePopup.InteractiveButtonClick += OnInteractiveButtonClick;

        _characterManagementPopupButtonHP.gameObject.SetActive(false);
        _characterManagementPopupButton.gameObject.SetActive(false);
        _characterManagementPopupButton.On_ButtonPress += OnCMPopupButtonPress;

        _conditionIconHP.SetActive(false);
        _conditionIconButton.gameObject.SetActive(false);
        _conditionIconButton.On_ButtonPress += OnConditionIconButtonPress;

        _skillIconHP.SetActive(false);
        _skillIconButton.gameObject.SetActive(false);
        _skillIconButton.On_ButtonPress += OnSkillIconButtonPress;

        _missionIconHP.SetActive(false);
        _missionIconButton.gameObject.SetActive(false);
        _missionIconButton.On_ButtonPress += OnMissionIconButtonPress;

        _skillChangeHP.SetActive(false);
        _skillChangeButton.gameObject.SetActive(false);
        _skillChangeButton.On_ButtonPress += OnSkillChangeButtonPress;

        _spinHP.SetActive(false);
        _spinButton.gameObject.SetActive(false);
        _spinButton.On_ButtonPress += OnSpinButtonPress;

        CreateTask();
    }

    void Start()
    {

    }

    void Update()
    {
    }

    private void OnDestroy()
    {
        _tutorialService.Prepared -= OnPrepared;
        _tutorialGuidePopup.InteractiveButtonClick -= OnInteractiveButtonClick;

        _characterManagementPopupButton.On_ButtonPress -= OnCMPopupButtonPress;

        _conditionIconButton.On_ButtonPress -= OnConditionIconButtonPress;

        _skillIconButton.On_ButtonPress -= OnSkillIconButtonPress;

        _missionIconButton.On_ButtonPress -= OnMissionIconButtonPress;

        _skillChangeButton.On_ButtonPress -= OnSkillChangeButtonPress;

        _spinButton.On_ButtonPress -= OnSpinButtonPress;
    }





    private void OnPrepared()
    {
        if (!_tutorialService.isPrepared) return;

        StartConditionTutorial();
    }

    private void OnInteractiveButtonClick(string buttonType)
    {
        if (buttonType == "OK")
        {
            Debug.Log("----- ok: index: " + _tutorialService.GetCueIndex());
            if (_tutorialService.GetCueIndex() == 0) // Tutorial_2_0_0
            {
                _tutorialService.SetCueIndex(1);
                base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);

                _tutorialGuidePopup.ChangeBgAlpha(0.01f);
                _characterManagementPopupButtonHP.gameObject.SetActive(true);
                _characterManagementPopupButton.gameObject.SetActive(true);
            }
            else if (_tutorialService.GetCueIndex() == 2) // Condition 설명 종료
            {
                _conditionIconHP.SetActive(true);
                _conditionIconButton.gameObject.SetActive(true);

                _tutorialService.SetCueIndex(3);
                base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
            }
            else if (_tutorialService.GetCueIndex() == 4) // Skill 설명 시작
            {
                _tutorialService.SetCueIndex(5);
                base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
            }
            else if (_tutorialService.GetCueIndex() == 5) // Skill 설명 시작
            {
                _skillIconHP.SetActive(true);
                _skillIconButton.gameObject.SetActive(true);

                _tutorialService.SetCueIndex(6);
                base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
            }
            else if (_tutorialService.GetCueIndex() == 7)
            {
                _missionIconHP.SetActive(true);
                _missionIconButton.gameObject.SetActive(true);

                _tutorialService.SetCueIndex(8);
                base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
            }
            else if (_tutorialService.GetCueIndex() == 9)
            {
                _tutorialService.SetCueIndex(10);
                base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
            }
            else if (_tutorialService.GetCueIndex() == 11)
            {
                _tutorialGuidePopup.HidePopup();
                _tutorialGuidePopup.HideBg();

                _tutorialService.EndConditionTutorial();
            }
        }
        else if (buttonType == "CLOSE")
        {
            if (_tutorialService.GetCueIndex() == 0) // Tutorial_2_0_0
            {
                Debug.Log("=====> End Condition Tutorial");
                _tutorialGuidePopup.HidePopup();
                _tutorialGuidePopup.HideBg();

                _tutorialService.EndConditionTutorial();
            }
        }
    }

    private void OnCMPopupButtonPress(bool isPress)
    {
        if(!isPress)
        {
            _characterManagementPopupButtonHP.gameObject.SetActive(false);
            _characterManagementPopupButton.gameObject.SetActive(false);

            _characterManagementPopup.OpenPopup(2);

            _tutorialGuidePopup.ChangeBgAlpha(0.3f);

            _tutorialService.SetCueIndex(2);
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
    }

    private void OnConditionIconButtonPress(bool isPress)
    {
        if(isPress)
        {
            int thumbKimCharacterId = 2;
            GDECharacterManagementItemData cmItem = _characterManagementService.GetItemById(thumbKimCharacterId);
            _conditionInfoBubble.Show(cmItem.condition, _conditionIconButton.transform.position, ConditionInfoBubble.DisplayDirection.T);
        }
        else
        {
            _conditionInfoBubble.Hide();

            _conditionIconHP.SetActive(false);
            _conditionIconButton.gameObject.SetActive(false);

            _tutorialService.SetCueIndex(4);
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
    }

    private void OnSkillIconButtonPress(bool isPress)
    {
        if(isPress)
        {
            int thumbKimCharacterId = 2;
            GDECharacterManagementItemData cmItem = _characterManagementService.GetItemById(thumbKimCharacterId);
            _skillInfoBubble.Show(cmItem.basicSkillId, _skillIconButton.transform.position, SkillInfoBubble.DisplayDirection.T);
        }
        else
        {
            _skillInfoBubble.Hide();

            _skillIconHP.SetActive(false);
            _skillIconButton.gameObject.SetActive(false);

            _tutorialService.SetCueIndex(7);
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
    }

    private void OnMissionIconButtonPress(bool isPress)
    {
        if(isPress)
        {
            int thumbKimCharacterId = 2;
            GDECharacterManagementItemData cmItem = _characterManagementService.GetItemById(thumbKimCharacterId);
            _skillMissionInfoBubble.Show(cmItem.basicSkillId, 1, _missionIconButton.transform.position, SkillMissionInfoBubble.DisplayDirection.T);
        }
        else
        {
            _skillMissionInfoBubble.Hide();

            _missionIconHP.SetActive(false);
            _missionIconButton.gameObject.SetActive(false);
            _skillChangeHP.SetActive(true);
            _skillChangeButton.gameObject.SetActive(true);

            _tutorialService.SetCueIndex(9);
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
    }

    private void OnSkillChangeButtonPress(bool isPress)
    {
        if(!isPress)
        {
            _skillChangeHP.SetActive(false);
            _skillChangeButton.gameObject.SetActive(false);
            _spinHP.SetActive(true);
            _spinButton.gameObject.SetActive(true);

            int thumbKimCharacterId = 2;
            _skillSlotMachinePopup.OpenPopup(thumbKimCharacterId);

            _tutorialService.SetCueIndex(10);
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
    }

    private void OnSpinButtonPress(bool isPress)
    {
        if(!isPress)
        {
            _spinHP.SetActive(false);
            _spinButton.gameObject.SetActive(false);

            int thumbKimCharacterId = 2;
            _skillSlotMachinePopup.Spin(thumbKimCharacterId);

            _tutorialService.SetCueIndex(11);
            base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
        }
    }





    private void CreateTask()
    {
        _guideCues = new List<GuideCue>();
        Vector2 topAlign = new Vector2(0f, UIScreen.instance.GetHeight() / 2 - 110f);
        Vector2 bottomAlign = new Vector2(0f, -(UIScreen.instance.GetHeight() / 2) + 150f);

        // index 0
        List<TextCue> textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_0_0")));
        _guideCues.Add(CreateGuideCue(1, textCues, Vector2.zero, true, true));
        // index 1
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_1_0")));
        _guideCues.Add(CreateGuideCue(2, textCues, Vector2.zero, false, false, true));
        // index 2
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_2_0")));
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_2_1")));
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_2_2")));
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_2_3")));
        _guideCues.Add(CreateGuideCue(3, textCues, topAlign, true));
        // index 3
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_2_4")));
        _guideCues.Add(CreateGuideCue(4, textCues, topAlign, false, false, true));
        // index 4
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_3_0")));
        _guideCues.Add(CreateGuideCue(5, textCues, topAlign, true));
        // index 5
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_4_0")));
        _guideCues.Add(CreateGuideCue(6, textCues, topAlign, true));
        // index 6
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_4_1")));
        _guideCues.Add(CreateGuideCue(7, textCues, topAlign, false, false, true));
        // index 7
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_5_0")));
        _guideCues.Add(CreateGuideCue(8, textCues, topAlign, true));
        // index 8
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_5_1")));
        _guideCues.Add(CreateGuideCue(9, textCues, topAlign, false, false, true));
        // index 9
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_6_0")));
        _guideCues.Add(CreateGuideCue(10, textCues, topAlign, false, false, true));
        // index 10
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_7_0")));
        _guideCues.Add(CreateGuideCue(11, textCues, topAlign, false, false, true));
        // index 11
        textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_2_8_0")));
        _guideCues.Add(CreateGuideCue(12, textCues, bottomAlign, true));
    }

    private GuideCue CreateGuideCue(int id, List<TextCue> textCues, Vector2 pos, bool useOkButton = false, bool useCloseButton = false, bool useInteraction = false)
    {
        GuideCue guideCue = new GuideCue();
        guideCue.index = id;
        guideCue.useOkButton = useOkButton;
        guideCue.useCloseButton = useCloseButton;
        guideCue.useInteraction = useInteraction;
        guideCue.textCues = textCues;
        guideCue.popupPos = pos;
        return guideCue;
    }

    private TextCue CreateTextCue(string text)
    {
        TextCue textCue = new TextCue();
        textCue.text = text;
        return textCue;
    }





    private void StartConditionTutorial()
    {
        if (!_tutorialService.IsBasicTutorialEnd()) return;
        if (_tutorialService.IsConditionTutorialEnd()) return;

        _tutorialService.SetTaskIndex(2);
        _tutorialService.SetCueIndex(0);
        base.StartTask();
        base.DispatchCue(_tutorialService.GetTaskIndex(), _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);

        _tutorialService.StartConditionTutorial();
    }
}
