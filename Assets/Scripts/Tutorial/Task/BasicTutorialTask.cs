﻿using Com.Mod.Game.LanguageManager;
using Com.Mod.NGUI.LogConsole;
using Com.Mod.ThumbRun.Race.Application;
using Com.Mod.ThumbRun.Tutorial.Application;
using Com.Mod.ThumbRun.Tutorial.Domain;
using Com.Mod.ThumbRun.Tutorial.View;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTutorialTask : Task
{
    [SerializeField]
    private TutorialGuidePopup _tutorialGuidePopup;
    [SerializeField]
    private TutorialService _tutorialService;
    [SerializeField]
    private EntryService _entryService;
    [SerializeField]
    private MainScene _mainScene;
    private List<GuideCue> _guideCues;
    private bool _isTaskCreated = false;

    private void Awake()
    {
        base._taskName = "BasicTutorial";
        _tutorialService.Prepared += OnPrepared;

        _entryService.Prepared += OnPrepared;

        if (_tutorialService.isPrepared && _entryService.isPrepared) OnPrepared();

        _tutorialGuidePopup.InteractiveButtonClick += OnInteractiveButtonClick;

        CreateTask();
    }

    void Start ()
    {
        
	}

    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Z))
        {
            if(_tutorialService.GetCueIndex() == 0)
            {
                base.DispatchCue(0, _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
            }
        }
        else if(Input.GetKeyUp(KeyCode.X))
        {
            if (_tutorialService.GetCueIndex() == 1)
            {
                base.DispatchCue(1, _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);
            }
        }
    }

    private void OnDestroy()
    {
        _tutorialService.Prepared -= OnPrepared;
        _tutorialGuidePopup.InteractiveButtonClick -= OnInteractiveButtonClick;
    }





    private void OnPrepared()
    {
        Debug.Log("OnPrepared / _isTaskCreated: " + _isTaskCreated + " / _tutorialService.isPrepared: " + _tutorialService.isPrepared);
        if (!_isTaskCreated) return;
        if (!_tutorialService.isPrepared) return;
        if (!_entryService.isPrepared) return;

        StartBasicTutorial();
    }

    private void OnInteractiveButtonClick(string buttonType)
    {
        if(buttonType == "OK")
        {
            if(_tutorialService.GetCueIndex() == 0) // 튜토리얼 시작 수락
            {
                _mainScene.MoveToRaceScene(RaceMode.mode.SINGLE, true, "0"); // "0" Practice Field
            }
            else if(_tutorialService.GetCueIndex() == 1)
            {
                Debug.Log("=====> Go Lobby");
            }
        }
        else if(buttonType == "CLOSE")
        {
            if (_tutorialService.GetCueIndex() == 0) // 튜토리얼 시작 취소
            {
                _tutorialGuidePopup.HidePopup();
                _tutorialGuidePopup.HideBg();

                _tutorialService.EndBasicTutorial();
            }
        }
    }





    private void CreateTask()
    {
        _guideCues = new List<GuideCue>();

        List<TextCue> textCues = new List<TextCue>();
        textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_0_0_0")));
        _guideCues.Add(CreateGuideCue(0, textCues, true, true));

        _isTaskCreated = true;
        OnPrepared();

        //textCues = new List<TextCue>();
        //textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_0_4_0")));
        //textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_0_5_0")));
        //textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_0_6_0")));
        //textCues.Add(CreateTextCue(LanguageManager.instance.GetValue("Tutorial_0_7_0")));
        //_guideCues.Add(CreateGuideCue(1, textCues, false, true));
    }

    private GuideCue CreateGuideCue(int id, List<TextCue> textCues, bool useOkButton = false, bool useCloseButton = false)
    {
        GuideCue guideCue = new GuideCue();
        guideCue.index = id;
        guideCue.useOkButton = useOkButton;
        guideCue.useCloseButton = useCloseButton;
        guideCue.textCues = textCues;
        guideCue.popupPos = Vector2.zero;
        return guideCue;
    }

    private TextCue CreateTextCue(string text)
    {
        TextCue textCue = new TextCue();
        textCue.text = text;
        return textCue;
    }





    private void StartBasicTutorial()
    {
        if (_tutorialService.IsBasicTutorialEnd())
        {
            LogConsole.instance.Log("tutorial end.");
            _tutorialGuidePopup.gameObject.SetActive(false);
            return;
        }
        LogConsole.instance.Log("cue index: " + _tutorialService.GetCueIndex());
        base.StartTask();
        base.DispatchCue(0, _tutorialService.GetCueIndex(), _guideCues[_tutorialService.GetCueIndex()]);

        _tutorialGuidePopup.ChangeBgAlpha(0.5f);
        _tutorialGuidePopup.ShowBg();
        //base.StartCue();
    }
}
