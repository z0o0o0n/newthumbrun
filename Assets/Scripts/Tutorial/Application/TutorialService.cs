﻿namespace Com.Mod.ThumbRun.Tutorial.Application
{
    using GameDataEditor;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Domain;

    public class TutorialService : MonoBehaviour
    {
        public delegate void TutorialServiceEventHandler();
        public delegate void TutorialCueEvent(int taskIndex, int guideCueIndex, List<TextCue> textCues);
        public event TutorialServiceEventHandler Prepared;
        public event TutorialCueEvent Cue;

        [SerializeField]
        private Tutorial _tutorial;
        private bool _isPrepared = false;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }

        private void Awake()
        {
            _tutorial.Prepared += OnPrepared;
        }

        void Start()
        {

        }

        void Update()
        {
            // if (Input.GetKeyUp(KeyCode.Z))
            // {
            //     ResetRawData();
            // }
        }

        private void OnDestroy()
        {
            _tutorial.Prepared -= OnPrepared;
        }




        private void OnPrepared()
        {
            _isPrepared = true;
            if (Prepared != null) Prepared();
        }




        public GDETutorialData GetData()
        {
            return _tutorial.GetData();
        }

        public void ResetRawData()
        {
            _tutorial.ResetRawData();
        }

        // Basic Tutorial
        // Basic Tutorial Task(초기 튜토리얼 진입 안내) 및 Race Tutorial Task 종료까지가 Basic Tutorial임.
        public bool IsBasicTutorialEnd()
        {
            return _tutorial.IsBasicTutorialEnd();
        }

        public void StartBasicTutorial()
        {
            _tutorial.StartBasicTutorial();
        }

        public void EndBasicTutorial()
        {
            _tutorial.EndBasicTutorial();
        }

        // Condition Tutorial
        public bool IsConditionTutorialEnd()
        {
            return _tutorial.IsConditionTutorialEnd();
        }

        public void StartConditionTutorial()
        {
            _tutorial.StartConditionTutorial();
        }

        public void EndConditionTutorial()
        {
            _tutorial.EndConditionTutorial();
        }


        public int GetTaskIndex()
        {
            return _tutorial.GetTaskIndex();
        }

        public void SetTaskIndex(int taskIndex)
        {
            _tutorial.SetTaskIndex(taskIndex);
        }

        public int GetCueIndex()
        {
            return _tutorial.GetCueIndex();
        }

        public void SetCueIndex(int cueIndex)
        {
            _tutorial.SetCueIndex(cueIndex);
        }
    }
}