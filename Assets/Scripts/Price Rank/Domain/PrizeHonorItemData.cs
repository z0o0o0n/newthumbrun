﻿namespace Com.Mod.ThumbRun.PrizeRank.Domain
{
    public class PrizeHonorItemData
    {
        public string season;
        public string countryCode;
        public string nickname;
        public ulong prize;
    }
}