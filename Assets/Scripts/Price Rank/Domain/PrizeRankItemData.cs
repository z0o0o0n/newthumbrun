﻿namespace Com.Mod.ThumbRun.PrizeRank.Domain
{
    public class PrizeRankItemData
    {
        public int rank;
        public string countryCode;
        public string nickname;
        public ulong prize;
    }
}