﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeasonTimer : MonoBehaviour
{
    public delegate void SeasonTimerEvent(string remainingTimeText);
    public event SeasonTimerEvent Tick;

    private static SeasonTimer _instance;

    private int _remainingSec = 0;
    private string _remainingTimeText;

    public static SeasonTimer instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start ()
    {
		StartCoroutine(OnTick());
	}
	
	void Update ()
    {
		
	}





    private IEnumerator OnTick()
    {
        while (true)
        {
            _remainingSec -= 1;
            ChangeTimeToText(_remainingSec);
            if (Tick != null) Tick(_remainingTimeText);
            yield return new WaitForSeconds(1);
        }
    }

    private void ChangeTimeToText(int sec)
    {
        TimeSpan remainingTime = new TimeSpan(0, 0, sec);

        if (remainingTime.Days == 0)
        {
            _remainingTimeText = string.Format("{0}:{1:D2}:{2:D2}", remainingTime.Hours, remainingTime.Minutes, remainingTime.Seconds);
        }
        else
        {
            _remainingTimeText = string.Format("{0:D}d {1:D}h {2:D}m", remainingTime.Days, remainingTime.Hours, remainingTime.Minutes);
        }
    }




    public void SetRemainingSec(int sec)
    {
        _remainingSec = sec;
    }
}
