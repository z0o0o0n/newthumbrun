﻿namespace Com.Mod.ThumbRun.PrizeRank.Infrastructure
{
    using UnityEngine;
    using System.Collections.Generic;
    using Domain;
    using DG.Tweening;
    using App;
    using Util;
    using System;
    using LitJson;

    public class PrizeRankRepository : MonoBehaviour
    {
        public delegate void PrizeRankRepositoryHandler(string wwwText);
        public delegate void UserPrizeRankEvent(int currentRank, int offsetRank);
        public event PrizeRankRepositoryHandler WeeklyRankLoadCompleted;
        public event PrizeRankRepositoryHandler CountryRankLoadCompleted;
        public event PrizeRankRepositoryHandler HonorRankLoadCompleted;
        //public event UserPrizeRankEvent UserRankReceived;
        public event PrizeRankRepositoryHandler RankRecordingCompleted;
        public event PrizeRankRepositoryHandler Error;

        private List<string> _weekPlayer = new List<string> { "2", "410", "Junhee", "55000" };
        private List<string> _weekBoardCountryCodes = new List<string>() {"156", "410", "380", "156", "380" };
        private List<string> _weekBoardNickname = new List<string>() {"Thumb.Kim", "Junhee", "Jinjoo", "HaHa", "JinBong84" };
        private List<string> _weekBoardGold = new List<string>() {"10000000000", "55000", "32000", "2000", "1000" };

        private List<string> _lastweekPlayer = new List<string> { "3", "410", "Junhee", "32000" };
        private List<string> _lastweekBoardCountryCodes = new List<string>() {"170", "324", "410", "410", "410" };
        private List<string> _lastweekBoardNickname = new List<string>() {"Jamin.Koo", "Jihoon", "Junhee", "Wanda", "PongPong" };
        private List<string> _lastweekBoardGold = new List<string>() {"100000", "70000", "32000", "2000", "1000" };

        private List<string> _nationPlayer = new List<string> { "1", "410", "Junhee", "55000" };
        private List<string> _nationBoardCountryCodes = new List<string>() {"410", "410", "410" };
        private List<string> _nationBoardNickname = new List<string>() {"Junhee", "MOD", "Iznit" };
        private List<string> _nationBoardGold = new List<string>() {"55000", "32000", "2000" };

        private List<PrizeRankItemData> _weeklyData = new List<PrizeRankItemData>();
        private List<PrizeRankItemData> _countryData = new List<PrizeRankItemData>();
        private List<PrizeHonorItemData> _honorData = new List<PrizeHonorItemData>();
        private PrizeRankItemData _weeklyPlayerData;
        private PrizeRankItemData _countryPlayerData;
        //private PrizeRankItemData _honorPlayerData;

        [SerializeField]
        private string _weekRankRequestURI;
        [SerializeField]
        private string _countryRankRequestURI;
        [SerializeField]
        private string _honorListRequestURI;
        [SerializeField]
        private string _rankRecordingRequestURI;
        private HttpRequest _httpRequest;

        private void Awake()
        {
            _httpRequest = gameObject.AddComponent(typeof(HttpRequest)) as HttpRequest;
            _httpRequest.Complete += OnCompleted;
            _httpRequest.TimeOut += OnError;
            _httpRequest.Error += OnError;
        }

        void Start()
        {
            
        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            _httpRequest.Complete -= OnCompleted;
            _httpRequest.TimeOut -= OnError;
            _httpRequest.Error -= OnError;

            DOTween.Kill("RequireUserRank." + GetInstanceID());
            DOTween.Kill("LoadPrizeRankData." + GetInstanceID());
        }





        public void RequireRankRecording(string nickname, string countryCode, string prize, string uuidForDummy = null)
        {
            Debug.Log("nickname: " + nickname + " / countryCode: " + countryCode + " / prize: " + prize);
            int unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string uuid = (uuidForDummy == null) ? SystemInfo.deviceUniqueIdentifier : uuidForDummy;
            String hashToken = TokenManager.CreateToken(ServerEnv.clientId + uuid + countryCode + unixTimestamp, ServerEnv.secretKey);

            string platform = Platform.GetPlatformType().ToString();
            Dictionary<string, string> stringArgs = new Dictionary<string, string>();
            stringArgs.Add("client_id", ServerEnv.clientId);
            stringArgs.Add("uuid", uuid);
            stringArgs.Add("nickname", nickname);
            stringArgs.Add("country", countryCode);
            stringArgs.Add("gold", prize);
            stringArgs.Add("ts", unixTimestamp.ToString());
            stringArgs.Add("hash", hashToken);

            string uri = ServerEnv.host + _rankRecordingRequestURI;
            _httpRequest.Post("rankRecording", uri, stringArgs, 10);
        }

        public void RequireWeeklyRank(string nickname, string countryCode)
        {
            int unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            String hashToken = TokenManager.CreateToken(ServerEnv.clientId + SystemInfo.deviceUniqueIdentifier + countryCode + unixTimestamp.ToString(), ServerEnv.secretKey);

            string platform = Platform.GetPlatformType().ToString();
            Dictionary<string, string> stringArgs = new Dictionary<string, string>();
            stringArgs.Add("client_id", ServerEnv.clientId);
            stringArgs.Add("uuid", SystemInfo.deviceUniqueIdentifier);
            stringArgs.Add("nickname", nickname);
            stringArgs.Add("country", countryCode);
            stringArgs.Add("start", "1");
            stringArgs.Add("finish", "30");
            stringArgs.Add("ts", unixTimestamp.ToString());
            stringArgs.Add("hash", hashToken);

            Debug.Log("client_id: " + ServerEnv.clientId + "uuid: " + SystemInfo.deviceUniqueIdentifier + "nickname: " + nickname + "country: " + countryCode + "start: " + "1" + "finish: " + "3" + "ts: " + unixTimestamp.ToString() + "hash: " + hashToken);

            string uri = ServerEnv.host + _weekRankRequestURI;
            _httpRequest.Post("weeklyRank", uri, stringArgs, 10);
        }

        public void RequireCountryRank(string nickname, string countryCode)
        {
            int unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            String hashToken = TokenManager.CreateToken(ServerEnv.clientId + SystemInfo.deviceUniqueIdentifier + countryCode + unixTimestamp, ServerEnv.secretKey);

            string platform = Platform.GetPlatformType().ToString();
            Dictionary<string, string> stringArgs = new Dictionary<string, string>();
            stringArgs.Add("client_id", ServerEnv.clientId);
            stringArgs.Add("uuid", SystemInfo.deviceUniqueIdentifier);
            stringArgs.Add("nickname", nickname);
            stringArgs.Add("country", countryCode);
            stringArgs.Add("start", "1");
            stringArgs.Add("finish", "30");
            stringArgs.Add("ts", unixTimestamp.ToString());
            stringArgs.Add("hash", hashToken);

            string uri = ServerEnv.host + _countryRankRequestURI;
            _httpRequest.Post("CountryRank", uri, stringArgs, 10);
        }

        public void RequireHonorList(string nickname, string countryCode)
        {
            int unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            String hashToken = TokenManager.CreateToken(ServerEnv.clientId + SystemInfo.deviceUniqueIdentifier + countryCode + unixTimestamp, ServerEnv.secretKey);

            string platform = Platform.GetPlatformType().ToString();
            Dictionary<string, string> stringArgs = new Dictionary<string, string>();
            stringArgs.Add("client_id", ServerEnv.clientId);
            stringArgs.Add("uuid", SystemInfo.deviceUniqueIdentifier);
            stringArgs.Add("nickname", nickname);
            stringArgs.Add("country", countryCode);
            stringArgs.Add("ts", unixTimestamp.ToString());
            stringArgs.Add("hash", hashToken);

            string uri = ServerEnv.host + _honorListRequestURI;
            _httpRequest.Post("HonorRank", uri, stringArgs, 10);
        }

        private void LoadData()
        {
            _weeklyData.Clear();
            for (int i = 0; i < _weekBoardCountryCodes.Count; i++)
            {
                PrizeRankItemData data = new PrizeRankItemData();
                data.rank = i + 1;
                data.nickname = _weekBoardNickname[i];
                data.countryCode = _weekBoardCountryCodes[i];
                data.prize = ulong.Parse(_weekBoardGold[i]);
                _weeklyData.Add(data);
            }
            _weeklyPlayerData = new PrizeRankItemData();
            _weeklyPlayerData.rank = int.Parse(_weekPlayer[0]);
            _weeklyPlayerData.countryCode = _weekPlayer[1];
            _weeklyPlayerData.nickname = _weekPlayer[2];
            _weeklyPlayerData.prize = ulong.Parse(_weekPlayer[3]);

            _countryData.Clear();
            for (int i = 0; i < _lastweekBoardCountryCodes.Count; i++)
            {
                PrizeRankItemData data = new PrizeRankItemData();
                data.rank = i + 1;
                data.nickname = _lastweekBoardNickname[i];
                data.countryCode = _lastweekBoardCountryCodes[i];
                data.prize = ulong.Parse(_lastweekBoardGold[i]);
                _countryData.Add(data);
            }
            _countryPlayerData = new PrizeRankItemData();
            _countryPlayerData.rank = int.Parse(_lastweekPlayer[0]);
            _countryPlayerData.countryCode = _lastweekPlayer[1];
            _countryPlayerData.nickname = _lastweekPlayer[2];
            _countryPlayerData.prize = ulong.Parse(_lastweekPlayer[3]);

            _honorData.Clear();
            for (int i = 0; i < _nationBoardCountryCodes.Count; i++)
            {
                PrizeHonorItemData data = new PrizeHonorItemData();
                data.season = "2017년 " + i + "주차";
                data.nickname = _nationBoardNickname[i];
                data.countryCode = _nationBoardCountryCodes[i];
                data.prize = ulong.Parse(_nationBoardGold[i]);
                _honorData.Add(data);
            }
            //_nationPlayerData = new PrizeRankItemData();
            //_nationPlayerData.rank = int.Parse(_nationPlayer[0]);
            //_nationPlayerData.countryCode = _nationPlayer[1];
            //_nationPlayerData.nickname = _nationPlayer[2];
            //_nationPlayerData.prize = ulong.Parse(_nationPlayer[3]);
        }





        private void OnCompleted(string id, string wwwText)
        {
            if (id == "weeklyRank")
            {
                Debug.Log("Rank(weeklyRank) load Completed: " + wwwText);

                //{"status":"success","player":[{"rank":0, "nickname":junhee, "country":410, "gold":10000}, {"rank":0, "nickname":junhee, "country":410, "gold":10000}],"me":{"rank":-1,"nickname":"KimJunhee","country":"392","gold":600}}
                JsonData json = JsonMapper.ToObject(wwwText);
                _weeklyData.Clear();
                for (int i = 0; i < json["player"].Count; i++)
                {
                    PrizeRankItemData data = new PrizeRankItemData();
                    data.rank = int.Parse(json["player"][i]["rank"].ToString());
                    data.nickname = json["player"][i]["nickname"].ToString();
                    data.countryCode = json["player"][i]["country"].ToString();
                    data.prize = ulong.Parse(json["player"][i]["gold"].ToString());
                    if (data.prize <= 0) data.prize = 50;
                    _weeklyData.Add(data);
                }

                if (int.Parse(json["me"]["rank"].ToString()) == -1) _weeklyPlayerData = null;
                else
                {
                    _weeklyPlayerData = new PrizeRankItemData();
                    _weeklyPlayerData.rank = int.Parse(json["me"]["rank"].ToString());
                    _weeklyPlayerData.nickname = json["me"]["nickname"].ToString();
                    _weeklyPlayerData.countryCode = json["me"]["country"].ToString();
                    _weeklyPlayerData.prize = ulong.Parse(json["me"]["gold"].ToString());
                    if (_weeklyPlayerData.prize <= 0) _weeklyPlayerData.prize = 50;
                }

                if (WeeklyRankLoadCompleted != null) WeeklyRankLoadCompleted(wwwText);
            }
            else if (id == "CountryRank")
            {
                Debug.Log("Rank(CountryRank) load Completed: " + wwwText);

                JsonData json = JsonMapper.ToObject(wwwText);
                _countryData.Clear();
                for (int i = 0; i < json["player"].Count; i++)
                {
                    PrizeRankItemData data = new PrizeRankItemData();
                    data.rank = int.Parse(json["player"][i]["rank"].ToString());
                    data.nickname = json["player"][i]["nickname"].ToString();
                    data.countryCode = json["player"][i]["country"].ToString();
                    data.prize = ulong.Parse(json["player"][i]["gold"].ToString());
                    _countryData.Add(data);
                }

                if (int.Parse(json["me"]["rank"].ToString()) == -1) _countryPlayerData = null;
                else
                {
                    _countryPlayerData = new PrizeRankItemData();
                    _countryPlayerData.rank = int.Parse(json["me"]["rank"].ToString());
                    _countryPlayerData.nickname = json["me"]["nickname"].ToString();
                    _countryPlayerData.countryCode = json["me"]["country"].ToString();
                    _countryPlayerData.prize = ulong.Parse(json["me"]["gold"].ToString());
                }

                if (CountryRankLoadCompleted != null) CountryRankLoadCompleted(wwwText);
            }
            else if (id == "HonorRank")
            {
                Debug.Log("Rank(HonorRank) load Completed: " + wwwText);

                //{"status":"success","honor":[{"season":20171, "nickname":junhee, "country":410, "gold":10000}, {"season":20172, "nickname":junhee, "country":410, "gold":10000}]}

                _honorData.Clear();

                JsonData json = JsonMapper.ToObject(wwwText);
                for (int i = 0; i < json["honor"].Count; i++)
                {
                    string rawSeason = json["honor"][i]["season"].ToString();
                    string season = rawSeason.Substring(0, 4) + " Y\n" + rawSeason.Substring(4, rawSeason.Length - 4) + " W";

                    PrizeHonorItemData data = new PrizeHonorItemData();
                    data.season = season;
                    data.nickname = json["honor"][i]["nickname"].ToString();
                    data.countryCode = json["honor"][i]["country"].ToString();
                    data.prize = ulong.Parse(json["honor"][i]["gold"].ToString());
                    _honorData.Add(data);
                }

                //List<string> dummyHonorNames = new List<string>() { "BamBam", "김준희천재", "MOD", "JinJu" };
                //List<string> dummyHonorCountryCode = new List<string>() { "840", "410", "410", "410" };
                //List<ulong> dummyHonorPrize = new List<ulong>() { 645100, 711500, 1008150, 985050 };
                //for (int i = 3; i >= 0; i--)
                //{
                //    PrizeHonorItemData data = new PrizeHonorItemData();
                //    data.season = "2017 Y\n" + (7 + i) + " W";
                //    data.nickname = dummyHonorNames[i];
                //    data.countryCode = dummyHonorCountryCode[i];
                //    data.prize = dummyHonorPrize[i];
                //    _honorData.Add(data);
                //}

                if (HonorRankLoadCompleted != null) HonorRankLoadCompleted(wwwText);
            }
            else if(id == "rankRecording")
            {
                Debug.Log("Rank Recording load Completed: " + wwwText);
                if (RankRecordingCompleted != null) RankRecordingCompleted(wwwText);
            }
        }

        private void OnError(string id, string wwwText)
        {
            if (id == "weeklyRank")
            {
                Debug.Log("Rank(weeklyRank) load Error: " + wwwText);
            }
            else if (id == "CountryRank")
            {
                Debug.Log("Rank(CountryRank) load Error: " + wwwText);
            }
            else if (id == "HonorRank")
            {
                Debug.Log("Rank(HonorRank) load Error: " + wwwText);
            }
            else if (id == "rankRecording")
            {
                Debug.Log("Rank Recording load Error: " + wwwText);
            }
            if (Error != null) Error(wwwText);
        }

        //private void OnLoadCompleted()
        //{
        //    if (LoadCompleted != null) LoadCompleted("");
        //}

        //private void OnUserRankReceived()
        //{
        //    if (UserRankReceived != null) UserRankReceived(UnityEngine.Random.Range(100, 99999999), UnityEngine.Random.Range(-9999, 9999));
        //}






        //public void RequireUserRank()
        //{
        //    DOTween.Kill("RequireUserRank." + GetInstanceID());
        //    DOVirtual.DelayedCall(1.5f, OnUserRankReceived).SetId("RequireUserRank." + GetInstanceID());
        //}

        //public void Load(string nickname, string countryCode)
        //{
        //    LoadData();
        //    RequirePrizeRank(nickname, countryCode);
        //    DOTween.Kill("LoadPrizeRankData." + GetInstanceID());
        //    DOVirtual.DelayedCall(1.5f, OnLoadCompleted).SetId("LoadPrizeRankData." + GetInstanceID());
        //}

        public void StopLoad()
        {
            
            DOTween.Kill("LoadPrizeRankData." + GetInstanceID());
        }

        public List<PrizeRankItemData> GetWeeklyRankDatas()
        {
            return _weeklyData;
        }

        public List<PrizeRankItemData> GetCountryRankDatas()
        {
            return _countryData;
        }

        public List<PrizeHonorItemData> GetHonorListDatas()
        {
            return _honorData;
        }

        public PrizeRankItemData GetWeeklyPlayerData()
        {
            return _weeklyPlayerData;
        }

        public PrizeRankItemData GetCountryPlayerData()
        {
            return _countryPlayerData;
        }

        //public PrizeRankItemData GetPlayerNationData()
        //{
        //    return _nationPlayerData;
        //}
    }
}