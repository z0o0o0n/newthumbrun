﻿namespace Com.Mod.ThumbRun.PrizeRank.View
{
    using UnityEngine;
    using System.Collections;

    public class PrizeRankPopupButton : MonoBehaviour
    {
        [SerializeField]
        private PrizeRankPopup _popup;
        [SerializeField]
        private UILabel _remainingTimeLabel;

        void Start()
        {
            SeasonTimer.instance.Tick += OnTick;
        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            SeasonTimer.instance.Tick -= OnTick;
        }





        private void OnTick(string remainingTiemText)
        {
            _remainingTimeLabel.text = remainingTiemText;
        }




        public void OnButtonClick()
        {
            _popup.Open();
        }
    }
}