﻿using System.Collections.Generic;
using Com.Mod.ThumbRun.Common.View;

namespace Com.Mod.ThumbRun.PrizeRank.View
{
    using UnityEngine;
    using System.Collections;
    using Domain;
    using Com.Mod.ThumbRun.UI.Popup;
    using Application;

    public class PrizeRankPopup : BasicPopup
    {
		[SerializeField]
		private List<TapButton> _tapButtons;
		[SerializeField]
		private List<UIScrollView> _scrollViews;
        [SerializeField]
        private List<UIGrid> _grids;
        [SerializeField]
		private LoadingIcon _loadingIcon;
        [SerializeField]
        private PrizeRankService _prizeRankService;
        [SerializeField]
        private GameObject _prizeRankItemPrefab;
        [SerializeField]
        private PrizeRankItem _playerRankItemView;

        private List<GameObject> _weekRankItemGO = new List<GameObject>();
        private List<GameObject> _lastWeekRankItemGO = new List<GameObject>();
        private List<GameObject> _nationRankItemGO = new List<GameObject>();

        void Awake()
        {
            _prizeRankService.WeeklyRankLoadCompleted += OnWeeklyRankLoadCompleted;
            _prizeRankService.CountryRankLoadCompleted += OnCountryRankLoadCompleted;
            _prizeRankService.HonorRankLoadCompleted += OnHonorRankLoadCompleted;
            Close();
        }

        void Start()
        {
			for (int i = 0; i < _tapButtons.Count; i++) {
				_tapButtons [i].Click += OnTapButtonClick;
			}

			transform.localPosition = Vector3.zero;
        }

        void Update()
        {
        }

		void OnDestroy()
		{
			for (int i = 0; i < _tapButtons.Count; i++) {
				_tapButtons [i].Click -= OnTapButtonClick;
			}
            _prizeRankService.WeeklyRankLoadCompleted -= OnWeeklyRankLoadCompleted;
            _prizeRankService.CountryRankLoadCompleted -= OnCountryRankLoadCompleted;
            _prizeRankService.HonorRankLoadCompleted -= OnHonorRankLoadCompleted;
        }





        private void OnWeeklyRankLoadCompleted()
        {
            DestroyRankItems();

            List<PrizeRankItemData> weekData = _prizeRankService.GetWeeklyRankDatas();
            Debug.Log("week data count: " + weekData.Count);
            for (int i = 0; i < weekData.Count; i++)
            {
                GameObject instance = GameObject.Instantiate(_prizeRankItemPrefab);
                instance.transform.parent = _grids[0].transform;
                instance.transform.localScale = Vector2.one;
                instance.transform.localPosition = new Vector2(0f, i * -65f);

                PrizeRankItem prizeRankItem = instance.GetComponent<PrizeRankItem>();
                prizeRankItem.SetRankValue(weekData[i].rank, weekData[i].countryCode, weekData[i].nickname, weekData[i].prize);

                UIDragScrollView dragScrollView = instance.GetComponent<UIDragScrollView>();
                dragScrollView.scrollView = _scrollViews[0];

                _weekRankItemGO.Add(instance);
            }
            ChangeNormalScreen(0);
        }

        private void OnCountryRankLoadCompleted()
        {
            DestroyRankItems();

            List<PrizeRankItemData> lastWeekData = _prizeRankService.GetCountryRankDatas();
            for (int i = 0; i < lastWeekData.Count; i++)
            {
                GameObject instance = GameObject.Instantiate(_prizeRankItemPrefab);
                instance.transform.parent = _grids[1].transform;
                instance.transform.localScale = Vector2.one;
                instance.transform.localPosition = new Vector2(0f, i * -65f);

                PrizeRankItem prizeRankItem = instance.GetComponent<PrizeRankItem>();
                prizeRankItem.SetRankValue(lastWeekData[i].rank, lastWeekData[i].countryCode, lastWeekData[i].nickname, lastWeekData[i].prize);

                UIDragScrollView dragScrollView = instance.GetComponent<UIDragScrollView>();
                dragScrollView.scrollView = _scrollViews[1];

                _lastWeekRankItemGO.Add(instance);
            }
            ChangeNormalScreen(1);
        }

        private void OnHonorRankLoadCompleted()
        {
            DestroyRankItems();

            List<PrizeHonorItemData> honorListDatas = _prizeRankService.GetHonorListDatas();
            for (int i = 0; i < 5; i++)
            {
                GameObject instance = GameObject.Instantiate(_prizeRankItemPrefab);
                instance.transform.parent = _grids[2].transform;
                instance.transform.localScale = Vector2.one;
                instance.transform.localPosition = new Vector2(0f, i * -65f);

                PrizeRankItem prizeRankItem = instance.GetComponent<PrizeRankItem>();
                if (i < honorListDatas.Count)
                {
                    prizeRankItem.SetHonorValue(i, honorListDatas[i].season, honorListDatas[i].countryCode, honorListDatas[i].nickname, honorListDatas[i].prize);
                }
                else
                {
                    prizeRankItem.ChangeBgMode(i);
                }

                UIDragScrollView dragScrollView = instance.GetComponent<UIDragScrollView>();
                dragScrollView.scrollView = _scrollViews[2];

                _nationRankItemGO.Add(instance);
            }

            ChangeNormalScreen(2);
        }


        private void OnRankDataLoadComplete()
        {
            List<PrizeRankItemData> weekData = _prizeRankService.GetWeeklyRankDatas();
            for(int i = 0; i < weekData.Count; i++)
            {
                GameObject instance = GameObject.Instantiate(_prizeRankItemPrefab);
                instance.transform.parent = _grids[0].transform;
                instance.transform.localScale = Vector2.one;
                instance.transform.localPosition = new Vector2(0f, i * -65f);

                PrizeRankItem prizeRankItem = instance.GetComponent<PrizeRankItem>();
                prizeRankItem.SetRankValue(weekData[i].rank, weekData[i].countryCode, weekData[i].nickname, weekData[i].prize);

                UIDragScrollView dragScrollView = instance.GetComponent<UIDragScrollView>();
                dragScrollView.scrollView = _scrollViews[0];

                _weekRankItemGO.Add(instance);
            }

            List<PrizeRankItemData> lastWeekData = _prizeRankService.GetCountryRankDatas();
            for (int i = 0; i < lastWeekData.Count; i++)
            {
                GameObject instance = GameObject.Instantiate(_prizeRankItemPrefab);
                instance.transform.parent = _grids[1].transform;
                instance.transform.localScale = Vector2.one;
                instance.transform.localPosition = new Vector2(0f, i * -65f);

                PrizeRankItem prizeRankItem = instance.GetComponent<PrizeRankItem>();
                prizeRankItem.SetRankValue(lastWeekData[i].rank, lastWeekData[i].countryCode, lastWeekData[i].nickname, lastWeekData[i].prize);

                UIDragScrollView dragScrollView = instance.GetComponent<UIDragScrollView>();
                dragScrollView.scrollView = _scrollViews[1];

                _lastWeekRankItemGO.Add(instance);
            }

            List<PrizeHonorItemData> honorListDatas = _prizeRankService.GetHonorListDatas();
            for (int i = 0; i < honorListDatas.Count; i++)
            {
                GameObject instance = GameObject.Instantiate(_prizeRankItemPrefab);
                instance.transform.parent = _grids[2].transform;
                instance.transform.localScale = Vector2.one;
                instance.transform.localPosition = new Vector2(0f, i * -65f);

                PrizeRankItem prizeRankItem = instance.GetComponent<PrizeRankItem>();
                //prizeRankItem.SetValue(honorListDatas[i].season, honorListDatas[i].countryCode, honorListDatas[i].nickname, honorListDatas[i].prize);

                UIDragScrollView dragScrollView = instance.GetComponent<UIDragScrollView>();
                dragScrollView.scrollView = _scrollViews[2];

                _nationRankItemGO.Add(instance);
            }

            Init();
        }

        private void Init()
		{
			_loadingIcon.gameObject.SetActive (false);
			_loadingIcon.Stop ();
            ChangeNormalScreen(0);
		}

		private void OnTapButtonClick(int id)
		{
            if(id == 0)
            {
                _prizeRankService.LoadWeeklyRank();
            }
            else if(id == 1)
            {
                _prizeRankService.LoadCountryRank();
            }
            else if(id == 2)
            {
                _prizeRankService.LoadHonorRank();
            }
            ChangeLoadingScreen();
        }

        private void DestroyRankItems()
        {
            for (int i = 0; i < _weekRankItemGO.Count; i++)
            {
                Destroy(_weekRankItemGO[i]);
            }
            _weekRankItemGO.Clear();

            for (int i = 0; i < _lastWeekRankItemGO.Count; i++)
            {
                Destroy(_lastWeekRankItemGO[i]);
            }
            _lastWeekRankItemGO.Clear();

            for (int i = 0; i < _nationRankItemGO.Count; i++)
            {
                Destroy(_nationRankItemGO[i]);
            }
            _nationRankItemGO.Clear();
        }

        private void ChangeNormalScreen(int tapIndex)
        {
            _loadingIcon.gameObject.SetActive(false);
            _loadingIcon.Stop();

            _scrollViews[tapIndex].gameObject.SetActive(true);

            for (int i = 0; i < _tapButtons.Count; i++)
            {
                if (i == tapIndex) _tapButtons[i].Disable();
                else _tapButtons[i].Enable();
            }

            for (int j = 0; j < _scrollViews.Count; j++)
            {
                if (j == tapIndex) _scrollViews[j].gameObject.SetActive(true);
                else _scrollViews[j].gameObject.SetActive(false);
            }

            PrizeRankItemData playerData;
            if (tapIndex == 0) playerData = _prizeRankService.GetWeeklyPlayerData();
            else if (tapIndex == 1) playerData = _prizeRankService.GetCountryPlayerData();
            else playerData = null;
            if (playerData != null)
            {
                _playerRankItemView.SetRankValue(playerData.rank, playerData.countryCode, playerData.nickname, playerData.prize);
                _playerRankItemView.gameObject.SetActive(true);
            }
            else _playerRankItemView.gameObject.SetActive(false);
        }

        private void ChangeLoadingScreen()
        {
            _loadingIcon.gameObject.SetActive(true);
            _loadingIcon.Play();

            for (int i = 0; i < _tapButtons.Count; i++)
            {
                _tapButtons[i].Disable();
            }

            for (int j = 0; j < _scrollViews.Count; j++)
            {
                _scrollViews[j].gameObject.SetActive(false);
            }

            _playerRankItemView.gameObject.SetActive(false);
        }





        public void Open()
        {
            gameObject.SetActive(true);

            ChangeLoadingScreen();
            _prizeRankService.LoadWeeklyRank();

            base.Open();
        }

        public void Close()
        {
            DestroyRankItems();
            //_prizeRankService.StopRankDataLoad();
            gameObject.SetActive(false);

            base.Close();
        }
    }
}