﻿namespace Com.Mod.ThumbRun.PrizeRank.View
{
    using UnityEngine;
    using System.Collections;
    using System;

    public class PrizeRankItem : MonoBehaviour
    {
        [SerializeField]
        private bool _isPlayer = false;
        [SerializeField]
        private UISprite _bg;
        [SerializeField]
        private UISprite _badge;
        [SerializeField]
        private UILabel _rankLabel;
        [SerializeField]
        private UISprite _flag;
        [SerializeField]
        private UILabel _nicknameLabel;
        [SerializeField]
        private UISprite _goldIcon;
        [SerializeField]
        private UILabel _prizeLabel;

        void Start()
        {

        }

        void Update()
        {

        }



        public void SetRankValue(int rank, string countryCode, string nickname, ulong gold)
        {
            if(!_isPlayer)
            {
                if (rank % 2 == 0) _bg.gameObject.SetActive(false);

                if (rank <= 3)
                {
                    _badge.gameObject.SetActive(true);
                    if (rank == 1) _badge.spriteName = "GoldBadge";
                    else if (rank == 2) _badge.spriteName = "SilverBadge";
                    else if (rank == 3) _badge.spriteName = "BronzeBadge";
                }
                else
                {
                    _badge.gameObject.SetActive(false);
                }
            }

            _rankLabel.text = rank.ToString();
            _flag.spriteName = "Flag_" + countryCode;
            _nicknameLabel.text = nickname;

            if (gold <= 0) _prizeLabel.text = "0";
            else _prizeLabel.text = String.Format("{0:##,##}", gold);

            Replace();
        }

        public void SetHonorValue(int index, string yw, string countryCode, string nickname, ulong gold)
        {
            if (!_isPlayer)
            {
                if (index % 2 == 0) _bg.gameObject.SetActive(false);

                _badge.gameObject.SetActive(false);
            }

            _rankLabel.text = yw.ToString();
            _flag.spriteName = "Flag_" + countryCode;
            _nicknameLabel.text = nickname;
            if (gold <= 0) _prizeLabel.text = "0";
            else _prizeLabel.text = String.Format("{0:##,##}", gold);

            Replace();
        }

        private void Replace()
        {
            Vector2 goldIconPosition = _goldIcon.transform.localPosition;
            goldIconPosition.x = _prizeLabel.transform.localPosition.x - (_prizeLabel.width + 10);
            _goldIcon.transform.localPosition = goldIconPosition;
        }

        // 정보 없이 빈 배경만 노출함
        public void ChangeBgMode(int index)
        {
            if (index % 2 == 0) _bg.gameObject.SetActive(false);

            _badge.gameObject.SetActive(false);
            _rankLabel.gameObject.SetActive(false);
            _flag.gameObject.SetActive(false);
            _nicknameLabel.gameObject.SetActive(false);
            _goldIcon.gameObject.SetActive(false);
            _prizeLabel.gameObject.SetActive(false);
        }
    }
}