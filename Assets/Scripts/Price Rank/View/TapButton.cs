﻿namespace Com.Mod.ThumbRun.PrizeRank.View
{
	using UnityEngine;
	using System.Collections;

	public class TapButton : MonoBehaviour 
	{
		public delegate void TapButtonEventHandler (int id);
		public event TapButtonEventHandler Click;
		
		[SerializeField]
		private UISprite _bg;
		[SerializeField]
		private UILabel _label;
		[SerializeField]
		private int _id;
		[SerializeField]
		private bool _isEnabled = false;

		void Awake ()
		{
			Disable ();
		}

		void Start () 
		{

		}

		void Update () 
		{
		
		}

		void OnClick()
		{
			if (_isEnabled) {
				if (Click != null) {
					Click (_id);
				}
			}
		}



		public void Enable()
		{
			_isEnabled = true;
			_bg.alpha = 0.1f;
			_label.alpha = 0.2f;
		}

		public void Disable()
		{
			_isEnabled = false;
			_bg.alpha = 0.2f;
			_label.alpha = 1f;
		}
	}
}