﻿namespace Com.Mod.ThumbRun.PrizeRank.View
{
    using Application;
    using DG.Tweening;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class RankChangeDisplay : MonoBehaviour
    {
        [SerializeField]
        private UILabel _rankNameLabel;
        [SerializeField]
        private UILabel _currentRankLabel;
        [SerializeField]
        private UISprite _arrow;
        [SerializeField]
        private UILabel _rankOffsetLabel;
        [SerializeField]
        private UITexture _arrowBgEffect;
        [SerializeField]
        private UISprite _bg;
        [SerializeField]
        private PrizeRankService _prizeRankService;
        [SerializeField]
        private AnimationCurve _customEase;
        [SerializeField]
        private Color _increaseBgColor;
        [SerializeField]
        private Color _increaseArrowColor;
        [SerializeField]
        private Color _increaseArrowEffectColor;
        [SerializeField]
        private Color _decreaseBgColor;
        [SerializeField]
        private Color _decreaseArrowColor;
        [SerializeField]
        private Color _decreaseArrowEffectColor;

        private void Awake()
        {
            Reset();
        }

        void Start()
        {
        }

        void Update()
        {

        }

        private void OnDestroy()
        {
        }






        public void Show(int userPrizeRank, int fluctuationRange)
        {
            if (fluctuationRange == 0) return;

            _currentRankLabel.text = String.Format("{0:##,##}", userPrizeRank);
            _rankOffsetLabel.text = String.Format("{0:##,##}", fluctuationRange);

            _arrow.transform.localPosition = new Vector2(_rankOffsetLabel.transform.localPosition.x - _rankOffsetLabel.width - 5f, 0f);
            _currentRankLabel.transform.localPosition = new Vector2(_arrow.transform.localPosition.x - _arrow.width - 20f, -1f);
            _rankNameLabel.transform.localPosition = new Vector2(_currentRankLabel.transform.localPosition.x - _currentRankLabel.width - 10f, -1f);

            int bgWidth = (int)Mathf.Abs(_rankNameLabel.transform.localPosition.x - _rankNameLabel.width - 24);
            if (bgWidth < 240)
            {
                float leftMargin = (240 - bgWidth) / 2;
                Debug.Log("------------left margin: " + leftMargin);
                _rankOffsetLabel.transform.localPosition = new Vector2(_rankOffsetLabel.transform.localPosition.x - leftMargin, 0f);
                _arrow.transform.localPosition = new Vector2(_rankOffsetLabel.transform.localPosition.x - _rankOffsetLabel.width - 5f, 0f);
                _currentRankLabel.transform.localPosition = new Vector2(_arrow.transform.localPosition.x - _arrow.width - 20f, -1f);
                _rankNameLabel.transform.localPosition = new Vector2(_currentRankLabel.transform.localPosition.x - _currentRankLabel.width - 10f, -1f);
                bgWidth = 240;
            }
            _bg.width = bgWidth;

            _arrowBgEffect.transform.localPosition = new Vector2(-(bgWidth/2), 0f);

            ChnageArrowNum(fluctuationRange);
            ChangeColor(fluctuationRange);

            DOTween.Kill("RankChangeDisplay.MoveY." + GetInstanceID());
            transform.DOLocalMoveY(60f, 0.5f).SetId("RankChangeDisplay.MoveY." + GetInstanceID()).SetEase(_customEase);
        }





        private void ChnageArrowNum(int rankOffset)
        {
            int length = rankOffset.ToString().Length;
            if (0 < length && length <= 2) _arrow.spriteName = "RankChnageDisplayArrow0";
            else if (2 < length && length <= 3) _arrow.spriteName = "RankChnageDisplayArrow1";
            else _arrow.spriteName = "RankChnageDisplayArrow2";
        }

        private void ChangeColor(int fluctuationRange)
        {
            if(fluctuationRange > 0)
            {
                _bg.color = _increaseBgColor;
                _arrowBgEffect.color = _increaseArrowEffectColor;
                _arrowBgEffect.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
                _arrow.color = _increaseArrowColor;
                _arrow.transform.localScale = new Vector3(1f, 1f, 1f);
                _rankOffsetLabel.color = _increaseArrowColor;
            }
            else
            {
                _bg.color = _decreaseBgColor;
                _arrowBgEffect.color = _decreaseArrowEffectColor;
                _arrowBgEffect.transform.localEulerAngles = new Vector3(0f, 0f, 180f);
                _arrow.color = _decreaseArrowColor;
                _arrow.transform.localScale = new Vector3(1f, -1f, 1f);
                _rankOffsetLabel.color = _decreaseArrowColor;
            }
        }

        private void Hide()
        {
            DOTween.Kill("RankChangeDisplay.MoveY." + GetInstanceID());
            transform.DOLocalMoveY(-100f, 0.3f).SetId("RankChangeDisplay.MoveY." + GetInstanceID()).SetEase(Ease.OutBack);
            //gameObject.transform.localPosition = new Vector2(-32f, -100f);
        }

        private void Reset()
        {
            DOTween.Kill("RankChangeDisplay.MoveY." + GetInstanceID());
            transform.DOLocalMoveY(-100f, 0f).SetId("RankChangeDisplay.MoveY." + GetInstanceID());
        }
    }
}