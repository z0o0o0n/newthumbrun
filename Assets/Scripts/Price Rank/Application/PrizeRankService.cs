﻿namespace Com.Mod.ThumbRun.PrizeRank.Application
{
    using UnityEngine;
    using System.Collections;
    using Com.Mod.ThumbRun.PrizeRank.Infrastructure;
    using Com.Mod.ThumbRun.PrizeRank.Domain;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.User.Application;
    using LitJson;

    public class PrizeRankService : MonoBehaviour
    {
        public delegate void PrizeRankServiceHandler();
        public delegate void PrizeRankRecordingEvent(int currentRank, int offsetRank);
        public event PrizeRankServiceHandler Prepared;
        //public event PrizeRankServiceHandler LoadCompleted;
        //public event UserPrizeRankEvent UserRankReceived;
        public event PrizeRankServiceHandler WeeklyRankLoadCompleted;
        public event PrizeRankServiceHandler CountryRankLoadCompleted;
        public event PrizeRankServiceHandler HonorRankLoadCompleted;
        public event PrizeRankRecordingEvent RankRecordingCompleted;

        [SerializeField]
        private PrizeRankRepository _repository;
        [SerializeField]
        private UserService _userService;
        [SerializeField]
        private DummyRacePlayDataCreator _dummyPlayerDataCreator;
        private bool _isLoading = false;
        private bool _isPrepared = false;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }





        void Awake()
        {
            _repository.WeeklyRankLoadCompleted += OnWeeklyRankLoadCompleted;
            _repository.CountryRankLoadCompleted += OnCountryRankLoadCompleted;
            _repository.HonorRankLoadCompleted += OnHonorRankLoadCompleted;
            _repository.RankRecordingCompleted += OnRankRecordingCompleted;
            _userService.Prepared += OnPrepared;
        }

        void Start()
        {

        }

        void Update()
        {

        }

        void OnDestroy()
        {
            _repository.WeeklyRankLoadCompleted -= OnWeeklyRankLoadCompleted;
            _repository.CountryRankLoadCompleted -= OnCountryRankLoadCompleted;
            _repository.HonorRankLoadCompleted -= OnHonorRankLoadCompleted;
            _repository.RankRecordingCompleted -= OnRankRecordingCompleted;
            _userService.Prepared -= OnPrepared;
        }





        private void OnPrepared()
        {
            if (!_userService.isPrepared) return;

            _isPrepared = true;
            if (Prepared != null) Prepared();
        }

        private void OnWeeklyRankLoadCompleted(string wwwText)
        {
            _isLoading = false;

            if (WeeklyRankLoadCompleted != null) WeeklyRankLoadCompleted();
        }

        private void OnCountryRankLoadCompleted(string wwwText)
        {
            _isLoading = false;

            if (CountryRankLoadCompleted != null) CountryRankLoadCompleted();
        }

        private void OnHonorRankLoadCompleted(string wwwText)
        {
            _isLoading = false;

            if (HonorRankLoadCompleted != null) HonorRankLoadCompleted();
        }

        private void OnRankRecordingCompleted(string wwwText)
        {
            //{ "status":"success","player":{ "prev":{ "rank":-1,"gold":0},"next":{ "rank":1,"gold":200,"up":"new"} } }
            JsonData json = JsonMapper.ToObject(wwwText);
            int prizeRank = int.Parse(json["player"]["next"]["rank"].ToString());
            int prevPrizeRank = int.Parse(json["player"]["prev"]["rank"].ToString());
            int fluctuationRange = prevPrizeRank - prizeRank;
            if (prevPrizeRank == -1) fluctuationRange = prizeRank;
            if (RankRecordingCompleted != null) RankRecordingCompleted(prizeRank, fluctuationRange);
        }





        public void LoadWeeklyRank()
        {
            if (!_isLoading)
            {
                _isLoading = true;
                _repository.RequireWeeklyRank(_userService.GetNickname(), _userService.GetCountryCode());
            }
        }

        public void LoadCountryRank()
        {
            if (!_isLoading)
            {
                _isLoading = true;
                _repository.RequireCountryRank(_userService.GetNickname(), _userService.GetCountryCode());
            }
        }

        public void LoadHonorRank()
        {
            if (!_isLoading)
            {
                _isLoading = true;
                _repository.RequireHonorList(_userService.GetNickname(), _userService.GetCountryCode());
            }
        }

        //public void StopRankDataLoad()
        //{
        //    if (_isLoading)
        //    {
        //        _isLoading = false;
        //        _repository.StopLoad();
        //    }
        //}

        public void RequireRankRecording(string nickname, string countryCode, int prize)
        {
            if (RaceModeInfo.raceMode == RaceMode.mode.SINGLE)
            {
                RacePlayData data = _dummyPlayerDataCreator.GetDummyUser();
                string[] prizeList = { "100", "500", "1000", "10000", "100000"};
                string randomPrize = prizeList[(int)Random.Range(0, 4)];
                _repository.RequireRankRecording(data.nickname, data.countryCode, randomPrize, data.uuid);
            }
            else if (RaceModeInfo.raceMode == RaceMode.mode.MULTI)
            {
                _repository.RequireRankRecording(nickname, countryCode, prize.ToString());
            }
        }





        public List<PrizeRankItemData> GetWeeklyRankDatas()
        {
            return _repository.GetWeeklyRankDatas();
        }

        public List<PrizeRankItemData> GetCountryRankDatas()
        {
            return _repository.GetCountryRankDatas();
        }

        public List<PrizeHonorItemData> GetHonorListDatas()
        {
            return _repository.GetHonorListDatas();
        }

        public PrizeRankItemData GetWeeklyPlayerData()
        {
            return _repository.GetWeeklyPlayerData();
        }

        public PrizeRankItemData GetCountryPlayerData()
        {
            return _repository.GetCountryPlayerData();
        }

        //public PrizeRankItemData GetPlayerNationData()
        //{
        //    return _repository.GetPlayerNationData();
        //}
    }
}