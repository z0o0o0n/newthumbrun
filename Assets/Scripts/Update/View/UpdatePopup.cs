﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Com.Mod.ThumbRun.Setting.Application;
using Com.Mod.ThumbRun.UI.Popup;

public class UpdatePopup : MonoBehaviour
{
    [SerializeField]
    private UIBasicButton downloadButton;
    [SerializeField]
    private string _iOSDownloadURL;
    [SerializeField]
    private string _androidDownloadURL;
    private SettingService _settingService;

    void Awake()
    {
        _settingService = GameObject.FindGameObjectWithTag("Setting").GetComponent<SettingService>();
        _settingService.Prepared += OnPrepared;
        if (_settingService.isPrepared) OnPrepared();

        downloadButton.On_Click += OnDownloadButtonClick;
    }

    void Start()
    {
    }

    void Update()
    {
    }

    void OnDestroy()
    {
        downloadButton.On_Click -= OnDownloadButtonClick;
    }




    private void OnPrepared()
    {
        if (!_settingService.isPrepared) return;
        if (!_settingService.IsOldVersion())
        {
            GetComponent<BasicPopup>().Close();
        }
    }




    private void OnDownloadButtonClick()
    {
        string url;
#if UNITY_IPHONE
            url = _iOSDownloadURL;
#elif UNITY_ANDROID
        url = _androidDownloadURL;
#endif
        Application.OpenURL(url);
    }
}
