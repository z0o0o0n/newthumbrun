﻿namespace Com.Mod.ThumbRun.User.Domain
{
    using Io;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class LvExpTable : MonoBehaviour
    {
        private static LvExpTable _instance;

        [SerializeField]
        private CSVLoader _csvLoader;
        private CSVData _lvExpTable;
        private bool _isPreapred = false;

        public static LvExpTable instance
        {
            get { return _instance; }
        }


        
        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);

                LoadLvExpData();
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void Start()
        {

        }

        void Update()
        {

        }





        private void LoadLvExpData()
        {
            _csvLoader.Load("LvExp");
            _lvExpTable = CSVParser.Parse(4, _csvLoader.GetText());
            _isPreapred = true;
        }





        //public CSVData GetLvExpTable()
        //{
        //    return _lvExpTable;
        //}

        public int GetGoalExp(int level)
        {
            return int.Parse(_lvExpTable.GetField("goalExp", level.ToString()));
        }

        public int GetLevelUpReward(int level)
        {
            return int.Parse(_lvExpTable.GetField("reward", level.ToString()));
        }
    }
}