﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.User.Application;
using GameDataEditor;
using DG.Tweening;
using Com.Mod.ThumbRun.CharacterManagement.Application;

public class UserInfoDisplay : MonoBehaviour
{
    [SerializeField]
    private UISprite _countryFlag;
    [SerializeField]
    private UISprite _characterBg;
    [SerializeField]
    private UISprite _characterBgSide;
    [SerializeField]
    private UISprite _character;
    [SerializeField]
    private UILabel _nickname;
    [SerializeField]
    private UILabel _lvLabel;
    [SerializeField]
    private UIPanel _lvGaugeContainer;
    [SerializeField]
    private UISprite _lvGauge;
    [SerializeField]
    private ConditionDisplay _conditionDisplay;
    [SerializeField]
    private UIDynamicButton _characterButton;
    private UserService _userService;
    private CharacterManagementService _characterManagementService;

    void Awake()
    {
    }

    void Start ()
    {
        _userService = GameObject.FindGameObjectWithTag("User").GetComponent<UserService>();
        _userService.Prepared += OnPrepared;
        _userService.CharacterChanged += OnCharacterChanged;
        _userService.NicknameChanged += OnNicknameChanged;
        _userService.CountryCodeChanged += OnCountryCodeChanged;

        _characterManagementService = GameObject.FindGameObjectWithTag("CharacterManagement").GetComponent<CharacterManagementService>();
        _characterManagementService.Prepared += OnPrepared;
        _characterManagementService.Updated += OnCaracterManagementDataUpdated;
    }
	
	void Update ()
    {
	
	}

    void OnDestroy()
    {
        _userService.Prepared -= OnPrepared;
        _userService.CharacterChanged -= OnCharacterChanged;
        _userService.NicknameChanged -= OnNicknameChanged;
        _userService.CountryCodeChanged -= OnCountryCodeChanged;

        _characterManagementService.Prepared -= OnPrepared;
        _characterManagementService.Updated -= OnCaracterManagementDataUpdated;
    }



    private void OnPrepared()
    {
        if (!_userService.isPrepared) return;
        if (!_characterManagementService.isPrepared) return;

        _countryFlag.spriteName = "Flag_" + _userService.GetCountryCode();

        CharacterData characterData = CharacterDataManager.instance.GetCharacterDataByID(_userService.GetCharacterId());
        _characterBg.color = characterData.bgColor;
        _characterBgSide.color = characterData.bgColor;
        _character.spriteName = "CharacterProfileThumb_" + characterData.id;

        GDECharacterManagementItemData cmItem = _characterManagementService.GetItemById(_userService.GetCharacterId());
        _conditionDisplay.ChangeCondition(cmItem.condition);

        _nickname.text = _userService.GetNickname();

        _lvLabel.text = "Lv." + _userService.GetLevel();
        Debug.Log(">>>>>>>>>>>>>>>>> User Exp: " + _userService.GetExp());
        float currentExpRate = (float)_userService.GetExp() / (float)_userService.GetGoalExp();
        float targetWidth = (64f * currentExpRate) + 10f;
        _lvGauge.width = (int)targetWidth;
        _lvGaugeContainer.transform.DOLocalMoveX(_lvLabel.transform.localPosition.x + _lvLabel.width + 5, 0f);
    }

    private void OnCaracterManagementDataUpdated(int characterId)
    {
        if (!_userService.isPrepared) return;
        if(_userService.GetCharacterId() == characterId)
        {
            GDECharacterManagementItemData cmItem = _characterManagementService.GetItemById(_userService.GetCharacterId());
            _conditionDisplay.ChangeCondition(cmItem.condition);
        }
    }

    private void OnCharacterChanged()
    {
        CharacterData characterData = CharacterDataManager.instance.GetCharacterDataByID(_userService.GetCharacterId());
        _characterBg.color = characterData.bgColor;
        _characterBgSide.color = characterData.bgColor;
        _character.spriteName = "CharacterProfileThumb_" + characterData.id;

        GDECharacterManagementItemData cmItem = _characterManagementService.GetItemById(_userService.GetCharacterId());
        _conditionDisplay.ChangeCondition(cmItem.condition);
    }

    private void OnNicknameChanged()
    {
        _nickname.text = _userService.GetNickname();
    }

    private void OnCountryCodeChanged()
    {
        _countryFlag.spriteName = "Flag_" + _userService.GetCountryCode();
    }




    public void ShowNewIcon()
    {
        _characterButton.ShowNewIcon();
    }

    public void HideNewIcon()
    {
        _characterButton.HideNewIcon();
    }
}
