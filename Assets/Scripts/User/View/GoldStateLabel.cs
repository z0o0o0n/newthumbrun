﻿using UnityEngine;
using System.Collections;
using System;
using Junhee.Utils;
using DG.Tweening;

public class GoldStateLabel : MonoBehaviour 
{
    public UISprite sign;
    public UISprite goldIcon;
    public UILabel label;

	void Start () 
    {
        SetAlpha(0);
	}
	
	void Update () 
    {
	
	}

    private void Relocate()
    {
        Vector3 goldIconPostion = goldIcon.transform.localPosition;
        goldIconPostion.x = -(label.width + (goldIcon.width / 2) + 30);
        goldIcon.transform.localPosition = goldIconPostion;

        if(sign)
        {
            Vector3 signPosition = sign.transform.localPosition;
            signPosition.x = goldIcon.transform.localPosition.x - (goldIcon.width / 2) - (sign.width / 2) - 5;
            sign.transform.localPosition = signPosition;
        }
    }

    private void SetAlpha(float alpha)
    {
        if (sign) sign.alpha = alpha;
        goldIcon.alpha = alpha;
        label.alpha = alpha;
    }





    public void SetValue(int won)
    {
        if (won == 0) label.text = "0";
        else label.text = String.Format("{0:##,##}", won);
        Relocate();
    }

    public void SetColor(int hexValue)
    {
        Color32 color = ColorUtil.ConvertHexToColor(hexValue);
        sign.color = color;
        goldIcon.color = color;
        label.color = color;
    }



    public void Show()
    {
        float time = 0.2f;

        Vector3 pos = transform.localPosition;
        pos.y = -45;
        transform.localPosition = pos;

        transform.DOLocalMoveY(-90, time).SetId("moveY").SetEase(Ease.OutBack).OnComplete(Hide);
        if (sign) DOTween.To(() => sign.alpha, x => sign.alpha = x, 1, time).SetId("sign").SetEase(Ease.OutCubic);
        DOTween.To(() => goldIcon.alpha, x => goldIcon.alpha = x, 1, time).SetId("goldIcon").SetEase(Ease.OutCubic);
        DOTween.To(() => label.alpha, x => label.alpha = x, 1, time).SetId("label").SetEase(Ease.OutCubic);
    }


    private void Hide()
    {
        float time = 0.1f;
        float delay = 1f;

        transform.DOLocalMoveY(-45, time).SetId("moveY").SetEase(Ease.InBack).SetDelay(delay);
        if (sign) DOTween.To(() => sign.alpha, x => sign.alpha = x, 0, time).SetId("sign").SetEase(Ease.InCubic).SetDelay(delay);
        DOTween.To(() => goldIcon.alpha, x => goldIcon.alpha = x, 0, time).SetId("goldIcon").SetEase(Ease.InCubic).SetDelay(delay);
        DOTween.To(() => label.alpha, x => label.alpha = x, 0, time).SetId("label").SetEase(Ease.InCubic).SetDelay(delay);
    }



    public void Reset()
    {
        DOTween.Kill("moveY");
        DOTween.Kill("sign");
        DOTween.Kill("goldIcon");
        DOTween.Kill("label");
    }
}
