﻿namespace Com.Mod.ThumbRun.User.View
{
    using UnityEngine;
    using System.Collections;
    using Application;
    using GameDataEditor;
    using System;
    using DG.Tweening;

    public class WalletDisplay : MonoBehaviour
    {
        [SerializeField]
        private UserService _userService;
        [SerializeField]
        private UIBasicButton _goldButton;
        [SerializeField]
        private UISprite _goldIcon;
        [SerializeField]
        private UILabel _label;
        [SerializeField]
        private bool _isStoreButtonEnabled = true;
        private long _currentGoldAmount = 0;

        void Awake()
        {
            _userService.Prepared += OnUserServiePrepared;
            _userService.Saved += OnGoldSaved;
            _userService.Spent += OnGoldSpent;

            if (_isStoreButtonEnabled) EnableStoreButton();
            else DisableStoreButton();
        }

        void Start()
        {
            
        }

        void Update()
        {

        }

        void OnDestroy()
        {
            _userService.Prepared -= OnUserServiePrepared;
            _userService.Saved -= OnGoldSaved;
            _userService.Spent -= OnGoldSpent;

            DOTween.Kill("GoldUpdate." + GetInstanceID());
        }



        private void SetGold(long goldAmount)
        {
            //Debug.Log("========= +++++ currentGold: " + _currentGold + " / goldAmount: " + goldAmount);
            DOTween.Kill("GoldUpdate." + GetInstanceID());
            DOTween.To(() => _currentGoldAmount, x => _currentGoldAmount = x, goldAmount, 1f).SetId("GoldUpdate." + GetInstanceID()).OnUpdate(OnGoldUpdate);
        }

        private void OnGoldUpdate()
        {
            if (_currentGoldAmount == 0) _label.text = "0";
            else _label.text = String.Format("{0:##,##}", _currentGoldAmount);

            _goldButton.transform.DOLocalMoveX(-_label.width, 0);
            _goldIcon.transform.DOLocalMoveX(-_label.width - 8f, 0);
            //Debug.Log("+++++ width: " + _label.width);
        }



        private void OnUserServiePrepared()
        {
            _currentGoldAmount = _userService.GetGoldAmount();
            SetGold(_userService.GetGoldAmount());
        }

        private void OnGoldSaved(int savedGold, long goldAmount)
        {
            SetGold(goldAmount);
        }

        private void OnGoldSpent(int spentGold, long goldAmount)
        {
            SetGold(goldAmount);
        }



        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void EnableStoreButton()
        {
            _goldButton.gameObject.SetActive(true);
            _goldIcon.gameObject.SetActive(false);
        }

        public void DisableStoreButton()
        {
            _goldButton.gameObject.SetActive(false);
            _goldIcon.gameObject.SetActive(true);
        }
    }
}
