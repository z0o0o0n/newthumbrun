﻿using System.Collections.Generic;
using DG.Tweening;

namespace Com.Mod.ThumbRun.User.View
{
	using UnityEngine;
	using System.Collections;

	public class GoldBundle : MonoBehaviour
	{
        public delegate void GoldBundleEventHandler();
        public event GoldBundleEventHandler GoldAcquired;

		[SerializeField]
		private GameObject _throwingGoldPrefab;
		private List<ThrowingGold> _throwingGolds;
		private int _acquiredGoldCount = 0;

		void Start () 
		{
		
		}

		void Update () 
		{
		
		}

		public void Init(int gold, int depth)
		{
			_acquiredGoldCount = 0;
			int count = (int)(gold / 100);
            if (count > 100) count = 100;

            if (gold == 0) count = 0;
            if (0 < gold && gold < 500) count = 5;
            else if (500 <= gold && gold < 2000) count = 15;
            else if (2000 <= gold && gold < 5000) count = 20;
            else if (5000 <= gold && gold < 10000) count = 30;
            else if (10000 <= gold && gold < 20000) count = 40;
            else if (20000 <= gold && gold < 50000) count = 50;
            else if (50000 <= gold && gold < 100000) count = 60;
            else if (100000 <= gold && gold < 500000) count = 70;

            _throwingGolds = new List<ThrowingGold>();
			for(int i = 0; i < count; i++)
			{
				ThrowingGold throwingGold = GameObject.Instantiate(_throwingGoldPrefab).GetComponent<ThrowingGold>();
				throwingGold.index = i;
				throwingGold.transform.parent = transform;
				throwingGold.transform.localPosition = new Vector2(Random.Range(-100f, 100), Random.Range(-100f, 100));
				throwingGold.transform.localScale = Vector2.zero;
				throwingGold.SetDepth(depth);
				_throwingGolds.Add(throwingGold);

				float delay = Random.Range(0.1f, 0.5f);
				DOTween.Kill("ThrowGoldMotion." + i + GetInstanceID());
				throwingGold.transform.DOScale(1, 0.2f).SetDelay(delay).SetId("ThrowGoldMotion." + i + throwingGold.GetInstanceID()).SetEase(CustomEase.instance.OutBangElastic).OnComplete(()=>OnGoldThrown(throwingGold));

				throwingGold.PlayThrowAudio(delay);
				//DOVirtual.DelayedCall(0.45f, OnGoldThrown);
			}
		}

		#region Private Functions
		private void OnGoldThrown(ThrowingGold throwingGold)
		{
			AcquireGold(throwingGold);
		}

		private void AcquireGold(ThrowingGold throwingGold)
		{
			Vector2 goldDisplayGlobalPos = new Vector2(1.4f, 0.8f);
			DOTween.Kill("AcquireGoldMotion." + throwingGold.index + GetInstanceID());
			throwingGold.transform.DOMove(goldDisplayGlobalPos, 0.6f).SetId("AcquireGoldMotion." + throwingGold.index + throwingGold.GetInstanceID()).SetEase(Ease.InCubic).OnComplete(()=>OnGoldAcquired(throwingGold));
		}

		private void OnGoldAcquired(ThrowingGold throwingGold)
		{
			_acquiredGoldCount++;
			GameObject.Destroy(throwingGold.gameObject);

			if (_acquiredGoldCount >= _throwingGolds.Count) 
			{
				OnAllGoldAcquired();	
			}
		}

		private void OnAllGoldAcquired()
		{
			_throwingGolds.Clear();

            if (GoldAcquired != null) GoldAcquired();

            // Debug.Log ("+++++ On All Gold Acquired");
			Destroy(gameObject);
		}
		#endregion
	}
}