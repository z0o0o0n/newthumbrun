﻿namespace Com.Mod.ThumbRun.User.View
{
	using UnityEngine;
	using System.Collections;
	using DG.Tweening;
	using System;

	public class GoldDisplay : MonoBehaviour 
	{
	    public delegate void CompleteCallbak();

	    public UISprite goldIcon;
	    public UILabel label;
	    public GoldStateLabel saveStateLabel;
	    public GoldStateLabel spendStateLabel;

	    public AnimationCurve shakeAnimCurve;

	    void Awake()
	    {
	        saveStateLabel.gameObject.SetActive(false);
	        spendStateLabel.gameObject.SetActive(false);

	        SetGold(GameData.instance.GetConfigData().Gold);

	        Hide();
	    }

		void Start () 
	    {
		
		}
		
		void Update () 
	    {
		
		}

	    private void Relocate()
	    {
	        float goldIconTargetPosX = label.transform.localPosition.x - ((label.localSize.x) + (goldIcon.localSize.x / 2) + 10);
	        Vector3 goldIconPos = goldIcon.transform.localPosition;
	        goldIconPos.x = goldIconTargetPosX;
	        goldIcon.transform.localPosition = goldIconPos;
	    }





	    // Spend, Save
//	    public bool Spend(int goldValue)
//	    {
//	        if (GameData.instance.GetConfigData().Gold < goldValue) return false;
//
//	        saveStateLabel.gameObject.SetActive(false);
//	        saveStateLabel.Reset();
//
//	        spendStateLabel.gameObject.SetActive(true);
//	        spendStateLabel.SetValue(goldValue);
//	        spendStateLabel.Show();
//
//	        GameData.instance.GetConfigData().Gold -= goldValue;
//	        SetGold(GameData.instance.GetConfigData().Gold, true);
//
//	        return true;
//	    }
//
//	    public bool Save(int goldValue)
//	    {
//	        //Debug.LogWarning("Save - " + goldValue);
//	        spendStateLabel.gameObject.SetActive(false);
//	        spendStateLabel.Reset();
//
//	        saveStateLabel.gameObject.SetActive(true);
//	        saveStateLabel.SetValue(goldValue);
//	        saveStateLabel.Show();
//
//	        GameData.instance.GetConfigData().Gold += goldValue;
//	        SetGold(GameData.instance.GetConfigData().Gold, true);
//
//	        return true;
//	    }



	    // Set, Get Gold
	    public void SetGold(int gold, bool isShake = false)
	    {
	        if (gold == 0) label.text = "0";
	        else label.text = String.Format("{0:##,##}", gold);

	        if (isShake)
	        {
	            DOTween.Kill("LabelShake");
	            label.transform.DOLocalMoveY(-50, 0.1f).SetId("LabelShake").SetEase(Ease.OutCubic);
	            label.transform.DOLocalMoveY(-43, 0.3f).SetId("LabelShake").SetEase(shakeAnimCurve).SetDelay(0.1f);
	        }

	        Relocate();
	    }

	    public int GetGold()
	    {
	        return GameData.instance.GetConfigData().Gold;
	    }



	    // Show, Hide
	    private CompleteCallbak showCallback;
	    public void Show(float time = 0.0f, CompleteCallbak callback = null)
	    {
	        showCallback = callback;
	        goldIcon.transform.DOLocalMoveY(-42, time).SetEase(Ease.OutCubic);
	        label.transform.DOLocalMoveY(-43, time).SetEase(Ease.OutCubic).OnComplete(OnCompleteShow);
	    }

	    private void OnCompleteShow()
	    {
	        if (showCallback != null) showCallback();
	    }

	    private CompleteCallbak hideCallback;
	    public void Hide(float time = 0.0f, CompleteCallbak callback = null)
	    {
	        hideCallback = callback;
	        goldIcon.transform.DOLocalMoveY(50, time).SetEase(Ease.InCubic);
	        label.transform.DOLocalMoveY(50, time).SetEase(Ease.InCubic).OnComplete(OnCompleteHide);
	    }

	    private void OnCompleteHide()
	    {
	        if (hideCallback != null) hideCallback();
	    }
	}
}