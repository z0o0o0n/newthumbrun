﻿namespace Com.Mod.ThumbRun.User.View
{
	using UnityEngine;
	using System.Collections;
	using DG.Tweening;
	using System.Collections.Generic;
	using Com.Mod.ThumbRun.User.Application;

	public class GoldThrowEffect : MonoBehaviour 
	{
		[SerializeField]
		private UserService _saveGoldService;
		[SerializeField]
		private GameObject _goldBundle;
		[SerializeField]
		private GoldDisplay _goldDisplay;
		[SerializeField]
		private int _depth;

        void Awake ()
		{
			_saveGoldService.Saved += OnGoldSaved;
		}

		void Start () 
		{
		
		}

		void OnDestroy()
		{
			_saveGoldService.Saved -= OnGoldSaved;
		}



		#region Public Functions
		private void OnGoldSaved(int savedGold, long goldAmount)
		{
			//Debug.Log ("+++++ Throw Gold / saveGold: " + savedGold + " / goldAmount: " + goldAmount);
			Throw (savedGold);
		}

		public void Throw(int gold)
		{
			GoldBundle goldBundle = GameObject.Instantiate (_goldBundle).GetComponent<GoldBundle> ();
			goldBundle.transform.parent = transform;
			goldBundle.transform.localPosition = new Vector2(Random.Range(-100f, 100), Random.Range(-100f, 100));
			goldBundle.transform.localScale = Vector2.one;
            goldBundle.Init(gold, _depth);
		}
        #endregion




    }
}