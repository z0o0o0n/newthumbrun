﻿namespace Com.Mod.ThumbRun.User.Domain
{
    using DG.Tweening;
    using GameDataEditor;
    using UnityEngine;

    public class LvExpTemp : MonoBehaviour
    {
        public event ExpEvent Prepared;
        public delegate void ExpEvent();
        public delegate void LevelStatusEvent(int level);
        public delegate void ExpAcquirementEvent(int currentExp, int goalExp);
        public event LevelStatusEvent LevelIncreased;
        public event ExpAcquirementEvent ExpAcquiring;
        public event ExpAcquirementEvent ExpAcquired;

        private int _currentExp = 0;
        private float _acquirementDuration = 0.5f;
        private bool _isPrepared = false;
        private GDEUserData _rawData;

        void Start()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.User_User, out _rawData))
            {
                Debug.Log("+++++ User Data Prepared");

                _isPrepared = true;
                if (Prepared != null) Prepared();
            }
            else
            {
                Debug.LogError("Read Error User Data");
            }
        }

        void Update()
        {
        }




        public void AcquireExp1(int acuiredExp)
        {
            Debug.Log("경험치 획득 요청 - acuiredExp: " + acuiredExp);
            _currentExp = _rawData.level.exp;

            if (_rawData.level.exp + acuiredExp >= _rawData.level.goalExp)
            {
                int remainingGoalExp = _rawData.level.goalExp - _rawData.level.exp; // 목표경험치에 도달하기까지 남은 경험치
                int overExp = acuiredExp - remainingGoalExp; // 획득한 경험치 중 목표경험치를 넘어선 경험치
                float duraction = _acquirementDuration * remainingGoalExp / acuiredExp;

                DOTween.To(() => _currentExp, x => _currentExp = x, _rawData.level.goalExp, duraction).SetEase(Ease.Linear).OnUpdate(OnExpAcquiring).OnComplete(() => OnLevelUp(acuiredExp, overExp));
            }
            else
            {
                int targetExp = _rawData.level.exp + acuiredExp;
                DOTween.To(() => _currentExp, x => _currentExp = x, targetExp, _acquirementDuration).SetEase(Ease.Linear).OnUpdate(OnExpAcquiring).OnComplete(OnExpAcuired);
            }
        }

        private void OnLevelUp(int acuiredExp, int overExp)
        {
            _currentExp = 0;
            _rawData.level.goalExp = (int)(_rawData.level.goalExp * _rawData.level.goalExpIncreaseRate);

            float duraction = _acquirementDuration * overExp / acuiredExp;
            DOTween.To(() => _currentExp, x => _currentExp = x, overExp, duraction).SetEase(Ease.Linear).OnUpdate(OnExpAcquiring).OnComplete(OnExpAcuired);

            IncreaseLevel();
            Debug.Log("레벨업");
        }

        private void IncreaseLevel()
        {
            _rawData.level.lv += 1;
            if (LevelIncreased != null) LevelIncreased(_rawData.level.lv);
        }

        private void OnExpAcquiring()
        {
            //Debug.Log("경험치 획득 중... / _currentExp: " + _currentExp + " / _goalExp: " + _goalExp);
            if (ExpAcquiring != null) ExpAcquiring(_currentExp, _rawData.level.goalExp);
        }

        private void OnExpAcuired()
        {
            //Debug.Log("경험치 획득 완료" + _currentExp);
            _rawData.level.exp = _currentExp;
            if (ExpAcquired != null) ExpAcquired(_currentExp, _rawData.level.goalExp);
        }

        public GDELevelData GetLevelData()
        {
            return _rawData.level;
        }
    }
}