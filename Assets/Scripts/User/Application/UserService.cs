﻿namespace Com.Mod.ThumbRun.User.Application
{
    using UnityEngine;
    using System.Collections;
    using Domain;
    using GameDataEditor;

    public class UserService : MonoBehaviour 
	{
        public delegate void UserServiceEventHandler();
		public delegate void SaveGoldHandler(int gold, long goldAmount);
        public delegate void UserLevelEventHandler(GDELevelData levelData);
        public event UserServiceEventHandler Prepared;
        public event UserServiceEventHandler CharacterChanged;
        public event UserServiceEventHandler NicknameChanged;
        public event UserServiceEventHandler CountryCodeChanged;
        public event SaveGoldHandler Saved;
        public event SaveGoldHandler Spent;
        public event SaveGoldHandler ShortGold;

        [SerializeField]
		private User _user;

        private bool _isPrepared = false;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }
		
        void Awake ()
        {
            _user.Prepared += OnPrepared;
            _user.CharacterChanged += OnCharacterChanged;
            _user.NicknameChanged += OnNicknameChanged;
            _user.CountryCodeChanged += OnCountryCodeChanged;
            //_user.LevelUp += OnLevelUp;
        }

		void Start ()
		{

		}

        void Update()
        {
            if(Input.GetKeyUp(KeyCode.Alpha0))
            {
                _user.SaveGold(10000);
            }            
        }

        void OnDestroy()
        {
            _user.Prepared -= OnPrepared;
            _user.CharacterChanged -= OnCharacterChanged;
            _user.NicknameChanged -= OnNicknameChanged;
            _user.CountryCodeChanged -= OnCountryCodeChanged;
            //_user.LevelUp -= OnLevelUp;
        }



        private void OnPrepared()
        {
            _isPrepared = true;
            SetGoalExp(LvExpTable.instance.GetGoalExp(GetLevel()));
            if (Prepared != null) Prepared();
        }

        private void OnCharacterChanged()
        {
            if (CharacterChanged != null) CharacterChanged();
        }

        private void OnNicknameChanged()
        {
            if (NicknameChanged != null) NicknameChanged();
        }

        private void OnCountryCodeChanged()
        {
            if (CountryCodeChanged != null) CountryCodeChanged();
        }
        //private void OnLevelUp(int lv)
        //{
        //    if (LevelUp != null) LevelUp(_user.GetLvData());
        //}



        public long GetGoldAmount()
        {
            return _user.GetGoldAmount();
        }

        public string GetNickname()
        {
            return _user.GetNickname();
        }

        public void SetNickname(string nickname)
        {
            _user.SetNickname(nickname);
        }

        public int GetCharacterId()
        {
            return _user.GetCharacterId();
        }

        public void SetCharacterId(int characterId)
        {
            _user.SetCharacterId(characterId);
        }

        public string GetCountryCode()
        {
            return _user.GetCountryCode();
        }

        public void SetCountryCode(string countryCode)
        {
            _user.SetCountryCode(countryCode);
        }

        public void Save(int savedGold)
		{
            Debug.Log("+++++ save: " + savedGold);
            _user.SaveGold(savedGold);
			if (Saved != null) Saved (savedGold, _user.GetGoldAmount());
		}

        public bool Spend(int spentGold)
        {
            Debug.Log("+++++ spend: " + spentGold);
            if(_user.GetGoldAmount() < spentGold)
            {
                if (ShortGold != null) ShortGold(spentGold, _user.GetGoldAmount());
                return false;
            }

            _user.SpendGold(spentGold);
            if (Spent != null) Spent(spentGold, _user.GetGoldAmount());
            return true;
        }

        //public GDELevelData GetLevelData()
        //{
        //    return _user.GetLevelData();
        //}




        public int GetLevel()
        {
            return _user.GetLevel();
        }

        public void AddLevel(int lv)
        {
            _user.AddLevel(lv);
        }

        public int GetExp()
        {
            return _user.GetExp();
        }

        public void AddExp(int exp)
        {
            _user.AddExp(exp);
        }

        public void ResetExp()
        {
            _user.ResetExp();
        }

        public int GetGoalExp()
        {
            return _user.GetGoalExp();
        }

        public void SetGoalExp(int goalExp)
        {
            _user.SetGoalExp(goalExp);
        }

        //public GDELevelData AddExp(int exp)
        //{
        //    return _user.AddExp(exp);
        //}
    }
}