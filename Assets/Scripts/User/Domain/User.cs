﻿namespace Com.Mod.ThumbRun.User.Domain
{
    using UnityEngine;
    using System.Collections;
    using GameDataEditor;
    using TapjoyUnity;

    public class User : MonoBehaviour 
	{
        public delegate void UserEventHandler();
        public delegate void UserLevelUpEventHandler(int lv);
        public event UserEventHandler Prepared;
        public event UserEventHandler CharacterChanged;
        public event UserEventHandler NicknameChanged;
        public event UserEventHandler CountryCodeChanged;
        public event UserLevelUpEventHandler LevelUp;

        [SerializeField]
		private GoldWallet _goldWallet;
        private bool _isPrepared = false;
        private GDEUserData _rawData;

        void Start () 
		{
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.User_User, out _rawData))
            {
                //Debug.Log("+++++ User Data Prepared");

                _isPrepared = true;

                if(!_rawData.isNewGoldType)
                {
                    // 기존 골드 타입을 새로운 골드 타입으로 이관
                    _rawData.goldString = _rawData.gold.ToString();
                    _rawData.isNewGoldType = true;
                    Debug.Log("기존 보유 골드를 새로운 골드 타입으로 이관함");
                }

                if (Prepared != null) Prepared();
                //_rawData.level.exp = 130506080;
                //_rawData.level.exp = 100;
                //_rawData.goldString = "20000000000";
                //_rawData.level.lv = 20;
                //_rawData.Reset_gold();
                //_rawData.ResetAll();
            }
            else
            {
                Debug.LogError("Read Error User Data");
            }
        }

		public long SaveGold(long gold)
		{
            _rawData.goldString = (long.Parse(_rawData.goldString) + (long) gold).ToString();
			return long.Parse(_rawData.goldString);
		}

        public long SpendGold(long gold)
        {
            _rawData.goldString = (long.Parse(_rawData.goldString) - (long)gold).ToString();
            return long.Parse(_rawData.goldString);
        }

		public long GetGoldAmount()
		{
            return long.Parse(_rawData.goldString);
        }

        public string GetNickname()
        {
            return _rawData.nickname;
        }

		public void SetNickname(string nickname)
		{
			_rawData.nickname = nickname;
            if (NicknameChanged != null) NicknameChanged();
        }

        public string GetCountryCode()
        {
            return _rawData.countryCode;
        }

		public void SetCountryCode(string code)
		{
			_rawData.countryCode = code;
            if (CountryCodeChanged != null) CountryCodeChanged();
		}

        public int GetCharacterId()
        {
            return _rawData.characterId;
        }

        public void SetCharacterId(int characterId)
        {
            _rawData.characterId = characterId;
            if (CharacterChanged != null) CharacterChanged();
        }

		public bool isPrepared
		{
			get{ return _isPrepared; }
		}

        //public int GetLv()
        //{
        //    return _rawData.level.lv;
        //}

        //public GDELevelData AddExp(int exp)
        //{
        //    _rawData.level.exp += exp;
        //    if(_rawData.level.exp >= _rawData.level.goalExp)
        //    {
        //        _rawData.level.exp = _rawData.level.exp - _rawData.level.goalExp;
        //        _rawData.level.lv += 1;
        //        _rawData.level.goalExp = (int)(_rawData.level.goalExp * _rawData.level.goalExpIncreaseRate);
        //        if (LevelUp != null) LevelUp(_rawData.level.lv);
        //    }

        //    Debug.Log("+++++ added exp: " + exp + " / current exp: " + _rawData.level.exp + " / lv: " + _rawData.level.lv + " / goalExp: " + _rawData.level.goalExp);

        //    return _rawData.level;
        //}


        public int GetLevel()
        {
            return _rawData.level.lv;
        }

        public void AddLevel(int lv)
        {
            _rawData.level.lv += lv;
            if (_rawData.level.lv > 300) _rawData.level.lv = 300;
        }

        public int GetExp()
        {
            return _rawData.level.exp;
        }

        public void AddExp(int exp)
        {
            _rawData.level.exp += exp;
        }

        public void ResetExp()
        {
            _rawData.level.exp = 0;
        }

        public int GetGoalExp()
        {
            return _rawData.level.goalExp;
        }

        public void SetGoalExp(int goalExp)
        {
            _rawData.level.goalExp = goalExp;
        }




        //public GDELevelData GetLevelData()
        //{
        //    //Debug.Log("lv: " + _rawData.level.lv + " / exp: " + _rawData.level.exp + " / goalExp: " + _rawData.level.goalExp);
        //    return _rawData.level;
        //}
    }
}