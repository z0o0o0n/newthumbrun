﻿namespace Com.Mod.ThumbRun.User.Domain
{
    using Application;
    using DG.Tweening;
    using GameDataEditor;
    using Io;
    using UnityEngine;

    public class LvExp : MonoBehaviour
    {
        public event ExpEvent Prepared;
        public delegate void ExpEvent();
        public delegate void LevelStatusEvent(int level);
        public delegate void ExpAcquirementEvent(int currentExp, int goalExp);
        public event LevelStatusEvent LevelIncreased;
        public event ExpAcquirementEvent ExpAcquiring;
        public event ExpAcquirementEvent ExpAcquired;

        private static GameObject _instance;

        [SerializeField]
        private CSVLoader _csvLoader;
        private UserService _userService;
        private int _currentExp = 0;
        private int _goalExp = 0;
        private bool _isPrepared = false;
        private CSVData _lvExpTable;
        private bool _lvExpTablePrepared = false;

        private void Awake()
        {
            if(_instance == null)
            {
                _instance = gameObject;
                DontDestroyOnLoad(gameObject);

                _userService = GameObject.FindGameObjectWithTag("User").GetComponent<UserService>();
                _userService.Prepared += OnPrepared;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void Start()
        {
            LoadLvExpData();
        }

        void Update()
        {
        }

        private void OnDestroy()
        {
            _userService.Prepared -= OnPrepared;
        }





        private void LoadLvExpData()
        {
            _csvLoader.Load("LvExp");
            _lvExpTable = CSVParser.Parse(4, _csvLoader.GetText());
            _lvExpTablePrepared = true;
        }





        private void OnPrepared()
        {
            if (!_userService.isPrepared) return;
            if (!_lvExpTablePrepared) return;

            _isPrepared = true;
            if (Prepared != null) Prepared();
        }





        public void AcquireExp(int acuiredExp)
        {
            //Debug.Log("경험치 획득 요청 - acuiredExp: " + acuiredExp);
            ////GDELevelData levelData = _userService.GetLevelData();
            //_currentExp = _userService.GetExp();
            //_goalExp = int.Parse(_lvExpTable.GetField("goalExp", _userService.GetLevel().ToString()));
            //_userService.SetGoalExp(_goalExp);

            //if (_currentExp + acuiredExp >= _goalExp)
            //{
            //    // LvUp
            //    int remainingGoalExp = _goalExp - _currentExp; // 목표경험치에 도달하기까지 남은 경험치
            //    int overExp = acuiredExp - remainingGoalExp; // 획득한 경험치 중 목표경험치를 넘어선 경험치

            //    _currentExp = overExp;
            //    _goalExp = int.Parse(_lvExpTable.GetField("goalExp", levelData.lv.ToString()));
            //    _userService.GetLevelData().lv += 1;
            //}
            //else
            //{
            //    _currentExp += acuiredExp;
            //}

            //_userService.GetLevelData().exp = _currentExp;
            //_userService.GetLevelData().goalExp = _goalExp;
            
            //Debug.Log("^^^^^^^^^ AcquireExp1 / _rawData.level.exp: " + _userService.GetLevelData().exp + " / _rawData.level.goalExp: " + _userService.GetLevelData().goalExp);
        }

        //private void OnLevelUp(int acuiredExp, int overExp)
        //{
        //    //_currentExp = 0;
        //    //_rawData.level.goalExp = (int)(_rawData.level.goalExp * _rawData.level.goalExpIncreaseRate);

        //    float duraction = _acquirementDuration * overExp / acuiredExp;
        //    DOTween.To(() => _currentExp, x => _currentExp = x, overExp, duraction).SetEase(Ease.Linear).OnUpdate(OnExpAcquiring).OnComplete(OnExpAcuired);

        //    IncreaseLevel();
        //    Debug.Log("레벨업");
        //}

        //private void IncreaseLevel()
        //{
        //    _rawData.level.lv += 1;
        //    if (LevelIncreased != null) LevelIncreased(_rawData.level.lv);
        //}

        //private void OnExpAcquiring()
        //{
        //    //Debug.Log("경험치 획득 중... / _currentExp: " + _currentExp + " / _goalExp: " + _goalExp);
        //    if (ExpAcquiring != null) ExpAcquiring(_currentExp, _rawData.level.goalExp);
        //}

        //private void OnExpAcuired()
        //{
        //    //Debug.Log("경험치 획득 완료" + _currentExp);
        //    _rawData.level.exp = _currentExp;
        //    if (ExpAcquired != null) ExpAcquired(_currentExp, _rawData.level.goalExp);
        //}

        //public GDELevelData GetLevelData()
        //{
        //    return _rawData.level;
        //}
    }
}