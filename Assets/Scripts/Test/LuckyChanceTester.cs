﻿namespace Com.Mod.ThumbRun.Test
{
	using Com.Mod.ThumbRun.Control;
	using UnityEngine;
	using System.Collections;

	public class LuckyChanceTester : MonoBehaviour
	{
		public LuckyChance _luckyChance;

		void Start ()
		{
			
		}

		void Update ()
		{
			if (Input.GetKeyUp (KeyCode.Alpha1)) 
			{
				_luckyChance.data.AddRaceOutcome(true);
			} 
			else if (Input.GetKeyUp (KeyCode.Alpha2)) 
			{
				_luckyChance.data.AddRaceOutcome(false);
			}
			else if (Input.GetKeyUp (KeyCode.Alpha3)) 
			{
				_luckyChance.ResetLuckyChance();
			}
		}
	}
}