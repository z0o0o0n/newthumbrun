﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Event;
using DG.Tweening;

namespace Com.Mod.ThumbRun.Test
{
    public class WJBoostTester : MonoBehaviour
    {
        public event PlayerEvent.PlayerEventHandler WallTouchBegin;
        public event PlayerEvent.PlayerEventHandler WallTouchEnd;
        public event PlayerEvent.PlayerJumpEventHandler JumpBegin;

        private bool _isJumping = false;
        private float _jumpTime = 1.5f; // 벽점프 뒤 다시 벽에 닿기까지의 시간

        void Start()
        {
        }

        void Update()
        {
            if(Input.GetKeyUp(KeyCode.Space))
            {
                Jump();
            }
        }



        #region Private Functions
        public void Jump()
        {
            if (!_isJumping)
            {
                _isJumping = true;

                if (JumpBegin != null) JumpBegin(0);
                if (WallTouchEnd != null) WallTouchEnd();

                DOTween.Kill("JumpTimeTween." + GetInstanceID());
                DOTween.To(() => _jumpTime, x => _jumpTime = x, _jumpTime, _jumpTime).SetId("JumpTimeTween." + GetInstanceID()).OnComplete(OnJumpTimeComplete);
            }
        }

        private void OnJumpTimeComplete()
        {
            _isJumping = false;
            if (WallTouchBegin != null) WallTouchBegin();
        }
        #endregion
    }
}
