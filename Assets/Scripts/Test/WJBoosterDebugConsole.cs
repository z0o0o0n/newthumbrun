﻿using UnityEngine;
using System.Collections;
using Com.Mod.ThumbRun.Controller;
using UnityEngine.UI;
using DG.Tweening;

namespace Com.Mod.ThumbRun.Test
{
    public class WJBoosterDebugConsole : MonoBehaviour
    {
        public WJBooster wjBooster;
        public PlayerManager playerManager;
        public Text touchWallConsole;
        public Image reactionTimeLimitGauge;
        public Text cumulativeBoostQtyConsole;
        public Text cumulativeMSIncRateConsole;

        [Header("Tester")]
        public WJBoostTester wjBoostTester;

        void Awake ()
        {
            if (wjBoostTester)
            {
                wjBoostTester.WallTouchBegin += OnWallTouchBegin;
                wjBoostTester.WallTouchEnd += OnWallTouchEnd;
            }
            else
            {
                playerManager.WallTouchBegin += OnWallTouchBegin;
                playerManager.WallTouchEnd += OnWallTouchEnd;
            }

            wjBooster.ReactionTimeStart += OnReactionTimeStart;
            wjBooster.ReactionTimeProgressing += OnReactionTimeProgressing;
            wjBooster.ReactionTimeOut += OnReactionTimeOut;
            wjBooster.BoostAccumulated += OnBoostAccumulated;
        }

        void Start()
        {
            RectTransform rect = GetComponent<RectTransform>();
            rect.DOLocalMoveX(-Screen.width / 2f, 0f);
            rect.DOLocalMoveY(Screen.height / 2f, 0f);
        }

        void Update()
        {
            UpdateConsole();
        }

        void OnDestroy()
        {
            if (wjBoostTester)
            {
                wjBoostTester.WallTouchBegin -= OnWallTouchBegin;
                wjBoostTester.WallTouchEnd -= OnWallTouchEnd;
            }
            else
            {
                playerManager.WallTouchBegin -= OnWallTouchBegin;
                playerManager.WallTouchEnd -= OnWallTouchEnd;
            }

            wjBooster.ReactionTimeStart -= OnReactionTimeStart;
            wjBooster.ReactionTimeProgressing -= OnReactionTimeProgressing;
            wjBooster.ReactionTimeOut -= OnReactionTimeOut;
            wjBooster.BoostAccumulated -= OnBoostAccumulated;
        }



        #region Private Functions
        private void OnWallTouchBegin()
        {
            touchWallConsole.text = "TOUCH WALL";
            touchWallConsole.color = Color.green;
        }

        private void OnWallTouchEnd()
        {
            touchWallConsole.text = "JUMP";
            touchWallConsole.color = Color.red;
        }

        private void OnReactionTimeStart()
        {
            reactionTimeLimitGauge.fillAmount = 1;
            reactionTimeLimitGauge.DOFade(1f, 0);
        }

        private void OnReactionTimeProgressing(float progress)
        {
            reactionTimeLimitGauge.fillAmount = progress;
        }

        private void OnReactionTimeOut()
        {
            reactionTimeLimitGauge.fillAmount = 1;
            reactionTimeLimitGauge.DOFade(0.3f, 0);
        }

        private void OnBoostAccumulated(int qty, float rate)
        {
        }
        #endregion



        #region Methods
        private void UpdateConsole()
        {
            cumulativeBoostQtyConsole.text = "x" + wjBooster.cumulativeBoostQty.ToString();
            cumulativeMSIncRateConsole.text = "+" + wjBooster.cmlWJMSIncRate.ToString() + "%";
        }
        #endregion
    }
}