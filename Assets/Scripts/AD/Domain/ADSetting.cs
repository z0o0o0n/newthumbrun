﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ADSetting : MonoBehaviour
{
    public ADBrandName.name brandName;
    public bool isShow;
    public int rate;
    
    public ADSetting(ADBrandName.name brandName, bool isShow, int rate)
    {
        this.brandName = brandName;
        this.isShow = isShow;
        this.rate = rate;
    }
}
