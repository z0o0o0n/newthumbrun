﻿using Com.Mod.ThumbRun.Analytics.Application;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class NewADManager : MonoBehaviour
{
    public delegate void ADManagerEventHandler(string key);
    public event ADManagerEventHandler Success;
    public event ADManagerEventHandler Failed;

    [Header("AD Colony")]
    [SerializeField]
    private string _iOSAppId;
    [SerializeField]
    private string _iOSAdZoneId;
    [SerializeField]
    private string _androidAppId;
    [SerializeField]
    private string _androidAdZoneId;
    private List<ADSetting> _adSettings;
    private List<int> _adRateList;
    private ADBrandName.name _seletedAd = ADBrandName.name.NONE;
    private string _key;
    private AnalyticsService _analyticsService;
    
    private static NewADManager _instance = null;

    public static NewADManager instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if(_instance == null)
        {
            _instance = gameObject.GetComponent<NewADManager>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start ()
    {
        // Setup AD Colonay
        AdColony.OnVideoFinished = OnAdColonyVideoFinished;

        string appId;
        string adZoneId;
        
#if UNITY_IPHONE
        appId = _iOSAppId;
        adZoneId = _iOSAdZoneId;
#elif UNITY_ANDROID
        appId = _androidAppId;
        adZoneId = _androidAdZoneId;
#endif

        AdColony.Configure
        (
          "1.0",
          appId,
          adZoneId
        );
    }
	
	void Update ()
    {
	}





    public bool IsReady()
    {
        if (_seletedAd == ADBrandName.name.NONE) _seletedAd = GetRandomAdBrand();

        if (_seletedAd == ADBrandName.name.UNITY_AD)
        {
            if (CheckUnityAd())
            {
                return true;
            }
            else
            {
                return CheckAdColony();
            }
        }
        else if (_seletedAd == ADBrandName.name.AD_COLONY)
        {
            if(CheckAdColony())
            {
                return true;
            }
            else
            {
                return CheckUnityAd();
            }
        }

        return false;
    }

    private bool CheckUnityAd()
    {
        bool IsUnityAdReady = Advertisement.IsReady("rewardedVideo");
        if (IsUnityAdReady) return true;
        return false;
    }

    private bool CheckAdColony()
    {
        string zoneId;
#if UNITY_IPHONE
        zoneId = _iOSAdZoneId;
#elif UNITY_ANDROID
        zoneId = _androidAdZoneId;
#endif
        bool IsColonyAdReady = AdColony.IsVideoAvailable(zoneId);
        if (IsColonyAdReady) return true;
        return false;
    }


    public bool PlayAD(string key)
    {
        _key = key;
        bool result = false;

        if(_seletedAd == ADBrandName.name.NONE) _seletedAd = GetRandomAdBrand();

        if (_seletedAd == ADBrandName.name.UNITY_AD)
        {
            if(PlayUnityAd())
            {
                return true;
            }
            else
            {
                return PlayAdColony();
            }
        }
        else if (_seletedAd == ADBrandName.name.AD_COLONY)
        {
            if(PlayAdColony())
            {
                return true;
            }
            else
            {
                return PlayUnityAd();
            }
        }
        return false;
    }

    private bool PlayUnityAd()
    {
        bool IsUnityAdReady = Advertisement.IsReady("rewardedVideo");
        if (IsUnityAdReady)
        {
            var options = new ShowOptions { resultCallback = OnUnityAdVideoFinished };
            Advertisement.Show("rewardedVideo", options);
            return true;
        }
        return false;
    }

    private void OnUnityAdVideoFinished(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                _seletedAd = ADBrandName.name.NONE;

                _analyticsService = GameObject.FindGameObjectWithTag("Analytics").GetComponent<AnalyticsService>();
                _analyticsService.CountWatchedAd();

                if (Success != null) Success(_key);
                break;
            case ShowResult.Skipped:
                _seletedAd = ADBrandName.name.NONE;
                if (Failed != null) Failed(_key);
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                _seletedAd = ADBrandName.name.NONE;
                if (Failed != null) Failed(_key);
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    private bool PlayAdColony()
    {
        string zoneId;
#if UNITY_IPHONE
            zoneId = _iOSAdZoneId;
#elif UNITY_ANDROID
        zoneId = _androidAdZoneId;
#endif
        bool IsColonyAdReady = AdColony.IsVideoAvailable(zoneId);
        if (IsColonyAdReady)
        {
            AdColony.ShowVideoAd(zoneId);
            return true;
        }
        return false;
    }

    private void OnAdColonyVideoFinished(bool ad_was_shown)
    {
        _seletedAd = ADBrandName.name.NONE;

        _analyticsService = GameObject.FindGameObjectWithTag("Analytics").GetComponent<AnalyticsService>();
        _analyticsService.CountWatchedAd();

        if (Success != null) Success(_key);
        Debug.Log("On Video Finished : " + ad_was_shown);
    }





    public void SetAdSettings(List<ADSetting> adSettings)
    {
        _adSettings = adSettings;

        _adRateList = new List<int>();

        //Debug.Log(">>>> _adSettings.Count: "  + _adSettings.Count);
        string log = "";
        for(int i = 0; i < _adSettings.Count; i++)
        {
            //Debug.Log(">>>> _adSettings[i].isShow: " + _adSettings[i].isShow + " / " + _adSettings[i].brandName.ToString());
            if (_adSettings[i].isShow)
            {
                if(_adSettings[i].brandName != ADBrandName.name.TAPJOY)
                {
                    //Debug.Log(">>>> _adSettings[i].rate: " + _adSettings[i].rate);
                    for (int j = 0; j < _adSettings[i].rate; j++)
                    {
                        _adRateList.Add(i);
                        log += i.ToString() + ", ";
                    }
                }
            }
        }
        Debug.Log("ADManager - adRateList: " + log);
    }

    



    private ADBrandName.name GetRandomAdBrand()
    {
        int randomIndex = Random.Range(0, _adRateList.Count);
        int adSettingIndex = _adRateList[randomIndex];
        //return ADBrandName.name.AD_COLONY;
        return _adSettings[adSettingIndex].brandName;
    }
}
