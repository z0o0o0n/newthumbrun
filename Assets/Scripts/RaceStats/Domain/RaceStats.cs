﻿namespace Com.Mod.ThumbRun.RaceStats.Domain
{
    using System.Collections;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.LuckyChance.Domain;
    using GameDataEditor;
    using UnityEngine;

    // 사용하지 말것. Analytics를 사용할 것.
    public class RaceStats : MonoBehaviour 
	{
		public delegate void RaceStatsEventHandler();
		public event RaceStatsEventHandler Prepared;

		private bool _isPrepared = false;
		private GDERaceStatsData _rawData;

		public bool isPrepared
		{
			get{ return _isPrepared; }
		}

		void Start () 
		{
			if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.RaceStats_RaceStats, out _rawData))
            {
                Debug.Log("RaceStats_RaceStats Data Prepared");

                _isPrepared = true;
                if (Prepared != null) Prepared();
            }
            else
            {
                Debug.LogError("Read Error RaceStats_RaceStats Data");
            }
		}
		
		void Update () 
		{
			
		}




		public void CountRace()
		{
			_rawData.raceCount += 1;
		}

		public void CountBadge(LuckyChanceBadge.badge badge)
		{
			if(badge == LuckyChanceBadge.badge.GOLD)
			{
				_rawData.goldBadgeCount += 1;
			}
			else if(badge == LuckyChanceBadge.badge.SILVER)
			{
				_rawData.silverBadgeCount += 1;
			}
			else if(badge == LuckyChanceBadge.badge.BRONZE)
			{
				_rawData.bronzeBadgeCount += 1;
			}
		}

		public void CountPrize(int prize)
		{
			_rawData.prizeAmount += prize;
			_rawData.prizeAmountWeek += prize;
		}

		public int GetRaceCount()
		{
			return _rawData.raceCount;
		}

		public int GetGoldBadgeCount()
		{
			return _rawData.goldBadgeCount;
		}

		public int GetPrizeAmount()
		{
			return _rawData.prizeAmount;
		}

		public int GetPrizeAmountWeek()
		{
			return _rawData.prizeAmountWeek;
		}
	}
}