﻿namespace Com.Mod.ThumbRun.RaceStats.Application
{
    using System.Collections;
    using System.Collections.Generic;
    using Com.Mod.ThumbRun.Achievement.Application;
    using Com.Mod.ThumbRun.LuckyChance.Domain;
    using UnityEngine;
	using Domain;

    public class RaceStatsService : MonoBehaviour 
	{
		public delegate void RaceStatsServiceEventHandler();
		public event RaceStatsServiceEventHandler Prepared;

		[SerializeField]
		private RaceStats _raceStats;
		private bool _isPrepared = false;

		public bool isPrepared
		{
			get{ return _isPrepared; }
		}

		void Awake ()
		{
			_raceStats.Prepared += OnPrepared;
		}

		void Start () 
		{

		}
		
		void Update () 
		{
			
		}

		void OnDestroy()
		{
			_raceStats.Prepared -= OnPrepared;
		}



		private void OnPrepared()
		{
			_isPrepared = true;
			if(Prepared != null) Prepared();
		}




		public void CountRace()
		{
			_raceStats.CountRace();
		}

		public void CountBadge(LuckyChanceBadge.badge badge)
		{
			_raceStats.CountBadge(badge);
		}

		public void CountPrize(int prize)
		{
			_raceStats.CountPrize(prize);
            SocialManagementService.instance.PostGoldToLeaderBoard(_raceStats.GetPrizeAmount());
        }

		public int GetRaceCount()
		{
			return _raceStats.GetRaceCount();
		}

		public int GetGoldBadgeCount()
		{
			return _raceStats.GetGoldBadgeCount();
		}

		public int GetPrizeAmount()
		{
			return _raceStats.GetPrizeAmount();
		}

		public int GetPrizeAmountWeek()
		{
			return _raceStats.GetPrizeAmountWeek();
		}
	}
}