﻿using UnityEngine;
using System.Collections;

public class JumpKey : MonoBehaviour
{
	public IJumpControllerContainer jumpController;

	void Start () 
	{

	}

	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			jumpController.Result.PressJumpButton(0);
		}
		if(Input.GetKeyUp(KeyCode.Space))
		{
			jumpController.Result.ReleaseJumpButton();
		}
	}
}
