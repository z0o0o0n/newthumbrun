﻿namespace Com.Mod.ThumbRun.Store.Domain
{
    using UnityEngine;
    using System.Collections;
    using GameDataEditor;
    using System.Collections.Generic;

    public class Store : MonoBehaviour
    {
        public delegate void EntryEventHandler();
        public event EntryEventHandler Prepared;

        private GDEStoreData _rawData;
        private bool _isPrepared = false;

        void Start()
        {
            if (GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Store_Store, out _rawData))
            {
                Debug.Log("=====> Entry Data Prepared");

                _isPrepared = true;
                if (Prepared != null) Prepared();
            }
            else
            {
                Debug.LogError("Read Error Entry Data");
            }
        }

        void Update()
        {

        }



        public List<GDEGoldProductItemData> GetGoldProductItemDatas()
        {
            return _rawData.goldProducts;
        }

        public GDEGoldProductItemData GetGoldProductItemDataById(int goldProductId)
        {
            for(int i = 0; i < _rawData.goldProducts.Count; i++)
            {
                if (_rawData.goldProducts[i].id == goldProductId) return _rawData.goldProducts[i];
            }
            return null;
        }

        public List<GDEEventGoldProductItemData> GetEventGoldProductItemDatas()
        {
            return _rawData.eventGoldProducts;
        }

        public GDEEventGoldProductItemData GetEventGoldProductItemDataById(int goldProductId)
        {
            for (int i = 0; i < _rawData.eventGoldProducts.Count; i++)
            {
                if (_rawData.eventGoldProducts[i].id == goldProductId) return _rawData.eventGoldProducts[i];
            }
            return null;
        }

        public int GetProductValueByProductId(string productId)
        {
            for(int i = 0; i < _rawData.goldProducts.Count; i++)
            {
                if(productId == _rawData.goldProducts[i].iapId)
                {
                    return _rawData.goldProducts[i].gold;
                }
            }

            for (int j = 0; j < _rawData.eventGoldProducts.Count; j++)
            {
                if (productId == _rawData.eventGoldProducts[j].iapId)
                {
                    return _rawData.eventGoldProducts[j].gold;
                }
            }

            //if (productId == "item.gold_a") return 5000;
            //else if (productId == "item.gold_b") return 16500;
            //else if (productId == "item.gold_c") return 30000;
            //else if (productId == "item.gold_d") return 65000;
            //else if (productId == "item.gold_e") return 210000;
            //else if (productId == "item.goldevent_a") return 33000;
            //else if (productId == "item.goldevent_b") return 49500;
            return 0;
        }
    }
}