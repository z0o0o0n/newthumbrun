﻿namespace Com.Mod.ThumbRun.Store.Application
{
    using UnityEngine;
    using System.Collections;
    using Domain;
    using GameDataEditor;
    using System.Collections.Generic;

    public class StoreService : MonoBehaviour
    {
        public delegate void EntryEventHandler();
        public event EntryEventHandler Prepared;

        [SerializeField]
        private Store _store;
        private bool _isPrepared = false;

        public bool isPrepared
        {
            get { return _isPrepared; }
        }


        void Awake()
        {
            _store.Prepared += OnStorePrepared;
        } 

        void Start()
        {

        }

        void Update()
        {

        }

        void OnDestroy()
        {
            _store.Prepared -= OnStorePrepared;
        }



        private void OnStorePrepared()
        {
            Debug.Log("=====> OnStorePrepared");
            _isPrepared = true;
            if (Prepared != null) Prepared();
        }



        public List<GDEGoldProductItemData> GetGoldProductItemDatas()
        {
            return _store.GetGoldProductItemDatas();
        }

        public GDEGoldProductItemData GetGoldProductItemDataById(int goldProductId)
        {
            return GetGoldProductItemDataById(goldProductId);
        }

        public List<GDEEventGoldProductItemData> GetEventGoldProductItemDatas()
        {
            return _store.GetEventGoldProductItemDatas();
        }

        public GDEEventGoldProductItemData GetEventGoldProductItemDataById(int goldProductId)
        {
            return GetEventGoldProductItemDataById(goldProductId);
        }

        public int GetProductValueByProductId(string productId)
        {
            return _store.GetProductValueByProductId(productId);
        }
    }
}