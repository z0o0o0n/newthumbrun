﻿namespace Com.Mod.ThumbRun.Store.View
{
    using UnityEngine;
    using System.Collections;
    using Application;
    using System.Collections.Generic;
    using GameDataEditor;
    using Com.Mod.ThumbRun.UI.Popup;
    using Com.Mod.ThumbRun.IAPManager.Domain;
    using Common.View;
    using User.Application;
    using ProductBanner.Application;
    using System.Linq;
    using UnityEngine.Purchasing;

    public class StorePopup : BasicPopup
    {
        [SerializeField]
        private StoreService _storeService;
        [SerializeField]
        private UIScrollView _scrollView;
        [SerializeField]
        private GameObject _goldProductItemPrefab;
        [SerializeField]
        private GameObject _eventGoldProductItemPrefab;
        [SerializeField]
        private GameObject _goldProductItemContainer;
        [SerializeField]
        private LoadingScreen _loadingScreen;
        [SerializeField]
        private UserService _userService;
        private ProductBannerService _productBannerService;
        private bool _isOpend = false;

        private Dictionary<int, GoldProductItem> _goldProductItems = new Dictionary<int, GoldProductItem>();
        private Dictionary<int, EventProductItem> _eventProductItems = new Dictionary<int, EventProductItem>();

        private void Awake()
        {
            _productBannerService = GameObject.FindGameObjectWithTag("ProductBanner").GetComponent<ProductBannerService>();

            _storeService.Prepared += OnStoreServicePrepared;
            transform.localPosition = Vector2.zero;
            transform.gameObject.SetActive(false);

            //Debug.Log("===========================");
            //Debug.Log("_storeService.isPrepared: " + _storeService.isPrepared);
            //if (_storeService.isPrepared) Init();
        }

        void Start()
        {

        }

        void Update()
        {
            if (_eventProductItems == null) return;
            if (!_productBannerService.isPrepared) return;

            List<GDEEventGoldProductItemData> eventGoldProductItemDatas = _storeService.GetEventGoldProductItemDatas();
            for (int i = 0; i < _eventProductItems.Count; i++)
            {
                //Debug.Log("count: " + _eventProductItems.Count + "id: " + eventGoldProductItemDatas[i].id + " / contain: " + _eventProductItems.ContainsKey(eventGoldProductItemDatas[i].id));
                if (_eventProductItems.ContainsKey(eventGoldProductItemDatas[i].id))
                {
                    if (_eventProductItems[eventGoldProductItemDatas[i].id] != null && _productBannerService.GetRemainingTimeByIndex(i) != null)
                    {
                        _eventProductItems[eventGoldProductItemDatas[i].id].SetTimeLimit(_productBannerService.GetRemainingTimeByIndex(i));
                    }
                }
            }
        }

        private void OnDisable()
        {
            
        }

        void OnDestroy()
        {
            _storeService.Prepared -= OnStoreServicePrepared;
        }





        public void ClosePopup()
        {
            if(_isOpend)
            {
                _isOpend = false;
                DestroyGoldProductItems();
                gameObject.SetActive(false);
                base.Close();
            }
        }

        public void OpenPopup()
        {
            if(!_isOpend)
            {
                _isOpend = true;
                CreateGoldProductItems();
                gameObject.SetActive(true);
                base.Open();
            }
        }

        private void OnBuyButtonClick(string iapId)
        {
            _loadingScreen.gameObject.SetActive(true);
            IAPManager.instance.PurchaseSuccess += OnPurchaseSuccess;
            IAPManager.instance.PurchaseFail += OnPurchaseFail;
            IAPManager.instance.Buy(iapId);
        }





        private void OnPurchaseSuccess(string productId)
        {
            IAPManager.instance.PurchaseSuccess -= OnPurchaseSuccess;
            IAPManager.instance.PurchaseFail -= OnPurchaseFail;
            _loadingScreen.gameObject.SetActive(false);

            int value = _storeService.GetProductValueByProductId(productId);
            _userService.Save(value);
            Debug.Log("=====> OnPurchaseSuccess / productId: " + productId + " / value: " + value);
        }

        private void OnPurchaseFail(string productId, string errorId)
        {
            IAPManager.instance.PurchaseSuccess -= OnPurchaseSuccess;
            IAPManager.instance.PurchaseFail -= OnPurchaseFail;
            _loadingScreen.gameObject.SetActive(false);

            int value = _storeService.GetProductValueByProductId(productId);
            Debug.Log("=====> OnPurchaseFail / productId: " + productId + " / value: " + value + " / errorId: " + errorId);
        }

        private void OnStoreServicePrepared()
        {
            Debug.Log("=====> Store Popup - OnStoreServicePrepared");
            Init();
        }

        private void Init()
        {
            
        }

        private void CreateGoldProductItems()
        {
            int count = 0;
            int displayCount = 0;
            //List<Product> storeProduct = IAPManager.instance.GetProducts(); // 스토어에서 가져온 상품정보
            //Debug.Log(">>>>>>>>>>>>>>>>>>>>>>> storeProduct - " + storeProduct);
            //Debug.Log(">>>>>>>>>>>>>>>>>>>>>>> storeProduct Count - " + storeProduct.Count);

            if (_productBannerService != null)
            {
                List<GDEEventGoldProductItemData> eventGoldProductItemDatas = _storeService.GetEventGoldProductItemDatas();
                for (int j = 0; j < eventGoldProductItemDatas.Count; j++)
                {
                    GameObject instance = GameObject.Instantiate(_eventGoldProductItemPrefab);
                    if (_productBannerService.GetBannerList()[j].isShow)
                    {
                        instance.transform.parent = _goldProductItemContainer.transform;
                        instance.transform.localScale = Vector2.one;
                        instance.transform.localPosition = new Vector2(displayCount * 200f, 0f);
                        displayCount++;
                    }

                    Debug.Log("=====> " + j + " itemId: " + eventGoldProductItemDatas[j].id);
                    EventProductItem eventProductItem = instance.GetComponent<EventProductItem>();
                    eventProductItem.BuyButtonClick += OnBuyButtonClick;
                    //string productPriceString;

                    Product product = IAPManager.instance.GetProductById(eventGoldProductItemDatas[j].iapId); // 스토어에서 가져온 상품정보

                    eventProductItem.SetValue(eventGoldProductItemDatas[j].id, eventGoldProductItemDatas[j].gold, product.metadata.localizedPriceString, eventGoldProductItemDatas[j].imageId, eventGoldProductItemDatas[j].badgeInfo, eventGoldProductItemDatas[j].info, eventGoldProductItemDatas[j].bgColor, eventGoldProductItemDatas[j].iapId);

                    UIDragScrollView uiDragScrollView = instance.GetComponent<UIDragScrollView>();
                    uiDragScrollView.scrollView = _scrollView;

                    _eventProductItems.Add(eventGoldProductItemDatas[j].id, eventProductItem);
                    count++;
                }
            }
            
            List<GDEGoldProductItemData> goldProductItemDatas = _storeService.GetGoldProductItemDatas();
            for(int i = 0; i < goldProductItemDatas.Count; i++)
            {
                GameObject instance = GameObject.Instantiate(_goldProductItemPrefab);
                instance.transform.parent = _goldProductItemContainer.transform;
                instance.transform.localScale = Vector2.one;
                instance.transform.localPosition = new Vector2(displayCount * 200f, 0f);

                displayCount++;

                Product product = IAPManager.instance.GetProductById(goldProductItemDatas[i].iapId); // 스토어에서 가져온 상품정보

                GoldProductItem goldProductItem = instance.GetComponent<GoldProductItem>();
                goldProductItem.BuyButtonClick += OnBuyButtonClick;
                goldProductItem.SetValue(goldProductItemDatas[i].id, goldProductItemDatas[i].gold, product.metadata.localizedPriceString, goldProductItemDatas[i].imageId, goldProductItemDatas[i].iapId);

                if (i % 2 == 0) goldProductItem.HideBg();

                UIDragScrollView uiDragScrollView = instance.GetComponent<UIDragScrollView>();
                uiDragScrollView.scrollView = _scrollView;

                _goldProductItems.Add(goldProductItemDatas[i].id, goldProductItem);
            }
        }

        private void DestroyGoldProductItems()
        {
            for (int j = 0; j < _eventProductItems.Count; j++)
            {
                _eventProductItems[_eventProductItems.Keys.ToArray()[j]].BuyButtonClick -= OnBuyButtonClick;
                Destroy(_eventProductItems[_eventProductItems.Keys.ToArray()[j]].gameObject);
            }
            _eventProductItems.Clear();

            for (int i = 0; i < _goldProductItems.Count; i++)
            {
                _goldProductItems[_goldProductItems.Keys.ToArray()[i]].BuyButtonClick -= OnBuyButtonClick;
                Destroy(_goldProductItems[_goldProductItems.Keys.ToArray()[i]].gameObject);                
            }
            _goldProductItems.Clear();
        }
    }
}