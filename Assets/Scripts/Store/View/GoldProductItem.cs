﻿namespace Com.Mod.ThumbRun.Store.View
{
    using UnityEngine;
    using System.Collections;
    using System;

    public class GoldProductItem : MonoBehaviour
    {
        public delegate void GoldProductItemEvent(string _iapId);
        public event GoldProductItemEvent BuyButtonClick;

        [SerializeField]
        public UISprite _itemBg;
        [SerializeField]
        private UISprite _productImage;
        [SerializeField]
        private UISprite _goldIcon;
        [SerializeField]
        private UILabel _goldPrice;
        [SerializeField]
        private UILabel _dallorIcon;
        [SerializeField]
        private UILabel _ipaPrice;
        [SerializeField]
        private UIDynamicButton _button;
        private string _iapId;

        void Awake()
        {
            _button.Click += OnButtonClick;
        }

        void Start()
        {

        }

        void Update()
        {

        }

        void OnDestroy()
        {
            _button.Click -= OnButtonClick;
        }




        private void OnButtonClick(GameObject target)
        {
            //Debug.Log("_iapId>>>>>>>>>>>>>>>>>>>>>>>>>: " + _iapId);
            if (BuyButtonClick != null) BuyButtonClick(_iapId);
            //IAPManager.instance.Buy(_iapId);
        }




        public virtual void SetValue(int productId, int gold, string ipaPriceString, int imageId, string iapId)
        {
            _productImage.spriteName = "GoldProductImage" + imageId;
            _goldPrice.text = String.Format("{0:##,##}", gold);
            _ipaPrice.text = ipaPriceString;
            _iapId = iapId;
        }

        public void HideBg()
        {
            _itemBg.gameObject.SetActive(false);
        }
    }
}