﻿namespace Com.Mod.ThumbRun.Store.View
{
    using UnityEngine;
    using System.Collections;

    public class EventProductItem : GoldProductItem
    {
        [SerializeField]
        private UILabel _badgeLabel;
        [SerializeField]
        private UILabel _infoLabel;
        [SerializeField]
        private UILabel _timeLimitLabel;
        // [SerializeField]
        // private UISprite _itemBg;

        void Start()
        {

        }

        void Update()
        {

        }



        public void SetValue(int productId, int gold, string ipaPriceString, int imageId, string badgeInfo, string info, Color bgColor, string iapId)
        {
            base.SetValue(productId, gold, ipaPriceString, imageId, iapId);
            _badgeLabel.text = badgeInfo.ToString();
            _infoLabel.text = info.ToString();
            base._itemBg.color = bgColor;
        }

        public void SetTimeLimit(string timeLimit)
        {
            _timeLimitLabel.text = timeLimit;
        }

        public void Disable()
        {
            
        }
    }
}