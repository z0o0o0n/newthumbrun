﻿using UnityEngine;
using System.Collections;

public class PageIndicatorTester : MonoBehaviour 
{
    public PageIndicator pageIndicator;

    private int _count = 4;

	void Start () 
	{
        pageIndicator.Moved += OnMoved;
        pageIndicator.Ended += OnEnded;
        pageIndicator.Init(10, _count);
	}
	
    private void OnMoved(int index)
    {
        //Debug.LogWarning("pageIndicator - OnMoved() / index: " + index);
    }

    private void OnEnded(int arrow)
    {
        //Debug.LogWarning("pageIndicator - OnEnded() / arrow: " + arrow);
    }

	void Update () 
	{
	    if(Input.GetKeyUp(KeyCode.LeftArrow))
        {
            _count--;
            if (_count <= 0) _count = 0;
            pageIndicator.Move(_count);
        }
        else if(Input.GetKeyUp(KeyCode.RightArrow))
        {
            _count++;
            if (_count > pageIndicator.GetLength()) _count = pageIndicator.GetLength() - 1;
            pageIndicator.Move(_count);
        }
	}
}
