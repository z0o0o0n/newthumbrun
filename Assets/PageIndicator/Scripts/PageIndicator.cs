﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class PageIndicator : MonoBehaviour 
{
    public delegate void PageIndicatorMoveEvent(int index);
    public delegate void PageIndicatorEndEvent(int arrow);
    public event PageIndicatorMoveEvent Moved;
    public event PageIndicatorEndEvent Ended;

    public UIWidget widget;
    public int dotSize;
    public GameObject dot;
    public int dotSpace;
    public float normalScale = 0.5f;
    public Color normalColor;
    public float selectionScale = 1;
    public Color selectionColor;
    public float moveTime = 1f;
    public AnimationCurve customElastic;

    private bool _isDebug = false;
    private bool _isInitialized = false;
    private int _length;
    private int _currentIndex = -1;
    private List<UISprite> _dotSpriteList = new List<UISprite>();

    void Awake ()
    {
        
    }

	void Start () 
	{
        widget = gameObject.GetComponent<UIWidget>();
	}
	
	void Update () 
	{
	
	}

    public void Init(int length, int startIndex)
    {
        if (_isDebug) Debug.Log("PageIndicator - Init() / length: " + length + " / start index: " + startIndex);
        _isInitialized = true;
        _length = length;
        CreateDots();
        Move(startIndex);
    }

    private void CreateDots()
    {
        for(int i = 0; i < _length; i++)
        {
            GameObject instance = GameObject.Instantiate(dot, Vector3.zero, Quaternion.identity) as GameObject;
            instance.transform.parent = transform;
            instance.transform.localScale = new Vector3(normalScale, normalScale, normalScale);
            instance.transform.localPosition = new Vector3(dotSpace * i - (dotSpace * (_length - 1) / 2), 0, 0);

            UISprite sprite = instance.GetComponent<UISprite>();
            sprite.depth = widget.depth + 1;
            sprite.width = dotSize;
            sprite.height = dotSize;
            sprite.color = normalColor;

            _dotSpriteList.Add(sprite);
        }
    }

    public void Move(int index)
    {
        if(0 <= index && index <= _length -1)
        {
            //Debug.LogWarning("currentIndex: " + _currentIndex + " / index: " + index);
            if(_currentIndex != index)
            {
                for (int i = 0; i < _length; i++)
                {
                    if (i == _currentIndex)
                    {
                        _dotSpriteList[i].transform.DOScale(normalScale, moveTime).SetEase(customElastic).SetUpdate(true);
                        _dotSpriteList[i].color = normalColor;
                    }

                    if (i == index)
                    {
                        _dotSpriteList[i].transform.DOScale(selectionScale, moveTime).SetEase(customElastic).SetUpdate(true);
                        _dotSpriteList[i].color = selectionColor;
                    }
                }

                _currentIndex = index;

                if (Moved != null) Moved(_currentIndex);
            }
        }

        if (index <= 0)
        {
            _currentIndex = 0;
            if (Ended != null) Ended(-1);
        }
        else if (index >= _length - 1)
        {
            _currentIndex = _length - 1;
            if (Ended != null) Ended(1);
        }
    }

    public int GetLength()
    {
        return _length;
    }

    public void SetPointerColor(Color color)
    {
        selectionColor = color;
        _dotSpriteList[_currentIndex].color = color;
    }
}