#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("Ruok6pthbW1paWxcDl1nXGVqbznSmB/3gr4IY6cVI1i0zlKVFJMHpGPxUZ9HJUR2pJKi2dVitTJwuqdRWvUgQRTbgeD3sJ8b954avhtcI63ubWxqZUbqJOqbDwhpbVztnlxGamhqf245P11/XH1qbzloZn9mLRwcQizKmyshE2QyXHNqbzlxT2h0XHoCCEwPAwIIBRgFAwIfTAMKTBkfCQ4ACUwfGA0CCA0eCEwYCR4BH0wNC+Nk2Eybp8BATAMc2lNtXODbL6PEsBJOWaZJubVjuge4zkhPfZvNwGpcY2pvOXF/bW2TaGlcb21tk1xxXO5o11zub8/Mb25tbm5tblxhamUVTA0fHxkBCR9MDQ8PCRwYDQIPCRwACUw+AwMYTC8tXHJ7YVxaXFheUUoLTOZfBpth7qOyh89DlT8GNwgeDQ8YBQ8JTB8YDRgJAQkCGB9CXBwACUwvCR4YBQoFDw0YBQMCTC0ZX1o2XA5dZ1xlam85aGp/bjk/XX9z6e/pd/VRK1uexfcs4kC43fx+tKwPXxubVmtAOoe2Y01ittYfdSPZAAlMJQIPQl1KXEhqbzloZ39xLRzdXDSANmhe4ATf43GyCR+TCzIJ0OMf7QyqdzdlQ/7elCgknAxU8nmZelx4am85aG9/YS0cHAAJTD4DAxildR6ZMWK5EzP3nklv1jnjITFhnUiOh73bHLNjKY1Lpp0BFIGL2Xt7tVoTres5tcv11V4ul7S5HfISzT41y2llEHssOn1yGL/b509XK8+5AxMtxPSVvaYK8EgHfbzP14h3Rq9zc/23cis8h2mBMhXoQYdazjsgOYAFCgUPDRgFAwJMLRkYBAMeBRgVXWuAEVXv5z9Mv1So3dP2I2YHk0eQCFlPeSd5NXHf+Jua8PKjPNatNDwYBQoFDw0YCUwOFUwNAhVMHA0eGGlsb+5tY2xc7m1mbu5tbWyI/cVl53XlspUnAJlrx05cboR0UpQ8Zb9MDQIITA8JHhgFCgUPDRgFAwJMHEpcSGpvOWhnf3EtHBwACUwvCR4Y23fR/y5IfkarY3HaIfAyD6Qn7HspEnMgBzz6LeWoGA5nfO8t61/m7dlWwZhjYmz+Z91NekIYuVBhtw56QEwPCR4YBQoFDw0YCUwcAwAFDxVMLy1c7m1OXGFqZUbqJOqbYW1tbRZc7m0aXGJqbzlxY21tk2hob25tGAQDHgUYFV16XHhqbzlob39hLRxZXl1YXF9aNnthX1lcXlxVXl1YXGQyXO5tfWpvOXFMaO5tZFzubWhcYWplRuok6pthbW1paWxv7m1tbDAbG0INHBwACUIPAwFDDRwcAAkPDUNc7a9qZEdqbWlpa25uXO3adu3fPgkABQ0CDwlMAwJMGAQFH0wPCR5cfWpvOWhmf2YtHBwACUwlAg9CXSW0GvNfeAnNG/ilQW5vbWxtz+5t+fIWYMgr5ze4eltfp6hjIaJ4Bb1kR2ptaWlrbm16cgQYGBwfVkNDG2pvOXFiaHpoeEe8BSv4GmWSmAfhx88d/is/Oa3DQy3flJePHKGKzyBMAwpMGAQJTBgECQJMDRwcAAUPDex4R7wFK/gaZZKYB+FCLMqbKyETPMbmubaIkLxla1vcGRlN");
        private static int[] order = new int[] { 1,50,22,48,34,22,31,43,42,21,54,29,50,40,31,56,46,34,42,49,57,48,26,55,50,49,53,58,52,34,56,34,51,57,39,39,46,45,54,59,49,55,44,49,52,53,49,57,57,49,53,54,57,58,57,59,56,58,59,59,60 };
        private static int key = 108;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
