#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("EKIhAhAtJikKpmim1y0hISElICP7ZYH20GfyRN8c1/DVWlVl+29pBig/zu+NRe6thyD4Hx/UGEI749ASD4CWI9nTcztbo0pe1UTc/LAZE7yKx4z62tQ3bH1SmZY3DN4Py3jAPKIhLyAQoiEqIqIhISD2bhUFXLCeea69H3ctXrxO9grV5vwF54zPwOA8RDLdvj4I5shZVQd9J7LG0GkMWzYR+F4zlZveO1Hy+68pR+PTno6mO/kgQNQLFjphkhRsMOMd/ROGrM+A8HWbjaZyVoKNjor1KN5ooyPZ3Z8p5XSqENXMNXZu0d16s0iJMEGP1kyJN8+6G9pp+u0mYA6yuoTL8O2pFEoKV7A+WffK04CyvqEXg6t/jY4MF4nqJfBRPSIjISAh");
        private static int[] order = new int[] { 0,4,3,6,9,9,8,12,10,9,12,13,12,13,14 };
        private static int key = 32;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
