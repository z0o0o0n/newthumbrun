// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by the Game Data Editor.
//
//      Changes to this file will be lost if the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using UnityEngine;
using System;
using System.Collections.Generic;

using GameDataEditor;

namespace GameDataEditor
{
    public class GDEAutoShieldData : IGDEData
    {
        private static string isInfoShowedKey = "isInfoShowed";
		private bool _isInfoShowed;
        public bool isInfoShowed
        {
            get { return _isInfoShowed; }
            set {
                if (_isInfoShowed != value)
                {
                    _isInfoShowed = value;
                    GDEDataManager.SetBool(_key+"_"+isInfoShowedKey, _isInfoShowed);
                }
            }
        }

        private static string hasAutoShieldKey = "hasAutoShield";
		private bool _hasAutoShield;
        public bool hasAutoShield
        {
            get { return _hasAutoShield; }
            set {
                if (_hasAutoShield != value)
                {
                    _hasAutoShield = value;
                    GDEDataManager.SetBool(_key+"_"+hasAutoShieldKey, _hasAutoShield);
                }
            }
        }

        public GDEAutoShieldData()
		{
			_key = string.Empty;
		}

		public GDEAutoShieldData(string key)
		{
			_key = key;
		}
		
        public override void LoadFromDict(string dataKey, Dictionary<string, object> dict)
        {
            _key = dataKey;

			if (dict == null)
				LoadFromSavedData(dataKey);
			else
			{
                dict.TryGetBool(isInfoShowedKey, out _isInfoShowed);
                dict.TryGetBool(hasAutoShieldKey, out _hasAutoShield);
                LoadFromSavedData(dataKey);
			}
		}

        public override void LoadFromSavedData(string dataKey)
		{
			_key = dataKey;
			
            _isInfoShowed = GDEDataManager.GetBool(_key+"_"+isInfoShowedKey, _isInfoShowed);
            _hasAutoShield = GDEDataManager.GetBool(_key+"_"+hasAutoShieldKey, _hasAutoShield);
         }

        public void Reset_isInfoShowed()
        {
            GDEDataManager.ResetToDefault(_key, isInfoShowedKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetBool(isInfoShowedKey, out _isInfoShowed);
        }

        public void Reset_hasAutoShield()
        {
            GDEDataManager.ResetToDefault(_key, hasAutoShieldKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetBool(hasAutoShieldKey, out _hasAutoShield);
        }

        public void ResetAll()
        {
            GDEDataManager.ResetToDefault(_key, isInfoShowedKey);
            GDEDataManager.ResetToDefault(_key, hasAutoShieldKey);


            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            LoadFromDict(_key, dict);
        }
    }
}
