// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by the Game Data Editor.
//
//      Changes to this file will be lost if the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using UnityEngine;
using System;
using System.Collections.Generic;

using GameDataEditor;

namespace GameDataEditor
{
    public class GDEEventGoldProductItemData : IGDEData
    {
        private static string goldKey = "gold";
		private int _gold;
        public int gold
        {
            get { return _gold; }
            set {
                if (_gold != value)
                {
                    _gold = value;
                    GDEDataManager.SetInt(_key+"_"+goldKey, _gold);
                }
            }
        }

        private static string idKey = "id";
		private int _id;
        public int id
        {
            get { return _id; }
            set {
                if (_id != value)
                {
                    _id = value;
                    GDEDataManager.SetInt(_key+"_"+idKey, _id);
                }
            }
        }

        private static string imageIdKey = "imageId";
		private int _imageId;
        public int imageId
        {
            get { return _imageId; }
            set {
                if (_imageId != value)
                {
                    _imageId = value;
                    GDEDataManager.SetInt(_key+"_"+imageIdKey, _imageId);
                }
            }
        }

        private static string ipaPriceKey = "ipaPrice";
		private float _ipaPrice;
        public float ipaPrice
        {
            get { return _ipaPrice; }
            set {
                if (_ipaPrice != value)
                {
                    _ipaPrice = value;
                    GDEDataManager.SetFloat(_key+"_"+ipaPriceKey, _ipaPrice);
                }
            }
        }

        private static string infoKey = "info";
		private string _info;
        public string info
        {
            get { return _info; }
            set {
                if (_info != value)
                {
                    _info = value;
                    GDEDataManager.SetString(_key+"_"+infoKey, _info);
                }
            }
        }

        private static string badgeInfoKey = "badgeInfo";
		private string _badgeInfo;
        public string badgeInfo
        {
            get { return _badgeInfo; }
            set {
                if (_badgeInfo != value)
                {
                    _badgeInfo = value;
                    GDEDataManager.SetString(_key+"_"+badgeInfoKey, _badgeInfo);
                }
            }
        }

        private static string iapIdKey = "iapId";
		private string _iapId;
        public string iapId
        {
            get { return _iapId; }
            set {
                if (_iapId != value)
                {
                    _iapId = value;
                    GDEDataManager.SetString(_key+"_"+iapIdKey, _iapId);
                }
            }
        }

        private static string bgColorKey = "bgColor";
		private Color _bgColor;
        public Color bgColor
        {
            get { return _bgColor; }
            set {
                if (_bgColor != value)
                {
                    _bgColor = value;
                    GDEDataManager.SetColor(_key+"_"+bgColorKey, _bgColor);
                }
            }
        }

        public GDEEventGoldProductItemData()
		{
			_key = string.Empty;
		}

		public GDEEventGoldProductItemData(string key)
		{
			_key = key;
		}
		
        public override void LoadFromDict(string dataKey, Dictionary<string, object> dict)
        {
            _key = dataKey;

			if (dict == null)
				LoadFromSavedData(dataKey);
			else
			{
                dict.TryGetInt(goldKey, out _gold);
                dict.TryGetInt(idKey, out _id);
                dict.TryGetInt(imageIdKey, out _imageId);
                dict.TryGetFloat(ipaPriceKey, out _ipaPrice);
                dict.TryGetString(infoKey, out _info);
                dict.TryGetString(badgeInfoKey, out _badgeInfo);
                dict.TryGetString(iapIdKey, out _iapId);
                dict.TryGetColor(bgColorKey, out _bgColor);
                LoadFromSavedData(dataKey);
			}
		}

        public override void LoadFromSavedData(string dataKey)
		{
			_key = dataKey;
			
            _gold = GDEDataManager.GetInt(_key+"_"+goldKey, _gold);
            _id = GDEDataManager.GetInt(_key+"_"+idKey, _id);
            _imageId = GDEDataManager.GetInt(_key+"_"+imageIdKey, _imageId);
            _ipaPrice = GDEDataManager.GetFloat(_key+"_"+ipaPriceKey, _ipaPrice);
            _info = GDEDataManager.GetString(_key+"_"+infoKey, _info);
            _badgeInfo = GDEDataManager.GetString(_key+"_"+badgeInfoKey, _badgeInfo);
            _iapId = GDEDataManager.GetString(_key+"_"+iapIdKey, _iapId);
            _bgColor = GDEDataManager.GetColor(_key+"_"+bgColorKey, _bgColor);
         }

        public void Reset_gold()
        {
            GDEDataManager.ResetToDefault(_key, goldKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetInt(goldKey, out _gold);
        }

        public void Reset_id()
        {
            GDEDataManager.ResetToDefault(_key, idKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetInt(idKey, out _id);
        }

        public void Reset_imageId()
        {
            GDEDataManager.ResetToDefault(_key, imageIdKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetInt(imageIdKey, out _imageId);
        }

        public void Reset_ipaPrice()
        {
            GDEDataManager.ResetToDefault(_key, ipaPriceKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetFloat(ipaPriceKey, out _ipaPrice);
        }

        public void Reset_info()
        {
            GDEDataManager.ResetToDefault(_key, infoKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(infoKey, out _info);
        }

        public void Reset_badgeInfo()
        {
            GDEDataManager.ResetToDefault(_key, badgeInfoKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(badgeInfoKey, out _badgeInfo);
        }

        public void Reset_iapId()
        {
            GDEDataManager.ResetToDefault(_key, iapIdKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetString(iapIdKey, out _iapId);
        }

        public void Reset_bgColor()
        {
            GDEDataManager.ResetToDefault(_key, bgColorKey);

            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            dict.TryGetColor(bgColorKey, out _bgColor);
        }

        public void ResetAll()
        {
            GDEDataManager.ResetToDefault(_key, goldKey);
            GDEDataManager.ResetToDefault(_key, idKey);
            GDEDataManager.ResetToDefault(_key, ipaPriceKey);
            GDEDataManager.ResetToDefault(_key, infoKey);
            GDEDataManager.ResetToDefault(_key, badgeInfoKey);
            GDEDataManager.ResetToDefault(_key, bgColorKey);
            GDEDataManager.ResetToDefault(_key, imageIdKey);
            GDEDataManager.ResetToDefault(_key, iapIdKey);


            Dictionary<string, object> dict;
            GDEDataManager.Get(_key, out dict);
            LoadFromDict(_key, dict);
        }
    }
}
