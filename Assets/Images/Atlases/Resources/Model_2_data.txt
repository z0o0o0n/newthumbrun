{"frames": {

"MOD_Desk.psd":
{
	"frame": {"x":2,"y":2,"w":16,"h":16},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":16,"h":16},
	"sourceSize": {"w":16,"h":16}
},
"MOD_Floor.psd":
{
	"frame": {"x":2,"y":20,"w":16,"h":16},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":16,"h":16},
	"sourceSize": {"w":16,"h":16}
},
"MOD_lamp.psd":
{
	"frame": {"x":2,"y":38,"w":16,"h":16},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":16,"h":16},
	"sourceSize": {"w":16,"h":16}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "Model_2.png",
	"format": "RGBA8888",
	"size": {"w":32,"h":64},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:6bdede6bf31fbbfc051432342fc31ceb:a6c3ca16e4ba1a0c08973b0501c7ba33:a3c607dd070f2d8abaeca656152120c0$"
}
}
