﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour {

    private GameManager _gameManager;
    [SerializeField]
    private UILabel _label;
	// Use this for initialization
    void Awake ()
    {
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        if (_gameManager.isRecordMode) _label.text = "Match";
        else _label.text = "Record";
    }

	void Start () {
	
	}
	
	void Update () {
	
	}

    public void PressButton()
    {
        if (_gameManager.isRecordMode)
        {
            _gameManager.isRecordMode = false;
            _label.text = "Record";
        }
        else
        {
            _gameManager.isRecordMode = true;
            _label.text = "Match";
        }
    }
}
