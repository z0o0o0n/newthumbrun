﻿interface IStartPointView
{
    void Activate();
    void Reset();
}