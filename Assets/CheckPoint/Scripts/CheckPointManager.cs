﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckPointManager : MonoBehaviour
{
    public delegate void CheckPointManagerEvent();
    public event CheckPointManagerEvent CheckPointActivated;
    public event CheckPointManagerEvent Complete;

    public bool debug = false;
    public GameObject targetPlayer;
    public StartPoint startPoint;
    public List<CheckPoint> checkPoints;

	void Start ()
    {
        startPoint.PlayerEnter += OnStartPointPlayerEnter;

        if (checkPoints.Count > 0)
        {
            // check point 유효성 검사 필요
            if(debug) Debug.Log("check points count : " + checkPoints.Count);

            for(int i = 0; i < checkPoints.Count; i++)
            {
                checkPoints[i].CheckPointActivate += OnCheckPointActivated;
            }
        }
        else if(checkPoints.Count == 0)
        {
            Debug.LogWarning("경고 : Check Point는 0개 일 수 없습니다.");
        }
	}

    void OnDestroy()
    {
        for (int i = 0; i < checkPoints.Count; i++)
        {
            checkPoints[i].CheckPointActivate -= OnCheckPointActivated;
        }
    }



    #region Private Functions
    private void OnStartPointPlayerEnter()
    {
        if(debug) Debug.Log("CheckPointManager - OnStartPointPlayerEnter()");
        if(debug) Debug.Log("Start Point 활성화 여부 : " + startPoint.isActivated);
        if(startPoint.isActivated)
        {
            bool complete = CheckCPActivation();
            if(debug) Debug.Log("모든 Check Point 활성화 여부 : " + complete);
            
            if (complete)
            {
                startPoint.ShowEffect();
                if (Complete != null) Complete();
            }
        }
    }

    private void OnCheckPointActivated()
    {
        if (CheckPointActivated != null) CheckPointActivated();
    }
    #endregion

    public void Play()
    {
        //Debug.Log("CheckPointManager - Play()");
        startPoint.Activate();
    }

    public bool CheckCPActivation()
    {
        bool isActivated = true;

        for (int i = 0; i < checkPoints.Count; i++)
        {
            if (!checkPoints[i].isActivated) isActivated = false;
        }

        return isActivated;
    }

    public void Reset()
    {
        startPoint.Reset();
        for (int i = 0; i < checkPoints.Count; i++)
        {
            checkPoints[i].Reset();
        }
    }

    public StartPoint GetStartPoint()
    {
        return startPoint;
    }

    public List<CheckPoint> GetCheckPoints()
    {
        return checkPoints;
    }

    public CheckPoint GetCheckPointByIndex(int index)
    {
        return checkPoints[index];
    }
}
