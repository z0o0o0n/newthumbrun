﻿interface ICheckPointView
{
    void Activate();
    void Reset();
}