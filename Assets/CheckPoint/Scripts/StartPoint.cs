﻿using UnityEngine;
using System.Collections;

public class StartPoint : MonoBehaviour
{
    public delegate void StartPointEvent();
    public event StartPointEvent PlayerEnter;
    public event StartPointEvent StartPointActivate;
    public event StartPointEvent EffectShow;
    public event StartPointEvent StartPointReset;

	public bool isActivated = false;
	
	void Start ()
	{
	}

	void OnTriggerEnter(Collider collider)
	{
        if (collider.tag == "Player")
        {
            if(PlayerEnter != null) PlayerEnter();
        }
	}

	public void Activate()
	{
        if (!isActivated)
        {
            isActivated = true;
            if (StartPointActivate != null) StartPointActivate();
        }
	}

    public void ShowEffect()
    {
        if (EffectShow != null) EffectShow();
    }

	public void Reset()
	{
        if (isActivated)
        {
            isActivated = false;
            if (StartPointReset != null) StartPointReset();
        }
	}

	void Update ()
	{
	
	}
}
