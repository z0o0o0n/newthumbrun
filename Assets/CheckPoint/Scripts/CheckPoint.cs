﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour
{
    public delegate void CheckPointEvent();
    public event CheckPointEvent CheckPointActivate;
    public event CheckPointEvent CheckPointReset;

    public bool isActivated = false;

	void Start () {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            Activate();
        }
    }

    public void Activate()
    {
        if(!isActivated)
        {
            isActivated = true;
            if (CheckPointActivate != null) CheckPointActivate();
        }
    }

    public void Reset()
    {
        if(isActivated)
        {
            isActivated = false;
            if (CheckPointReset != null) CheckPointReset();
        }
    }
}
